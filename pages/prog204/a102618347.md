### 何韵诗联合国发言 两度被中共官员打断（视频）
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/850x445_610523360608.jpg" target="_blank">
  <figure>
   <img alt="何韵诗联合国发言 两度被中共官员打断（视频）" src="https://i.ntdtv.com/assets/uploads/2019/07/850x445_610523360608-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  香港歌手何韵诗在联合国人权理事会对“反送中”事件发言，虽遭中共官员打断2次。（视频截图）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月09日讯】参与且声援港人“反送中”的香港知名歌手
  <span href="https://www.ntdtv.com/gb/何韵诗.htm">
   何韵诗
  </span>
  ，于7月8日应邀出席联合国人权理事会发言，她说，一国两制几近死亡，呼吁人权理事会保护香港民众，将中共除名。但她仅90秒的发言，两次被中共代表打断。
 </p>
 <p>
  人权理事会（UNHRC）正在瑞士日内瓦召开会议，香港知名歌手、民主活动人士
  <span href="https://www.ntdtv.com/gb/何韵诗.htm">
   何韵诗
  </span>
  ，在非政府人权组织“联合国观察”和“人权基金会”邀请下，8日下午约2时，在会场内全程以英文发言，以90秒的时间讲述香港问题。
 </p>
 <p>
  何韵诗说，6月份有200万香港市民上街游行，反对港府提出修订《
  <span href="https://www.ntdtv.com/gb/逃犯条例.htm">
   逃犯条例
  </span>
  》（送中条例），这项修例如果获得通过将允许把嫌疑人遣送中国大陆。
 </p>
 <p>
  她续说，修例会“拆去保护香港不受中国政府干预的防火墙”。她的发言才进行到30秒，中共常驻联合国代表团的官员戴德茂就将其打断，声称把香港与中国并列“违反联合国宪章”。
 </p>
 <div style="display: block; position: relative; max-width: 660px; max-height: 371px;">
  <div style="padding-top: 56.25%;">
  </div>
 </div>
 <p>
  人权理事会副主席、冰岛常驻联合国日内瓦办事处代表阿斯佩龙德裁决，让何韵诗继续发言。
 </p>
 <p>
  何韵诗说，在最近的“反送中”抗议示威中，警察向抗议者发射橡皮子弹。几十人被捕，有4人因绝望而自杀。而中共多年的欺骗性政策也令香港感到愤怒。
 </p>
 <p>
  何韵诗还说，香港主权移交北京以来，香港的自治逐渐流失。在北京的阻挠下香港至今仍未有普选，5名立法会成员被取消资格，书商及民运份子入狱，而在联合国登记注册“中英联合声明”已遭中共否认，港府被中共控制。
 </p>
 <p>
  她批评，中共不计代价的破坏香港民主自由，一国两制已“近乎死亡”。
 </p>
 <p>
  何韵诗话音没落，中共官员戴德茂再次强硬反对，声称何韵诗“攻击”一国两制。
 </p>
 <p>
  现场主持会议的副主席在中共官员抗议后，提点何韵诗留意中共论点后，让她继续发言。
 </p>
 <p>
  最后何韵诗询问联合国“是否会召集紧急会议保护香港人民？ 考虑到中国的践踏，联合国是否会把中国从人权理事会除名？”
 </p>
 <p>
  中共代表则在两次程序发言时，声称香港特别行政区是中华人民共和国组成的部分，“这位所谓NGO代表”发言将香港与中国并列，挑战了一个中国原则，违反联合国宪章，并指何韵诗攻击一国两制，中方表示坚决反对。
 </p>
 <p>
  何韵诗在联合国人权理事会发言后告诉媒体记者，她并没有试图把中国和香港“放在同一水平上”。但是，她说，“在香港我们大家都处于危险之中，因为我们处于成为另一个（中国）城市的边缘，我们会失去言论自由。”
 </p>
 <p>
  当晚何韵诗接受中央社电话访问时说，这是很重要的时刻，因为香港才刚经历过一个月的“反送中”运动，而港府至今没有聆听港人的声音。
 </p>
 <p>
  她表示，这次演说让她有机会向国际讲述当前香港问题，虽然只是90秒，但很重要。虽然香港实施“一国两制”，但外国人未必清楚当前香港的“两制”问题。
 </p>
 <p>
  何韵诗并证实，在她演说时，中方代表曾两度试图强令大会停止她的演说，但未得逞。
 </p>
 <p>
  6月初香港爆发反送中运动后，何韵诗不但参与了大游行，还站在这场运动的最前线。期间并透过社群宣扬理念，替示威民众打气。她在接受CNN专访时表示：尽管对政府失望，但绝不会放弃对民主自由的渴望。
 </p>
 <p>
  现年42岁的何韵诗曾在中国大陆举办过100多场音乐会。但在2014年她公开参加“雨伞运动”，并呼吁进行更自由的选举之后，遭北京全面封杀。 中共当局禁止她到中国大陆演出，也不许大陆网友从网站上下载其音乐。
 </p>
 <p>
  （记者文馨报导/责任编辑：剑彤）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102618347.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102618347.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102618347.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/09/a102618347.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

