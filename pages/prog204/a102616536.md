### 香港抗争蔓延大陆 坦克车进驻镇压画面曝光
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/1562268592814460.jpg" target="_blank">
  <figure>
   <img alt="香港抗争蔓延大陆 军车进驻镇压画面曝光" src="https://i.ntdtv.com/assets/uploads/2019/07/1562268592814460-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  3日晚起，当局出动全副武装警察上街暴力镇压抗议的武汉民众，很多人被打的头破血流。（合成图片）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月06日讯】不断发酵的香港大规模
  <span href="https://www.ntdtv.com/gb/抗议.htm">
   抗议
  </span>
  事件，已经蔓延至大陆。湖北民众连续多日上街抗议政府兴建大型垃圾焚烧厂。期间，遭到警方血腥镇压，5日晚，更传出中共军队战车进驻市区的画面。
 </p>
 <p>
  湖北省
  <span href="https://www.ntdtv.com/gb/武汉市.htm">
   武汉市
  </span>
  的民众抵制当地政府的垃圾焚烧发电厂兴建计划，仍在持续发酵中，媒体消息称，
  <span href="https://www.ntdtv.com/gb/抗议.htm">
   抗议
  </span>
  活动持续到3日晚间，大批民众聚集在街头，而当地政府出动武装警察。
 </p>
 <p>
  许多人将影片上传至推特，从画面中可以看到，大批武装警察排列整齐的朝民众跑过去，甚至不少人被拖到一旁殴打至头破血流，更惊悚的是民众拍到有
  <span href="https://www.ntdtv.com/gb/坦克车.htm">
   坦克车
  </span>
  开到武汉街头，担心是官方想加强镇压。
 </p>
 <p>
  7月5日，深夜凌晨时分，有民众拍到中共军队多部
  <span href="https://www.ntdtv.com/gb/坦克车.htm">
   坦克车
  </span>
  进驻武汉阳逻。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102616562" style="width: 600px">
  <span href="https://i.ntdtv.com/assets/uploads/2019/07/2004981-PH.jpg">
   <img alt="" class="size-medium wp-image-102616562" src="https://i.ntdtv.com/assets/uploads/2019/07/2004981-PH-600x375.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   当地民众拍到坦克车开上街头。（图／翻摄自脸书）。
  </figcaption><br/>
 </figure><br/>
 <p>
  据悉，大批阳逻居民自6月23日起上街拯救家园，当天百余名民众在保利园梦城处维权，发传单。由于警方事先获得消息，导致20余人当场被抓，第二天才获释。
 </p>
 <p>
  6月28日起抗议升级，上万名示威者拉着“还我青山绿水、拒绝垃圾污染”等横额，高呼口号游行抗议。上千警力手持警棍进行
  <span href="https://www.ntdtv.com/gb/暴力清场.htm">
   暴力清场
  </span>
  ，警察不分男女老少，专门殴打头部。有人被打得头破血流，送院急救。
 </p>
 <p>
  同时警方还逮捕录影或喊口号者。有民众高呼“放人”，但警方持续逮捕抗议者。
 </p>
 <p>
  当天整个阳逻街都陷入瘫痪状态。有现场网民称，民众的合法维权被当局指为暴乱。现场的妇女、小孩多人被打致重伤昏迷。
 </p>
 <p>
  当天示威过后，当地政府派出更多警察、特警，还刻意屏蔽网路讯号，不让相关消息散播出去。许多民众将事件发布到微博，瞬间即被清空。
 </p>
 <blockquote class="twitter-tweet" data-lang="zh-cn">
  <p dir="ltr" lang="zh">
   武汉阳逻游行公民遭遇当局暴力镇压
   <span href="https://t.co/Fbc0b9UTML">
    pic.twitter.com/Fbc0b9UTML
   </span>
  </p>
  <p>
   — 秋风悲电扇 (@qiufengbeids)
   <span href="https://twitter.com/qiufengbeids/status/1146808247665360896?ref_src=twsrc%5Etfw">
    2019年7月4日
   </span>
  </p>
 </blockquote>
 <p>
  <script async="" charset="utf-8" src="https://platform.twitter.com/widgets.js">
  </script>
 </p>
 <p>
  由于中共禁言，在大陆网络上无法搜索到相关消息。中国民众通过翻墙将现场影片发布在推特、脸书与YouTube等社交媒体上，真实情况令网友们震惊。
 </p>
 <p>
  视频显示，在武汉新洲区阳逻数以千计的民众挤满马路，从白天抗议到夜晚，大批防暴警察出现在当地。还有的视频显示，民众坐在警察人墙面前，试图阻止警察前进。另有影片显示，警察挥动警棍在驱散示威者。
 </p>
 <p>
  示威民众说，本来当地已有垃圾掩埋场，阳逻的空气因此变得十分糟糕，地下水也被污染，眼看2020年就要到期，但政府突然说要盖垃圾焚烧发电厂，居民认为垃圾焚化后产生致癌物，影响30多万人的健康，因此强烈反对。
 </p>
 <p>
  6月28日示威当晚，中共新洲区政府仅口头承诺，如果民众不同意，项目不开工。但当地民众说，垃圾发电厂已经在偷偷开工了，大家要的是彻底取消该项目，否则将持续抗议，直至令
  <span href="https://www.ntdtv.com/gb/垃圾场.htm">
   垃圾场
  </span>
  滚出阳逻。
 </p>
 <p>
  （记者李芸报导/责任编辑：戴明）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616536.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616536.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616536.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/06/a102616536.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

