### 传杨恒均大五毛妻子染香被带走 出境时被扣
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/2-26.png" target="_blank">
  <figure>
   <img alt="传杨恒均大五毛妻子染香被带走 出境时被扣" src="https://i.ntdtv.com/assets/uploads/2019/07/2-26-800x450.png"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  据微信群的留言显示，杨恒均此次被抓事件，令很多熟悉他的人感到意外，而他的妻子是国内大五毛染香的真身袁小靓，亦引发关注。（视频截图）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月08日讯】澳大利亚华裔作家
  <span href="https://www.ntdtv.com/gb/杨恒均.htm">
   杨恒均
  </span>
  ，被
  <span href="https://www.ntdtv.com/gb/中共.htm">
   中共
  </span>
  政府以涉嫌危害国家安全逮捕后，其大
  <span href="https://www.ntdtv.com/gb/五毛.htm">
   五毛
  </span>
  妻子
  <span href="https://www.ntdtv.com/gb/染香.htm">
   染香
  </span>
  （
  <span href="https://www.ntdtv.com/gb/袁小靓.htm">
   袁小靓
  </span>
  ），出境时被扣，且被当局带走问话，最少三部手机被没收。
 </p>
 <p>
  7月8日，澳洲媒体消息称，7月7日，
  <span href="https://www.ntdtv.com/gb/杨恒均.htm">
   杨恒均
  </span>
  的妻子
  <span href="https://www.ntdtv.com/gb/袁小靓.htm">
   袁小靓
  </span>
  一度被北京当局带走问话，最少三部手机被没收。5日袁小靓试图离开中国返回澳大利亚时被拒出境。
 </p>
 <p>
  杨恒均的朋友、悉尼科技大学教授冯崇义说，
  <span href="https://www.ntdtv.com/gb/中共.htm">
   中共
  </span>
  当局曾向袁小靓明确表示，如果她接受国际传媒访问，将受到惩罚。 目前，其代表律师已促请澳大利亚政府介入。
 </p>
 <p>
  袁小靓原名袁瑞娟，是中国网路知名的大
  <span href="https://www.ntdtv.com/gb/五毛.htm">
   五毛
  </span>
  “
  <span href="https://www.ntdtv.com/gb/染香.htm">
   染香
  </span>
  ”，因强烈支持中共的政治立场，且经常点赞中共，攻击西方的民主、自由，攻击批评中共的人，因此博取了“知名五毛”、“大五毛”等名号。
 </p>
 <p>
  袁小靓丈夫、现年53的杨恒均毕业于复旦大学国际政治系，曾在中共外交部、海南省政府、香港中资公司任职。他于2000年入籍澳洲，近年和家人居住美国。
 </p>
 <p>
  今年1月19日杨恒均在回国入境广州机场时，被中共政府以“涉嫌从事危害国家安全犯罪活动”罪名逮捕。
 </p>
 <p>
  杨恒均被抓后，香港《明报》引述北京消息说，杨恒均是中共江派安排的海外特务，这次被抓实质上是北京当局清洗海外江派特工的行动之一。
 </p>
 <p>
  <strong>
   大五毛染香出镜哭诉夫回国被抓
  </strong>
 </p>
 <p>
  据澳大利亚广播公司4月8日报导，袁小靓向媒体哭诉，她的丈夫被中共强力机构粗暴带走，且拒绝家属探视，甚至自己也失去自由，让她“处于崩溃状态”。
 </p>
 <p>
  她说她是冒着危险接受采访的，就是想让澳洲政府“关注澳洲公民在海外的情况，展示他们的关心，但我现在感到，说真的，十分失望”。
 </p>
 <p>
  4月9日，海外社交媒体上传出袁小靓哭诉丈夫被抓的一段视频。
 </p>
 <p>
 </p>
 <p>
  视频中袁小靓除了哭诉杨恒均如何被当局带走，家属和律师都不让见，至今下落不明之外，还提到，自己陪同杨恒均返国后，也被中共限制在上海，不能离境。她在视频中呼吁澳大利亚政府，关注自己的公民在海外的人权状况，给他的丈夫“一点点关怀”。
 </p>
 <p>
  讽刺的是去年5月24日，袁小靓还曾发表了一篇名为“一个奇女子的自白”的文章，称自己已赴美生活，自己的小孩已在纽约入学。她还自称：“反美是工作，赴美是生活”。
 </p>
 <p>
  再加上袁小靓此前经常发表维护中共专制独裁政权言论、大骂西方的民主自由，因此事件发生后，她的哭诉不但没能引来同情声，反倒遭来一片讽刺：“这就是五毛的下场”、“五毛悲歌”。
 </p>
 <p>
  有网民留言称，“3个月前，还高调嚣张地大放厥词，称共产党的专制也是民主，批评西方民主世界的种种不是！3个月后却对着西方记者镜头跪求西方国家拯救她的男人！”
 </p>
 <p>
  “在公知们呼吁民主和法律透明时，她曾经痛斥民主与法治；如今，也成了社会主义铁拳下的哀嚎者，不知道作何感想。”
 </p>
 <p>
  “袁小靓哭诉老公杨恒均被中共绑架时，忘了之前别人控诉被中共欺负时她的态度。”
 </p>
 <p>
  “过去作为头牌五毛党，专骂公知美分党和西方，讥讽民主法治，现在呼唤法治，求助西方。如此转变，令人唏嘘感叹。”
 </p>
 <p>
  “五毛像贪官一样，得势的时候，藐视法治，痛骂西方。一旦落难，呼唤法治，求助西方，到头来还是由他们责难的公知、律师，为他们说话。”
 </p>
 <p>
  对此，旅美作家张林说，袁小靓作为大五毛攻击民主文明世界，结果成为中共受害者，反而求助西方文明社会，所有的五毛都该意识到，他们不过是走狗、蚂蚁和微不足道的韭菜，随时可能被组织收割，随时可能被共产党踩在脚下。
 </p>
 <p>
  也有网民表示：“这给成千上万的五毛、小粉红一个警示：尽管你们一心‘为政府说话’，但这种一味吹捧会催生专横跋扈、草菅人命的官府，到头来倒楣会降临到自己头上。”
 </p>
 <p>
  （记者李芸报导/责任编辑：李泉）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102617781.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102617781.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102617781.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/08/a102617781.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

