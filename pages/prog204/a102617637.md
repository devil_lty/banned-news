### 港媒：川普香港牌引而不发 林郑成香港金融最大灰犀牛
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1154346090.jpg" target="_blank">
  <figure>
   <img alt="港媒：川普香港牌引而不发 林郑成香港金融最大灰犀牛" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1154346090-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  香港“反送中”大游行一波接一波，但港府对民众的诉求仍置之不理。(Anthony Kwan/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月08日讯】香港“反送中”大游行一波接一波，但港府对民众的诉求仍置之不理。不少人质疑川普（特朗普）政府为什么不制裁香港。港媒分析说，川普若打香港牌，香港可能爆发金融危机，倒楣的是全港人了，而根源却是林郑月娥这头香港史上最大的大灰犀牛。
 </p>
 <p>
  7月7日，香港再爆发23万人“反送中”（《
  <span href="https://www.ntdtv.com/gb/逃犯条例.htm">
   逃犯条例
  </span>
  》修订）大游行，示威者重申“五大诉求”，同时希望向大陆游客表达港人的和平诉求。
 </p>
 <p>
  “五大诉求”包括：撤回
  <span href="https://www.ntdtv.com/gb/逃犯条例.htm">
   逃犯条例
  </span>
  修订、收回暴动定义、撤销被捕人士控罪、追究警队滥权、立即实行双普选。
 </p>
 <p>
  在此之前，已发生多波更大规模的游行；先是103万港人6月9日走上街头；紧接着是6月12日逾万人包围立法会，遭警方开枪镇压；6月16日，再有200万人的大游行；6月26日晚，又有万人聚集在中环爱丁堡广场举行集会；7月1日，又有逾55万人上街。
 </p>
 <p>
  港人之所以发起反送中抗议活动，原因是此次“修例”受到中共政府的支持，港人担心中共会滥用该法律，把异己人士从香港引渡到大陆受审。同时该恶法也为中共要求香港当局的搜查、扣押和冻结资产以及提供文件铺平道路。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102617646" style="width: 600px">
  <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1154346080-1.jpg">
   <img alt="" class="size-medium wp-image-102617646" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1154346080-1-600x314.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   香港“反送中”大游行一波接一波，但港府对民众的诉求仍置之不理。(Anthony Kwan/Getty Images)
  </figcaption><br/>
 </figure><br/>
 <p>
  商业团体警告说，“引渡条例”的修改将削弱香港的法律自治，动摇国际社会对香港这个金融中心的信心。
 </p>
 <p>
  6月12日，港警开枪镇压示威者当天，香港的汇丰控股公司（HSBC Holdings Plc）和渣打银行（Standard Chartered Plc）等商业巨头，配合组织者号召的全市罢工运动，容许弹性上班时间。
 </p>
 <p>
  当天香港资金市场利率更是迎来全线上涨，港币大幅升值，股价下挫。
 </p>
 <p>
  香港《苹果日报》7月8日刊发评论文章说，这预示香港有可能爆发金融危机。这轮金融危机如果爆发，将与之前的亚洲金融风暴、次按危机等外部危机完全不同，这次是来自香港内部，根源在于林郑月娥这头香港史上最大的大灰犀牛。
 </p>
 <p>
  “灰犀牛”在金融领域，是指那些经常被提示，却没有得到充分重视的大概率风险事件。
 </p>
 <p>
  文章说，香港的货币政策是世界上独一无二的，也是符合原理的，特别与美元挂钩的联系汇率，虽然遭受过金融大鳄索罗斯攻击，仍屹然不倒。
 </p>
 <p>
  但随着林郑们粗暴处置修例，引发的不确定性增加，外资和港币外流现象的加剧，香港的外汇和港币双双减少——外汇减少发钞行不能印港币，香港将爆发史上没有过的资金枯竭危机。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/prog422848.htm">
   香港反送中
  </span>
  爆发之初，美国国会就因香港修例直接危及美国国民和企业，在香港的安全和利益为由，多次呼吁港府悬崖勒马，虽然修例一事被暂缓，但国会跨党派议员重推的《香港人权和民主法案》。
 </p>
 <p>
  据相关消息披露，该法案设下2020年普选立法会的“时限”，并明确要求普选特首，法案如获通过，港府不回应，势必影响美国与《香港关系法》，最严重可换来美方制裁。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102617647" style="width: 600px">
  <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1154346077.jpg">
   <img alt="" class="size-medium wp-image-102617647" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1154346077-600x338.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   香港“反送中”大游行一波接一波，但港府对民众的诉求仍置之不理。(Anthony Kwan/Getty Images)
  </figcaption><br/>
 </figure><br/>
 <p>
  港警开枪镇压示威者的次日（6月13日），美国国会及联邦众议员麦高文等联同共同主席共和党参议员鲁比奥、众议员史密斯，提出新版本的《香港人权和民主法案》，并获十位跨党派参众议员支持。
 </p>
 <p>
  美国总统川普也证实了他在6月底的G20峰会上同习近平谈了香港问题。
 </p>
 <p>
  对于川普政府为什么至今仍没有制裁香港？苹果评论文章分析有3大原因。
 </p>
 <p>
  1，议会新的法案还没通过；2，香港牌打出，中美贸易战达成协议的可能性基本为零，并会形成中美全面对抗，甚至延伸到军事领域。
 </p>
 <p>
  3，香港牌易打难收，若美国取消香港独立关税、高科技禁运特别是金融制裁后，香港瞬间失去国际金融中心和自由港地位，股市汇市联系汇率顷刻崩盘，假设香港改回到两制状态，美国也没有能力顷刻恢复繁荣的香港。这样制裁下去倒楣的是全港人了。
 </p>
 <p>
  文章称，新法可能有制裁官员条款，区分制裁就是精准打击不伤无辜。文章最后警告犯错误林郑们，只有他们辞职香港金融最大的威胁才能得以缓解。
 </p>
 <p>
  （记者李韵报导/责任编辑：祝馨睿）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102617637.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102617637.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102617637.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/08/a102617637.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

