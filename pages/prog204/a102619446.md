### 1980年代出生的中国人将无养老金可领
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1137674320-1.jpg" target="_blank">
  <figure>
   <img alt="1980年代出生的中国人将无养老金可领" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1137674320-1-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  更多信息显示，中国养老危机迫在眉睫。( NICOLAS ASFOURI/AFP/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月10日讯】中国内地多家媒体报导，
  <span href="https://www.ntdtv.com/gb/养老金.htm">
   养老金
  </span>
  2035年耗尽“
  <span href="https://www.ntdtv.com/gb/80后.htm">
   80后
  </span>
  ”成为无
  <span href="https://www.ntdtv.com/gb/养老.htm">
   养老
  </span>
  金可领的第一代，对此，官方7月9日做出回应。更多信息显示，中国养老危机迫在眉睫。
 </p>
 <p>
  7月9日，中国人社部有关司室负责人表示，媒体的报导是对
  <span href="https://www.ntdtv.com/gb/养老.htm">
   养老
  </span>
  保险制度理解不到位。“对于养老保险可持续发展问题……会保证制度的健康平稳运行。”
 </p>
 <p>
  该负责人指的报导是在7月8日，中国内地媒体在名为《社科院报告：
  <span href="https://www.ntdtv.com/gb/养老金.htm">
   养老金
  </span>
  2035年或将耗尽结余 专家建议年轻人尽早筹划养老投资》的文章中引用了《中国养老金精算报告2019-2050》数据。文中说，未来30年间，全国城镇企业职工基本养老保险基金当期结余在勉强维持几年的正数后便开始加速跳水，赤字规模越来越大，到2035年将耗尽累计结余。
 </p>
 <p>
  文中表示：“如果按照退休年龄60岁来算，到2035年最早一批‘
  <span href="https://www.ntdtv.com/gb/80后.htm">
   80后
  </span>
  ’也只有55岁，没有到达退休年龄。也就是说，‘80后’很有可能成为无养老金可领的第一代。”
 </p>
 <p>
  此前，中国人社部养老保险司司长聂明隽在人社部一季度的新闻发布会上称，“2018年末，企业养老保险基金累计结余达到了4.78万亿元。但是客观地说，结构性矛盾比较突出，地区之间很不平衡，不仅养老保险单位缴费比例不统一，基金结余差异也非常大。当前和今后一个时期，结构性矛盾问题是养老保险运行的主要矛盾。从去年下半年起，实施了养老保险基金中央调剂制度，有效地缓解结构性矛盾问题。”
 </p>
 <p>
  今年4月，中国官方发布的《关于印发降低社会保险费率综合方案的通知》显示，各省必须在2020年年底前实现以基金省级统收统支为主要内容的企业职工基本养老保险省级统筹，为养老保险全国统筹打好基础。
 </p>
 <p>
  作为中国最高学术研究机构、中南海智库之一的中国社会科学院4月10日发布《中国养老金精算报告2019-2050》（以下简称《报告》），报告列出养老金累计结余达到峰值和耗尽的时间。中国人面临未富先老的困境，学界认为养老金实际缺口更大。
 </p>
 <p>
  经过测算《报告》认为，未来30年中国的制度赡养率翻倍，2019年当期结余总额为1,062.9亿元，不过到2028年，当期结余可能会首次出现负数，为负1,181.3亿元。到2050年，当期结余可能达到负11.28万亿元。
 </p>
 <p>
  《报告》同时指出，城镇职工基本养老保险基金累计结余到2027年有望达到峰值6.99万亿元，然后开始下降，到2035年养老金有耗尽的可能性。
 </p>
 <p>
  2035年看似遥远，但是即使上世纪八十年代出生的人们，到那时还依然没超过退休年龄。
 </p>
 <p>
  中国社科院世界社保研究中心副秘书长齐传钧在《报告》发布会上表示，中国养老保险制度本身都会面临问题爆发，制度不改变、制度本身问题不解决，实际上趋势是不改变的，只是累计结余耗尽早晚而已。
 </p>
 <p>
  “《报告》同时对于16%+延退进行情景模拟，测算结果显示延退政策实施肯定是对养老金可持续有帮助，但是这个趋势仍然没有彻底改变，”齐传钧说。
 </p>
 <p>
  养老金是中国老百姓被政府收取并管理的“养命钱”，尽管按照中国财政部的数据，目前养老金总和结余可以满足各省17个月的支付，但不同省份间结余悬殊。
 </p>
 <p>
  《报告》显示，2019年当期结余排在第一位的广东高达2,000.7亿元，几乎等于排在第二到第十位即北京、湖南、四川、福建、云南、贵州、新疆、安徽和天津当期结余的总和，而当期收不抵支的省份则会高达16个。
 </p>
 <p>
  中国社保基金理事会副理事长王文灵表示，针对《报告》显示未来养老金存在支付缺口的情况，无论是现有结余资金规模还是储备基金累计规模，都不够充足。
 </p>
 <p>
  老龄人口数量剧增，劳动力日益不足，给整个中国经济带来巨大负担。大规模的养老金缺口，在当下已然出现。
 </p>
 <p>
  ──转自《看中国》
 </p>
 <p>
  （责任编辑：叶萍）
 </p>
 <p>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619446.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619446.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619446.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/10/a102619446.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

