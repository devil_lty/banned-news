### “花木兰她爸是习近平”？ 迪士尼真人版吵翻天
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/34bea417549d773131ed06edcdbc850c.jpg" target="_blank">
  <figure>
   <img alt="“花木兰她爸是习近平”？ 迪士尼真人版吵翻天" src="https://i.ntdtv.com/assets/uploads/2019/07/34bea417549d773131ed06edcdbc850c-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  花木兰真人版电影海报。（合成图片）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月10日讯】近日，由中国演员
  <span href="https://www.ntdtv.com/gb/刘亦菲.htm">
   刘亦菲
  </span>
  主演的
  <span href="https://www.ntdtv.com/gb/迪士尼.htm">
   迪士尼
  </span>
  真人版“
  <span href="https://www.ntdtv.com/gb/花木兰.htm">
   花木兰
  </span>
  ”预告片，引发大陆网友关注，片中饰演花木兰父亲的演员，因为长相极似中国国家主席习近平，不少网友调侃“原来木兰她爸是习近平”，而片中花木兰额头出现华为logo也引起网友调侃。
 </p>
 <p>
  7月8日凌晨，由中国演员
  <span href="https://www.ntdtv.com/gb/刘亦菲.htm">
   刘亦菲
  </span>
  、李连杰、巩俐、甄子丹等人主演的，
  <span href="https://www.ntdtv.com/gb/迪士尼.htm">
   迪士尼
  </span>
  史上首位真人亚裔公主电影——《
  <span href="https://www.ntdtv.com/gb/花木兰.htm">
   花木兰
  </span>
  》正式公布首款预告。
 </p>
 <p>
  90秒的预告片一开始，便呈现花木兰骑着马在绿色草原上奔驰回家，镜头接着切换到一栋土楼，花木兰的家人在土楼中告诉她，已经帮她安排了一场婚事，她虽然答应得并不情愿，但还是向父母承诺：“我会光宗耀祖的！”
 </p>
 <p>
  随后，片中呈现了木兰苦练武术的一连串镜头，同时她还画起了宫廷妆，并配由饰演礼仪嬷嬷的香港星郑佩佩的旁白：“娴静、沉着、淑雅、守礼是成为贤妻应有的品质。”
 </p>
 <p>
  接着是木兰战场杀敌的系列镜头。她一袭红衣，一头长发，手持一柄长剑，与敌人厮杀，显得武功高强，箭术了得。片中旁白：“这些也正是木兰所具备的特质。”
 </p>
 <p>
  预告片的最后，刘亦菲潇洒地献上一段武术时，还誓言：“我身负重任，挺身抗敌！”点明花木兰的抉择。
 </p>
 <p>
 </p>
 <p>
  预告片虽然仅90秒，但眼尖的网民仍发现“破绽”。根据木兰诗所述，花木兰应该是北方人，但却住在南方福建一带的土楼建筑。网友质疑：花木兰怎么会住在土楼呢？
 </p>
 <p>
  木兰诗还提到，“朝辞爷娘去，暮宿黄河边”，花木兰离家第一个晚上睡在黄河边，再度暗示她是北方人。据学者考证，花木兰应该是北魏人，约公元386年至535年之间。
 </p>
 <p>
  大陆网友挖苦说，如果一天内就从福建到黄河应该是搭高铁。也有网友批评，这部戏专拍给不懂中国文化的美国人看的！
 </p>
 <p>
  预告片中，花木兰的妆容也引发了众人的一片吐槽和质疑。浓重的腮红，黄色的额头，以及眉间酷似“华为logo”的红色图样，让刘亦菲原本淡雅精致的面容，像是涂上了一层人体彩绘，许多网友直呼这个妆容简直太辣眼睛，并戏称这是“华为妆”。
 </p>
 <p>
  外国观众也疑惑：为啥花木兰要把华为的Logo画在额头上？
 </p>
 <p>
  不过，有人解释说，这一妆容，正好符合《木兰辞》中的描写——“当窗理云鬓，对镜贴花黄。”认为这个妆容，还原了魏晋南北朝时期女子的妆容。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102619245" style="width: 600px">
  <span href="https://i.ntdtv.com/assets/uploads/2019/07/photo-1.jpg">
   <img alt="" class="size-medium wp-image-102619245" src="https://i.ntdtv.com/assets/uploads/2019/07/photo-1-600x312.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   饰演木兰父亲的演员因为长相极似习近平，意外掀起网友讨论。 （YouTube截图）
  </figcaption><br/>
 </figure><br/>
 <p>
  另外，预告片中最令网友关注的是饰演木兰父亲的演员，因为长相与习近平极其相似，网友调侃“原来木兰她爸是习近平”、“超像”、“看了好几篇，确认是习总”。
 </p>
 <p>
  还有网友发现，花家祖先派来守护木兰的木须龙和蟋蟀，没有出现在预告片中，而经典的音乐也有所变动。演员不演唱“花木兰”经典主题曲“Reflection”，不禁让许多动画版“花木兰”粉丝大失所望！
 </p>
 <p>
  有网友喊话：“把我的木须龙、蟋蟀、匈奴的老鹰（最后变烤火鸡）、胖媒婆、开趴老祖宗还有阿宁、阿尧、金宝，通通还给我呀！”
 </p>
 <figure class="wp-caption alignnone" id="attachment_102619247" style="width: 600px">
  <span href="https://i.ntdtv.com/assets/uploads/2019/07/photo.jpg">
   <img alt="" class="size-medium wp-image-102619247" src="https://i.ntdtv.com/assets/uploads/2019/07/photo-600x338.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   花木兰真人版电影海报。（合成图片）
  </figcaption><br/>
 </figure><br/>
 <p>
  尽管剧情改动了许多，但仍吸引不少中外网友关注，据说刘亦菲是打败1000个对手，才拿到花木兰这个角色，当时，迪士尼在全球五大洲选角，开出三条件，包括要会武术、会说英语，还要有明星风范。
 </p>
 <p>
  “花木兰”真人版电影，预计在2020年春天上档，故事讲述，有一天皇帝突然下令，每一户人家都必须要有一名男丁加入军队，一起抵抗北方入侵的外族。花木兰身为长女，毅然决定代替爸爸从军。她换上了男装、并用了男人的名字成功混入军中，屡建战功的故事。
 </p>
 <p>
  影片由新西兰导演妮基·卡罗（Niki Caro）执导，香港电影人江志强担任监制。改编自迪士尼1998年上映的同名经典动画片，由温明娜、米盖尔·弗尔、艾迪·墨菲配音饰演的动画片，曾获得一项奥斯卡、两项金球奖提名。
 </p>
 <p>
  （记者李芸报导／责任编辑：李泉）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619244.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619244.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619244.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/10/a102619244.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

