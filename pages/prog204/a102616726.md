### 【精彩推荐】军委间谍案内幕 /胡锦涛前世故事
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/b2711c03a5e6f257c2d6e64e31bf5f9c.jpg" target="_blank">
  <figure>
   <img alt="【精彩推荐】军委间谍案内幕 /胡锦涛前世故事" src="https://i.ntdtv.com/assets/uploads/2019/07/b2711c03a5e6f257c2d6e64e31bf5f9c.jpg"/>
  </figure><br/><br/>
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月06日讯】新唐人为读者推荐每日精彩文章和视频节目：
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616657.html" rel="noopener" target="_blank">
   中共军委间谍案更多内幕流出 传副部长子女被策反
  </span>
  <br/>
  中共军方惊爆高层间谍案。最新消息称，掌握登月计划等机密的中央军委装备发展部副部长
  <span href="https://www.ntdtv.com/gb/钱卫平.htm">
   钱卫平
  </span>
  被查，原因是其子女已被中央情报局（CIA）成功策反。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616729.html" rel="noopener" target="_blank">
   北戴河会议即将登场？警方发布限行通告
  </span>
  <br/>
  河北秦皇岛警方日前发布北戴河区限行通告。这意味着具有充满权斗谜团的中共北戴河秘密会议，可能即将登场。中共高层似乎也在为此做准备，党媒近日刊登了习近平的内部谈话，重提毛泽东“延安整风”，被认为是又一轮中共惨烈内斗将拉开帷幕。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616581.html" rel="noopener" target="_blank">
   柯文哲脱口而出“习大大”：总不能叫他习总书记
  </span>
  <br/>
  台北市长
  <span href="https://www.ntdtv.com/gb/柯文哲.htm">
   柯文哲
  </span>
  日前在上海和中共国台办主任举行会谈，他在谈到习近平时，脱口而出“习大大”三个字。事后，柯文哲对媒体表示，在台北讲习惯了，总不能煞有介事地叫习近平——习总书记。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616691.html" rel="noopener" target="_blank">
   逾40美议员致函川普 制裁新疆书记陈全国
  </span>
  <br/>
  美国有超过40名国会议员5日再次致函川普（特朗普）政府，敦促制裁中国新疆涉侵犯当地维吾尔族人及其他少数民族权益的官员，当中包括中共政治局委员、新疆党委书记陈全国。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616677.html" rel="noopener" target="_blank">
   李彦宏被“洗脑”段子热传 网售“宏颜获水”T恤
  </span>
  <br/>
  近日百度创始人李彦宏被泼水事件迅速登上热搜，网友戏称为“百毒宏颜祸水”，淘宝商城还立马推出“宏颜获水”主题T恤、手机壳等商品。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616654.html" rel="noopener" target="_blank">
   分析：人大政协“藏污纳垢” 王振华事件揭中共现状
  </span>
  <br/>
  上海新城控股董事长
  <span href="https://www.ntdtv.com/gb/王振华性侵.htm">
   王振华性侵
  </span>
  9岁女童事件持续发酵，近日再有媒体揭露，王振华除了拥有香港居留权和香港身份证，还拥有加拿大永久居民身份。这意味着具有中共上海政协委员身份，而且曾经是江苏省中共人大代表的王振华拥有“双重国籍”，而在中共人大和政协都不允许有“双重国籍”，这再次说明中共人大与政协成为权钱交易、藏污纳垢之地。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616597.html" rel="noopener" target="_blank">
   香港反送中再上街 示威者：积怨已久的爆发
  </span>
  <br/>
  香港“
  <span href="https://www.ntdtv.com/gb/反送中.htm">
   反送中
  </span>
  ”运动仍持续不断，一批香港母亲7月5日晚上在中环遮打花园再次集会，要求政府撤回修逃犯条例修订草案等。有消息称，本周六、周日香港仍有示威行动。示威者表示，香港“反送中”示威是香港社会积怨已久的一种爆发。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616570.html" rel="noopener" target="_blank">
   传中共军委曝严重谍案 副部长被抄家
  </span>
  <br/>
  7月5日，社交媒体中热传，掌握登月计划等机密的中共中央军委装备发展部副部长钱卫平，日前因涉间谍罪被带走。钱卫平是少将军衔，但传闻称其为中将，目前中共官方没有证实这一消息。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616536.html" rel="noopener" target="_blank">
   香港抗争蔓延大陆 坦克车进驻镇压画面曝光
  </span>
  <br/>
  不断发酵的香港大规模抗议事件，已经蔓延至大陆。湖北民众连续多日上街抗议政府兴建大型垃圾焚烧厂。期间，遭到警方血腥镇压，5日晚，更曝光出中共军队多部坦克车进驻武汉阳逻的画面。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/05/a102615943.html" rel="noopener" target="_blank">
   柯文哲访沪脸书直播受阻 “一家亲”成笑话
  </span>
  <br/>
  台北市长
  <span href="https://www.ntdtv.com/gb/柯文哲.htm">
   柯文哲
  </span>
  7月5日下午在上海与中共国台办主任刘结一会面，台北政府原计划直播“柯刘会”，但遭到中方人员阻止，直播被迫中断。外界认为，这起事件令中共所谓的“两岸一家亲”成为笑话。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616512.html" rel="noopener" target="_blank">
   香港8000父母哽咽发声：愿为你们挡棍挡子弹
  </span>
  <br/>
  港人反对《逃犯条例》，90后年轻人成为抗议主力军，8000名香港爸妈本周五在一个月内二度集会，他们举起写上“加油”等横幅，为年轻人打气。更有爸爸在台上，声泪俱下叮嘱“愿为你们挡棍挡子弹。”“路很长，我们要笑着抗争！”
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616754.html" rel="noopener" target="_blank">
   库德洛：美国没有谈判时间表 华为仍在黑名单上
  </span>
  <br/>
  日本川习会结束后，美中双方准备重回谈判桌。美国国家经济委员会主席库德洛周五（7月5日）表示，双方正在准备重启谈判，但美国没有谈判时间表，美国重视质量多于速度。他同时强调，美国对华为依然强硬，华为仍在出口管制的黑名单上。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/05/a102616253.html" rel="noopener" target="_blank">
   谷歌地图示三峡大坝扭曲变形 官方辟谣引质疑
  </span>
  <br/>
  日前谷歌卫星地图显示三峡大坝严重扭曲变形，引起海内外网友的关注。中共动用内外宣和网军大规模辟谣，但并没有完全打消民众的疑虑。这再次验证中共已深度陷入塔希罗陷阱。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/05/a102616245.html" rel="noopener" target="_blank">
   俄媒：人均收入近万美元中国走上苏联老路
  </span>
  <br/>
  中国最近宣布，人均国民收入已高于中等收入国家平均水平。俄罗斯媒体对此关注，分析了中国的成功，以及今天的中国与苏联的共同之处和区别。分析认为，虽然中国极力从苏联解体中吸取教训，想避免重复苏联命运，但中国将走或是已经部分走上了苏联的道路。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/mkt_ipad/2019/07/05/a102616367.html" rel="noopener" target="_blank">
   纳瓦罗证实习近平撤回悔棋 北京仍要求取消关税
  </span>
  <br/>
  川习会后美中重启贸易谈判，党媒宣告“胜利”。但白宫鹰派纳瓦罗稍后明示，重启谈判是因为北京妥协，撤回了5月悔棋。北京则重申达成协议必须取消关税。谈判似乎重返5月前，不同的是，美国又增加了关税并封杀了华为。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616707.html" rel="noopener" target="_blank">
   修炼人看到的胡锦涛前世：历史上一个真实故事
  </span>
  <br/>
  一位佛法修炼之人早年投书明慧网，讲到了当时时任中共党魁胡锦涛前世的一个故事，谈到了胡锦涛姓名的由来，整理出来，特飨读者，对于不信之人，只当神话故事吧。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/06/a102616539.html" rel="noopener" target="_blank">
   【中国禁闻】7月5日完整版
  </span>
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/05/a102616298.html" rel="noopener" target="_blank">
   【今日点击】中国武汉爆发大规模环保抗议
  </span>
 </p>
 <p>
  <strong>
   <span href=" https://www.ntdtv.com/gb/2019/02/15/a102512426.html" rel="noopener" target="_blank">
    《九评》编辑部：魔鬼在统治着我们的世界（播报版全集）
   </span>
  </strong>
 </p>
 <p>
  <strong>
   <span href=" https://www.ntdtv.com/gb/2018/06/08/a1378888.html" rel="noopener" target="_blank">
    《九评》编辑部：魔鬼在统治着我们的世界（文字版全集）
   </span>
  </strong>
 </p>
 <p>
  （责任编辑：文慧）
 </p>
 <p>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/05/a102616095.html" rel="noopener" target="_blank">
    习要严查假情报？/高层爆重大分歧
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/04/a102615395.html" rel="noopener" target="_blank">
    习下令7.1不准流血 /刘鹤险成罪人
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/03/a102614715.html" rel="noopener" target="_blank">
    韩正七一密赴深圳 /党内对手伺机倒习
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/02/a102613872.html" rel="noopener" target="_blank">
    公安部机密曝光 /王岐山尴尬自嘲
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/30/a102612439.html" rel="noopener" target="_blank">
    王沪宁再遭重挫？/习前后都有“坑”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/29/a102611757.html" rel="noopener" target="_blank">
    川习会敲定重启谈判 /党媒扔“手榴弹”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/28/a102611034.html" rel="noopener" target="_blank">
    李源潮早就死了?/习近平车队翻车
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/27/a102610400.html" rel="noopener" target="_blank">
    习访日再出状况 /川普提前“出牌”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/26/a102609552.html" rel="noopener" target="_blank">
    李克强微信群被封？/网传李源潮被抓
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/25/a102608819.html" rel="noopener" target="_blank">
    习收王牌报告 /刘鹤与王沪宁唱反调？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/24/a102607896.html" rel="noopener" target="_blank">
    湖南埋尸案真相 /“川习会”结果难料
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/23/a102607320.html" rel="noopener" target="_blank">
    习刚走金正恩收到川普“有趣来信”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/22/a102606824.html" rel="noopener" target="_blank">
    习拉金正恩壮胆？川普立即行动
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/21/a102605914.html" rel="noopener" target="_blank">
    广东紧跟香港示威 /习近平又挨一刀？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/20/a102605053.html" rel="noopener" target="_blank">
    上山下乡有隐情？/习2年打烂一手好牌
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/19/a102604362.html" rel="noopener" target="_blank">
    常委暗器毁习近平 /林郑还能走多远？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/18/a102603592.html" rel="noopener" target="_blank">
    高层对习渐失信任 /林郑月娥两度痛哭
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/17/a102602754.html" rel="noopener" target="_blank">
    习近平遭“埋刀设伏”/中南海慌了？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/16/a102602202.html" rel="noopener" target="_blank">
    中共顶级专家意外身亡/习出访王沪宁后院点火
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/15/a102601542.html" rel="noopener" target="_blank">
    香港发布重大消息 逃犯条例“猝死”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/14/a102600674.html" rel="noopener" target="_blank">
    中共军警悄悄抵港？ /习遇麻烦专机延飞
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/13/a102599980.html" rel="noopener" target="_blank">
    官二代揭高层内幕/港教协宣布全面罢课
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/12/a102599165.html" rel="noopener" target="_blank">
    王沪宁再夺笔杆子/习近平父亲显灵？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/11/a102598235.html" rel="noopener" target="_blank">
    曾庆红对习下手？/王立军口供流出
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/10/a102597452.html" rel="noopener" target="_blank">
    胡锦涛谈64内幕 /习近平掉“温水锅”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/09/a102596880.html" rel="noopener" target="_blank">
    传中共党内吵翻天/任正非“遗言”成真？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/08/a102596314.html" rel="noopener" target="_blank">
    习近平险被拉下台 /白羽鸟亡党预言
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/07/a102595527.html" rel="noopener" target="_blank">
    习近平被吁下台？/任正非留“遗言”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/06/a102594640.html" rel="noopener" target="_blank">
    习近平背要命黑锅 /胡锦涛曾遭软禁
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/05/a102593892.html" rel="noopener" target="_blank">
    习近平遭另类政变？/川普打出致命牌
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/04/a102593090.html" rel="noopener" target="_blank">
    64张64照片(慎入) /特稿警示习近平
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/03/a102592288.html" rel="noopener" target="_blank">
    薄瓜瓜大祸临头？/一类人让习更危险
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/02/a102591836.html" rel="noopener" target="_blank">
    习近平摇摆不定 王沪宁率先夺权？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/01/a102591340.html" rel="noopener" target="_blank">
    王岐山车队遇袭/ 江泽民赃款不保
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/31/a102590562.html" rel="noopener" target="_blank">
    习近平情报从哪来？内部要求“出硬招”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/30/a102589817.html" rel="noopener" target="_blank">
    美中女主播论战变味 /曾庆红干儿受审
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/19/a102581946.html" rel="noopener" target="_blank">
    习近平遭逼宫？ /华为4万员工拒回国
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/27/a102587334.html" rel="noopener" target="_blank">
    中共沉船计划曝光 /副国级被处死内幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/27/a102587274.html" rel="noopener" target="_blank">
    中南海重大误判 有人出招帮习解套
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/26/a102586812.html" rel="noopener" target="_blank">
    李克强紧急喊5字 /王岐山访欧遇麻烦
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/25/a102586347.html" rel="noopener" target="_blank">
    王岐山突访欧盟 /习大秘：有人想夺权
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/24/a102585608.html" rel="noopener" target="_blank">
    韩正设局拿下习？王沪宁卖力煸情
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/23/a102584835.html" rel="noopener" target="_blank">
    陆股511亿外资出逃 /胡春华急任新职
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/22/a102583989.html" rel="noopener" target="_blank">
    习近平“7”的定数 /胡锦涛带走老江
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/21/a102583249.html" rel="noopener" target="_blank">
    崔永元再发信号 /习近平考察“烂牌”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/20/a102582412.html" rel="noopener" target="_blank">
    习近平震怒无效？/刘士余案涉债市一姐
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/19/a102581940.html" rel="noopener" target="_blank">
    拉毛泽东打贸易战？/令计划吓到女记者
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/18/a102581514.html" rel="noopener" target="_blank">
    “反习派”镇不住？/习近平遭暗讽
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/17/a102580969.html" rel="noopener" target="_blank">
    习近平三张王牌被毙 /网现灭党魔咒
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/16/a102580367.html" rel="noopener" target="_blank">
    贸易战风云突变 / 川普宣布紧急状态
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/15/a102579579.html" rel="noopener" target="_blank">
    政治局突然开会/中共沉船计划曝光
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/14/a102578756.html" rel="noopener" target="_blank">
    李克强大喊稳定/习近平紧急训话
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/13/a102577805.html" rel="noopener" target="_blank">
    崔永元写藏头诗？/红二代警告北京
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/11/a102576421.html" rel="noopener" target="_blank">
    习近平在走钢丝？/崔永元：地狱走一回
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/10/a102575417.html" rel="noopener" target="_blank">
    美2千亿关税开打/崔永元发表声明
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/09/a102574561.html" rel="noopener" target="_blank">
    习近平真要负全责？川普不提习主席
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/08/a102573697.html" rel="noopener" target="_blank">
    刘鹤访美丢头衔/北京反腐首开杀戒
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://cn.ntdtv.com/gb/2019/05/07/a102572709.html" rel="noopener" target="_blank">
    习近平愿负全责？/中南海紧急定对策
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/06/a102571936.html" rel="noopener" target="_blank">
    中美谈判风云突变 中国股市千股跌停
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/04/a102570937.html" rel="noopener" target="_blank">
    千张六四照独家面世/毛纪念堂风水黑幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/03/a102570178.html" rel="noopener" target="_blank">
    武警政委去向不明/山东观音像变孔子
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/02/a102569477.html" rel="noopener" target="_blank">
    央视主播挖鼻曝光/中国手机变电子手铐
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/01/a102568706.html" rel="noopener" target="_blank">
    习讲话前罕见一幕 “跟党走”惹人烦
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/30/a102567935.html" rel="noopener" target="_blank">
    北京纪念五四 真相不敢外传
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/29/a102567028.html" rel="noopener" target="_blank">
    女谍撂倒四川副省长？/金正恩访俄惹祸
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/27/a102565895.html" rel="noopener" target="_blank">
    金正恩匆匆回国内幕/江泽民向美求饶
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/26/a102565032.html" rel="noopener" target="_blank">
    四川副省长被带走？/中共南海弃岛内幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/25/a102564230.html" rel="noopener" target="_blank">
    习近平健康异常？/江泽民被恶梦吓醒
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/24/a102563421.html" rel="noopener" target="_blank">
    习阅兵遭遇凶兆/外交部性贿赂曝光
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/23/a102562718.html" rel="noopener" target="_blank">
    习阅兵现尴尬：美国上校笑/央视出意外
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/22/a102561799.html" rel="noopener" target="_blank">
    李锐日记吓坏中共？/解密中南海爆炸性丑闻
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/20/a102560884.html" rel="noopener" target="_blank">
    崔永元留下悄悄话/曾庆红设局杀主席
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/19/a102560070.html" rel="noopener" target="_blank">
    国航林英案与神秘特工/朱军憔悴现身
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/18/a102559177.html" rel="noopener" target="_blank">
    赵本山影视城被烧/毕福剑悄悄复出？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/17/a102558369.html" rel="noopener" target="_blank">
    圣母院火灾预言/习两接班人罕见同台
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/16/a102557586.html" rel="noopener" target="_blank">
    圣母院起火原因/阿桑奇曝中共黑幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/15/a102556902.html" rel="noopener" target="_blank">
    天津首富涉百条命案/刘欢突发心脏病
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/13/a102555776.html" rel="noopener" target="_blank">
    王岐山养兵千日/中国四大魔王有代号
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/12/a102554998.html" rel="noopener" target="_blank">
    山东一处绝命风水 毁了三个“接班人”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/06/a102550628.html" rel="noopener" target="_blank">
    勒紧腰带跟党走？/江泽民包围香山内幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/02/a102547319.html" rel="noopener" target="_blank">
    第二中央在搅局？/习近平要查身边人
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/01/a102546062.html" rel="noopener" target="_blank">
    响水爆炸王泸宁施招？/胡锦涛夫妇遇奇景
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/29/a102544134.html" rel="noopener" target="_blank">
    习近平腿脚出状况？/北京疑陷更大危机
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/28/a102543384.html" rel="noopener" target="_blank">
    大红人骗习近平 /王岐山爆不死内幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/27/a102542467.html" rel="noopener" target="_blank">
    元老“逼宫”内幕 习近平躲过一劫
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/25/a102540801.html" rel="noopener" target="_blank">
    胡锦涛帮习压阵？/王岐山神秘回巢
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/23/a102539771.html" rel="noopener" target="_blank">
    习访欧后院爆炸 应急部长诡异消失
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/22/a102538967.html" rel="noopener" target="_blank">
    江苏爆炸大起底 /微信让全球用户裸奔
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/21/a102538211.html" rel="noopener" target="_blank">
    2019拍案惊奇:两起事件让习近平震惊
   </span>
  </strong>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616726.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616726.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616726.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/06/a102616726.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

