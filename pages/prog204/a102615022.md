### 【禁闻】法轮功反迫害20周年 各界齐说法轮功好
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月04日讯】中共迫害
  <span href="https://www.ntdtv.com/gb/法轮功.htm">
   法轮功
  </span>
  已经20年, 法轮功学员不屈不饶的和平反迫害也坚持了20年。最近一些了解和接触过法轮功学员的法律界人士和学者，向本台记者讲述他们对法轮功的认识，都交口称赞，法轮功是好功法，修炼者为人善良，乐于助人。
 </p>
 <p>
  原大陆
  <span href="https://www.ntdtv.com/gb/维权律师.htm">
   维权律师
  </span>
  卢伟华、中国人权理事会前主席刘青、人权思想家遇罗文等，都对
  <span href="https://www.ntdtv.com/gb/法轮功.htm">
   法轮功
  </span>
  及其修炼者留有好印象。
 </p>
 <p>
  原大陆
  <span href="https://www.ntdtv.com/gb/维权律师.htm">
   维权律师
  </span>
  卢伟华：“他们人是很好的，炼（法轮功）的这些人对你很友善的，我之前是这种感觉，非常好！”
 </p>
 <p>
  中国人权理事会前主席刘青：“我接触过，我感觉到法轮功是一个很良善的群体。”
 </p>
 <p>
  人权思想家遇罗文：“法轮功刚刚出现的时候，我印象挺好的，其实现在我印象也很好，而且还更好。”
 </p>
 <p>
  遇罗文说，法轮功在民间很深入人心。不仅邻居炼，自己有很多亲戚也在炼。
 </p>
 <p>
  而在渥太华大学人类学教授史国良眼里，法轮功修炼者是快乐的。
 </p>
 <p>
  渥太华大学人类学教授史国良：“他们也好像很快乐啊！而且我看有时候在大使馆前面好像还站在那边，在台湾有时候看过那个booth，在等待中国的观光客来。”
 </p>
 <p>
  中共退役军官胡伟表示，在1999年中共镇压之前，他知道有相当多的普通民众修炼法轮功。
 </p>
 <p>
  中共退役军官胡伟：“法轮功的出现，李洪志先生的出现，给很多中国人带来了信仰理念上一个选择的机会。这样有很多的人就开始修炼法轮功。”
 </p>
 <p>
  而在刘青看来，法轮功能够在短时间里吸引如此大量的炼功者，最主要一点，是创始人李洪志先生传授的这种功法，使人受益。
 </p>
 <p>
  刘青：“如果没有明显好处，不可能这么多人，尤其有很多是高学历的，有相当的思维能力，相当的辨识能力的人，他们都加入了法轮功，而且成了法轮功坚实的中坚力量。”
 </p>
 <p>
  99年，仅中国大陆就有上亿人在学炼法轮功。同年7月，江泽民集团开始以国家名义正式发动迫害。
 </p>
 <p>
  遇罗文：“99年的时候，我们都替法轮功，觉得很冤枉，因为法轮功讲真善忍，不伤害任何人，而且对这个社会还起稳定的作用，为什么（中共）它要迫害这些人呢?中共一向是欺负老实人。”
 </p>
 <p>
  卢伟华：“在我的印象里，在99年之前，没有人听说过炼法轮功的人特别坏，说它邪教。这我从来没听说过。”
 </p>
 <p>
  由于中共长期动用整个国家宣传机器，来传播对法轮功的污蔑之词，不少中国民众在信息封闭的情况下，产生误解。
 </p>
 <p>
  刘青：“我曾经碰到一些人，这些人受到中共的蛊惑，提起法轮功的时候，多有贬词，有些不屑的态度。我曾经就向这些人问过，我说，你说法轮功不好，你能说清它不好的具体事实吗？如果不能说出具体的事实，你说法轮功不好，那么实际上你就是在盲目的宣传中共的说法和态度。”
 </p>
 <p>
  迫害开始后的两年，也就是2001年，遇罗文来到了美国，很高兴又看到了法轮功学员。
 </p>
 <p>
  遇罗文：“你们（法轮功）能取得这么大的成就，首先说明你们这个方法做的很对，让（中共的）谎言破产嘛，所以才能得到全世界人们的拥护，因为我还看到好多洋人也参加了法轮功，也来修炼法轮功，可见你们很深入人心的。”
 </p>
 <p>
  目前法轮大法已经洪传100多个国家和地区，法轮功以及李洪志先生受到多国政府褒奖。
 </p>
 <p>
  胡伟：“绝大多数国家都认可法轮功是一个信仰修炼的团队，它都是合法的，在海外的情况这两年我也看了一下，特别是在澳洲，可以说经过长时期对中共的抗争，现在可以说法轮功修炼者表现的更加成熟、理性。”
 </p>
 <p>
  不仅如此，胡伟说，法轮功修炼者在传播中华传统文化，揭露中共暴政等领域，都发挥了非常重要的作用。
 </p>
 <p>
  采访/常春、陈汉 编辑/王子琦 后制/郭敬
 </p>
 <p>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102615022.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102615022.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102615022.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/03/a102615022.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

