### 为何冲击立法会？记者描述香港“暴徒”
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/gettyimages-1153137904-594x594.jpg" target="_blank">
  <figure>
   <img alt="为何冲击立法会？记者描述香港“暴徒”" src="https://i.ntdtv.com/assets/uploads/2019/07/gettyimages-1153137904-594x594-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  7月1日晚上9点，警方突然撤离立法会，示威人群进入立法会大楼内继续抗议，书写标语并拉起横幅。 （Anthony Kwan/Getty Images）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月03日讯】
  <span href="https://www.ntdtv.com/gb/prog424858.htm">
   香港七一大游行
  </span>
  当天，发生示威者
  <span href="https://www.ntdtv.com/gb/冲击立法会.htm">
   冲击立法会
  </span>
  事件，港府公开将事件定性为“暴力违法行为”，并声称“追究到底”。然而，数位在现场追踪的记者，向外界描述了事件过程和细节，在他们眼中，港府口中的“暴徒”，都是些善良、守望相助的香港青年。
 </p>
 <p>
  由于百万港人要求撤回《逃犯条例》的诉求，一直被港府漠视并持续暴力镇压示威民众。7月1日下午，一群示威者在立法会抗议，用铁制推车撞碎了立法会大厦的玻璃。然而，令外界质疑的是，驻守在立法会室内的大批香港防暴警察，在整个过程中袖手旁观，并未上前阻止、驱赶示威者。
 </p>
 <p>
  晚上9点，警方突然撤离立法会，示威人群进入立法会大楼内继续抗议，书写标语并拉起横幅。凌晨时分，警方开始清场，示威者迅速撤离，双方没有发生冲突。
 </p>
 <p>
  7月2日凌晨4点，港府召开记者会，宣布
  <span href="https://www.ntdtv.com/gb/冲击立法会.htm">
   冲击立法会
  </span>
  是“暴力违法行为”，并声称“追究到底”。然而，在现场追踪事件的记者看来，这些被港府贴上“暴徒”标签的示威者，却是一个又一个可爱又可敬的香港青年。
 </p>
 <p>
  数名记者对苹果日报描述了7月1日晚上，冲击立法会事件发生时的一些细节。1日晚间，示威人群进入立法会继续抗议，一名记者看到，示威者们拆下特首办外路边的铁栏作防守之用，只剩下一根根铁柱。为免他人碰到铁柱尖角而受伤，示威者们将劳工手套套在铁柱顶部。
 </p>
 <p>
  记者说，看得出，这些示威者很关心他人。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102614571" style="width: 600px">
  <img alt="" class="size-medium wp-image-102614571" src="https://i.ntdtv.com/assets/uploads/2019/07/235405-600x450.jpg">
   <br/><figcaption class="wp-caption-text">
    晚12点左右，警方将要清场的消息传出，一些示威者开始主动拿起垃圾袋收集垃圾。（大纪元）
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  晚12点左右，警方将要清场的消息传出，一些示威者开始主动拿起垃圾袋收集垃圾，有人拿起扫帚扫走地上索带等杂物，当扫帚扫到记者脚旁时，他还叮嘱“小心脚啊！”
 </p>
 <p>
  还有示威者分工合作，高喊“我们一人扫一边！”争取在撤退前清走垃圾。
 </p>
 <p>
  在夏悫道的一名记者看到，凌晨时分，警方清场时间逼近，有示威者在派发口罩和生理盐水，见有记者没有头盔，立即递上一个全新的头盔，并叮嘱记者小心安全。
 </p>
 <p>
  另一名记者说，就在警方发射催泪弹前5分钟，有示威者看到她只有头盔和眼罩，便递上N95口罩。记者说，还好有这个口罩，才可以让她在催泪烟中保护自己，继续采访工作。
 </p>
 <p>
  驻守龙和道的一名记者说，凌晨时分，警方防暴队开始清场，沿龙和道向立法会推进，警方施放多枚催泪弹，记者虽然戴着眼罩，也被催泪烟薰得泪流满面。
 </p>
 <p>
  众人迅速向添马公园方向走避，有示威者一边走，一边替其他人和记者用生理盐水洗眼，还有人递给记者保护力较好的眼罩，并叮嘱说“记者小姐小心！”
 </p>
 <p>
  记者说，虽然烟雾令她看不清这些示威者的面容，但烙印在她心中的是那一个个善良、守望相助的香港孩子。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102614572" style="width: 600px">
  <img alt="" class="size-medium wp-image-102614572" src="https://i.ntdtv.com/assets/uploads/2019/07/gettyimages-1153141544-594x594-600x400.jpg">
   <br/><figcaption class="wp-caption-text">
    7月2日凌晨时分，警方防暴队开始清场，沿龙和道向立法会推进，警方施放多枚催泪弹。（ANTHONY WALLACE/AFP/Getty Images）
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  记者们都表示，在他们心中，这些示威者并非暴徒。他们看到的是这些示威者对陌生人的爱、对香港人的爱和对香港的爱。
 </p>
 <p>
  另有香港记者Ching Kris在脸书发文称，他在立法会前厅查看时，看到示威者在柜子上贴了四张纸，写着“切勿破坏”，而柜子上的摆设，完好无缺。同样的情况也发生在地下的餐厅。这些示威者拿了雪柜内的饮料，但留下钞票，并在雪柜外贴纸写字，表明不是偷饮品。
 </p>
 <p>
  Ching Kris在文中称，这些香港青年冲击立法会是在展示他们对世道，对制度，对政权漠视他们声音的不满。由和平示威，到不合作运动，到武力冲击，甚至以身明志，他们几乎做尽了一切。
 </p>
 <p>
  但港府没有丝毫怜惜，它明知，一直不作回应，只会令年轻人的怒火加剧，逼使他们寻找更激进的方法，却依然为之。终于年轻人冲立法会了，港府便在凌晨四时出来谴责，强调“法治”，玩舆论战，誓要把年轻人都打成暴民。
 </p>
 <p>
  Ching Kris表示，在责怪这些冲击立法会的年轻人之前，我们是不是该先看看，为什么一个自诩先进文明的城市，会把一整代的年轻人逼到快要疯了？甚至逼死他们？他们眼中，已经看不到希望。
 </p>
 <p>
  （记者罗婷婷报导/责任编辑：戴明）
 </p>
 <p>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/03/a102614463.html">
    香港青年给爸妈的一封信：我们不是暴徒
   </span>
   <br/>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/02/a102614424.html">
    香港立法会现场记者：这是一个吃孩子的政权
   </span>
   <br/>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/01/a102613380.html">
    七一游行突现意外 港警放任示威者冲入立法会
   </span>
   <br/>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/02/a102614193.html">
    港版“国会纵火案”？一只手表引发港警构陷疑云
   </span>
   <br/>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/02/a102614238.html">
    现场视频揭立法会“暴力”之谜:手表门空城计全记录
   </span>
   <br/>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/02/a102614185.html">
    香港立法会震撼一幕 抗议者最后一刻抢出4“死士”（视频）
   </span>
   <br/>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/02/a102614100.html">
    美欧英聚焦香港立法会事件 英警告勿作镇压借口
   </span>
  </strong>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102614566.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102614566.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102614566.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/03/a102614566.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

