### 揭密：邓小平力挺王震镇压新疆 习仲勋胡耀邦受挫
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/5-21.jpg" target="_blank">
  <figure>
   <img alt="揭密：邓小平力挺王震镇压新疆 习仲勋胡耀邦受挫" src="https://i.ntdtv.com/assets/uploads/2019/07/5-21-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  习仲勋（右）与胡耀邦（左）对新疆提出的政策，都因王震反对被否定。（网络图片）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月11日讯】自中共篡权以来，新疆遭到中共多次血腥镇压。曾担任中共新疆分局第一书记的
  <span href="https://www.ntdtv.com/gb/王震.htm">
   王震
  </span>
  ，在新疆大开杀戒，当时习近平父亲
  <span href="https://www.ntdtv.com/gb/习仲勋.htm">
   习仲勋
  </span>
  提出反对意见，但被王否定。此后
  <span href="https://www.ntdtv.com/gb/胡耀邦.htm">
   胡耀邦
  </span>
  执政期间，对新疆提出怀柔政策，王震向
  <span href="https://www.ntdtv.com/gb/邓小平.htm">
   邓小平
  </span>
  告状，最终否决了胡的提议。
 </p>
 <p>
  <strong>
   <span href="https://www.ntdtv.com/gb/王震.htm">
    王震
   </span>
   对抗
   <span href="https://www.ntdtv.com/gb/习仲勋.htm">
    习仲勋
   </span>
  </strong>
 </p>
 <p>
  7月10日，自由亚洲电台刊登文章，披露
  <span href="https://www.ntdtv.com/gb/邓小平.htm">
   邓小平
  </span>
  替王震撑腰，在对待新疆的政策上，先否定习仲勋，再否定
  <span href="https://www.ntdtv.com/gb/胡耀邦.htm">
   胡耀邦
  </span>
  的内幕。
 </p>
 <p>
  文章称，胡耀邦和赵紫阳先后担任中共总书记期间，党内最著名的两大左王一文一武，“文”是邓力群，“武”是王震。他们两人当时都是习仲勋的“政治死敌”。
 </p>
 <p>
  王震当时担任著中共新疆分局第一书记，邓力群是常委兼宣传部长。他们在新疆的剿匪、平叛、土改、镇反中“左”的出奇。邓力群当时应王震要求起草的镇压指令内容中，把“大回族主义”作为镇反对象。“大回族主义”是把“回族”两个字做为“伊斯兰”和“清真”的代名词，包括了回族、维族、哈萨克等少数民族。
 </p>
 <p>
  当时主持中共西北局的习仲勋，不赞成在新疆地区的军队和机关中检举“反革命分子”，因为新疆的国民党政府和军队都是和平起义的，如果这样做会伤害许多起义人员。
 </p>
 <p>
  1951年4月13日，习仲勋给王震发了一个电报，要求不要在军队和机关中检举反革命分子，在镇反中不能提“大回族主义”。
 </p>
 <p>
  文章指，有曾经公开过的中共党史资料记载说，就在王震推敲对抗习仲勋的利弊得失时，邓力群给王震出主意，在《新疆日报》上刊载习仲勋讲话时用打省略号的方式，把关键的反左内容全部删除。
 </p>
 <p>
  随后，王震和邓力群对新疆少数民族特别是维吾尔族宗教势力大举镇压。
 </p>
 <p>
  <strong>
   王震血腥治疆
  </strong>
 </p>
 <p>
  2016年年底，大陆网站刊登《新疆人为什么怕王震 王震在新疆的功过是非》一文，披露王震当年血腥治疆的一些内幕。
 </p>
 <p>
  文章说：王震1949年率领自己的军队挺进新疆。之后担任中共中央新疆分局书记，新疆军区第一副司令员、代司令员兼政委。王震对新疆进行严厉整治，大开杀戒，持续了数月。
 </p>
 <p>
  后来，王震回京汇报工作时说：“老子杀得新疆50年出不了一个反革命”。一直到80年代，新疆的吓唬小孩就说：再哭！王胡子来了！（王震被新疆人称为王胡子，意即杀人的土匪。）
 </p>
 <p>
  2016年年中，西陆网也刊登过一篇标题为《新疆人怕到骨子里的将军 让维族群众养猪示爱国》的文章，讲述了当年王震血腥治疆的几则事例。王震命令新疆人每人必须养一头猪，猪养大了还必须亲手宰杀，宰杀了还必须看着你吃下几块肉，否则，劳改。
 </p>
 <p>
  一些新疆人不服，要到北京告状。王震说好啊，我欢迎，我还派车送你们去，车到荒凉的地方，将这些代表拉下车，全部“突突”了。王震由于在新疆杀人过多，引发新疆民怨，最终被调离新疆。
 </p>
 <p>
  <strong>
   胡耀邦怀柔政策被否定
  </strong>
 </p>
 <p>
  一直到1980年2月，胡耀邦当选为中共中央总书记，新疆局势才有些许改变。
 </p>
 <p>
  胡耀邦当选之初便召开书记处会议，讨论少数民族问题，他提出要放权给少数民族自治区，他的民族政策概括为6个字：“免征、放开、走人。”
 </p>
 <p>
  “免征”就是至少免除自治区两年的农牧税。“放开”，就是在所有经济领域都要放宽政策。“走人”，就是除必要的官员外，所有汉族官员都调回内地安排工作。
 </p>
 <p>
  胡耀邦鼓励各地少数民族一把手制定相关政策维护自身的权益，为本族谋福利。在新疆，他支持重修被毁的清真寺，修突厥文化史，平反冤假错案。
 </p>
 <p>
  1984年，胡耀邦在西藏工作座谈会上再次谈及少数民族问题时说：“我们的汉族干部如果不尊重人家，甚至想用汉族文化代替人家的文化，肯定是要碰大钉子的”。（1984年《西藏工作座谈会纪要》）
 </p>
 <p>
  在胡耀邦当政时期，新疆没有发生一起暴动事件，这与王震主政新疆一年（1950年）出现16次武装暴动形成鲜明对比。
 </p>
 <p>
  然而，胡耀邦力主施行对新疆和西藏少数民族的怀柔政策，曾在中共政权内部遭到否定，当时中共高层否定“胡乱邦”的内部行动，就是始自王震对撤销“生产建设兵团“的坚决抵制。
 </p>
 <p>
  邓小平三女儿邓榕曾在人民日报上发文称，1981年7月的一天，王震把她叫去，一脸不高兴地说“毛毛，回去跟你爸爸说，建设兵团和国营农场不能撤销！”
 </p>
 <p>
  邓榕回忆说：这一年的8月，她的父亲邓小平视察西北和新疆，王震同行。“这次视察以后，那位领导同志提出撤销兵团和国营农场的意见被否决了。”
 </p>
 <p>
  自由亚洲电台的评论文章称，这里说的“那位领导同志”谁都知道说的就是胡耀邦。1987年胡耀邦去世，宽松的民族宗教自由时期也宣告结束。
 </p>
 <p>
  （记者罗婷婷报导/责任编辑：文慧）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619887.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619887.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619887.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/11/a102619887.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

