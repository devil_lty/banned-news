### 为留学生配女伴 中共高校留学黑幕曝光（视频）
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/gettyimages-460371902-594x594.jpg" target="_blank">
  <figure>
   <img alt="为留学生配女伴 中共高校留学黑幕曝光（视频）" src="https://i.ntdtv.com/assets/uploads/2019/07/gettyimages-460371902-594x594-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  近日，山东大学为外国留学生配3名女学伴的消息在网络曝光，引发对来华留学生特权的质疑。示意图（ERIC FEFERBERG/AFP/Getty Images）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月11日讯】近日，
  <span href="https://www.ntdtv.com/gb/山东大学.htm">
   山东大学
  </span>
  为外国留学生配3名女学伴的消息在网络曝光，引发对来华留学生特权的质疑。有大陆网友批评道，某些大学把小女生介绍给外国男性留学生当学伴，究竟意欲何为？”还有网友直斥：“这是拉皮条。”
 </p>
 <p>
  大陆网友“沙和尚的微博”近日在微博披露，
  <span href="https://www.ntdtv.com/gb/山东大学.htm">
   山东大学
  </span>
  公布了2018年学伴的选拔结果，选出141名中国学生为来自巴基斯坦、尼泊尔、叶门、马来西亚、俄罗斯、阿富汗、肯尼亚等国的47名留学生做“学伴”。
 </p>
 <p>
  该网友还披露，该大学2017年也为留学生一对一招募学伴，现在加码了，为每位留学生配3个学伴。该微博贴文被大量转发，引发关于对留学生特权的质疑。
 </p>
 <p>
  根据山东大学公布的学伴名字显示，主要是以女生为主。该大学2017年留学生“学伴”名单显示，30名学伴就有26人为女生。
 </p>
 <p>
  此外，山东大学的招收学伴的报名表，以及学伴管理规定，还特别强调学伴的性别，将“结交外国异性友人”也列为选项之一。这引起网络上极大争议。
 </p>
 <p>
  ＠于洋律师在微博中批评道：“真搞不懂，某些大学把那些未谙人事，甚至可能连恋爱都没谈过的小女生介绍给外国男性留学生当学伴，究竟意欲何为？”
 </p>
 <p>
  网民小胖猫Q质问：“中国留学生在哪个国家能有这种待遇？外国留学生在哪个国家还能有这种待遇？”
 </p>
 <p>
  网友“铁马秋风吐谷浑”表示：“结交外国异性友人都是选项，简直无耻到极点！”
 </p>
 <p>
  还有网友斥责：“这是拉皮条”“这么不要脸的规定是人提出来的？还是大学？！”
 </p>
 <p>
  留学生在中国校园内私生活淫乱，也屡遭诟病。某大学辅导员2017年曾在网上爆料，“最近几年，中国高校内出现了这个现象，男性黑人留学生置身校园，如同置身于皇帝的后宫一般，这绝不是危言耸听，据我观察，我们班的一个男性黑人留学生平均每月都要换一个中国女大学生女友，这绝不是极端的个案。”
 </p>
 <p>
  近年来，中共为增加来华留学生数量，以高额奖学金、豪华宿舍、极低的招生标准、大幅降低的考核标准等增加吸引力。而为留学生提供学伴，在南京大学，东北师大，吉林大学，中山大学，哈工大都曾经开展过。
 </p>
 <p>
  据悉，留学生的奖学金1年可高达6万至10万元人民币，而中国学生奖学金只有几千元。留学生的特权待遇，不断引发国人的不满。
  <div class="video_fit_container">
  </div>
 </p>
 <p>
  2018年6月，一部访谈记录片《一个国家两种宿舍》在网路上热传，拍摄者疑似是在中国的外国留学生，她选择了北京、兰州两地的两所高校，以拍摄及采访方式，揭示中共教育部对本国学生和外国学生在住宿条件上的差别对待。
 </p>
 <p>
  调查结果令拍摄者感到非常惊讶，她表示，中国学生没有和外国学生享有同样的待遇，他们的条件很一般，政府似乎忽视了他们。
 </p>
 <p>
  这段视频引发网民广泛热议。有网民讽刺中共：“崇洋媚外，卑躬屈膝，祸国殃民。”还有网民直指：“中共为什么对外国人好，因为需要外国人为它装点门面。中共为什么对本国人坏，因为全都被它代表了。”
 </p>
 <p>
  （记者罗婷婷报导/责任编辑：文慧）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102620099.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102620099.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102620099.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/11/a102620099.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

