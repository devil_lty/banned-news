### 【禁闻】路透：统促党与中共合作 布局大选夺台湾
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月01日讯】台湾境内的亲共团体已引起国际媒体关注，
  <span href="https://www.ntdtv.com/gb/统促党.htm">
   统促党
  </span>
  和爱国同心会日前被揭，与中共“里应外合”，企图影响台湾明年1月总统大选，将亲共候选人推上台。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/路透.htm">
   路透
  </span>
  社26号以“亲共团体强化攻势以夺得台湾”（Pro-China groups step up offensive to win over Taiwan）为题，揭露台湾各个“亲共团体”已为台湾明年的总统大选展开计划。
 </p>
 <p>
  报导说，北京当局正积极在台湾建立支持者网络，以利润丰厚的商业机会换取台湾商人支持统一的立场。台湾亲共团体“
  <span href="https://www.ntdtv.com/gb/统促党.htm">
   统促党
  </span>
  ”透露，计划在全台各地举办研讨会和集会，以增进台湾人的理解和支持，扩大“红色部队”。
 </p>
 <p>
  据
  <span href="https://www.ntdtv.com/gb/路透.htm">
   路透
  </span>
  社的报导，统促党正积极布局
  <span href="https://www.ntdtv.com/gb/台湾大选.htm">
   台湾大选
  </span>
  ，试图拉下现任总统、民进党2020年总统候选人蔡英文，将亲共候选人推上台。
 </p>
 <p>
  台湾政治大学国家发展研究所教授李酉潭：“习近平在年初的时候定调九二共识就是一国两制以后，各单位就动起来，去执行这样一个政策，所以他们就会无所不用其极的想要透过影响台湾的选举来选择他们代理人，就会动员所有力量来做这个事情。”
 </p>
 <p>
  台湾涛涛国际企管顾问公司专任讲师田方宇表示，亲共团体早就被中共用金钱利益收买。
 </p>
 <p>
  台湾涛涛国际企管顾问公司专任讲师田方宇：“这一次在台湾的二零二零总统大选之中，他们准备跟中共里应外合，在台湾推出所谓的亲中共的候选人，这个是理所当然，一定的，而且他们一定也会用利益来诱惑或者说收买台湾的商人。”
 </p>
 <p>
  另一亲共团体爱国同心会秘书长、总干事兼执行长张秀叶表示，今年的首要任务是向农村地区宣扬“一国两制”，将类似香港的制度作为台湾自治的范本。
 </p>
 <p>
  李酉潭：“这些急统的势力就是他们原来的马前卒，先锋部队，他们就透过这些力量走入民间，走入地方，来宣扬一国两制的优点，将里乡村这些干部招揽，带他们去中国大陆免费旅游，用金钱收买。”
 </p>
 <p>
  田方宇表示，向台湾农村宣扬一国两制，就是中共前党魁毛泽东提出的农村包围城市策略。
 </p>
 <p>
  田方宇：“乡村的同胞比较善良，心地比较纯洁，但是比较不明是非，针对他们洗脑，或针对他们利诱，或针对他们统战，下手实在是更容易、更方便、更简单了。用乡村大量的人员，先针对他们洗脑之后再包围回城市。”
 </p>
 <p>
  统促党总裁张安乐在接受路透专访时，否认收受中共资金。报导说，文件没有显示中共与台湾亲共团体有资金往来，但潜在的关连性仍引起台湾国安部门忧心。
 </p>
 <p>
  田方宇：“一定有资金往来，没有钱，怎么去办这些事情，资金进来的方法相当相当多，可以透过洗钱管道，甚至透过假买卖的管道。从文件上来看，完全都是合法的，甚至要查也查不出来。”
 </p>
 <p>
  《半岛电视台》（Al Jazeera）去年透过记者卧底制作的纪录片揭露，中共通过台商捐款，把资金流入同心会和统促党，试图渗透统战台湾。
 </p>
 <p>
  台湾政府表示，统促党的行为很危险，但没有触犯法律。陆委会副主委邱垂正告诉路透：“只有修法巩固我们的法律，才能强化国家安全体系。”
 </p>
 <p>
  李酉潭：“我赞成邱副主委所讲的，要修法，对于敌对势力，要明确地规定什么样的行为是不可以的。兼顾国家安全之下，还要维持自由民主的体制。”
 </p>
 <p>
  田方宇：“我建议台湾政府，在面对假新闻谣言散播的时候，就用真的新闻来稀释它。在立法上，除了所谓的代理人法之外，也许还可以用所谓的对等公平法。我认为，这两个法律应该要同时双管齐下。 ”
 </p>
 <p>
  报导说，中共国台办及统战部的内部文件显示，中共正在实施以亲共组织为中心的计划，而且被列为“优先重点”。
 </p>
 <p>
  田方宇指出，中共把统战台湾列为优先政策是必然的，只有消灭中华民国，也才能让大陆民众对台湾的民主自由死心。
 </p>
 <p>
  采访/陈汉 编辑/陈洁 后制/周天
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102612568.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102612568.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102612568.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/30/a102612568.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

