### 安邦集团被正式接管 吴小晖“人财两空”
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-956748330-1.jpg" target="_blank">
  <figure>
   <img alt="安邦集团被正式接管 吴小晖“人财两空”" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-956748330-1-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  安邦集团董事长吴小晖被判有期徒刑近一年后，安邦集团于7月11日被大家保险集团正式接管。（GREG BAKER/AFP/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月11日讯】
  <span href="https://www.ntdtv.com/gb/安邦集团.htm">
   安邦集团
  </span>
  董事长吴小晖被判18年有期徒刑后，安邦集团于7月11日被大家保险集团正式接管，这预示著曾经的金融大鳄吴小晖与其安邦帝国彻底倾塌。吴小晖本人最终落的个“人财两空”。
 </p>
 <p>
  7月11日，中共银
  <span href="https://www.ntdtv.com/gb/保监会.htm">
   保监会
  </span>
  官网显示，经银保监会批准，近期，中共保险保障基金有限责任公司、中共石油化工集团有限公司、上海汽车工业（集团）总公司，共同出资设立了大家保险集团有限责任公司（以下简称“大家保险集团”）。
 </p>
 <p>
  大家保险集团将受让安邦人寿、安邦养老和安邦资管股权，并设立大家财险，受让安邦财险的部分保险业务、资产和负债。该集团的设立，则标志着
  <span href="https://www.ntdtv.com/gb/安邦集团.htm">
   安邦集团
  </span>
  风险处置工作取得阶段性成果。
 </p>
 <p>
  重组完成后，安邦集团将不开展新的保险业务。
 </p>
 <p>
  天眼查信息显示，大家保险集团成立于6月25日，注册资本为203.6亿元（人民币，下同），法定代表人为何肖锋。其最大股东为中共保险保障基金有限责任公司，持股比例达98.23%；上海汽车工业集团总公司，持股比例1.22%；中石油化工集团持股比例0.55%。
 </p>
 <p>
  这预示著曾经的金融大鳄吴小晖，与其安邦帝国彻底倾倒。
 </p>
 <p>
  吴小晖于2017年6月9日被带走调查。2018年2月23日，吴小晖被公诉，同时安邦集团被中共
  <span href="https://www.ntdtv.com/gb/保监会.htm">
   保监会
  </span>
  接管一年。
 </p>
 <p>
  同年5月10日，吴小晖被以“集资诈骗罪、职务侵占罪”判处18年有期徒刑，被没收财产105亿元人民币。
 </p>
 <p>
  之后，吴小晖不服一审判决，提起无罪上诉。同年8月16日，上海高院对吴小晖集资诈骗、职务侵占案二审宣判，裁定驳回上诉，维持原判。
 </p>
 <p>
  今年2月22日，中共银保监会发布消息称，决定将安邦集团接管期限延长一年。
 </p>
 <p>
  2018年9月，财新网在保监会前主席项俊波被“双开”后，曾发长文揭露项在任内滥用审批和监管权，导致民营资本大举进入保险业，而肖建华、吴小晖等资本大鳄进入保险业，搅得资本市场“腥风血雨”。
 </p>
 <p>
  安邦集团被指涉及很多的红二代，包括陈毅的儿子陈晓鲁曾经主持安邦，但实际上真正背后的是邓家的资产。吴小晖是邓家的外甥女婿。
 </p>
 <p>
  吴小晖还被指涉嫌参与了2015年江派人马针对现当局的经济政变。
 </p>
 <p>
  有北京历史学者曾表示，吴小晖是多个权贵家族的“白手套”，对这个案子的审理，可以对中共多个权贵家族及其形成的利益集团、金融寡头和行业寡头构成震慑。
 </p>
 <p>
  （记者李芸报导/责任编辑：李泉）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619996.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619996.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619996.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/11/a102619996.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

