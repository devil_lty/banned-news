### 辽宁女监 逼人写“转化书”的非人手段
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/1-468.jpg" target="_blank">
  <figure>
   <img alt="辽宁女监 逼人写“转化书”的非人手段" src="https://i.ntdtv.com/assets/uploads/2019/06/1-468-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  辽宁省女子监狱。（网络图片）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月01日讯】辽宁女子监狱所谓的“矫治监区”，用非人性的手段逼迫六十一岁的法轮功学员刘艳明写所谓的“
  <span href="https://www.ntdtv.com/gb/转化书.htm">
   转化书
  </span>
  ”。
 </p>
 <p>
  所谓“转化”，是在劳教所、劳改营、或专门的洗脑班（打着法制学校、法制学习班等等旗号的私设监狱），用各种暴力折磨、精神摧残的方式迫使法轮功学员放弃对“真、善、忍”的信仰。
 </p>
 <p>
  明慧网报导，刘艳明，锦州市义县刘龙台镇人，2017年8月31日被北票市国保大队和八图营乡派出所警察绑架，于中国新年前被非法庭审，律师和本人都做了无罪辩护。
 </p>
 <p>
  2018年4月中旬，律师告诉家属刘艳明被非法判4年。
 </p>
 <p>
  刘艳明于2018年10月被非法关押在辽宁女子监狱所谓“矫治监区”，遭受如下折磨：
 </p>
 <p>
  1.
  <strong>
   羞辱
  </strong>
  ：三名罪犯把她的衣服全部扒光，两名恶警说：“给她照相，发到网上，给她儿子看，说她精神病了。”
 </p>
 <p>
  2.
  <strong>
   不许去厕所
  </strong>
  ：三名罪犯看着她，每天都不停地辱骂她，从不允许她去厕所，恶意叫她忍受痛苦，尿床。
 </p>
 <p>
  3.
  <strong>
   灌药：
  </strong>
  经过量血压后，以血压高为理由强行对她灌药，用（做活的工具）竹板撬开牙，捏住鼻子，她的牙齿都被撬松动了。
 </p>
 <p>
  4.
  <strong>
   不许睡觉：
  </strong>
  她白天一直在床上盘腿坐直，晚上，在床上躺着，被监狱派人轮班几分钟叫一次，不许睡着。
 </p>
 <p>
  5.
  <strong>
   受冻：
  </strong>
  10月份，早晚冷，她只被许穿一套特别薄的“监狱服”。
 </p>
 <p>
  刘艳明遭受四十多天的痛苦折磨后，在意识不清楚的情况下，被强行在监狱方写完的所谓“
  <span href="https://www.ntdtv.com/gb/转化书.htm">
   转化书
  </span>
  ”上写名字。当时，这里还有二十多名法轮功学员也受到这样的迫害。
 </p>
 <p>
  现在，刘艳明被转到辽宁省女子监狱四监区非法关押。
 </p>
 <p>
  <strong>
   “转化”的由来
   <br/>
  </strong>
  <br/>
  1999年7月20日，江泽民发动了对法轮功学员的全面迫害。迫害之初，以为这些手无寸铁，打不还手、骂不还口的法轮功学员会很快被“镇压”下去，江泽民因此叫嚣：“三个月铲除法轮功。”
 </p>
 <p>
  谁知，法轮功学员的坚忍超过了迫害者的想像，全国各地的法轮功学员在各种高压下，将生死置之度外，前赴后继地、和平理性地持续上访，反而赢得了了解实情的各阶层民众的同情。
 </p>
 <p>
  三个月过去了，“铲除”却遥遥无期，江泽民集团骑虎难下，发现纯暴力并不能起多大作用，于是就想以各种手段摧毁法轮功学员的精神和意志，使之放弃信仰，“转化”由此而生。
 </p>
 <p>
  2001年1月，大陆各地纷纷出现中共各地党委、政府一把手参加的所谓“加强法轮功练习者教育转化工作会议”，会议公然叫嚣“转化工作是战胜法轮功的根本”。
 </p>
 <p>
  中共各级党委还要求各级各部门落实“转化”、“帮教小组”，并且制定所谓“转化”细则，要求落实到基层，落实到每个法轮功学员身上，并将对法轮功学员的所谓“转化率”列为各级领导的政绩考核指标，并与单位经济利益挂钩。
 </p>
 <p>
  报导说，辽宁女子监狱为达到让法轮功学员放弃修炼，写什么所谓的“悔过”，制定了一整套“转化”迫害方案，形成了一系列不成文的规定，这些手段被外界认为是严重的违法犯罪行为。
 </p>
 <p>
  根据对明慧网报导的统计：2018年1月至12月，共有251位法轮功学员被非法关押在辽宁省女子监狱遭受迫害；孙敏、冷冬梅等四位法轮功学员因在辽宁省女子监狱非法关押期间身体被严重摧残，含冤离世。
 </p>
 <p>
  ──转自《大纪元》
 </p>
 <p>
  （责任编辑：叶萍）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102612289.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102612289.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102612289.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/30/a102612289.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

