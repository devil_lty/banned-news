### 江泽民姘头出访密闻 被送上法庭不敢声张
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-56841059.jpg" target="_blank">
  <figure>
   <img alt="江泽民姘头出访密闻 被送上法庭不敢声张" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-56841059-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  中共前教育部长陈至立成为第一个在外访时被迫出庭的中共高官。（ADALBERTO ROQUE/AFP/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月10日讯】近年来，许多中共高官因迫害中国民众被陆续告上国际法庭。相对那些收到法庭传票后迅速消失、遁回国内的中共官员，
  <span href="https://www.ntdtv.com/gb/江泽民.htm">
   江泽民
  </span>
  最高级别情妇
  <span href="https://www.ntdtv.com/gb/陈至立.htm">
   陈至立
  </span>
  ，在外访国的法庭上没能找到回避的借口，成为来不及隐遁而被迫上法庭的第一人。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/陈至立.htm">
   陈至立
  </span>
  于1998年任中共教育部长，掌控中国教育大权，中共“十六大”后任国务委员，仍主管教育，同时分管卫生。陈曾按照
  <span href="https://www.ntdtv.com/gb/江泽民.htm">
   江泽民
  </span>
  的指示，下令“高校扩招”和“教育产业化”，被外界认为是祸国殃民。陈至立不但将教育产业化，造成百姓“看病难”的医疗产业化也跟其有关系。
 </p>
 <p>
  陈至立更大的罪行是自江泽民1999年残酷镇压法轮功之后，在教育系统积极追随江迫害法轮功学员。陈至立早已被海外“追查迫害法轮功国际组织”列为被追查的对象。
 </p>
 <p>
  <strong>
   第一个被迫出庭的中共高官陈至立
  </strong>
 </p>
 <p>
  据《
  <span href="https://www.ntdtv.com/gb/江泽民其人.htm">
   江泽民其人
  </span>
  》一书讲述，2004年7月19日时任中共国务委员、江泽民的死党陈至立在坦桑尼亚访问期间，被人权律师送上当地法庭，她被指控在中国教育系统对法轮功学员实施酷刑和虐杀，并于7月19日被迫出庭应讯。
 </p>
 <p>
  相对那些收到法庭传票后迅速消失、遁回国内的中共官员，在坦桑尼亚以贵宾身份进行外交访问的陈至立，在被访国的法庭上没能找到回避的借口，成为来不及隐遁而被迫上庭的第一人。
 </p>
 <p>
  陈至立1998年到2003年担任中共教育部长，是分管文教工作的国务委员。她利用与江泽民的特殊关系和获得的特权，强行在中共教育系统推行江的迫害政策，使得中国教育系统镇压法轮功极为凶残。
 </p>
 <p>
  陈至立利用职位之便，有系统地向学生灌输“仇恨及镇压法轮功有理”的思想，并直接导致教育界大量炼法轮功的学生、教职员工被关押，至少61人被迫害致死。
 </p>
 <p>
  2004年12月7日，坦桑尼亚法庭对陈至立被控酷刑和虐杀罪进行了初步司法审查。消息来源透露说，受害者提供的大量证据足以证明陈至立受到的指控成立。
 </p>
 <p>
  <strong>
   陈至立是江泽民最高级别情妇
  </strong>
 </p>
 <p>
  据媒体报导，在江泽民的情妇里，级别最高、对江最“忠心”的是陈至立。
 </p>
 <p>
  文革结束后，陈至立在中科院上海硅酸盐研究所工作，与江泽民大儿子江绵恒在同一所。江泽民任中共上海市委书记后，在江绵恒的引见下，陈至立与江泽民一拍即合，相见恨晚。
 </p>
 <p>
  1988年，陈至立被江委以上海市委宣传部长，市委的人都知道，她的职位是用什么换来的。
 </p>
 <p>
  “六•四”前，上海《世界经济导报》因支持学运被江泽民查封。1989年5月，江泽民进京，时任总书记赵紫阳严厉批评江处理导报事件不当，江感到大祸临头。陈至立向江表示：中央怪罪下来，她一个人把责任全揽下来就是了。
 </p>
 <p>
  据报，陈至立和江几十年的交情表现在她与江在政治上的“生死恋”，丑陋的政治组合。凡是江要迫害的，陈至立一定变本加厉去镇压；凡是江最爱的，陈至立一定费尽心机去培植。
 </p>
 <p>
  陈至立紧靠江泽民，官至中共教育部长、中共人大常委会副委员长。陈至立1998年被任命为中共教育部部长，掌控了中国教育大权。2003年，她又晋升为主管教科文体的国务委员，成为“国家领导人”。
 </p>
 <p>
  港媒曾报导，陈至立最害人的是提出“教育产业化”政策，也就是将中国教育当成做生意一样。
 </p>
 <p>
  教育界本应是一块净土，但陈至立却推销所谓的“长远经济眼光”，搞“教育产业化”。学校成了肮脏生意的交易场，教育界乱收费现象愈演愈烈，伪造文凭，花钱买文凭等事情层出不穷。
 </p>
 <p>
  陈至立还把教育当作是巩固江泽民统治的重要手段，从小学开始对学生进行洗脑，利用教育传播仇恨和谎言。组织所谓校园百万签名活动，让中小学生签字支持江泽民迫害法轮功。
 </p>
 <p>
  陈至立主管教育部7年，不择手段摧毁教育体系，采用一切手段毒害青少年。教育改革混乱，教学质量倒退，教风学风涣散堕落。嫖、赌、抄三风充斥校园，学生素质明显下降。大学生毕业后求职困难的现象司空见惯。引起极大的民愤。
 </p>
 <p>
  近年来，北京当局持续清洗、整顿中共教育界。外界认为，陈至立或面临清算。
 </p>
 <p>
  （记者文馨报导／责任编辑：戴明）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619083.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619083.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619083.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/10/a102619083.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

