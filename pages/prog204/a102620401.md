### 【禁闻】北戴河会议前 中共高层危机意识突显
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月12日讯】中国的秦皇岛市北戴河区，将实施夏季的限行措施，外界认为，有可能中共夏季的
  <span href="https://www.ntdtv.com/gb/北戴河会议.htm">
   北戴河会议
  </span>
  将要召开了。不过今年的北戴河会议前夕，
  <span href="https://www.ntdtv.com/gb/中共高层.htm">
   中共高层
  </span>
  连续召开会议，显示出比往年更强的危机意识。
 </p>
 <p>
  日前，河北省秦皇岛市公安局发布旅游旺季交通限行通告，北戴河区将于7月13号到8月18号实施限行措施。这表示
  <span href="https://www.ntdtv.com/gb/北戴河会议.htm">
   北戴河会议
  </span>
  可能即将开始。
 </p>
 <p>
  中共中央每年夏季在北戴河避暑办公，夏季的重要会议几乎都在这里召开。这个制度在2003年胡锦涛出任总书记后曾经废除。
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  上任总书记后，再次召开北戴河会议。
 </p>
 <p>
  今年的北戴河会议前夕，
  <span href="https://www.ntdtv.com/gb/中共高层.htm">
   中共高层
  </span>
  连续召开会议，显示出比往年更强的危机意识。
 </p>
 <p>
  一个是7月5号召开的“深化党和国家机构改革总结会议”，
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  在讲话中连提六个“变”字。他说：“形势在变、任务在变、工作要求也在变”，因此，必须“准确识变、科学应变、主动求变”。
 </p>
 <p>
  旅美时事评论员蓝述：“中共的内政，现在这个香港民众的愤怒它平息不了，外交上中美贸易这就是突出的例子。所以说中共现在是内外交困，变来变去，它所有的办法都不灵了。”
 </p>
 <p>
  旅美时事评论员郑浩昌：“这六个‘变’也反映出，现在官场的懒政之风很严重。现在贸易战已经大军压境，不谋变会死得很快，所以当局就逼着官员变。但是官员一变的话，搞不好就挨中纪委的铡刀，所以他又不敢乱动。这就有点像，中共把自己两头锁死了，动弹不得。”
 </p>
 <p>
  旅美时事评论员郑浩昌认为，这个机构改革总结会议的真正目的不是应变，而是习近平在北戴河会议前，给自己改革的政绩打个定心桩。
 </p>
 <p>
  郑浩昌：“现在党内斗争很激烈，习近平在贸易战和香港的问题上进退两难，给了政敌足够的攻击他的弹药。他这么强调改革的成功，其实就是为了在北戴河会议前巩固他自己那个方面的阵地。”
 </p>
 <p>
  另一个显示出党内人心松散，和中共高层危机感的会议，是7月9号在北京召开的“中央和国家机关党建工作会议”。
 </p>
 <p>
  习近平在会上要求官员提高政治的所谓“站位”，他要求官员在维护当局的权威时，既要从感性、理性上高度认同，又要有维护的定力和能力。他严肃批评官员拿反腐败当成不作为的借口，要求中共官员不做“昏官、懒官、庸官、贪官”。
 </p>
 <p>
  《大纪元时报》引用时政评论员石实的观点说，中共官场是缺什么说什么。因此这说明中共高层内部有不少人与习近平不一心，没有站在习近平一边，显示中共高层内部权斗激烈。
 </p>
 <p>
  尽管高层要求官员凝聚起来，但旅美时事评论员蓝述认为，就目前情况来看，这次北戴河会议，可能会加剧中共的内斗。
 </p>
 <p>
  蓝述：“内外交困的情况之下，它的这种矛盾是方方面面，从上到下的。既然这些矛盾没有办法解决，那么它下一步只会增加它内部各个派系之间的斗争，它不可能越来越走向凝聚。”
 </p>
 <p>
  郑浩昌也认为，今年的北戴河会议，习近平已经预见到了挑战。
 </p>
 <p>
  郑浩昌：“现在经济下滑非常严重，几乎所有利益集团都受损，他的对头巴不得在北戴河会议上借机发难，把他撵下台。你看他连中央全会都压着不开了，他是知道对手可能会藉中央全会发难。”
 </p>
 <p>
  蓝述认为，目前唯一能解决中国面临的这些危机的方法，就是解体中共。
 </p>
 <p>
  蓝述：“因为中共的这个体制，是所有这一切矛盾的根源。你只要是为了维护中共的统治而继续去变的话，变来变去，到最后都解决不了中共这个体制本身所面临的问题。”
 </p>
 <p>
  去年北戴河会议前夕，中共高层的内斗也很剧烈，当时党内有派系放风给香港媒体，宣称在世的大多数退休中共老常委，要求举行中共中央政治局扩大会议，来解决中央主要领导所犯的错误问题等。
 </p>
 <p>
  采访/常春 编辑/尚燕 后制/李沛灵
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102620401.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102620401.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102620401.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/11/a102620401.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

