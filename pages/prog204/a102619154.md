### 粤前统战部长曾志权被判无期 梅州“客家帮”覆灭
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/22-8-800x450.jpg" target="_blank">
  <figure>
   <img alt="粤前统战部长曾志权被判无期 梅州“客家帮”覆灭" src="https://i.ntdtv.com/assets/uploads/2019/07/22-8-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  曾志权被外界称为广东官场“客家帮最后的大佬”，他的落马也被视为是“客家帮”覆灭的最后阶段。（网络图片）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月10日讯】落马近一年后，中共广东省委原常委、统战部原部长
  <span href="https://www.ntdtv.com/gb/曾志权.htm">
   曾志权
  </span>
  ，在福建福州中院以受贿罪被判处无期徒刑。曾志权被指是在广东官场势力强大的梅州“
  <span href="https://www.ntdtv.com/gb/客家帮.htm">
   客家帮
  </span>
  ”成员兼最后大佬。不过“客家帮”自中共十八大后土崩瓦解，已落马的前省委副秘书长刘小华、前广州市委书记
  <span href="https://www.ntdtv.com/gb/万庆良.htm">
   万庆良
  </span>
  等人，都被认为是“客家帮”的典型成员。
 </p>
 <p>
  据中共福建福州中院在官方微博上的通报，2004年至2017年，
  <span href="https://www.ntdtv.com/gb/曾志权.htm">
   曾志权
  </span>
  利用担任广东省财政厅副厅长、厅长职务上的便利，为相关单位和个人在取得开发用地、承建工程等事项上提供帮助，直接或通过其近亲属非法收受财物共计1.408余亿元人民币。
 </p>
 <p>
  曾志权于2018年7月11日落马，是中共十九大后广东落马的“首虎”。此前陆媒报导，曾志权当天上午原本要参加省委常委会议。在会议召开前，他被中纪委工作人员带走。
 </p>
 <p>
  港媒曾报导，2018年6月，广东鸿艺集团有限公司董事长蔡鸿文被调查，该集团是“梅州客天下旅游产业园”的开发建设者，据悉被调查的原因是违规操作项目。报导说，蔡鸿文被调查与客天下项目土地或存在千丝万缕的关联。而曾志权、蔡鸿文是五华客家老乡，曾志权落马可能与蔡鸿文有关。
 </p>
 <p>
  广东官场派系严重，梅州“
  <span href="https://www.ntdtv.com/gb/客家帮.htm">
   客家帮
  </span>
  ”在广东官场势力极大。曾志权就被指是梅州“客家帮”成员之一。但中共十八大后，包括前省委副秘书长刘小华、前政府副秘书长罗欧、前副省长
  <span href="https://www.ntdtv.com/gb/刘志庚.htm">
   刘志庚
  </span>
  、前省人大常委会副秘书长李珠江、前广州市委书记
  <span href="https://www.ntdtv.com/gb/万庆良.htm">
   万庆良
  </span>
  、前珠海市委书记李嘉等典型的“客家帮”成员，纷纷落马。
 </p>
 <p>
  其中，万庆良、
  <span href="https://www.ntdtv.com/gb/刘志庚.htm">
   刘志庚
  </span>
  、罗欧被判处无期徒刑，李珠江、李嘉则分别判刑14年及13年。
 </p>
 <p>
  曾志权被外界称为“客家帮最后的大佬”，他的落马也被视为是“客家帮”覆灭的最后阶段。
 </p>
 <p>
  公开资料显示，曾志权生于1963年，是广东梅州五华人，长年在广东省财政系统工作，历任广东省财政厅企业财务处主任科员、省财政厅农业处处长、省财政厅副厅长、厅长等职位。
 </p>
 <p>
  曾志权在2017年5月跻身广东省委常委，2018年1月当选第十三届全国政协委员，同年4月13日正式出任统战部部长。但不足3个月，他便因为涉嫌严重违纪违法，落马受查，今年1月被双开（开除党籍和公职）。
 </p>
 <p>
  （记者陈远辉报导/责任编辑：戴明）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619154.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619154.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619154.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/10/a102619154.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

