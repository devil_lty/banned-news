### 【禁闻】《八佰》遭撤映 大陆电影制作人披露内情
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月03日讯】一部讲述“八百壮士”坚守上海四行仓库的抗日
  <span href="https://www.ntdtv.com/gb/电影《八佰》.htm">
   电影《八佰》
  </span>
  ，被官方以所谓的“技术原因”，取消了它在“
  <span href="https://www.ntdtv.com/gb/上海国际电影节.htm">
   上海国际电影节
  </span>
  ”开幕式上的首映, 并叫停了7月5号的院线上映。外界质疑，电影可能是因为正面呈现国民党士兵的形象及多次出现青天白日旗，才遭撤映。
 </p>
 <p>
  有大陆电影制作人披露，《八佰》被撤映，与台湾最近的“
  <span href="https://www.ntdtv.com/gb/反红媒.htm">
   反红媒
  </span>
  ”有关。
 </p>
 <p>
  影片《八佰》是一部抗战电影，讲述的是1937年淞沪会战最后一战，国民党军队中校团副谢晋元奉命率国军坚守上海四行仓库，浴血奋战4天4夜，成功掩护大部队撤退的故事。
 </p>
 <p>
  影片原定6月15号在“
  <span href="https://www.ntdtv.com/gb/上海国际电影节.htm">
   上海国际电影节
  </span>
  ”开幕上首映，但《八佰》的官方微博账号14号晚上发消息说，因“技术问题”取消影片的放映。同时原定7月5号在中国院线的上映，也被叫停。
 </p>
 <p>
  《八佰》原来被定为中共建政70周年的“献礼片”，将于“十一”期间播放。
 </p>
 <p>
  台湾中央社报导说，6月9号，中共红色文化研究会在北京举办“电影创作倾向问题学术”研讨会，与会人士认为，《八佰》用历史碎片对国民党的抗战做了所谓的“美化”，严重违背了中共认可的所谓“历史事实”；同时，电影中还有护旗、升旗的画面，渲染了国民党“青天白日旗”的庄严和神圣。因此，认为将《八佰》作为中共建政70周年的“献礼片”，很不合适。
 </p>
 <p>
  加拿大时政评论人士盛雪：“现在这个影片，是进一步地揭示出当时的真相。当然共产党它是不希望看到。因为国民革命军是在前边跟日本浴血奋战，是折损了相当多的人和相当多的英才。共产党是在利用这样的一个历史阶段，让它自己的力量发展壮大，而同时又窃取了中国国家的权力。”
 </p>
 <p>
  大陆电影制作人石宇歌对《八佰》这部正视史实的电影，在中共现行的体制下竟能拍摄完成，表示惊讶。他分析说，拍摄抗日战争片，故事内容必须符合中共两大原则中的任何一项。但是《八佰》很显然不具备这些元素。
 </p>
 <p>
  大陆电影制作人石宇歌：“第一大原则就是，这些国军抗战的事件当中，中国共产党要在其中起到关键的作用。比方说，像‘八百壮士’这个背景，如果你能够说谢晋元是共产党人，那没问题。现在问题（谢晋元）不是（共产党员）。”
 </p>
 <p>
  石宇歌表示，中共对抗日片的宣传中的这些硬性的规定，电影界很难逾越。
 </p>
 <p>
  石宇歌：“第二原则，就是故事的最终结局，这些人必须要走向他们所说的光明，就是投到共产党这边来。那么现在很显然‘八百壮士’这部片子，它不可能会走到这两个任何一个的结局去。”
 </p>
 <p>
  对于《八佰》被撤映，石宇歌表示，根据他国内传媒界一些朋友传来的讯息，他研判可能与台湾最近的“
  <span href="https://www.ntdtv.com/gb/反红媒.htm">
   反红媒
  </span>
  ”有关系。
 </p>
 <p>
  石宇歌：“这一次台湾‘反红媒’行动，让国内这些搞大外宣的非常头疼。他们觉得，可能岛内的这种局势对他们非常不利。所以我感觉，它这一次对一部电影这种安排，最根本的原因应该还是跟国际形式上的一些变化有关系。”
 </p>
 <p>
  加拿大时政评论人士盛雪认为，《八佰》被撤映是因为中共怕大陆民众藉由这部影片，看到当时国军浴血奋战，誓死护国卫旗的英勇事迹，进而影响了它政权的合法性。
 </p>
 <p>
  盛雪：“这个电影如果播放的话，会让许许多多的人去反思这个历史。同时，也要去追问这个问题：就是共产党是如何壮大的？共产党是如何夺取这个政权的？那么这样的一个党派，今天有没有资格统治14亿的人民？”
 </p>
 <p>
  盛雪认为，中共对于所有的历史事件，它的唯一重点就是，要让历史服务于它的政治需求；而它的政治需求的核心，就是它的最高的利益。虽然它以强权掩盖真相，但在互联网发达的时代，信息传递快速的时代，中共很难再继续以谎言治国。
 </p>
 <p>
  采访/陈汉 编辑/李鸣 后制/
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102614182.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102614182.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102614182.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/02/a102614182.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

