### 【禁闻】女律师险命丧监牢 大法救助感恩终生
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月06日讯】2002年4月30号深夜，北京西城区看守所女号内，突然被扔进一个奄奄一息的“女犯人”，她遍体鳞伤，骨骼多处变形，趴在地上无法动弹。令人意外的是，她的身份竟是一名律师。在她苦苦挣扎请求就医被拒绝后，在距离死神祇有一步之遥时，一群同样身陷囹圄的法轮功学员用尽全力将她救了回来，令她感激一生。
 </p>
 <p>
  毕业于中国政法大学的倪玉兰，在北京从事律师一职已经有十多年了。2002年4月27号, 因为帮助委托人取证，她在北京西城区的一个强拆现场拍照，被警察发现后拖进新街口派出所，在那里她遭到8名警察长达15个小时的连续毒打和开水烫、玻璃割、头撞墙等酷刑折磨。之后, 被以“扰乱办公秩序”为名关进西城区看守所。
 </p>
 <p>
  大陆律师倪玉兰：“他们由八个警察把我抬到拘留所。0当时我真是伤的非常重，已经没有人形了被打的，失去记忆，遍体鳞伤，身体多处骨折，骨髓神经受到严重损伤。”
 </p>
 <p>
  从昏迷中醒来的倪玉兰发现，自己几乎丧失了自理能力，但看守所却拒绝将她送医治疗。绝望之时，看守所中的一个特殊群体向她伸出了援手。
 </p>
 <p>
  倪玉兰：“关押在我这个屋子里头的大多数全都是法轮功。因为她们有医生啊，各个行业都有，她们从各个方面帮助我，-无论做什么，比如上卫生间、吃东西、帮我洗衣服、喂饭都是她们。”
 </p>
 <p>
  倪玉兰表示，她几次生命垂危，幸亏法轮功学员不分昼夜、悉心照料，才使她得以度过危险期，身体慢慢恢复。
 </p>
 <p>
  倪玉兰：“因为那时候我已经很难吃东西，因为我的脏器受到了很大的损伤，全身几乎没有一个地方是好的，所以真的要是想活下来，是非常难的。那时候她们为了让我能够活下来，她们买吃的，一点一点的喂我吃。她们用各种办法让我保护自己，能够活下来。”
 </p>
 <p>
  最令倪玉兰受到触动的是，看守所中的法轮功学员本身也承受着巨大的迫害, 她曾多次目睹警察残酷毒打她们，同时还唆使犯人行凶。尽管如此，这些法轮功学员依然秉持善念，关心和帮助着他人。
 </p>
 <p>
  倪玉兰：“真、善、忍这几个字说的是非常准确的，她们真的是都做到了。她们非常善良，无论怎么样对她们，她们都会去用自己的善良去感化他们，感化那些对她们施压的人，这真的是非常难得的。”
 </p>
 <p>
  75天后，倪玉兰带着落下残疾的左腿被释放。两个月后，拄著双拐维权的倪玉兰，在上访时再次遭新街口派出所警察毒打，被摔断尾骨, 并被以“妨害公务罪”判刑一年, 再次关进西城区看守所。
 </p>
 <p>
  这一次, 身受重伤的倪玉兰，只能躺在看守所冰冷潮湿的地板上，连坐起都需要人搀扶。她不知道, 自己怎样才能熬过这漫长的一年。然而，法轮功学员们再一次向她伸出了温暖的双手。一年365天，法轮功学员们轮流守侯在倪玉兰的身边，无微不至的照顾着她，鼓励她坚持下去。一次次的救命之恩，令她终生难忘。
 </p>
 <p>
  倪玉兰：“幸亏当时遇见了好人，遇见了那么多善良的法轮功(学员)，才有我的今天。这十多年过去了，但是我仍然非常感谢她们。由于她们的师父教育出这么好的这些人，她们帮助了我，给了我一个新的生命，当时如果没有她们的话，我已经难活下来。所以我也非常感谢师父。”
 </p>
 <p>
  倪玉兰说，多年来，她一直在寻找当年救助她的法轮功学员，可是她们至今下落不明，她只能默默祝福：愿她们不再受邪恶势力的镇压，能够保护好自己，不再受到任何伤害。
 </p>
 <p>
  采访/易如 编辑/张天宇 后制/周天
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616270.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616270.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102616270.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/05/a102616270.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

