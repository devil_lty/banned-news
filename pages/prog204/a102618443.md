### 林郑月娥承认修例失败：草案已寿终正寝
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/2-50.jpg" target="_blank">
  <figure>
   <img alt="林郑月娥承认修例失败：草案已寿终正寝" src="https://i.ntdtv.com/assets/uploads/2019/07/2-50-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  香港特首林郑月娥7月9日在香港特首办召开记者会，首次承认《逃犯条例》修订完全失败。（李逸/大纪元）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月09日讯】香港特首
  <span href="https://www.ntdtv.com/gb/林郑月娥.htm">
   林郑月娥
  </span>
  今天早上（7月9日）在香港特首办召开记者会，她一改早前傲慢的态度，首次承认《逃犯条例》修订完全失败，并强调这项草案已“寿终正寝”。这是香港6.9反送中大游行之后，林郑首度恢复在行政会议前会见记者。
 </p>
 <p>
  周二早上9点半，
  <span href="https://www.ntdtv.com/gb/林郑月娥.htm">
   林郑月娥
  </span>
  在出席行政会议前会见记者。她说，这次修例是完全失败的。“我们今次的修订工作是完全失败，我早前已为此致歉。失败的原因是因为我们的工作做得不好，我们对社会的脉络掌握得不够，我们的政治敏感度很有偏差，这种种都构成了今次修例工作的失败。”
 </p>
 <p>
  她表示，修订工作已彻底全面停止下来。“我今日再清晰讲明，修订工作，这条条例草案已经寿终正寝，草案已死亡。”她又以英文重复说一次“the bill is dead”。
 </p>
 <p>
  林郑月娥此次会见记者明显收起了早前傲慢的态度，她表示明白港人的愤怒失望，以及对政府的不信任。她还表明愿意与学生代表进行公开、没有前设的对话，并会尽快安排。
 </p>
 <p>
  不过，对于反送中民众提出的5项诉求：撤回逃犯条例修订；收回暴动定义；撤销被捕人士控罪；追究警队滥权；立即实行双普选。林郑月娥仍持推诿态度，她重申没有对6.12反送中集会定性。
 </p>
 <p>
  6月12日，数万港人包围立法会阻止逃犯条例审议。港府派出数千防暴警察强力镇压示威民众，引发流血事件，多人被打伤。
 </p>
 <p>
  据网上传出的视频显示，当晚，有泛民议员找警察，希望不要使用武力。但警员回答说：“已经定性为暴动，执行公务任何手段都可以使用，包括对你们！”舆论为之哗然。
 </p>
 <blockquote class="twitter-tweet" data-lang="zh-cn">
  <p dir="ltr" lang="zh">
   21:15分最新消息，泛民议员找警察，希望不要使用武力。警察的回答：“已经定性为暴动，执行公务任何手段都可以使用，包括对你们！”
   <span href="https://t.co/bZpLYBgE2u">
    pic.twitter.com/bZpLYBgE2u
   </span>
  </p>
  <p>
   — 铲?人 (@Tonyworld15)
   <span href="https://twitter.com/Tonyworld15/status/1138797680665120769?ref_src=twsrc%5Etfw">
    2019年6月12日
   </span>
  </p>
 </blockquote>
 <p>
  <script async="" charset="utf-8" src="https://platform.twitter.com/widgets.js">
  </script>
 </p>
 <p>
  6.16再有2百万港人上街游行，并向港府提出5项诉求，但港府一直未予以回应。7月1日，55万港人再次走上街头，另有一些香港青年冲击立法会表达诉求，引发国际关注。
 </p>
 <p>
  林郑在2日凌晨4点，召开记者会将冲击立法会事件定性为“暴力事件”，声称要“追究到底”。香港警方随即展开大搜捕，至今已有超过50名示威者被捕，最小的年仅14岁。且搜捕行动仍在继续。
 </p>
 <p>
  香港各界纷纷谴责港府漠视民意的“制度暴力”，促使香港青年采取激进方式表达诉求，凸显香港青年对港府的失望和伤心。在反送中抗议期间，已有4名香港青年因对港府绝望，而跳楼自杀。
 </p>
 <p>
  然而，林郑此次会见记者仍否认对抗议活动的暴动定性，还推诿说，检控工作由律政司负责，不受干预，独立检控工作是视乎证据而定；又指不追究示威者破坏立法会，违反香港法治精神。
 </p>
 <p>
  虽然林郑一再表示会改革政府的施政作风，聆听更多方面和更广泛意见，但从她的讲话中，明显流露出港府仍在为继续暴力镇压民众寻找借口。
 </p>
 <p>
  而且，林郑此次讲话仍未对港人的5项诉求给出回应，只不过是在当前香港局势日趋紧张的情况下，试图缓和一下。
 </p>
 <p>
  一如从前，林郑再度被问到会否下台的问题。她回答说，自己仍有热诚为香港服务，希望大家能给予她空间。
 </p>
 <p>
  （记者罗婷婷报导/责任编辑：文慧）
 </p>
 <p>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/12/a102599472.html">
    港议员现场吁警勿施暴 港警:已定性暴动包括你们（视频）
   </span>
  </strong>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102618443.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102618443.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102618443.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/09/a102618443.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

