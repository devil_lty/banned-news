### 【精彩推荐】四类官难倒中南海 习下令“抓书记”
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/f5cca42d1e2193bb0c31f75e68fff4ad.jpg" target="_blank">
  <figure>
   <img alt="【精彩推荐】四类官难倒中南海 习下令“抓书记”" src="https://i.ntdtv.com/assets/uploads/2019/07/f5cca42d1e2193bb0c31f75e68fff4ad.jpg"/>
  </figure><br/><br/>
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月10日讯】新唐人为读者推荐每日精彩文章和视频节目：
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619297.html" rel="noopener" target="_blank">
   四类官难倒中南海 习近平下令“抓书记”
  </span>
  <br/>
  在9日召开的中共党建会议上，习近平要求各级官员绝不能做昏官、懒官、庸官、贪官，同时要求各部门强化抓党建，坚持“书记抓、抓书记”，也就是书记要带头抓党建工作。显示中共官场正经历重大危机，上述四类官难倒中南海。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619317.html" rel="noopener" target="_blank">
   香港送中“寿终正寝” 中共禁声为哪般？
  </span>
  <br/>
  香港特首林郑9日宣布，《
  <span href="https://www.ntdtv.com/gb/逃犯条例.htm">
   逃犯条例
  </span>
  》的修订工作完全失败，草案已“
  <span href="https://www.ntdtv.com/gb/寿终正寝.htm">
   寿终正寝
  </span>
  ”。外界认为，港府与中共导演的这场送中闹剧，在港民强烈的反对声中被迫以“送终”收场，而中共却对内封锁消息。究其原因，是惧怕大陆民众效仿上街游行，加速中共垮台。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619182.html" rel="noopener" target="_blank">
   习近平低调“点将”内幕 一批将领正被查
  </span>
  <br/>
  近期，习近平低调晋升一批少壮派将领，引起关注。10日有港媒消息披露，最近还有一批将领正被查。由于北戴河会议即将登场，有评论称，中共军队此时大调整，说明军心不急，各派权斗剧烈，北戴河山雨欲来。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619236.html" rel="noopener" target="_blank">
   辽宁村官之子被判死刑 驾车泄愤撞死6学童
  </span>
  <br/>
  辽宁葫芦岛市建昌县去年11月有韩姓村官之子，开私家车逆线行车撞倒过路学生，造成6人死亡20人受伤。7月9日，韩男被判死刑。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619223.html" rel="noopener" target="_blank">
   风水师：三峡大坝切断中华龙脉
  </span>
  <br/>
  近几天来，
  <span href="https://www.ntdtv.com/gb/三峡大坝.htm">
   三峡大坝
  </span>
  变形的传闻，引起海内外舆论关注。长江三峡被视为世界上最大的一条龙脉，早年即有风水师说，中共兴建三峡大坝，切断了中华民族的龙脉，破坏了中国风水，导致三峡流域灾祸连连。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619244.html" rel="noopener" target="_blank">
   “花木兰她爸是习近平”？ 迪士尼真人版吵翻天
  </span>
  <br/>
  近日，由中国演员刘亦菲主演的迪士尼真人版“花木兰”预告片，引发大陆网友关注，片中饰演花木兰父亲的演员，因为长相极似中国国家主席习近平，不少网友调侃“原来木兰她爸是习近平”，而片中花木兰额头出现华为logo也引起网友调侃。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619184.html" rel="noopener" target="_blank">
   大陆器官移植游 “活死人”黑幕曝光
  </span>
  <br/>
  “他们活着，就变成了死人。这对中国的情况来说是一个独特的术语。”多伦多圣迈克尔医院肾移植负责人Jeff Zaltzman医生说。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619154.html" rel="noopener" target="_blank">
   粤前统战部长曾志权被判无期 梅州“客家帮”覆灭
  </span>
  <br/>
  曾志权被指是在广东官场势力强大的梅州“客家帮”成员兼最后大佬。不过“客家帮”自中共十八大后土崩瓦解，已落马的前省委副秘书长刘小华、前广州市委书记万庆良等人，都被认为是“客家帮”的典型成员。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619132.html" rel="noopener" target="_blank">
   贸易战打惨中国富豪 财富缩水5300亿美元
  </span>
  <br/>
  中国大陆最富有的人群正笼罩在
  <span href="https://www.ntdtv.com/gb/贸易战.htm">
   贸易战
  </span>
  阴云之中。美国《石英》（Quartz）杂志7月9日刊文称，受2018年美中贸易争端的影响，中国大陆富人财富缩水近5,300亿美元，近6.7万人跌出中国最富裕阶层的行列。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619097.html" rel="noopener" target="_blank">
   大陆官员出国 惊叹三件事颠覆认知
  </span>
  <br/>
  在国内时，我是大陆某县级市的机关工作人员，算是一名体制内人士，长期接受洗脑教育，曾经对中共各级党委、政府的权威性一直没有发生过质疑；当我来到海外，经过自己的观察和了解，才发现有三件事情完全颠覆了我的认知。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/10/a102619083.html" rel="noopener" target="_blank">
   江泽民姘头出访密闻 被送上法庭不敢声张
  </span>
  <br/>
  近年来，许多中共高官因迫害中国民众被陆续告上国际法庭。相对那些收到法庭传票后迅速消失、遁回国内的中共官员，江泽民最高级别情妇陈至立，在外访国的法庭上没能找到回避的借口，成为来不及隐遁而被迫上法庭的第一人。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/09/a102618602.html" rel="noopener" target="_blank">
   香港反送中“遍地开花”港媒：林郑三度错失拆弹时机
  </span>
  <br/>
  一个月前的今天（6月9日），因港府强推《
  <span href="https://www.ntdtv.com/gb/逃犯条例.htm">
   逃犯条例
  </span>
  》（送中条例），香港掀起反送中抗议浪潮。港媒称，期间，特首
  <span href="https://www.ntdtv.com/gb/林郑月娥.htm">
   林郑月娥
  </span>
  三度错失拆弹时机，令事态无回旋空间，难以收拾。如今，反送中抗议势头未减，新的行动正在筹划中，反送中将在香港“遍地开花”。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/09/a102619025.html" rel="noopener" target="_blank">
   哈尔滨马拉松还没开跑 补给站已被大妈们抢空
  </span>
  <br/>
  中国东北的哈尔滨市近日举行了一场城市乐跑马拉松赛事。开赛当天，尚未正式鸣枪开跑，一群衣着鲜艳的大妈突然涌入补给站，不由分说便开始哄抢饮用水和其他补给品，让工作人员傻了眼，再次引发网络舆论对“中国大妈”不文明现象的诸多感慨。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/09/a102618922.html" rel="noopener" target="_blank">
   硅谷谍案审判天津教授 与中兴窃美技术给共军
  </span>
  <br/>
  美国硅谷所审理的一桩经济间谍案揭露，中国（中共）利用主要技术公司及大学窃取美国的知识产权和技术，并把偷来的成果服务于中国的军需产业。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/09/a102618828.html" rel="noopener" target="_blank">
   肖建华副手获释 律师:肖能否活着出来难说
  </span>
  <br/>
  港媒爆料称，肖建华的秘书温英杰配合北京当局调查后已获释，对他的有关指控均已撤销。不过，有维权律师分析称，肖建华本人不大可能得到同样的宽大处理，他知道中共高层内幕太多，能否活着出来还难说，更不可能轻松脱罪。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/09/a102618775.html" rel="noopener" target="_blank">
   贸战重创投资 中共一带一路野心或付诸东流
  </span>
  <br/>
  “在分析
  <span href="https://www.ntdtv.com/gb/贸易战.htm">
   贸易战
  </span>
  可能对投资者造成的潜在影响后，我们正考虑拒绝为一些‘一带一路’项目提供资金。” 一位驻北京的资深银行家日前对路透社这样说。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/09/a102618653.html" rel="noopener" target="_blank">
   前大陆公安：中共镇压法轮功早有预谋
  </span>
  <br/>
  1999年7月20日凌晨4、5点钟，时任派出所副所长的刘晓斌正在值班，两个市局一处（当时属于政保处）的便衣上门通知他，“从凌晨开始全国统一行动，对法轮功的辅导员和站长进行统一抓捕”。
 </p>
 <p>
  <span href="https://cn.ntdtv.com/gb/2019/07/10/a102619255.html" rel="noopener" target="_blank">
   美售台108辆“地表最强战车” 三大看点引关注
  </span>
  <br/>
  美国五角大楼7月9日宣布，美国国务院已批准2项对台军售，包括108辆“M1A2T艾布兰”（Abrams） 坦克、250枚刺针飞弹（Stinger Missiles）和相关设备， 总额超过22亿美元。 美国此次对台军售的M1A2T战车被称为“地表最强战车”，它有三大看点将使台湾陆军战力全面升级。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/09/a102619045.html" rel="noopener" target="_blank">
   【中国禁闻】7月9日完整版
  </span>
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/09/a102618809.html" rel="noopener" target="_blank">
   【热点互动】中国假丐帮澳洲被诉 “坏人变老”影响他国？
  </span>
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/2019/07/08/a102618165.html" rel="noopener" target="_blank">
   【今日点击】李小龙哲学对香港抗议运动有何启发？
  </span>
 </p>
 <p>
  <strong>
   <span href=" https://www.ntdtv.com/gb/2019/02/15/a102512426.html" rel="noopener" target="_blank">
    《九评》编辑部：魔鬼在统治着我们的世界（播报版全集）
   </span>
  </strong>
 </p>
 <p>
  <strong>
   <span href=" https://www.ntdtv.com/gb/2018/06/08/a1378888.html" rel="noopener" target="_blank">
    《九评》编辑部：魔鬼在统治着我们的世界（文字版全集）
   </span>
  </strong>
 </p>
 <p>
  （责任编辑：王馨宇）
 </p>
 <p>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/09/a102618542.html" rel="noopener" target="_blank">
    北戴河会议怕变天？/林郑月娥承认失败
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/08/a102617804.html" rel="noopener" target="_blank">
    大五毛染香被带走/三峡大坝变形？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/06/a102616726.html" rel="noopener" target="_blank">
    军委间谍案内幕 /胡锦涛前世故事
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/05/a102616095.html" rel="noopener" target="_blank">
    习要严查假情报？/高层爆重大分歧
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/04/a102615395.html" rel="noopener" target="_blank">
    习下令7.1不准流血 /刘鹤险成罪人
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/03/a102614715.html" rel="noopener" target="_blank">
    韩正七一密赴深圳 /党内对手伺机倒习
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/07/02/a102613872.html" rel="noopener" target="_blank">
    公安部机密曝光 /王岐山尴尬自嘲
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/30/a102612439.html" rel="noopener" target="_blank">
    王沪宁再遭重挫？/习前后都有“坑”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/29/a102611757.html" rel="noopener" target="_blank">
    川习会敲定重启谈判 /党媒扔“手榴弹”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/28/a102611034.html" rel="noopener" target="_blank">
    李源潮早就死了?/习近平车队翻车
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/27/a102610400.html" rel="noopener" target="_blank">
    习访日再出状况 /川普提前“出牌”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/26/a102609552.html" rel="noopener" target="_blank">
    李克强微信群被封？/网传李源潮被抓
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/25/a102608819.html" rel="noopener" target="_blank">
    习收王牌报告 /刘鹤与王沪宁唱反调？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/24/a102607896.html" rel="noopener" target="_blank">
    湖南埋尸案真相 /“川习会”结果难料
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/23/a102607320.html" rel="noopener" target="_blank">
    习刚走金正恩收到川普“有趣来信”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/22/a102606824.html" rel="noopener" target="_blank">
    习拉金正恩壮胆？川普立即行动
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/21/a102605914.html" rel="noopener" target="_blank">
    广东紧跟香港示威 /习近平又挨一刀？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/20/a102605053.html" rel="noopener" target="_blank">
    上山下乡有隐情？/习2年打烂一手好牌
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/19/a102604362.html" rel="noopener" target="_blank">
    常委暗器毁习近平 /林郑还能走多远？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/18/a102603592.html" rel="noopener" target="_blank">
    高层对习渐失信任 /林郑月娥两度痛哭
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/17/a102602754.html" rel="noopener" target="_blank">
    习近平遭“埋刀设伏”/中南海慌了？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/16/a102602202.html" rel="noopener" target="_blank">
    中共顶级专家意外身亡/习出访王沪宁后院点火
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/15/a102601542.html" rel="noopener" target="_blank">
    香港发布重大消息 逃犯条例“猝死”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/14/a102600674.html" rel="noopener" target="_blank">
    中共军警悄悄抵港？ /习遇麻烦专机延飞
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/13/a102599980.html" rel="noopener" target="_blank">
    官二代揭高层内幕/港教协宣布全面罢课
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/12/a102599165.html" rel="noopener" target="_blank">
    王沪宁再夺笔杆子/习近平父亲显灵？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/11/a102598235.html" rel="noopener" target="_blank">
    曾庆红对习下手？/王立军口供流出
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/10/a102597452.html" rel="noopener" target="_blank">
    胡锦涛谈64内幕 /习近平掉“温水锅”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/09/a102596880.html" rel="noopener" target="_blank">
    传中共党内吵翻天/任正非“遗言”成真？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/08/a102596314.html" rel="noopener" target="_blank">
    习近平险被拉下台 /白羽鸟亡党预言
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/07/a102595527.html" rel="noopener" target="_blank">
    习近平被吁下台？/任正非留“遗言”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/06/a102594640.html" rel="noopener" target="_blank">
    习近平背要命黑锅 /胡锦涛曾遭软禁
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/05/a102593892.html" rel="noopener" target="_blank">
    习近平遭另类政变？/川普打出致命牌
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/04/a102593090.html" rel="noopener" target="_blank">
    64张64照片(慎入) /特稿警示习近平
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/03/a102592288.html" rel="noopener" target="_blank">
    薄瓜瓜大祸临头？/一类人让习更危险
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/02/a102591836.html" rel="noopener" target="_blank">
    习近平摇摆不定 王沪宁率先夺权？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/01/a102591340.html" rel="noopener" target="_blank">
    王岐山车队遇袭/ 江泽民赃款不保
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/31/a102590562.html" rel="noopener" target="_blank">
    习近平情报从哪来？内部要求“出硬招”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/30/a102589817.html" rel="noopener" target="_blank">
    美中女主播论战变味 /曾庆红干儿受审
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/19/a102581946.html" rel="noopener" target="_blank">
    习近平遭逼宫？ /华为4万员工拒回国
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/27/a102587334.html" rel="noopener" target="_blank">
    中共沉船计划曝光 /副国级被处死内幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/27/a102587274.html" rel="noopener" target="_blank">
    中南海重大误判 有人出招帮习解套
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/26/a102586812.html" rel="noopener" target="_blank">
    李克强紧急喊5字 /王岐山访欧遇麻烦
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/25/a102586347.html" rel="noopener" target="_blank">
    王岐山突访欧盟 /习大秘：有人想夺权
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/24/a102585608.html" rel="noopener" target="_blank">
    韩正设局拿下习？王沪宁卖力煸情
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/23/a102584835.html" rel="noopener" target="_blank">
    陆股511亿外资出逃 /胡春华急任新职
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/22/a102583989.html" rel="noopener" target="_blank">
    习近平“7”的定数 /胡锦涛带走老江
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/21/a102583249.html" rel="noopener" target="_blank">
    崔永元再发信号 /习近平考察“烂牌”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/20/a102582412.html" rel="noopener" target="_blank">
    习近平震怒无效？/刘士余案涉债市一姐
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/19/a102581940.html" rel="noopener" target="_blank">
    拉毛泽东打贸易战？/令计划吓到女记者
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/18/a102581514.html" rel="noopener" target="_blank">
    “反习派”镇不住？/习近平遭暗讽
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/17/a102580969.html" rel="noopener" target="_blank">
    习近平三张王牌被毙 /网现灭党魔咒
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/16/a102580367.html" rel="noopener" target="_blank">
    贸易战风云突变 / 川普宣布紧急状态
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/15/a102579579.html" rel="noopener" target="_blank">
    政治局突然开会/中共沉船计划曝光
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/14/a102578756.html" rel="noopener" target="_blank">
    李克强大喊稳定/习近平紧急训话
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/13/a102577805.html" rel="noopener" target="_blank">
    崔永元写藏头诗？/红二代警告北京
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/11/a102576421.html" rel="noopener" target="_blank">
    习近平在走钢丝？/崔永元：地狱走一回
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/10/a102575417.html" rel="noopener" target="_blank">
    美2千亿关税开打/崔永元发表声明
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/09/a102574561.html" rel="noopener" target="_blank">
    习近平真要负全责？川普不提习主席
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/08/a102573697.html" rel="noopener" target="_blank">
    刘鹤访美丢头衔/北京反腐首开杀戒
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://cn.ntdtv.com/gb/2019/05/07/a102572709.html" rel="noopener" target="_blank">
    习近平愿负全责？/中南海紧急定对策
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/06/a102571936.html" rel="noopener" target="_blank">
    中美谈判风云突变 中国股市千股跌停
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/04/a102570937.html" rel="noopener" target="_blank">
    千张六四照独家面世/毛纪念堂风水黑幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/03/a102570178.html" rel="noopener" target="_blank">
    武警政委去向不明/山东观音像变孔子
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/02/a102569477.html" rel="noopener" target="_blank">
    央视主播挖鼻曝光/中国手机变电子手铐
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/01/a102568706.html" rel="noopener" target="_blank">
    习讲话前罕见一幕 “跟党走”惹人烦
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/30/a102567935.html" rel="noopener" target="_blank">
    北京纪念五四 真相不敢外传
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/29/a102567028.html" rel="noopener" target="_blank">
    女谍撂倒四川副省长？/金正恩访俄惹祸
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/27/a102565895.html" rel="noopener" target="_blank">
    金正恩匆匆回国内幕/江泽民向美求饶
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/26/a102565032.html" rel="noopener" target="_blank">
    四川副省长被带走？/中共南海弃岛内幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/25/a102564230.html" rel="noopener" target="_blank">
    习近平健康异常？/江泽民被恶梦吓醒
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/24/a102563421.html" rel="noopener" target="_blank">
    习阅兵遭遇凶兆/外交部性贿赂曝光
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/23/a102562718.html" rel="noopener" target="_blank">
    习阅兵现尴尬：美国上校笑/央视出意外
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/22/a102561799.html" rel="noopener" target="_blank">
    李锐日记吓坏中共？/解密中南海爆炸性丑闻
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/20/a102560884.html" rel="noopener" target="_blank">
    崔永元留下悄悄话/曾庆红设局杀主席
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/19/a102560070.html" rel="noopener" target="_blank">
    国航林英案与神秘特工/朱军憔悴现身
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/18/a102559177.html" rel="noopener" target="_blank">
    赵本山影视城被烧/毕福剑悄悄复出？
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/17/a102558369.html" rel="noopener" target="_blank">
    圣母院火灾预言/习两接班人罕见同台
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/16/a102557586.html" rel="noopener" target="_blank">
    圣母院起火原因/阿桑奇曝中共黑幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/15/a102556902.html" rel="noopener" target="_blank">
    天津首富涉百条命案/刘欢突发心脏病
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/13/a102555776.html" rel="noopener" target="_blank">
    王岐山养兵千日/中国四大魔王有代号
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/12/a102554998.html" rel="noopener" target="_blank">
    山东一处绝命风水 毁了三个“接班人”
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/06/a102550628.html" rel="noopener" target="_blank">
    勒紧腰带跟党走？/江泽民包围香山内幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/02/a102547319.html" rel="noopener" target="_blank">
    第二中央在搅局？/习近平要查身边人
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/04/01/a102546062.html" rel="noopener" target="_blank">
    响水爆炸王泸宁施招？/胡锦涛夫妇遇奇景
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/29/a102544134.html" rel="noopener" target="_blank">
    习近平腿脚出状况？/北京疑陷更大危机
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/28/a102543384.html" rel="noopener" target="_blank">
    大红人骗习近平 /王岐山爆不死内幕
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/27/a102542467.html" rel="noopener" target="_blank">
    元老“逼宫”内幕 习近平躲过一劫
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/25/a102540801.html" rel="noopener" target="_blank">
    胡锦涛帮习压阵？/王岐山神秘回巢
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/23/a102539771.html" rel="noopener" target="_blank">
    习访欧后院爆炸 应急部长诡异消失
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/22/a102538967.html" rel="noopener" target="_blank">
    江苏爆炸大起底 /微信让全球用户裸奔
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/03/21/a102538211.html" rel="noopener" target="_blank">
    2019拍案惊奇:两起事件让习近平震惊
   </span>
  </strong>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619301.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619301.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619301.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/10/a102619301.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

