### 贸易战打惨中国富豪 财富缩水5300亿美元
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/gettyimages-180372582-594x594.jpg" target="_blank">
  <figure>
   <img alt="贸易战打惨中国富豪 财富缩水5300亿美元" src="https://i.ntdtv.com/assets/uploads/2019/07/gettyimages-180372582-594x594-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  受2018年美中贸易争端的影响，中国大陆富人财富缩水近5,300亿美元。（STR/AFP/Getty Images）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月10日讯】中国大陆最富有的人群正笼罩在贸易战阴云之中。美国《石英》（Quartz）杂志7月9日刊文称，受2018年美中贸易争端的影响，中国大陆富人
  <span href="https://www.ntdtv.com/gb/财富缩水.htm">
   财富缩水
  </span>
  近5,300亿美元，近6.7万人跌出中国最富裕阶层的行列。
 </p>
 <p>
  法国咨询公司凯捷(Capgemini SE,CGEMY)发表的2019年世界财富报告也显示，2018年度，亚太地区的富人
  <span href="https://www.ntdtv.com/gb/财富缩水.htm">
   财富缩水
  </span>
  了1万亿美元——从21.6万亿美元降至20.6万亿美元。
 </p>
 <p>
  其中大陆富豪的财富缩水就占了一半，约为5,300亿美元，金融资产超过100万美元的人数减少了5%，至120万人。
 </p>
 <p>
  一般用高净值人群（HNWI）代表资产净值在100万美元（600万人民币）以上的富豪。
 </p>
 <p>
  《石英》报导说，中国股票市场跌落，是导致亚太地区财富缩水的原因之一。去年贸易战开打后，中国股市跌跌不休。根据上证、深证综合指数，去年中国股市下跌25%，仅第四季度就下跌了10%。
 </p>
 <p>
  凯捷表示，这种下跌反映了中国经济增长放缓、房地产市场停滞、中美贸易关系紧张以及人民币贬值等因素的影响。
 </p>
 <p>
  凯捷参与准备该公司年度《全球财富报告》(World Wealth Report)的副总裁Bill Sullivan称，中国经济降温导致
  <span href="https://www.ntdtv.com/gb/中国富豪.htm">
   中国+富豪
  </span>
  人数减少，也对全球富人的资产总价值构成拖累。
 </p>
 <p>
  2018年贸易战开打以来，中国经济增速放缓至25年来的最低水平。2017年至2018年间全球富豪数减少了约10万个，其中中国大陆富豪减少的数量最多，近6.7万人跌出中国最富裕阶层的行列。
 </p>
 <p>
  近期的其他报告也同样显示，中国最富有的人群正笼罩在贸易战阴云之中。
 </p>
 <p>
  波士顿咨询公司(Boston Consulting Group Inc.,BCG.XX)在上月公布的调查中表示，去年亚洲家庭资产增速从上年的11.5%下滑至7.1%左右，主要原因是中国富人对股市的敞口不断扩大。
 </p>
 <p>
  PNC金融服务集团高级国际经济学家威廉．亚当斯（William Adams）告诉《石英》，中国富人人数下降是中国股市在2018年期间下跌带来的一个连锁效应。
 </p>
 <p>
  同时，他表示人民币贬值也在较小程度上发挥了作用，人民币贬值主要是“由于中国与美国之间的贸易冲突加深所致”。
 </p>
 <p>
  “由于美中关系的不确定性和人民币的（贬值）压力，中国市场市值损失超过2.5万亿美元。”亚当斯说。
 </p>
 <p>
  他表示，“人民币贬值抵消了美国关税对中国出口商的影响”，美国政府一直在指责中共让人民币贬值。
 </p>
 <p>
  （记者罗婷婷报导/责任编辑：戴明）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619132.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619132.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog204/a102619132.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/10/a102619132.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

