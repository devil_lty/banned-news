### 【独立评论】“送终”指挥部痛悔 为什么不上城管？
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="香港议员要求一些穿制服的“警察”出示警员证，这些人拒不回应，而由身边的便衣警察代为推搪。（视频截图）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/be5905b8daaf1add69274714932d0c83-800x450-800x533-600x400.jpg"/>
 </div>
 <div class="caption">
  香港议员要求一些穿制服的“警察”出示警员证，这些人拒不回应，而由身边的便衣警察代为推搪。（视频截图）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月18日】
  </span>
  <span class="content-info-type">
   （作者：真羽）
  </span>
  在中国大陆经历过房屋拆迁的人大多听到过这样一个名词——动拆迁指挥部。就是政府看中你家所在的那块地了，会和你谈判给你一笔钱做为“补偿”，然后劝你搬到偏远的地方，这叫“动迁”。当然，给你的钱一般远远不够你买回被拆的家所在地重新盖起的房子。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  而很多根本不想搬家的、或给的“补偿”离心里预期太远的被拆对象，就根本没有谈判成功的机会，那就等着家被拆了、钱也没了、甚至人也没了……这种历年来制造无数流血事件、被各地政府看成最有效拉动GDP树立政绩的、不给钱先拆房的行为叫做“强迁”。为了“强迁”的顺利进行，在动迁的一开始会成立一个协调当地政府、公安、房屋开发商三方的领导小组——动拆迁指挥部。
 </p>
 <p>
  我曾直面过一个“动拆迁指挥部”的负责人，是个女的四十来岁。她说的什么我已经不记得，但她留给我的印象和信息太深刻了，那就是她是来指挥这场战役的，她是来打仗的。这位女士给我上了一课：共产党一直没有也从未想要过和平，它从出现到篡政到今天一直是战时状态，而它的真正敌人就是人民。
 </p>
 <p>
  中共与人民为敌的手段主要是靠谎言加暴力，杀人放火不说了，今天谈一种不易被察觉的、很阴毒的手段：长年来以“党文化”破坏中华传统文化，败坏人的思想和行为，使今天的中国大陆人在全世界丢尽了脸。让我们看一组场景
 </p>
 <p>
  2015年一中国大陆女性抱着孩子在英国奢侈品商店巴宝莉（Burberry）服装店门口大便
 </p>
 <p style="text-align: center;">
  <img alt="" class="alignnone size-full wp-image-2968141" src="http://img.soundofhope.org/2019/06/-1166.png" srcset="http://img.soundofhope.org/2019/06/-1166.png 511w, http://img.soundofhope.org/2019/06/-1166-180x132.png 180w, http://img.soundofhope.org/2019/06/-1166-366x268.png 366w"/>
 </p>
 <p>
  2016年中国大陆游客在俄罗斯皇宫大厅小便震惊俄罗斯人
 </p>
 <p>
  2018年中共大使馆力挺大陆游客曾先生一家在瑞典旅店碰瓷大闹只为赖一晚住宿费
 </p>
 <p>
  2018年中共大使馆再次力挺央视女记者孔琳琳在英国保守党年会大闹扇人耳光
 </p>
 <p>
  2019年一中国女游客再次突破人类承受极限在泰国地铁里小便
 </p>
 <p style="text-align: center;">
  <img alt="" class="alignnone size-medium wp-image-2968144" src="http://img.soundofhope.org/2019/06/-569x600-569x600.jpg" srcset="http://img.soundofhope.org/2019/06/-569x600.jpg 569w, http://img.soundofhope.org/2019/06/-569x600-180x190.jpg 180w, http://img.soundofhope.org/2019/06/-569x600-366x386.jpg 366w"/>
 </p>
 <div>
 </div>
 <p>
  ……
 </p>
 <p>
  我有时怀疑，除了大多数是自幼被中共邪党文化毒害造成的变异，会不会真有些就是专干这行的——败坏中国人在全世界的形象？答案是肯定的。不管他们本人是否意识到，就是这样被安排的，如上面提到的、被中共大使馆力挺的就是。
 </p>
 <p>
  中共这样做的目的是什么？就是要让全世界的政府和人民都厌恶和唾弃中国人，那样他们将不再有兴趣关注中国人的基本人权和自由；那样中国人在全世界人的眼中将不再是人而是奴隶和牲口；那样即使中国人的器官成为一种商品也不会再让世人难以承受良知的冲击。
 </p>
 <p>
  最后就说到香港现在这个话题大热门了。在百万港人“反送中”的整个事件中，作为“送中”事件的制造者策划方有没有一个“送终指挥部”呢？一定是有的。不管它的具体名称叫什么，成员是谁，一定有这样一个由邪党领导的“送终指挥部”。
 </p>
 <p>
  指挥部派出中国大陆公安武警冒充香港警察来镇压香港人民本是一箭双雕的毒计。一是以港人从未亲身体验过的残忍手段制造流血事件甚至命案，以步步升级的恐怖效应来摧毁香港人民的自由精神。二是以这些冒牌警察的恐怖行为嫁祸于香港警察，败坏香港警察、香港法制的形象，以期制造更大的社会冲突，最终达到败坏全体香港人民的形象，这样的香港才是中共真正需要的，才是它能够控制的香港。至于香港今后的金融中心地位是否受损，那根本不是中共有意愿去考虑的。
 </p>
 <p>
  但问题来了，冒牌货被当场抓包了！其实那些长年被中共谎言洗脑毒害的武警战狼们也很苦的，他们也都是按照“送终指挥部”的命令去努力伤害香港市民。除了站姿有点斜、肩膀有点歪、口音有点重、面目有点恶、表情有点狠、出手有点凶、好奇心有点强、市面见得有点少、打完人还集体合影留念外……其它的和香港本地警察也没什么太大的不同。
 </p>
 <p>
  可现在的“送终指挥部”头目们一定是在深深的痛悔自责中：为什么当初没想到派上内地的城管部队呢？！至少配备一些特别挑选的“城管尖兵”也好啊。因为城管和那些冒牌警察相比恐吓能力、暴力程度都不落下风，更有他们不具备的一项奇异功能——瞬时夺物功。就是在近距离只要城管一出手，就能把别人手上身上的财物等东西瞬间转移到自己手上。如果当初配备一批城管尖兵，关键时刻施展瞬时夺物功，那些对着冒牌货拍照的摄像机、手机随便什么机，可全都收入囊中了。那样嫁祸可能会成功，民愤可能会变成恐惧，一箭双雕可能真的会成功……
 </p>
 <p>
  现在完了，“送终指挥部”再痛悔再狂想也没用了。假冒警察被抓现行，香港人的眼睛更亮了。200万黑衣港人在一片黑的香港大地上，用人性的光芒照亮了香港的天空，照清了幕后的黑手中共和它的嘴脸，也告诉了世人真正的暴徒在哪里。
 </p>
 <p>
 </p>
 <p>
  （仅代表作者个人立场和观点）
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2968114.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2968114.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2968114.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/18/n2968114.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

