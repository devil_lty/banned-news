### 【独立评论】七年前的臭弹（上）：从自行车到王立军
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="七年前王立军的那颗致命弹被锁进保险柜，成了一颗打不响的臭弹。（视频截图）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/05/hqdefault-800x533-1-600x400.jpg"/>
 </div>
 <div class="caption">
  七年前王立军的那颗致命弹被锁进保险柜，成了一颗打不响的臭弹。（视频截图）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年5月13日】
  </span>
  <span class="content-info-type">
   （作者：真羽）
  </span>
  我生在中国大陆，幼时常见到这一幕：“哐才、哐才”的锣鼓声中，几个人脖子上吊着牌子，被一群意气风发的红卫兵小将和居委干部们摁低了头游街。一顿激昂的批斗过后，再次锣鼓声大噪，然后齐刷刷的爆发出各种山呼万岁！仔细听，翻来覆去的是“某主席万岁”！“哐、才、当万岁”！
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  年幼无知的我还以为那“哐、才、当”是喧天锣鼓的人声合唱版。长大了才知道，“哐、才、当”就是“共产党”，但儿时脑海中的破锣声总会不时响起。
 </p>
 <p>
  时光穿梭到了7年前，我迷茫的人生也到了十字路口。那一年好莱坞大片《2012》的末日没有上演，玛雅人预言的新世纪也没有出现，但那一年出的2件事，却生生改变了我。也紧紧牵动着今天的一切。
 </p>
 <p>
  一件事是2月6日，王立军“叛逃”美领馆震惊了全球！而国内依旧祥和一派、岁月静好。另一件事同在2月，日本驴友河源启一郎武汉丢自行车事件。
 </p>
 <p>
  先说自行车事件，很多网友也可能听说过。大概意思就是一位日本人在武汉被偷了一辆自行车，然后武汉警方以雷霆之势发动全城警力、号召全体爱国百姓和志愿者，迅速破案并找回了那辆自行车。
 </p>
 <p>
  接着媒体喉舌一拥而上，狂炒此举是多么为国争光云云。
 </p>
 <p>
  我当时脑子有点错乱，一连串从小到大充斥脑海的、有关日本的关键词句排比而来：“地雷战”“一衣带水”“倭寇”“中日友好世世代代” “手撕鬼子”“钓鱼岛”……
 </p>
 <p>
  虽说见惯了一切的翻云覆雨都是党的政治需要，可如此兴师动众给一个日本非著名人士如此超国民的待遇，仅仅为了得个日式表扬？“公安局长叛逃”这样天大的事怎的没人提呀？
 </p>
 <p>
  正堵得慌，当事人河源君及时出场了！ 他接受各大媒体采访时很激动，在按要求一番情真意切的感慨之后，还意犹未尽继续发挥：“实在是太阿里嘎多，太谢谢了……我真切地希望这个中国政府，也能够‘对自己的老百姓更好一点’，也能够‘像对我那样’……”
 </p>
 <p style="text-align: center;">
  <img alt="" class="alignnone wp-image-2881898" src="http://img.soundofhope.org/2019/05/u24229350458821691fm20.jpg" srcset="http://img.soundofhope.org/2019/05/u24229350458821691fm20.jpg 330w, http://img.soundofhope.org/2019/05/u24229350458821691fm20-180x100.jpg 180w">
   <br/>
   自行车被偷的当事人河源君接受采访。（视频截图）
  </img>
 </p>
 <div>
 </div>
 <p>
  也许在场直播的广大“新闻工作者”没太注意，也许当时场面太令人激动了，这段严重挑起愚民仇恨、忘恩负义的煽颠言论，竟当场直播了出来！尽管很快删除了，可在外溜达了一圈，还被我看见了！
 </p>
 <p>
  我震惊之余迅速上网查了查这位日本驴友的老底，资料显示他是个经常自费赈灾的慈善家，我信了。对这位真正的慈善家，我放下了历史恩怨，内心对他深深一鞠躬，可我自己的心更堵了。
 </p>
 <p>
  平静下来之后开始脊梁冒汗，一个答案渐渐清晰了。一直以来，谁是党和政府真正的敌人？苏修？美帝？日寇？都不是！
 </p>
 <p>
  是我们，是百姓，是人民！人民才是所有独裁极权的真正敌人！河源君的自行车撞醒了我，至少让我无法再装睡。
 </p>
 <p>
  那么对敌人，首先是要欺骗！我明白了奇葩的自行车为什么此时会出现，是为了让人的视线离开“王立军”。我学会了翻墙，找到了“王立军事件”的真相。
 </p>
 <h4>
  <strong>
   王立军的臭弹
  </strong>
 </h4>
 <p>
  2012年王立军为了躲避薄熙来的追杀逃进美领馆，带了一大包保命的绝密文件，那是一枚当时足以摧毁中共政权的绝命弹，但一切都没发生。次年，中共新核心访美，奥巴马送给习核心的礼物是老大一把红杉木长椅——向全世界宣告，赐座，都看见了没，我给的。王立军的那颗致命弹被锁进保险柜，成了一颗打不响的臭弹。
 </p>
 <p>
 </p>
 <p>
  （未完待续）
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2881652.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2881652.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2881652.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/05/13/n2881652.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

