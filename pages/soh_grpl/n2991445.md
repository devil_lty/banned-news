### 让人权恶棍在惩治下心有顾忌
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="在台湾中正纪念堂展出“六四”三十周年特展。（RFA)）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/-800x533-35-600x400.jpg"/>
 </div>
 <div class="caption">
  在台湾中正纪念堂展出“六四”三十周年特展。（RFA)）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月27日】
  </span>
  <span class="content-info-type">
   （自由亚洲 作者刘青）
  </span>
  刚刚过去的“六四”三十周年，与往年相比有两个不一般之处：第一是对“六四”三十周年的悼念活动，比一般年份的悼念更为广泛众多，这与中共三十年来引颈期盼的，即寄望时光洗去罪恶血迹的窃意恰恰相反。第二是美国官方所表达的立场和态度，甚至比“六四大屠杀”发生时的立场和态度，其谴责的力度更为鲜明和强硬。不仅国会举行听证、议长直言“六四”就是“大屠杀”，美国驻华大使馆降半旗，甚至国务卿彭佩傲就“六四”发表声明，这是当年“六四大屠杀”时也未曾有过的。而最令人关注的是美国举措中还隐含了实锤内容，即六月三日美国驻华大使馆的官方网站发布《全球马格尼茨基人权问责法》。这是美国政府于“六四”三十周年之际明确无疑的表示，今后对于中共官员严重的人权迫害案件，《全球马格尼茨基人权问责法》将是大杀器。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  马格尼茨基是因为检举俄国官员贪腐，被关入监狱历尽肉体折磨而惨死的。美国为此通过立法而惩治有关罪行的俄国官员，具体措施主要是不给有罪官员美国签证，还有冻结这些有罪官员存放于美国的财产。马格尼茨基问责法在世界上取得良好成效，美国又通过立法将这一针对俄国的问责法，扩展至适用于全球任何国度的人权恶棍，以及严重贪腐为恶的官吏。目前这一惩治全球人权恶棍的法律，已经有爱沙尼亚、英国、加拿大等多国跟进立法，还有欧洲议会、澳大利亚和法国、瑞典、乌克兰等国正在审议立法或相关版本，另有一些国度如丹麦、台湾、泽西岛等对立法正在讨论中。这状况说明对人权恶棍和犯下严重罪行的官吏，全球采取惩治措施的趋势已经开始并将成为潮流。
 </p>
 <p>
  自从赫尔辛基人权协议问世以来，人类虽然对于专制独裁肆意迫害他人树立了一面普世谴责的道义大旗，但是对于迫害者的遏制和震慑，可以说并没有实质的有力的压制作用。在这方面中共可以说是典型的反面教员。民主世界以往面对中共式的无耻无赖，可以说令人憋气的束手无策一愁莫展。中共一方面以人权是各国的内政为由，强烈拒绝任何对中共恶劣人权的谴责压力。另一方面又以在政府间开展人权对话之活动，迷惑消除民主世界对人权无所作为的焦虑，而同时又成功施展所谓人权对话就是各说各话的手段，让民主世界对专制政权的人权迫害憋屈到说不成话。中共在恣意迫害十三亿中国大陆民众人权的同时，得意而又有效的充当全世界反人权的恶棍之首。
 </p>
 <p>
  但是《全球马格尼茨基人权问责法》的出现，改观了世界人权无力无奈的软弱现实，让全世界的人权恶棍在迫害人权之时，不得不考虑面临此法的惩治而心有顾虑。《全球马格尼茨基人权追责法》的惩治程度，虽然与人权恶棍所犯罪行及犯罪所获相比，远远没有表现出相应性和具有够大的震慑力，但是对犯罪就是为了权钱与享受这些的人权恶棍，无疑也具有令其心有顾忌和为将来考虑的警示意义。正如美国要签证入境者提供近五年的社交网站帐号，就令大骂美国吹捧中共的五毛成为网上濒危物种，《全球马格尼茨基人权问责法》只要通过各种渠道，让人权恶棍逐渐了解到“出来混总要还的”现实，一定能产生他们行恶时不再毫无顾忌的意义。
 </p>
 <p>
  （文章只代表特约评论员个人的立场和观点）
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    岳文骁
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2991445.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2991445.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_grpl/n2991445.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/27/n2991445.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

