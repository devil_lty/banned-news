### 亲共建制派纷纷表态促撤回「反送中恶法」民主派指北京与港府图挽回民意
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="尹兆坚议员指亲共的建制派为了配合当局挽回民意的做法不得人心。（摄影：郑铭）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/2019-06-2500.18.07-600x400.jpg"/>
 </div>
 <div class="caption">
  尹兆坚议员指亲共的建制派为了配合当局挽回民意的做法不得人心。（摄影：郑铭）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月24日】
  </span>
  <span class="content-info-type">
   （本台记者鄭銘采访报导）
  </span>
  香港一次百万人和一次二百万人上街游行反对《逃犯条例》修订（反送中恶法），不仅震动国际社会，更让中港政坛不断产生连锁反应。港府至今只肯公开表示暂缓修例，不肯按反对的市民的要求撤回修例。二百万人反送中大游行后， 接连有亲北京的建制派政党表态要求港府顺应民意宣布撤回条例。民主派议员形容建制派是配合北京和港府挽回大幅度流失的民意，同时企图借机抹黑民主派。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  邻近G20峰会，接连有建制派出来要求特首林郑月娥宣布「撤回」恶法的呼声，包括这次「送中恶法」中姿态最强硬的亲共政党民建联的主席李慧琼，突然一改态度，星期日在一个节目中批评港府暂缓修例做法「不实际和失去焦点」，直言若政府决定撤回修例和反对者对话，有助修复社会撕裂，相信民建联的支持者会理解。
 </p>
 <p>
  代表商界的自由党党魁钟国斌星期一表示，特首林郑月娥「暂缓」逃犯条例修订，实质上与撤回修例没有分别。但是为平民愤以及避免给部份人借口冲击政府，当局应该宣布撤回修例。「我认为政府在这方面不要再执著，如果现实上效果与撤回无分别，就直接告诉大家撤回。或者特首个人不方便说这句说话，可由政务司司长张建宗代为去处理亦可，我认为这不是很大的面子问题。」
 </p>
 <p>
  对于是否调查警方在「反送中」示威中是否滥用武力，钟国斌则跟足北京的口吻，指要调查事件中是否外国势力的介入。
 </p>
 <p>
  民主派会议召集人毛孟静回应建制派的要求时表示， 林郑因为之前威望问题不肯表明撤回修例，但建制派的做法明显是感受到民意的支持在大幅度失去， 她认为林郑月娥更应表明在任内不再提出修例。
 </p>
 <p>
  至于财委会方面，毛孟静又直言建制派已经在民间大打民生牌试图挽回民意，并趁机抹黑民主派阻碍民生议题拨款，民主派议员不会与亲共的财委会主席陈健波见面和商量是否优先审议民生项目的拨款，因为财委会的审议项目排列一向由港府决定。
 </p>
 <p>
  民主党尹兆坚形容建制派认同「撤回」修例的言论是「捉错用神」，因为市民除了要求撤回恶法，还包括「彻查、撤控、撤销暴动定义」，建制派此举仅是想舒缓民怨，给港府下台阶：「我想告诉政府这个楼梯不是只有一级，而是四级。只是说撤回是解决不到的。指撤回一项民愤不会平息。」
 </p>
 <p>
  他不满整个事件过程中建制派的嘴脸是假惺惺，先是错判民情，一直宣称有大量民意支持修例，让港府陪他们疯。
 </p>
 <p>
  尹兆坚又批评建制派制造流会及取消会议，均涉及民生事务，阻碍民生的责任恰恰在于政府及建制派议员。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    李璐
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2982652.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2982652.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2982652.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/24/n2982652.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

