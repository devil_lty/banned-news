### 高雄「罢免韩国瑜」门槛高 台民间团体：向「不可能的任务」迈进
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="台湾民间团体「Wecare高雄」和「公民割草行动」宣布结盟，一起加入罢免高雄市长韩国瑜的行列。（新唐人影片截图）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.08.41pm-600x364.png"/>
 </div>
 <div class="caption">
  台湾民间团体「Wecare高雄」和「公民割草行动」宣布结盟，一起加入罢免高雄市长韩国瑜的行列。（新唐人影片截图）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月29日】
  </span>
  <span class="content-info-type">
   （本台记者斐珍综合报导）
  </span>
  台湾民间团体「Wecare高雄」和「公民割草行动」昨天(28日)宣布结盟，一起加入罢免高雄市长韩国瑜的行列，要号召30万名高雄市民罢免韩国瑜，朝这条「不可能的任务」迈进。「馆长」陈之汉响应并呼吁高雄人一起来罢免不适任的市长韩国瑜。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  高雄市长韩国瑜自从去年(2018)就职后，种种言论及作为都引起台湾各界抨击、热议，目前甚至还想要带职参选角逐2020年台湾总统大选，此举让许多高雄市民感到失望与无法接受。「Wecare高雄」和「公民割草行动」两大民间发起的倒韩团体，昨(28)日宣布结盟，两方将结合各自的资源，并将邀请馆长等社会知名入士，希望能号召30万高雄市民一起来罢免韩国瑜。
 </p>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_2994898">
  <img alt="台湾民间团体结盟罢免高雄市长韩国瑜。（新唐人影片截图）" class="wp-image-2994898 size-medium" src="http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.00pm-600x365.png" srcset="http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.00pm-600x365.png 600w, http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.00pm-180x110.png 180w, http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.00pm-366x223.png 366w, http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.00pm.png 664w">
   <br/><figcaption class="wp-caption-text">
    台湾民间团体结盟罢免高雄市长韩国瑜。（新唐人影片截图）
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  对此，「馆长」陈之汉28日晚间在脸书回应，「高雄朋友加油，也许这是历史上第一个被罢免的市长」。网友们也纷纷留言，「我印200份 全家大小同事都加」、「高雄加油！我已经准备列印在街上发了！」、「现在高雄问候语不是你发财了吗？而是你罢免了吗？」、「拜托高雄人不要再让这草包毒害高雄了」。
 </p>
 <p>
  关于市民发动罢免活动，韩国瑜受访时表示，尊重在民主机制下民众表达的意见及网路的声音。
  <br/>
  国民党团总召曾俊杰认为，韩国瑜应该留任市长，否则好不容易回归国民党执政的高雄市可能又将失去。他说，如果输了高雄，当上总统也没用；若高雄市长换人，国民党等于回到原点，这是他担忧的、也是基层支持者担心的。因为人在人情在，已有不少投资者在观望，所以别赌那么大。
 </p>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_2994901">
  <img alt="台湾民间团体结盟罢免高雄市长韩国瑜。（新唐人影片截图）" class="wp-image-2994901 size-medium" src="http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.13pm-600x363.png" srcset="http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.13pm-600x363.png 600w, http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.13pm-180x109.png 180w, http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.13pm-366x221.png 366w, http://img.soundofhope.org/2019/06/screenshot2019-06-29at2.09.13pm.png 658w">
   <br/><figcaption class="wp-caption-text">
    台湾民间团体结盟罢免高雄市长韩国瑜。（新唐人影片截图）
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  依据台湾现行选罢法的规定，罢免分成3阶段。第一阶段为罢免提议，提议人数要达到高雄选举人数的1%。以去年大选高雄市的总选举人数228万1338人来看，要达到第一阶段「提议」的门槛至少就要有达到约2.28万份的有效提议书，加上误差值，至少需要3万份才能进入第二阶段的连署。
 </p>
 <p>
  第二阶段的连署，得在60天内蒐集到22.8万份的有效连署书，才能进入第三阶段。第三阶段的「投票」阶段中，有效同意票数除了要多于不同意票数外，更要达到57万票以上，罢免案才能通过。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    鄭欣
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2994886.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2994886.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2994886.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/29/n2994886.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

