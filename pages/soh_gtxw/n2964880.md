### 黄之锋获释后即刻归队 与港民并肩“反送中”
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="黄之锋现身立法会为反修例打气。（香港众志脸书）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/fireshotcapture031-facebook-www.facebook.com-600x400.png"/>
 </div>
 <div class="caption">
  黄之锋现身立法会为反修例打气。（香港众志脸书）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月17日】
  </span>
  <span class="content-info-type">
   （本台记者高健雯综合报导）
  </span>
  民主派政党“香港众志”秘书长黄之锋于今天（17日）上午10时30分获释，之后随即加入“反送中”行列。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  综合媒体报导，黄之锋今天上午10时30分出狱，在会见本地及国外媒体时表示，他被囚禁4个多星期，错过了12日冲击行动及两次逾百万人大游行，无法与港人并肩作战让他深感遗憾。但他表示，逾百万人的游行还会有，要继续要求特首林郑月娥全面撤回《逃犯条例》修订，撤回对6.12冲击的“暴动”定义，及停止拘捕示威者，“如果检控的话，会令香港有更大反扑”。
 </p>
 <p>
  他说，当他在监狱里从电视看到港人走上街头，出现在夏悫道，对他来说是一件振奋人心的事，他没有想到“雨伞运动”后，夏悫道还会重新被民众占领，让他感动。他说感谢香港人，经历了“雨伞运动”的低潮后，在“反送中”行动中又一次让全世界看到，“香港面对强权不会低头”，亦不会被灭声。也感谢支持香港，为港人打气，与港人共同努力面对北京当局压迫的台湾人。
 </p>
 <p>
  之前特首林郑月娥在专访时流泪，黄之锋对此表示不屑一顾，他说林郑“流的是眼泪，而金钟市民却在流血”， 他表示林郑已没有资格再做特首，要求她引咎辞职，问责下台。
 </p>
 <p>
  黄之锋强调，现在港人还没有完全获胜，他鼓励港人继续抗争，希望继2012年反国教的成功之后，这次能再次赢得更彻底。
 </p>
 <figure class="wp-caption aligncenter img-width-l" id="attachment_2965108">
  <img alt="" class="wp-image-2965108" src="http://img.soundofhope.org/2019/06/64530406-2317402338352261-2204357716602781696-n-600x600.jpg" srcset="http://img.soundofhope.org/2019/06/64530406-2317402338352261-2204357716602781696-n-600x600.jpg 600w, http://img.soundofhope.org/2019/06/64530406-2317402338352261-2204357716602781696-n-150x150.jpg 150w, http://img.soundofhope.org/2019/06/64530406-2317402338352261-2204357716602781696-n-180x180.jpg 180w, http://img.soundofhope.org/2019/06/64530406-2317402338352261-2204357716602781696-n-366x367.jpg 366w">
   <br/><figcaption class="wp-caption-text">
    <span style="color: #0000ff;">
     黄之锋出狱后向日前坠楼牺牲的壮士献花哀悼。（黄之锋脸书）
    </span>
   </figcaption><br/>
  </img>
 </figure><br/>
 <div id="fb-root">
 </div>
 <p>
 </p>
 <div class="fb-video" data-href="https://www.facebook.com/joshuawongchifung/videos/2032839483678863/?t=0" data-width="750">
  <blockquote cite="https://www.facebook.com/joshuawongchifung/videos/2032839483678863/" class="fb-xfbml-parse-ignore">
   <p>
    <span href="https://www.facebook.com/joshuawongchifung/videos/2032839483678863/">
    </span>
   </p>
   <p>
    之鋒服畢刑期，歸隊！
   </p>
   <p>
    Posted by
    <span href="https://www.facebook.com/joshuawongchifung/">
     黃之鋒 Joshua Wong
    </span>
    on Sunday, June 16, 2019
   </p>
  </blockquote>
 </div>
 <div>
 </div>
 <p>
  今早出狱后，黄之锋在会见本地及外国媒体时，要求特首林郑月娥全面撤回《逃犯条例》修订，撤回对6.12冲击的“暴动”定义及停止拘捕示威者，他说：“如果检控的话，会令香港有更大反扑”。
 </p>
 <p>
  黄之锋，“香港众志”秘书长。2014年“雨伞运动”期间，和社民连副主席黄浩铭等运动人士“占领旺角”阻挠警方清场，因违反禁制令，被控涉嫌藐视法庭罪，被判刑入狱3个月，黄之锋提出上诉，香港高等法院将刑期由原来的3个月减至2个月，当法官作出裁决后，旁听席中有民众激动落泪。
 </p>
 <p>
  黄之锋在狱中仍十分关注香港动态，并一直通过脸书传播真相，声援“反送中”。他还于12日投稿《时代杂志》，要求中共政府和香港政府对警民冲突负责，强调修例一旦通过，将代表“独裁政权的胜利”。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    云天
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2964880.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2964880.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2964880.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/17/n2964880.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

