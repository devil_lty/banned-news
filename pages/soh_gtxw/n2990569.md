### 港媒:  G20前夕 日本首先出声 安倍向习近平提送中条例争议
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="G20前夕，日相安培晋三向习近平提及香港修订条例引发的争议。（视频截图）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/abe-600x401.jpg"/>
 </div>
 <div class="caption">
  G20前夕，日相安培晋三向习近平提及香港修订条例引发的争议。（视频截图）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月27日】
  </span>
  <span class="content-info-type">
   （本台记者鄭欣综合报导）
  </span>
  港府强推《逃犯条例》修订（又称送中条例）引发港人接力式抗争受到国际关注。日本首相安倍晋三27日与出席G20峰会的中国国家主席习近平会晤期间，提及修例争议并强调，香港在“一国两制”下自由、繁荣发展非常重要。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  多家港媒引述日本传媒消息指，安倍向习近平提及近期在香港修订《逃犯条例》引发民众大规模示威抗议。安倍表示，“‘一国两制’下香港自由繁荣很重要”。
 </p>
 <p>
  此外，安倍亦提到新疆维吾尔族问题，表示尊重人权、法治等普世价值非常重要。
 </p>
 <p>
  G20峰会28、29日在日本大阪召开之际，港人发起多种抗议活动，呼吁国际关注反送中呼声，以施压港府撤回条例。
 </p>
 <p>
  当日（27日），有四十名左右的香港民众手持“Save Hong Kong”、“守护香港自由”等横幅在靠近G20会场附近集会，希望趁着今次G20峰会，让国际社会更加关注香港，并给予一臂之力。
 </p>
 <p>
  26日上午，约1,500名港人前往19个驻港总领事馆递信请愿，促请各国于峰会上就香港现况向中共施压。晚上，民阵举办6.26再集结，向G20喊话，要求国际声援港人抗争。
 </p>
 <p>
  另外，港人还在25日发起众筹，筹得款项用于在国际媒体刊登头版广告。据悉，筹款金额已超过目标。G20期间，国际多个主流媒体会刊登全版广告，要求港府撤回《逃犯条例》。
 </p>
 <p>
  日本首相安倍晋三是此次G20峰会首位提及香港修订《逃犯条例》的领袖。
 </p>
 <p>
  据白宫高级顾问康韦（Kellyanne Conway）透露，美国总统川普在G20峰会期间与习近平会面时，也会向对方提及香港修例争议。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    李璐
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2990569.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2990569.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2990569.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/27/n2990569.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

