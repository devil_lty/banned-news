### 【组图】风雨无阻 台湾拒绝红色媒体大集会  民众塞爆凯道
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="6月23日，台湾“拒绝红色媒体”大集会。（视频截图）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/zhiding-74-600x400.jpg"/>
 </div>
 <div class="caption">
  6月23日，台湾“拒绝红色媒体”大集会。（视频截图）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月23日】
  </span>
  <span class="content-info-type">
   （本台记者斐珍采访报导）
  </span>
  今天（23日）下午2点，台湾民众走上街头参加由台湾立委黄国昌与“馆长”陈之汉共同发起的“拒绝红色媒体 守护台湾民主”大集会。虽然遭遇大雨，但仍有上万名民众挤爆凯道，并高呼“拒绝红色媒体”“保卫台湾家园”等口号。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  <img alt="" class="aligncenter wp-image-2979895 size-medium" src="http://img.soundofhope.org/2019/06/623-7-600x450.jpg" srcset="http://img.soundofhope.org/2019/06/623-7-600x450.jpg 600w, http://img.soundofhope.org/2019/06/623-7-768x575.jpg 768w, http://img.soundofhope.org/2019/06/623-7-1024x767.jpg 1024w, http://img.soundofhope.org/2019/06/623-7-180x135.jpg 180w, http://img.soundofhope.org/2019/06/623-7-366x274.jpg 366w, http://img.soundofhope.org/2019/06/623-7.jpg 1280w"/>
 </p>
 <p style="text-align: center;">
  （摄影：菲珍）
 </p>
 <p>
  <img alt="" class="aligncenter wp-image-2979898 size-medium" src="http://img.soundofhope.org/2019/06/623-1-600x450.jpg" srcset="http://img.soundofhope.org/2019/06/623-1-600x450.jpg 600w, http://img.soundofhope.org/2019/06/623-1-768x575.jpg 768w, http://img.soundofhope.org/2019/06/623-1-1024x767.jpg 1024w, http://img.soundofhope.org/2019/06/623-1-180x135.jpg 180w, http://img.soundofhope.org/2019/06/623-1-366x274.jpg 366w, http://img.soundofhope.org/2019/06/623-1.jpg 1280w"/>
 </p>
 <p style="text-align: center;">
  （摄影：菲珍）
 </p>
 <p>
  <img alt="" class="aligncenter wp-image-2979901 size-medium" src="http://img.soundofhope.org/2019/06/623-3-600x450.jpg" srcset="http://img.soundofhope.org/2019/06/623-3-600x450.jpg 600w, http://img.soundofhope.org/2019/06/623-3-768x575.jpg 768w, http://img.soundofhope.org/2019/06/623-3-1024x767.jpg 1024w, http://img.soundofhope.org/2019/06/623-3-180x135.jpg 180w, http://img.soundofhope.org/2019/06/623-3-366x274.jpg 366w, http://img.soundofhope.org/2019/06/623-3.jpg 1280w"/>
 </p>
 <p style="text-align: center;">
  （摄影：菲珍）
 </p>
 <p>
  <img alt="" class="aligncenter wp-image-2979904 size-medium" src="http://img.soundofhope.org/2019/06/623-4-600x450.jpg" srcset="http://img.soundofhope.org/2019/06/623-4-600x450.jpg 600w, http://img.soundofhope.org/2019/06/623-4-768x575.jpg 768w, http://img.soundofhope.org/2019/06/623-4-1024x767.jpg 1024w, http://img.soundofhope.org/2019/06/623-4-180x135.jpg 180w, http://img.soundofhope.org/2019/06/623-4-366x274.jpg 366w, http://img.soundofhope.org/2019/06/623-4.jpg 1280w"/>
 </p>
 <p style="text-align: center;">
  （摄影：菲珍）
 </p>
 <p>
  <img alt="" class="aligncenter wp-image-2979907" src="http://img.soundofhope.org/2019/06/623-2-450x600.jpg" srcset="http://img.soundofhope.org/2019/06/623-2-450x600.jpg 450w, http://img.soundofhope.org/2019/06/623-2-180x240.jpg 180w"/>
 </p>
 <p style="text-align: center;">
  （摄影：菲珍）
  <br/>
  <img alt="" class="aligncenter wp-image-2979910" src="http://img.soundofhope.org/2019/06/623-6-450x600.jpg" srcset="http://img.soundofhope.org/2019/06/623-6-450x600.jpg 450w, http://img.soundofhope.org/2019/06/623-6-180x240.jpg 180w"/>
 </p>
 <p style="text-align: center;">
  （摄影：菲珍）
 </p>
 <p>
  <img alt="" class="size-medium wp-image-2979964" src="http://img.soundofhope.org/2019/06/photo-2019-06-23-17-15-11-600x450.jpg" srcset="http://img.soundofhope.org/2019/06/photo-2019-06-23-17-15-11-600x450.jpg 600w, http://img.soundofhope.org/2019/06/photo-2019-06-23-17-15-11-768x575.jpg 768w, http://img.soundofhope.org/2019/06/photo-2019-06-23-17-15-11-1024x767.jpg 1024w, http://img.soundofhope.org/2019/06/photo-2019-06-23-17-15-11-180x135.jpg 180w, http://img.soundofhope.org/2019/06/photo-2019-06-23-17-15-11-366x274.jpg 366w, http://img.soundofhope.org/2019/06/photo-2019-06-23-17-15-11.jpg 1280w"/>
 </p>
 <p style="text-align: center;">
  （摄影：菲珍）
 </p>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_2979967">
  <img alt="" class="size-medium wp-image-2979967" src="http://img.soundofhope.org/2019/06/photo-2019-06-23-18-05-05-600x450.jpg" srcset="http://img.soundofhope.org/2019/06/photo-2019-06-23-18-05-05-600x450.jpg 600w, http://img.soundofhope.org/2019/06/photo-2019-06-23-18-05-05-768x575.jpg 768w, http://img.soundofhope.org/2019/06/photo-2019-06-23-18-05-05-1024x767.jpg 1024w, http://img.soundofhope.org/2019/06/photo-2019-06-23-18-05-05-180x135.jpg 180w, http://img.soundofhope.org/2019/06/photo-2019-06-23-18-05-05-366x274.jpg 366w, http://img.soundofhope.org/2019/06/photo-2019-06-23-18-05-05.jpg 1280w"/>
  <br/><figcaption class="wp-caption-text">
   （摄影：菲珍）
  </figcaption><br/>
 </figure><br/>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    郑欣
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2979877.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2979877.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2979877.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/23/n2979877.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

