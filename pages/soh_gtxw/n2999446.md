### 助阵港人7.1为自由抗争 新唐人大纪元推出浩然正气歌
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="新唐人大纪元媒体联合出品的镇港之歌《自由路》。(网络截图)" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/2-367-600x397.png"/>
 </div>
 <div class="caption">
  新唐人大纪元媒体联合出品的镇港之歌《自由路》。(网络截图)
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月1日】
  </span>
  <span class="content-info-type">
   （本台记者高健雯综合报导）
  </span>
  香港民众今天（1日）再度上街举行七一大游行。民阵表示，港府一天不回应港人的五大诉求，香港人就会共同进退到底。新唐人大纪元赶在七一大游行这天特别推出歌曲《自由路》以示声援。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  继6月9日的103万人上街、12日的示威被武力镇压、16日的200万零一人上街、26日的G020峰会前向世界传递港人诉求、野餐、包围行政机关等不合作运动之后，民阵于26日再次发起七一游行，主题是“撤回恶法 林郑下台”， 要求港府重启政改、释放所有政治犯、撤销暴动定性、彻查6.12镇压。
 </p>
 <p>
  <img alt="" class="alignnone size-medium wp-image-2999500 aligncenter" src="http://img.soundofhope.org/2019/07/photo-2019-07-01-18-30-47-600x450.jpg" srcset="http://img.soundofhope.org/2019/07/photo-2019-07-01-18-30-47-600x450.jpg 600w, http://img.soundofhope.org/2019/07/photo-2019-07-01-18-30-47-768x576.jpg 768w, http://img.soundofhope.org/2019/07/photo-2019-07-01-18-30-47-1024x768.jpg 1024w, http://img.soundofhope.org/2019/07/photo-2019-07-01-18-30-47-180x135.jpg 180w, http://img.soundofhope.org/2019/07/photo-2019-07-01-18-30-47-366x275.jpg 366w, http://img.soundofhope.org/2019/07/photo-2019-07-01-18-30-47.jpg 1280w"/>
 </p>
 <p>
  <img alt="" class="alignnone size-medium wp-image-2999506 aligncenter" src="http://img.soundofhope.org/2019/07/photo-2019-07-01-16-40-55-600x401.jpg" srcset="http://img.soundofhope.org/2019/07/photo-2019-07-01-16-40-55-600x401.jpg 600w, http://img.soundofhope.org/2019/07/photo-2019-07-01-16-40-55-768x513.jpg 768w, http://img.soundofhope.org/2019/07/photo-2019-07-01-16-40-55.jpg 1024w, http://img.soundofhope.org/2019/07/photo-2019-07-01-16-40-55-180x120.jpg 180w, http://img.soundofhope.org/2019/07/photo-2019-07-01-16-40-55-366x244.jpg 366w"/>
 </p>
 <p style="text-align: center;">
  <img alt="" class="alignnone size-medium wp-image-2999509" src="http://img.soundofhope.org/2019/07/photo-2019-07-01-16-41-11-600x401.jpg" srcset="http://img.soundofhope.org/2019/07/photo-2019-07-01-16-41-11-600x401.jpg 600w, http://img.soundofhope.org/2019/07/photo-2019-07-01-16-41-11-768x513.jpg 768w, http://img.soundofhope.org/2019/07/photo-2019-07-01-16-41-11.jpg 1024w, http://img.soundofhope.org/2019/07/photo-2019-07-01-16-41-11-180x120.jpg 180w, http://img.soundofhope.org/2019/07/photo-2019-07-01-16-41-11-366x244.jpg 366w"/>
 </p>
 <p>
  游行于下午14:45 从维园起步，计划终点至金钟政府总部，但由于下午1时许，示威者在金钟立法会的抗议行动升级，警方建议民阵将游行延期、只办集会或改终点，因此民阵改终点至中环遮打道。
 </p>
 <p>
  为声援香港民众争取民主自由，新唐人电视台和大纪元时报特别于今日推出一首镇港之歌-《自由路》。这首粤语歌由伯克利音乐学院的高材生、纽约首饰设计师及动漫主题网店女主，三个来自美国的年轻创作人牵手新唐人大纪元共同制作。为了能在7月1日这天播放，他们几乎一周不眠不休赶制。
 </p>
 <div class="sohzw-video-wrapper">
  <div class="ar-wrap-16x9">
   <div class="ar-wrap-inside-fill">
   </div>
  </div>
 </div>
 <p>
  《自由路》歌词：
 </p>
 <p>
  “云起变迁，几经多风雨，
 </p>
 <p>
  繁荣香江，怎彻夜未眠？
 </p>
 <div>
 </div>
 <p>
  明天会否，曙光心中见？
 </p>
 <p>
  谁用雨伞，撑起半边天？
 </p>
 <p>
  邪魔的喧嚣，夜雨天，
 </p>
 <p>
  不畏惧，为了家这乐园。
 </p>
 <p>
  求天可指引，路哪边？
 </p>
 <p>
  谁放弃，我绝不退避！
 </p>
 <p>
  全凭你撑起这手臂去捍卫自由地，
 </p>
 <p>
  仔经几多打击，心不会死，
 </p>
 <p>
  如若我张开这双臂是否会感动你？
 </p>
 <p>
  就算这刻跌低，心中满希翼。
 </p>
 <p>
  迷雾里睁开着双眼，哪怕再度流泪，
 </p>
 <p>
  眼中闪烁的光，一睹壮举。
 </p>
 <p>
  如若我此刻拥紧你，仍可守护你，
 </p>
 <p>
  何用再要惧怕，孤身去面对？
 </p>
 <p>
  这勇敢的故事，就在今天，
 </p>
 <p>
  徐徐地望向这段路，难免崎岖。
 </p>
 <p>
  人如那浪潮，从未可顾虑，
 </p>
 <p>
  迈向终点，迷茫只因抉择吗？
 </p>
 <p>
  神已指引路归家。
 </p>
 <p>
  全凭你撑起这手臂去捍卫自由地，
 </p>
 <p>
  再经几多打击，心不会死，
 </p>
 <p>
  如若我张开这双臂是否会感动你？
 </p>
 <p>
  就算这刻跌低，心中满希翼。
 </p>
 <p>
  迷雾里睁开着双眼，哪怕再度流泪，
 </p>
 <p>
  眼中闪烁的光，一睹壮举。
 </p>
 <p>
  如若我此刻拥紧你，仍可以守护你，
 </p>
 <p>
  何用再要惧怕，孤身去面对？
 </p>
 <p>
  齐聚这狮子山脚，不屈不挠退后没余地，
 </p>
 <p>
  拾起信心捍守终有新转机。
 </p>
 <p>
  来共我抛开心中郁结同携手勇闯下去，
 </p>
 <p>
  明日你我盛放，峥峥岁月里。
 </p>
 <p>
  民阵也于脸书表示，我们疲乏，但不是无力；我们坚守，因为我们就是希望。最后民阵号召大家一起走下去，一个都不能少。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    云天
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2999446.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2999446.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2999446.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/01/n2999446.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

