### 遍地开出自由花 港人「反送中」抗争走进社区
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="香港多个社区涌现具民主象征的连侬墙，吸引民众留言为港人打气。（网络图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/shatian-600x401.jpg"/>
 </div>
 <div class="caption">
  香港多个社区涌现具民主象征的连侬墙，吸引民众留言为港人打气。（网络图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月6日】
  </span>
  <span class="content-info-type">
   （本台记者鄭欣综合报导）
  </span>
  香港「反送中」运动经过6月及7月初的多次大型抗争后，港府一直未回应民间诉求。港人正采取社区抗争的方式，在民间将「反送中」运动持续下去。周日，在香港多个社区涌现具民主象征的连侬墙，吸引民众留言为港人打气。此外，网民在网络发起的7.7九龙区大游行也获得警方不反对通知书，发起人表示希望能将港人的和平理性展示给大陆人，以对抗中共的新闻封锁。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  综合多家港媒报导，周日下午，香港多个社区有民众自发建设连侬墙，「为前线手足打气」、「香港人加油」、「一个都不能少」等各式写有打气字句的彩纸贴满告示板，表达港人持续抗争的决心。
 </p>
 <div id="fb-root">
 </div>
 <p>
 </p>
 <div class="fb-video" data-href="https://www.facebook.com/ArmChannelTV/videos/475209876646269/" data-width="750">
  <blockquote cite="https://www.facebook.com/ArmChannelTV/videos/475209876646269/" class="fb-xfbml-parse-ignore">
   <p>
    <span href="https://www.facebook.com/ArmChannelTV/videos/475209876646269/">
    </span>
   </p>
   <p>
    大埔連儂牆越來越多人留言…
   </p>
   <p>
    Posted by
    <span href="https://www.facebook.com/ArmChannelTV/">
     啱Channel
    </span>
    on Saturday, July 6, 2019
   </p>
  </blockquote>
 </div>
 <p style="text-align: center;">
  <span style="color: #0000ff;">
   网络视频
  </span>
 </p>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_3012382">
  <img alt="大埔区连侬墙。（网络图片）" class="wp-image-3012382 size-medium" src="http://img.soundofhope.org/2019/07/dapu-600x400.jpg" srcset="http://img.soundofhope.org/2019/07/dapu-600x400.jpg 600w, http://img.soundofhope.org/2019/07/dapu-768x513.jpg 768w, http://img.soundofhope.org/2019/07/dapu-180x120.jpg 180w, http://img.soundofhope.org/2019/07/dapu-366x244.jpg 366w, http://img.soundofhope.org/2019/07/dapu.jpg 869w">
   <br/><figcaption class="wp-caption-text">
    大埔区连侬墙。（网络图片）
   </figcaption><br/>
  </img>
 </figure><br/>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_3012385">
  <img alt="西环连侬墙。（网络图片）" class="wp-image-3012385 size-medium" src="http://img.soundofhope.org/2019/07/xihuan-600x400.jpg" srcset="http://img.soundofhope.org/2019/07/xihuan-600x400.jpg 600w, http://img.soundofhope.org/2019/07/xihuan-180x120.jpg 180w, http://img.soundofhope.org/2019/07/xihuan-366x244.jpg 366w, http://img.soundofhope.org/2019/07/xihuan.jpg 717w">
   <br/><figcaption class="wp-caption-text">
    西环连侬墙。（网络图片）
   </figcaption><br/>
  </img>
 </figure><br/>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_3012388">
  <img alt="官塘连侬墙。（网络图片）" class="wp-image-3012388 size-medium" src="http://img.soundofhope.org/2019/07/guantang-600x400.jpg" srcset="http://img.soundofhope.org/2019/07/guantang-600x400.jpg 600w, http://img.soundofhope.org/2019/07/guantang-768x512.jpg 768w, http://img.soundofhope.org/2019/07/guantang-180x120.jpg 180w, http://img.soundofhope.org/2019/07/guantang-366x244.jpg 366w, http://img.soundofhope.org/2019/07/guantang.jpg 816w">
   <br/><figcaption class="wp-caption-text">
    官塘连侬墙。（网络图片）
   </figcaption><br/>
  </img>
 </figure><br/>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_3012391">
  <img alt="西营盘连侬墙。（网络图片）" class="wp-image-3012391 size-medium" src="http://img.soundofhope.org/2019/07/xiyingpan-600x450.jpg" srcset="http://img.soundofhope.org/2019/07/xiyingpan-600x450.jpg 600w, http://img.soundofhope.org/2019/07/xiyingpan-180x135.jpg 180w, http://img.soundofhope.org/2019/07/xiyingpan-366x275.jpg 366w, http://img.soundofhope.org/2019/07/xiyingpan.jpg 720w"/>
  <br/><figcaption class="wp-caption-text">
   西营盘连侬墙。（网络图片）
  </figcaption><br/>
 </figure><br/>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_3012394">
  <img alt="大埔连侬墙。（网络图片）" class="wp-image-3012394 size-medium" src="http://img.soundofhope.org/2019/07/daipo3-450x600.jpg" srcset="http://img.soundofhope.org/2019/07/daipo3-450x600.jpg 450w, http://img.soundofhope.org/2019/07/daipo3-180x240.jpg 180w, http://img.soundofhope.org/2019/07/daipo3-366x488.jpg 366w, http://img.soundofhope.org/2019/07/daipo3.jpg 720w"/>
  <br/><figcaption class="wp-caption-text">
   大埔连侬墙。（网络图片）
  </figcaption><br/>
 </figure><br/>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_3012397">
  <img alt="红磡连侬墙。（网络图片）" class="wp-image-3012397 size-medium" src="http://img.soundofhope.org/2019/07/hongkan-450x600.jpg" srcset="http://img.soundofhope.org/2019/07/hongkan-450x600.jpg 450w, http://img.soundofhope.org/2019/07/hongkan-180x240.jpg 180w, http://img.soundofhope.org/2019/07/hongkan-366x488.jpg 366w, http://img.soundofhope.org/2019/07/hongkan.jpg 640w"/>
  <br/><figcaption class="wp-caption-text">
   红磡连侬墙。（网络图片）
  </figcaption><br/>
 </figure><br/>
 <p>
  据《明报》消息，90后见习工程师Matthew与多名义工在观塘社区摆下街站，呼吁过往市民在连侬墙写下心声。Matthew表示，近日有多宗轻生个案，希望借助连侬墙让市民缓解情绪，同时希望「反送中」的抗争运动不会慢慢熄灭。
 </p>
 <p>
  现场经过大埔墟连侬墙的郑小姐表示，没听说过有人会于社区内重设连侬墙，今日看到时不禁「哗」了一声，感动之余惊呼「每个区都有，就真系遍地开花啦！」
 </p>
 <p>
  目前，港铁香港大学站、荃湾站、沙田站、屯门站、元朗站外都有「连侬墙」出现。而数日前金钟立法会大楼旁的「连侬墙」曾遭亲共团体破坏，今天也有民众自发前往修复。
 </p>
 <div>
 </div>
 <p>
  香港连侬墙最早出现于2014年「雨伞运动」期间，位于金钟香港政府总部外墙。当时的民众用便利贴或纸张写下要求真普选等诉求或画作，成为金钟「占领区」的地标性艺术作品。
 </p>
 <h4>
  <strong>
   社区小型放映会 还原冲突真相
  </strong>
 </h4>
 <p>
  香港警方在 6.12万名民众和平示威期间，动用不必要武力开枪暴力镇压，震惊世界。早前，因不满亲政府媒体片面报导误导市民，已有民间团体在多个社区举办放映会，让民众了解事件真相。7月1日，示威者攻占立法会后，亦有团体摆设街站，放映完整、动容的抗争画面，向巿民解释事件的来龙去脉。
 </p>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_3012400">
  <img alt="港人在社区举行放映会，还原6.12警民冲突真相。（网络图片）" class="wp-image-3012400 size-medium" src="http://img.soundofhope.org/2019/07/fangyinhui-600x400.jpg" srcset="http://img.soundofhope.org/2019/07/fangyinhui-600x400.jpg 600w, http://img.soundofhope.org/2019/07/fangyinhui-768x513.jpg 768w, http://img.soundofhope.org/2019/07/fangyinhui-180x120.jpg 180w, http://img.soundofhope.org/2019/07/fangyinhui-366x244.jpg 366w, http://img.soundofhope.org/2019/07/fangyinhui.jpg 947w"/>
  <br/><figcaption class="wp-caption-text">
   港人在社区举行放映会，还原6.12警民冲突真相。（网络图片）
  </figcaption><br/>
 </figure><br/>
 <p>
  今天，在观塘地铁站外，约10名年轻人以简单的放映设备举办小型放映会，播放有关「反送中」抗争运动及对抗争者访问的片段。
 </p>
 <p>
  参加者90后设计师Alex表示，这些年轻人素未谋面，经由网络群组达成共识，希望通过这种方式，把抗争的种子带入社区不同年龄层，让较年长的居民都认识抗争原因。
 </p>
 <h4>
  <strong>
   7.7九龙区游行：展示港人和平理性 抗衡大陆新闻封锁
  </strong>
 </h4>
 <p>
  早前有网民发起7月7日（7.7）九龙区大游行，已获警方不反对通知书。
 </p>
 <p>
  今天下午，游行发起者之一的刘颕匡，就「7.7 九龙区游行」召开记者招待会，强调游行将是「和平理性优雅」的行动，目的是让大陆游客了解港人「反送中」的诉求，并展示港人和平游行的一面，抗衡大陆的新闻封锁。
 </p>
 <p>
  对于近日有传言游行或组织人群冲击高铁站，以及到广东道袭击游客，刘颕匡驳斥这些说法是无中生有，强调游行是以「优雅」方式进行，绝对不希望有冲突，并已安排监察人员应对意外事件。
 </p>
 <p>
  刘颕匡相信，出现谣言的原因，是有人想令害怕有冲突的市民，放弃参与游行。
 </p>
 <p>
  「7.7 九龙游行」将于周日下午 3 点半举行。游行人士先于梳士巴利花园集会，下午 4 点由梳士巴利道公园出发，经梳士巴利道、九龙公园径、广东道、柯士甸道游行，终点为西九龙高铁站对出的汇民道。预计将有超过2千名市民参与。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    李璐
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n3012349.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n3012349.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n3012349.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/06/n3012349.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

