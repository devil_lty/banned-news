### 长荣航空空服员罢工  一万五千旅客受影响
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="台湾长荣航空桃园空服员工会昨天下午四时开始罢工。（新唐人影片截图）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/screenshot2019-06-21at1.48.14pm-600x365.png"/>
 </div>
 <div class="caption">
  台湾长荣航空桃园空服员工会昨天下午四时开始罢工。（新唐人影片截图）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月20日】
  </span>
  <span class="content-info-type">
   （本台记者斐珍综合报导）
  </span>
  台湾长荣航空因劳资双方多次协商破裂，取得合法罢工权的桃园空服员职业工会昨天下午四时宣布开始罢工。长荣航空表示，已成立紧急应变小组，全力疏导旅客，希望能降低对旅客带来的影响。今天(21日)将取消45班台湾出发航班、26班返台航班，长荣将尽全力维持5成运能。目前预估约将有1万5千名旅客受到影响。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  由劳动部次长刘士豪主持的长荣航空劳资双方的协商昨日在台北万丽酒店进行，因针对空服员工会所提出的8大诉求进行讨论后未达成共识，工会认为，已历经2次协商，公司所提出的版本并无改变，因为无法感受公司的诚意，工会在14时10分宣布当日16时起正式罢工。
 </p>
 <p>
  长荣航空昨日召开记者会说明应变措施。总经理孙嘉明表示，对于罢工对旅客造成的不便感到遗憾，公司立场以确保飞安、降低旅客不便为优先考量，会以负责任的态度因应此次事件。
 </p>
 <p>
  孙嘉明说，公司目前已成立紧急应变小组，也会盘点现有人力，以最大载运能力来服务旅客。有关工会提出的8项诉求，长荣会提出完整方案回应，希望能就此拉近劳资双方的落差，但对于禁搭便车条款，基于长荣公司是个大家庭，理应一视同仁，不能因员工不加入工会，就给予差别待遇。
 </p>
 <p>
  长荣空服员罢工今天进入第二天，一长荣地勤客服部的男子向媒体抱怨，由于此次的空服员罢工，造成他们第一线地勤及客服人员要面对旅客强烈的不满与抱怨，甚至是咆哮。
 </p>
 <p>
  该名男子表示，客服部里有接不完的抱怨电话，可说就是24小时服务旅客。另外，机场也是一团乱，机场没有办法处理、消化的业务，都是由地勤人员来承受。「为什么她们（罢工空服员）不自己上来去面对旅客？」该名男子说，他们可以来机场静坐也可以，旅客才会看得到啊！
 </p>
 <p>
  这名男子还认为，罢工其实是霸凌了旅客，很多旅客真的是有很大的困难，没有很多航班可以转机，到目前还有很多旅客的机票没有办法处理完。
 </p>
 <p>
  针对昨天工会无预警的罢工，长荣航空表示，已成立紧急应变小组，全力疏导旅客，希望减低对旅客的影响。公司会在网页罢工专区或社交网站，公布最新航班取消或改动的消息。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    Leo
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2975248.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2975248.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2975248.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/20/n2975248.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

