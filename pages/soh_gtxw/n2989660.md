### 再行动！数百港人包围律政中心问责郑若骅
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="大批网民响应网上号召，到中环律政中心外向律政司长郑若骅抗议。（网络图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/central-thumb-20190627-e-1024--600x400.jpg"/>
 </div>
 <div class="caption">
  大批网民响应网上号召，到中环律政中心外向律政司长郑若骅抗议。（网络图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月27日】
  </span>
  <span class="content-info-type">
   （本台记者高健雯综合报导）
  </span>
  香港民阵昨晚（26日）在G20峰会前夕于中环爱丁堡广场发起集会，希望出席峰会的各国领导人支持港人要求港府撤回送中条例的诉求，有数千人参加。今早又有香港网友发起包围香港律政中心行动，数百名民众聚集现场向律政司司长郑若骅抗议。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  据香港媒体报导，有网民发起今早 10 时到律政中心“观赏建筑风格” ，警方早上就在通往律政中心的花园道截查路人，记录身份证，并要求打开背包搜查。
 </p>
 <p>
  截至上午11时30分，律政中心门外聚集了约300人。包围行动开始两小时后，民众开始坐在路旁及马路上休息，高喊“反送中”、“撤回修例”、“释放义士”、“撤回暴动定性”、“郑若骅出来”。他们要求香港律政司司长郑若骅现身会见示威者，撤回《逃犯条例》，成立独立调查委员会调查警察在6月12日暴力对待示威者的事件，要求追究警方当日滥用权力，释放被捕的示威者并撤销“暴动”定性。但律政司司长郑若骅早上10点左右驾车进入律政中心后再没有露面。
 </p>
 <p>
  香港众志秘书长黄之锋、香港立法会前议员罗冠聪等人也一早赶到现场。黄之锋表示他响应网民呼吁而来，并要求郑若骅回应群众诉求，撤回暴动定义等，否则他相信7月1日大游行前还会有多场小规模示威。
 </p>
 <p>
  根据黄之锋脸书推文，由于示威人数众多，花园道局部变成单线双程行车，交通一度出现挤塞，但多位示威者主动协助疏导交通。
 </p>
 <figure class="wp-caption alignnone img-width-m" id="attachment_2989873">
  <img alt="" class="size-medium wp-image-2989873" src="http://img.soundofhope.org/2019/06/65928040-2334938509931977-8758147626097967104-n-600x413.jpg" srcset="http://img.soundofhope.org/2019/06/65928040-2334938509931977-8758147626097967104-n-600x413.jpg 600w, http://img.soundofhope.org/2019/06/65928040-2334938509931977-8758147626097967104-n-768x529.jpg 768w, http://img.soundofhope.org/2019/06/65928040-2334938509931977-8758147626097967104-n-180x124.jpg 180w, http://img.soundofhope.org/2019/06/65928040-2334938509931977-8758147626097967104-n-366x252.jpg 366w, http://img.soundofhope.org/2019/06/65928040-2334938509931977-8758147626097967104-n.jpg 960w">
   <br/><figcaption class="wp-caption-text">
    示威者自发协助疏导交通（黄之锋脸书）
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  有报导称，至下午 2 时，律政中心外仍有数百人留守，现场不时的有物资送到，并鼓励示威者“加油”。
 </p>
 <p>
  律政中心的职员当时向示威者表示，有位职员患有肌肉萎缩症，需要乘坐的士离开，希望示威者让路。的士抵达后，占据行车线的示威者们主动散开，的士顺利驶入律政中心接载患者。
 </p>
 <p>
  据香港众志的成员表示，示威者包围了律政中心包含正门在内的3个出入口，成功的阻碍了律政中心运作。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    云天
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2989660.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2989660.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2989660.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/27/n2989660.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

