### 林郑强推「送中恶法」失败 民阵泛民警告不回应民意持久抗争
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="民主派议员与民阵批评林郑至今未真正回应民意。（摄影 ：郑铭）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/c6f915fb-5f70-4bff-bae2-fbe627e098fe-600x400.jpeg"/>
 </div>
 <div class="caption">
  民主派议员与民阵批评林郑至今未真正回应民意。（摄影 ：郑铭）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月10日】
  </span>
  <span class="content-info-type">
   （本台记者鄭銘采访报导）
  </span>
  面对九龙23万人「反送中」大游行，「龟缩」多日的香港特首林郑月娥9日终于露面，承认强推「送中恶法」全面失败，又用「寿终正寝」来形容修例失败，但始终不肯宣布撤回修例，对「反送中」团体和人士的其他四项诉求（追究警队滥权、实行真双普选、收回暴动定义、撤销义士控罪）更是不为所动。发起多次「反送中」大游行的民阵与民主派议员批评林郑当局为了面子至今不肯真正认错，不排除未来继续发起抗争行动。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  民阵召集人岑子杰批评林郑今次的言论还是之前的坚持「暂缓」但不肯「撤回」恶法的那一套说辞，骨子里还是为了面子牺牲很多市民，令人感到心寒，尤其是立法会的议事规则、法律用词都没有「暂缓」或「寿终正寝」，林郑月娥所谓的愿意聆听市民的意见及对话，其实是陷阱。「因为林郑一直以来都是假对话，真对立。包括她的老师，周永新教授，在全民退休保障，她找她老师来做研究，因为研究的结果不合林郑的心意，周永新教授的专业被林郑侮辱，说他不懂公共开支。」
 </p>
 <p>
  岑子杰强调香港人很难再相信林郑月娥，因为她以往有种种骗人的旧绩。「当年雨伞运动期间，她跟学生的对话，到现在都没有兑现；皇后码头，与示威者对话，谈完即拆；喜帖街重建的对话，倾完即拆。林郑月娥的对话是陷阱，她是假对话，真对立，前科累累。」
 </p>
 <p>
  对于林郑再次拒绝针对警方「反送中」滥用武力成立独立的调查委员会，岑子杰批评林郑政府没有法治观念更是知法犯法。「如警方执法可以平射地向示威者开枪，可以用警棍殴打示威者的头部，警察执法没有戴委任证，甚至速龙小队也不需要展示警员编号。我们希望有一个独立调查委员会去调查反送中条例，警察执法的问题、官员的错误，但是林郑月娥就认为，监警会成立一个调查小组就已经足够。」
 </p>
 <p>
  民主派立法会议员则发表联署声明回应，批评林郑月娥躲避多日，仍沉迷于语言伪术和面子问题，毫无诚意回应港人大声清楚提出的五大诉求，强烈谴责林郑月娥毫无担当、厚颜无耻，必须下台。
 </p>
 <p>
  公民党郭家麒议员批评林郑月娥「黑心、坏心肠」分化港人，「因为她企图将香港人分化，将一些所谓和平示威和冲击的香港人分化。更令人愤怒的是，从伞运至今林郑月娥皆无承担责任，是最大的罪魁祸首。她继续这种态度，继续希望转移视线，最终受害是整个香港，香港寿终正寝，我们的言论自由，我们的法治、我们所有的东西都将被牺牲。」
 </p>
 <p>
  郭家麒又质疑当局起诉年轻示威者，但问责高官却不用为强推送中条例犯错而下台：「特别是李家超、郑若骅和行会成员，不需要受罚？为甚么一个人做错事，林郑做错事不需要受罚？但是在香港为了争取撤销这个条例，而付上自己生命，包括死了的4位，还有在几次冲突中，冒着被警察殴打和控告的人，就要负责任呢？」
 </p>
 <p>
  民主党尹兆坚议员呼吁林郑尽快下台。「在十一中共的建政周年前若不回应诉求，除了遍地开花运动会继续外，十一将会激发更多人上街，届时可能不只200万人。」
 </p>
 <p>
  民主派议员与民阵都警告林郑当局，再不回应民意，将继续一系列抗争。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    李璐
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n3021034.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n3021034.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n3021034.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/10/n3021034.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

