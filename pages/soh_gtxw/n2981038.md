### 香港警方公立医院抓5人4涉「反送中」暴动罪 82特首选委轰违规执法
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="82名特首选委斥责警方到医院抓捕「反送中」示威者涉嫌违规执法。（摄影：郑铭）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/medical-photo-600x400.jpg"/>
 </div>
 <div class="caption">
  82名特首选委斥责警方到医院抓捕「反送中」示威者涉嫌违规执法。（摄影：郑铭）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月23日】
  </span>
  <span class="content-info-type">
   （本台记者鄭銘采访报导）
  </span>
  面对二百万人上街游行反对「送中恶法」，警务处处长卢伟聪次日17日改口，指形容6月12日的「反送中」示威行动为暴动，是因为其中有人涉及暴动行为，又透露警方当日拘捕15名的示威人士，当中五人涉暴动罪。有团体揭发这五名涉嫌暴动的人士都是警方在公立医院进行抓捕。医学界、护理界、法律界等82名有特首选举委员会选委发表联合声明，批评警方涉嫌违规执法，阻碍救护工作和不尊重病人私隐。
 </p>
 <div class="sohzw-video-wrapper">
  <div class="ar-wrap-16x9">
   <div class="ar-wrap-inside-fill">
   </div>
  </div>
 </div>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  视频制作：郑铭
 </p>
 <p>
  来自医学界、衞生服务界及法律界等多个团体代表星期日发表联署声明，促请警方切勿阻碍救护工作及尊重病人私隐，声明一共得到82名选委签名支持。
 </p>
 <p>
  参与联署的法政汇思在记者会指，继荃湾港安医院证实有警员要求前线医护提供6.12求诊示威者资料后，综合民阵、民权观察、星火同盟及大专院校等资料，据了解有四男一女6月12日的「反送中」示威者在公立医院被捕，其中四人涉暴动罪，另一人涉非法集结，包括三人在伊利沙伯医院被捕，广华和仁济医院各有一人被捕；有医护人员更踢爆有警员于医院内偷听医护谈话内容，甚至威逼医护提供病人资料，质疑警方此举属越界蒐证。法政汇思召集人吴宗銮称，警员若以非法手段蒐证，相关证供呈堂性会备受质疑，惟被告不能以此作抗辩理由。
 </p>
 <p>
  杏林觉醒成员、医学界选委黄任匡强调，《警察通例》要求警员遵守私隐条例，医护都有专业守则，保护病人私隐，斥责警方越界搜证执法的做法绝不恰当，警方应该申请法庭手令以书面向医院索取报告蒐证，而不是随口向医护人员索取资料。他重申医护绝无责任向警方披露病人资料，「家暴病人让人打到流血，医护都不会主动报警」。
 </p>
 <p>
  衞生服务界选委、医疗政策关注组发言人洪梓然披露，一位护士透露日前有警员不佩戴委任证，走到医院多处地方偷听医护人员谈话内容，「比较粗鲁甚至以恐吓语气恐吓护士姑娘，向她拿资料」。
 </p>
 <p>
  洪承认为，若护士未经许可披露病人资料或会牴触操守问题，可被处分甚至停牌，强调医护不曾面对相关情况并且被恐吓时忧会否负上法律责任，才在高压下提供资料。
 </p>
 <p>
  香港个人资料私隐公署表示，截至6月21日下午4时，共接获32宗涉医护人员未经批准向警方提交病人资料投诉和查询，强调执法机构不能以侦测或防止罪行为由，任意收集资料，在提出要求收集资料时，有责任明确告知医院是否必须提供资料，否则，执法机构或因误导医院或滥权而违反私隐条例。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    李璐
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2981038.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2981038.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2981038.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/23/n2981038.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

