### 电影《归途》台北试映 带观众一起见证奇蹟
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="现场与会的贵宾有主演《归途》刚获得美国阿克莱德电影节男主角最佳成奖的姜光宇(中间）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/img-0217-600x450.jpg"/>
 </div>
 <div class="caption">
  现场与会的贵宾有主演《归途》刚获得美国阿克莱德电影节男主角最佳成奖的姜光宇(中间）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月22日】
  </span>
  <span class="content-info-type">
   （本台记者斐珍采访报导）
  </span>
  由美国新世纪影视基地推出的电影《归途》，22日晚在台湾台北市议会聼举办了试映及座谈会。现场与会的贵宾有专程从美国而来的《归途》主演姜光宇，他因主演《归途》刚获得美国阿克莱德电影节男主角最佳成奖。台湾大学新闻研究所教授张锦华和人权律师朱婉琪也到场观赏。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  《归途》剧中男女主角都取材于真人真事，男主角姜光宇的人生经历和剧中男主角有着类似的经历。姜光宇毕业于中央戏剧学院，曾是大陆颇有名气的影视名星。
 </p>
 <p>
  姜光宇在受访时表示，二十多年前在他名利双收后却没有感到幸福、快乐，反而在精神上感到极度的欠缺，甚至曾过着花天酒地、醉生梦死的生活。直到1998年，在偶然的机会阅读《转法轮》后，让他感觉到如醍醐灌顶。
 </p>
 <p>
  “就像困在黑暗的小房间里，没有出口，而《转法轮》这本书仿佛替他打开了天窗。于是他开始修炼法轮大法，放弃了人人称羡的名利。姜光宇说：「金钱和快乐没有关系，坚持信仰才让我看到希望。」
 </p>
 <p>
  张锦华教授在座谈会中提到，她已修炼法轮功20年了，她发现每一位法轮功学员几乎都见证了自己身心灵改变的奇蹟。她过去便常在想，这些人的故事都应该变成电影，变成一本书，变成纪录片，分享给全世界的人，所以她今天非常开心来观赏这部见证奇蹟，充满正能量的电影。
 </p>
 <p>
  谈到「整部影片最触动人的地方」张锦华说，当男主角「王浩晨」说自己「错过很多得大法的机会，一直到生命的最后」，这句话让他感慨很深。
 </p>
 <p>
  人权律师朱婉琪认为，以法轮功学员或者从事法轮功人权工作者的角度来看，《归途》这部电影更像是一部纪录片。因为在世界各国各个阶层都有「王浩晨」，都有社会主流菁英在人生的低谷，无论身体的心理的，经由修炼大法而获得重生。
 </p>
 <p>
  朱婉琪也提到，当男主角「王浩晨」开始跟大家一起炼功的那一刻是让他最为感动的地方。因为自己二十年前也是因为身体病痛的原因而走入大法修炼，一直到后来自己脱胎换骨。所以很可以理解片中女主角李雪在被举报后，经历了两年的劳教冤屈后，并没有打击她的意志，来到了自由世界加拿大的街头，继续帮助当地的民众来了解法轮大法。
 </p>
 <p>
  朱婉琪说，这表现出法轮大法在全球集体信仰的精神力量，不会因为牢狱之灾而打垮修炼人信仰的本质。受害者反而能够帮助加害者离开中国共产党，帮助那些不理解、受到谎言欺骗的人能够重新认识大法。
 </p>
 <div>
 </div>
 <p>
  日前参加了美国阿克莱德电影节，姜光宇获得男主角最佳成就奖。阿克莱德电影节曾被美国权威杂志《电影人杂志》评为25个最值得参赛的电影节之一。
 </p>
 <p>
  饰演当男主角「王浩晨」的姜光宇，谈到他的得奖感言时表示，自己是二十多年前很偶然的机会进入了演艺圈，能在二十多年后拿到这个奖，虽然他不是奥斯卡、不是Gala（棕榈泉国际电影节），但对他自己以及新世纪都是很好的开始。
 </p>
 <p>
  另外，姜光宇觉得，目前世界上还有许多不公平的事，对人权、对不同群体的迫害。未来他也将在人权问题上更加关注，希望能为不同的群体站台，发表人权言论。
 </p>
 <p>
  新世纪影视基地自成立以来，连续多次获得国际电影节大奖。新世纪影视基地的作品超越了种族、文化的差异，因此广受到了各族裔民众的认可和喜爱。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    鄭欣
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2978785.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2978785.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2978785.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/22/n2978785.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

