### 外媒：捍卫香港自由 不信中共 港人再掀抗议“送中”恶法潮
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="6月16日的抗议人群（网友推特）。" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/download1-14-600x400.png"/>
 </div>
 <div class="caption">
  6月16日的抗议人群（网友推特）。
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月16日】
  </span>
  <span class="content-info-type">
   （本台记者宇宁综合编译）
  </span>
  6月16日下午，200万港人身着黑衫、挂着白丝带再次走上街头和平抗议，要求港府撤回“送中”恶法， 追究警察镇压暴行、释放被捕人士， 停止白色恐怖， 并要求香港特首林郑月娥引咎下台。CNN和纽约时报16日分别以“捍卫香港自由”和“不信任中共”为主题报导了港民于6月16日的和平游行。
 </p>
 <h4>
  <strong>
   CNN：捍卫香港的自由 我们必须站出来
  </strong>
 </h4>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  据CNN报导，一位23岁的香港抗议人士Michael说，他们是为了护卫香港，也为了悼念于15日坠楼而牺牲的义士来参加16日的抗议。
 </p>
 <p>
  他说：“我们需要站出来，告诉港府，我们不同意这个引渡法案。 因为香港是一个非常特殊的地方， 这里的经济、文化对这个世界而言都非常特殊。 ”
 </p>
 <p>
  他只提供了他的名字，没有提供他的姓氏， 他的手里拿着一个标语牌，上面写着：“没有免费的自由。 ”
 </p>
 <p>
  他指着自己手中的一束白花，说：“我们买了白花，是因为我们希望那位死者能够安息。”6月15日，一位35岁的人士在抗议“送中”法案时坠楼身亡， 抗议人士在16日的抗议前还曾经为他举行了简短的悼念仪式。
 </p>
 <p>
  他表示，虽然港府宣布推迟对“送中”恶法的讨论，但是并没有撤除此法案，这意味着他们可能还会再次推出这个法案，因此他们必须继续抗议，直到港府撤回“送中”恶法。
 </p>
 <p>
  18岁的抗议者Mandy则表示， 她并没有参加6月9日的抗议。她说：“然而经过这一周的时间， 我发现这个问题越来越严重，并意识到我应该站出来，所以我今天来参加了抗议。”她表示16日是她的生日，“我认为这个抗议比我的生日更重要，所以我就来了。 ”
 </p>
 <p>
  65岁的Chik Kim Ping 女士是和夫君， 70岁的Tse 先生专门从新界赶到维园参加抗议的。她表示她在为自己的孩子们的权利而抗争。她说：“我们来参加今天的抗议，这对于我们的孩子们会很重要，因为我们老了，不会看到2047年的事情了，但是我的孩子们会看到。”香港的所谓的“一国两制”将在2047年结束。
 </p>
 <h4>
  纽约时报：不信任中共 港民再次抗议
 </h4>
 <p>
  纽约时报的报导表示，虽然香港特首林郑月娥15日做出了让步，承诺推迟对”送中”法案的讨论， 但是港民出于对警方的暴力行为的不满和对北京当局的不信任，16日再次走上街头和平抗议，报导中写道“中共当局推出的‘送中’法案完全没有隐瞒其试图将香港纳入与中国大陆其他地区一样的专制统治的野心。”
 </p>
 <div>
 </div>
 <p>
  一位23岁的抗议者Kenneth Wong 表示，“我想告知中共，港民不信任中共。” 她手中拿的标语牌写着要求撤回“送中”恶法。
 </p>
 <p>
  他说：“我们香港人站在一起，都在传递同一个信息，那就是我们不要这个法案，我们想让林郑月娥下台。 ”
 </p>
 <p>
  游行队伍高喊着口号：“林郑下台！”， “撤回法案！” “我们不是暴徒！“ ”释放被捕的学生！”、“不要杀害我们！“ 等。
 </p>
 <p>
  40岁的抗议者Anthony Tam 是一位工程师，他和妻子带着他们分别是六岁和九岁的两个女儿参加了抗议游行。他表示他本来并不关心政治，这是他第一次参加抗议游行。他说：“我是土生土长的香港人，而现在这个香港已经变得我不认识了。”他指责警察在用暴力对待一群基本上是和平的抗议者，他说：“这种处理方法令人厌恶！”
 </p>
 <p>
  30岁的抗议者Pan Chow 是一位办公室职员，他表示2014年的“雨伞运动”中很多港人对于防暴警察使用催泪瓦斯对待和平的抗议人士就感到非常震惊，而“这次警察在使用更严厉的方式驱散抗议者，我无法容忍这种情况变成一种常态。”
 </p>
 <p>
  另外一位18岁的少女Betty说：“我们在为我们的自由而抗争。” 她认为港府暂停辩论“送中”恶法，而非撤回它，就如同“拿着一把刀对着一个人的头，声称‘我现在不杀你’ ，但却可以在其它任何时候杀害他。”
 </p>
 <p>
  曾经领导黄雀行动，在香港营救由于“六四”事件遭到中共通缉的异议人士的朱耀明先生表示，在三十年前协助营救“六四”学生时，他完全没有料到他在自己的故乡香港也会遇到这种问题。他因为是2014年的“雨伞运动”的“占中三子”之一而被判缓行两年。
 </p>
 <p>
  据卫报报导，如果前几次抗议一样，16日的抗议也是非常有序的，在抗议途中有免费供水站，而且还有人提醒抗议人士随身携带自己的垃圾。
 </p>
 <p>
  批评者表示，受中共控制的港府试图强行推动“送中”恶法会摧毁捍卫香港的经济和社会自由，以及防止其受到中共侵害的司法独立，很多香港的商界人士出于对此法案的担忧已经决定将自己的资产转移出香港。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    云天
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2963293.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2963293.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_gtxw/n2963293.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/16/n2963293.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

