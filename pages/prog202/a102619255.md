### 美售台108辆“地表最强战车” 三大看点引关注
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/3000x2903_593313032486.jpg" target="_blank">
  <figure>
   <img alt="美售台108辆“地表最强战车” 三大看点引关注" src="https://i.ntdtv.com/assets/uploads/2019/07/3000x2903_593313032486-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  艾布兰坦克被称为“地表最强战车”，美国此次军售将使台湾陆军战力全面升级。（美国陆军官网）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月10日讯】美国五角大楼7月9日宣布，美国国务院已批准2项对台军售，包括108辆“M1A2T艾布兰”（Abrams） 坦克、250枚刺针飞弹（Stinger Missiles）和相关设备， 总额超过22亿美元。 美国此次对台军售的M1A2T战车被称为“
  <span href="https://www.ntdtv.com/gb/地表最强战车.htm">
   地表最强战车
  </span>
  ”，它有三大看点将使台湾陆军战力全面升级。
 </p>
 <p>
  此次军售是川普政府任内第4次对台军售，而且是金额最高的一次。蔡英文总统9日在脸书发文称，美国政府持续以具体行动履行承诺，协助台湾强化自我防卫的力量，我要表达感谢。台湾会坚定守护得来不易的民主和自由，也会共同捍卫区域中的安全与稳定。
 </p>
 <p>
  当天，台湾外交部的新闻稿表示，“美国政府依照《台湾关系法》持续军售台湾，并不会对两岸关系造成影响；相反地，强化台湾防卫实力，吓阻可能的军事进犯，才能维系两岸的和平与稳定。”
 </p>
 <p>
  声明还称，中国大陆近年来对台湾有许多“言论恐吓及军事挑衅”，此次采购让台湾自我防卫的能力增加。
 </p>
 <p>
  BBC的报导称，美国这次对台军售估计本来在4月就会通过，但因为美国与北京在G20峰会期间将“继续协商”的缘故，军售案有所延期。
 </p>
 <p>
  台媒中央社报导称，此次通过军售案，对台湾军事士气有所帮助。尤其，美国出售的艾布兰战车，对亟需升级的台湾陆军帮助很大。M1A2T被称为“
  <span href="https://www.ntdtv.com/gb/地表最强战车.htm">
   地表最强战车
  </span>
  ”，将使国军战力全面升级，强化反登陆、不对称作战能力。
 </p>
 <p>
  <strong>
   M1A2T战车三大亮点
  </strong>
 </p>
 <p>
  报导引述中正大学战略暨国际事务研究所兼任助理教授林颖佑的分析称，因应共军新的两栖作战模式，陆战队登陆不再像过去建立滩头堡，而是可能利用机动力长驱直入台湾的后方。M1A2可以守住后方战线，不再是过去传统的战车以平原展开的作战模式。
 </p>
 <p>
  美国售台M1A2的次型号定名为T（TAIWAN），M1A2T战车采用燃气涡轮引擎是一大亮点，燃气涡轮的好处是马力大、比柴油还经济，而且同样能获得良好的功率表现。报导指M1A2T战车有三大亮点：
 </p>
 <p>
  中华战略前瞻协会研究员揭仲认为，  M1A2配备  “车际间资讯互联系统           （IVIS）”，能即时接收或分送战场资讯，大幅提升国军整体作战能力。
 </p>
 <p>
  国防安全研究院资源与产业研究所长苏紫云表示，M1A2T的120公厘战车炮接战距离更长、对动态目标的命中率更高。
 </p>
 <p>
  军事连线杂志主编陈维浩表示，M1A2T的规格不可能与美军相同，但M1A2T会拥有数位化的内装及遥控枪塔，等同于美军正式升级型号SEP3的等级，战力大大跃升。
 </p>
 <p>
  陆军参谋长杨海明中将表示，M1A2T装备配备将在后续发价书研讨时做细部讨论，也会依作战需求做性能评估跟购置。
 </p>
 <p>
  许多军事分析称，M1A2T战车在台湾将肩负如何在敌方“抢滩”中，发挥摧毁抢滩敌军的功能。此次美方售予台湾
  <span href="https://www.ntdtv.com/gb/艾布兰坦克.htm">
   艾布兰坦克
  </span>
  ，未来将成为2个装甲营的编制，部署在台湾北部新竹湖口军区。
 </p>
 <p>
  台湾立法院第9届第7会期外交及国防委员会召集委员王定宇分析，未来在北台湾可能会组建3个营的M1A2战车营。
 </p>
 <p>
  （记者罗婷婷报导/责任编辑：文慧）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102619255.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102619255.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102619255.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/10/a102619255.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

