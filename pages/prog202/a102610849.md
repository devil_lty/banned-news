### 奇闻！被棕熊捉走当储食 男子月余奇迹获救
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/bear-422682_960_720.jpg" target="_blank">
  <figure>
   <img alt="奇闻！被棕熊捉走当储食 男子月余奇迹获救" src="https://i.ntdtv.com/assets/uploads/2019/06/bear-422682_960_720-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  俄罗斯媒体报导，男子亚历山大（Alexander）在森林遇上棕熊，遭棕熊袭击受伤后，被当作“储备粮”拖回熊窝，一个月后被猎人发现奇迹生还。示意图（pixabay.com）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月28日讯】近日，一名
  <span href="https://www.ntdtv.com/gb/俄罗斯.htm">
   俄罗斯
  </span>
  <span href="https://www.ntdtv.com/gb/男子.htm">
   男子
  </span>
  的离奇遭遇引发舆论高度关注。据俄罗斯媒体报导，俄男亚历山大（Alexander）在
  <span href="https://www.ntdtv.com/gb/森林.htm">
   森林
  </span>
  遇上
  <span href="https://www.ntdtv.com/gb/棕熊.htm">
   棕熊
  </span>
  ，遭棕熊袭击受伤后，被当作“
  <span href="https://www.ntdtv.com/gb/储备粮.htm">
   储备粮
  </span>
  ”拖回熊窝，一个月后被猎人发现奇迹生还。
 </p>
 <p>
  6月26日，“
  <span href="https://www.ntdtv.com/gb/俄罗斯.htm">
   俄罗斯
  </span>
  -24”新闻频道网站报导，当地一群猎人在经过一个熊的巢穴时，猎犬在熊穴前方狂吠不肯离开，于是他们进到巢穴里，发现一个严重受伤、奄奄一息的
  <span href="https://www.ntdtv.com/gb/男子.htm">
   男子
  </span>
  。猎人起初还以为他是一具“木乃伊”。
 </p>
 <p>
  男子被送往医院后，被诊断出脊柱骨折，身上多处受伤，左眼严重受伤几乎张不开，严重消瘦。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102610851" style="width: 600px">
  <span href="https://i.ntdtv.com/assets/uploads/2019/06/bear-1376361_960_720.jpg">
   <img alt="" class="size-medium wp-image-102610851" src="https://i.ntdtv.com/assets/uploads/2019/06/bear-1376361_960_720-600x338.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   俄罗斯媒体报导，男子亚历山大（Alexander）在
   <span href="https://www.ntdtv.com/gb/森林.htm">
    森林
   </span>
   遇上
   <span href="https://www.ntdtv.com/gb/棕熊.htm">
    棕熊
   </span>
   ，遭棕熊袭击受伤后，被当作“
   <span href="https://www.ntdtv.com/gb/储备粮.htm">
    储备粮
   </span>
   ”拖回熊窝，一个月后被猎人发现奇迹生还。（合成图片）
  </figcaption><br/>
 </figure><br/>
 <p>
  从《EAST2WEST NEWS》发布的影片可以看到，亚历山大肤色接近死亡白色，身上满是尘土与血液干涸的痕迹，勉强能够睁开眼睛，也能说出自己的名字。片中其他人似乎是说图瓦当地方言，亚历山大讲俄语。
 </p>
 <p>
  据报，棕熊会将它们杀死的动物或找到的腐肉局部或全部埋好，有时会等上数天或数周，接着才挖出来吃掉。目前不清楚亚历山大被发现的确切时间与地点、以及他的身份与遭到棕熊袭击的过程。
 </p>
 <p>
  当地医生表示，亚力山大在这样的条件下，能幸存下来堪称奇迹，他们也无法对此作出合理解释。
 </p>
 <p>
  医生还说，亚力山大还可以移动手臂，但被诊断出脊椎骨折与体重严重偏低，身上还有多处受伤，皮肤组织因长时间躺着不动已经开始腐烂，此刻他精疲力尽无法动弹，已入住加护病房。
 </p>
 <p>
  据《西伯利亚时报》（The Siberian Times）和《每日邮报》报导，此事发生在邻近俄国及蒙古边镜的图瓦共和国（Republic of Tuva），这名男子自称名叫亚历山大。
 </p>
 <p>
  但亚历山大记得自己的名字，不记得姓氏与年纪，他自称一个月前，遭到棕熊袭击导致脊椎骨折，最后被拖入熊穴中，靠喝尿液维生。
 </p>
 <p>
  亚历山大说：那熊没有当场吃了他，而是将他拖回巢穴内当作“储备粮”存放，留下日后再吃，期间他感到非常害怕。
 </p>
 <p>
  目前尚不清楚亚历山大是在什么情况下与熊相遇，并遭到袭击，也不清楚他是如何带着伤，在熊窝里存活了一个月。
 </p>
 <p>
  当地媒体也没有报导这名受害男子的年龄情况，也没有提及事发地的具体位置，只是称这处熊窝靠近俄罗斯图瓦共和国和蒙古的交界处。
 </p>
 <p>
  这一条离奇新闻，经主流传媒《消息报》（Izvestia）与通讯社《EADaily》披露后，一位图瓦共和国卫生部的代表称，无法证实此事的真实性，因为当地卫生部、紧急情况部以及其他部门都没有关于此事的登记记录。
 </p>
 <p>
  这名代表说，也许此事发生在别的地方。亚历山大在影片中操俄语，而不是图瓦方言。
 </p>
 <p>
  （记者李芸报导/责任编辑：戴明）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102610849.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102610849.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102610849.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/28/a102610849.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

