### 【禁闻】香港问题对峙升级 英召见中共大使
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月05日讯】中共和英国在香港问题上的对立升级。在中共外交部和官员连续指责英国支持香港民众的言论后，英国外交部在7月3号下午，召见了中共驻英大使刘晓明，对他的公开发言表示“不能接受”。7月4号，
  <span href="https://www.ntdtv.com/gb/英国外交大臣.htm">
   英国外交大臣
  </span>
  亨特（Jeremy Hunt，侯俊伟）在接受采访时表示，英国不排除对中共实施制裁、驱逐
  <span href="https://www.ntdtv.com/gb/中共外交官.htm">
   中共外交官
  </span>
  的可能性。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/英国外交大臣.htm">
   英国外交大臣
  </span>
  亨特（Jeremy Hunt，侯俊伟）7月2号在接受英国广播公司（BBC）专访时，呼吁中国当权者不要把香港的抗议当做压迫的借口，他说，英国并不支持暴力示威，但能理解香港人对于自由被夺走的忧心。中共高压的方式只会让事情更严重。他呼吁中共遵守《
  <span href="https://www.ntdtv.com/gb/中英联合声明.htm">
   中英联合声明
  </span>
  》，维护香港自治，如果违反联合声明将会产生“严重后果”。
 </p>
 <p>
  第二天上午，中共驻英大使刘晓明临时召开记者会，声称亨特支持“暴力违法者”，并指控英国还对香港抱有“殖民者的思维”。中共外交部驻港公署官网也在同一天发布言词激烈的声明，抨击亨特“多次发表错误涉港言论”，并声称：“《
  <span href="https://www.ntdtv.com/gb/中英联合声明.htm">
   中英联合声明
  </span>
  》中规定的与英国有关的权利和义务都已经全部履行完毕。英方对主权移交中国后的香港，一无主权，二无治权，三无监督权，根本不存在任何所谓‘责任’。”
 </p>
 <p>
  消息人士告诉路透社，英国认为刘晓明的言词“令人无法接受”。当天下午英国外交部召见刘晓明，英国外交部常务次长麦克唐纳（Simon McDonald）告诉他，他所说的“既不能接受也不准确”。
 </p>
 <p>
  加拿大华裔作家盛雪：“现在英国召见中共驻英国的大使，这个是完全必要的。一定要申明英国现在对香港局势的这种关切，而且要有一个非常明确的强硬的立场。因为中共要拿下香港，实际上是对整个国际社会的民主阵营发出一个非常明显的威胁。”
 </p>
 <p>
  英国对香港是否有责任呢？回顾历史，《中英联合声明》签订前，在英国国内和国际都曾引发过极大的争议。中华民国行政院院长孙运璿曾严正声明，香港是满清割让，而中华民国推翻满清，继承满清法统，任何涉及香港问题时须以中华民国为谈判对象。
 </p>
 <p>
  盛雪：“《中英联合声明》签署的过程当中，本身就有相当多的异议。因为首先认为在中共是一个专制独裁政权的时候，英国是没有义务将香港的主权移交给中国。第二就是，如果说要移交给中国人的话，那么首先是应该移交给中华民国。”
 </p>
 <p>
  当时的中共领导人邓小平提出实行“一国两制、高度自治、港人治港”政策，最终与英国首相撒切尔夫人签订了《中英联合声明》，中共承诺保持香港现有的生活方式五十年不变。
 </p>
 <p>
  盛雪：“所以说在这种情况下，英国对香港确实是有着不可推卸的责任。现在英国在香港的问题上它是需要强硬的。不仅仅有这个道义的责任，而且它也有整个的历史责任。”
 </p>
 <p>
  不过，50年还没过一半，中共外交部2017年已经宣称：中英联合声明是历史文件，不具任何现实意义及约束力。
 </p>
 <p>
  香港作家赵耀景：“《中英联合声明》在联合国报备的。它维持一国两制是有一个国际担保的。所以英国的外相说，如果中共继续倒行逆施会有严重后果，这个严重后果是会有的。因为美国跟英国如果一起来执行的话，中国大陆的经济是会受不了。”
 </p>
 <p>
  被刘晓明猛烈抨击的亨特，目前正在角逐首相大位。另一名首相角逐者，是前任伦敦市长约翰逊（Boris Johnson），他也同样对香港人表达了支持。而英国现任首相梅伊在二十国集团（G20）大阪峰会上，已经直接向中国领导人提出“香港的高度自治和《中英联合声明》中规定的各项权利和自由能得到保障，这一点至关重要”。
 </p>
 <p>
  香港作家赵耀景认为，现在有40%的外商正在撤离大陆，国内的消费疲软，如果英国加入美国对中共进行经济，或人权方面的制裁，中共的日子将会更加难熬。
 </p>
 <p>
  采访/常春 编辑/尚燕 后制/陈建铭
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102615777.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102615777.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102615777.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/04/a102615777.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

