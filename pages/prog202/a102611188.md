### 川普否定休战说 称川习会将是“刺激的一天”
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1152459818.jpg" target="_blank">
  <figure>
   <img alt="川普否定休战说 称川习会将是“刺激的一天”" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1152459818-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  图为28日的G20开幕式上，习近平带翻译与川普握手寒暄。(LUDOVIC MARIN/AFP/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月28日讯】川习会被认为是日本G20峰会上的重头戏。出席峰会的美国总统
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  （
  <span href="https://www.ntdtv.com/gb/特朗普.htm">
   特朗普
  </span>
  ）公开否定了港媒所谓“美中休战半年”的报导，并称和
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  会晤的周六（29日）将是“刺激的一天”。
 </p>
 <p>
  据《美国之音》报导，G20峰会首日（28日），
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  回答记者提问时明确表示，他没有许诺
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  在半年内不会加征新关税。
 </p>
 <p>
  27日，中资港媒《南华早报》报导称，在川习会前，川普和习近平已经达成了“休战半年”的“初步协议”。不过白宫首席经济顾问库德洛当天就对美媒澄清，美中在川习会前并没有达成任何事前协议。
 </p>
 <p>
  川普28日谈及次日的川习会时表示，他认为会晤将会有成果。但是他又说，“谁知道呢，但我想会有成果，至少会有成果。”他还说，29日将会是“刺激的一天”，“我们将看到会发生什么，会有什么结果。”
 </p>
 <p>
  在出发前往日本前，川普已对美媒透露，他对川习会还抱有希望，但是如果会晤无法促成贸易协议，他将对另外3,000亿美元中国商品加征关税，但是会先加10%，而不是原计划的25%。
 </p>
 <p>
  川普28日还提到，他将和习近平讨论有关华为公司的问题。上个月，美国已将华为列入美国出口管制清单，并祭出严厉的封杀令，令华为经营陷入巨大困境。
 </p>
 <p>
  据外媒报导，在G20开幕式上合影时，领导人们改变了组织者的安排，临时调整了站位，原本应该挨在一起的川普和习近平，最后分站主办方两侧，隔开了一段距离。美方公布的川普29日活动时间表显示，他当日将马不停蹄地与多国领导人会晤，给习近平的时间只有短短一个半小时。
 </p>
 <p>
  尽管川普本人一再表明对川习会所抱的乐观态度，但外界多认为此次会晤难有实质进展。路透社24日引述白宫一位高级官员表示，川普只把这次川习会看作“保持接触的机会”，以了解中方在贸易战中的立场，并“乐于接受任何结果”。
 </p>
 <p>
  川习会前，白宫一再表明强硬立场，除非中共撤回5月的“悔棋”举动，否则贸易战就将继续。而北京的态度忽软忽硬，摇摆不定。中共官媒仍在大力煽动“反美抗战”，但中共外交部发言人28日又强调了“中美通过对话协商来化解分歧”。
 </p>
 <p>
  据《美国之音》报导，习近平28日在日本会晤外国领导人时，不点名地重批“某些发达国家”的“保护主义措施”。但新华网当日发表的习近平讲话“全文”中，这种批评又被努力淡化，只在第一段一笔带过。
 </p>
 <p>
  （记者谷珄综合报导/责任编辑：明轩）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102611188.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102611188.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102611188.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/28/a102611188.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

