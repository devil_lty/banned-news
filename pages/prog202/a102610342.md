### 习近平访日仅吃工作餐？尴尬定位“永远的邻国”
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/gettyimages-1152248139-594x594.jpg" target="_blank">
  <figure>
   <img alt="习近平访日仅吃工作餐？尴尬定位“永远的邻国”" src="https://i.ntdtv.com/assets/uploads/2019/06/gettyimages-1152248139-594x594-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  6月27日，习近平抵达日本，一下飞机就受到大雨“洗礼”。（KAZUHIRO NOGI/AFP/Getty Images）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月27日讯】
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  今天（27日）中午抵达日本，他将与日本首相安倍晋三在傍晚举行会晤。日本取消了此前计划接待习近平的“国宾”待遇，改以“日中峰会”工作晚宴招待习。日媒报导称，这次领袖会谈，日中关系将定位为“永远的邻国”。
 </p>
 <p>
  日本《朝日新闻》6月27日报导，
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  当天中午搭乘专机抵达日本，参加28日起在大阪举行的G20首脑峰会。
 </p>
 <p>
  从网上传出的现场照片可见，习近平此次访日似乎运气不佳，一下飞机就受到大雨“洗礼”，日本方面所谓的隆重接待，在一片密密麻麻的雨伞中，不见丝毫踪影。习近平则匆匆坐进了专车，离开机场。
 </p>
 <p>
  报导称，预定27日傍晚，日本首相安倍晋三将与习近平举行会谈，双方会谈后会公布一份类似共识的内容，包括双方为避免关系再度恶化，将日中两国关系定位为“永远的邻国”。
 </p>
 <p>
  共识还确认两国领导人等高层将持续往来互访，并提到习近平预定于明年春天再次访日。
 </p>
 <p>
  据台湾联合早报透露，此次日中峰会，日本方面为习近平准备了“日中峰会”工作晚宴。而在此之前，日本政府原本计划以“国宾”待遇接待习近平，但最终被取消。
 </p>
 <p>
  日本共同社曾引述外务省消息人士称，G20期间非常忙碌，不可能将习近平作为国宾接待。
 </p>
 <p>
  据了解，日本接待外国政要有5个礼遇级别，国宾规格是日本以最高礼仪接待外国元首与准元首等的礼仪，由内阁会议决定，作为王室的国宾接待。
 </p>
 <p>
  在此次
  <span href="https://www.ntdtv.com/gb/习近平访日.htm">
   习近平访日
  </span>
  之前，美国总统
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  （特朗普）于5月25日访问日本，作为日本“令和”时代的首位国宾。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  和夫人梅拉尼娅受到日本德仁天王及王后的热情款待，天王夫妇在日本王宫宫殿举行宫中晚宴招待川普。
 </p>
 <p>
  此外，法国总统
  <span href="https://www.ntdtv.com/gb/马克龙.htm">
   马克龙
  </span>
  及夫人布丽吉特26日已抵达日本，日本政府也以“国宾”待遇接待马克龙。
 </p>
 <p>
  27日，日本德仁天王及王后在东京王宫会见
  <span href="https://www.ntdtv.com/gb/马克龙.htm">
   马克龙
  </span>
  夫妇，并举办隆重的宫中宴会招待。
 </p>
 <p>
  相比之下，习近平此次访日的待遇似乎有些惨，既比不上川普，也不及马克龙。
 </p>
 <p>
  此外，有消息称，受香港反送中运动的影响，出席G20峰会的多位首脑包括川普都将向北京施压，香港以及多个维权团体已前往大阪抗议中共，北京方面强烈要求日本政府做好习近平的维安工作，以维护习的政治尊严。
 </p>
 <p>
  （记者罗婷婷报导/责任编辑：文慧）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102610342.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102610342.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102610342.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/27/a102610342.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

