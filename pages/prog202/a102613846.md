### 暴雨破10年纪录 孟买水淹墙倒逾20死66伤
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1153080777.jpg" target="_blank">
  <figure>
   <img alt="暴雨破10年纪录 孟买水淹墙倒逾20死66伤" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1153080777-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  孟买连2天遭暴雨侵袭，道路淹水成河。(PUNIT PARANJPE / AFP)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月02日讯】印度金融之都
  <span href="https://www.ntdtv.com/gb/孟买.htm">
   孟买
  </span>
  连2天遭
  <span href="https://www.ntdtv.com/gb/暴雨.htm">
   暴雨
  </span>
  侵袭，一夜之间累积约100毫米雨量，造成低洼地区被水淹没。今(2日)凌晨2处围墙因禁不住豪雨而倒塌，造成逾20人死亡，包括一名3岁男童，另有66人受伤。
 </p>
 <p>
  倒塌事故其中一件发生于当地时间凌晨2时，位于Tanaji Kamble营区的收容贫民所，造成13人死亡，4人重伤，另有3至5人被困瓦砾下。国家救难队随即抵达现场，并出动搜救犬搜索可能还压在墙底下的受困者。
 </p>
 <p>
  另一件倒塌事故发生在近郊的塔纳市（Thane）因大雨冲刷，让学校外墙倒塌，压垮周围2间房屋，造成包含一名3岁男童在内共3人死亡。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/孟买.htm">
   孟买
  </span>
  市府官员表示，从6月30日晚间开始，当地已降下540毫米的雨量，打破10年来的纪录，令人口约2000万的孟买这座城市，灾情不断，陷入停滞状态。
 </p>
 <blockquote class="twitter-tweet" data-lang="zh-tw">
  <p dir="ltr" lang="en">
   Sion to Matunga route railway track has been waterlogged. all trains have been stopped. people are going through the water logged tracks and Police as well as fire brigade has come to help them.
   <span href="https://twitter.com/MumbaiPolice?ref_src=twsrc%5Etfw">
    @MumbaiPolice
   </span>
   <span href="https://twitter.com/hashtag/MumbaiRainsLive?src=hash&amp;ref_src=twsrc%5Etfw">
    #MumbaiRainsLive
   </span>
   <span href="https://twitter.com/hashtag/MumbaiRainsLiveUpdates?src=hash&amp;ref_src=twsrc%5Etfw">
    #MumbaiRainsLiveUpdates
   </span>
   <span href="https://twitter.com/hashtag/Mumbai?src=hash&amp;ref_src=twsrc%5Etfw">
    #Mumbai
   </span>
   <span href="https://twitter.com/hashtag/MumbaiRain?src=hash&amp;ref_src=twsrc%5Etfw">
    #MumbaiRain
   </span>
   <span href="https://t.co/WDLXd0Lx7X">
    pic.twitter.com/WDLXd0Lx7X
   </span>
  </p>
  <p>
   — DINESH SHARMA (@dinujournalist)
   <span href="https://twitter.com/dinujournalist/status/1145872974097948672?ref_src=twsrc%5Etfw">
    2019年7月2日
   </span>
  </p>
 </blockquote>
 <p>
  <script async="" charset="utf-8" src="https://platform.twitter.com/widgets.js">
  </script>
 </p>
 <p>
  当局宣布2日停班停课，建议民众待在室内，各学校机关皆关闭。
 </p>
 <p>
  空中交通方面，2日凌晨还有一架飞机在孟买降落时不慎冲出跑道，所幸未造成人员伤亡，随后飞往孟买的各航班被转往其他机场。
 </p>
 <p>
  铁道系统因轨道被水淹而被迫减班或取消。中央铁路局在推特宣布，3列受困在郊区铁路上的乘客，已经全面疏散。另有部分驾驶人则需在淹水的街道上推著抛锚的车辆。
 </p>
 <p>
  气象部门表示，未来3天孟买与近郊地区仍须严防豪大雨，居民必须做好淹水的准备。
 </p>
 <p>
 </p>
 <p>
  (责任编辑：卢勇信)
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102613846.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102613846.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102613846.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/02/a102613846.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

