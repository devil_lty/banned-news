### 川普：美中谈判已从“破裂处”恢复 华为尚未解禁
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1159007422.jpg" target="_blank">
  <figure>
   <img alt="川普：美中谈判已从“破裂处”恢复 华为尚未解禁" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1159007422-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  图为川普29日在大阪新闻发布会上答记者问。 (Tomohiro Ohsumi/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月29日讯】
  <span href="https://www.ntdtv.com/gb/g20川习会.htm">
   G20川习会
  </span>
  确定
  <span href="https://www.ntdtv.com/gb/34765.htm">
   美中贸易谈判
  </span>
  重启。
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  事后透露，中方同意从“破裂的地方”恢复谈判，美方将再次尝试能否达成协议。他不会取消原有关税，暂时也不会加征新关税。另外川普确认，华为禁令并未完全撤销。
 </p>
 <p>
  据《华盛顿时报》报导，29日川习会后在日本大阪召开的记者会上，
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  表示，他与
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  进行了非常好的会谈，美中谈判将“重回正轨”。他说，习近平同意从上次“破裂的地方（where we left off）”恢复谈判，并设置适当的防护措施（with guardrails in place）。
 </p>
 <p>
  “我可以告诉你，他们（中方）希望达成协议，”川普说，“如果协议达成的话，这将是一个历史性事件。”
 </p>
 <p>
  白宫此前一再强调，美方立场不会改变，重回谈判的条件是中共撤回5月的“悔棋”举动，重返谈判破裂前的协议。4月时，双方本已接近达成协议，中方答应解决美中贸易不平衡和保护知识产权等一系列美方关切的问题，但5月初北京突然撤回了多项关键承诺，令谈判破裂，贸易战再次升级。
 </p>
 <p>
  川习会前，中共官方一直开足马力宣传“反美抗战”，誓言“绝不妥协”。有评论人士指，北京又回到谈判桌，或显示其将继续拖延战术。
 </p>
 <p>
  《大纪元时报》报导，川普在记者会上还提到，美国“至少在目前”不会对3250亿美元中国商品加征关税，双方将再次谈判，看看能否达成协议。他补充说，如果无法取得协议，美国将重返关税，“如果我想要加关税的话，我有能力再加上”。
 </p>
 <p>
  川普还说，
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  已承诺会购买大量的食品和农产品，并将很快开始行动，“几乎是立即的”，美国农民将会直接受益。他表示，美方会向中方提供希望采购的项目清单。
 </p>
 <p>
  “我不着急达成协议，我要达成一个好的协议，一个正确的协议，”川普说，“如果达成协议，对双方是有益的。”
 </p>
 <p>
  他还透露，习近平没有和他提到孟晚舟的案子，但双方有讨论华为的问题。
 </p>
 <p>
  据阿波罗网报导，川普并未确认要把华为从商务部的出口管制黑名单中去除，只是允许美国公司向华为出售不涉及“重大国家安全问题”的产品。他说，国家安全问题很重要。
 </p>
 <p>
  川普还强调，他并没有说允许向华为出售技术，虽然有媒体这样报导。
 </p>
 <p>
  川普透露，他的政府不久将就华为问题开会，但不会立即完全解决这个复杂问题，而是将其留到最后，“我们必须把它留在（贸易谈判的）非常最后阶段去解决”。
 </p>
 <p>
  美中贸易战开战以来，一直这样打打停停，但是关税一再升级。持续存在的关税威胁和不断升级的美中对抗，迫使大部分外企都已经或准备撤离中国，以规避风险，这正在对中共经济体系造成致命的打击。川普和白宫官员都表示，中共最终将会接受美方的贸易协议。
 </p>
 <p>
  （记者谷珄综合报导/责任编辑：云涛）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102611880.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102611880.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102611880.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/29/a102611880.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

