### 中共罕见海上动作 马六甲海峡安保提升最高级
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-836198326.jpg" target="_blank">
  <figure>
   <img alt="中共罕见海上动作 马六甲海峡安保提升最高级" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-836198326-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  7月3日，中共交通运输部办公厅发布通知，将航行于及准备驶往马六甲海峡的中国籍船舶的保安等级提升为最高级别。图为马六甲海峡。（Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月03日讯】7月3日，
  <span href="https://www.ntdtv.com/gb/中共交通运输部.htm">
   中共交通运输部
  </span>
  办公厅发布
  <span href="https://www.ntdtv.com/gb/通知.htm">
   通知
  </span>
  ，将航行于及准备驶往
  <span href="https://www.ntdtv.com/gb/马六甲海峡.htm">
   马六甲海峡
  </span>
  的中国籍船舶的
  <span href="https://www.ntdtv.com/gb/保安等级.htm">
   保安等级
  </span>
  提升为最高级别，中共这一罕见之举引发外界猜测。
 </p>
 <p>
  中国大陆多家船公司已经确认，收到一份由
  <span href="https://www.ntdtv.com/gb/中共交通运输部.htm">
   中共交通运输部
  </span>
  办公厅发布的《关于将航行于及拟驶往
  <span href="https://www.ntdtv.com/gb/马六甲海峡.htm">
   马六甲海峡
  </span>
  的中国籍国际航行船舶
  <span href="https://www.ntdtv.com/gb/保安等级.htm">
   保安等级
  </span>
  提升为3级的
  <span href="https://www.ntdtv.com/gb/通知.htm">
   通知
  </span>
  》，并已按照该通知采取相关应对措施。
 </p>
 <p>
  这份发给各国际航运公司、各直属海事局的通知称，马六甲海峡安保升级时间，从2019年7月2日22时起，截止时间另行通知。
 </p>
 <p>
  中共国际船舶保安规则中相关规定中可见：传播保安等级从低到高分为3级。
 </p>
 <p>
  保安等级1：是指应当始终保持的最低防范性保安措施的等级。
 </p>
 <p>
  保安等级2：是指由于保安事件危险性升高而应在一段时间内保持适当的附加保护性保安措施的等级。
 </p>
 <p>
  保安等级3：是指当保安事件可能，或者即将发生时，应在一段有限时间内，保持进一步的特殊保护性保安措施的等级。
 </p>
 <p>
  而此次通知中准备把船舶保安等级提升为3级，属于最高级，此前霍尔木兹海峡油轮被袭击后，该地区的保安等级也才被定位2级，因此3级保安等级极为罕见。
 </p>
 <p>
  而此次中共把此海峡保安定为最高级别3级，舆论纷纷质疑，到底发生了什么大事件？
 </p>
 <p>
  <strong>
   马六甲海峡是兵家必争之地
  </strong>
 </p>
 <p>
  马六甲海峡是世界上最繁忙的海峡之一，是中共对外的能源及贸易重要海上通道，目前有80%的进口石油需经马六甲海峡运送，预计到2020年，将有超过85%的进口石油仍需通过马六甲运输。
 </p>
 <p>
  马六甲海峡也是日本、韩国的最主要能源运输通道，被称为是“海上生命线”。一旦发生冲突，马六甲海峡将成为兵家必争之地。
 </p>
 <p>
  而中国与欧洲、非洲及中东地区等地，重要的海运贸易也要经由马六甲航线，这种状况被称为中共的“马六甲困局”。
 </p>
 <p>
  有分析认为，此次中共把马六甲海峡的安保等级，提升到最高级别，或是针对上述窘境。
 </p>
 <p>
  <strong>
   马六甲海峡受美国军力覆盖
  </strong>
 </p>
 <p>
  另外二战结束后，美国在世界多地部署了军事力量以维护地区稳定，其中至今仍有众多国家邀请美国在当地留有军事力量，基于历史原因，马六甲海峡目前受美国的军事力量所覆盖。
 </p>
 <p>
  而美国在位于新加坡的樟宜海军基地部署的军事力量，是对马六甲海域最具掌控力的体现。根据美新协议，樟宜海军基地分担着向美军第7舰队提供后勤补给和维修服务，这里也是美军在海外的又一处航母基地。
 </p>
 <p>
  据报导，美国军方曾表示，如果中美之间发生冲突，美国根本不需要出兵，只要封锁或者控制马六甲海峡，就可以起到让中共终止战争的目的。
 </p>
 <p>
  值得一提的是，目前正值中共在
  <span href="https://www.ntdtv.com/gb/南海军演.htm">
   南海军演
  </span>
  的最后一天。
 </p>
 <p>
  中共海事局网站6月29日刊发三沙海事局发布的一则禁航通知，指定南海一片海域为禁止航行区域，禁止航行的时间是6月29日0时到7月3日午夜为止的5天时间。海事局表示，在此期间内会举行军事演习。
 </p>
 <p>
  据中央社报导，此次禁航划定的是南沙与中沙群岛之间一片2万多平方公里的海域，报导说，中共以前也曾以军事训练为由，在南海海域划定临时禁航区，但几乎都在广东或海南岛近海一带。这次则将范围划定在远离陆地的南沙与中沙群岛之间。
 </p>
 <p>
  有军事学者分析，此次演习区是有史以来距离中国大陆最远的一次，被视为反击美日在南海巡航的军事行动。
 </p>
 <p>
  至于此次中共把马六甲海峡的安保等级，提升到最高级别，是否和本次军演有关，目前还不明确。
 </p>
 <p>
  （记者李韵报导/责任编辑：李泉）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102614687.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102614687.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog202/a102614687.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/03/a102614687.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

