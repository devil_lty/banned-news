### 李文足成功探监 四年来首见丈夫 王全璋性情大变消瘦苍老
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="李文足带着儿子，28日下午到山东临沂监狱探望丈夫王全璋。（推特图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/d-irrayxkae2bau.jpg-large-800x450-800x533-600x400.jpg"/>
 </div>
 <div class="caption">
  李文足带着儿子，28日下午到山东临沂监狱探望丈夫王全璋。（推特图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月28日】
  </span>
  <span class="content-info-type">
   （本台记者岳文骁综合报导）
  </span>
  大陆维权律师王全璋妻子李文足，历经四年“寻夫”，终于在今天（28日）下午获准到与正在山东临沂监狱服刑的王全璋会面，这也是两人近4年来首次见面，令人唏嘘。李文足形容王全璋性情大变，表现很焦躁，显得消瘦苍老，像另一个人。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  李文足与儿子王广微，还有王全璋姊姊王全秀、“709律师”家属之一的王琑岭等人，于今天下午1时50分到达临沂监狱探望王全璋。而这次会面是在世界20国集团（G20）大阪峰会28日登场首日，中共当局破天荒“允许”的。
 </p>
 <p>
  据美国之音报导，李文足会见王全璋后表示，情况很糟糕，让她很难受，更加担忧。
 </p>
 <p>
  王全璋在会见时表情呆滞，担心李文足和儿子的安全，还要她对公安的态度不要太差。李文足忧虑丈夫是因为长期受压及被洗脑，才会出现目前的状态，担心王全璋即使获释也难与外界沟通。
 </p>
 <p>
  李文足指出，虽然王全璋表示他在监狱里情况很好，吃得好，睡得好，但是问他中午吃了什么却记不起来。
 </p>
 <p>
  另据香港电台报导，李文足在会面后形容，王全璋性情大变，表现很焦躁，显得消瘦苍老，令她对王全璋的精神状况更感忧虑。她又指，王全璋与儿子有交谈，谈及读书，又叫儿子要乖，儿子为父亲打气。
 </p>
 <p>
  李文足在推文中说：“王全璋走路正常，声音没变，就是苍老的像老年人。皮肤很黑。要是走在路上，我一下子认不出他了，他始终是焦躁不安的样子。全璋身边坐着一个警察，带着耳麦，全程记录。还有一个拿着摄像机的，对我们一直拍摄。”
 </p>
 <p>
  李文足说，王全璋看了前2次她到监狱探望丈夫的视频，还看了李文足去其他地方的视频，吓坏了。
 </p>
 <p>
  陪同李文足前往临沂监狱的有王峭岭表示，当天监狱外面有许多身份不明的壮汉，手持雨伞遮挡媒体记者在现场拍摄的镜头。网络上传出的视频显示，有人被多名便衣人员按倒在地。美国之音证实，被按倒的人是一名日本记者。
 </p>
 <div>
 </div>
 <p>
  <!--{cke_protected}%3Cscript%20async%20src%3D%22https%3A%2F%2Fplatform.twitter.com%2Fwidgets.js%22%20charset%3D%22utf-8%22%3E%3C%2Fscript%3E-->
 </p>
 <p>
  维权人士野靖环在Twitter发文称，李文足在现场向其他会见者及不明身份人员讲起王全璋，却被看守所门口的警察威胁：“你再说，我就取消你会见。”
 </p>
 <blockquote class="twitter-tweet" data-lang="zh-cn">
  <p dir="ltr" lang="zh">
   【最新消息：李文足已和王全璋见面】
  </p>
  <p>
   李文足周五(28日)下午，成功在山东临沂监狱，会见丈夫王全璋。
  </p>
  <p>
   她接受本台采访表示，会面长约30分钟，期间王全璋表现焦躁，消瘦苍老，“好像变了个人一样”。
  </p>
  <p>
   她进入监狱会见前，现场出现大批身份不明男子，用雨伞遮挡李文足。有日本传媒拍摄时，被便衣按在地上
   <span href="https://t.co/UXhzfs2MPG">
    pic.twitter.com/UXhzfs2MPG
   </span>
  </p>
  <p>
   — 自由亚洲电台 (@RFA_Chinese)
   <span href="https://twitter.com/RFA_Chinese/status/1144534276534202369?ref_src=twsrc%5Etfw">
    2019年6月28日
   </span>
  </p>
 </blockquote>
 <p>
 </p>
 <p>
  王全璋原为北京锋锐律师事务所律师，是最早为法轮功辩护的律师之一。他2007年开始律师执业，曾经代理多起敏感案件。2015年7月被抓捕后失去音信，到2019年1月28日，天津市第二中级法院以“颠覆国家政权罪”，判处王全璋有期徒刑4年6个月。在王全璋与外界隔绝4年来，李文足为夫奔走呼吁，期间曾会见德国总理默克尔（Angela Merkel）及多国外交官。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2993158.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2993158.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2993158.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/28/n2993158.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

