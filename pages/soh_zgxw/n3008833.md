### 李锐妻发表声明：起诉李南央非自己意愿
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="李锐父女 （高瑜推特）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/22-502-600x400.jpg"/>
 </div>
 <div class="caption">
  李锐父女 （高瑜推特）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月4日】
  </span>
  <span class="content-info-type">
   （本台记者刘莹综合报导）
  </span>
  中共元老李锐女儿李南央按李锐生前意愿已将其日记捐赠给美国斯坦福大学胡佛馆，却被继母张玉珍向北京法院控告索要。日前，张玉珍发表书面声明，称与李南央打官司并不是她的个人意愿。对此，李南央做出回应，要求张玉珍撤诉。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  7月4日，自由亚洲电台报导，近日，中共元老李锐的遗孀张玉珍的声明在网络流传，引起外界关注。
 </p>
 <p>
  张玉珍的声明写道：“一，首先声明，与李南央打官司，并不是我的个人意愿。二，事实上也不存在我与李南央争夺李锐遗产之事。我与李锐共同生活四十年，李锐对自己的后事安排，我从无异议，对李锐给他人包括自己女儿东西，我从无关注和反对过。三，我今年已近九十岁了，身体长期多病。李锐刚去世不久，我自己的精神上还没有调整过来，实在无力应付这社会上的各种传言和质疑，我想平平静静度过我的余生，我希望打官司这事不要再来找我了。”
 </p>
 <p>
  声明的落款是张玉珍的签字，日期是6月25日北京西城区法院开庭审理李锐日记索要案当天。
 </p>
 <p>
  对此，李南央说：“这个声明实际上什么作用都没有，因为它不是一个法律文件，并没有改变张玉珍起诉李南央这个案子张玉珍的原告身份，李南央和胡佛研究所的被告身份也没有改变，这个案子还继续存在着共产党领导的法院之中。还有她的声明中的‘不是我的个人意愿’，这个都被大家解读为揭发了她背后的党组织，但是她并没有明确的说这是谁的意愿。”
 </p>
 <p>
  另外，对于张玉珍的声明，李南央表明自己的态度。她说：“我的意思是，她在还没有撤诉的情况下，在她的律师正在为她出庭的时候，她发表这样一篇声明，是对自己的律师毫无诚信。其实最直接的是她撤诉，她撤了诉，她就不是原告了，李南央也就不是被告了，斯坦福大学胡佛研究所也就不是被告了，这是让人能够相信张玉珍不是她个人意愿的唯一的做法。”
 </p>
 <p>
  张玉珍的声明首先在微信上流传，引起许多人的关注。著名记者高瑜在推特上写道：“被组织胁迫当原告的李锐遗孀，90高龄的张玉珍女士，也受到舆论关注多日，身心不堪疲惫，可想而知。不得不写下排除外界干扰的声明，在情在理。只是不知组织岂可放过她。”
 </p>
 <p>
  另一位网名“酥迷”的网友说：“一点也不可怜。不是本意干嘛要答应。撤诉啊。”
 </p>
 <p>
  此前，据美国之音报导，6月25日上午，张玉珍索要《李锐日记》一案在西城区中级法开庭审理，但原告张玉珍并未现身，只有她的委任律师张金澎出庭。
 </p>
 <div>
 </div>
 <p>
  而在美国旧金山居住的李南央，除质疑法院不公开审理的用意外，并强调李锐生前多次明确表示，日记和手稿交给斯坦福大学胡佛研究所保存，并有录音为证。而在张玉珍提出控告后，她已向法院出示李锐的录音作为证物。
 </p>
 <p>
  李南央表示，北京法院对李锐决定将日记捐赠给斯坦福一案并没有管辖权。
 </p>
 <p>
  李南央近期曾在网上披露，胡佛研究所就张玉珍向北京西城区法院起诉索要《李锐日记》一案，向美国法院提出反诉。
 </p>
 <p>
  她表示，“斯坦佛大学在诉讼中认定，李锐的捐赠物现在美国，对胡佛的捐赠手续是李南央在李锐去世之前完成的，故对李锐捐赠物所有权的争议，应该由捐赠物实际所在地的美国法院作出裁决。”
 </p>
 <p>
  外界认为，张玉珍向北京西城区法院起诉索要《李锐日记》一案是中共官方在背后操控。
 </p>
 <p>
  李南央指出，中共方面为了索要《李锐日记》费尽了心思，多次指使李南央的亲属索要，并向李南央转寄北京法院的法律文书以及派人与胡佛研究所交涉。
 </p>
 <p>
  李锐曾任中共党魁毛泽东的秘书，今年2月16日在北京病逝，享寿101岁。之后，其遗孀张玉珍4月2日向北京西城区法院起诉李锐的女儿李南央，认为李南央擅自处分李锐日记等文稿原件，要求由其本人继承。
 </p>
 <p>
  《李锐日记》被认为是与中共官方党史不同的另一部真实党史。李南央认为中共索要《李锐日记》是要将其销毁。
 </p>
 <p>
  对于张玉珍索要《李锐日记》一案，资深媒体人高瑜评论表示，这并不是一起普通的民事官司，而是中共官方操纵的案件，妄图夺取李锐的日记等文稿，是对人权的践踏，对言论的封杀。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3008833.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3008833.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3008833.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/04/n3008833.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

