### 罕见获许参访新疆“再教育营”  他发现令人吃惊的细节
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="新疆“思想转化营”中的学员，眼圈处明显有被殴打过的痕迹（视频截图）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/xinjiang-800x533-600x400.jpg"/>
 </div>
 <div class="caption">
  新疆“思想转化营”中的学员，眼圈处明显有被殴打过的痕迹（视频截图）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月19日】
  </span>
  <span class="content-info-type">
   （本台记者蕭晴综合报导）
  </span>
  日前，英国《BBC》罕见获得许可，进入中共新疆“再教育营”进行实地访问。中共当局试图借此澄清，这些设施是“培训学校”而非“集中营”或“监狱”。但BBC记者表示，随着越是独立深入的访问、探查，越是觉得事情不对劲，所得到的疑问亦远比答案多。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  BBC记者沙雷（John Sudworth）近日获许进入中国西部新疆地区的多个戒备森严的设施。中共当局一直坚称这些设施是“培训学校”，而非外界所指的“再教育集中营”或洗脑基地。
 </p>
 <p>
  沙雷表示，在这个横跨整个新疆的庞大网络中，羁押着数以十万计的维族人和哈萨克人，但在他采访期间，这些人却出奇地“被消失了”。
 </p>
 <h4>
  <strong>
   亲临新疆“培训学校”专访 引发更多质疑
  </strong>
 </h4>
 <p>
  在BBC于6月19日的采访视频中，我们可留意到如下几个片段：
 </p>
 <p>
  记者询问：“你被判有罪吗？”学员回应：“我没有犯罪，我就是犯了错。”
 </p>
 <p>
  记者询问：“你在这多久可以礼拜一次？”学员用生硬的套话回应：“国家（中共）法律规定，学校是公共场所，不能从事宗教活动。”
 </p>
 <p>
  随后，记者被引入一间教室，里面被关押的穆斯林正“载歌载舞”。记者询问：“是你自己选择过来的吗？”该男学员虽然回应“就是”，但视频中明显可见，他的右眼眶附近有被殴打过的瘀痕。
 </p>
 <p>
  在BBC访问“培训学校”的全程中，中共政府官员一直在记者背后，监视其每一次采访。但尽管如此，记者还是在洗漱间的隐蔽处发现了一些涂鸦，上面写道“我心碎欲裂”等。
 </p>
 <p>
  此外，当局宣称这些学员需要接受“职业技能培训”，而培训的内容却只有“铺床叠被、打扫卫生”，更匪夷所思的是，这样简单的动作竟需要“培训”长达2-4个月。
 </p>
 <div>
 </div>
 <p>
  当局还宣称，这些学员“每人每周都可以（轮流）回家一晚”。但当BBC记者当晚准时（当局规定的离校回家时间）站在“培训学校”大楼外等待离校学员时，却不见一人回家。
 </p>
 <p>
  记者向当局相关人员提出质疑：“为什么我们没有看到任何学生离开，甚至准备离开？”得到的回复却是，“有的天有回家的，有的（天）没有回家的”。但此前记者曾被明确告知，“每天都有人回家”。
 </p>
 <p>
  BBC记者说，在这些“培训学校”中，中共具体实施“转化思想”的方式包括：长时间死记硬背汉语；学习中共日益收紧的宗教法律法规；用效忠共产党的口号，改变穆斯林原来的宗教信仰和文化等。
 </p>
 <p>
  此外，记者还发现，学员们在每晚天黑后很久，仍在读著那些中共用来“转化思想”的教材……
 </p>
 <h4>
  <strong>
   外媒实地采访前 中共对“再教育营”做了改建
  </strong>
 </h4>
 <p>
  视频中通过卫星照片的对比，BBC记者发现，他们被安排参观的场地，在此前不久被改建过。
 </p>
 <p>
  此前的营区在新疆多地被兴建，它们通常被高墙环绕，墙外有铁丝网和瞭望塔。但在BBC记者允许参观的营区，卫星图片显示，内部的安全网及似瞭望塔结构的物体，在不久前被移除；而此前空旷的操场，也被改为各种球类运动场地，并在记者访问期间被安排使用，以此营造所谓“和谐”气氛。
 </p>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_2971942">
  <img alt="" class="size-medium wp-image-2971942" src="http://img.soundofhope.org/2019/06/caochang-1-600x345.jpg" srcset="http://img.soundofhope.org/2019/06/caochang-1-600x345.jpg 600w, http://img.soundofhope.org/2019/06/caochang-1-768x442.jpg 768w, http://img.soundofhope.org/2019/06/caochang-1-180x104.jpg 180w, http://img.soundofhope.org/2019/06/caochang-1-366x211.jpg 366w, http://img.soundofhope.org/2019/06/caochang-1.jpg 813w">
   <br/><figcaption class="wp-caption-text">
    空旷的操场在BBC记者访问前不久，被改建为各种球类运动场（视频截图/希望之声合成）
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  记者质疑，他被当局安排访问的营区，是被用来“展示”的营区；而隔壁仍高墙矗立、墙外可见铁丝网和瞭望塔的处所，才是中共对穆斯林进行暴力洗脑的罪恶场所，然而，记者在刚要靠近时，就被勒令禁止入内。沙雷说，这些地方“看上去更不像学校”。
 </p>
 <h4>
  <strong>
   “歌舞升平”的背后
  </strong>
 </h4>
 <p>
  随后，BBC记者对现居哈萨克斯坦的Rakhima Senbay进行了一次专访。
 </p>
 <p>
  Rakhima Senbay女士曾被中共关押在新疆“再教育营”一年多时间，她被关押的理由，只是因其手机里安装了Whatsapp聊天软件。
 </p>
 <p>
  她告诉BBC，只有在她将要被放出的时候，才“有幸”被关到上述用于“展示”的营地（即BBC记者参访的“培训学校”），而其它更多的时间里，那些维族和哈萨克族穆斯林人，则都被关押在更加严厉且环境恶劣的集中营里。
 </p>
 <p>
  Rakhima Senbay还透露：“我被戴上脚链超过一周，我们被殴打过几次；有一次，我被电棍击打。”
 </p>
 <p>
  而上述BBC记者参访时看到的情景，比如，“开心的”穆斯林、“努力地”学习汉语，甚至在唱歌、跳舞等，Rakhima Senbay回应说：“我在营地里亲身经历过，他们（当局）在外人访问前警告我们，如果你们说出来，你们会被送到一个更差的地方。所以每个人都很害怕，（不得不）按照他们说的去做，包括唱歌和跳舞。”
 </p>
 <p>
  记者沙雷也认为，中共所有让他看到的场景，只不过“是它（中共）想让世界看到的……，以此来证明这些人并非‘囚犯’，而是‘学员’”，并自愿接受当局的所谓“再教育”。
 </p>
 <p>
  沙雷表示，当他们独立收集到的证据越多，所产生的疑问也就越多……
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2971897.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2971897.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2971897.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/19/n2971897.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

