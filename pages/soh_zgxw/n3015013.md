### 半年内335名法轮功学员被非法判刑 89岁老人也入狱
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="香港，法轮功学员反迫害大游行。（希望之声）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/01/flg-2-600x400.jpg"/>
 </div>
 <div class="caption">
  香港，法轮功学员反迫害大游行。（希望之声）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月7日】
  </span>
  <span class="content-info-type">
   （博谈网  作者：苏智敏）
  </span>
  近年来，中共当局不断加大对信仰群体的打击力度，被迫害近20年的法轮功也在其中。据明慧网统计，2019年上半年至少有335名法轮功学员被中共非法判刑，非法庭审356场，刑期最长12年。其中，至少有60名65岁以上的老年法轮功学员被非法判刑。中共公检法司部门还对149名法轮功学员非法敲诈勒索高达189万元。
 </p>
 <h4>
  <strong>
   80岁以上有7位
  </strong>
 </h4>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  信仰“真善忍”的法轮功学员，尽管对社会无害，却在1999年被时任中共领导人江泽民下令镇压，一句“名誉上搞臭，经济上搞垮，肉体上消灭”的指令，使法轮功至今在中国仍受到残酷镇压。
 </p>
 <p>
  被非法抓捕的法轮功学员，一向受到非法庭审及判刑。政法委与为迫害法轮功而设立的610办公室操纵公检法司人员透过打压律师、干扰阻止律师阅卷、造假证据、违背司法程序、不通知律师和家属秘密开庭等手段制造冤假错案。
 </p>
 <p>
  海外独立媒体明慧网7月3日指出，在今年上半年，中共当局对至少335名法轮功学员非法判刑，非法庭审356场，有律师为法轮功学员做无罪辩护的仅有95场。而非法判刑最严重地区为：山东68人、黑龙江42人、辽宁35人、吉林25人、江苏24人、河北20人。
 </p>
 <p>
  另有60名65岁以上的老年法轮功学员被非法判刑，有4人刑期最重为8至10年，其中3人来自黑龙江哈尔滨。山西太原的67岁王素平和来自哈尔滨的65岁宋玉芝被重判10年。同样来自哈尔滨的73岁张洪珠、73岁武桂芝分别被重判9、8年。
 </p>
 <p>
  而80岁及80岁以上被非法判刑的有7位：年龄最大者为89岁的张新伟，四川人，被判3年；7人中，贵州省81岁赵英翠被判最重，判刑5年半。
 </p>
 <p>
  由于中国监狱环境恶劣，餐点品质差，常传出使用腐败食材的消息，对法轮功学员这类良心犯，不论年龄大小更是普遍存在虐待与酷刑。因此中共掌控的法院对这些老年法轮功学员判刑，都像是在间接宣布死刑。日前刚宣判中共当局强摘器官证据确凿的独立人民法庭，在其结论中指出：“中共随便对待法轮功学员，不管他们的死活。”
 </p>
 <h4>
  <strong>
   为捞政治资本而下手
  </strong>
 </h4>
 <p>
  明慧网报导指出，一些党委书记为了捞政治资本而积极策划、操控迫害法轮功学员。今年上半年非法判刑最严重地区的第一名山东省，就有这样的案例。
 </p>
 <p>
  山东省沂南县政法委将两起非法抓捕法轮功学员的案子，指示沂南公检法炮制成一起所谓的“大案”，以完成“扫黑”指标。沂南县副县长、公安局长刘星，也因此被省、市奖励其“扫黑”成果，登上山东电视台。
 </p>
 <div>
 </div>
 <p>
  这起被炮制出的“大案”更存在荒唐的非法行径。报导称，今年1月24日沂南县公检法人员在异地——临沂市河东区看守所内非法组成一个所谓的“法庭”，仓促对4名法轮功学员进行“庭审”。3月27日沂南法院进行秘密判决，还规定判决书不允许其他人看，不允许拍照。
 </p>
 <h4>
  <strong>
   共产党的法院与法官
  </strong>
 </h4>
 <p>
  在非法审判中，也存在法律素养差、被立案调查的法官。报导介绍，黑龙江宾县现年39岁的法轮功学员王晓荣，去年11月9日早晨在家中被突然闯进来的警察抓走，关押在哈尔滨市第二看守所。今年4月16日王晓荣的丈夫接到依兰法院电话，通知隔天17日开庭。
 </p>
 <p>
  王晓荣的丈夫在急忙中，联系上一名北京律师，律师赶到当地连夜看卷，隔天下午出庭辩护。律师要求法庭依法给他10天的时间阅卷，并书面申请法官张安克回避，因为张安克正因“黑恶势力的保护伞”问题被省纪检立案调查。但张安克法官拒回避，并称“这是共产党的法院”。
 </p>
 <p>
  当律师称王晓荣修炼真善忍不违法时，张安克像触电一样急喊：“在这里不准提这个（真善忍）”。律师又指王晓荣完全没有任何物证证明有罪，起诉书仅称别人指认她有罪，可是法庭是执法部门，律师反问：“难道连‘孤证不能定罪’都不懂吗？”对此，张安克恼羞成怒吼道：“你爱开（庭）不开，不开就出去。”
 </p>
 <h4>
  <strong>
   半年敲诈勒索近2百万
  </strong>
 </h4>
 <p>
  明慧网多年来的报导指出，中共对法轮功的迫害也普遍存在各种名为交罚金，实为敲诈勒索的经济迫害。所谓罚金，其实是中共法院敲诈勒索，藉以加重迫害，是中共打压良心犯的罪证之一。
 </p>
 <p>
  光是今年上半年，中共法院与公安就对149名法轮功学员非法敲诈勒索1,894,320元。其中，法院非法罚金1,747,000，警察抢劫勒索147,320元。
 </p>
 <p>
  其中，原本是宁夏劳动人事厅教育中心副主任的栾凝，因不放弃信仰，先后3次被非法抓捕、4次被非法抄家、1次被抓到洗脑班拘禁、2次非法判刑入狱，并被单位开除失去养老金等社会保障，更在狱中遭受多种酷刑折磨。
 </p>
 <p>
  栾凝在今年因寄发讲述法轮功受迫害的真相信，被宁夏银川市中级法院于4月16日非法判刑10年，罚金10万，为今年遭敲诈勒索的法轮功学员中，金额最高者。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    岳文骁
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3015013.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3015013.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3015013.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/07/n3015013.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

