### 中纪委罕见呼吁官员炒股表“爱国” 灰黑收入洗白？
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="图为人民币砌成的墙。（网络图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/rmb1-800x533-600x400.jpg"/>
 </div>
 <div class="caption">
  图为人民币砌成的墙。（网络图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月26日】
  </span>
  <span class="content-info-type">
   （本台记者岳文骁综合报导）
  </span>
  中共原规定县处级以上官员不准买卖股票，即使早些年已有松动，但官员炒股仍未高调进行。中共中纪委日前罕见发文，指“党员干部”投资证券市场是支持国家建设。同期官方又发文盘点反腐中拿下的几名涉内幕交易炒股获暴利的高官。在中国股市低迷、前景黯淡之际，当局却鼓励官场人士去进行投资，其意图显而易见。有评论嘲讽这是方便把那些灰色的、黑色的收入洗成白色的？
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  中共中纪委国家监委网站6月24日发文称：“证券市场是我国社会主义市场经济体系的重要组成部分，党政机关工作人员将其合法财产以合法的方式投资于证券市场，是对国家建设的支持。”
 </p>
 <p>
  而据1993年10月5日中共中央发布的所谓“关于反腐败斗争近期抓好几项工作的决定”。其中第一条第3项规定：党政机关县(处)级以上领导干部不准买卖股票。
 </p>
 <p>
  从2001年开始，官员炒股有所松动，当年4月官方决定有限制地放宽对党政机关县（处）级以上领导干部买卖股票的规定，并出台了“关于党政机关工作人员个人证券投资行为若干规定”。但如今由掌管反腐的中纪委直接公开呼吁党员干部炒股，还与“爱国”挂钩，变化之大、之快令人咂舌。
 </p>
 <p>
  据苹果日报报导称，以镰刀、铁锤作为党旗、党徽标志的中共容许党员炒股并称是对国家建设支持。中共中央纪委国家监委网站并发文，容许党员炒股。
 </p>
 <p>
  中纪委的文章也罗列4类人群不能炒股：
 </p>
 <p>
  1.上市公司的主管部门以及上市公司的国有控股单位的主管部门中掌握内幕讯息的人员及其父母、配偶、子女及其配偶，不准买卖上述主管部门所管理的上市公司的股票。
 </p>
 <p>
  2.国务院证券监督管理机构及其派出机构、证券交易所和期货交易所的工作人员及其父母、配偶、子女及其配偶，不准买卖股票。
 </p>
 <p>
  3.父母、配偶、子女及其配偶在证券公司、基金管理公司任职的，或者在由国务院证券监督管理机构授予证券期货从业资格的会计（审计）师事务所、律师事务所、投资咨询机构、资产评估机构、资信评估机构任职的，该党政机关工作人员不得买卖与上述机构有业务关系的上市公司的股票。
 </p>
 <div>
 </div>
 <p>
  4.掌握内幕讯息的党政机关工作人员，在离开岗位3个月内，继续受该规定的约束。由于新任职务而掌握内幕讯息的党政机关工作人员，在任职前已持有的股票和证券投资基金必须在任职后1个月内作出处理，不得继续持有。
 </p>
 <p>
  此外，中纪委6月24日还另外发文画了7红线，警告党员勿借收受股份贪污。
 </p>
 <p>
  文章点名了有炒股方面出事的8名省部级官员，分别是四川省原副省长李成云、山东省原副省长季缃绮、证监会原副主席姚刚、国家安全部原副部长马建、安徽省原副省长陈树隆、安徽省原副省长周春雨、贵州省原副省长王晓光、内蒙古自治区原副主席白向群。
 </p>
 <p>
  这8名部级官员7人已宣判，其中马建和陈树隆获无期徒刑。此外8人中有6人涉及“内幕交易”，其中周春雨通过“内幕交易”获利最多，达3.5亿，而姚刚获利210万。
 </p>
 <p>
  中国大陆股市长期低迷，表现不佳有目共睹。中共现时呼吁官场以“爱国”为由炒股，引发网民、股民热议。有股民直指，是“韭菜不够用了，需要新韭菜入场被收割”；也有的说，“公务员大约有9000万人，若是平均每人拿出5万元投资A股，那么市场将迎来4万多亿元的增量资金，这对于市场来说明显的利好啊。”
 </p>
 <p>
  自由亚洲电台6月25日引述湖北股民周先生说，最近几年在股市亏损数百万元。他表示大部分投资股票的民众，目前亏损严重，而买卖股票的人愈来愈少，政府希望公务员加入股民的行列：“现在是不是股市没有资金进去啦，那些投资机构全都有问题，股市是不是要崩盘了。所以要那些党员干部把钱送进去，把股市托住。”
 </p>
 <p>
  贵州一名不愿透露姓名的大学讲师表示，在中国股市低迷、前景黯淡之际，政府却鼓励公务员去进行投资，其意图显而易见。他嘲讽道：“这样好啊，有利拉动股市。另外，可以把那些灰色的、黑色的收入洗成白色的。”
 </p>
 <p>
  湖南居民孙女士说，当前买股票很难赚钱：“加上现在物价飞涨，更加没有钱。现在只有公务员可能才有一点钱，所以国家就要求公务员来炒股。贸易战造成国内经济严重下滑，国库也没有钱。现在有一点钱的就是公务员了。”
 </p>
 <p>
  中国大陆知名经济学家吴敬琏曾指出，大陆股市很像一个赌场，而且还是一个没有规矩的赌场——一个有人可以看别人底牌的赌场。可能会形成一种羊群效应，造成更多的悲剧。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2989204.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2989204.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2989204.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/26/n2989204.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

