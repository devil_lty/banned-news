### 中国研发非洲猪瘟疫苗 价值10亿 竟是笔误？
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="非洲猪瘟在中国不断扩散（Pixabay图库）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/01/576023227423-600x398.jpg"/>
 </div>
 <div class="caption">
  非洲猪瘟在中国不断扩散（Pixabay图库）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月18日】
  </span>
  <span class="content-info-type">
   （本台记者韩梅综合报导）
  </span>
  广东海印集团股份有限公司（简称海印股份）前段时间因为研发出非洲猪瘟疫苗而名声大噪。但公司昨天（6月17日）发布公告称，此前将非洲猪瘟防治“注射液”错写成“疫苗”，非洲猪瘟“疫苗”系笔误。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  该公司先是在6月11日晚间公告称，拟与许启太、海南今珠农业发展有限公司签署《合作合同》，涉及金额约9亿元（人民币，下同），许启太教授及其团队成功研制了“今珠多糖注射液”并拥有专利权（含专利申请权），可以实现对非洲猪瘟不低于92%有效率的预防；海印股份拟斥资1亿元为非洲猪瘟防治疫苗投产做准备。
 </p>
 <p>
  这一消息迅速吸引了资本的关注。海印股份次日一字涨停；13日再以6.03%的涨幅收盘，盘中放出巨量成交，换手率也接近8%，两天股价大涨16.78%，市值两天大涨逾10亿元。
 </p>
 <p>
  但随即，公告中提到的信息受到质疑。国内媒体深挖后发现，号称防治猪瘟疫苗的“药”并未获得专利备案，药监局官网也没有相关信息。研发团队带头人是一位槟榔专家，另有市场观点指出，标的资产可能有“皮包公司”嫌疑。
 </p>
 <p>
  6月13日，深圳证券交易所向海印股份发出关注函，让其说明合作情况以及“今珠多糖注射液”的种种疑问。中共农业农村部又在6月13日晚间发布消息称，在没有使用非洲猪瘟病毒开展动物实验数据的情况下，“今珠多糖可有效防治非洲猪瘟”一说缺乏科学依据。
 </p>
 <p>
  6月17日，海印股份发致歉声明称：因工作人员疏忽，出现了一处错误表述，将合同中的“为非洲猪瘟防治注射液的投产做准备”表述为“为非洲猪瘟防治疫苗的投产做准备”。“疫苗”一词从未在合同中出现。
 </p>
 <p>
  但上海明伦律师事务所合伙人、律师王智斌对《上海证券报》表示，误导性陈述的认定，取决于该陈述是否使市场产生“错误判断”、是否产生“重大影响”，与行为人主观上是故意还是过失无关。也就是说，即便“笔误”一说成立，也不影响监管部门对其行为性质的认定。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2967418.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2967418.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2967418.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/18/n2967418.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

