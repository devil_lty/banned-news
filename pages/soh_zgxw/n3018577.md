### 中共抹黑港人抗争 北京民众：怎不报起因？
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="6月9日香港反送中大游行，103万民众参与，震撼全球！（大纪元图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/20190609-parade-kiri-17-600x400-1-600x400.jpg"/>
 </div>
 <div class="caption">
  6月9日香港反送中大游行，103万民众参与，震撼全球！（大纪元图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月9日】
  </span>
  <span class="content-info-type">
   （本台记者杨正综合报导）
  </span>
  一个月来，香港多次爆发民众反对修订逃犯条例的抗议活动，中共官媒先是集体失声，待冲击立法会事件发生后，则开足马力指控示威者是“暴徒”，有北京民众质疑，怎么不报起因？
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  为了抗议港府强推修订逃犯条例，香港民众于6月9日、16日分别爆发了100万人和200万人的大游行，7月1日，也就是香港主权被移交给中共22周年之际，又有55万香港民众再次走上街头，引发各个国际主流媒体争相聚焦，持续报道，而中共官媒则集体失声。
 </p>
 <p>
  7月1日发生冲击立法会事件后，中共官媒突然活跃起来，新华社火速跟进报道，并接连刊发中共国务院港澳办、中共香港中联办的讲话，将示威者称作“反修例的激进分子”，将事件称为“暴力冲击并占领立法会”，强调要追究“刑事责任”。
 </p>
 <p>
  央视新闻联播则将示威者称作“一些极端分子”，将事件描述为“肆意打砸破坏”。
 </p>
 <p>
  但中共没头没脑一边倒的抹黑宣传似乎效果并不尽其意。
 </p>
 <p>
  台湾中央社记者近日询问多名北京民众对香港事件的看法时，有一位受访的北京大学生质疑，“人家（香港民众）过得好好的，干嘛要抗议？”“为什么报导完全没有提及抗议的原因跟诉求？为什么没来由的要办个活动‘支持警察执法’？（官媒）报导完全没说清楚呀！”
 </p>
 <p>
  当然对于冲击立法会事件中，各界提出的为何市民冲入立法会时，警察都不见了；警察公共关系科总警司谢振中的声明视频中，网民放大谢的手表，为何时间显示是5点05分左右，比市民冲入立法会大楼早了4个小时等等质疑，中共官媒也不会提及的。
 </p>
 <p>
  据中央社报道，已有十余名曾在私人微信群组讨论，并表示支持香港示威者的北京民众，近日陆续受到警方约谈盘问。主要话题发起者近日更遭到国保二度警告，称先前的行为已是“寻衅滋事”，倘若再转发相关言论，就涉及“煽动颠覆政权”。
 </p>
 <p>
  报道引述一位曾历经六四事件的北京体制内分析人士指，中共高层最担心的其实是“香港的火星，会在中国引发燎原大火”。
 </p>
 <div>
 </div>
 <p>
  他认为，执政者面临的内外压力持续加剧，“中美摩擦、香港示威都显而易见，党内的暗潮汹涌才难以测量”。因此，为了巩固执政的稳定性，“哪怕只有一点点示威，都一定要强力打压，因为你永远也不会知道，哪个小事件会导致政权崩溃”。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3018577.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3018577.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3018577.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/09/n3018577.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

