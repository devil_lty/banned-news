### 香港事未了广东已开始？云浮传两万人上街(视频)
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="6月19日起，广东云浮市郁南县爆发村民维权活动（大纪元图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/img-6431-600x400-800x533-600x400-600x400.jpg"/>
 </div>
 <div class="caption">
  6月19日起，广东云浮市郁南县爆发村民维权活动（大纪元图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月22日】
  </span>
  <span class="content-info-type">
   （本台记者岳文骁综合报导）
  </span>
  香港人“反送中”行动越演越烈之际，广东云浮郁南县有村民因不满当局准备兴建垃圾焚烧发电厂，连日来上街示威，据称周六上街人士达2万人。示威民众堵塞高速公路收费站，瘫痪主要干道，当局出动特警戒备。当地政府下午发出通知决定“暂停建设”有关项目，但村民担心政府“捉字虱”（玩文字游戏），至今仍未完全散去。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  据香港《苹果日报》报导，广东云浮郁南县村民抗议当局准备兴建垃圾焚烧发电厂的行动，周六（6月22日）进一步升级。南江口镇、连滩镇、东坝镇共2万名示威者，早上在南江口镇民堂村集合后，前往连滩高速出口，堵塞高速公路。
 </p>
 <p>
  消息人士透露，因周六日是假期，不少老师及学生参加示威，亦创连日来示威者人数纪录，游行队伍更有家长拖着小孩上街，示威过程无人受伤或被拉。
 </p>
 <p>
  报导说，当地政府派出持盾特警戒备，并在现场部署多重防线，以防事态激化。村民不满持盾特警推前防线，一度投掷杂物泄愤，警民发生推撞。
 </p>
 <p>
  据悉，至当天下午，当地政府发出通知暂时取消项目，但村民要求完全撤销项目，人群至今未散去。
 </p>
 <p>
  但也有当地民众向大纪元披露，参与示威者大约为三四千人。
 </p>
 <div class="sohzw-video-wrapper">
  <div class="ar-wrap-4x3">
   <div class="ar-wrap-inside-fill">
   </div>
  </div>
 </div>
 <div class="sohzw-video-wrapper">
  <div class="ar-wrap-4x3">
   <div class="ar-wrap-inside-fill">
   </div>
  </div>
 </div>
 <p>
  据本台早前报导，6月10日，郁南县自然资源局发布了该垃圾焚烧发电项目选址意见核发批前公示，定下的公示期为6月10日至6月19日，为期10天。村民到这时才知道，当局要在民堂村建垃圾焚烧发电项目。
 </p>
 <p>
  据悉，官方将于民堂村大满塘设立的垃圾焚烧发电项目，占地7万平方米（约70万平方尺），落成后每日可处理1,050吨生活垃圾。
 </p>
 <p>
  当地村民不满垃圾焚烧发电厂接近水源地和民居，对环境造成污染之余，还危及当地人健康，因此自发进行了征签行动上交反对意见。但官方未回复村民就开始动工。于是6月19日、20日两天，当地二、三百名村民在选址地游行示威，他们从民堂村一直游街至连滩镇，全程距离9公里多，村民一共走了三、四个小时，并一直留守在现场阻止施工。
 </p>
 <div>
 </div>
 <p>
  网传视频显示，村民高举写有“强烈反对垃圾焚烧，拒绝被动吸食二恶英！”“一人患癌祸害三代”等横额，一路高喊抗议口号。据称，游行过程虽然没警察干预，但结束后政府人员恐吓带头村民不要闹事。
 </p>
 <p>
  村民当时表示，如果政府一意孤行，在外打工的村民也将回来参与，周边十公里之内的连滩镇、南江口镇、东坝镇、西坝镇、河口镇数万名村民也将群起抗议。
 </p>
 <p>
  由于近期正当香港爆发“反送中”大游行，邻近的广东民众维权示威令当局异常紧张。有消息指，广东郁南高速已封路，所有车辆不能出入，中共郁南县委办公室周五（21日）就发出红头文件决定取消本周末（6月22日至23日）休假，要求全县各机关企事单位干部职工全员在岗，“切实做好相关工作”。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979475.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979475.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979475.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/22/n2979475.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

