### 评论：这是世界正义力量对迫害信仰者的宣战
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="“追查迫害法轮功国际组织”发言人汪志远在集会上发言。（李莎/SOH）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/05/5-20190516-am3a7994-600x395.jpg"/>
 </div>
 <div class="caption">
  “追查迫害法轮功国际组织”发言人汪志远在集会上发言。（李莎/SOH）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月22日】
  </span>
  <span class="content-info-type">
   （本台记者田溪采访报导）
  </span>
  6月21日，美国国务院发布2018年度《国际宗教自由报告》，美国国务卿蓬佩奥（Mike Pompeo）在国务院新闻发布会上表示将对迫害者追究责任。评论认为，这是世界正义力量对迫害信仰人士的宣战。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  美国国务卿蓬佩奥（Mike Pompeo）在国务院新闻发布会上说，美国国务院肩负着推动国际宗教自由的使命，这不仅是川普政府的优先事项，也是他个人深为关切的事项。
 </p>
 <p>
  蓬佩奥说：“报告揭露了残暴政权、暴力极端主义团体及个人所犯下的一系列令人生畏的暴行。我要对所有那些对宗教自由不屑一顾的人说：美国正在注视着，你们将被追究责任。”“在中国，（中共）政府对许多信仰者——法轮功学员、基督徒和藏传佛教徒的加剧迫害成了常态。”历史不会对这些暴行保持沉默。
 </p>
 <p>
  美国国际宗教自由大使布朗巴克（Sam Brownback）也在新闻发布会上说，中共已对信仰宣战，并指出，中共政府对中国大陆各地的几乎所有信仰人士进行迫害。他谴责中共镇压宗教信仰者并“强摘”良心犯器官。“别搞错了，你不会赢得对信仰的战争。”“这将给你在国内和世界上的地位带来后果。”
 </p>
 <p>
  “追查国际”负责人汪志远对本台表示，美国政府官员的表态宣布了中共迫害信仰者的破产、宣告了他们的末日、是世界正义力量对迫害信仰者的宣战。
 </p>
 <p>
  汪志远：“特别是对中共迫害法轮功、中共对道德良知的毁灭性的迫害，有了一个更加明确的、更加重视的态度！而且从他们的这些表态上看，他们已经开始采取行动了。（这）是人类的一个进步！世界上对中共邪恶本质有了一个更深的认识、对中共在全球范围内追究中共迫害信仰的罪行、制止中共迫害的罪行有了一个新的阶段。这也标志着在全世界范围内开始全面的追究中共的反人类罪行、解体中共到了一个新的时段。特别是“中共活摘法轮功学员器官”，这个罪恶是人类历史上从来没有过的！我相信，从他们现在的态度上看，他们已经开始认识到这个问题了，已经看到中共的邪恶的性质了！随着“中共活摘”法轮功学员器官的罪恶的彻底曝光，中共也就彻底解体了！人类就永远摆脱了邪恶的干扰、邪恶的迫害！”
 </p>
 <p>
  一位大陆异议人士对本台表示，在中国文化传统中，任何信仰都是个人的事情，但是在中共邪恶统治下却出现信仰没有个人选择的空间、政府可以随便干涉信仰自由甚至迫害信仰者，对信仰迫害是最大的，政府越界管了不该管的事情，利用公权力迫害打压信仰者、“活摘”他们的器官、而且将迫害延伸到律师群体。
 </p>
 <p>
  他说：“作为我们在国内生活的（人们）也希望对信仰的迫害能够结束，我也不愿意看到这个世界今天还在政治生活中、在国际生活中，一直以信仰讨伐、以信仰定罪，因为这就是欧洲在中世纪结束前期发生的。但是我们国家，说白了，现在就面临这种局面。所以作为在国内生活的人，我是希望这一页能翻过去、不在出现这类对信仰的跨界干涉！我们中国是个世俗多元的社会，任何信仰应该是个人的自由、是个人私人的事情，所以我希望我的国家避免在欧洲进入现代化前期的那种信仰冲突和信仰打压、信仰迫害的悲剧。”
 </p>
 <p>
  曾在中国独立调查“中共活摘器官”罪行的法轮功学员于溟，日前在一次共产主义受难者基金会座谈中表示，如果我们让这个邪恶政权继续存在，任其势力在全球扩张，后果真是无法想像！那真是人类历史上没有过的灾难！
 </p>
 <div>
 </div>
 <p>
  于溟说，现在是时候行动了。中共因为过于暴虐和专制，在国内引发前所未有的民怨沸腾，全民反迫害的基础已经具备；中共在国际上的肆意扩张和为所欲为，也已在国际上被以美国为首的国家有所警惕、醒悟并开始从经济上到人权领域付诸行动。如果要对中共政权采取任何行动，那么现在这是一个绝好时机！
 </p>
 <p>
  希望之声国际广播电台田溪采访报导
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979415.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979415.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979415.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/22/n2979415.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

