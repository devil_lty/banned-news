### 《皇后大道东》因敏感遭“下课” 雨果《悲惨世界》曲目也难逃厄运
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="香港市民200万游行“反送中” AP" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/600-phplhailf-600x400.jpg"/>
 </div>
 <div class="caption">
  香港市民200万游行“反送中” AP
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月25日】
  </span>
  <span class="content-info-type">
   （本台记者董筱然综合报导）
  </span>
  香港200万市民上街游行反对香港政府修订《逃犯条例》，引发广泛关注。游行期间，香港人相互传唱的歌曲也在社交媒体流传，吸引民众竞相传唱。其中根据法国大文豪雨果名著《悲惨世界》改编音乐剧的知名歌曲《你有听见人民的声音吗？》（Do You Hear the People Sing?），在此次“反送中”行动中也是传唱的主题曲之一，该歌被视为反对极权统治的革命代表作，据报已遭中国音乐下载平台封杀。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  台湾《自由时报》报导，大陆腾讯QQ音乐的下载清单上，已搜寻不到上述歌曲，从中国大陆境内登入，也没有结果。该歌曲也被改编为粤语版《问谁未发声》，自2014年开始流传，是香港占中运动、七一大游行及学界大罢课等社会运动的主题曲。
 </p>
 <p>
  除了香港，台湾也将这首歌改编成台语、汉语及客语多种版本，其中以台语版《你敢有听着咱的歌》最为人知，曾在大埔事件、洪仲丘事件及太阳花学运中被群众齐唱。
 </p>
 <p>
  《Do You Hear the People Sing?》是现年93岁的克西茨默（Herbert Kretzmer）于1985年为英文版的法国音乐剧《悲惨世界》创作的歌曲。克西茨默20日于英国《每日邮报》撰文表示，他看到两百万港人游行时传唱他写的歌曲，十分钦佩港人挺身反抗中共的勇气。
 </p>
 <p>
  这是香港“反送中”事件后，被中共官方下架的最新一首歌曲。目前大陆部分音乐平台已移除多首香港社运歌曲，包括台湾歌手罗大佑的《皇后大道东》及《东方之珠》、香港摇滚乐团Beyond的《海阔天空》、《光辉岁月》及《抗战二十年》 ，连香港歌神张学友的《人间道》及基督教歌曲《唱哈利路亚赞美主》（Hallelujah to the Lord）也被禁。
 </p>
 <p>
  香港政府修订《逃犯条例》引发社会巨大争议。6月9日，香港103万人上街游行反对恶法。虽然行政长官林郑月娥在游行过后对公众道歉，但并未表示会撤回修例决定，于是引发了16日200万人上街游行。而经此一事，香港政府官员和特首民望大跌，香港中文大学（中大）亚太研究所昨日24日发表6月份特区政府及特首民望意见调查结果，特首及3司评分均为有纪录以来最低，其中特首林郑月娥民望评分为37.5分，较上月大跌10.6分。
 </p>
 <p>
  政务司司长张建宗的民望评分为37.0分，较上月下跌8分；财政司司长陈茂波的评分为34.4分，较较上月下跌3.9分；而律政司司长郑若骅的评分则为26.8分，亦较上月下跌6.9分。
 </p>
 <p>
  市民对香港特区政府的不信任度也创新高，只有22.1%受访市民表示信任港府，较上月低2.8个百分点；表示不信任的却有48.9%，较上月大幅上升18.8个百分点。至于对中央政府的信任度，17.4%受访市民表示信任中央政府，较上月低4.2个百分点；表示不信任的则有54.7%，较上月高13.6个百分点。
 </p>
 <h4>
  G20香港人众筹在主流媒体登广告反恶法
 </h4>
 <p>
  中国国家主席习近平将出席27日在大阪举行的G20第14次峰会，届时将与美国总统川普进行会面。中共外交部表示，
  <strong>
   G20不会讨论香港问题
  </strong>
  ，香港事务纯属中国内政，中方也不会允许G20讨论香港问题。
 </p>
 <div>
 </div>
 <p>
  中共外交部话音刚落，香港网民为了让“反送中”事件成为G20峰会的话题，随即发起众筹。截至25日下午12时42分，众筹平台已超额筹得约336.42万港元，有关广告已确认将会在27日《金融时报》（Financial Times）的美国及亚洲版刊登。
 </p>
 <p>
  发起众筹得港人表示，在西方主流登头版广告所费不菲，除了《金融时报》外，他希望在更多国家的报章如日本、法国、英国、德国等登头版广告，令回响更大；但单是New York Times一个全版广告就需20万美元，因此发起众筹，期望“反送中”成为G20话题。
 </p>
 <p>
  目前，Financial Times已经确认本月27日，G20的峰会当天，美国及亚洲版都会预留位置刊登“反送中”广告，所需14980欧元（折合约13.32万港元），发起人亦与《纽约时报》、《华尔街日报》、《朝日新闻》、《产经新闻》、《读卖新闻》、《奈良新闻》、《20 Minutes》、《The Guardian》等媒体接洽。
 </p>
 <p>
  发起人指，所有费用将用明细公开，香港众志秘书长黄之锋会监察筹款，亦正在落实其他名人；而扣除平台必要开支（4%平台手续费+约2.9%信用卡/银行手续续费）后将用作刊登G20相关纸媒广告之用，如捐款有剩余将拨入进行持续纸媒宣传基金，该基金正接洽名人或议员监察。
 </p>
 <p>
  发起人表明，一旦最终筹集的资金不够刊登广告，会将已经筹得的款项全部捐予“反送中受伤被捕者人道支援基金 Anti- Extradition Protest Trust”。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2984320.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2984320.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2984320.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/25/n2984320.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

