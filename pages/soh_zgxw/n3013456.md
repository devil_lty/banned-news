### 谷歌地图示三峡大坝严重扭曲变形 官方数次辟谣后终改口承认
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="长江一级支流大宁河一处水电站大坝下的干涸河床 网络图片" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/4260a108d70c89433a5ba25730cb4aab-600x400.jpg"/>
 </div>
 <div class="caption">
  长江一级支流大宁河一处水电站大坝下的干涸河床 网络图片
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月7日】
  </span>
  <span class="content-info-type">
   （本台记者董筱然综合报导）
  </span>
  近日，谷歌卫星地图显示三峡大坝严重扭曲变形的图片在社交媒体刷屏，引发关注。面对来势汹汹的舆论，中共官方先是发布高分六号观测卫星的图像，显示大坝并不存在变形。随后又改口承认坝体确实会“漂移”。7月6日三峡微信公众号刊文直言三峡大坝坝体已经变形，但解释坝体变形处于弹性状态，符合重力坝变形规律。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  谷歌卫星地图显示三峡大坝扭曲变形的事件在7月初开始在网络上传播。华裔经济学者“冷山”7月1日在其推特中，将三峡早前的谷歌卫星图像和当前图像进行对比，认为三峡大坝已经变形，有溃坝的危险。
 </p>
 <blockquote class="twitter-tweet" data-dnt="true" data-width="550">
  <p dir="ltr" lang="zh">
   三峡大坝已经变形，一旦溃坝，半个中国将生灵涂炭，垬和那些大家族也将玩完！
   <span href="https://t.co/K5AQPgx1mK">
    pic.twitter.com/K5AQPgx1mK
   </span>
  </p>
  <p>
   — 冷山 (@goodrick8964)
   <span href="https://twitter.com/goodrick8964/status/1145577897479954433?ref_src=twsrc%5Etfw">
    July 1, 2019
   </span>
  </p>
 </blockquote>
 <p>
 </p>
 <p>
  不少网民通过
  <span href="https://www.google.com/maps/@30.8228322,111.0033702,1572m/data=!3m1!1e3">
   谷歌卫星地图
  </span>
  查看后，发现当前的大坝卫星图像确实存在严重的变形问题。事件一石激起千层浪，三峡大坝扭曲变形所带来的安全问题在中国的社交媒体上引发广泛讨论。
 </p>
 <p>
  面对居高不下的舆论，中共党媒官媒开始辟谣，并援引“专家”指，是谷歌卫星图片的精确度、图片拍摄的角度和拼接成像时的算法导致了图像变形。
 </p>
 <p>
  岂料大量涌入谷歌卫星查看图像的网民们并不买账。有网民质疑，如果三峡大坝变形是出于图像偏差，那为什么大坝周边的许多道路没有变形。还有人说，“党媒一辟谣这事儿八成就是真的。”
 </p>
 <p>
  7月4日，中国航天科技集团加入辟谣团队。这家中共国企发布一张据称是中共高分六号观测卫星的图像，显示三峡大坝并无变形。大陆媒体解释说，官方放出这个卫星图像，是因为很多人仍然不大相信之前的官媒解释。
 </p>
 <p>
  之后官方开始承认坝体移位。据党媒援引“专家”指，三峡大坝整个坝体其实是由十几个独立区块拼接而成，在水压不均衡的情况下，互相之间确实存在“漂移”的可能。
 </p>
 <div>
 </div>
 <p>
  官方开始一再否认三峡坝体变形引发了更多民众寻求真相。7月6日，“冷山”又在推特上传了三峡大坝卫星图像的立体视频，从中可以更明显地看出大坝的变形。有网友发现，坝体不但水平方向存在扭曲，垂直方向也有明显的错位，因此质疑大坝底部是否也存在不同程度的塌陷问题。
 </p>
 <blockquote class="twitter-tweet" data-dnt="true" data-width="550">
  <p dir="ltr" lang="zh">
   三峽是否變形，看完這个視頻就明白了！
   <span href="https://t.co/wYRN2NkIFS">
    pic.twitter.com/wYRN2NkIFS
   </span>
  </p>
  <p>
   — 冷山 (@goodrick8964)
   <span href="https://twitter.com/goodrick8964/status/1147382868894633985?ref_src=twsrc%5Etfw">
    July 6, 2019
   </span>
  </p>
 </blockquote>
 <p>
 </p>
 <p>
  同一日，中国长江三峡集团有限公司微信公众号发表文章承认坝体确实已经变形，但强调三峡工程运行安全可靠，坝体变形处于弹性状态。
 </p>
 <p>
  三峡大坝是中共的军事禁区，有重兵把守，甚至百度地图都不提供当地的清晰卫星图像。因此外界了解大坝的现状存在一定困难。
 </p>
 <p>
  熟悉水利工程的人都可能了解，三峡工程在开工之前就一直争议不断，中共的水利专家们为此争得不可开交。近年来三峡工程的后果已经逐渐显现，河床干涸、鱼类死亡、湿地流失。拥有长江四大家鱼之称的青鱼、草鱼、鲢鱼、鳙鱼鱼苗出生量正在急剧下降，由上世纪50年代的300多亿尾降为目前不足1亿尾，珍稀物种也正以惊人的数字每年递减和消亡；仅中华鲟这一类水生动物，在2015年仅剩不到100尾，且不能自行繁殖。
 </p>
 <p>
  三峡工程是中共前党魁江泽民1992年在一片反对之声中强行修建的。著名水利专家黄万里曾给江泽民写信劝说，三峡工程祸国殃民，劝他不要开展这个有百害而无一利的工程，江泽民没有采纳。
 </p>
 <p>
  信中说，“长江三峡高坝是根本不可修建的，不是早修晚修、国家财政，生态、防洪效果、经济开发或国防的问题，主要是自然地理环境中河床演变的问题，根本不许可一个尊重科学民主的政府举办这一祸国殃民的工程。它终将被迫炸掉。川汉保路事件引起辛亥革命实为前车之鉴。”
 </p>
 <p>
  23年后，中科院研究员陈国阶说：三峡工程不纯粹是个技术工程，更多地会牵连到生态环境、政治问题、社会经济各个方面。完全验证了黄万里的警告，可是为时已晚。
 </p>
 <h4>
  <strong>
   水利专家：三峡大坝是移动的
  </strong>
 </h4>
 <p>
  近年来，三峡大坝的隐患引起越来越广泛的关注。三峡周边旱涝和地震等地质灾害不断，库区也已出现大量崩塌和滑坡事件。这也被认为是一个谷歌地图就引起民众和中共官方如此关注的原因之一。
 </p>
 <p>
  三峡大坝问题专家、水利专家王维洛博士透过谷歌卫星提供的图像为大众解答了此次三峡大坝扭曲变形的原因。他对大纪元表示，从结构来看，三峡大坝并非中共媒体所宣传的铜墙铁壁，所有人不知道一个秘密，“三峡大坝是在走动的，而这种设计决定了三峡大坝的脆弱性。”
 </p>
 <p>
  王维洛说，三峡大坝是混凝土重力坝，但并不像大家看到的三峡大坝模型那样是整体一块，而是由几十个独立的混凝土坝块组成（因为混凝土不可能那么长那么大块浇筑），每一个坝块利用重力放置在基岩上保持稳定。
 </p>
 <p>
  “也就是说，是摆在岩石上的，坝块和坝基的结合处不是像造房子时它的钢柱是打到地下去的，它和基岩是分离的，受水的压力和温度影响，它会发生不同的形变和位移。也就是说大坝在走。”
 </p>
 <p>
  为什么要这样设计？王维洛说，当初设计时，他们想像坝块之间是一个可控的、均匀的移动，“设计的时候是考虑要移动的，每年向前移动零点几个毫米（以前有公布这个数据，现在是不公布了），然后坝块相互挤在一起，靠挤的力量。就像木头，打个楔子打进去之后一挤压就会很牢固，当时设计的时候是这个思路。但现实情况位移是不均匀的，就是不在一条直线上运动，扭曲很容易在接缝的地方产生裂隙，使得这个不均匀的运动更加厉害，最后这个大坝就会废掉。”
 </p>
 <p>
  王维洛说，从对比的那张照片中可以看出，右边那张照片坝段是弯曲的，“就是在某一个小段是直线，因为坝块是一块一块的，而一块一块链接的部位就是发生了不均匀的移动。”
 </p>
 <p>
  王维洛推断，现在可能已经出现变形的问题，“因为出现这种问题的时候，他们首先会把水放掉，右边那张照片两边就是在放水，而且放水放的很厉害。我在那边工作时间比较长，我可以说，如果三峡大坝发生溃坝，三峡大坝下面的宜昌市居住的70万人命就没了。”
 </p>
 <p>
  王维洛表示，三峡工程从2003年开始试运行到现在16年都还没有验收过，没有人敢担保它的质量。现在大家在网上讨论这件事情非常有意义，中共必须对老百姓有个交代。
 </p>
 <p>
  “关键问题是大家都很关心，它可能存在着相当严重的工程质量问题和大坝安全问题，这是一件要命的事情。这些问题不是哪个网友来解释这只是光线问题，而且应该由独立的第三方介入工程检验和验收，没有第三方那基本上整个就是一个黑箱操作。”
 </p>
 <p>
  其实，三峡大坝早点拆了早点好，王维洛表示，现在世界的潮流不是建大坝去搞水利，而是顺应自然去搞水利，这是去年世界上所有的水利工程师得到的共识。
 </p>
 <p>
  “拆也很容易，就是把闸门全部打开，让水自己进多少出多少。但中共不愿这样做，主要是和它政绩、名声连在一起，如果现在废掉，那前面的功绩就没了。”王维洛说。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3013456.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3013456.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3013456.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/07/n3013456.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

