### 京城官商勾结之典型   北京前副市长陈刚被立案审查
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="北京前副市长陈刚因严重违纪被立案审查（大纪元图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/chengang-800x533-600x400.jpg"/>
 </div>
 <div class="caption">
  北京前副市长陈刚因严重违纪被立案审查（大纪元图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月11日】
  </span>
  <span class="content-info-type">
   （本台记者蕭晴综合报导）
  </span>
  中共官网7月11日消息，中国科协书记处书记陈刚因严重违纪被立案审查。陈刚曾长期担任北京副市长，主管北京的城市规划建设。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  中共纪委国家监委网站11日通报，陈刚搞两面派、做两面人，对抗审查，不如实说明问题，搞迷信活动；利用职权建造供个人享乐的豪华私家园林，弄虚作假，违规多占住房，长年无偿占用酒店豪华套房；经济上极度贪婪，长期利用规划审批的重要职权大肆敛财，为亲属经营活动谋取利益，大搞权钱交易，收受巨额贿赂；生活上极度腐化奢靡，肆无忌惮追求个人享乐等。
 </p>
 <p>
  陈刚被“双开”，其涉嫌犯罪问题移送检方审查起诉。
 </p>
 <p>
  今年1月6日晚，中共科协党组成员、书记处书记陈刚落马，成为新年第一个落马的副部级官员。
 </p>
 <p>
  陈刚1989年8月起历任清华大学建筑学院讲师、团委书记、政治辅导员、党委委员；广西柳州市市长助理、市城市综合管理办公室主任等职；2002年起任北京市规划委员会党组书记、主任；2006年6月接替被立案调查的刘志华，升任北京市政府副市长、党组成员，后进入常委。
 </p>
 <p>
  2017年，陈刚被调离北京，出任南水北调办副主任；2018年6月起任中国科协党组成员、书记处书记；同年9月任中国科协常委，直至落马。
 </p>
 <p>
  据财新网去年3月的报导，在九名北京市副市长中排名第二的陈刚，曾被认为是一名前途无量的“65后”官员：清华毕业，长期主管北京的城市规划建设，40岁时即升任北京市副市长，长期主管北京市国土、住房、城建、轨道交通等领域，2012年还当选中共十八届中央候补委员。
 </p>
 <p>
  不过，2012年，署名“知情人”在网络爆料，陈刚与移民加拿大的房地产商李某狼狈为奸，陈刚利用手中的规划大权给其批地，李某利用赚到的钱为陈刚买官铺路，充当陈刚的家族经济总管，实属近年来京城官商勾结、腐败透顶的典型。
 </p>
 <p>
  2013年4月，陈刚再被北京女律师崔家楠实名举报涉嫌诈骗住房维修基金等。当时，崔家楠被警方以“涉嫌扰乱社会秩序”的罪名扣押。
 </p>
 <div>
 </div>
 <p>
  2015年3月，新浪微博曾爆料，北京市委常委、常务副市长陈刚，目前正在接受有关部门传唤并协助调查。知情人士向港媒透露，陈刚利用主管城市规划的权力，接受诸多开发商、承包商的巨额贿赂。行贿商人将行贿款打给陈刚的妻子，陈妻现已移民加拿大。
 </p>
 <p>
  2017年，时任北京市副市长陈刚被调离北京，出任南水北调办副主任这一“闲职”时，就被外界认为其处境不妙。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3024697.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3024697.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3024697.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/11/n3024697.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

