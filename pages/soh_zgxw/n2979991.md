### 大陆学校实行刷脸报到 网民直呼太恐怖
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="大陆学校实行刷脸报导引热议（视频截图）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/screenshot2019-06-23at12.22.06-600x425.png"/>
 </div>
 <div class="caption">
  大陆学校实行刷脸报导引热议（视频截图）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月23日】
  </span>
  <span class="content-info-type">
   （本台记者杨正综合报导）
  </span>
  日前有网民在推特上发视频指，有学校已经实施刷脸报到了，引发网民热议，认为中共监控一切太恐怖了。但有评论认为，中共不计后果的末日心态下的做法会物极必反，整个体制已变得非常脆弱。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  网民紫诺19日发出的视频显示，一所学校大门口，齐刷刷的安装了一排人脸识别通道，上学的学生必须依次经过通道，才能进入学校上课。视频中的孩子们带着红领巾，显然是大陆的某所小学。
 </p>
 <blockquote class="twitter-tweet" data-dnt="true" data-width="550">
  <p dir="ltr" lang="zh">
   這不知是哪個學校，已經實施刷臉報到了🤨🤔👿
   <span href="https://t.co/lF6a6g5Xb2">
    pic.twitter.com/lF6a6g5Xb2
   </span>
  </p>
  <p>
   — 紫諾 (@dzjsqy)
   <span href="https://twitter.com/dzjsqy/status/1141305681036926976?ref_src=twsrc%5Etfw">
    June 19, 2019
   </span>
  </p>
 </blockquote>
 <p>
 </p>
 <p>
  网民纷纷发表评论说，“从小就被控制了。”“科技落在流氓手上，是文明的灾难。”“这个太恐怖了，极权政权与高科技相结合诞生出一个窥视监控一切的邪恶怪兽，最终没有任何个人隐私和自由生存的空间。”
 </p>
 <p>
  去年5月，浙江杭州市第十一中学也被媒体爆出，校方以提升“教学管理”为由，在学校教室里安装组合摄像头对学生“刷脸”，搜集学生的表情、动作进行大数据管理。而在2017年的时候，该校的食堂点餐取餐、自助购物以及图书馆借书等，都早已引入人脸识别技术。
 </p>
 <p>
  独立评论人士张起对海外中文媒体大纪元表示，“维稳正在被量化，与学校的绩效考核机制结合，数据采集的越细，工作做得越好，也就是说他们对社会管控技术的精细化、技术的升级已经达到变态的程度。就像当年东德，1/3的人都是线人，当时是用线人的方式来刷脸，现在是用计算机来刷脸，这是在极权或专制体制之下维持自己统治的手段。”
 </p>
 <p>
  多方报导显示，中共为了加强社会管控，正不惜斥巨资打造“全景监狱”。
 </p>
 <p>
  据中共官媒引用一位专家的说法，中共警方在未来数年内将在提升跟踪活动的技术能力上再花费2000多亿人民币，
 </p>
 <div>
 </div>
 <p>
  台湾《信传媒》去年报道说，中共决定3年内把全国监视器数量增至6亿个。
 </p>
 <p>
  研究公司IHS Markit预测，全球用于在视频画面搜索脸孔的伺服器当中，将有75%被中国采购。
 </p>
 <p>
  中共政府大量的采购合约，也在大规模推动监控技术的研究和开发，这些技术可以跟踪人脸、人穿的衣服，甚至人走路的样子，实验性小型装置，比如，人脸识别眼镜，也已经开始出现。
 </p>
 <p>
  张起表示，中共不计后果的末日心态下的做法会物极必反，“维稳经费在节节提高，基层警力的工作量在成倍增长，但整个体制到今天已经是变得很脆弱，已经呈现出各种各样不堪重负，如金融领域去杠杆化导致整个民间像P2P的崩盘，对新疆、西藏人权的打压，导致当地维稳体系也已经不堪重负，还有最近几年对教会的打压、对老师及各种群体社会性诉求的打压，基层的警务人员、公务人员已经出现疲态。”
 </p>
 <p>
  也有评论人士指出，随着中国经济的下滑，当中共政府无力支付巨额的维稳经费时，维稳体系中的底层人士如协警、城管等必将首先受到裁员等冲击，当这些维稳人员成为被维稳对象时，中共的统治也就走到尽头了。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979991.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979991.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2979991.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/23/n2979991.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

