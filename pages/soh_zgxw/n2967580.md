### 一日内中国两银行官员被爆出事 网友这样反应
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="官员坠亡新闻下的网友留言（网络截图)" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/screenshot2019-06-18at10.37.42-600x410.png"/>
 </div>
 <div class="caption">
  官员坠亡新闻下的网友留言（网络截图)
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月18日】
  </span>
  <span class="content-info-type">
   （本台记者杨正综合报导）
  </span>
  中共官方昨日（6月17日）公布，四川一名银行官员落马被查，安徽一名银行官员跳楼自杀。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  四川省纪委监委网站昨日通报称，成都农商银行原党委书记、董事长傅作勇涉嫌“严重违纪违法”，目前正接受纪律审查和监察调查。
 </p>
 <p>
  傅作勇，男，1957年生人，2010年1月进入成都农商银行，先后担任董事长、党委书记。
 </p>
 <p>
  2016年6月至2017年1月从党委书记降职到成都农商银行人力资源部工作，2017年1月，退休。
 </p>
 <p>
  官方宣布傅作勇被查的同一天，安徽枞阳县新闻办发布消息指，枞阳县农商银行党委委员、纪委书记、监事长周某于15日从县农商行办公楼楼顶坠至该行院内地面身亡。警方认定，周某系自行坠楼身亡，排除刑事案件可能。
 </p>
 <p>
  相关文章下方有不少网友留言：“上班时间从办公屋楼顶纵身一跳，一了百了。安微一县农商行监事长坠楼身亡，农商行又一热闻。”“怎么没公布结果是抑郁症啊，当然不是临时工。”“一看就是压力山大，自己坠亡的，还惊动警方？”“星期六还到单位加班，因工作太累，到楼顶透气放松，头晕失足坠楼。里面故事多。”
 </p>
 <p>
  从网友留言可见，民众对死者没有任何同情怜悯之意。
 </p>
 <p>
  长久以来，中共官员贪腐成风，民众早已深恶痛绝，其中金融业又是重灾区。近日有媒体盘点了10年来落马的15名银行高层官员以及4年来被查的银行厅级官员。
 </p>
 <h4>
  <strong>
   近10年来落马银行高层官员15人全名单
  </strong>
 </h4>
 <p>
  1、2019年2月被查的中信集团执行董事赵景文；
  <br/>
  2、2017年2月被查的交行原首席风险官杨东平；
  <br/>
  3、2016年6月被查的国开行原董事长姚开民，判14年；
  <br/>
  4、2012年6月被查的邮储银行原行长陶礼明，无期徒刑；
  <br/>
  5、2012年5月被查的农行副行长杨琨，无期徒刑；
  <br/>
  6、2008年6月被查的国开行原副行长王益，死刑，缓期2年执行；
  <br/>
  7、2005年3月被查的建行原行长、中央候补委员张恩照，判15年；
  <br/>
  8、2004年6月被查的农发行原副行长胡楚寿，无期徒刑；
  <br/>
  9、2004年6月被查的农发行原副行长于大路，无期徒刑；
  <br/>
  10、2003年5月被查的中国银行原副董事长、原中银香港总裁刘金宝，2005年被判死刑，缓期2年执行；
  <br/>
  11、2002年1月被查的建行原行长、中央候补委员王雪冰，判12年；
  <br/>
  12、2002年9月被查的中行原副行长赵安歌，无期徒刑；
  <br/>
  13、1999年7月被查的光大集团原董事长朱小华，判15年；
  <br/>
  14、1998年4月被查的中国国际信托投资公司（中信集团）原董事长金德琴，无期徒刑；
  <br/>
  15、交行原副行长鲁家善被查时间不详，1998年被判3年缓3年。
 </p>
 <h4>
  <strong>
   4年来被查银行厅级官员16人全名单
  </strong>
 </h4>
 <h3>
  <strong>
   中管金融企业
  </strong>
 </h3>
 <div>
 </div>
 <p>
  1、2019年6月被查的工行上海分行党委书记、行长顾国明；
  <br/>
  2、2019年4月被查的工行重庆分行党委书记、副行长谢明；
  <br/>
  3、2019年1月被查的交行发展研究部总经理李杨勇；
  <br/>
  4、2017年12月被查的建行山东省分行党委书记、行长薛峰；
  <br/>
  5、2017年8月被查的民生银行首席信息官林晓轩；
  <br/>
  6、2017年4月被查的进出口银行北京分行党委书记、行长李昌军；
 </p>
 <h3>
  <strong>
   省管金融企业
  </strong>
 </h3>
 <p>
  7、2018年12月被查的吉林银行党委委员、副行长王安华；
  <br/>
  8、2018年9月被查的富滇银行党委委员、副行长孔彩梅；
  <br/>
  9、2018年4月被查的海南省农村信用社联合社党委书记、理事长吴雄伟；
  <br/>
  10、2018年4月被查的中信银行南昌分行党委委员、副行长姚蔚；
  <br/>
  11、2018年3月被查的山东省农村信用社联合社党委书记、理事长宋文瑄；
  <br/>
  12、2017年6月被查的云南省农村信用社联合社党委书记、理事长万仁礼；
  <br/>
  13、2017年4月被查的甘肃省农村信用社联合社党委副书记、理事长雷志强；
  <br/>
  14、2017年2月被查的江苏银行党委书记王建华；
  <br/>
  15、2016年3月被查的河南省农村信用社联合社党委书记、理事长鲁铁；
  <br/>
  16、2015年10月被查的黄河农村商业银行党委委员、副行长贺立仁。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2967580.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2967580.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2967580.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/18/n2967580.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

