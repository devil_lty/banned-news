### 浙江三县级官员因嫖娼被协警敲诈 细节曝光
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="浙江县级官员因嫖娼被敲诈 获刑（网络图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/hammer-719066-640-600x450.jpg"/>
 </div>
 <div class="caption">
  浙江县级官员因嫖娼被敲诈 获刑（网络图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月20日】
  </span>
  <span class="content-info-type">
   （本台记者韩梅综合报导）
  </span>
  浙江常山县原副县长顾建华与另外两个县级官员因嫖妓被协警敲诈了78万元（人民币，下同）一事，近日再被披露一些细节。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  中共《中国纪检监察报》6月19日刊文，曝光顾建华的放荡生活。
 </p>
 <p>
  文章说，顾建华2001年任常山县偏远山区的芳村镇中共党委书记。为了离开芳村镇这个“艰苦环境”，顾建华开始了自己的“两面”人生：表面上看起来做事“风风火火”，实际上是为了“贪图享乐、追求奢靡”。
 </p>
 <p>
  2007年，顾建华如愿调任常山县环保局局长，进入“县域权力核心”，有权、有钱。
 </p>
 <p>
  顾建华被调查后说，他“常与所谓的同路人‘同流’，晚上吃饭、唱歌、夜宵接续进行，醉生梦死，乐此不疲”，第二天上班云里雾里，闭目养神，以备晚上再战。
 </p>
 <p>
  顾建华说，他“铺张浪费、挥霍无度、骄奢淫逸”，无所不为。
 </p>
 <p>
  2011年4月27日，时任县环保局长的顾建华与常山县原中共副县长甘土木、原中共县人大常委会副主任熊雨土在杭州一间酒店内嫖妓时，被中共杭州市下城区武林派出所当场抓获。在处理的过程中，三人得到协警俞欧的帮忙，真实身份被隐瞒，并被从轻处理，仅罚款500元。
 </p>
 <p>
  事后，三人被俞欧等人以举报嫖娼为名要挟，共被敲诈78万余元。当地一王姓老板因为在企业经营、工程承接方面获得三人帮助，所以为他们支付被敲诈款达102.7万元。
 </p>
 <p>
  俞欧及同伙因该案于2018年被刑事拘留，因敲诈勒索罪全部获刑。顾建华三人因共同受贿分别被判囚3年2个月至5年不等。
 </p>
 <div>
 </div>
 <p>
  中国裁判文书网今年4月近公布的一份刑事判决书令事件来龙去脉曝光。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2975215.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2975215.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2975215.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/20/n2975215.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

