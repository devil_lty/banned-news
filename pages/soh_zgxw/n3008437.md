### 党的大红人蹂躏幼女 贵州孤儿院有关？红黄蓝呢？
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="王振华是中共会弄大钱供养其党和假惺惺做“善事”的“大红人”。（网络图片）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/bd26-hzfekep9419862-800x534-600x401.jpg"/>
 </div>
 <div class="caption">
  王振华是中共会弄大钱供养其党和假惺惺做“善事”的“大红人”。（网络图片）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月4日】
  </span>
  <span class="content-info-type">
   （作者：郑中原）
  </span>
  7月4日打开网络，映入眼帘的是一个中国千亿红顶富商王振华涉猥亵女童
  <strong>
   （
  </strong>
  到底是算强奸还是猥亵？
  <strong>
   ）
  </strong>
  被拘的新闻，不禁倒吸一口冷气。笔者早在2017年4月写过一篇揭露这类黑幕的文章（《
  <a data-cke-saved-href="https://www.secretchina.com/news/gb/2017/04/02/818757.html" href="https://www.secretchina.com/news/gb/2017/04/02/818757.html" rel="noopener noreferrer" target="_blank">
   国已不国 官商运作“卖处一条龙”
  </span>
  》），当时还有读者（可能是五毛）留言说“根本不可信”，如今官媒披露这般人物涉案，细节详尽，不用我多言，这只是冰山一角。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  先简要介绍下王振华案。据见诸公开报导的资料，事发地是上海大渡河路一家五星级酒店，王振华6月29日下午在此“猥亵”（还是先按官方说法吧）9岁女童，而该女童是被一女子周某从江苏带到上海的，同行的还有一个12岁女孩，周某谎称带两女孩去上海迪斯尼玩，当天王振华对9岁女童实施犯罪，事后付周某现金1万元。
 </p>
 <p>
  1961年出生的王振华本身是横跨政商界的中共红人，掌控总部在上海的上市公司新城控股。新城控股对外公布总资产达3,000多亿元人民币。
 </p>
 <p>
  王振华属于中国国籍，中共党员。他除了担任中共上海市政协委员，还是江苏省人大代表，并在全国工商联、全国工商联房地产商会、上海市房地产商会、江苏省工商联等担任一定职位，获得过江苏省、上海市不少“荣誉”。
 </p>
 <p>
  王振华还创办了专门“资助”偏远农村孩子的“七色光计划”。
 </p>
 <p>
  笔者关注到几个要点：一，受害女童是由专门角色安排给王振华奸淫的；二，中介收到费用；三、女童是异地被伤害；四，王振华具有中国特色的权贵标签，既是富豪，又是官方人物，属于中共的“红人”；五是王还亲自参与一个所谓资助农村孩子的“七色光计划”，这个“色计划”令人生疑。
 </p>
 <p>
  和网上有人质疑，事件背后存在着一个长期有组织输送幼女专供富豪权贵们淫乐的犯罪链条一样，笔者认为，如果有独立公正的第三方调查，而不是向来隐瞒事实真相、或者出于政治目的避重就轻的中共来调查的话，这一事件深挖下去，必然会深度触碰中共政权根基。
 </p>
 <p>
  笔者在2017年4月在《看中国》刊发的《
  <a data-cke-saved-href="https://www.secretchina.com/news/gb/2017/04/02/818757.html" href="https://www.secretchina.com/news/gb/2017/04/02/818757.html" rel="noopener noreferrer" target="_blank">
   国已不国 官商运作“卖处一条龙”
  </span>
  》一文中，已经以亲身了解的情况证实：早在十年前，还在中国广东工作和生活时，就有身边一些知情人说，当地几乎每间初中、高中，特别是职业中学这类管理不太严格的学校，都有黑社会性质的势力虎视眈眈，虽然不会都将学生直接拐去“卖处”，但很多人毕业时已不是处女，就有被暗中“买处”这个原因。后来甚至发展到盯上了小学生。
 </p>
 <p>
  笔者还已提出，在中国的官商勾连中，有一个钱权色输送“一条龙”的“产业化”链条，其中，可怜的幼女正是其中被输送的“商品”。
 </p>
 <div>
 </div>
 <p>
  这当中，就与中国大陆的拐骗人口犯罪有关，拐骗人口多年来已在中国大陆形成一种“产业化”。女人，特别是那些被拐骗去卖淫的幼女，成为最大的受害者之一。她们先是被作为猎物诱骗，或是供商人奸淫，或由不法商人付费后输送给官商权贵，而那些女孩子根本不知道嫖客是什么身份。甚至，这些幼女直接就是权贵按需开出的订单式受害者。
 </p>
 <p>
  这方面的例子很多了，比如6月4日，中共河南尉氏县人大代表赵志勇，因奸污25名在校未成年女学生，被执行死刑。赵志勇本身拥有上亿资产，曾多次高调现身“慈善活动”。
 </p>
 <p>
  更多例子不再举，本文想继续探讨的是，此前相关的两个被官方“辟谣”的事件，水可能还真是很深、很深。
 </p>
 <p>
  一个是最近在上月下旬爆出的同样令人震惊的消息：贵州毕节市、凯里市的留守儿童幼儿园和孤儿院被曝培养幼儿满足客人兽欲……这消息官方并未查清，但很快被压下去了。爆料者新浪微博博主“鹅组兔区爆料”已被公安抓捕，承认爆料是“编造的”。
 </p>
 <p>
  另一个是2017年北京红黄蓝幼儿园曝出性侵幼儿事件，其性侵幼儿的情节和贵州的爆料极其类似。当时有受访的幼童家长透露，有十多名家长除了发现自己的孩子疑似遭“打针”及“喂食不明药片”外，有孩子被检查出肛裂原因不明。
 </p>
 <p>
  孩子们还向家长讲述有“爷爷医生”和“叔叔医生”检查身体，“小朋友光溜溜”、“叔叔也光溜溜”的疑似猥亵情景。
 </p>
 <p>
  参与性侵的坏人被指涉多名北京“老虎团”军官，而红黄蓝也被指有军方背景。北京朝阳警方介入事件调查，但先抓“造谣者”，后发调查报告。红黄蓝幼儿园也连续在官方“支持”下反攻家长“造谣”。官方一如过往处理同类事件一样，大量删除、屏蔽网络上的相关视频、文章和评论。这一巨大丑闻最后也是处理“造谣者”以及以个别教师涉嫌虐待获罪，不了了之。
 </p>
 <p>
  笔者在文章标题中说王振华案，与贵州孤儿院培养幼儿满足客人兽欲传闻有关？与红黄蓝有关？这只是打个比喻。毕竟王振华这一恶行持续多年，受害者是跨省进行的，到底有多少人受害？会不会包括红黄蓝或者贵州的孤儿院？天知道！
 </p>
 <p>
  而且，笔者在中国时就知道，这类淫棍都有一个圈子“交流”，到底还有多少人在其中，也不得而知。但有一点是必然的，这些人都是非官即商，非富即贵。王振华本人就是一名共产党员，是中共会弄大钱供养其党和假惺惺做“善事”的“大红人”。根本上说，王振华非常符合共产党列祖列宗立下的标准：马克思有私生女，列宁得梅毒，毛泽东妻妾成群，江泽民有四大情妇、搞色情治国……
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3008437.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3008437.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n3008437.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/04/n3008437.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

