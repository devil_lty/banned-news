### 大陆青年乔装到香港 为“反送中”示威打气
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="6月21日香港警察总部外集会（希望之声 摄影：郑名）" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/photo-2019-06-21-13-11-09-600x450.jpg"/>
 </div>
 <div class="caption">
  6月21日香港警察总部外集会（希望之声 摄影：郑名）
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月21日】
  </span>
  <span class="content-info-type">
   （本台记者岳文骁综合报导）
  </span>
  香港近两百万人上街游行，反对修订《逃犯条例》（送中恶法）。尽管中共封锁信息，但仍传往大陆，引发强烈响应。甚至有大陆青年特意乔装打扮来到香港为示威者打气。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  据香港01报导，一名男青年戴上鸭嘴帽、太阳眼镜和口罩，手持标语“RETRACT EVIL BILL”、“HONG KONG CANNOT FALL!”坐在立法会示威区一旁，默默支持香港示威者。
 </p>
 <p>
  这名男青年Mark（化名）是特意从中国大陆来港，支持反修例行动的，“全副武装”是以防被认出。
 </p>
 <p>
  Mark笑指自己到场前，先换了全身衣服、变装一番：“因为内地由你一出门口就开始监控，但原来香港是very easy的，我还以为满街都是监控录影机。”
 </p>
 <p>
  Mark说属于大陆为数不多的“觉醒派”，他坦言自己过往也未留意太多政治，但在五年前，香港“雨伞运动”期间，自己“翻墙”接触有关新闻、讨论，方知道“六四”真相。
 </p>
 <p>
  “我本来也以为‘六四’没死几个人，但见到‘占中’期间，很多人都在说‘八九’、‘八九’，我才翻墙搜寻。”花了不少工夫后，他找到的是一幅幅示威者被子弹打中、头破血流的画面：“我是做IT的，看得出那不是修改过的图片。”
 </p>
 <p>
  Mark此前只能透过“翻墙”，登上Twitter等网站，接触了解关于香港的游行情况。他说：“内地现在很多资讯，都是用暴动来形容这场运动，知道实际情况的内地人不多。”
 </p>
 <p>
  初次在港参与集会的Mark感叹两地分别：“香港真得很厉害，那些标语……如在内地，早就被收，然后整个区会被戒严，四周人车都不能通过。你一出来示威，一踏步早就被抓了。”
 </p>
 <p>
  Mark认为如果香港通过了《逃犯条例》修订，把港人引渡回内地受审是相当危险的事，在内地生活受尽折监控和压迫，若港人送到内地受审，后果堪虞。
 </p>
 <div>
 </div>
 <p>
  “大陆是没有法律的。香港的司法制度是基石，修订通过了，会影响香港的经济和形象。”
 </p>
 <p>
  他还披露现时在中国大陆乘搭地铁不但要检查行李，连乘客手机都被彻底调查，“所有讯息都被翻出来”，“他们有枪，你不给查就会被抓，他说你有罪就有罪，可以为求政绩随便强加一个罪名，审判过程更是草率了事，除非你有特别背景”。
 </p>
 <p>
  Mark说，大陆人为求自保，一般不敢“硬碰”，而是“斗智斗力”。比如，“以乘坐地铁来说，我们可能会带两支手机，一支给警察查，又或是不带手机”。
 </p>
 <p>
  Mark还以一名IT人的身份，呼吁港人勿使用内地应用程式，“微信是很危险”。他强调“这是政权监控手段”，他自己也会尽量使用现金，以避过监控。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    元明清
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2977660.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2977660.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_zgxw/n2977660.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/21/n2977660.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

