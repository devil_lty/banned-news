### 【故事新编大家听】帝尧的故事04-帝喾四子  姜嫄生后稷
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2017/01/gushixinbian-600x400.jpg"/>
 </div>
</div>
<hr/>
<div class="content">
 <p class="content-info-datetype">
  <span class="content-info-date">
   【希望之声2019年5月4日】
  </span>
 </p>
 <div class="soh-audio-player status-published">
  <div class="jp-jplayer" id="audio-player-0">
  </div>
  <div aria-label="media player" class="jp-audio" data-url-h="http://media.soundofhope.org/audio03/2019/05/04-4.mp3" data-url-l="http://media.soundofhope.org/16K-mp3/audio03/2019/05/04-4_16K.mp3" id="audio-player-container-0" role="application">
   <div class="panel">
    <div class="control">
     <div class="button pnp">
      <i aria-hidden="true" class="fa fa-play">
      </i>
     </div>
     <div class="slider-progress">
     </div>
     <div class="button mute">
      <i aria-hidden="true" class="fa fa-volume-up">
      </i>
     </div>
     <div class="slider-vol">
     </div>
     <div class="button download">
      <span href="http://media.soundofhope.org/16K-mp3/audio03/2019/05/04-4_16K.mp3">
       <i aria-hidden="true" class="fa fa-cloud-download">
       </i>
      </span>
     </div>
     <div aria-label="..." class="btn-group bps" role="group">
      <button class="btn btn-default bps-l active" type="button">
       16K
      </button>
      <button class="btn btn-default bps-h" type="button">
       128K
      </button>
     </div>
    </div>
   </div>
  </div>
 </div>
 <p>
  <b>
   <i>
    * 收听点选128K，感受更好音质 *
   </i>
  </b>
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  听众朋友您好！欢迎您来到‘故事新编大家听’节目！我是雪莉, 我是东方。
 </p>
 <p>
  <span style="font-weight: 400;">
   上集我们讲到帝尧出世。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   今天我们给大家讲讲尧的几个哥哥，也就是帝喾的另外几个儿子。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   前面我们给大家讲过，起初，帝喾的几个妃子，除了四妃常仪生了一儿一女，别人都没有生子女。后来，在尧之前，帝喾的正妃，生了一个儿子，
  </span>
  <span style="font-weight: 400;">
   这也是挺有意思的一个故事，在历史上也是很有名的
  </span>
  <span style="font-weight: 400;">
   。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   先说说帝喾的正妃。前面我们讲过了，她是有邰氏的女儿，名叫姜嫄。嫁给帝喾多年没有生育。据说她已经46岁了，还没有孩子。有一次她随同帝喾去祭祀女娲庙。同时也拜求女娲神赐给她一个儿子。
  </span>
  <span style="font-weight: 400;">
   刚到庙门不多几步，只见路旁泥土地上面有一个极大脚印在那里，五个脚趾看的非常明显，那个脚印足有好几尺长，就是那个大脚趾头，比平常人的整个脚也还要大些。看那个脚印的方向，足跟在后，五趾冲着庙门，应该是走进庙去的时候所踏的。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   那时，帝喾正在仔细看那庙宇的结构，仰着头没有留心。姜嫄低头而行，早一眼看见了，诧异之极，暗想：“天下竟有这样大的脚，那么这个人不知道有怎样大呢，可惜不曾看见。”正在想着，一面脚步慢移，不知不觉一脚踏到那大人的脚迹上去了，所踏的恰恰是大拇指。哪知一踏着之后，姜嫄如同感受了电气一般，顷刻间觉得神飞心荡，
  </span>
  <span style="font-weight: 400;">
   一时如醉
  </span>
  <span style="font-weight: 400;">
   如痴，如梦如醒，几乎要想卧到地上去。过了好一会儿。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   晚上回来姜嫄这夜就做了一个梦，梦见一个极长大的人向她说道：“我是天上的东方苍帝，那庙宫前面的大脚印迹就是我的脚印。你踏着我的大拇指，真是和我有缘。我奉女娲娘娘之命同你做了夫妻，你如今已有孕了，可知道吗？”
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   姜嫄梦中听了又羞又怕，不觉豁然而醒，心里想想，越发诧异，心想是不是妖邪呢？但是又不好意思对帝喾说，只得藏在肚里。
  </span>
 </p>
 <div>
 </div>
 <p>
  <span style="font-weight: 400;">
   日月如梭，很快十月怀胎，一朝分娩。不到半个时辰，小儿落地。
  </span>
  <span style="font-weight: 400;">
   姜嫄一点没有受到苦痛 。生了一个男孩，大家都很高兴
  </span>
  <span style="font-weight: 400;">
   。 姜嫄当时住在娘家。她心里还想着那个脚印和后来的那个梦，心里觉得是不祥之物，就一定非要把这个孩子扔了不可，可又不说为什么。家人拗不过她，就派侍女把孩子扔到了宫门外小巷中。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   那时正是冬天，没想到不知道哪里跑出来的许多牛羊，过来把孩子遮挡住，不让他受风寒，还有一只母牛给婴儿喂奶。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   街上的人们看见了，都觉得很稀奇，纷纷传告，许多人都跑去看。姜嫄又命人把孩子再扔的远点，扔到野外去。
  </span>
  <span style="font-weight: 400;">
   结果人们又发现，竟然树林子里跑出了母狼在给孩子喂奶！
  </span>
  <span style="font-weight: 400;">
   宫人觉得这个孩子太不寻常了，就又给抱了回来。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   可是姜嫄对大家说的就是不听不信。非要自己亲自试过，亲眼看见， 方才相信。于是又让仆人把这个孩子放到自己住的院子里的一个大水池子上。那时正是寒冬，那个大水池子已经连底冻上了，她叫人把这孩子的棉衣全部脱去，单剩小衣，抛在冰上，自己坐在屋里面看，说如果有一个时辰不冻死，她就抚养他。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   大家都想，如此寒天，我们大人穿了重裘还嫌冷的难受。 何况一个新生婴儿，可以单衣卧冰吗？何况一个时辰，就是现在的两个小时啊。人们都想，完了，这孩子没命了！
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   哪知刚把孩子放下去，忽听得空中一阵拍拍之声，满个院子顿时墨黑。大家都吃了一惊，不知怎么回事，仔细一看，却是无数大鸟纷纷的扑到池中，或是用大翅膀垫在孩子的下面；或是用大翅膀遮盖孩子的上面，团团圈圈，围得来密不通风，一齐伏着不动，足有一个时辰之久，把人们都看得呆了。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   姜嫄在房中尤其诧异之至，才相信前两次大家说的不是假的。心里正在追悔，忽然又是一阵拍拍之声，
  </span>
  <span style="font-weight: 400;">
   只见那些大鸟一霎
  </span>
  <span style="font-weight: 400;">
   都已飞去
  </span>
  <span style="font-weight: 400;">
   ，那孩子在冰上禁不住这股寒气，呱的一声，方才哭起来了。那哭声宏亮异常，差不多连墙外路上都能听见。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   宫人急忙把孩子抱起，就裹在怀里，走进来向姜嫄说道：“正妃娘娘，请抱他一抱，这个孩子要冻坏了！”
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   姜嫄此时又是惭
  </span>
  <span style="font-weight: 400;">
   愧
  </span>
  <span style="font-weight: 400;">
   ，又是感激，又是懊悔，又是心疼，禁不住一阵心酸，那眼泪竟同珠子一样簌簌的落下来。早有宫人递过小孩的衣服，给他穿好，姜嫄就抱在怀中，从此以后，用用心心的抚养他了。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   帝喾后来知道了这事，私底下问姜嫄为何一而再，再而三的要抛弃这个孩子？姜嫄就把那个大脚印和梦的事情说了，说自己原来以为是妖异。帝喾一听就笑了。说：‘老实和你说，这个不是妖异，正是个祥瑞啊。’
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   <br/>
  </span>
  <span style="font-weight: 400;">
   当初伏羲太昊帝的母亲毕胥就是和你一样，踏了大人脚迹而有孕的。即如母后生朕我，亦是因为踏了大人脚迹才有孕的。你要是不相信，回到亳都之后去问问母后，就知道了。你不要多想， 这是祥瑞，不是妖异。”也因为这，帝喾就给这个孩子起了个名字叫‘弃’， 几次三番被抛弃的意思。 姜嫄听了这番话，方才明白。从此之后，胸中才一无芥蒂。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   《列女传·母仪·弃母姜嫄》中讲到这件事，说的是姜嫄性情恬静专一
  </span>
  <span style="font-weight: 400;">
   ，爱好农事种庄稼。随着儿子弃渐渐长大，她就教他农桑，弃聪明而且仁厚，尽得母亲的教诲。尧要他管理农桑。到了舜帝的时候，仍旧是负责百姓的农时百谷。 弃的子孙后代一直居住在‘稷’
  </span>
  <span style="font-weight: 400;">
   这个地方。所以也称为后稷。后来的周朝，就是弃的子孙。就是说， 弃是周朝的始祖。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   这就是后稷出生的经过。也是尧的一个兄长。尧还有一个兄长，是帝喾的次妃，名叫简狄的生的。那也是一个非常离奇的故事。古书记载简狄吃了一个鸟蛋而怀孕生下了尧的另一个哥哥，名字叫契 . 契出生的时候很奇特，是从母亲的胸部生出来的。这也是一个挺有意思的故事，我们就不讲了。将来有机会在说吧。
  </span>
 </p>
 <p>
  <span style="font-weight: 400;">
   至此，我们知道，帝喾的四个妃子，各个都生了一个儿子：正妃姜嫄生弃；次妃简狄生契 ；三妃庆都生尧；四妃常仪生挚；但是挚却是长子。而尧是最小的儿子。  下面我们就要讲帝喾之后的事情了。 请您继续收听：尧的故事第五集：
  </span>
  <b>
   帝喾仙去，挚继帝位
  </b>
 </p>
 <p>
  <span style="font-weight: 400;">
   好，听众朋友，我们今天的故事，就讲到这里。谢谢您的收听。我是东方，
  </span>
  <span style="font-weight: 400;">
   我是雪莉。 我们下次节目再见。
  </span>
 </p>
 <p>
  <strong>
   <em>
    紫君根据钟毓龙《上古神话演义》编辑整理
   </em>
  </strong>
 </p>
 <p>
  ==============
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2019/04/10/n2797182.html">
   【故事新编大家听】
   <strong>
    帝尧的
   </strong>
   故事01-从帝喾说起
  </span>
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2019/04/19/n2812113.html">
   【故事新编大家听】
   <strong>
    帝尧的
   </strong>
   故事02-赤帝之精生于翼
  </span>
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2019/04/27/n2841072.html">
   【故事新编大家听】
   <strong>
    帝尧的
   </strong>
   故事03-赤帝之精
  </span>
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2019/05/11/n2877054.html">
   【故事新编大家听】帝尧的故事05-帝喾仙去 挚继帝位
  </span>
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2019/04/01/n2773008.html">
   【故事新编大家听】鹊鸟报恩鸣冤
  </span>
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2019/02/25/n2678431.html">
   【故事新编大家听】俞伯牙与钟子期
  </span>
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2019/02/18/n2659852.html">
   【故事新编大家听】约伯与神的故事
  </span>
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2019/02/03/n2618677.html">
   【故事新编大家听】俞净意公遇灶神记
  </span>
 </p>
 <p>
  <span href="https://www.soundofhope.org/gb/2018/12/18/n2488846.html">
   【故事新编大家听】张尚书善解父子恶缘
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/gb/2018/10/21/n2292372.html">
   【故事新编大家听】
  </span>
  <span href="https://www.soundofhope.org/gb/2018/12/04/n2449441.html">
   吕大朗还金完骨肉；吕二朗卖嫂失妻
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/gb/2018/10/21/n2292372.html">
   【故事新编大家听】她曾是日本历史上最美丽女子
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/gb/2018/09/24/n2204379.html">
   【故事新编大家听】法海禅师的故事
  </span>
 </p>
 <div>
  &gt;&gt;
 </div>
 <div>
  <p>
   <strong>
    更多故事请看：
   </strong>
  </p>
  <p>
   <strong>
    <span href="http://www.soundofhope.org/gb/category/%e5%8f%a4%e4%bb%8a%e6%96%87%e5%8c%96/%e6%96%87%e5%8c%96%e5%af%bb%e6%a0%b9/%e6%95%85%e4%ba%8b%e6%96%b0%e7%bc%96%e5%a4%a7%e5%ae%b6%e5%90%ac">
     故事新编大家听
    </span>
   </strong>
  </p>
 </div>
 <p>
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    紫君
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_whxg/n2856990.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_whxg/n2856990.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_whxg/n2856990.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/05/04/n2856990.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

