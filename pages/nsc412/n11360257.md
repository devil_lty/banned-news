### 【新闻看点】“我能过来吗？” 川普惊世之举
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="North Korea's leader Kim Jong Un and US President Donald Trump walk together south of the Military Demarcation Line that divides North and South Korea, after Trump briefly stepped over to the northern side, in the Joint Security Area (JSA) of Panmunjom in the Demilitarized zone (DMZ) on June 30, 2019. (Photo by Brendan Smialowski / AFP)" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1152948689-600x400.jpg"/>
<div class="red16 caption">
 “我可以过来吗？”川普一句话，金正恩赶紧做了邀请的姿势，并且说“这将是莫大的荣幸”。6月30日在韩朝非军事区，川普向朝鲜一边走了20步。(BRENDAN SMIALOWSKI/AFP/Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月03日讯】大家好，欢迎大家关注
 <span href="http://www.epochtimes.com/gb/tag/%E6%96%B0%E9%97%BB%E7%9C%8B%E7%82%B9.html">
  新闻看点
 </span>
 ，我是李沐阳。
</p>
<p>
 历史，往往是在看似不经意的时候创造。
</p>
<p>
 川普前天（6月30日）靠着一个“简单”的推特和一句问话，轻巧地逾越了看似几乎不可能逾越的障碍，成了历史上第一位踏足朝鲜领土的现任美国总统。川普在朝鲜境内的前后40步，给曾经陷入僵局的半岛无核化谈判注入了新的动力。不过也有一方（对此感觉）“酸溜溜”。
</p>
<h4>
 川普推特约见
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%91%E6%AD%A3%E6%81%A9.html">
  金正恩
 </span>
 喜出望外
</h4>
<p>
 缘起于川普在G20峰会期间的一则推文。（川普在推文中表示，）鉴于自己已经在日本参加峰会，如果
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%91%E6%AD%A3%E6%81%A9.html">
  金正恩
 </span>
 也有兴趣，双方可以见个面。哪怕是握握手，彼此问候一声。
</p>
<p>
 国际间、特别是“敌对国”之间的一场领导人会面，川普采用了一种“朋友式”的邀请。这也正是他非常富有个人特色的外交风格，历史上的美国总统从来没有尝试过，甚至都没曾想过。而川普说得非常轻松，就好像到镇上逛街，顺便到老朋友那里转一圈。
</p>
<p>
 金正恩当然接受了川普的邀请，一天之后，两位“自带戏码”的领导人在板门店会面了。隔着韩朝边界，川普和金正恩的手握在了一起。
</p>
<p>
 喜出望外的金正恩告诉川普，“从没想过会在这个地方见到你。”“在这里会面，两个曾有敌意的国家向全世界展示，我们有一个新的现在，也将会有一个积极的会谈。”
</p>
<h4>
 “我可以过来吗？” 创造历史一刻
</h4>
<p>
 随即，川普似乎丝毫没有考虑历史背景，问了一句：“我可以过来吗？”
</p>
<p>
 这句美国人之间征求意见的普通问话，让金正恩显得有些吃惊。金正恩根本想不到川普会提出踏足朝鲜的要求，谁又能想的到呢？
</p>
<p>
 金正恩赶紧做了邀请的姿势，并且说“这将是莫大的荣幸”。历史性的一刻，就在此时诞生了。川普跨过了世界上封锁最严密的“三八线”，一步就跨越了历史，并朝着朝鲜一边走了20多米。
</p>
<p>
 当时在场的福克斯资深新闻记者、主持人塔克·卡尔森（Tucker Carlson）表示，“川普还是那样乐观，因为（美国总统进入朝鲜）确实是前所未有的事情。如果不是川普总统，不是他这种与众不同的执政和思考方式，这件事可能不会发生”。
</p>
<p>
 卡尔森还表示，看上去金正恩似乎“不健康”，而川普总统“气场强大，掌控着两人的会晤”。他认为，跟金正恩交往，体现了川普总统的“政治智慧”，他是“很真诚地想跟金正恩发展关系”。
</p>
<h4>
 川普“突发奇想”世界震动
</h4>
<p>
 外界曾有猜测，说双方早已有沟通，并且为这次的会晤做了比较多的准备。但金正恩确认，他是在前一天（6月29日）才收到川普想会面的信息，然后当天下午双方才通过官方渠道确认了这件事。川普在与金正恩会谈中也表示，自己是“突发奇想”。
</p>
<p>
 就这么简单，一点都不隆重，就像是朋友和邻居之间的“串门”。但德国之声认为，这却传递出一个信号：不要再三考虑，直接赶快行动，“百说不如一做”，灵活的首脑外交已经超越了职业外交的常规思维。
</p>
<p>
 一次“不经意的邂逅”，被美韩朝三方上升到了历史性的高度。看看川普和金正恩在会谈中的表情，可以想见，在韩国一侧的会谈气氛是比较欢快的。
</p>
<p>
 川普也向还在“懵头、吃惊”中的金正恩发出了邀请，如果有时间，也有兴趣，可以到华盛顿去访问。
</p>
<p>
 三国领导人达成共识，今后一段时间内，将在工作层面上建立一套模式。继续谈判，目标依然是半岛彻底无核化，最终签署和平条约。不过对朝鲜的严厉制裁依然全面有效，并没有因为这次“邂逅”而发生改变。
</p>
<p>
 与金正恩握手并跨越“三八线”，虽然不能说美朝之间已经完全“化干戈为玉帛”，但其中的收益也是不言而喻的。川普用“非常规”的手段，极短的时间当中，超越了过去几十年的成就。仅此一点，国际社会还没有第二个领导人有这样的魄力。
</p>
<h4>
 川普三大收益 北京角色淡化
</h4>
<p>
 吉林大学公共外交学院副院长孙兴杰在《金融时报》撰文表示，第一层收益是外界公认的，川普是二战结束以来，最会处理朝鲜半岛问题的美国总统，这个形象得到了巩固。川普的这个政绩，为争取连任已经增加了无限量的筹码。
</p>
<p>
 第二层收益就是维持美朝关系的现状，通过巩固和金正恩之间的私人关系，维持半岛基本稳定。
</p>
<p>
 第三层收益就是彰显了美国在朝鲜半岛的绝对主导地位。
</p>
<p>
 特别是这个第三层收益，让中共“不是滋味”， 有那么一种“酸溜溜”的感觉。
</p>
<p>
 众所周知，前不久习近平曾访问朝鲜，受到了“超高规格”的接待，但是最后双方没有签署联合公报。而川普的一个推特、一句问话和一小步，改变了美朝之间的关系，举重若轻。
</p>
<p>
 圣汤姆斯大学国际研究中心叶耀元教授对自由亚洲表示，川普灵活的外交风格使美朝关系得到了升温，而中朝关系则有所退步。这表明“朝鲜渐渐脱离中国（中共）的控制，美国也正向中方发出不需要通过中国来接触朝鲜的讯号”。
</p>
<p>
 很明显，川金三会是临时起意，没有预约，所以中共完全不知情。川普主动出击，将中共在朝鲜问题中的角色淡化了许多。
</p>
<p>
 俄克拉荷马中部大学国际问题专家李小兵表示，“川普的强人外交打破了过去很多边缘政策、多边外交的纠葛，使很多‘第三者’的干涉或者三角形的‘恋爱’变得非常困难。”
</p>
<p>
 《国家利益》也认为，川普在力图甩开中共在朝鲜外交中的作用，这是“非常正确的一步”。文章表示，金正恩咧开嘴笑个不停，但是有些人笑不出来。川普对金正恩的赞美以及他对普京的慷慨赞扬，如果把这些放在美国要孤立中共的大背景下，“就是非常有意义的举措”。
</p>
<p>
 好的，感谢您关注
 <span href="http://www.epochtimes.com/gb/tag/%E6%96%B0%E9%97%BB%E7%9C%8B%E7%82%B9.html">
  新闻看点
 </span>
 ，再会。
</p>
<p>
 大纪元《新闻看点》制作组  #
</p>
<p>
 责任编辑：李昊
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360257.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360257.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360257.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/2/n11360257.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

