### 组团：川普独立日阅兵 展示哪些先进军机
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153792667-600x400.jpg"/>
<div class="red16 caption">
 2019年7月4日，美国总统川普在华盛顿特区林肯纪念堂举行“向美国致敬”国庆庆祝活动，人们聚集在国家广场上，蓝天使的6架F-18战机飞过头顶。(ANDREW CABALLERO-REYNOLDS / AFP)        (Photo credit should read ANDREW CABALLERO-REYNOLDS/AFP/Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月05日讯】7月4日是美国独立纪念日（也叫国庆日），下午6点40分，美国总统川普（特朗普）在林肯纪念堂（Lincoln Memorial）阅兵并发表演讲，同时美军各类先进军机在空中表演，展示美军强大的空中作战能力。
</p>
<p>
 这也是美国自1991年以来首次大型阅兵。随着21响礼炮的巨响，一架代表“空军一号”波音飞机飞越林肯纪念堂，阅兵式正式开始。
</p>
<figure class="wp-caption aligncenter" id="attachment_11365726" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153792697.jpg">
  <img alt="" class="size-large wp-image-11365726" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153792697-600x434.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年7月4日，美国总统川普在华盛顿特区林肯纪念堂举行“向美国致敬”国庆庆祝活动，人们聚集在国家广场上，蓝天使的6架F-18战机飞过头顶。(ANDREW CABALLERO-REYNOLDS/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11365725" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153792668.jpg">
  <img alt="" class="size-large wp-image-11365725" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153792668-600x415.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年7月4日，美国总统川普在华盛顿特区林肯纪念堂举行“向美国致敬”国庆庆祝活动，人们聚集在国家广场上，蓝天使的6架F-18战机飞过头顶。(ANDREW CABALLERO-REYNOLDS/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<p>
 川普在演讲中逐个介绍美军五大军种。作为三军统帅，他率先介绍了海岸警卫队，接下来依次是：空军、海军、海军陆战队，以及陆军。他在演讲中还提及打造太空军队的计划。
</p>
<figure class="wp-caption aligncenter" id="attachment_11365715" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790565.jpg">
  <img alt="" class="size-large wp-image-11365715" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790565-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年7月4日，美国总统川普在华盛顿特区林肯纪念堂举行“向美国致敬”国庆庆祝活动，人们聚集在国家广场上，蓝天使的6架F-18战机飞过头顶。(MANDEL NGAN/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 现场也依次展示了五大军种的部分先进军事装备。包括：B-2“幽灵”隐身轰炸机、2架F-22战机、4架阿帕奇武直、2架F-35战机、2架F/A-18“大黄蜂”战机、H-60/65直升机等。
</p>
<figure class="wp-caption aligncenter" id="attachment_11365723" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790792.jpg">
  <img alt="" class="size-large wp-image-11365723" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790792-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年7月4日，美国总统川普在华盛顿特区林肯纪念堂举行“向美国致敬”国庆庆祝活动，人们聚集在国家广场上。 (Susan Walsh/POOL/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 白宫公布了讲话稿的节选。下面是这些段落的中文翻译：
</p>
<p>
 “今天，我们作为同一个国家走到一起，以这种非常特殊的方式向美国致敬。我们庆祝我们的历史、我们的人民和自豪地捍卫我们的国旗的英雄们，——美国军队勇敢的男女官兵！
</p>
<p>
 “鼓舞建国先贤的美国精神在我们的历史中始终让我们保持坚强。直到今天，这种精神仍然流淌在每一位美国爱国者的血脉中，珍藏在你们每一个人的心中。……
</p>
<p>
 “今天，就像243年前一样，美国自由的未来靠那些挺身捍卫它的人来肩负。”
</p>
<p>
</p>
<figure class="wp-caption aligncenter" id="attachment_11365720" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790704.jpg">
  <img alt="" class="size-large wp-image-11365720" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790704-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  两架F-35（L和R）和两架F-18飞过头顶。 (MANDEL NGAN/AFP)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11365762" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153784420.jpg">
  <img alt="" class="wp-image-11365762 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153784420-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  一架B-2幽灵轰炸机（中）和两架F-22战机飞过头顶。(Brendan Smialowski/AFP)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11365718" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790696.jpg">
  <img alt="" class="size-large wp-image-11365718" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790696-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  一架B-2幽灵轰炸机（中）和两架F-22战机飞过头顶。 (MANDEL NGAN/AFP)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11365729" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153792699.jpg">
  <img alt="" class="size-large wp-image-11365729" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153792699-600x411.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  直升机飞过头顶。(ANDREW CABALLERO-REYNOLDS/AFP)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11365712" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790441.jpg">
  <img alt="" class="size-large wp-image-11365712" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790441-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  一架新的海军陆战队一号直升机（中）和两架鱼鹰式倾转旋翼机飞过头顶。 (Brendan Smialowski/ AFP)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11365716" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790687.jpg">
  <img alt="" class="size-large wp-image-11365716" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790687-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  直升机飞掠。(Brendan Smialowski/AFP)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11365713" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790529.jpg">
  <img alt="" class="size-large wp-image-11365713" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790529-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  美国海军飞行演示中队蓝天使在表演。 (Nicholas Kamm/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 责任编辑：夏雨
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11365352.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11365352.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11365352.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/4/n11365352.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

