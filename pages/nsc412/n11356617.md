### 10岁女孩在《美国达人秀》唱歌剧 惊倒众人
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/Fotolia_104591643_Subscription_L-600x400.jpg"/>
<div class="red16 caption">
 <p>
  在2019年《美国达人秀》的初赛中，10岁女孩比莎演唱歌剧曲目，令众人惊讶不已。图为乐谱，与本文无关。（Fotolia）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月01日讯】（大纪元记者陈俊村报导）在2019年《
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD%E8%BE%BE%E4%BA%BA%E7%A7%80.html">
  美国达人秀
 </span>
 》（America’s Got Talent）的初赛中，10岁
 <span href="http://www.epochtimes.com/gb/tag/%E5%A5%B3%E5%AD%A9.html">
  女孩
 </span>
 比莎（Emanne Beasha）
 <span href="http://www.epochtimes.com/gb/tag/%E6%BC%94%E5%94%B1.html">
  演唱
 </span>
 一首著名的
 <span href="http://www.epochtimes.com/gb/tag/%E6%AD%8C%E5%89%A7.html">
  歌剧
 </span>
 曲目，让评审惊呼，这美妙的歌声如何从她娇小的身躯中发出？
</p>
<p>
 比莎来自佛罗里达州，看起来活泼外向。她上节目时还不忘带着独角兽玩偶，也就是她的幸运物。
</p>
<p>
 尽管满脸笑嘻嘻的，但比莎在上台时还是显得相当紧张。她表示，他的父亲是一家冰淇淋工厂的老板。对她而言，这显然是世界上最好的工作，而她当然也喜欢吃冰淇淋。
</p>
<p>
 在表演一开始时，比莎收拾起笑容，开始调整心情。随着音乐声响起，她引吭高歌，唱起了广为人知的《
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%AC%E4%B8%BB%E5%BD%BB%E5%A4%9C%E6%9C%AA%E7%9C%A0.html">
  公主彻夜未眠
 </span>
 》（Nessun dorma）这首歌曲。
</p>
<p>
 《
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%AC%E4%B8%BB%E5%BD%BB%E5%A4%9C%E6%9C%AA%E7%9C%A0.html">
  公主彻夜未眠
 </span>
 》是意大利作曲家普契尼（Giacomo Puccini）为
 <span href="http://www.epochtimes.com/gb/tag/%E6%AD%8C%E5%89%A7.html">
  歌剧
 </span>
 《杜兰朵》（Turandot）所创作的咏叹调，由男主角卡拉富王子（Calaf）
 <span href="http://www.epochtimes.com/gb/tag/%E6%BC%94%E5%94%B1.html">
  演唱
 </span>
 ，诉说杜兰朵公主要全城彻夜不睡，替她寻找卡拉富的名字的情节。
</p>
<p>
 当比莎演唱到曲中的高潮时，现场观众和评审不由自主地起立为她鼓掌和欢呼，不少人面露惊讶和折服的表情。
</p>
<p>
 比莎的表演让人想起曾在2010年赢得《
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD%E8%BE%BE%E4%BA%BA%E7%A7%80.html">
  美国达人秀
 </span>
 》亚军的10岁
 <span href="http://www.epochtimes.com/gb/tag/%E5%A5%B3%E5%AD%A9.html">
  女孩
 </span>
 伊万科（Jackie Evancho）。她也曾在比赛中演唱《公主彻夜未眠》。
</p>
<p>
 而针对比莎的表演，评审哈克（Julianne Hough）惊叹道：“这声音如何从那么小的身躯中发出来？哇！”
</p>
<p>
 评审考威尔（Simon Cowell）接着说：“我说真的，那真的令人难以置信。好一个令人惊奇的人啊！”
</p>
<p>
 而比莎最终以其优异的演出获得晋级下一回合比赛的机会。
</p>
<p>
</p>
<p>
 责任编辑：茉莉
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11356617.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11356617.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11356617.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/1/n11356617.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

