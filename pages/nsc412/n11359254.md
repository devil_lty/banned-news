### 伊朗铀存量超限 白宫：绝不允许其发展核武
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/111.jpg"/>
<div class="red16 caption">
 <p>
  6月24日，美国总统川普签署了行政令，对伊朗最高领导人进行严厉制裁。 (MANDEL NGAN/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月02日讯】（大纪元记者徐简综合报导）周一（7月1日），联合国原子能监督机构证实，
 <span href="http://www.epochtimes.com/gb/tag/%E4%BC%8A%E6%9C%97.html">
  伊朗
 </span>
 的低浓缩铀库存已经超出了国际协议规定的上限。当天白宫发表声明，重申了
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD.html">
  美国
 </span>
 对这个问题的立场，那就是美国永远不允许伊朗发展核武器。
</p>
<h4>
 白宫：
 <span href="http://www.epochtimes.com/gb/tag/%E4%BC%8A%E6%9C%97.html">
  伊朗
 </span>
 政权必须结束核野心
</h4>
<p class="p1">
 <span class="s1">
  川普总统说，伊朗正在“玩火”。当被记者问道，伊朗违反了
  <span href="http://www.epochtimes.com/gb/tag/%E6%A0%B8%E5%8D%8F%E8%AE%AE.html">
   核协议
  </span>
  ，川普总统是否要对他们发出信息，川普说，他不用发出什么信息，因为伊朗人知道自己在干些什么，那就是在“玩火”。
 </span>
</p>
<p class="p1">
 <span class="s1">
  周一早些时候，白宫发表声明，重申了
  <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD.html">
   美国
  </span>
  在这个问题上的立场，即绝对不允许伊朗发展核武器。
 </span>
</p>
<p class="p1">
 <span class="s1">
  白宫的声明说，“国际社会必须恢复对伊朗的永久不扩散（核武器）标准， 美国及其盟国永远不会允许伊朗发展核武器。”
 </span>
</p>
<p class="p1">
 <span class="s1">
  “（美国）将持续对伊朗政权施加最大压力，直到其领导人改变路线。 伊朗政权必须结束其核野心及其恶行。”声明说。
 </span>
</p>
<p class="p3">
 <span class="s1">
  当天以色列总理内塔尼亚胡说，伊朗的举动表示，该国朝着制造核武器迈出了重要一步。他敦促欧洲国家对伊朗施加制裁。
 </span>
</p>
<p class="p3">
 <span class="s1">
  英国外交大臣亨特（Jeremy Hunt）对媒体说，英国当初之所以签订
  <span href="http://www.epochtimes.com/gb/tag/%E6%A0%B8%E5%8D%8F%E8%AE%AE.html">
   核协议
  </span>
  ，是希望控制伊朗的核活动，现在如果伊朗违反核协议，那英国也要退出该协议。
 </span>
</p>
<h4 class="p3">
 <span class="s1">
  伊朗低浓缩铀储量超上限 违反核协议
 </span>
</h4>
<p class="p3">
 <span class="s1">
  周一，伊朗外长扎里夫对媒体说，“伊朗低浓缩铀储量已经越过了300公斤，超过了国际社会给伊朗浓缩铀规定的上限。”
 </span>
 <span class="s1">
  监督伊朗是否遵守核协议的联合国原子能机构证实，伊朗的确超过了协议规定的浓缩铀限制。
 </span>
</p>
<p class="p3">
 <span class="s1">
  2015年的《伊朗核协议》由伊朗与美、英、法、德、俄、中及欧盟共同签署，此协议旨在通过减缓经济制裁来换取伊朗约束核计划。2018年美国退出《伊朗核协议》。
 </span>
</p>
<p class="p3">
 <span class="s1">
  根据协议， 伊朗核设施的低浓缩铀存量不得超出300公斤的上限，伊朗重水的库存不能超过130吨。在15年内伊朗不得生产浓度超过3.67%的浓缩铀。
 </span>
</p>
<p class="p3">
 <span class="s1">
  对伊朗来说，虽然该协议使得其核设施功能受到了限制，但合法地保留了这些设施，也就是说，伊朗制造核弹的能力得到了有核国家的默许。川普说，该协议的核心存在缺陷。
 </span>
</p>
<p class="p3">
 <span class="s1">
  上周一，针对美国无人侦察机被伊朗击落事件，川普宣布了美国对伊朗的新制裁，针对伊朗最高领导人哈梅内伊（Ayatollah Ali Khamenei）及其同伙实施“严厉打击”，卡断他们的金融工具。美国国务卿蓬佩奥也访问中东地区，跟盟友协商如何建立抵制伊朗的全球联盟。#
 </span>
</p>
<p>
</p>
<p class="p3">
 责任编辑：林琮文
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11359254.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11359254.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11359254.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/2/n11359254.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

