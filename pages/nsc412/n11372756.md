### 美新设人权委员会 审查人权在外交中的作用
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1160779173-600x400.jpg"/>
<div class="red16 caption">
 <p>
  7月8日，美国国务卿蓬佩奥（Mike Pompeo）与哈佛教授哈佛法学教授玛丽·安·葛兰顿（Mary Ann Glendon）一同宣布成立一个新的委员会，根据“自然法和自然权利”，重新审视人权在美国外交政策中的作用。(Mark Wilson/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月09日讯】(大纪元记者林南编译报导）美国国务卿
 <span href="http://www.epochtimes.com/gb/tag/%E8%93%AC%E4%BD%A9%E5%A5%A5.html">
  蓬佩奥
 </span>
 （Mike Pompeo）周一（8日）宣布国务院成立一个
 <span href="http://www.epochtimes.com/gb/tag/%E4%BA%BA%E6%9D%83.html">
  人权
 </span>
 委员会，该委员会旨在审查“人权在美国外交政策中的作用”。
</p>
<p>
 这个委员会由多名顾问组成，他们将“对世界上不可剥夺的权利进行最深刻的审查”。
 <span href="http://www.epochtimes.com/gb/tag/%E8%93%AC%E4%BD%A9%E5%A5%A5.html">
  蓬佩奥
 </span>
 说，“世界各地的严重侵犯行为仍在继续，有时甚至以
 <span href="http://www.epochtimes.com/gb/tag/%E4%BA%BA%E6%9D%83.html">
  人权
 </span>
 为名。”
</p>
<p>
 蓬佩奥没有透露有关该委员会运作的具体细节。该委员会将由哈佛法学教授玛丽·安·葛兰顿（Mary Ann Glendon）担任主席，她曾在乔治·布什政府期间担任驻梵蒂冈大使，也是一位著名的反堕胎倡导者。
</p>
<p>
 根据CBS新闻，
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD%E5%9B%BD%E5%8A%A1%E9%99%A2.html">
  美国国务院
 </span>
 高级官员表示，该小组的运作方式就像一个“研究小组”，审视普世人权的概念，这些权利来自于何处，以及与生俱有的权利与政府规定的权利之间的区别。
</p>
<p>
 蓬佩奥周一称赞美国前第一夫人埃莉诺·罗斯福（Eleanor Roosevelt）推动的1948年“
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%96%E7%95%8C%E4%BA%BA%E6%9D%83%E5%AE%A3%E8%A8%80.html">
  世界人权宣言
 </span>
 ”，他说，该人权宣言为这个新的委员会奠定了基础。
</p>
<p>
 “每隔一段时间我们就需要退后一步，认真反思我们在哪里，我们去过哪里以及我们是否朝着正确的方向前进，”他说。他还补充说，该委员会将提供有关人权在美国外交政策中的作用的知情审查。”#
</p>
<p>
 责任编辑：叶紫微
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11372756.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11372756.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11372756.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/8/n11372756.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

