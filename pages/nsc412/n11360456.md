### 硅谷高科技公司员工的中位数年薪知多少
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/e630387d8e2043f1033cadd8f6dc0680-600x400.jpg"/>
<div class="red16 caption">
 <p>
  SEC的最新报告，硅谷一些上市公司员工的中位数年薪。（来自《连线》（Wired）杂志）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月03日讯】（大纪元记者李沁一综合报导）自从去年有法案要求上市公司应该公开员工的年薪，我们知道了他们收入是多少。
</p>
<p>
 美国证券交易委员会（SEC）的一份最新报告，显示了
 <span href="http://www.epochtimes.com/gb/tag/%E7%A1%85%E8%B0%B7.html">
  硅谷
 </span>
 一些高科技上市公司员工的
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E4%BD%8D%E6%95%B0%E5%B9%B4%E8%96%AA.html">
  中位数年薪
 </span>
 。所谓中位数年薪，是指一家公司之内，一半员工的薪金超过中位数年薪，另一半的薪金则低于中位数年薪。
</p>
<p>
 报告显示，
 <span href="http://www.epochtimes.com/gb/tag/%E8%B0%B7%E6%AD%8C.html">
  谷歌
 </span>
 员工的收入最高，2018年其
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E4%BD%8D%E6%95%B0%E5%B9%B4%E8%96%AA.html">
  中位数年薪
 </span>
 为246,804美元，2017年为197,274美元，增长了近5万美元。当然，这个计算不包括与99,000名员工一起工作的许多临时工和合同工。彭博社（Bloomberg）去年披露，
 <span href="http://www.epochtimes.com/gb/tag/%E8%B0%B7%E6%AD%8C.html">
  谷歌
 </span>
 超过一半的员工不是正式员工。
</p>
<p>
 <span href="http://www.epochtimes.com/gb/tag/%E8%84%B8%E4%B9%A6.html">
  脸书
 </span>
 （Facebook）排名第2，2018年员工的中位数年薪为228,651美元。比2017年下降了1.2万美元。一位发言人对《连线》（Wired）杂志表示，不清楚脸书股价下跌的具体原因，但该公司去年确实遭遇了多次危机，包括前不久“大规模数据泄露”导致其股价暴跌。
</p>
<p>
 推特（Twitter）排名第3，2018年员工的中位数年薪为172,703美元，2017年为161,860美元，增加了近1.1万美元。
</p>
<p>
 出乎意料的是亚马逊员工的工资没有想像的那么高，2018年的中位数年薪是28,836美元，2017年是28,448美元，变化不大。这和它的大部分员工不是高薪酬的软件工程师，而是仓库管理和填写仓库订单的蓝领工人有关。
</p>
<p>
 美国证券交易委员会的报告显示，推特、Square、人力资源软件制造商Workday和图形芯片制造商英伟达（Nvidia），2018年员工的中位数年薪均超过15万美元。
</p>
<p>
 此外，来自政府的数据显示，去年旧金山湾区软件工程师的平均年薪约为14万美元，而全国的平均年薪为10.4万美元。所以，难怪旧金山湾区涌来那么多的移民。
</p>
<p>
 其实，对于
 <span href="http://www.epochtimes.com/gb/tag/%E9%AB%98%E7%A7%91%E6%8A%80%E5%85%AC%E5%8F%B8.html">
  高科技公司
 </span>
 ，把员工的薪酬与CEO的放一起对比没有什么意义。因为很多高科技公司的CEO都是创始人，他们的薪酬最低。比如，Alphabet的首席执行官，即创始人拉里•佩奇（Larry Page），和Facebook的首席执行官，也是创始人马克•扎克伯格（Mark Zuckerberg），他们的年薪，只有1美元！◇
</p>
<p>
 （此文发表于1244F期旧金山湾区新闻版）
</p>
<p>
 <b>
  <span href="https://www.facebook.com/sfdjy/" style="color: #3339ff;">
   关注我们facebook主页，及时获得更多资讯，请点这里。
  </span>
 </b>
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360456.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360456.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360456.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/2/n11360456.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

