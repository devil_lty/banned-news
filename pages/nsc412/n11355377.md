### 川普向驻韩美军讲话：你们是美国最强大资产
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-1159176694-600x400.jpg"/>
<div class="red16 caption">
 <p>
  6月30日，川普向驻韩美军发表演说。(Kim Min-Hee – Pool/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月30日讯】（大纪元记者张婷综合报导）周日（6月30日），美国总统
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E6%99%AE.html">
  川普
 </span>
 （特朗普）在访问了朝韩非军事区（DMZ），并与朝鲜领导人金正恩会面后，前往乌山空军基地（Osan Air Base），并在那里向
 <span href="http://www.epochtimes.com/gb/tag/%E9%A9%BB%E9%9F%A9%E7%BE%8E%E5%86%9B.html">
  驻韩美军
 </span>
 发表了大约35分钟的演说。
</p>
<p>
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E6%99%AE.html">
  川普
 </span>
 开始便说，今天这里有很多“伟大的人”，“伟大的军事人员”。
</p>
<figure class="wp-caption alignnone" id="attachment_11355505" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/000_1I246S-e1561900526796.jpg">
  <img alt="" class="size-large wp-image-11355505" src="http://i.epochtimes.com/assets/uploads/2019/06/000_1I246S-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  川普在6月30日对
  <span href="http://www.epochtimes.com/gb/tag/%E9%A9%BB%E9%9F%A9%E7%BE%8E%E5%86%9B.html">
   驻韩美军
  </span>
  发表讲话。(Brendan Smialowski / AFP)
 </figcaption><br/>
</figure><br/>
<p>
 接着，川普谈到了他来乌山空军基地与金正恩在DMZ会面的事情。他表示，这次“意外”会面“非常棒”。社交媒体是一个“非常强有力”的东西。川普指的是，他在临去非军事区之前，在推特上邀请金正恩在该地进行简短会面，之后很快就安排了这次会面。
</p>
<p>
 “我们在DMZ会面了，这是令人难以置信的一件事。”川普说。
</p>
<p>
 他还强调，朝鲜是一个有着“巨大潜力的”国家。“我和金主席有着良好的关系，他们在非军事区（DMZ）给我们一个很棒的简报。”川普说。
</p>
<p>
 “我实际上踏入了朝鲜（的领土），他们表示，这真的是一个历史时刻。”川普说，“我注意到，来自半岛的很多人（激动得）留下眼泪。这是一件大事。”
</p>
<p>
 在简短介绍
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E9%87%91%E4%BC%9A.html">
  川金会
 </span>
 后，川普对在场的男女军人为捍卫“美国伟大的国旗”而做出的付出表示感谢。“只要我们有伟大的士兵、水手、空军、海军，我们知道，我们的自由将永远不会死亡。”川普说。
</p>
<figure class="wp-caption alignnone" id="attachment_11355411" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/000_1I2469.jpg">
  <img alt="" class="size-large wp-image-11355411" src="http://i.epochtimes.com/assets/uploads/2019/06/000_1I2469-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  乌山空军基地的军人欢迎川普总统的到来。(Brendan Smialowski / AFP)
 </figcaption><br/>
</figure><br/>
<p>
</p>
<p>
 “代表所有的美国人，我想要感谢这里的每个人。”川普说，“你们是很特别的人，你们准备好捍卫及打败任何威胁。”
</p>
<p>
 “我们最强大的资产，我们最强大的武器将永远是你们。”川普说。
</p>
<p>
</p>
<blockquote class="twitter-tweet" data-lang="en">
 <p dir="ltr" lang="en">
  “You walk in the footsteps of American soldiers through history.” – President Donald J. Trump
  <span href="https://twitter.com/realDonaldTrump?ref_src=twsrc%5Etfw">
   @realDonaldTrump
  </span>
  speaks to service members at Osan Air Base, Republic of Korea.
  <span href="https://twitter.com/hashtag/ROKsolid?src=hash&amp;ref_src=twsrc%5Etfw">
   #ROKsolid
  </span>
  <span href="https://twitter.com/hashtag/AllianceStrong?src=hash&amp;ref_src=twsrc%5Etfw">
   #AllianceStrong
  </span>
  <span href="https://t.co/eWNmnt9Qdf">
   pic.twitter.com/eWNmnt9Qdf
  </span>
 </p>
 <p>
  — U.S. Forces Korea (@USForcesKorea)
  <span href="https://twitter.com/USForcesKorea/status/1145288599899820032?ref_src=twsrc%5Etfw">
   June 30, 2019
  </span>
 </p>
</blockquote>
<p>
 <p>
  川普在讲话期间还让国务卿蓬佩奥和川普女儿伊万卡上台和美军会面。蓬佩奥说，“很高兴和你们所有人在一起，很高兴有机会看到在这里服务的优秀男女军人。感谢你们每一天为美国的服务，上帝保佑你们。”
 </p>
 <p>
  “上帝保佑美国，上帝保佑你们每个人！”伊万卡说。
 </p>
 <p>
  她对在场的军人为了保护国家安全所付出的牺牲表示感激。
 </p>
 <p>
 </p>
 <figure class="wp-caption alignnone" id="attachment_11355410" style="width: 600px">
  <span href="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-1159176687-e1561896922170.jpg">
   <img alt="" class="size-large wp-image-11355410" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-1159176687-600x715.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   图为川普的女儿伊万卡向美军讲话。(Kim Min-Hee – Pool/Getty Images)
  </figcaption><br/>
 </figure><br/>
 <p>
  川普稍早前，在朝韩非军事区与金正恩举行了大约50分钟的会谈。川普表示，他们同意，双方各自指定团队，重新启动无核化会谈。
 </p>
 <p>
  在访问驻韩美军后，川普总统乘坐空军一号在当地晚大约7点09分从乌山空军基地起飞离开
  <span href="http://www.epochtimes.com/gb/tag/%E9%9F%A9%E5%9B%BD.html">
   韩国
  </span>
  ，结束了他为期4天的亚洲行。
 </p>
 <figure class="wp-caption alignnone" id="attachment_11355418" style="width: 600px">
  <span href="http://i.epochtimes.com/assets/uploads/2019/06/000_1I258K.jpg">
   <img alt="" class="size-large wp-image-11355418" src="http://i.epochtimes.com/assets/uploads/2019/06/000_1I258K-600x400.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   川普总统乘坐空军一号在乌山空军基地离开
   <span href="http://www.epochtimes.com/gb/tag/%E9%9F%A9%E5%9B%BD.html">
    韩国
   </span>
   。(Brendan Smialowski / AFP)
  </figcaption><br/>
 </figure><br/>
 <p>
  责任编辑：林诗远
 </p>
 <p>
 </p>
</p>
<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11355377.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11355377.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11355377.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/30/n11355377.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

