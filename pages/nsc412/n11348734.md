### 美媒揭秘：华为员工与中共军方合作研究
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/05/Huawei-1128822574-600x400-1.jpg"/>
<div class="red16 caption">
 美媒爆料，数名华为员工与中共军方人员合作研究。(WANG ZHAO/AFP/Getty Images)
</div>
<hr/><p>
 【大纪元2019年06月27日讯】（大纪元记者吴英编译报导）美媒爆料，数名
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%8E%E4%B8%BA.html">
  华为
 </span>
 员工与
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1%E5%86%9B%E6%96%B9.html">
  中共军方
 </span>
 人员合作研究，这意味着华为与军队的关系，并非如该公司所言毫无关系。
</p>
<p>
 彭博社报导，在过去的十年中，
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%8E%E4%B8%BA.html">
  华为
 </span>
 员工与
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1%E5%86%9B%E6%96%B9.html">
  中共军方
 </span>
 相关机构人员开展了至少10项研究计划，内容包括人工智能及无线电通信等。
</p>
<p>
 文章举例说，华为与中共中央军委会（武装部队的最高机构）的调查单位，共同研究在线视频评论的情绪分析及分类，以及与国防科技大学合作探索卫星图像和地理坐标的收集及分析。
</p>
<p>
 彭博社的分析，主要是调查一个在线研究数据库中已发表的期刊论文，这个数据库是中国学者和行业专家经常查阅的资料库。
</p>
<p>
 彭博社说，华为及中共军方在该线数据库总共发表了数千篇论文，目前找到10篇论文的作者是华为员工及中共军方人员，最早可追溯到2006年。
</p>
<p>
 此外，这些被挑选出来的研究项目，都是被公开披露的研究，这些论文的作者自称是华为员工，而且华为公司名称被标志在论文中的明显位置。
</p>
<p>
 至于华为是否与中共军方进行敏感、无法公开的研究项目，目前无法确认。
</p>
<p>
 华为发言人格楞‧史劳斯（Glenn Schloss）在一则消息声明中表示，否认华为与中共军方附属机构有任何研发合作或伙伴关系，且不清楚其员工以个人身份发表的研究论文。
</p>
<p>
 “华为只开发和生产符合全球民用标准的通信产品，没有为军方研发产品。”史劳斯在声明中说。
</p>
<p>
 中共军方没有回应彭博社的评论请求。
</p>
<p>
 彭博社报导说，过去几十年来，科技公司与军方人员合作是常有之事，互联网的出现也是两者合作的成果。在中共独特的经济模式下，华为一直否认其与中共军方有关系，反而启人疑窦。
</p>
<p>
 华为创始人
 <span href="http://www.epochtimes.com/gb/tag/%E4%BB%BB%E6%AD%A3%E9%9D%9E.html">
  任正非
 </span>
 具有中共军方背景，曾是中共部队的通信工程员。华为一直否认其与中共政权有任何关系。
</p>
<p>
 彭博社说，虽然这些论文无法证实华为公司与中共军方机构的关系，但至少证明华为员工与中共军方人员有合作关系，并不像华为所说的其与军方毫无瓜葛。
</p>
<p>
 中共《国家情报法》（National Intelligence Law）第七条规定，所有组织和公都必须支持、协助和合作开展国家情报工作，并保护他们所知道的国家情报工作机密。这使得西方国家担心中企是中共间谍活动的工具。
</p>
<p>
 美国政府2012年对华为进行安全风险调查，虽然没有找到明确证据表明该公司是中共的间谍工具，但结论是其设备存在网络安全风险，有许多会被黑客利用的漏洞。
</p>
<p>
 美国商务部5月中旬将华为及其68家关联实体列入出口管制黑名单，以断绝华为取得美国企业关键零部件及服务的通路。
</p>
<p>
 美国一家网络安全公司Finite State，近日独立完成对华为设备的调查。看过该报告的《华尔街日报》记者说，研究人员发现华为制造的电信设备比竞争对手的设备，更容易被黑客恶意入侵，而且华为设备的漏洞数量，是他们所见过的最大值。
</p>
<p>
 看过该报告的多位白宫官员表示，调查结果显示华为有可能公然违反了国际标准协议，故意在其产品中置入漏洞。根据该报告，华为设备被发现的一些漏洞是众所皆知的网络安全问题，要避免这些问题并不难。#
</p>
<p>
 责任编辑：叶紫微
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11348734.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11348734.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11348734.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/27/n11348734.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

