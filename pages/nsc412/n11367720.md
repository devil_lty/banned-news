### 美中安排会晤 白宫顾问重申贸易谈判底线
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1GC5F9-600x400.jpg"/>
<div class="red16 caption">
 <p>
  图为美中贸易谈判代表5月10日结束华盛顿的高层贸易谈判，中共副总理刘鹤准备离开。(SAUL LOEB / AFP)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月06日讯】（大纪元记者林燕编译报导）白宫官员周五（7月5日）表示，美、中官员近两周互通电话、安排接下来的高层贸易谈判，不确定下周是否有面对面的会谈。
</p>
<p>
 上周，美国总统川普（
 <span href="http://www.epochtimes.com/gb/tag/%E7%89%B9%E6%9C%97%E6%99%AE.html">
  特朗普
 </span>
 ）和中国国家主席习近平在日本大阪会晤，同意重启陷入僵局的贸易谈判，以及停止未来加征额外关税。5月，因中方擅自修改几个月谈判达成的核心承诺，令美中贸易谈判陷入停滞，美中双方都宣布提高部分对方进口商品的关税。
</p>
<p>
 白宫经济顾问拉里·库德洛（Larry Kudlow）周五接受彭博社以及福克斯商业网络采访时表示，美中贸易谈判团队正通过电话沟通接下来的面对面会晤，但他不确定下周是否有高级别的贸易会议。
</p>
<p>
 同时，库德洛还证实，美国贸易代表
 <span href="http://www.epochtimes.com/gb/tag/%E8%8E%B1%E7%89%B9%E5%B8%8C%E6%B3%BD.html">
  莱特希泽
 </span>
 （Robert Lighthizer）以及财政部长姆钦（Steven Mnuchin）领导美方谈判团队，中方首席谈判代表是中共副总理
 <span href="http://www.epochtimes.com/gb/tag/%E5%88%98%E9%B9%A4.html">
  刘鹤
 </span>
 。
</p>
<p>
 他同时表示，对于上周美中首脑重新承诺恢复谈判，“有对话总比没有强”。
</p>
<p>
 接下来的贸易谈判将从上次破裂的位置开始。库德洛证实，双方在过去的谈判已取得许多进展，还尚未达成协议。
</p>
<p>
 他说，媒体总是有各种各样的消息，但他想特别重申一下美方的底线与之前保持一致，即“结束知识产权盗窃、停止强制技术转让、解决黑客和网络干扰问题、取消关税和非关税壁垒、改变不平衡的贸易条件以及建立执法机制。”
</p>
<p>
 美国贸易代表办公室的一位官员也向法新社证实，正在努力安排下周高级谈判代表之间的电话会议。
</p>
<p>
 与此同时，香港《南华早报》周五报导说，美中贸易谈判将于下周在北京恢复。
</p>
<p>
 美国商务部周五公布的6月非农就业数据出现超强反弹，降低市场对美联储降息的预期，美国股市早上承压下走，随后库德洛的贸易谈判即将恢复的消息令股市做出积极反应，回吐部分损失。
</p>
<p>
 川普总统周一（1日）也告诉记者，美国和中国的谈判代表“通过电话谈了很多，他们也会见面谈。基本上（谈判）已经开始”。
</p>
<p>
 美国已对价值2,500亿美元中国商品征收25%关税，中方则对1,100亿美元美国商品征收5%到25%的报复性关税。川习会上，川普同意暂时不对剩下3,000亿美元中国商品加征额外的关税。
</p>
<p>
 设在伦敦的独立经济研究公司凯投宏观（Capital Economics）首席经济学家尼尔‧希尔林（Neil Shearing）告诉美联社，随着川习会达成暂时停火共识，投资人对
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%B8%E6%98%93%E6%88%98.html">
  贸易战
 </span>
 松了一口气，这标志着双方对峙的趋势会有所改变。
</p>
<p>
 他预计，美中贸易谈判将起伏不定，接下来12个月的走势仍然会朝向争端再度升级方向前行，因为跟产业有关的战略（问题）会非常棘手。#
</p>
<p>
 责任编辑：李缘
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11367720.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11367720.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11367720.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/5/n11367720.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

