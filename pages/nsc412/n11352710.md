### 乔治亚州弃婴命运大转弯 美国人排队领养
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-2406496-600x400.jpg"/>
<div class="red16 caption">
 乔治亚州一遭弃女婴有几百个家庭排队等着领养她。（示意图）(MOD/Getty Images)
</div>
<hr/><p>
 【大纪元2019年06月28日讯】（大纪元记者李言编辑报导）来自
 <span href="http://www.epochtimes.com/gb/tag/%E4%B9%94%E6%B2%BB%E4%BA%9A%E5%B7%9E.html">
  乔治亚州
 </span>
 有关部门的消息显示，本月早些时候在该州发现的遭遗弃女婴现在健康又开心，预计她很快就能找到一个永久的家，因为有几百个家庭表示愿意领养她 。
</p>
<p>
 据ABC新闻报导，
 <span href="http://www.epochtimes.com/gb/tag/%E4%B9%94%E6%B2%BB%E4%BA%9A%E5%B7%9E.html">
  乔治亚州
 </span>
 “家庭和儿童服务部”主任汤姆·罗林斯（Tom Rawlings）表示，6月上旬被发现的女婴现在已经会笑了。她还有了自己的名字——印度（India）。估计她很快就会找到一个永久接纳自己的家。
</p>
<p>
 “我们有很多人在排队等候，想给这孩子一个永久的家。”罗林斯在接受“早安美国”（Good Morning America）采访时说。
</p>
<p>
 这名身份不明的女婴于6月6日在福赛斯县（Forsyth）一条道路旁被发现。她被人遗弃在一个塑料食品袋中。当局发布了女婴在获救现场的照片。她小小的身体上还留有血迹、缠着肚脐。这令人心酸的一幕牵动了全国各地观众的心。
</p>
<p>
 <center>
 </center>
 “在儿童保护部门工作，我们处理了很多悲剧。有奇迹发生真的是太好了。”罗林斯说起“印度”的幸存时说，“这真的是一个奇迹。”
</p>
<p>
 “我在儿童保护部门工作了将近20年，这是我这辈子见过的最激动人心的奇迹之一。”他说。
</p>
<p>
</p>
<blockquote class="twitter-tweet" data-lang="en">
 <p dir="ltr" lang="en">
  ‘Divine intervention’ saved baby India,
  <span href="https://twitter.com/ForsythCountySO?ref_src=twsrc%5Etfw">
   @ForsythCountySO
  </span>
  says. Today, we’re retracing the steps of how this precious infant ended up in a wooded area during her first hour of life.
  <span href="https://twitter.com/hashtag/TrueCrimeLive?src=hash&amp;ref_src=twsrc%5Etfw">
   #TrueCrimeLive
  </span>
  <span href="https://twitter.com/HLNTV?ref_src=twsrc%5Etfw">
   @HLNTV
  </span>
  <span href="https://t.co/xbb1zZrbgg">
   pic.twitter.com/xbb1zZrbgg
  </span>
 </p>
 <p>
  — MikeGalanosHLN (@MikeGalanosHLN)
  <span href="https://twitter.com/MikeGalanosHLN/status/1143911153728282624?ref_src=twsrc%5Etfw">
   June 26, 2019
  </span>
 </p>
</blockquote>
<p>
 <p>
  罗林斯表示，由于保密原因，他无法过多透露“印度”的情况。他在亲自对她进行检查。“我知道这孩子得到了很好的照顾，我一直在尽心检查她。”他说。
 </p>
 <p>
  “婴儿情况良好，健康很好。以她的经历，可以说是奇迹般地好。”他补充说，“我们当然希望可以找到做这事的人，但我们也希望能够尽快给她找到一个永久的家。”
 </p>
 <p>
  罗林斯补充说，他的机构已收到几百个电话和社交媒体信息，是那些有意要
  <span href="http://www.epochtimes.com/gb/tag/%E6%94%B6%E5%85%BB.html">
   收养
  </span>
  “印度”的人打来和发来的。不过，他补充说，仅在乔治亚州就已经有大约200个家庭在排队等候收养婴儿。
 </p>
 <p>
  虽然执法部门一直想进一步了解“印度”身份方面的信息，但罗林斯说“到目前为止，我们还没有找到（她的）父母或亲戚。”
 </p>
 <p>
  罗林斯表示，他希望“印度”的故事可以帮助“提高人们对那些需要家庭孩子的认识”，并鼓励家庭
  <span href="http://www.epochtimes.com/gb/tag/%E6%8A%9A%E5%85%BB.html">
   抚养
  </span>
  或领养孩子。
 </p>
 <p>
  “每个人都喜欢婴儿，但是有很多6岁、12岁和15岁的孩子确实很需要一个永久的家庭。”他补充说。#
 </p>
 <p>
  责任编辑：李寰宇
 </p>
</p>
<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11352710.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11352710.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11352710.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/28/n11352710.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

