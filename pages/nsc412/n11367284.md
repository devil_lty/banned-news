### 购物如旅游 四大秘诀让Costco和亚马逊抗衡
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/01/GettyImages-1072266062-600x400.jpg"/>
<div class="red16 caption">
 <p>
  消费者喜欢仓储式卖场Costco（好事多）的低价格、散装产品，以及免费样品。(Scott Olson/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月05日讯】（大纪元记者夏雨综合报导）在全球范围内有9400万人拥有Costco（
 <span href="http://www.epochtimes.com/gb/tag/%E5%A5%BD%E5%B8%82%E5%A4%9A.html">
  好市多
 </span>
 ）会员卡。好市多甚至成为一个旅游目的地，一名粉丝说，她在旅行期间去过70家Costco商店，至少有一对夫妇在好市多结婚。Costco有何秘诀，能吸引这么多消费者？
</p>
<p>
 现在很多人看待零售业生态，都认为亚马逊等大型电商的低成本和便利优势会让其在与传统零售商竞争中最终胜出。比如西尔斯（Sears）、玩具反斗城等很多失败案例，以及大量零售商店关门的现象，都可说明这一点。
</p>
<p>
 但美国大型仓储式零售商Costco是个获得成功的例外。
</p>
<p>
 2018财年，Costco营收规模达1416亿美元，仅次于沃尔玛（Walmart）。除2009年受金融危机影响，数十年来Costco几乎每年都保持增长。在很多社区，即使是在工作日，门店里依然是人来人往。
</p>
<p>
 Costco之所以获得成功，是因为该商店的经营方式，考虑到从客户心理到商店设计等各方面因素。全球管理咨询公司A.T. Kearney的合伙人帕特里夏‧洪（Patricia Hong）告诉CNBC Make It节目，一些粉丝十分喜欢在Costco的购物体验和出售的商品品牌。
</p>
<figure class="wp-caption aligncenter" id="attachment_11188645" style="width: 594px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/04/gettyimages-1131382676-594x594.jpg">
  <img alt="" class="size-full wp-image-11188645" src="http://i.epochtimes.com/assets/uploads/2019/04/gettyimages-1131382676-594x594.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  现在很多人看待零售业生态，都认为亚马逊等大型电商的低成本和便利优势会让其在与传统零售商竞争中最终胜出。(Photo by Kyodo News via Getty Images)
 </figcaption><br/>
</figure><br/>
<h4>
 会员获得一种独有的感受
</h4>
<p>
 要在
 <span href="http://www.epochtimes.com/gb/tag/%E5%A5%BD%E5%B8%82%E5%A4%9A.html">
  好市多
 </span>
 仓库式商店购物，从散装食品和洗漱用品到衣服、电子产品和钻石戒指，你必须购买60美元（普通卡）或120美元（高级卡）的年度会员卡。
</p>
<p>
 《顾客为什么买：购物者营销》（Why We Buy : The Science of Shopping）的作者帕克‧昂德黑尔（Paco Underhill）说道，这使得在Costco购物有一种专属感，因此更具吸引力。
</p>
<p>
 但更重要的是，存在心理方面的问题：由于会员费是成本，导致他们更频繁购物，并且每次购买更多产品。
</p>
<p>
 当然，如果你不经常在Costco购物，或你独自生活，或没有足够的空间存放大件物品，均不需要在Costco办卡购物。
</p>
<p>
 尽管如此，该公司仍表示，在美国和加拿大拥有90%的续订率。
</p>
<figure class="wp-caption aligncenter" id="attachment_5880849" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2015/06/1503021107332546.jpg">
  <img alt="" class="size-large wp-image-5880849" src="http://i.epochtimes.com/assets/uploads/2015/06/1503021107332546-600x445.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  Costco的年卡分公司卡和个人卡两种，个人卡又分普通会员卡（Gold Star）和行政高级会员卡（executive membership，见图）。(Tim Boyle/Getty Images)
 </figcaption><br/>
</figure><br/>
<h4>
 每个人都喜欢低价
</h4>
<p>
 好市多的金融计划和投资者关系高级副总裁鲍勃‧尼尔森（Bob Nelson）表示，价值是Costco销售策略背后的驱动力，“我们的价值定义是最高质量，最低价格和最佳会员服务”。
</p>
<p>
 根据尼尔森的说法，这个策略的一部分是好市多是“一项商品”。仓库货架上每件物品都是同类产品中最好的。尼尔森说，这意味着每个仓库只有3,700到3,800种待售物品。
</p>
<p>
 相比之下，根据沃尔玛的网站，沃尔玛超级购物中心销售142,000种不同的商品。
</p>
<p>
 “每件商品都必须保持自己的价值。”尼尔森说。
</p>
<p>
 另一个因素是价格：例如，从Costco网上购买的两个48盎司罐装Skippy奶油花生酱价格为11.49美元，或每盎司0.12美元；与此同时，在Amazon Fresh服务下购买一个16.3盎司花生酱，并在纽约市交付，售价3.19美元，即每盎司0.20美元。
</p>
<p>
 2017年10月J.P. Morgan价格比较发现，Costco商品的价格比Whole Foods价格便宜58%。
</p>
<figure class="wp-caption aligncenter" id="attachment_6643785" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2012/11/1211141231512546.jpg">
  <img alt="" class="size-large wp-image-6643785" src="http://i.epochtimes.com/assets/uploads/2012/11/1211141231512546-600x368.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  Costco的许多商品都是大包装销售。(GABRIEL BOUYS/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<h4>
 在好市多购物就像在旅游
</h4>
<p>
 Costco不仅仅是杂货店，其购物地图具有战略性，不仅吸引客户，也最终让Costco实现销售更多商品。
</p>
<p>
 这一切都始于商店布局：Costco商店后面摆放着生活必需品，促使购物者走过其它商品货架，包括电脑和电视等物品，最后才看到必须买的牛奶等商品。
</p>
<p>
 一路上，购物者可以吃喝Costco的无限量样品，甚至可以偶尔享受免费服务。昂德黑尔表示，这不仅让购物体验更加愉快，而且免费食品可以让你“成为一个不那么自律的购物者”，免费赠品实际上促进了销售。
</p>
<p>
</p>
<blockquote class="twitter-tweet" data-lang="en">
 <p dir="ltr" lang="en">
  We are straight up
  <span href="https://twitter.com/hashtag/Costco?src=hash&amp;ref_src=twsrc%5Etfw">
   #Costco
  </span>
  savages with ZERO shame when it comes to the honey smoked salmon samples (
  <span href="https://twitter.com/jmarteen?ref_src=twsrc%5Etfw">
   @jmarteen
  </span>
  )
  <span href="https://t.co/HXp94DrdsQ">
   pic.twitter.com/HXp94DrdsQ
  </span>
 </p>
 <p>
  — Jasmine Sadry (@JasmineSadry)
  <span href="https://twitter.com/JasmineSadry/status/1108085948934443008?ref_src=twsrc%5Etfw">
   March 19, 2019
  </span>
 </p>
</blockquote>
<p>
 <p>
 </p>
 <p>
  Costco购物者也可以期待找到意想不到的东西。 帕特里夏‧洪说：“人们回到好市多商店寻求寻宝的愉快感觉。”
 </p>
 <p>
  除了Kirkland Signature品牌卫生纸（30卷双层纸巾，在线售价18.99美元）之外，Costco还有一系列令人惊喜的优惠，如设计师品牌Marc Jacobs，Coach和Rebecca Minkoff等，这些精品均让客户有寻宝的感觉。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_6504126" style="width: 594px">
  <span href="http://i.epochtimes.com/assets/uploads/2015/08/100927235432.jpg">
   <img alt="" class="size-full wp-image-6504126" src="http://i.epochtimes.com/assets/uploads/2015/08/100927235432.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   在Costco买电子产品可以享受他们提供的免费技术支持。(Getty Images)
  </figcaption><br/>
 </figure><br/>
 <h4>
  好市多是怀旧的
 </h4>
 <p>
  昂德黑尔告诉CNBC Make It，Costco于1983年在西雅图首次开业，并与美国富裕的老年消费者建立了特别关系，主要是婴儿潮一代（1946年至1964年之间出生的人）。
 </p>
 <p>
  Costco的2017年（最近一年）数据显示，Costco会员平均家庭收入为92,236美元。
 </p>
</p>
<p>
</p>
<blockquote class="twitter-tweet" data-lang="en">
 <p dir="ltr" lang="en">
  When you hated
  <span href="https://twitter.com/hashtag/costco?src=hash&amp;ref_src=twsrc%5Etfw">
   #costco
  </span>
  trips as a kid. And now BEG your parents to give you in on their membership or at least take you for a shopping trip there.
  <span href="https://twitter.com/hashtag/adulting?src=hash&amp;ref_src=twsrc%5Etfw">
   #adulting
  </span>
  <span href="https://twitter.com/hashtag/graduation?src=hash&amp;ref_src=twsrc%5Etfw">
   #graduation
  </span>
  is too real
 </p>
 <p>
  — Samantha Alexa Art (@SamSunKidis)
  <span href="https://twitter.com/SamSunKidis/status/993509751383355392?ref_src=twsrc%5Etfw">
   May 7, 2018
  </span>
 </p>
</blockquote>
<p>
 <p>
 </p>
 <p>
  因为Costco在婴儿潮一代中非常受欢迎，现在这种怀旧情绪已经传给他们的子女和孙辈，即千禧一代。
 </p>
 <p>
  帕特里夏‧洪表示，在和不久前与Costco建立（会员）关系的消费者交谈时，其中一些消费者回忆起曾经在Costco与家人共处的经历，即购物之旅，比如在美食广场吃饭等等。现在他们自己购买了会员资格，几乎成了一个成年礼。
 </p>
 <p>
  Costco推崇怀旧之情，因为它的品牌很棒：例如，Costco的1.50美元Kirkland热狗和百事可乐苏打组合，自1985年以来一直没有涨价，行业追踪者Winsight Grocery Business称其为“Costco的秘密武器”。
 </p>
 <p>
  “这是遗产。看看所有谈论它（热狗苏打水组合）的人。 ……这是我们所知道的事情之一。”尼尔森说。#
 </p>
</p>
<p>
</p>
<blockquote class="instagram-media" data-instgrm-captioned="" data-instgrm-permalink="https://www.instagram.com/p/BpXZht8lULA/" data-instgrm-version="12" style="background: #FFF; border: 0; border-radius: 3px; box-shadow: 0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width: 540px; min-width: 326px; padding: 0; width: calc(100% - 2px);">
 <div style="padding: 16px;">
  <div style="display: flex; flex-direction: row; align-items: center;">
  </div>
  <div style="padding: 19% 0;">
  </div>
  <div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;">
  </div>
  <div style="padding-top: 8px;">
   <div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;">
    View this post on Instagram
   </div>
  </div>
  <div style="padding: 12.5% 0;">
  </div>
  <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
  </div>
  <p style="margin: 8px 0 0 0; padding: 0 4px;">
   <span href="https://www.instagram.com/p/BpXZht8lULA/" rel="noopener noreferrer" style="color: #000; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px; text-decoration: none; word-wrap: break-word;" target="_blank">
    #CostcoLife #costcohotdog breakfast/lunch
   </span>
  </p>
  <p style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; line-height: 17px; margin-bottom: 0; margin-top: 8px; overflow: hidden; padding: 8px 0 7px; text-align: center; text-overflow: ellipsis; white-space: nowrap;">
   A post shared by
   <span href="https://www.instagram.com/kemtz_18/" rel="noopener noreferrer" style="color: #c9c8cd; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: normal; line-height: 17px;" target="_blank">
    Kat M
   </span>
   (@kemtz_18) on
   <time datetime="2018-10-25T18:09:00+00:00" style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;">
    Oct 25, 2018 at 11:09am PDT
   </time>
  </p>
 </div>
</blockquote>
<p>
 <p>
  责任编辑：李寰宇
 </p>
</p>
<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11367284.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11367284.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11367284.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/5/n11367284.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

