### 康尼岛独立日吃热狗大赛 去年男女冠军蝉联第一
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/8dc0f761421e56cd9c8fefddd5df02c7-600x400.png"/>
<div class="red16 caption">
 <p>
  7月4日康尼岛举行一年一度的独立日内森吃热狗比赛。 (奥立弗/大纪元)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月05日讯】（大纪元特约记者宇亭纽约报导）7月4日的纽约
 <span href="http://www.epochtimes.com/gb/tag/%E5%BA%B7%E5%B0%BC%E5%B2%9B.html">
  康尼岛
 </span>
 天气炎热，“内森热狗店”门前人山人海。台上的选手身穿白色T恤衫，前面的长桌上摆着一堆堆热狗面包和一杯杯白水；而台下的空中也好像“漂浮”着一排排的“热狗面包”，那是观众们头上顶着的棕黄相间的帽子——深黄色的外围是“面包”，中间的夹心是棕色“香肠”，上面那一条弯弯曲曲的黄色图案正是“内森”牌热狗上面的挤上去的奶油，岂不是活脱脱的“热狗面包”。
</p>
<figure class="wp-caption aligncenter" id="11366094" style="width: 500px">
 <img alt="上届男子组冠军切斯纳以10分钟内吃掉71根热狗的肚量第12此获得这个比赛的冠军。" src="http://i.epochtimes.com/assets/uploads/2019/07/2230bcf5224b175baca32d21a8b477d6-450x248.png"/>
 <br/><figcaption class="wp-caption-text">
  上届男子组冠军切斯纳以10分钟内吃掉71根热狗的肚量第12此获得这个比赛的冠军。（奥立弗/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 随着裁判一声令下，35个“大胃王”运动起来，只见他们左右手开弓，一个劲往嘴里塞热狗面包。站在舞台中间的上届冠军切斯纳（Joey Chestnut）像一个机器人一般，一把一个热狗，塞进嘴里就咽下去了，不到5分钟就把40条热狗吃下肚；但是5分钟以后他的速度就慢下来了。
</p>
<figure class="wp-caption aligncenter" id="11366091" style="width: 500px">
 <img alt="日裔女子须藤以31个热狗的成绩蝉联女子冠军。" src="http://i.epochtimes.com/assets/uploads/2019/07/a0b18eeaf8d99fe388c9e3404d52568f-450x367.png"/>
 <br/><figcaption class="wp-caption-text">
  日裔女子须藤以31个热狗的成绩蝉联女子冠军。（奥立弗/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 每个选手后面各有一个计数的牌子，随着数字的上涨，选手们的速度越来越慢。最终，到10分钟到点的时候，切斯纳的数字停留在71上面，这个数字比他去年的成绩差了三个，不过他还是稳稳地拿到了第一名，比第二名选手多出了21个。这是切斯纳第12次获得这个比赛的冠军。
</p>
<figure class="wp-caption aligncenter" id="11366090" style="width: 500px">
 <img alt="切斯纳第12次获得冠军，成绩71个/10分钟。" src="http://i.epochtimes.com/assets/uploads/2019/07/60b8cf6a17886ffdc0a02848062e1633-450x252.png"/>
 <br/><figcaption class="wp-caption-text">
  切斯纳第12次获得冠军，成绩71个/10分钟。（奥立弗/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 70多个热狗面包吃进胃中，并没有把他的肚子变大，他的白色T恤也并没有鼓起来。赛后他对记者说，是这个大热天影响了他的成绩。
</p>
<figure class="wp-caption aligncenter" id="11366089" style="width: 500px">
 <img alt="在独立日吃热狗比赛上观众带的“热狗帽”。" src="http://i.epochtimes.com/assets/uploads/2019/07/f488d54f1179495854a13adcd6dc0d77-450x252.png"/>
 <br/><figcaption class="wp-caption-text">
  在独立日吃热狗比赛上观众带的“热狗帽”。（奥立弗/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 “我想打破纪录，对手想打败我，天气对我造成一些影响，我有点发晕，5分钟的时候，速度减慢超出了预料，不过还好我赢了。”
</p>
<figure class="wp-caption aligncenter" id="11366087" style="width: 500px">
 <img alt="在吃热狗比赛上观众们都带着“热狗帽”。" src="http://i.epochtimes.com/assets/uploads/2019/07/d3be0ec7d3e9df4c705349d56b4b5513-450x251.png"/>
 <br/><figcaption class="wp-caption-text">
  在吃热狗比赛上观众们都带着“热狗帽”。（奥立弗/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 女子组的上届冠军须藤（Miki Sudo）也卫冕成功。她的成绩同样不如以前，只吃了31个，她自己也感觉发挥水平失常，她把原因归罪于“自满”。
</p>
<p>
 “自满会让我少吃了几个热狗。”她说，不过她能够保持冠军的宝座还是让她感觉很棒。“我不喜欢可能被取代的感觉，我已经尽力去获胜了。”
</p>
<p>
 华裔选手李蕙以23个热狗的成绩和另一个选手并列第三。
</p>
<p>
 男女冠军的奖品是1万美元（更正：昨天预告时写的2万，经确认仍为1万美元）和一条金色的芥末腰带。
</p>
<p>
 热狗是美国人的国民小吃，每年的独立日吃热狗比赛已经成了国庆节的传统节目。像以往一样，今年的主办方内森热狗店又捐赠10万个热狗给需要救济的人。◇
</p>
<p>
 责任编辑：家瑞
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11366083.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11366083.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11366083.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/5/n11366083.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

