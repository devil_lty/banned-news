### 加州强震 美军关闭“中国湖”基地疏散人员
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-51094134-600x400.jpg"/>
<div class="red16 caption">
 <p>
  在加州于7月4日和5日发生的强震之后，美国海军“中国湖海军航空武器站”暂时关闭。图为2002年4月30日，美军一架F-15战机在该基地上空投放联合直接攻击炸弹以进行测试。(USAF/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月07日讯】（大纪元记者陈俊村编译报导）在美国
 <span href="http://www.epochtimes.com/gb/tag/%E5%8A%A0%E5%B7%9E.html">
  加州
 </span>
 近日发生几次强烈
 <span href="http://www.epochtimes.com/gb/tag/%E5%9C%B0%E9%9C%87.html">
  地震
 </span>
 之后，该国海军位于该州“
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD%E6%B9%96.html">
  中国湖
 </span>
 ”（China Lake）的
 <span href="http://www.epochtimes.com/gb/tag/%E5%9F%BA%E5%9C%B0.html">
  基地
 </span>
 已经暂时关闭，非必要人员也已经疏散，而其恢复正常运作的时间仍有待通知。
</p>
<p>
 这个“
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD%E6%B9%96.html">
  中国湖
 </span>
 海军航空武器站”（Naval Air Weapons Station China Lake）位于
 <span href="http://www.epochtimes.com/gb/tag/%E5%8A%A0%E5%B7%9E.html">
  加州
 </span>
 莫哈韦沙漠（Mojave Desert）西部、洛杉矶以北150英里（241公里），是美国海军研发和测试武器的大型
 <span href="http://www.epochtimes.com/gb/tag/%E5%9F%BA%E5%9C%B0.html">
  基地
 </span>
 。
</p>
<p>
 在加州于7月4日发生规模6.4的
 <span href="http://www.epochtimes.com/gb/tag/%E5%9C%B0%E9%9C%87.html">
  地震
 </span>
 之后，该基地在脸书上发出暂时关闭的通知。该通知说，该场地震并未造成人员伤亡，所有建筑物也完好无损，但该基地需要评估建筑结构和地基的损害。
</p>
<p>
 这项通知提到，美国地质调查局证实，最近这一系列地震的震央位于该基地附近的两个断层上，该局已派人前往该基地进行调查。
</p>
<p>
 而在7月5日发生的规模5.0和7.1的地震之后，该基地已对非必要的人员进行疏散，例如：受训的后备人员、文职员工和亲属等，但安全协议仍然有效。
</p>
<p>
 该基地在脸书上发出通知说，在进一步的通知之前，“中国湖海军航空武器站”都无法执行任务。“人员的安全是目前的第一要务。”
</p>
<p>
 “中国湖海军航空武器站”占地超过110万英亩（4,452万公亩），比罗德岛州还大。该基地占了海军用来研究、开发、取得、测试和评估武器系统的所有国内土地的85%。
</p>
<p>
 这个基地的基础设施耗资约30亿美元，它含有2,132栋建筑物和设施、329英里（529公里）长有铺砌的道路、以及1,801英里（2,898公里）长未铺砌的道路。
</p>
<p>
 该基地曾开发出多种著名的武器系统，其中包括：先进中程空对空飞弹（Advanced Medium-Range Air-to-Air Missile）、高速反辐射飞弹（High-speed anti-Radiation Missile）、地狱火飞弹（Hellfire）、联合直接攻击炸弹（Joint Direct Attack Munition）、联合距外武器（Joint Standoff Weapon）、响尾蛇飞弹（Sidewinder）、垂直发射反潜火箭（Vertical Launch Anti-submarine Rocket）等。
</p>
<p>
 责任编辑：李玲
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11369550.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11369550.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11369550.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/7/n11369550.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

