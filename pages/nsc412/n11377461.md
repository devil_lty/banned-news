### “7‧20”反迫害20周年 7/15纽约中领馆集会夜悼
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/e531233d7817173771d91854dae4817b-600x400.jpg"/>
<div class="red16 caption">
 <p>
  2018年7月16日，纽约部分法轮功学员在纽约中领馆前举行了“7.20”法轮功反迫害19周年集会及烛光悼念活动，悼念被中共迫害致死的法轮功学员，并呼吁国际社会共同制止中共迫害。 (大纪元资料图片)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月11日讯】7月15日周一晚6点至9点，纽约地区的部分法轮功学员将在曼哈顿十二大道的纽约中领馆前，举行集会和烛光夜悼，谴责中共二十年来对法轮功学员的残酷迫害，悼念被迫害致死的法轮功修炼者。
</p>
<figure class="wp-caption aligncenter" id="11377463" style="width: 500px">
 <img alt="2018年7月16日，纽约部分法轮功学员在纽约中领馆前举行了“7.20”法轮功反迫害19周年集会及烛光悼念活动，悼念被中共迫害致死的法轮功学员，并呼吁国际社会共同制止中共迫害。" src="http://i.epochtimes.com/assets/uploads/2019/07/f06bb19328f4f9f2a7d14e4444b3cf54-450x300.jpg"/>
 <br/><figcaption class="wp-caption-text">
  2018年7月16日，纽约部分法轮功学员在纽约中领馆前举行了“7.20”法轮功反迫害19周年集会及烛光悼念活动，悼念被中共迫害致死的法轮功学员，并呼吁国际社会共同制止中共迫害。（大纪元资料图片）
 </figcaption><br/>
</figure><br/>
<p>
 一九九九年七月二十日，中共发动了对信仰“真、善、忍”的法轮功修炼群体的镇压，残酷迫害一直持续至今。二十年来，面对血腥迫害，法轮功学员始终坚守着法轮大法所教导的“真、善、忍”准则，和平理性反迫害。每年的
 <span href="http://www.epochtimes.com/gb/tag/%E2%80%9C7%E2%80%A720%E2%80%9D.html">
  “7‧20”
 </span>
 前后，纽约法轮功学员来到中领馆前集会，呼吁国际社会共同制止中共发动的这场惨无人道的迫害。
</p>
<p>
 法轮功，又称法轮大法，是由李洪志先生于1992年5月传出的佛家上乘修炼大法，以宇宙最高特性“真、善、忍”为根本指导，按照宇宙的演化原理而修炼，包含五套缓慢优美的功法动作。法轮功不收分文，义务教功。经亿万人的修炼实践证明，法轮功教人向善，同时具有祛病健身的奇效。法轮大法是大法大道，在把真正修炼的人带到高层次的同时，对稳定社会、提高人们的身体素质和道德水准，也起到了不可估量的正面作用。
</p>
<p>
 目前法轮功已经洪传全世界100多个国家和地区，获得多国各级政府及组织的褒奖、支持信函达3,000多项，赢得了世界各族裔民众的爱戴和尊敬。
</p>
<p>
 一九九九年，中共党魁江泽民看到修炼法轮功的人数超过中共党员的人数，妒嫉李洪志先生的威望，发起迫害。时至今日，全世界唯有中共统治下的中国大陆一地残酷迫害法轮功信仰者。
</p>
<p>
 二十年来，有成千上万的法轮功学员被关押、遭受酷刑，经核实有名有姓被迫害致死的有四千多人，甚至发生活摘法轮功学员器官的罪恶。这场迫害是对人性、对人类普世价值的摧残，是人类空前的浩劫。
</p>
<p>
 本次集会将呼吁国际社会上的更多正义之士关注这场历史上最大的针对正信的、反人类的迫害，希望更多的民众关注法轮功学员的人权，知道“法轮大法好”，并加入到共同制止中共迫害的行列，这也是维护人类的道德底线和普世价值。
</p>
<p>
 活动安排：7月15日（周一）晚6点至7点30分为反迫害集会，晚7点30分至9点为烛光夜悼。地点在曼哈顿十二大道的纽约中领馆旁。◇
</p>
<p>
 责任编辑：家瑞
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11377461.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11377461.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11377461.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/11/n11377461.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

