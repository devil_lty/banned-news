### 美经济创历史最长扩张期纪录 超过十年
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-953781032-600x400.jpg"/>
<div class="red16 caption">
 <p>
  美国经济增长自7月1日起进入第121个月，创最长的逾十年扩张期纪录。(BRENDAN SMIALOWSKI/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月02日讯】（大纪元记者林燕编译报导）自周一（7月1日）起，
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD%E7%BB%8F%E6%B5%8E.html">
  美国经济
 </span>
 连续增长、进入第121个月，创下最长的逾十年扩张期新纪录。
</p>
<p>
 从2009年6月开始，美国已经连续经济增长120个月，打破了之前1991年3月至2001年3月连续增长120个月的纪录。
</p>
<p>
 经济学界认为，持续10年的
 <span href="http://www.epochtimes.com/gb/tag/%E4%BD%8E%E5%88%A9%E7%8E%87.html">
  低利率
 </span>
 政策有助经济增长，并帮助2,200万人重返工作岗位。5月的整体
 <span href="http://www.epochtimes.com/gb/tag/%E5%A4%B1%E4%B8%9A%E7%8E%87.html">
  失业率
 </span>
 降至3.6%，为1969年以来的最低水平。
</p>
<p>
 但总体上，本次的经济增长比过去的扩张稍弱，累计的单季国民生产总值（GDP）增长约25%，远低于上一次的经济增长速度。
</p>
<p>
 经济学家表示，尽管扩张动力显得不足上一轮，但扩张仍在继续，最新的一轮推动是从2018年川普（
 <span href="http://www.epochtimes.com/gb/tag/%E7%89%B9%E6%9C%97%E6%99%AE.html">
  特朗普
 </span>
 ）的减税政策以及放松商业法规管制。
</p>
<p>
 美国GDP到2018年才达到潜力水平估值，超过了国会预算办公室（CBO）分析师的假设：如果2007年房地产泡沫没有破裂、投资银行雷曼兄弟在次年没有破产、世界没有陷入深度衰退情况下，
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD%E7%BB%8F%E6%B5%8E.html">
  美国经济
 </span>
 早该达到的水准。
</p>
<p>
 据CBO每年做出的预估，2007-2009年的经济衰退导致美国经通胀调整后的GDP蒸发了6,000亿美元。同时，衰退还导致美国实际GDP与潜在GDP之差进一步扩大至接近1万亿美元。潜在GDP是根据人口、工业基础和其它因素计算出来。
</p>
<p>
 美国国家经济研究局也将2009年6月定为最近一次经济衰退的“谷底”。经过10年时间，美国经济恢复到金融危机前的水平。
</p>
<p>
 “我们现在才赶上来，”梅隆银行（Mellon）首席经济学家文森特·莱因哈特（Vincent Reinhart）说。
</p>
<p>
 GDP超过潜力水平的时期，通常是工人薪资增长最快、向来被边缘化的族群也能找到工作的时期。
</p>
<p>
 在经济增长持续期接近十年大关之际，市场也在猜测这轮复苏还能持续多久，未来几年是否将不可避免地再度出现衰退。而
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E8%81%94%E5%82%A8.html">
  美联储
 </span>
 等官员一直在评估美国经济所处的阶段，以及权衡是否要降息或出台其它政策帮助延长经济增长期。
</p>
<p>
 若能逐步实现
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E8%81%94%E5%82%A8.html">
  美联储
 </span>
 和川普政府的重建中产阶层收入目标，美国经济复苏还能再持续数年。
</p>
<p>
 美联储在6月中旬曾暗示，可能最早在本月降息；而现在它正试图传递一个相同讯息，希望可以让“紧促”的经济再维持几年时间，让美国人可以享受更大比例的经济产出。
</p>
<p>
 但短期内，受制于全球贸易争端和其它风险因素，可能拖慢全球经济脚步。
</p>
<p>
 路透社报导说，美联储或许需要一些胆识，才能延续这从许多方面来看都只是刚刚起步的复苏。
</p>
<p>
 “这就像是个新世界，面临结构性的全球需求下降，以及美国经济在结构上的低韧性。”奥巴马政府负责国际事务的财政部副部长、现PGIM Fixed Income首席经济学家奈森‧许次（Nathan Sheets）说。“这也是我们所在的新世界特性，只是变得更加复杂。”＃
</p>
<p>
 责任编辑：李寰宇
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360057.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360057.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc412/n11360057.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/2/n11360057.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

