### 中共党魁毛泽东秘书的悲惨命运（十）第一任秘书长李六如文革中丧生
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="第一任秘书长李六如文革中丧生" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/05/-liliuhe-600x401.jpg"/>
 </div>
 <div class="caption">
  第一任秘书长李六如文革中丧生
 </div>
</div>
<hr/>
<div class="content">
 <p class="content-info-datetype">
  <span class="content-info-date">
   【希望之声2019年5月27日】
  </span>
  <span class="content-info-type">
   （本台記者李明、元成綜合報導）
  </span>
 </p>
 <div class="soh-audio-player status-published">
  <div class="jp-jplayer" id="audio-player-0">
  </div>
  <div aria-label="media player" class="jp-audio" data-url-h="http://media.soundofhope.org/audio03/2019/05/-285.mp3" data-url-l="http://media.soundofhope.org/16K-mp3/audio03/2019/05/-285_16K.mp3" id="audio-player-container-0" role="application">
   <div class="panel">
    <div class="control">
     <div class="button pnp">
      <i aria-hidden="true" class="fa fa-play">
      </i>
     </div>
     <div class="slider-progress">
     </div>
     <div class="button mute">
      <i aria-hidden="true" class="fa fa-volume-up">
      </i>
     </div>
     <div class="slider-vol">
     </div>
     <div class="button download">
      <span href="http://media.soundofhope.org/16K-mp3/audio03/2019/05/-285_16K.mp3">
       <i aria-hidden="true" class="fa fa-cloud-download">
       </i>
      </span>
     </div>
     <div aria-label="..." class="btn-group bps" role="group">
      <button class="btn btn-default bps-l active" type="button">
       16K
      </button>
      <button class="btn btn-default bps-h" type="button">
       128K
      </button>
     </div>
    </div>
   </div>
  </div>
 </div>
 <p>
  李六如，湖南省平江县人，1921年加入中共并开始了他的统战及地下党情报生涯。1937年为毛泽东的秘书长，也是长篇小说《六十年的变迁》的作者，文革中被迫害致死。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  1923年国共合作后，李六如曾在国民党湖南省党部任执行委员。1924年受中共湘区委派遣去广州筹集宣传经费，先后任国民革命军总司令部党务处处长、第二军军校政治部指导主任兼政治教官。1926年7月，任国民革命军第二军第四师党代表。1927年夏赴上海，受周恩来直接领导，从事中共地下党工作。1928年冬，受命前往新加坡从事特务活动，职务是中共南洋临委宣传部长。1929年到香港，在中共南方局宣传部工作,他以生意人的身份，与王稼祥、刘昂等人，谎称父子、父女的关系组成假家庭，以掩护他的特务活动。1937年3月李六如到了延安，任毛泽东办公室秘书长、延安行政学院代院长、中央财政经济部副部长等职。
 </p>
 <p>
  1949年后，李六如任政务院政法委员会委员，最高检察署副检察长、党组书记，第二、三届全国政协常委。1955年李六如退休后，便开始以他个人生涯为主线的《六十年的变迁》创作。两年后第一卷脱稿，约20万字。中央宣传部副部长林默涵负责审阅。1957年由人民文学出版社出版，时任元帅的彭德怀看望李六如时，对此倍加赞赏，各大报刊也纷纷转载，《六十年的变迁》红极一时。1961年，《六十年的变迁》第二部出版。接着，李六如在写《六十年的变迁》第三部时，中共发动的文革开始了，他的写作被迫中止，厄运接踵而来。
 </p>
 <p>
  被管制的李六如被遣送到桂林，还特别被“规定”：不许带女儿一起走，也不许与女儿及亲友通信，他们夫妇被安排在一幢房子里被告知：不许会客访友，不许离开圈定的活动范围，也没有任何文化生活。几个“监护”他们的“造反派”负责对他们的监督。桂林多雨，终年潮湿，天总是阴沉沉、灰濛濛的。李六如夫妇被关在屋子里，面对四壁，心情十分郁闷。每当心头的闷气无法发泄时，李六如便站在窗前，仰天长叹，他每次绝望而又凄惨的呼喊，使他妻子王美兰禁不住泪水满面。他们的女儿海龙，由于不允许带走，在一次车祸中，无依无靠的她，惨死在雪域高原，更使李六如夫妇雪上加霜，陷入了极度的悲痛之中。
 </p>
 <p>
  从桂林回到北京后不久，李六如再一次病倒，而且一病不起。李六如脚肿得厉害，连走路都困难。1973年4月，李六如的病情更加严重了。王美兰想把他送到医院去抢救，但是要不到车，只好到街坊那里借来一辆三轮车，艰难地把丈夫拉到医院。在李六如成为“叛徒”、“反党分子”之后，《六十年的变迁》一书，被定为“反党小说”，还有他网罗特务，为彭德怀翻案等。就在李六如听到这一消息时，他这天离开了人世!
 </p>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/03/07/n2706775.html">
   中共党魁毛泽东秘书的悲惨命运–（一）机要秘书叶子龙
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/03/16/n2730115.html">
   中共党魁毛泽东秘书的悲惨命运（二）在毛藏书室吊死的田家英
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/03/21/n2743879.html">
   中共党魁毛泽东秘书的悲惨命运（三）用自杀为毛泽东祝寿的周小舟
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/03/25/n2754307.html">
   中共党魁毛泽东秘书的悲惨命运（四）陪伴毛泽东三十一年的陈伯达
  </span>
 </p>
 <div>
 </div>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/04/05/n2785152.html">
   中共党魁毛泽东秘书的悲惨命运（五）“毛泽东思想”理论的殉葬者张如心
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/04/10/n2796846.html">
   中共党魁毛泽东秘书的悲惨命运（六）“四大左狂”之一 李井泉
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/04/24/n2832216.html">
   中共党魁毛泽东秘书的悲惨命运（七）夫人兼生活秘书的江青
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/05/06/n2862555.html">
   中共党魁毛泽东秘书的悲惨命运（八）重庆谈判时的秘书王炳南
  </span>
 </p>
 <p>
  <span href="http://www.soundofhope.org/b5/2019/05/14/n2884360.html">
   中共党魁毛泽东秘书的悲惨命运（九）同乡秘书江华文革中家破人亡
  </span>
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    蔡红
   </span>
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n2913256.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n2913256.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n2913256.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/05/27/n2913256.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

