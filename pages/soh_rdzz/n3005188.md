### 川普总统对独裁者们“外柔内刚” 让反对者都无话说
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="川普总统在G20亚洲行期间会见了三个独裁政权的领导人。(AP图片合成)" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/ap19179193345626-600x400-600x400.jpg"/>
 </div>
 <div class="caption">
  川普总统在G20亚洲行期间会见了三个独裁政权的领导人。(AP图片合成)
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月3日】
  </span>
  <span class="content-info-type">
   （本台记者張莉莉综合编译）
  </span>
  在不久前结束的亚洲四日行中，美国总统川普取得了丰硕的外交成果。在一点也不改变美国立场的情况下，他重启了与中国的贸易谈判，重启了与朝鲜的无核化谈判，也与俄罗斯建立了友好关系。在这期间，与他相谈甚欢的独裁者不仅是一个、两个，而是三个甚至更多。对此福克斯新闻评论员赞扬川普：对独裁者“外柔内刚”，在释出善意的同时，采取强硬政策。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  福克斯新闻评论员马克·泰森（Marc Thiessen）7月3日（周三）撰文表示，事实证明，面对独裁者们，川普总统尽管语善言慈，但实际上内心非常强硬。
 </p>
 <p>
  文章提到，川普总统成为第一位踏上朝鲜领土的美国总统，在那里，他高度赞扬了与朝鲜独裁者金正恩的“伟大友谊”。另外，在与俄罗斯总统普京举行双边会谈后，川普总统打趣他，让他不要干涉美国大选，将普京逗乐了。对待中共领导人习近平，川普总统更是给予极高的赞扬，说他是“200年来最伟大的领导人之一”，也不断重申习近平是他的“好朋友”。川普总统甚至还表示愿意在“没有先决条件”的情况下与伊朗领导人见面。
 </p>
 <p>
  所有这一切，泰森谈到，如果是奥巴马所为，那会引来美国国内保守派们愤怒的嚎叫。然而，川普总统这样说，他们却一点也不生气。为什么呢？这是因为川普总统实际上对朝鲜、俄罗斯、中国以及伊朗采取了前所未有的强硬立场。他的行为证明了他的立场，这比他在外交时说什么更加重要。
 </p>
 <p>
  泰森认为，与川普总统的强硬立场相对比，奥巴马一直在纵容那些“反美暴君们“。他不断地在外交上让步，并为他们提供了巨额的现金支持。举例来说，奥巴马曾在古巴独裁者卡斯特罗（Raúl Castro）的哈瓦那棒球场上欢呼，同时给予古巴政权外交承认和制裁豁免，却什么条件也没有。而古巴对其绥靖政策的回应则是：加强对国内持不同政见者的镇压。
 </p>
 <p>
  另外，奥巴马曾试图与伊朗总统哈桑·鲁哈尼（Hassan Rouhani）会面，却遭到拒绝。然而，奥巴马仍然与之签署所谓“核协议”，为该政权提供了数十亿美金的资助。而伊朗独裁者就用了这笔钱来加速恐怖主义以及什叶派穆斯林在中东的扩张。
 </p>
 <p>
  2009年，奥巴马取消了《美国导弹防御协议》，削弱了对波兰和捷克的保护，受到莫斯科的欢迎。2014年，他又支持普京将克里米亚并入俄罗斯的版图。
 </p>
 <p>
  与奥巴马的种种外交败笔相比，川普总统除了几句好听的话，几乎什么“礼物”都没有送给那些独裁者们。举例来说，他重启与朝鲜的无核化谈判，但是并没有取消对朝制裁，也没有解冻朝鲜的资产或给予朝鲜外交承认。与之相反，他加强了对朝鲜金正恩及其亲信的制裁，并扣押了一艘企图违反制裁规定的朝鲜船只。
 </p>
 <p>
  同时，川普总统还批准了对乌克兰大规模出售武器及军事援助计划，驱逐了俄罗斯外交官，授权国会对莫斯科进行多伦新的制裁，退出了《中程核力量条约》，说服北约盟国增加了国防投资，两次轰炸了普京的盟友阿萨德政权，等等。
 </p>
 <div>
 </div>
 <p>
  在与普京见面之前，川普总统还宣布考虑对俄罗斯实施新的经济制裁措施，以阻止其增加出售天然气给德国的Nord Stream 2管道建设。所有这些对俄强硬立场，证明了一点，除了里根总统之外，没有另外哪位美国总统比川普对俄罗斯更加强硬了。
 </p>
 <p>
  再来看川普总统对中共政权的手段。他与习近平达成重启贸易谈判的共识，但是，他并没有撤销对价值2,500亿中国商品征收的25%的关税，也对中国窃取美国知识产权的行为进行了惩罚。他同时也明确表示，如果不能达成协议，他随时准备对另外3,000亿中国商品征收关税。
 </p>
 <p>
  对于伊朗，川普总统虽然表示愿意与伊朗领导人会面，但是，他早已退出奥巴马签署的“灾难性”核协议，并对伊朗实施了严厉制裁，这导致伊朗的石油销售减少了88%，政府收入减少了40%。
 </p>
 <p>
  泰森最后表示，虽然目前尚不清楚川普总统的强硬政策加软性言论对这些独裁者们是否奏效，但是，他的外交政策目前已令许多民主党人无言以对。而对于保守派来说，只要总统没有改变立场，大多数人都不介意给他与那些独裁者们一些周旋的余地。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    楊曉
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3005188.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3005188.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3005188.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/03/n3005188.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

