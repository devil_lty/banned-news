### 金里奇：全世界都应该庆祝7月4日 美国独立宣言代表普世价值
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="国会前议长金里奇-AP" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/gingrich-a-1-600x400.jpg"/>
 </div>
 <div class="caption">
  国会前议长金里奇-AP
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月5日】
  </span>
  <span class="content-info-type">
   （本台记者季云综合编译）
  </span>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    美国会前议长金里奇博士
   </span>
  </span>
  7
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    月
   </span>
  </span>
  4
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    日在福克斯新闻发表评论。他认为
   </span>
  </span>
  7
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    月
   </span>
  </span>
  4
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    日的美国独立日代表着人民与政府关系的新定义，7月4日意义重大，应该成为普世的节日。
   </span>
  </span>
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    金里奇说，
   </span>
  </span>
  7
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    月
   </span>
  </span>
  4
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    日是世界上最重要的政治节日，其实，这一天应该是普世的节日。 因为美国的独立日纪念的不仅仅是美国的革命，更是人民与政府间关系的革命。
   </span>
  </span>
 </p>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    金里奇说，人类历史中，多数的时候是权力在控制、主导、盘剥弱者。即便在今天的俄国、委内瑞拉、古巴、伊朗和朝鲜，这种古老的模式依然存在。而在共产专制的中国，习近平总书记正在施行
   </span>
  </span>
  21
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    世纪的集权统治，其监视、控制的水平甚至超过乔治
   </span>
  </span>
  –
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    奥威尔（
   </span>
  </span>
  George Orwell
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    ）（笔下）的最恶的梦魇。
   </span>
  </span>
 </p>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    金里奇说，普通人与他们政府之间关系的最伟大的变革就发生在
   </span>
  </span>
  1776
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    年
   </span>
  </span>
  7
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    月
   </span>
  </span>
  4
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    日。在独立宣言中，建国先父们不仅宣布了他们离开英国国王乔治三世的自由，他们还为（世界上的）所有人建立了一个自由的框架。
   </span>
  </span>
 </p>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    独立宣言主要是汤玛斯
   </span>
  </span>
  –
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    杰佛逊（
   </span>
  </span>
  Thomas Jefferson
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    ）起草的（这点令左派们对杰斐逊新生的厌恶（显得）如此荒谬）。杰佛逊开始为全人类构建普遍的原则。他没有描述美国人的权力。他描述了人类的权力（人权），根据定义，此权力也适用于美国人。
   </span>
  </span>
 </p>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    这就是
   </span>
  </span>
  7
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    月
   </span>
  </span>
  4
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    日应该成为普世庆祝日的关键所在。杰佛逊写下了一个公式，这个公式能解放这个星球上的每个人。
   </span>
  </span>
 </p>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    金里奇请人们注意宣言中的普世宣示：“我们认为下面这些真理是不证自明的：人人生而平等，造物者赋予他们若干不可剥夺的权利，其中包括生命权、自由权和追求幸福的权利。为了保障这些权利，人们才在他们之间建立政府，而政府的正当权力来自被统治者的同意。任何形式的政府，只要破坏上述目的，人民就有权利改变或废除它，并建立新政府；新政府赖以奠基的原则，得以组织权力的方式，都要最大可能地增进民众的安全和幸福。”
   </span>
  </span>
 </p>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    金里奇说，独立宣言是一个任何人都可以寻求指导的宣言。宣言实施的日子应该成为所有希望再次宣示自己自治权力、不被他人剥削的人们的纪念日。而现在庆祝独立宣言尤为重要，因为在法制中的自我管理这一伟大概念正受到内外攻击。
   </span>
  </span>
 </p>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    金里奇说，普京、金正恩和卡斯特罗显然不喜欢这个写在法律中的造物主赋予权力的信条。不幸的是，在美国国内和民主的欧洲也有自由的敌人。暴力的极端主义分子如“反法西斯”（
   </span>
  </span>
  Antifa
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    ）（之类组织）寻求破坏自由，并通过恐吓和暴力来强制实施他们的价值。许多的学者也表现出对美国自我管理体系的鄙视。还有，在国会更多的激进元素明确寻求破坏并迅速改变美国的系统。
   </span>
  </span>
 </p>
 <div>
 </div>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    金里奇表示，利用每年的
   </span>
  </span>
  7
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    月
   </span>
  </span>
  4
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    <span style="font-family: Lucida Sans Unicode;">
     日再次宣示造物主赋予每个人的普世权力的重要性
    </span>
    ，
   </span>
  </span>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    这个事很重要。利用这个假日来记住美国有多么的优秀、记住这个地球上的任何人都有机会把这个优秀应用于他们的国家和政府，这个事也很重要。
   </span>
  </span>
 </p>
 <p>
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    而这就是对这个地球上的任何人来说
   </span>
  </span>
  7
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    月
   </span>
  </span>
  4
  <span style="font-family: Lucida Sans Unicode;">
   <span lang="zh-CN">
    日是最重要的政治节日的原因，金里奇说。
   </span>
  </span>
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    杨晓
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3010624.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3010624.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3010624.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/05/n3010624.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

