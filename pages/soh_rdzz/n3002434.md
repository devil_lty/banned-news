### 左派政策让“无家可归者像活在地狱里” 川普计划解决此棘手问题
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="川普总统7月1日在白宫椭圆形办公室里。(AP Photo/Carolyn Kaster)" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/ap19182757309797-600x400-600x400.jpg"/>
 </div>
 <div class="caption">
  川普总统7月1日在白宫椭圆形办公室里。(AP Photo/Carolyn Kaster)
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月2日】
  </span>
  <span class="content-info-type">
   （本台记者張莉莉综合编译）
  </span>
  美国总统川普近日就日益严重的无家可归人士现象表示忧虑，并计划解决此棘手问题。他表示，“这些被迫住在街头的人就像生活在地狱里一般，我会很认真的看待这件事情”。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  6月底，福克斯新闻主持人塔克尔‧卡尔森（Tucker Carlson）对川普总统做了一次专访，节目在7月1日晚播出了。川普总统从各个层面表达了对无家可归人士问题的看法。
 </p>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_3002662">
  <img alt="" class="size-medium wp-image-3002662" src="//www.soundofhope.org/wp-content/uploads/2019/07/2019-07-02-2-600x332.png" srcset="//www.soundofhope.org/wp-content/uploads/2019/07/2019-07-02-2-600x332.png 600w, //www.soundofhope.org/wp-content/uploads/2019/07/2019-07-02-2-768x425.png 768w, //www.soundofhope.org/wp-content/uploads/2019/07/2019-07-02-2-1024x567.png 1024w, //www.soundofhope.org/wp-content/uploads/2019/07/2019-07-02-2-180x100.png 180w, //www.soundofhope.org/wp-content/uploads/2019/07/2019-07-02-2-366x203.png 366w, //www.soundofhope.org/wp-content/uploads/2019/07/2019-07-02-2.png 1325w">
   <br/><figcaption class="wp-caption-text">
    川普总统接受福克斯新闻网大牌主持人塔克尔‧卡尔森（Tucker Carlson）专访，节目在7月1日晚播出。（视频截图）
   </figcaption><br/>
  </img>
 </figure><br/>
 <h3>
  <strong>
   “这些人像生活在地狱里”
  </strong>
 </h3>
 <p>
  对于美国部分大城市有如此多无家可归的人群，川普总统说：“这很可耻”。他表示，虽然联邦政府正在做其它许多很重要的事情，但也会非常认真的看待这件事情。“我们不可以允许这样的事情发生。”
 </p>
 <p>
  川普总统还说：“就连警察去无家可归者聚集的地方巡逻一圈，都可能会生病。那里的人许多都有疾病。他们像生活在地狱里一般。”
 </p>
 <h3>
  <strong>
   29%
  </strong>
  <strong>
   的无家可归者患有精神疾病
  </strong>
 </h3>
 <p>
  上个月公布的官方数据现实，大约有29%的无家可归者患有精神方面的疾病或者有药物滥用问题。
 </p>
 <p>
  川普总统表示，很多无家可归者罹患精神疾病，因此，他们可能自己都没有意识到自己生活在污秽当中。“他们甚至不知道自己在怎样生活。也许他们愿意，但是，他们不能这样。”川普总统认为，大城市的健康上班族，在城市里上班，在办公楼里工作，但是许多人需要经过那些流浪者聚集的街区才能到达工作地点。他谈到，“在他们开始影响到健康的上班族之前，我们需要解决这个问题。他们不能破坏我们的城市。”
 </p>
 <h3>
  <strong>
   “无家可归者破坏美国形象”
  </strong>
 </h3>
 <p>
  数据显示，在美国前十大无家可归者聚集的城市中，加州就占了四个，其中洛杉矶和旧金山情况尤其严重。川普总统说：“大城市里有这么多无家可归者，会破坏美国在世界人民心目中的形象。”
 </p>
 <p>
  他还谈到，不应该让人们在洛杉矶和旧金山看到那种场景，所以我们需要做一些事情，将整个这件事情清理掉，让它不复存在。“这不合适。”
 </p>
 <h3>
  <strong>
   “不会阻止联邦政府介入解决此事”
  </strong>
 </h3>
 <p>
  川普总统表示，虽然城市中的无家可归者问题不应该依靠联邦政府来解决。但他不会阻止政府介入此事。他说：“作为联邦政府，我们真的不具备能力来做这种（具体）工作。但是以前在我们的生活中从未出现过这样的事情。”
 </p>
 <div>
 </div>
 <p>
  川普总统表示，因为此事范围不仅限于几个城市，还有许多其它城市，因此政府“需要解决这个问题”。
 </p>
 <h3>
  <strong>
   无家可归现象近年来日益严重
  </strong>
 </h3>
 <p>
  6月25日，加州官方出台的数据显示，尽管政府每年开支6.19亿美金用于解决无家可归人士的安置问题，然而，这种现象的严重性却有增无减。仅洛杉矶一个城市，去年一年内，无家可归者人数就攀升了12%。
 </p>
 <p>
  据统计，整个洛杉矶县无家可归者总数为5.9万人，其中有3.6万人都来自洛杉矶市。而2018年，该县无家可归者数量为5.3万人。
 </p>
 <p>
  该统计还发现，去年在无家可归者中，25岁以下的年轻人数量增加了24%，而62岁以上的老人数量则增加了7%。
 </p>
 <h3>
  <strong>
   “自由派政客令此事件恶化”
  </strong>
 </h3>
 <p>
  川普总统在受访时指责自由派政客们的政策令当地无家可归者问题日益恶化。他表示，自己一直在与那些政客们对抗。
 </p>
 <p>
  川普总统说：“我不知道他们是否真的认为这种事情应该发生。也许是他们害怕（更多的）选票。但这件可怕的事情正在发生。记得我刚上任的时候，无家可归者正在华盛顿特区的一些地方聚集。我很快就解决了这件事。”因为，“当其他国家的领袖要来（特区）看望美国的总统，当他们的车队经过那些街区，我不希望他们看到这个。我真的相信这对我们的国家伤害很大。”
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    楊曉
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3002434.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3002434.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3002434.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/02/n3002434.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

