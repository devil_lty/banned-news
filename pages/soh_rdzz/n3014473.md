### 美国的好消息成为民主党的坏消息 民主党经历47年来最糟一周
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="川普总统出席7月4日国庆活动。 (AP Photo/Carolyn Kaster)" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/07/ap19186715253543-600x400.jpg"/>
 </div>
 <div class="caption">
  川普总统出席7月4日国庆活动。 (AP Photo/Carolyn Kaster)
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年7月7日】
  </span>
  <span class="content-info-type">
   （本台记者張莉莉综合编译）
  </span>
  7月7日（周日），布莱特巴特新闻评论员约翰·诺尔特（John Nolte）发表了一篇笔锋犀利的文章，从9个方面评论民主党在这一个星期内的表现。文章分析了该党如何在各方面丧失了更多民心，让这个星期成为民主党47年来最糟糕的一周。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  诺尔特认为，现在的民主党已经不是父辈们那个年代的民主党了。民主党的政策从各方面都越来越极端。他认为，对于民主党政客来说，经过了这灾难性的一个星期后，预计到2020年11月的总统大选，他们都很难再爬起来。他从以下九个方面做了分析：
 </p>
 <h3>
  <strong>
   第一方面：在美国经济蓬勃发展的大环境下，支持民主党的假新闻媒体却越来越萎缩。
  </strong>
 </h3>
 <p>
  数据显示，在川普总统执政下的美国，各个行业都呈蓬勃发展的趋势，唯独媒体行业日渐萎靡不振。据预测，到2019年底，全美媒体业将裁员超过1.2万人。由于以左派媒体CNN为首的美国主流媒体多数是为民主党唱赞歌，诺尔特表示，这意味着民主党将失去1.2万个习惯于撒谎的“宣传斗士”。
 </p>
 <h3>
  <strong>
   第二方面：民主党热门人物乔·拜登越来越丧失理智。
  </strong>
 </h3>
 <p>
  虽然诺尔特从来都看不上拜登，但他承认，拜登曾一度被民众认为是民主党中比较“理智”的老人。然而，从这一个星期拜登的言论来看，他已渐渐失去以往的好名声了。他现在公开支持枪支管制，支持纳税人资助的堕胎权，还希望保护非法移民。他还支持向非法移民提供免费政府医保并将他们渐渐合法化。这一系列的主张，足以让其失去民心了。
 </p>
 <h3>
  <strong>
   第三方面：川普总统7月4日国庆典礼获巨大成功，民主党再输一棋。
  </strong>
 </h3>
 <p>
  民主党人预测，川普总统会利用独立日庆典的机会，为自己大选造势，因此，不会有多少人参与。然而，川普主持的国庆阅兵及庆祝活动获得了巨大的成功，让民主党可谓再输一棋。7月4日当天前去华盛顿特区观礼者可谓“人山人海”。另外，诺尔特认为川普总统的国庆致辞是他任总统以来最好的演讲之一，让人们了解了美国的伟大。他说：“一点也没有，川普一点点也没有谈他自己。”
 </p>
 <h3>
  <strong>
   第四方面：民主党人公开站出来反对美国国旗。
  </strong>
 </h3>
 <p>
  在爱国的美国人心中，国旗是美国自由的象征。诺尔特说：“一点也不是开玩笑，民主党人现在憎恨美国国旗。”他谈到，在民主党人心中，美国国旗现在象征著“压迫”，他们的言论已将其想法道明，或许“他们的目标是将美国国旗换成镰刀斧头旗，才肯罢休。”
 </p>
 <h3>
  <strong>
   第五方面：民主党人的政策更像是在墨西哥拉选票，而不是在美国。
  </strong>
 </h3>
 <p>
  对于民主党人的移民政策，诺尔特也是嗤之以鼻。他调侃到，人们实在想不出，为什么民主党政客们如此希望非法移民涌入我们的国家。我告诉你们，“他们是希望在墨西哥拉选票。”他强调说：“注意哦，不是新墨西哥州，而是墨西哥！”
 </p>
 <h3>
  <strong>
   第六方面：民主党总统热门候选人是证据确凿的“极端分子”。
  </strong>
 </h3>
 <p>
  诺尔特认为，许多民主党总统候选人都是证据确凿的“极端分子”。他谈到，民主党那些总统候选人，他们支持的政策，他们的言论都说明了他们有多么极端。“他们承诺枪支管制，开放边境，堕胎合法化，为非法移民提供政府医保，强迫乘校车政策，免除1.5万亿学生贷款，强制取消私人医保，还要为变性男人提供他们永远用不上的免费堕胎，等等。”
 </p>
 <h3>
  <strong>
   第七方面：川普为美国人创造了前所未有的更多工作机会，对美国是好消息，对民主党却是坏消息。
  </strong>
 </h3>
 <p>
  诺尔特认为，美国的好消息对民主党人来说几乎总是坏消息。他谈到，如果经济下滑，可能成为美国人想要更换现任总统的唯一理由，而上个月，美国企业创造了224,000个工作机会。“数据显示，本周美国的就业人数，非常健康的就业人数，已成为民主党最糟的噩梦。”
 </p>
 <h3>
  <strong>
   第八方面：美国股市牛气冲天。
  </strong>
 </h3>
 <div>
 </div>
 <p>
  不用多说了。
 </p>
 <h3>
  <strong>
   第九方面：连沃伦和哈里斯这样的极端政客也成为民主党总统候选人。
  </strong>
 </h3>
 <p>
  诺尔特最后提到，民主党政客沃伦和哈里斯的人气已越来越盖过拜登，成为了热门总统候选人。然而他们的政策就更加极端了。他将这些候选人比喻为法西斯者。他认为这些人的支持率攀升，对民主党来说，也是一种灾难。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    楊曉
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3014473.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3014473.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n3014473.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/07/07/n3014473.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

