### 分析：川普“G20+韩国+板门店”亚洲四日行 外交获大丰收
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img alt="川普总统6月底出席日本大阪G20峰会、顺访板门店和金正恩见面，外交获大丰收。(AP图片合成)" class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2019/06/ap19181249670047-5in1-600x400-600x400.jpg"/>
 </div>
 <div class="caption">
  川普总统6月底出席日本大阪G20峰会、顺访板门店和金正恩见面，外交获大丰收。(AP图片合成)
 </div>
</div>
<hr/>
<div class="content">
 <p>
  <span class="content-info-date">
   【希望之声2019年6月30日】
  </span>
  <span class="content-info-type">
   （本台记者張莉莉综合编译）
  </span>
  川普总统6月底出席日本大阪G20峰会、顺道访问韩国以及朝韩军事分界线板门店的四日亚洲行已接近尾声，有分析认为，川普此行再次将他的外交艺术发挥的淋漓尽致，获得了一个个“外交大丰收”。
 </p>
 <div class="widget ad-300x250 ad-ecf">
  <!-- ZW30 Post Embed 300x250 1 -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-slot="9768754376" style="display:inline-block;width:300px;height:250px">
  </ins>
 </div>
 <p>
  据美联社的分析报导，川普总统此次赴日本大阪出席G20峰会，与9个国家元首举行了双边会谈，其中包括全世界瞩目的“川习会”。用他自己的话说，“我和每个人都有很好的关系。”
 </p>
 <h4>
  <strong>
   一、贸易战中“川习会” 贸易谈判重回正轨
  </strong>
 </h4>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_2996143">
  <img alt="" class="wp-image-2996143 size-medium" src="http://img.soundofhope.org/2019/06/ap19180108597311-600x400.jpg" srcset="http://img.soundofhope.org/2019/06/ap19180108597311.jpg 600w, http://img.soundofhope.org/2019/06/ap19180108597311-180x120.jpg 180w, http://img.soundofhope.org/2019/06/ap19180108597311-366x244.jpg 366w">
   <br/><figcaption class="wp-caption-text">
    6月29日川习会晤，贸易战暂时“停火”。 (AP Photo/Susan Walsh)
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  “川习会”之后，川普在新闻发布会上表示，与习的会晤“非常好，非常出色，甚至比预期的更好，谈判正回到正轨。”
 </p>
 <p>
  具体来说，美方的让步仅为：暂时不对3,000亿商品增加关税（之前加增的关税不变）；放松对华为的产品出口禁令，但仅限于不涉及美国国家安全的产品；不阻止中国学生来美留学。而中方则承诺：在回到4月底谈定的协议基础上继续谈判，其中包括进行必要的结构改革，不再窃取美国知识产权，取消强制技术转让，停止大量补贴重点行业；立即行动，大量购买美国农产品，并容许美方提出采购项目清单，等等。
 </p>
 <h4>
  <strong>
   二、访问韩国 跨入朝鲜 和金正恩在板门店会面
  </strong>
 </h4>
 <p>
  G20峰会后，6月29日，川普总统乘专机抵达韩国乌山空军基地，随后前往首尔与文在寅会面。在这之前川普总统就发推文表示：“我正从日本启程前往韩国。在那里，如果朝鲜的金主席看到这条信息，我愿意在边境非军事区与他会面，握手，并打个招呼！”川普此言一出，朝鲜方面立即回应，说这是个“非常有趣的建议”。
 </p>
 <p>
  接下来在与川普会面之后，文在寅在新闻发布会上确认，金正恩将与川普总统在非军事区见面。文在寅表示：“这将是真正的历史时刻。”
 </p>
 <p>
  川普总统6月30日下午2点30分（当地时间）抵达板门店非军事区（DMZ），在前沿哨所视察后，川普乘车离开。大约3点47分，金正恩也来了，川普总统和金正恩见面。两人先在军事分界线握手，随后川普步入朝鲜一侧，并与金正恩合影，然后返回韩国一侧。
 </p>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_2996716">
  <img alt="" class="size-medium wp-image-2996716" src="http://img.soundofhope.org/2019/06/ap19181249670047-600x400-1-600x400.jpg" srcset="http://img.soundofhope.org/2019/06/ap19181249670047-600x400-1.jpg 600w, http://img.soundofhope.org/2019/06/ap19181249670047-600x400-1-180x120.jpg 180w, http://img.soundofhope.org/2019/06/ap19181249670047-600x400-1-366x244.jpg 366w">
   <br/><figcaption class="wp-caption-text">
    2019年6月30日，川普总统在板门店朝鲜一侧和金正恩合影。(AP Photo/Susan Walsh)
   </figcaption><br/>
  </img>
 </figure><br/>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_2996689">
  <img alt="" class="size-medium wp-image-2996689" src="http://img.soundofhope.org/2019/06/ap19181249561896-600x400.jpg" srcset="http://img.soundofhope.org/2019/06/ap19181249561896-600x400.jpg 600w, http://img.soundofhope.org/2019/06/ap19181249561896-768x512.jpg 768w, http://img.soundofhope.org/2019/06/ap19181249561896-1024x682.jpg 1024w, http://img.soundofhope.org/2019/06/ap19181249561896-180x120.jpg 180w, http://img.soundofhope.org/2019/06/ap19181249561896-366x244.jpg 366w">
   <br/><figcaption class="wp-caption-text">
    2019年6月30日，川普总统在朝韩军事分界线板门店和金正恩握手。(AP Photo/Susan Walsh)
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  今年2月的第二次川金会，因为双方在去除核武器以及减少对朝制裁的问题上意见不一，谈判破裂。外界分析，此次川普再次向朝鲜释出善意，为第三次正式峰会打下基础。
 </p>
 <h4>
  三、打趣普京 “不要干预美国大选”
 </h4>
 <figure class="wp-caption aligncenter img-width-m" id="attachment_2996500">
  <img alt="" class="wp-image-2996500 size-medium" src="http://img.soundofhope.org/2019/06/6.29-1-600x440.jpeg" srcset="http://img.soundofhope.org/2019/06/6.29-1-600x440.jpeg 600w, http://img.soundofhope.org/2019/06/6.29-1-768x564.jpeg 768w, http://img.soundofhope.org/2019/06/6.29-1-180x132.jpeg 180w, http://img.soundofhope.org/2019/06/6.29-1-366x269.jpeg 366w, http://img.soundofhope.org/2019/06/6.29-1.jpeg 800w"/>
  <br/><figcaption class="wp-caption-text">
   6月28日，川普与普京等国家元首在G20峰会碰面寒暄。（AP Photo）
  </figcaption><br/>
 </figure><br/>
 <p>
  G20峰会上，川普与俄国总统普京会晤后，双方都表示，改善双边关系符合两国和全世界的利益。川普说他与普京的关系“非常非常好”，并表示“两国关系将产生许多积极的东西”。普京还邀请川普明年5月前往莫斯科，参加前苏联击败纳粹德国75周年纪念活动。
 </p>
 <div>
 </div>
 <p>
  民主党针对川普总统的“通俄门”指控，在经历数月的调查之后宣告失败。川普总统将该指控称为对他的“政治骚扰”，认为那是民主党陷害他的手段。在与普京会面时，川普总统幽默地对他说：“你不要干预美国大选。”逗得普京憋不住笑出声来。
 </p>
 <p>
  对此，美国前驻俄大使迈克尔·麦克福尔（Michael McFaul）表示：“川普总统希望与普京建立一个积极友好的双边关系。这是他达成下一个外交目标的基础。”
 </p>
 <p>
  另外，川普此次还与沙特阿拉伯王储穆罕默德·本·萨勒曼（Mohammed bin Salman）以及土耳其总统雷杰普·塔伊普·埃尔多安（Recep Tayyep Erdogan ）会晤并建立了友好的外交关系。
 </p>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    楊曉
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n2996494.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n2996494.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_rdzz/n2996494.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2019/06/30/n2996494.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

