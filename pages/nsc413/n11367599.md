### 学者：若贸易战转为货币战 中共的选择很少
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/1207061532141657-600x400.jpg"/>
<div class="red16 caption">
 <p>
  中共体制内学者近期在美国顶级期刊刊文，罕见提及“必须为贸易战升级为货币战争的可能性做好准备”，文章也从另一方面证明，若贸易战升级，中共几无应对之力。(Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月06日讯】（大纪元记者林燕综合报导）中共体制内学者近期在美国顶级期刊刊文，罕见提及“必须为
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%B8%E6%98%93%E6%88%98.html">
  贸易战
 </span>
 升级为
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%A7%E5%B8%81%E6%88%98.html">
  货币战
 </span>
 争的可能性做好准备”。该文凸显中共顶级学者对贸易战走向的思考，也从另一方面证明，若贸易战升级，中共几无应对之力。
</p>
<p>
 中共体制内学者
 <span href="http://www.epochtimes.com/gb/tag/%E4%BD%99%E6%B0%B8%E5%AE%9A.html">
  余永定
 </span>
 周四（7月4日）在美国《评论汇编》期刊（Project Syndicate）刊登的文章，题为“川普（特朗普）对华
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%B8%E6%98%93%E6%88%98.html">
  贸易战
 </span>
 的下一步走向”。他说，“如果人民币处于贬值压力之中、而（中共央行）中国人民银行未介入以稳定其对美元的汇率，美国可能会将中国列为汇率操纵国”。
 <span href="http://www.epochtimes.com/gb/tag/%E4%BD%99%E6%B0%B8%E5%AE%9A.html">
  余永定
 </span>
 是前中共央行货币政策委员会委员、社会科学院世界经济与政治研究所资深研究员。
</p>
<p>
 美国财政部每半年发表一份汇率报告，虽然这些年尚未把中国列为货币操纵国，但中国一直是美国密切关注的对象。
</p>
<p>
 尤其是美中贸易战以来，总统川普过去多次谈及要把中国列为货币操纵国，而中共长期外汇管理不透明，外汇储备构成以及外汇购买行为也都不公开。
</p>
<p>
 若贸易战升级为
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%A7%E5%B8%81%E6%88%98.html">
  货币战
 </span>
 ，将重创中国经济。“不幸的是，中国（中共）在这方面并没有什么回旋余地。”余永定写道。
</p>
<p>
 他还提到，中方在应对美国金融制裁的前景上也同样黯淡。上个月，一名美国法官认定三家大型中资银行藐视法庭，拒绝提供证据以调查朝鲜违反制裁的行为。
</p>
<p>
 余永定认为，中方能解决此类纠纷的可能性很小。他提到，中国金融机构需要做准备、应对包括被美国列入黑名单的风险。
</p>
<p>
 “若中国金融机构受限、不得使用美元以及各项重要服务的权利，例如被全球银行间金融电信协会金融信息服务（SWIFT）和纽约清算所银行间支付系统（CHIPS）排除在外，很少有企业能够扛住这惩罚措施。”他写道。
</p>
<p>
 他还举例说，已有一家中国银行被列入了《代理账户或代汇账户制裁》（CAPTA）清单，这意味着该银行无法在美国开设代理账户或代汇账户。
</p>
<p>
 他表示，“中国（中共）政府在这方面的选择很少”。除了加强立法以保护中国银行利益，鼓励中国金融机构极其谨慎地服从美国的金融法规，同时在人民币国际化方面还有很长的路要走。
</p>
<p>
 他认为，中方必须把精力加倍放在强化产权、坚持竞争中立等方面。
</p>
<p>
 余永定的文章中还提到，虽然美国对华关税尚未对中国经济造成严重冲击，但影响可能会随着时间的推移而不断加深。
</p>
<p>
 对贸易战加速外国企业转移业务到越南和泰国等较低成本国家，他认为，中国（中共）需要“改善（国内）当地投资环境，包括回应外国公司的合法投诉——比如加强知识产权保护，以及在更广泛意义上强化对世界贸易组织规则的遵守”。
</p>
<p>
 余永定罕见提及“必须为贸易战升级为货币战争的可能性做好准备”，代表着中共体制内顶级学者对贸易战下一步走向的最新思考，也从侧面证明，若中美贸易战升级，中共几无应对之力；换句话说，中共不敢轻易尝试升级中美贸易战，更惧触发货币战。#
</p>
<p>
 责任编辑：李缘
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11367599.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11367599.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11367599.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/5/n11367599.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

