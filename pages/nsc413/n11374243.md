### 王喜讽港警不戴委任证：只有坏蛋才匿藏身份
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="王喜嘲讽港警不戴委任证" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/190121070536100311-1-600x400.jpg"/>
<div class="red16 caption">
 <p>
  港星王喜资料照。（宋碧龙／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月10日讯】（大纪元记者佟亦加综合报导）在百万香港民众多次游行抗争恶法之后，特首林郑月娥7月9日在记者会上承认，《逃犯条例》（又称《引渡条例》或《送中条例》）的修订已经“完全失败”，明言修例已“寿终正寝”。但是，林郑并没有表示撤回修例，其表述依然被市民解读为玩文字游戏。
</p>
<p>
 由于林郑7月9日在记者会上所言“寿终正寝”这四个字并不等于撤回，因此支持
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 的市民和团体都担心修例草案在将来可能随时“复活”。一直参与反送中示威游行的港星
 <span href="http://www.epochtimes.com/gb/tag/%E7%8E%8B%E5%96%9C.html">
  王喜
 </span>
 ，7月9日晚在脸书以黑底白字帖文：“今寿终正寝，明借尸还魂，因完全失败，必卷土重来。”
</p>
<p>
 随后，
 <span href="http://www.epochtimes.com/gb/tag/%E7%8E%8B%E5%96%9C.html">
  王喜
 </span>
 又在脸书贴出在报纸专栏发表的长文，揭示中共俨然将暴力输入到香港，批评香港警方在
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 运动中执勤不戴委任证的行为，并直斥只有坏人才如此匿藏身份。
</p>
<p>
 一个多月来，香港民众持续反恶法，每次都是人山人海，秩序井然，令全球震撼。继6月9日103万人、6月16日200万人、7月1日55万人的大游行之后，港府对民众的诉求依然置若罔闻，有香港网民发起了7月7日到途经大陆游客必到的九龙尖沙咀游行。
</p>
<p>
 据大纪元报导，当日23万人的“反送中”游行，吸引了众多大陆民众了解被中共封锁的消息。由于部分游行人士入夜后未散去，晚上11时许，警方开始清场，在旺角弥敦道以“包抄”方式追截示威者，现场多次爆发冲突。在民众并无出现任何冲击行为的情况下，警方以警棍殴打示威者和记者，以长盾推撞议员，用武力制服无辜的行人。
</p>
<p>
 而有身穿便衣的警员，戴头盔、持圆盾参与清场行动，但没有展示警员编号或委任证。
</p>
<p>
 每次都参与游行的艺人王喜，在文章中描绘了这一幕：“七月七日晚，一位戴着警察头盔的便衣男子威风凛凛地向文明世界宣布︰‘这里只要有一位警员有委任证，代表这里全部是正规警察！’。”
</p>
<p>
 曾在港英政府时期担任警察的王喜嘲讽警方：“多环保啊！3万警察，印30张委任证就够，年终省了几吨胶。多先进啊！超然于全国200万天天挂上终身号码示人的公安。多威风啊！与伊拉克橙衣蒙面组织的隐藏身份技术看齐。作为供养这队为数三万，2019/2020年度开支预算为207亿港元，即平均每位警察拿69万公帑的港人，应作何反应呢？”
</p>
<p>
 警方网站的资料显示，便衣人员与巿民接触时，不论是否当值，在行使警察权力时，必须表明身份及出示委任证。如市民提出要求，军装警务人员须出示委任证，除非“情况不容许”、“出示委任证会影响警队行动及/或危及有关人员的安全”、“要求不合理”。
</p>
<p>
 王喜说：“巴士（司机）家属搭巴士都要出示家属证，煤气公司抄表员入屋前都要出示职员证，街市排档持牌人都要随身携带小贩证供食环署突击检查。”他揭示警方：“自6月12日起，你们一而再再而三，亲手摘下向特区政府宣誓效忠后所得的终身号码，由羞愧默认为求目的，不得不不择手段，到严正公告‘警察执行任务不需要出示委任证’。”
</p>
<p>
 面对警察的滥权施暴，王喜直斥：“香港警察自我确诊‘逃避刑责’已成高度传染风土病，专属这三万人交叉感染。自古以来，警匪片中，只有邪恶坏蛋才要想尽办法匿藏身份，逃避正义警察追捕。”
</p>
<p>
 看到王喜的文章，网友纷纷表示：“正义王喜！”“写得太好了！”“谢谢你！”“教我如何唔like你？”“勇敢的艺人！”
</p>
<p>
 责任编辑：杨明
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11374243.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11374243.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11374243.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/9/n11374243.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

