### 一个被监控15年的中国家庭 （1）
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="“709”律师谢燕益一家五口。（原珊珊提供）" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/xie-yanyi-and-yuan-shanshan-6-600x400.jpg"/>
<div class="red16 caption">
 <p>
  “709”律师谢燕益一家五口。（作者提供）
 </p>
</div>
<hr/><p style="padding-left: 30px;">
 <em>
  他依法去起诉一个违法之人，从此被这个国家监控。
 </em>
</p>
<p style="padding-left: 30px;">
 <em>
  妻子给他看了一张照片，没想到所引发的事件，成为“709”大抓捕的导火索。他因此被监禁酷刑，而他的妻子和家庭，长期处于严密监控之下。
 </em>
</p>
<p style="padding-left: 30px;">
 <em>
  走过“709”，监控打压依旧，但走出伤痛的他们，也走出了恐惧。这对夫妻的经历说明，任何以国家之力的残酷镇压，无论多么貌似强大，都无法征服人性中的良善与勇气，正义必将昭然。
 </em>
</p>
<h4>
 一、对前总书记发起的挑战
</h4>
<h3>
 <strong>
  唯一的遗憾
 </strong>
</h3>
<p>
 他其实心里没底，不知进去后能不能出来了。
</p>
<p>
 进了北京一中院立案庭的门，他对接待的女士说：“我要起诉。”
</p>
<p>
 那女士看到诉状，有些发懵，缓过神来，说要请示领导，回来后，把诉状退给了他。
</p>
<p>
 他严肃起来：“如果不给我立案，一切法律后果你们承担。”女士又去请示，回来收了他的材料，“填表吧。”
</p>
<p>
 被告一栏，他填上“江泽民”，原告一栏上，他填上了自己的名字：
 <span href="http://www.epochtimes.com/gb/tag/%E8%B0%A2%E7%87%95%E7%9B%8A.html">
  谢燕益
 </span>
 。
</p>
<p>
 因为紧张，填表的字他没写好。他想换一张，不给换。那么烂的字，蜘蛛爬一样，他觉得没有面子，“如果再有第二次，我一定把字写好。”这是他那天唯一的遗憾了。
</p>
<figure class="wp-caption aligncenter" id="attachment_11367134" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1-xie-yanyi-and-yuan-shanshan.jpg">
  <img alt="2003年秋天，谢燕益到天安门举牌，劝退江泽民下台。（作者提供）" class="size-large wp-image-11367134" src="http://i.epochtimes.com/assets/uploads/2019/07/1-xie-yanyi-and-yuan-shanshan-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2003年秋天，
  <span href="http://www.epochtimes.com/gb/tag/%E8%B0%A2%E7%87%95%E7%9B%8A.html">
   谢燕益
  </span>
  到天安门举牌，劝江泽民下台。（作者提供）
 </figcaption><br/>
</figure><br/>
<h3>
 <strong>
  “他和我接触的其他男孩不一样”
 </strong>
</h3>
<p>
 谢燕益认为这事非常紧要，“江泽民就是应该依法下台，为什么还当军委主席？”怎么大家都看不见？研究了相关法律、政治之后，他决定起诉江泽民，“他违反了宪法及民法通则。”
</p>
<p>
 那是2003年11月，谢燕益28岁，考上律师第三年。他称，对中共前总书记发起的法律挑战，是中国历史上的“宪政第一诉”，“法律提供了这个渠道，我就要利用它。”
</p>
<p>
 去起诉立案前，谢燕益就抱着“风萧萧兮易水寒，壮士一去兮不复还”的准备了，没有与任何人商量，他只告诉了女友原珊珊。珊珊替他买了很多邮票和信封，邮寄给他能够想到的各个部门。
</p>
<p>
 珊珊不觉得有什么风险，“他是律师，不会做违反法律的事啊。”
</p>
<p>
 但当天晚上，三个穿军大衣的保安，搬了个沙发，直接住他家门口了，24小时守着，从此就看管上他了。
</p>
<p>
 “现在这个事情严重了”，谢燕益去找了珊珊，“为了你的安全，咱们分手吧。”
</p>
<p>
 珊珊认为，他和她接触的其他男孩不一样，“那个时候我比较傻，不太了解他做的事会有什么后果。他没因我傻就牵连我，反而让我离开。我觉得他比较有责任心，所以我就那么决定了。”
</p>
<p>
 她去找了他，“我觉得那时他需要有人在身边。”后来珊珊给他生了儿子，后来他们结婚了。
</p>
<p>
 当时谢燕益的朋友，包括珊珊在内，都被国保调查了。谢燕益的父母和他划清了界限，母亲不让他到家里来，父亲把他赶出了家门。
</p>
<h3>
 <strong>
  国保跟踪到密云
 </strong>
</h3>
<p>
 从“诉江”以后，谢燕益就一直被国保监控。国家只要一开大会小会，节假日敏感日，国保就24小时监视他和珊珊的住处，“他们夜里睡在面包车里，也很辛苦的。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11367155" style="width: 450px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/xie-yanyi-and-yuan-shanshan-monitoring-8.jpg">
  <img alt="2016到2017年，谢燕益家楼道里、地下车库里的监控人员及无牌照车，原珊珊手机拍摄。（作者提供）" class="wp-image-11367155 size-medium" src="http://i.epochtimes.com/assets/uploads/2019/07/xie-yanyi-and-yuan-shanshan-monitoring-8-450x625.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2016到2017年，谢燕益家楼道里、地下车库里的监控人员及无牌照车，原珊珊手机拍摄。（作者提供）
 </figcaption><br/>
</figure><br/>
<p>
 为了躲开压力，他们选择到密云去租房。
</p>
<p>
 住进租房不久，密云县居委会、派出所所长还有国保就来了，“谢燕益在家吗？”珊珊又害怕又紧张，谎称不在，“如果你们想进来，就拿传唤手续。”
</p>
<p>
 这时，突然谢燕益在屋里面说，“在家，进来吧！”
</p>
<p>
 互相介绍之后，国保问谢燕益最近都做什么案子，对政府怎么看，有什么需要帮助的，什么给你媳妇找个工作、孩子上学什么的，如果需要帮忙的，都可以找他们……
</p>
<p>
 谢燕益说，“感谢你们，现在我们生活很满足，妻子要在家带孩子，同时也不希望你们再来家里，有事可以单独找我……”
</p>
<p>
 临走时，那国保开玩笑说，“老谢，你媳妇太厉害了，你要是想换就告诉我，我把她弄进去。”
</p>
<h3>
 <strong>
  “直到我们老二满月，我们才有能力吃饱”
 </strong>
</h3>
<p>
 之后他们在密云也处于监控之下了。他们靠咨询和代理案件生存，由于国保、司法局经常骚扰，他们被迫多次搬家。
</p>
<p>
 那时密云房价是每平米1300元左右，他们租的房子一年才2000元。
</p>
<p>
 很长一段时间，他们吃了上顿没下顿。有时，谢燕益出去，兜里只有坐车的钱。见了人，人家问吃饭了没，他说吃过了，他没钱请人家。
</p>
<p>
 每天，谢燕益都为一家四口能吃饱而奔波，直到2008年，“我们老二满月的时候，我们才有能力吃饱饭”。那时，谢燕益31岁，珊珊26岁。
</p>
<p>
 珊珊对人生原来想得很简单，“就是赚钱，吃饱饭”，接触谢燕益了，“他还想什么怎么做人？人还会有底线？人还要有什么社会责任？”这都是她没有思考过的，“这确实让我整个人精神起来”。
</p>
<p>
 “物质与精神共同进步，才是一个人。”这是谢燕益常说的一句话，那时，她听不太懂。但是，“我仰望他，所以跟他吃再多的苦，对我来说也不算啥。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11367133" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/xie-yanyi-and-yuan-shanshan-12.jpeg">
  <img alt="“709”律师谢燕益近照。（作者提供）" class="size-large wp-image-11367133" src="http://i.epochtimes.com/assets/uploads/2019/07/xie-yanyi-and-yuan-shanshan-12-600x450.jpeg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  “709”律师谢燕益近照。（作者提供）
 </figcaption><br/>
</figure><br/>
<h3>
 <strong>
  “我想和那世界远一点”
 </strong>
</h3>
<p>
 一次，珊珊帮他整理案卷材料，无意看见了一张照片，是一位老人的腿碳化后的惨状，“像黑炭一样，非常可怕”。谢燕益对她说：“这是被迫害的。”那是他代理的一个法轮功信仰案。
</p>
<p>
 “我真不愿听”，珊珊愿意看窗外，外面是大好的阳光，下面有很好的马路，人们都在干着自己的事情，“我想和那世界远一点。”
</p>
<p>
 珊珊认为那些是政治，她不想关心政治，有时她也对丈夫说一句：“这事为什么非得你管？”不过她觉得他也没有错，“总得有人替这老人伸张权利啊！”
</p>
<p>
 每日管孩子、做家务，珊珊就是希望过好“小日子”。那时，她觉得那种恐惧、迫害离她非常遥远。
</p>
<p>
 她怎么也没想到，她给谢燕益看的一张照片，会给谢燕益和自己的家庭带来了灾难。（待续）@*#
</p>
<p>
 <em>
  <span href="http://www.epochtimes.com/gb/tag/一个被监控15年的中国家庭.html" rel="noopener noreferrer" target="_blank">
   点阅《
   <span style="color: #0000ff;">
    <span href="http://www.epochtimes.com/gb/tag/%E4%B8%80%E4%B8%AA%E8%A2%AB%E7%9B%91%E6%8E%A715%E5%B9%B4%E7%9A%84%E4%B8%AD%E5%9B%BD%E5%AE%B6%E5%BA%AD.html">
     一个被监控15年的中国家庭
    </span>
   </span>
   》全文。
  </span>
 </em>
</p>
<p>
 责任编辑：苏明真
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365012.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365012.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365012.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/4/n11365012.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

