### “709”大抓捕四周年 律师剖析中共惧怕原因
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/page-horz.jpg"/>
<div class="red16 caption">
 <p>
  中共制造的“709”大抓捕事件已四周年，当局持续的打压没有使这个群体沉寂，反而令更多有良知的人士加入，使其保持着持续的影响力。（网络图片）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月09日讯】（大纪元记者洪宁采访报导）中共制造的“709”大抓捕事件已四周年，当局持续的打压没有使这个群体沉寂，反而令更多有良知的人士加入，使其保持着持续的影响力。曾被当局非法判刑、遭受
 <span href="http://www.epochtimes.com/gb/tag/%E9%85%B7%E5%88%91.html">
  酷刑
 </span>
 虐待的律师，剖析中共打压及惧怕的原因时指，中共害怕此群体对其执政地位构成威胁。
</p>
<p>
 2015年5月，维权人士徐纯合在黑龙江庆安火车站被警察开枪打死，事后，当局拘留了20多名到场声援的民众及律师，引发660名中国律师联署抗议，外界指这是造成中共进行“709”抓捕的主要原因。
</p>
<p>
 2015年7月9日，中共在20多个省市，大肆抓捕
 <span href="http://www.epochtimes.com/gb/tag/%E7%BB%B4%E6%9D%83%E5%BE%8B%E5%B8%88.html">
  维权律师
 </span>
 、家属、维权公民，人数高达320多人。其中的许多律师曾为大量法轮功案做无罪辩护，代理农民土地案、基督徒案、公民维权案等，因而遭受当局
 <span href="http://www.epochtimes.com/gb/tag/%E9%85%B7%E5%88%91.html">
  酷刑
 </span>
 、判监、家属连坐以及被吊销律师执照等迫害。
</p>
<h4>
 中共惧怕 疯狂打压
</h4>
<p>
 “709”中遭受过酷刑的谢阳律师接受大纪元采访时透露，早在2014年，律师中就传出一个消息，当局已准备好了一份（律师）名单。
</p>
<p>
 谢阳表示，随着互联网的推广，越来越多的人知道这个社会的真相，同时也激发了他们对自由民主的追求，再加上律师用专业知识为他们提供法律帮助，就使得当局产生更加恐惧的危机感，它们担心此群体对执政地位构成威胁。
</p>
<p>
 在“709”事件中同样遭受迫害的一位资深律师表示，中国现在的维权运动，人权律师群体是中共独裁专治不受制约，持续侵犯民众权利导致的必然结果。当局侵权越恶劣，民众的反抗也越强烈，在这种情况下，中国
 <span href="http://www.epochtimes.com/gb/tag/%E7%BB%B4%E6%9D%83%E5%BE%8B%E5%B8%88.html">
  维权律师
 </span>
 遭到了全面的打压，当局实行秘密关押，判刑，很多律师被警告，吊销执照，注销律师执业证。
</p>
<p>
 中共并不是要求自己的改变来维护民众的权利，而是想通过制造红色恐怖气氛，使全国人面对打压退却，但它是无法得逞的。
</p>
<blockquote class="twitter-tweet" data-lang="zh-tw">
 <p dir="ltr" lang="zh">
  中国人权律师的声音
  <br/>
  ——关于“709事件”四周年的声明！
  <span href="https://t.co/QvRT2DwNxk">
   pic.twitter.com/QvRT2DwNxk
  </span>
 </p>
 <p>
  — 中国人权律师团 (@RightsLawyersCN)
  <span href="https://twitter.com/RightsLawyersCN/status/1148361434318827521?ref_src=twsrc%5Etfw">
   2019年7月8日
  </span>
 </p>
</blockquote>
<p>
</p>
<h4>
 打压的非法性
</h4>
<p>
 这位律师指，中共打压“709”群体的非法性表现在，它们在整个案件的办理过程中，大量将人秘密失踪，不让律师会见，家属所请的律师无法介入案件，大量的用官派律师充当整个案件的合法性道具。
</p>
<p>
 “它将以前非法的事情用法律的形势规定成合法的，这种形式的合法掩盖不了非法性，这个法律就是非法的，有中共的独裁，就不可能有真正的法制。”他指，中共还搞非法连坐株连他们的家属，比如：律师及家属被禁止出境，李和平律师孩子无法入学，
 <span href="http://www.epochtimes.com/gb/tag/%E7%8E%8B%E5%85%A8%E7%92%8B.html">
  王全璋
 </span>
 的儿子不能入园，谢燕益律师不断地被逼迫搬家，江天勇、谢阳律师不能去美国与妻女团聚。
</p>
<p>
 “这不仅违法，也反人性。”他说。
</p>
<h4>
 律师执业艰难
</h4>
<p>
 越来越多的维权律师遭中共封杀，执业日益艰难。
</p>
<p>
 王宇是“709”事件中第一位被抓的律师，遭受过酷刑虐待，被释放已近3年，虽然她和先生的律师证没有被吊销，但至今不能代理案件，因为没有律师事务所开所函（原所在的北京锋锐律师事务所已被中共吊销执照），且被司法局限制转所，王宇多次去司法局和律协要求恢复执业，但至今未有解决。
</p>
<p>
 “像我们这种状况的人是更多的，所以它对人权律师的迫害一直在进行，越来越严重，且不断地在变化方式。”她说。
</p>
<p>
 记者了解到，现在当局要求很多律师对法轮功案件不允许进行无罪辩护，造成很多律师不敢在厅上进行无罪辩护，只敢做些轻的有罪辩护。
</p>
<p>
 王宇律师认为，这是一个有罪和无罪本质上的问题，尤其是作为刑事案件的辩护，如果明明是无罪，但是作为律师都不敢做无罪辩护，只是做有罪辩护，这是更严重的问题。
</p>
<p>
 谢阳律师说，当局对他代理的案件有限制，两类案件不能接手，一是对国家安全有危害的，一是法轮功案件。“我可能会继续接受一些采访，把真相披露出去，所以不允许我代理。”
</p>
<p>
 “但我是这样做的，作为律师助理，陪他们一起去有关单位了解案情。另一方面，只要不需要开所函的，坚持以律师的身份去帮助他们。但是不给我出具手续，很多场合是受到限制的，这也是让我很苦恼的问题。”谢阳说。
</p>
<p>
 目前，谢阳律师所代理的都是涉及公权利部门违法犯罪案件，因此任何一个案件都受到来自当地地方政府、行政司法部门的干扰。
</p>
<p>
 曾是王宇辩护律师的文东海律师以前曾经代理过不少法轮功案件，后来遭到当局禁止。他表示，自己的执业资格也被剥夺，不后悔选择了维权律师这个职业，许许多多的维权律师都是很有责任心的，他们都在一步一步，踏踏实实地推动中国的法制进程。
</p>
<p>
 文东海表示，希望更多的人对“709”事件有一个正确的认识，只有大家都认识到这些律师为什么要做这样的事情，有什么意义，对社会有什么样的帮助，那么这个国家才会有真正的进步和文明。
</p>
<h4>
 “709”影响力在持续上升
</h4>
<p>
 “709”事件发生后，有一批为被抓捕的律师做辩护的律师被抓，还有一大批律师被吊照、注销执业证，但因为不断有新的律师加入到维权律师群体中来，使得群体力量和影响力不断壮大上升。
</p>
<p>
 在谈到这股力量对中国社会的影响时，资深律师认为，恰恰是当局对709群体持续的打压，使这些律师及家人必然有反应，国际上的关注也使这个事情平息不下去，此外，当局对其他的律师进一步打压威胁，也可能令公众关注。
</p>
<p>
 他认为，中共看来有这么多警察、特务维稳，看起来很强大，但它们的行为恰恰反映它们内心极度的虚弱恐惧，对于它末日到来的焦虑。而另一方面，这些家属持续地抗争，也非常有效。
</p>
<p>
 谢阳律师认为，“709”能有这样的关注度，取决于国际上对中共人权迫害的谴责，国内的民从通过不同的途径和方式表达对维权律师的支持，“他们这些行动让我们备受鼓舞，在我们获得自由后，回报对我们进行帮助的人和机构，我们愿意继续从事人权工作。”
</p>
<p>
 王宇律师指，“709”律师以及他们的辩护律师，还包括之前像唐荆陵、高智晟、唐吉田等人权律师，像郭飞雄、许志友等法律人，他们对独裁体制的抗争，对中国法制是一种推动，也进一步使国际社会认清了中共独裁统治真实的面貌，同时也推动世界各个国家能够持续进一步关注中国人权问题。
</p>
<blockquote class="twitter-tweet" data-lang="zh-tw">
 <p dir="ltr" lang="zh">
  全世界陪着我们——坚持到底
  <br/>
  ——709四周年声明
  <span href="https://t.co/1bEPBN9rog">
   pic.twitter.com/1bEPBN9rog
  </span>
 </p>
 <p>
  — 李文足（
  <span href="http://www.epochtimes.com/gb/tag/%E7%8E%8B%E5%85%A8%E7%92%8B.html">
   王全璋
  </span>
  妻子） (@709liwenzu)
  <span href="https://twitter.com/709liwenzu/status/1148472246534410242?ref_src=twsrc%5Etfw">
   2019年7月9日
  </span>
 </p>
</blockquote>
<p>
</p>
<p>
 #
 <br/>
 责任编辑：高静
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11372840.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11372840.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11372840.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/9/n11372840.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

