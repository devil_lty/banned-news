### 一个被监控15年的中国家庭 (2)
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="参加“709”家属“我可以无发，你不能无法”剃头活动后的原珊珊和女儿。（原珊珊提供）" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/xie-yanyi-and-yuan-shanshan-3-600x400.jpg"/>
<div class="red16 caption">
 <p>
  参加“709”家属“我可以无发，你不能无法”剃头活动后的原珊珊和女儿。（作者提供）
 </p>
</div>
<hr/><p style="padding-left: 30px;">
 <em>
  他依法去起诉一个违法之人，从此被这个国家监控。
 </em>
</p>
<p style="padding-left: 30px;">
 <em>
  妻子给他看了网上的一张照片，没想到所引发的事件，成为“709”大抓捕的导火索。他因此被监禁酷刑，而他的妻子和家庭，长期处于严密监控之下。
 </em>
</p>
<p style="padding-left: 30px;">
 <em>
  走过“709”，监控打压依旧，但走出伤痛的他们，也走出了恐惧。这对夫妻的经历说明，任何以国家之力的残酷镇压，无论多么貌似强大，都无法征服人性中的良善与勇气，正义必将昭然。
 </em>
</p>
<p>
 续前文：
 <span style="color: #0000ff;">
  <span href="https://www.epochtimes.com/gb/19/7/4/n11365012.htm" rel="noopener noreferrer" style="color: #0000ff;" target="_blank">
   一、对前总书记发起的挑战
  </span>
 </span>
</p>
<h4>
 二、如果你是谭嗣同的妻子，你怎么办？
</h4>
<p>
 <strong>
  关注“庆安”事件
 </strong>
</p>
<p>
 2015年5月的一天晚上，珊珊注意到网上的一张照片，是一位老母亲和三个小孩，围着被警察击毙倒地的
 <span href="http://www.epochtimes.com/gb/tag/%E5%BE%90%E7%BA%AF%E5%90%88.html">
  徐纯合
 </span>
 ，徐纯合是这三个孩子的父亲。
</p>
<p>
 她把照片给
 <span href="http://www.epochtimes.com/gb/tag/%E8%B0%A2%E7%87%95%E7%9B%8A.html">
  谢燕益
 </span>
 看。和珊珊一样，谢燕益也觉得此事和他有关，不只是从律师的角度。那一年，谢燕益和珊珊的两个儿子，都不足十岁。
</p>
<p>
 分析现场后，
 <span href="http://www.epochtimes.com/gb/tag/%E8%B0%A2%E7%87%95%E7%9B%8A.html">
  谢燕益
 </span>
 提出了质疑：
 <span href="http://www.epochtimes.com/gb/tag/%E5%BE%90%E7%BA%AF%E5%90%88.html">
  徐纯合
 </span>
 拖家带口，没带凶器，没有作案预谋，警察为什么把他当作恐怖分子击毙？
</p>
<p>
 他把自己的感慨发到了微信朋友圈及律师群，朋友圈里有庆安本地人，有人就给他发信息：“我当天在现场，我知道这个事。”他至少找到了三个知情人，都愿意和他见面。
</p>
<p>
 谢燕益打了网友提供的一个电话，意外发现，对方是徐纯合的母亲权玉顺。知道他是北京的律师，权玉顺说：“求求你给我主持公道啊！我儿子死得不明不白……”通话过程中，一直有人干扰阻挡。谢燕益问她在哪，她说：“庆安市中医院。”
</p>
<p>
 谢燕益要去，二话没说，珊珊就给他订了第二天中午的票，他出门一向是她订票。
</p>
<p>
 <strong>
  见到了徐纯合母亲
 </strong>
</p>
<p>
 庆安中医院外面，已经有警车了，还有戴锅式帽的特警，医院大堂里，很多便衣走来走去。
</p>
<p>
 有人拍照，四楼病房门口有人把守阻挡，“不准外人与权玉顺见面”。但谢燕益和几个后来赶到的律师，还是硬闯进去，见了权玉顺，这位八十岁的老母亲，不认同政府补偿的20万块钱，“我儿子一条命就值20万？”
</p>
<figure class="wp-caption aligncenter" id="attachment_11367137" style="width: 450px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/2-xie-yanyi-and-yuan-shanshan.jpg">
  <img alt="2015年5月，谢燕益到庆安找徐纯合母亲，代理徐纯合被枪杀案。（原珊珊提供）" class="wp-image-11367137 size-medium" src="http://i.epochtimes.com/assets/uploads/2019/07/2-xie-yanyi-and-yuan-shanshan-450x600.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2015年5月，谢燕益到庆安找徐纯合母亲，代理徐纯合被枪杀案。（作者提供）
 </figcaption><br/>
</figure><br/>
<p>
 谢燕益与她签了代理合同，拿到了授权委托书。随后，谢燕益把权玉顺叙述的现场情况、徐纯合要带家人坐火车去上访的实情发到了网上。
</p>
<p>
 但微信上联系好见面的人，都拒绝了他，“我们学校专门下达了通知，这个事不能接受采访。”“我们不能见面了……”
</p>
<p>
 现场找到一些趴活的出租车司机、乘务人员、铁路工作人等，一个个都摇头退避了。一个自称姓于的中央电视台记者，却非常关心谢燕益：你现在在哪儿？你要去哪儿？
</p>
<p>
 <strong>
  寻找徐纯合的孩子
 </strong>
</p>
<p>
 当时，徐纯合的三个孩子被送进了福利院，妻子被送进了精神病院。谢燕益和李仲伟律师想帮孩子找到抚养他们的人，这需要家属的授权。
</p>
<p>
 谢燕益走访了徐纯合的叔伯兄弟，他们表示：不需要律师，不需要任何人帮助，政府都给我们安排了，我们很满意。
</p>
<p>
 在一个菜市场里的一个肉铺里，他们终于找到了徐纯合的妻姐。一见面，她就说不知道这个事，不愿参与。反复做工作之后，她同意签一个授权委托。
</p>
<p>
 带着委托书他们去了福利院，不让见孩子，他们坚持。最后一王姓院长，拍了一段视频给他们看，两个大男人当时就忍不住掉泪了。
</p>
<p>
 他们的爸爸前些天死在他们眼前，他们的奶奶被控制在医院，妈妈被送进了精神病院，而视频中，梳着羊角辫的姐姐和两个弟弟，天真快乐地玩着，特别开心的样子。
</p>
<p>
 孩子们看起来都挺知足，终于进了福利院了。而这，正是他们死去的爸爸带他们坐火车上访要解决的问题之一：老母亲养老问题，妻子精神病问题，孩子抚养问题。警察拦截他的原因，就因为他要上访。徐纯合死后，上访的问题政府全都给解决了。
</p>
<p>
 谢燕益去了六天，每一天调查取证的进展，他都告诉了妻子。珊珊当时还以为，谢燕益是在解答她对此事的关注呢。她不知道，丈夫已经处于危险之中了。
</p>
<figure class="wp-caption aligncenter" id="attachment_11367146" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/10-xie-yanyi-and-yuan-shanshan.jpg">
  <img alt="“709”律师谢燕益。（原珊珊提供）" class="size-large wp-image-11367146" src="http://i.epochtimes.com/assets/uploads/2019/07/10-xie-yanyi-and-yuan-shanshan-600x340.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  “709”律师谢燕益。（作者提供）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  如果你是谭嗣同的妻子，你怎么办？
 </strong>
</p>
<p>
 庆安回来后不久，珊珊对谢燕益说，“我可能又怀孕了。”谢燕益感到很突然，不太高兴，“多生一个孩子，在这个专制和奴役的社会，只不过又多了一个奴隶。”
</p>
<p>
 当时他不希望再有孩子，确实有些忧虑，但这事还没容他多想，更大的事发生了。
</p>
<p>
 很快，由于谢燕益在网上持续发布庆安事件的调查，很多公民和律师去庆安抗议，一些公民和熟识的律师——王宇、周世峰、刘世新等都被抓了。
</p>
<p>
 像往常一样，那天他们在楼下散步，后来坐在一块大石头上聊天。
</p>
<p>
 谢燕益对珊珊说，谁谁被带走了，谁谁谁消失了，谁谁被控制了，然后他说，自己现在也很危险。
</p>
<p>
 珊珊：“不行你就跑吧！躲起来！”
</p>
<p>
 谢燕益：“我没有做违法的事，这都是我应该做的，既然它来，我就在这儿等着吧。”
</p>
<p>
 最后，谢燕益就问了珊珊一个问题：如果你是谭嗣同的妻子，你怎么办？
</p>
<figure class="wp-caption aligncenter" id="attachment_11367143" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/7-xie-yanyi-and-yuan-shanshan.jpg">
  <img alt="2016年夏天，原珊珊与“709”家属姐妹搞“维权找爸爸”活动。（原珊珊提供）" class="size-large wp-image-11367143" src="http://i.epochtimes.com/assets/uploads/2019/07/7-xie-yanyi-and-yuan-shanshan-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2016年夏天，原珊珊与“709”家属姐妹搞“维权找爸爸”活动。（作者提供）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  恐惧到了
 </strong>
</p>
<p>
 珊珊自己也没想到，她关注的一张照片所引发的事件，竟然成为“709”大抓捕的导火索。
</p>
<p>
 7月11日，国保又约谈了谢燕益，问：对中国共产党有什么看法？谢燕益说，自己不是共产党员，不需要忠于党。国保要求他保证，不再为被捕的律师公开发言，谢燕益回答说，“发不发声是我的权利。”警察给他录了像，说要汇报给市局。
</p>
<p>
 回家时已经凌晨1点。六小时后，国保敲门了，密云公安局长要见谢燕益。
</p>
<p>
 一直到下午2点，丈夫都没回来。站在窗前不断张望的珊珊，看见楼下小区门口有二三十人，正呼啦呼啦往里走，“就像电影里那样”，他们边走边掏出白手套往手上戴，都穿便装，几乎全是小平头。
</p>
<p>
 直觉就是冲自己家来的。珊珊特别紧张，她拿起电话，想通知谢燕益的哥哥，但手抖得已经不听使唤了。
</p>
<p>
 她慌忙告诉两个儿子：就待在自己的房间里，无论外面发生什么，你们都不要出来。
</p>
<p>
 接着，二三十人一下就涌进来，家里被挤得满满的。
</p>
<p>
 他们要进孩子房间搜查，珊珊说那是孩子的房间，没有谢燕益的东西，他们还是冲进去了。
</p>
<p>
 透过房门的一点小缝，珊珊看到小儿子，他抱着被子，萎缩在床上，可怜巴巴望着自己。
</p>
<p>
 珊珊想，“不管怎么样，一定要在气势上压倒他们，给孩子打气。”
</p>
<p>
 她开始骂这些便衣：“你们做坏事会有报应的！”突然窗外就狂风大作，下起了冰雹，特别大，足足几分钟。他们很害怕的样子，大部分人都退到了楼道。
</p>
<p>
 等警察抄家走以后，珊珊打开儿子的房门，尿味扑鼻而来，两个儿子不敢出来上厕所，尿在矿泉水瓶里，瓶子又装满了……
</p>
<p>
 第三天晚上11点多，新华网报导了谢燕益被抓的消息，珊珊一夜没睡，写了一个对新华网的起诉书，凌晨5点多就发出去了。
</p>
<p>
 二小时后，儿子非常恐惧地盯着门口：有人敲门！国保。打开门他们就问珊珊：谁给你写的起诉书？谁给你出的主意？（待续）@* #
</p>
<figure class="wp-caption aligncenter" id="attachment_11367130" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/xie-yanyi-and-yuan-shanshan-9.jpeg">
  <img alt="谢燕益、原珊珊的三个孩子近照。（原珊珊提供）" class="size-large wp-image-11367130" src="http://i.epochtimes.com/assets/uploads/2019/07/xie-yanyi-and-yuan-shanshan-9-600x450.jpeg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  谢燕益、原珊珊的三个孩子近照。（作者提供）
 </figcaption><br/>
</figure><br/>
<p>
 <em>
  <span href="http://www.epochtimes.com/gb/tag/一个被监控15年的中国家庭.html" rel="noopener noreferrer" target="_blank">
   点阅《
   <span style="color: #0000ff;">
    <span href="http://www.epochtimes.com/gb/tag/%E4%B8%80%E4%B8%AA%E8%A2%AB%E7%9B%91%E6%8E%A715%E5%B9%B4%E7%9A%84%E4%B8%AD%E5%9B%BD%E5%AE%B6%E5%BA%AD.html">
     一个被监控15年的中国家庭
    </span>
   </span>
   》全文。
  </span>
 </em>
</p>
<p>
 #
 <br/>
 责任编辑：苏明真
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365103.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365103.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365103.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/4/n11365103.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

