### 大婚当天财产遭查封 张若昀遇最严限古令？
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2018/12/VCG111175621866_meitu_1-600x400.jpg"/>
<div class="red16 caption">
 <p>
  大陆男星张若昀资料照。（大纪元资料室）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月05日讯】（大纪元记者钟又淳综合报导）前几日，在大陆男星
 <span href="http://www.epochtimes.com/gb/tag/%E5%BC%A0%E8%8B%A5%E6%98%80.html">
  张若昀
 </span>
 婚礼当天，一份有关张若昀及其父亲
 <span href="http://www.epochtimes.com/gb/tag/%E5%BC%A0%E5%81%A5.html">
  张健
 </span>
 6,000万元（人民币，下同）财产被冻结的民事裁定书被爆出，引发网络热议。至于背后原因，有分析人士指，主要是张氏父子合拍已杀青两年的92集电视剧
 <span href="http://www.epochtimes.com/gb/tag/%E3%80%8A%E9%9C%8D%E5%8E%BB%E7%97%85%E3%80%8B.html">
  《霍去病》
 </span>
 遭遇了史上最严的“
 <span href="http://www.epochtimes.com/gb/tag/%E9%99%90%E5%8F%A4%E4%BB%A4.html">
  限古令
 </span>
 ”。
</p>
<p>
 6月27日，
 <span href="http://www.epochtimes.com/gb/tag/%E5%BC%A0%E8%8B%A5%E6%98%80.html">
  张若昀
 </span>
 与女演员唐若昕在爱尔兰举行婚礼。当天下午，张若昀在微博晒出两人的结婚大片，并配文“就这样笑一辈子吧”。
</p>
<p>
 从传出的现场图可见，张若昀骑着白马迎娶新娘，唐艺昕一袭刺绣婚纱出场，显得豪华浪漫。伴郎伴娘团井柏然、刘昊然、郭麒麟、宋茜、李沁、沈梦辰与新人有爱互动，在晚宴上，张若昀唐艺昕还跳起了双人舞。
</p>
<p>
 没想到的是，正陶醉在幸福和喜悦中的张若昀，当天却遇上了官司。原来，“华策影业”公司申请查封冻结扣押张若昀和他的父亲
 <span href="http://www.epochtimes.com/gb/tag/%E5%BC%A0%E5%81%A5.html">
  张健
 </span>
 价值6,000万的财产，正是在6月27日曝光了法院的裁定书。
</p>
<p>
 从民事裁定书上看，投资电视剧的“华策影业”于2018年4月向杭州仲裁委员会申请财产保全，并于2018年年底获得判决结果：查封、冻结、扣押被申请人浙江南北湖梦都影业、张若昀、张健财产价值60,095,000元人民币。其中，裁定书中提到的“梦都影业”最初的的法定代表人中有张健张若昀父子，后经多次变更，法定代表人于2017年变成了其他人。
</p>
<p>
 据搜狐娱乐报导，作为导演的张健，名下有多家公司。2015年，以他为主要股东的“西安梦舟”被上市公司“梦舟股份”收购，张健从中获利5亿多。因为张健家底雄厚，张若昀被网友称为“富二代”。
</p>
<p>
 报导指，一位知情人士透露，张健父子6,000万财产之所以被“华策”申请冻结，直接原因是张若昀的父亲张健在拍摄电视剧
 <span href="http://www.epochtimes.com/gb/tag/%E3%80%8A%E9%9C%8D%E5%8E%BB%E7%97%85%E3%80%8B.html">
  《霍去病》
 </span>
 时资金不足，曾向华策借款，但到期未还。为了确保将来裁决时赢得资金，“华策”申请财产保全。
</p>
<p>
 有律师解释，因为张若昀是连带责任保证人，因此才被牵扯到案件中，“所谓连带责任保证人，就是和债务人一样，债权人可以直接向担保人要钱，担保人不能拒绝，等于和自己欠人钱是一样的”。简言之，在这次债务中，张健欠钱未还，张若昀作为连带责任保证人，只能替父还债，因此财产才会被冻结。
</p>
<p>
 此外，公开资料显示，除了已经制作完成的《霍去病》，之后在2017年和2018年两年间，网传“华策”先后有古装剧《长歌行》、《极道少女养成记》、《四十成年礼》三部作品要和张若昀合作。不过，由于《霍去病》的合作纠纷，导致双方的后续合作无法进行。
</p>
<p>
 文章分析，上述债务问题，实则折射出演员、导演、影视公司在中国经济下滑背景下“资本退潮”，影视圈遭遇史上最严“
 <span href="http://www.epochtimes.com/gb/tag/%E9%99%90%E5%8F%A4%E4%BB%A4.html">
  限古令
 </span>
 ”密切相关。
</p>
<p>
 《霍去病》由张健执导，张若昀主演，2016年8月开机。彼时，热钱涌入影视圈，古装剧占了中国国产剧的半壁江山，如《芈月传》、《锦绣未央》、《女医明妃传》、《大唐荣耀》、《大军师司马懿之军师联盟》等。
</p>
<p>
 据早前报导，随着投资越来越大，大陆电视剧的集数也普遍从30+增长为50+，甚至是80+。在这个过程中，《霍去病》紧跟潮流，投资高达6.5亿，集数从一开始的30集，经过两次更改之后最终变成92集的长剧。
</p>
<p>
 不巧的是，当《霍去病》2018年制作完成时，遇上了史上最严的“限古令”。
</p>
<p>
 “限古令”明确要求，严禁古装宫斗剧在一线卫视播出。此令一出，古装剧撤档、台播走网播、滞压的剧集不在少数。
</p>
<p>
 等到2019年，形势更为严峻。
</p>
<p>
 一方面，中共广电总局再出新规：从3月到6月，所有古装题材网剧、电视剧都不允许播出，导致大批古装剧滞播，题材撞档严重。
</p>
<p>
 另一方面，2019年是中共窜政70周年，2021年是中共建党100周年，因此中共强推为其歌功颂德的“献礼剧”，现实题材作品获力捧。一位不愿具名的古装剧编剧就对搜狐娱乐透露，如今业内写古装剧的编剧不多了，大家纷纷转类型，写一些正剧、红色题材的剧本。
</p>
<p>
 迄今，《霍去病》仍未播出。就在今年3月，“华策影视”在第23届香港国际影视展时，还把《霍去病》作为重点项目进行推荐，但是否有平台购买，尚不可知。而受“限古令”的影响，张若昀主演的另一部古装剧《庆余年》也迟迟未能上线。
</p>
<p>
 事实上，电视剧《霍去病》因改编引发巨大争议，在网上掀起大论战。该剧自公布最新预告片后，就遭到包括多名历史学者和微博博主的批评。熟谙历史的专业人士表示，该剧不符合历史记载，并呼吁所有剧迷抵制该剧的播出。
</p>
<p>
 报导称，一直想拍电视剧《霍去病》的张健，大约四五年前，曾谋划让自己的儿子张若昀担任男主角，但30岁的张若昀愣是不相信霍去病是“天生战神”，便拒绝父亲的邀请。后来，张健对剧本进行改写，增加了霍去病成为将军之前在边塞的“草根”生活。而且，该剧还因在其它诸多情节上“胡编乱造”遭网友投诉。
</p>
<p>
 此外，还有业内人士描述：“当热钱退去，投机资金破产之后，整体情况就是行业不景气，大家都勒紧裤腰带过日子。”认为中美贸易战背景下的大陆影视业的残酷现实，也是张若昀财产遭查封的重要原因。
</p>
<p>
 责任编辑：叶紫微
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365311.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365311.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365311.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/4/n11365311.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

