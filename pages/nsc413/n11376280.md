### 河北一官员大闹酒店前台 扬言封了酒店
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/e9ae5b0df32f71f1a186b009041a3a33-600x400.jpg"/>
<div class="red16 caption">
 <p>
  7月5日，河北辛集市发改局科长盛某，大闹酒店前台。（视频截图）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月10日讯】（大纪元记者张顿报导）
 <span href="http://www.epochtimes.com/gb/tag/%E6%B2%B3%E5%8C%97%E7%9C%81%E8%BE%9B%E9%9B%86%E5%B8%82%E5%8F%91%E5%B1%95%E5%92%8C%E6%94%B9%E9%9D%A9%E5%B1%80.html">
  河北省辛集市发展和改革局
 </span>
 （发改局）科长盛某，入住酒店时不但不出具身份证，
 <span href="http://www.epochtimes.com/gb/tag/%E5%A4%A7%E9%97%B9%E9%85%92%E5%BA%97%E5%89%8D%E5%8F%B0.html">
  大闹酒店前台
 </span>
 ，而且还扬言要封了酒店。
</p>
<p>
 网上爆料，7月5日，河北辛集市发改局科长盛某酒后宾馆开房，拒不出示身份证， 还将酒店前台物品打落到地上，并骂不绝口。
</p>
<p>
 视频显示，这名身穿白色衬衣的男子还扬言：“不想干了是吧？找110把你们封了！”最后该男子在随行人员的劝阻下才停止了谩骂。
</p>
<p>
 爆料称，盛某是财政局长之弟，“可能他牛逼些，坑爹过后开始坑哥了？”
</p>
<figure class="wp-caption aligncenter" id="attachment_11376292" style="width: 440px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/822789a95db0014ee4d84c4aa1d9a8ab.jpg">
  <img alt="" class="wp-image-11376292 size-full" src="http://i.epochtimes.com/assets/uploads/2019/07/822789a95db0014ee4d84c4aa1d9a8ab.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  网曝盛某是财政局长之弟。（网页截图）
 </figcaption><br/>
</figure><br/>
<p>
 7月10日下午，河北省石家庄市辛集市纪委一名工作人员对澎湃新闻说，网传视频男子确实为辛集市发改局科长盛某，目前正在调查。
</p>
<p>
 辛集市发改局一名工作人员也表示，网传视频内容属实，目前辛集市纪委已介入调查。
</p>
<p>
 一个县级市发改局的科长，竟然如此嚣张，立即引发网民的热议：
</p>
<p>
 “辛集是县级市，其下属单位发改局里的一个科长能是什么级？充其量是股级！”
</p>
<p>
 “官小衙门大，实权部门！谁敢得罪呀！”
</p>
<p>
 “芝麻大的小官，嚣张跋扈惯了。”
</p>
<p>
 “鼻屎大的一个官，谁给了他好大官威？”
</p>
<p>
 “顶破天一个正科级干部，居然这么拽，莫非是当地的黑恶势力？！”
</p>
<p>
 其实，中共各级官员在民众面前作威作福已成普遍现象。
</p>
<p>
 今年5月18日晚，64岁的广西来宾市金秀县
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%AC%E5%AE%89%E5%B1%80%E5%8E%9F%E6%94%BF%E5%A7%94.html">
  公安局原政委
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E9%BB%84%E6%97%AD%E6%96%87.html">
  黄旭文
 </span>
 ，在柳州市五星商厦乐和城星巴克咖啡店，骚扰店里的两名女顾客，遭到其中一女子警告，若再不收手便用花露水喷他。恼羞成怒的黄旭文便将饮料砸向她们，又掀翻几张凳子和台子。
</p>
<p>
 该事件被媒体曝光后，退休副处级官员
 <span href="http://www.epochtimes.com/gb/tag/%E9%BB%84%E6%97%AD%E6%96%87.html">
  黄旭文
 </span>
 ，6月6日因“违纪问题”被调查。
</p>
<p>
 中共官员不仅在国内表现的蛮横，在国际场所也是一样。
</p>
<p>
 据报导，2018年9月3日，南太平洋岛国诺鲁举行年度外交峰会“太平洋岛国论坛”，非会员国的中共代表也与会。当美国内政部长首先发言后，中共官员竟然违反发言规则，挥舞桌牌、大声要求投票发言，接着代表团集体退席。
</p>
<p>
 中共官员大闹国际会场，震惊国际外交圈。人口仅有1.1万人的主办国诺鲁总统瓦卡指责，中共官员傲慢霸凌。#
</p>
<p>
 责任编辑：孙芸
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11376280.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11376280.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11376280.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/10/n11376280.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

