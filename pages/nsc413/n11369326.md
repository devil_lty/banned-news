### 探望残疾友人被控非法聚会 重庆十余人被抓
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/cdd9fb73c17fc6e5c106ab9e55e66575-600x400.jpg"/>
<div class="red16 caption">
 <p>
  重庆十余名维权人士探望残疾友人被控非法聚会遭警方抓走。（受访者提供）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月07日讯】（大纪元记者李熙采访报导）日前，
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%8D%E5%BA%86.html">
  重庆
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E7%BB%B4%E6%9D%83%E4%BA%BA%E5%A3%AB.html">
  维权人士
 </span>
 十余人前往探视残疾友人陈长海夫妻，没多久突然冲进一群黑衣人将他们押上警车，带往新山村街道办公室审问，指控他们“非法聚会”，涉嫌“寻衅滋事”和“扰乱公共秩序”。
</p>
<h4>
 探视残疾友人被控非法聚会
</h4>
<p>
 4日中午，
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%8D%E5%BA%86.html">
  重庆
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E7%BB%B4%E6%9D%83%E4%BA%BA%E5%A3%AB.html">
  维权人士
 </span>
 危文元、蔡邦英、郭兴梅、张翠英、周开惠、徐克勇等10余人，相约到大渡口区新山村探视重度残疾人陈长海夫妻。大约1点多钟，人还没到齐，有人敲门，陈长海去开门突然就冲进一群黑衣人要抓人，其中有二人穿警服（警号107293、107757），还有穿便衣的。
</p>
<p>
 维权人士危文元向大纪元记者表示，“70多岁的维权人士徐克勇让他们出示证件，他们很凶说走不走，不走就打人。我当时躺在他家床上休息，袜子还没来得及穿就被抓住后脑杓下楼，楼梯间和楼下还有很多（黑衣）人。”
</p>
<h4>
 绑架至街道办公室审问
</h4>
<p>
 据危文元介绍，这次重庆当局动用了大约有二三十人，二辆大型警车和三辆黑社会的车。“我们叫他们出示证件和传唤证，他们不出示，对我们极其凶恶，在车上，我要打110报警，他们就拿走我们手机不让报。”
</p>
<p>
 危文元表示，“在车上，徐克勇说我们要去派出所，他们不让去，把我们拉到大渡口新山村街道三楼会议室，强行检查每个人的身份证。”
</p>
<p>
 警察把他们关进多间办公室审问。审问的内容主要是：“你们为什么要聚会？有什么目的?？谁组织的？”审问过程中，警察并指控他们“非法聚会”，涉嫌“寻衅滋事”、“扰乱公共秩序”。
</p>
<p>
 徐克勇、周开慧当场反驳警方，说：“朋友患病前去探望就涉嫌‘寻衅滋事’、‘扰乱公共秩序’了？如果是这样，那么许许多多的市民婚丧嫁娶，聚会吃饭，岂不是都是非法聚会，都涉嫌‘寻衅滋事’、‘扰乱公共秩序’，都要被抓捕吗？”
</p>
<h4>
 关押审问数小时后释放
</h4>
<p>
 陈长海是一名重度残疾
 <span href="http://www.epochtimes.com/gb/tag/%E8%AE%BF%E6%B0%91.html">
  访民
 </span>
 ，因为房屋遭政府强拆而上访多年，上访期间，他和妻子胡文娟多次被非法截访、劳教、殴打、关黑监狱。近日，由于身体不适，重庆维权人士就相约前去探视，结果被大批警察闯入暴力抓走。
</p>
<p>
 审讯数小时后，陈长海由于身体残疾和身患疾病，先释放回家，其余十几名维权人士，经历长达几小吋的非法审问关押，最后警方通知各区派出所和政府来接人。当天晚上7点多危文元才被放出来。
</p>
<p>
 责任编辑：刘毅
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11369326.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11369326.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11369326.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/7/n11369326.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

