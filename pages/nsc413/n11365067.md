### 上海房企大佬猥亵女童案五大疑点
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/D-l_Fp-U8AAhMAF-1.jpg"/>
<div class="red16 caption">
 涉嫌猥亵女童的新城控股董事长王振华曾积极参与儿童慈善项目，引发担忧。（新城控股官网）
</div>
<hr/><p>
 【大纪元2019年07月05日讯】（大纪元记者凌云综合报导）9岁女童遭到房地产大佬
 <span href="http://www.epochtimes.com/gb/tag/%E7%8C%A5%E4%BA%B5.html">
  猥亵
 </span>
 案中，涉案女童下体撕裂，却说是“猥亵”而不是“强奸”；媒体报导案件后又集体删稿；嫌犯所做的最多的公益项目均与儿童有关，是否涉黑色产业链；7月1日警方接案抓人，3日才公布消息……
</p>
<p>
 千亿董事长
 <span href="http://www.epochtimes.com/gb/tag/%E7%8C%A5%E4%BA%B5.html">
  猥亵
 </span>
 案不断发酵，但仍有多个疑点待解。
</p>
<p>
 值得注意的是，嫌犯
 <span href="http://www.epochtimes.com/gb/tag/%E7%8E%8B%E6%8C%AF%E5%8D%8E.html">
  王振华
 </span>
 的来头非比寻常：上海市政协委员、江苏常州市人大代表，全国劳动模范、江苏省优秀民营企业家、常州市明星企业家、中华慈善突出贡献人物、全国工商联房地产商会常务副会长、上海市房地产商会会长、江苏省工商联副主席、常州市人大代表等等。
</p>
<h4>
 疑点一：删帖疑团
</h4>
<p>
 新城集团原董事长
 <span href="http://www.epochtimes.com/gb/tag/%E7%8E%8B%E6%8C%AF%E5%8D%8E.html">
  王振华
 </span>
 涉嫌猥亵女童并被刑拘的消息，最早由上海本地媒体于7月3日15时许报导，并被多家媒体转载，迅速引发热议。
</p>
<p>
 报导指，6月30日下午，姓周的女性同谋者以带朋友女儿到上海迪士尼游玩为由，带着两名分别为9岁和12岁的女童入住上海某五星级酒店，供疑犯
 <span href="http://www.epochtimes.com/gb/tag/%E6%80%A7%E4%BE%B5.html">
  性侵
 </span>
 。受害女孩事后以电话向母亲哭诉，其母立即赶赴上海报案。新城控股董事长王振华7月1日因猥亵幼女被上海普陀分局抓获。体检报告证实女孩有曾被性侵迹象。
</p>
<p>
 警方周三下午发布的消息称，目前王振华及周某两人都已被刑拘，案件正在调查中。
</p>
<p>
 但仅仅两个小时后，上海警方再度改口，称事件正在核查中。与此同时，一份由上海市委宣传部副新闻处长陆一波要求媒体删稿的指令截图，被一位离职的原官媒记者在网上公布，立即引爆舆论。
</p>
<figure class="wp-caption aligncenter" id="attachment_11365267" style="width: 440px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/3-6.jpg">
  <img alt="" class="size-full wp-image-11365267" src="http://i.epochtimes.com/assets/uploads/2019/07/3-6.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  知情人爆删贴内幕。（截图）
 </figcaption><br/>
</figure><br/>
<p>
 谁下令删帖？谁是嫌疑人的保护伞？
</p>
<h4>
 疑团二：为何延迟2天通报
</h4>
<p>
 新城老总王振华涉嫌猥亵幼女7月1日接受调查，直到3日警方才发通报，2天的时间有“知情方”精准出逃。
</p>
<p>
 据上海的警情通报，该案件事发是在6月30日22时许，7月1日下午嫌犯王某某至公安机关接受调查。而爆出该王某某是前新城控股董事长王振华的消息则是在7月3日下午A股收盘后发酵。
</p>
<p>
 第一财经报导，从近期新城控股的大宗交易来看，7月1日至3日期间共有5起大宗交易。
</p>
<p>
 7月1日，中国银河证券宁波柳汀街营业部以40.88元的成交价分别卖出19.48万和20.12万股。2日，中国银河证券宁波柳汀街营业部再以42.08元的价格分别卖出57.07万股和58.93万股，成交额都在2400万元以上。
</p>
<p>
 7月3日案件发酵当天，也有一笔大宗交易进行，海通证券宁波解放北路营业部以43.8元的价格，向国泰君安证券宁波彩虹北路营业部卖出32万股，成交额为1401.6万元。
</p>
<p>
 连续三天的5笔大宗交易，成交额累计达7900万元。近3天的5笔交易超过前几年一年的大宗交易数量。
</p>
<p>
 7月3日王振华涉
 <span href="http://www.epochtimes.com/gb/tag/%E6%80%A7%E4%BE%B5.html">
  性侵
 </span>
 被刑拘事件被媒体报导、警方发布公告前十多个小时，也有网友在新城控股股吧内提示：散户大家赶紧抛，不要问为什么，我只能这么好意提醒。
</p>
<figure class="wp-caption aligncenter" id="attachment_11365266" style="width: 410px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1-14.jpg">
  <img alt="" class="wp-image-11365266" src="http://i.epochtimes.com/assets/uploads/2019/07/1-14-600x441.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  网友在新城控股股吧内提示散户赶紧抛。（截图）
 </figcaption><br/>
</figure><br/>
<h4>
 疑点三：是强奸还是猥亵？
</h4>
<p>
 上海警方的通报称，新城控股董事长王振华涉嫌“猥亵”9岁女童属实。而据《新民晚报》的报导，女童已经验伤，阴道有撕裂伤，构成轻伤。
</p>
<p>
 官方说法引发炮轰。有网友说：别把法律当“卫生巾”，“强奸幼女的上市公司董事长，对9岁幼女造成阴道撕裂伤，只是构成轻伤，只是猥亵？你信不！ ”
</p>
<p>
 还有人怒斥，“孩子阴道已经撕裂了，还什么猥亵啊，就是强奸，这种人渣就该毙了！”“上海警察回复记者，中共党员强奸幼女不叫强奸，叫猥亵！幼女阴道撕裂伤可以构成轻伤！”
</p>
<p>
 根据中共的刑法，猥亵与强奸的量刑差距很大。强奸基本刑为三年以上十年以下有期徒刑，有加重情节的可以判处十年以上有期徒刑、无期徒刑甚至死刑。奸淫幼女的从重处罚。
</p>
<p>
 大陆律师表示，王振华如果涉嫌强奸罪，可以判处十年以上有期徒刑。
</p>
<p>
 而如果认定为猥亵，根据刑法第237条，则处五年以下有期徒刑或者拘役。显然如果认定为“猥亵”，对嫌犯的处罚会轻很多。
</p>
<h4>
 疑点四：有无性侵幼女前科？
</h4>
<p>
 事件曝光后，不少网友认为，王振华大概率有性侵幼女前科。
</p>
<p>
 大陆一家官媒评论文章认为，“作为一种变态的性心理，猥亵儿童不可能是临时起意。所以，此案中9岁的女童是不是唯一的受害者必须要查清楚。”
</p>
<p>
 还有大陆网友分析，“大佬的这种‘爱好’，通常不是心血来潮”，“无论是周某向受害女童撒谎，还是王某给周某钱，以及在酒店实施性侵，这个套路看起来驾轻就熟。”
</p>
<p>
 根据“女童保护基金”的报告， 2018年公开报导性侵儿童案有750名，熟人作案比例高达七成，超过六成性侵者是多次性侵。
</p>
<p>
 有知情人发帖称，以前王振华就曾重金包养初中女生，并透露王振华被抓后，已招供了多次侵犯幼女的事情。
</p>
<p>
 值得注意的是，在2016年1月份，上市（注：2015年12月，新城控股通过换股吸收合并江苏新城A股在上海证券交易所上市）的2个多月后，江苏常州武进区委副书记凌光耀被带走，而凌光耀和王振华都做过湖塘镇的副镇长。随后就爆出王振华被纪委带走的新闻。停牌一天后，新城控股发布公告，确认了王振华正在被纪委调查，但表示“王振华因为个人原因被调查”。
</p>
<figure class="wp-caption aligncenter" id="attachment_11365268" style="width: 500px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/2-6.jpg">
  <img alt="" class="size-full wp-image-11365268" src="http://i.epochtimes.com/assets/uploads/2019/07/2-6.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  （截图）
 </figcaption><br/>
</figure><br/>
<p>
 这次，新城控股的官方回应仍是“个人原因”，引发猜测？
</p>
<h4>
 疑点五： 慈善项目被质疑
</h4>
<p>
 事件曝光后，外界发现，王振华经常以做慈善的名义，在全国众多贫困地区的小学从事慈善活动，并积极打造亲子乐园。王振华所做的最多的公益项目均与儿童有关。
</p>
<p>
 从新城控股的官网可以看到，该公司用大通栏宣传了两个公益项目：“新城之夏”和“七色光计划”。
</p>
<p>
 新城控股2018年年报显示，公司专门成立“扶贫办公室”。董事长王振华先后多次带队赴云贵地区“调研扶贫”工作。王振华经常向外界宣传自己的“七色光计划”，这已成为新城控股的一张品牌名片。
</p>
<p>
 “犯下强奸幼女大罪的上市公司新城控股董事长的王振华在贫困山区做所谓的慈善。这让人不寒而栗。”
</p>
<p>
 “王振华创办了专门‘资助’偏远农村孩子的“七色光计划”，是否因为王本身具有恋童癖？他已接触了多少农村小女孩？又会有多少幼女被其猥亵？这一切令人细思极恐。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11365269" style="width: 416px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/4-3.jpg">
  <img alt="" class="wp-image-11365269" src="http://i.epochtimes.com/assets/uploads/2019/07/4-3-600x600.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  王振华创办最多的公益项目均与儿童有关，让外界担忧。（截图）
 </figcaption><br/>
</figure><br/>
<p>
 这让网友想到，不久前刚曝光的贵州孤儿院集体性侵丑闻。
</p>
<p>
 6月26日，微博网友爆料，疑似有幼儿园/孤儿院被重金贿赂，养成幼儿满足客户“兽欲”。爆料发布的图片包括多张聊天截图，谈论男子集体到专门收留留守儿童的福利院或幼儿园性侵儿童，涉及的地方包括贵州毕节、凯里。
</p>
<p>
 还有系列对话截图显示，有幼儿园工作人员和客户谈论给幼儿注射孕激素“黄体酮”，吃催情药，看黄片。消息迅速传播，网民斥责：“禽兽不如”、“令人发指”。官方迅速删除消息，并宣称信息系“编造”，但无法让人信服。
</p>
<p>
 王振华所谓的儿童慈善工程，同爆料的“福利院”集体性侵丑闻是否有瓜葛，引发外界怀疑。
</p>
<figure class="wp-caption aligncenter" id="attachment_11365186" style="width: 406px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/d3f23779b4c5a939a4c09bd4c1514cfc-1.jpg">
  <img alt="" class="wp-image-11365186" src="http://i.epochtimes.com/assets/uploads/2019/07/d3f23779b4c5a939a4c09bd4c1514cfc-1-600x306.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  （截图）
 </figcaption><br/>
</figure><br/>
<p>
 中共高官富豪专门奸淫未成年少女甚至女童，并不鲜见。早前，中共河南尉氏县人大代表赵志勇，因奸污25名在校未成年女学生，被执行死刑。赵志勇本身拥有上亿资产，曾多次高调现身“慈善活动”。
</p>
<p>
 此外，还有网民将此事联系到2017年爆发的北京“红蓝黄”事件。红黄蓝幼儿园孩童家长爆出，该幼儿园不仅给孩子打针、喂药，还将孩子脱光衣服罚站，看“叔叔医生脱光溜溜”猥亵小朋友的场面，引发大陆网民公愤。但媒体被噤声，家长被噤声。#
</p>
<p>
 责任编辑：孙芸
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365067.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365067.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11365067.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/4/n11365067.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

