### 中国教授盗取硅谷公司机密 与中兴有何关联
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2018/04/download-1-600x400.jpg"/>
<div class="red16 caption">
 <p>
  美国法庭文件显示，2015年在美国被起诉的天津大学教授张浩曾将从美国公司偷来的技术发给中兴通讯。(LLUIS GENE/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月10日讯】（大纪元记者张婷综合报导）天津大学教授张浩因盗窃美国技术于2015年在美被捕。相关法庭文件披露，张浩曾将盗窃来的美国无线技术发给中国电信公司
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B4.html">
  中兴
 </span>
 通讯。这种无线技术可用于过滤手机和军事通信中的电子信号。
</p>
<p>
 据“华盛顿自由灯塔”报导，一份相关文件称，张浩在2011年曾向
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B4.html">
  中兴
 </span>
 通讯一名代表发送了一封电子邮件，并附上了一个PowerPoint演示片。检察官称这些演示片的内容含有从美国“安华高科技公司”（Avago Technologies，2016年改名博通有限公司）所盗窃来的专有信息。
</p>
<p>
 安华的总部位于加州的圣荷西，其中一个工厂在科罗拉多州。起诉书开始介绍了此案中提到的表面声波（Surface Acoustic Wave，简称SAW）滤波器、体声波（Bulk Acoustic Wave，简称BAW）滤波器和薄膜体声波谐振器（Film Bulk Acoustic Resonator，简称FBAR），FBAR属于BAW滤波器的一种。
</p>
<h4>
 天津大学教授张浩盗窃案9月将审理
</h4>
<p>
 原订于6月19日在圣荷西联邦法庭进行的无陪审员审判，后来被推迟到9月10日。张浩仍没有认罪。
</p>
<p>
 2015年5月，张浩从中国入美时被警方逮捕。
</p>
<p>
 美方指控称，张浩和天津大学另一教授庞慰在南加州一所大学读博士期间，使用美国国防研究项目的经费研究薄膜体声波谐振器（FBAR）技术。毕业后，庞慰受聘于安华高科技公司在科罗拉多州的工厂，张浩则在马萨诸塞州的思佳讯公司（Skyworks Solution）工作，两人都担任FBAR工程师。
</p>
<p>
 FBAR技术非常重要，除了有利于消费者，也在军事和国防通信中被广泛应用。安华高公司是一家设计开发FBAR技术的全球供应商。曾在安华高工作的庞慰入选了第十批中共“千人计划 ”创业人才。
</p>
<p>
 张浩、庞慰和其他四名中国公民因涉嫌对安华高和思佳讯进行经济间谍和盗窃商业机密而在2015年被美国起诉。
</p>
<p>
 根据起诉书，天津大学官员在2008年曾前往加州圣荷西，跟庞慰、张浩和其他同谋者见面，不久，天大同意资助他们在中国建立FBAR生产基地，而庞、张二人继续就职于原公司，同时和天大密切合作。2009年，两人从美公司辞职，接受天大教授的职位。
</p>
<p>
 起诉书指称，庞、张和其他同谋窃取配方、源代码、技术规格、报告、设计图和其它被受害公司标有机密和专有字样的文件，将这些信息在彼此之间并与天大分享。
</p>
<p>
 庞、张等人在天津大学的指示下在开曼群岛创建了一家名为Novana的空壳公司。
</p>
<p>
 起诉书还披露，天大利用窃取的商业秘密，建造和装备了一个最先进的FBAR制造设施，并利用其拥有的MNMT（天津微纳米制造技术有限公司）与庞、张等人创办了诺思微系统有限公司（ROFS Microsystem），准备大批量生产FBAR，并从商业和军事实体那里获取了提供FBAR的合同。
</p>
<p>
 这些商业和军事实体包括中共国有企业和至少两家军事相关机构。
</p>
<p>
 该案件的起诉书概述了张浩等人窃取并对被盗的美国无线技术加以利用的精心策划。
</p>
<p>
 检察官称，Novana、MNMT和ROFS这三个实体被用来将盗窃来的技术汇集到中国。
</p>
<p>
 被盗的FBAR技术花了安华高公司约20年的时间和高达5000万美元的研发费用。
</p>
<p>
 在2000年代中期发生盗窃事件时，FBAR技术的市场价值估算超过10亿美元。它用于移动设备，如手机、平板电脑和GPS设备。该技术采用无线信号电子滤波器，允许发送和接收仅针对个体用户的特定通信，而不受电子干扰。
</p>
<p>
 检察官表示，FBAR既可用于消费者电话，也可用于各种军事和国防通信技术。
</p>
<p>
 据情报人士透露，天津大学被认为是中共军队在发展和获取外国商业和军事技术方面的一个前沿。
</p>
<p>
 起诉书中用“J.Y.”来代称一名和庞慰有联系的天津大学官员。该官员曾是中国科学院院士，也是张和庞担任教授的天津大学“精密仪器与光电子工程学院”的负责人。起诉书说，“J.Y.”“与中国（中共）政府有实质性的关联”，在中共政协及中国民主促进会等多个党政机构团体中担任主席或是成员。
</p>
<p>
 联邦调查人员所获得的，并在法庭文件中公布的张、庞等人通讯的信息揭示，张和庞的计划是将“安华高搬到中国去”，并在中国创建一个新公司。2007年5月25日，庞慰给张浩和另一名叫张会隋（HUISUI ZHANG，音译，此人也是被告之一）的人发了短信，让大家为在中国的公司想一个名字。
</p>
<p>
 “我想称这个公司‘Clifbaw’，你们想的名字是什么？”庞慰问道。
</p>
<p>
 张会隋随即让庞慰解释一下“Clifbaw”的背后意义是什么。庞回答说“China lift BAW technology，Clifbaw”（中国窃取BAW技术）。“哈哈”庞慰接着笑了。
</p>
<h4>
 张浩向中兴及中共军事相关机构兜售偷来的技术
</h4>
<p>
 从张浩的电话中所获得的张、庞的其它短信揭示，他们正在与中兴公司以及至少两家中共军事相关机构合作。
</p>
<p>
 法庭文件透露，在张浩发给中兴的PowerPoint演示片中，解释他在Novana和天津大学的工作，并强调Novana的BAW产品的重要性和性能。此外，演示片还用具体的安华高产品信息来说明Novana的产品，性能图表是参照思佳讯公司的产品性能。
</p>
<p>
 中兴通讯没有回应“华盛顿自由灯塔”的置评要求。
</p>
<p>
 中兴、华为目前是川普政府遏制中共政府渗透海外计算机和通信网络的目标。
</p>
<p>
 美国商务部长威尔伯·罗斯（Wilbur Ross）在6月份确认这些公司构成了国家安全威胁。“我们认为，这两家公司的行径都可能对国家安全造成伤害。”罗斯告诉CNBC。
</p>
<p>
 张浩的一则微信对话说：“13所（Institute 13）是我们军事产品的唯一客户。”
</p>
<p>
 Institute 13指的是中共央企“中国电子科技集团公司”（CETC）第十三研究所，专门从事电光研究。
</p>
<p>
 去年，美国商务部工业和安全局将中国电子科技集团公司第十三研究所及其下属的多个机构列入出口管制实体清单（也称黑名单），有效禁止名单上的实体购买美国商品。
</p>
<p>
 在另一则短信中，张浩询问有关5G振荡器的进展，并表示，（中国电子科技集团公司）第二十三研究所正在催着看到结果。
</p>
<p>
 CETC的第二十三研究所（又称上海传输线研究所）是中国最大的信息传输线技术研究所。该研究所还开展光纤和光缆研究。
</p>
<h4>
 美国
 <span href="http://www.epochtimes.com/gb/tag/%E7%9F%A5%E8%AF%86%E4%BA%A7%E6%9D%83.html">
  知识产权
 </span>
 和技术一直是中共的盗窃对象
</h4>
<p>
 上个月，美国国务院的一位高级官员讨论了有关中共利用大学进行技术和情报收集的问题。
</p>
<p>
 前国防情报局官员尼古拉斯·埃夫蒂米兹（Nicholas Eftimiades）说：“中国（中共）通过雇用大学、国有企业和商业实体窃取美国
 <span href="http://www.epochtimes.com/gb/tag/%E7%9F%A5%E8%AF%86%E4%BA%A7%E6%9D%83.html">
  知识产权
 </span>
 和技术的行径，来支持其‘中国制造2025’工业发展计划和军事现代化的企图。”
</p>
<p>
 美国律师梅丽娜‧哈格（Melina Haag）表示，该案件表明硅谷和整个加利福尼亚州的美国公司所开发的敏感技术，持续是外国政府通过各种手段盗窃的对象。
</p>
<p>
 张浩案子起诉时负责旧金山分部的联邦调查局特工大卫·约翰逊（David J. Johnson）表示，张案揭示外国利益集团有条不紊地、持续不断地利用在美国境内的个人获取敏感和有价值的美国技术。
</p>
<p>
 “复杂的外国政府资助计划，例如张的活动，对美国经济造成了不可逆转的破坏，并削弱了我们的国家安全。” 约翰逊说。#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11374684.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11374684.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11374684.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/9/n11374684.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

