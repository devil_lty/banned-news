### 特斯拉前华裔员工承认 上传源代码到个人账户
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/03/GettyImages-1126253927-600x400.jpg"/>
<div class="red16 caption">
 <p>
  电车制造商特斯拉（Tesla）的充电站已经开始运营。（Justin Sullivan/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月11日讯】
 <span href="http://www.epochtimes.com/gb/tag/%E7%89%B9%E6%96%AF%E6%8B%89.html">
  特斯拉
 </span>
 （Tesla）前员工
 <span href="http://www.epochtimes.com/gb/tag/%E6%9B%B9%E5%B9%BF%E5%BF%97.html">
  曹广志
 </span>
 （音译，Guangzhi Cao）在本周提交给法庭的一份文件中承认，他在2018年底还在为特斯拉工作时，将包含Autopilot（自动驾驶）源代码的压缩文件上传到了自己的iCloud个人账户。
</p>
<p>
 今年3月，
 <span href="http://www.epochtimes.com/gb/tag/%E7%89%B9%E6%96%AF%E6%8B%89.html">
  特斯拉
 </span>
 起诉
 <span href="http://www.epochtimes.com/gb/tag/%E6%9B%B9%E5%B9%BF%E5%BF%97.html">
  曹广志
 </span>
 涉嫌窃取特斯拉的自动驾驶源码，并将此技术转交给了中国的初创公司
 <span href="http://www.epochtimes.com/gb/tag/%E5%B0%8F%E9%B9%8F%E6%B1%BD%E8%BD%A6.html">
  小鹏汽车
 </span>
 。
</p>
<p>
 但曹广志在同一份文件中否认窃取特斯拉的敏感信息。据悉，曹广志现在是
 <span href="http://www.epochtimes.com/gb/tag/%E5%B0%8F%E9%B9%8F%E6%B1%BD%E8%BD%A6.html">
  小鹏汽车
 </span>
 的“感知主管”，正在“开发和交付用于生产汽车的自动驾驶技术”。
</p>
<p>
 据本周提交的一份联合文件，特斯拉还传召了苹果公司的文件。尽管苹果公司涉案，但一名苹果前员工张晓浪（Xiaolang Zhang）去年7月也被FBI指控窃取了苹果公司的商业机密。
</p>
<p>
 据报，张晓浪将敏感数据传至其妻子的笔记本电脑，并且被拍下从苹果公司带走了一箱设备。之后他离开了苹果公司到小鹏汽车任职。而据曹广志的LinkedIn个人资料，曹在加入特斯拉两年前，也曾是苹果的图像方面的高级研究员。
</p>
<p>
 今年3月21日，特斯拉将前华裔员工曹广志告上法庭，指控曹在今年1月离职时，将公司的自动驾驶源代码盗走，并交给了小鹏汽车。曹广志是特斯拉40名能够接触到自动驾驶源代码的员工之一。
</p>
<p>
 据悉，小鹏汽车出品的电动SUV汽车，同特斯拉显示在仪表盘的自动驾驶界面几乎一模一样。这种相似度，成为特斯拉状告其前自动驾驶部门的雇员曹广志的证据之一。
</p>
<p>
 根据Verge获取的法庭文件，特斯拉表示，从2018年开始，曹将一份完整的特斯拉自动驾驶相关源代码上传到他的iCloud账户。特斯拉称，他最终拷贝了超过30万份自动驾驶相关的文件、文件夹。当曹广志2018年底获得小鹏汽车的工作后，特斯拉发现，曹在离职那天，从工作电脑上删除了12万份文件，并关闭了与其个人iCloud账号的连接，接着“多次登录进入特斯拉的安全网络”，清除其浏览历史等记录。
</p>
<p>
 特斯拉认为，这些盗走的文件等于让小鹏汽车获得了对自动驾驶代码不受限制的访问。而小鹏汽车所采用的自动驾驶技术，确实与特斯拉的自动驾驶极为相似。
</p>
<p>
 特斯拉表示，公司的自动驾驶技术是数百人5年心血的结晶，耗资数千万美元，小鹏汽车无权拥有。
</p>
<p>
 特斯拉表示，在曹广志投奔小鹏汽车1年多之前，就有一位特斯拉自动驾驶机器学习方面的领军人物谷俊丽（Junli Gu），跳槽到小鹏汽车，并成为小鹏汽车自动驾驶部门的主管。
</p>
<p>
 在新的法庭文件中，曹广志承认“在2018年使用他的个人iCloud账户创建了某些特斯拉信息的备份副本”。他还承认，在2018年末创建了包含Autopilot源代码的zip文件，并证实小鹏汽车在12月12日向他发了录用通知。虽然曹广志没有具体说明他何时正式接受小鹏汽车的工作，但特斯拉说曹在特斯拉公司的最后一天是1月3日。
</p>
<p>
 曹广志还承认他离开特斯拉前，删除了“存储在他的特斯拉电脑上的某些文件并清除浏览器的浏览记录”，但否认这些活动构成任何形式的不当行为。尽管他宣称自己离开前为删除个人iCloud上的文件做了大量工作，但却没有说是否删除了所有文件。
</p>
<p>
 曹的律师则争辩说，曹离开特斯拉后留在他的设备上的任何源代码或其它机密信息只是“由于疏忽造成。”
</p>
<p>
 这不是小鹏汽车第一次卷入盗窃纠纷。去年7月，前苹果公司员工张晓浪被加州圣荷塞市的执法人员逮捕。
</p>
<p>
 据报导，张晓浪当时借口回北京陪产假，却在这段假期被监控摄像头拍到他进入苹果公司自动驾驶汽车实验室，将设计蓝图和其它资料下载到自己的个人电脑上。而后，他前往中国，并获得小鹏公司的聘用。
</p>
<p>
 今年1月，苹果公司工程师——陈继忠（Jizhong Chen），在计划飞往中国的前一天被抓捕。他电脑中被搜出数千个苹果公司商业机密文件。据报陈试图将这些文件提供给中国电动车公司小鹏汽车。#
</p>
<p>
 责任编辑：林诗远
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11378771.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11378771.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11378771.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/11/n11378771.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

