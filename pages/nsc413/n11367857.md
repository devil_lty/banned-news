### 【翻墙必看】北京难解香港危机
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<p>
 【大纪元2019年07月06日讯】大纪元每天为读者梳理
 <span href="http://www.epochtimes.com/gb/tag/%E7%BF%BB%E5%A2%99%E5%BF%85%E7%9C%8B.html">
  翻墙必看
 </span>
 的文章：
</p>
<p>
 1 .
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367491.htm">
  对形势错判 传韩正返京再议“香港政策”
 </span>
 <br/>
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 近期连续发生大规模民众游行示威反“
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 ”，但特首林郑一直未回应民众诉求，引爆香港危机。日前传到深圳训斥港府处置不当的中共常委韩正已回北京，认为对
 <span href="http://www.epochtimes.com/gb/tag/%E5%BD%A2%E5%8A%BF%E9%94%99%E5%88%A4.html">
  形势错判
 </span>
 ，要求调研以调整“香港政策”。
</p>
<p>
 2.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367174.htm">
  三峡大坝出现变形？水利专家揭示秘密
 </span>
 <br/>
 日前有网民在推特上发出三峡大坝变形的照片，不少网民担心，一旦溃坝，大片地区将生灵涂炭。大纪元采访到著名三峡大坝问题专家、旅居德国的王维洛博士，他透露了一个鲜为外界所知的秘密。
</p>
<p>
 3.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367558.htm">
  从香港到武汉 令中共头痛事情一件接一件
 </span>
 <br/>
 在武汉市新洲区阳逻街令人窒息的夏夜，空气中到处弥漫着垃圾腐臭的气味，当地居民不得不关闭窗户，时常抱怨他们无法入睡。当近期政府计划建垃圾焚烧发电厂后，他们的愤怒终于爆发了。武汉抗议是
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 反“送中”抗议之后，很令中共头痛的最新事件。
</p>
<p>
 4.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11365568.htm">
  港人怒火越过深圳河 大陆网民群起要真相
 </span>
 <br/>
 7月1日香港立法会遭到冲击后，中共高调“谴责”。当局此举引发大批大陆网民的质疑，网民要求官方公布香港抗议修例事件的真相。
</p>
<p>
 5.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367492.htm">
  【新闻看点】40年代颂美打蒋 毛文章被讥讽
 </span>
 <br/>
 美国的独立日期间，毛歌颂美国民主的文章广为流传，这是非常讽刺的一件事，也让网友更加看清了毛泽东的流氓欺骗。
</p>
<p>
 6.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367735.htm">
  送中酿4命案 8千港妈吁港府回应年轻人诉求
 </span>
 <br/>
 近日，因反对港府修订“
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 ”（也称，送中条例）已有4名港人死亡。对此，8,000名香港妈妈站出来，呼吁政府放下傲慢、尊重生命，尽快回应年轻人诉求。
</p>
<p>
 7.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11366873.htm">
  新疆大量幼童失踪 英媒揭儿童再教育营
 </span>
 <br/>
 一项新调查发现，中共故意将新疆维吾尔族儿童与他们的家庭分开，仅一个乡镇就有逾400名孩童的父母“下落不明”，很可能是被关进所谓的“再教育营”里。有外媒在土耳其伊斯坦布尔进行采访，证实中共一方面将成年人关押在“集中营”，另一方面又将孩童送往“儿童再教育营地”。
</p>
<p>
 8.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367010.htm">
  【新闻看点】山雨欲来风满楼 香港考验北京
 </span>
 <br/>
 香港在国际上的地位，对中共来说也是至关重要的，中共会抛弃这颗“金蛋”吗？不知是否出于这种考量，苹果日报在2日引述消息说，韩正专程到深圳向林郑转达了习近平的最高指示：香港处置反送中示威绝对不允许发生流血，而且习近平等北京领导人也在中南海“严阵以待”。
</p>
<p>
 9.
 <span href="http://www.epochtimes.com/gb/19/6/26/n11347388.htm">
  夺取台湾关键技术 中共七大手法渗透科技业
 </span>
 <br/>
 高科技，已成为中共对台渗透的核心目标之一。近年来，中共或中资企业屡屡通过窃密、挖角、强迫技术转让等手法，从台资（在台或大陆）企业或外商在台子公司窃取关键技术与机密，此类案件逐年增加。如何防范中共的技科渗透，保住台湾科技岛优势，已成为台湾政府的重要课题。
</p>
<p>
 10.
 <span href="http://www.epochtimes.com/gb/19/7/4/n11364733.htm">
  遭迫害致死法轮功学员的家属参与诉江案例
 </span>
 <br/>
 据统计，已有超过20万的法轮功学员、家属，向最高法院和最高检察院邮寄诉状，控告元凶江泽民等对法轮功的残酷迫害。大陆明白真相的民众也纷纷声援法轮功学员反迫害，要求惩治迫害元凶江泽民。
</p>
<p>
 11.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367720.htm">
  美中安排会晤 白宫顾问重申贸易谈判底线
 </span>
 <br/>
 白宫官员周五（7月5日）表示，美、中官员近两周互通电话、安排接下来的高层贸易谈判，不确定下周是否有面对面的会谈。
</p>
<p>
 12.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367047.htm">
  新疆7.5十周年 管控升级手机强装监控软件
 </span>
 <br/>
 新疆7.5事件十周年之际，中共政权面临前所未有的国际国内的压力和危机感，对新疆监控到了疯狂的地步。除高度军事戒备、安装人脸识别等大数据装置外，甚至勒令当地人手机上安装监控软件。
</p>
<p>
 13.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367670.htm">
  黑龙江鹤岗 五位法轮功学员遭非法起诉
 </span>
 <br/>
 黑龙江省鹤岗市五位法轮功学员由金英、李春辉、冯俊清、彭岩峰、庞中艳，遭遇鹤岗市兴安区检察院跨区非法起诉。
</p>
<p>
 14.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11367223.htm">
  四川眉山大规模爆猪瘟 官员瞒疫情推责任
 </span>
 <br/>
 因遭严重非洲猪瘟疫情冲击，四川眉山养猪户近日在网上求助，并批评政府对疫情无动于衷。该起疫情至今也没有被通报，且有当地官场人士揭示已无力防控。讽刺的是，就在7月1日，中共声称25省的非洲猪瘟疫区全部解除封锁。
</p>
<p>
 15.
 <span href="http://www.epochtimes.com/gb/19/7/5/n11365941.htm">
  【专访】澳参议员：制裁中共 不做迫害同谋
 </span>
 <br/>
 7月3日，澳洲绿党参议员莱斯（Janet Rice）就日前“独立人民法庭”针对中共持续强制活摘良心犯器官的裁判结果表示，澳洲不应对迫害保持沉默，否则就是中共迫害人权的同谋。澳洲应该考虑和讨论《马格尼茨基法案》中的制裁措施。
</p>
<p>
 责任编辑：许梦儿
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11367857.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11367857.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11367857.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/6/n11367857.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

