### 709四周年 谢燕益：中国正在发生巨变
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="大陆人权律师谢燕益。（大纪元）" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/05/XIEYANYI-600x400.jpg"/>
<div class="red16 caption">
 <p>
  大陆人权律师谢燕益。（大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月08日讯】（大纪元记者洪宁、张顿采访报导）7月9日，是“709”大抓捕事件四周年。四年来，被残酷打压的维权律师及家属、维权公民仍坚守着内心的正义，勇敢地向中共极权政专治说“不”。事件当事人之一的
 <span href="http://www.epochtimes.com/gb/tag/%E8%B0%A2%E7%87%95%E7%9B%8A.html">
  谢燕益
 </span>
 律师接受大纪元采访时表示，他在经历了坐牢、酷刑等迫害后，如今，对“709”有了新的认识：这是一场信仰之战。而
 <span href="http://www.epochtimes.com/gb/tag/%E6%B3%95%E8%BD%AE%E5%8A%9F.html">
  法轮功
 </span>
 学员、709律师、港人
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 游行等非暴力的运动将促使中国发生巨变。
</p>
<p>
 2015年7月9日，中共在二十多个省市，大肆抓捕维权律师、家属、维权公民，人数高达320多人。其中的许多律师曾为大量
 <span href="http://www.epochtimes.com/gb/tag/%E6%B3%95%E8%BD%AE%E5%8A%9F.html">
  法轮功
 </span>
 案做无罪辩护，代理农民土地案、基督徒案、公民维权案等，因而遭受当局酷刑、判监、家属连坐以及被吊销律师执照等迫害，他们当中有许多人是有信仰的。
</p>
<h4>
 <strong>
  信仰之战
 </strong>
</h4>
<p>
 “709”事件四周年来临之际，
 <span href="http://www.epochtimes.com/gb/tag/%E8%B0%A2%E7%87%95%E7%9B%8A.html">
  谢燕益
 </span>
 说“709”事件是“正义与邪恶，光明与黑暗，和平民主与暴力专政之战。这个极权的统治靠的是谎言加暴力，而我们这些人是走向人性的觉醒，神性的复归，必然就会产生一场信仰之战。”
</p>
<p>
 谢燕益说：“中共给你扣帽子，进行妖魔化，包括在肉体上、名誉上、经济上等等各个方面去威胁你，不择手段，总之就是为了达到一个目的，造成人们的恐惧，摧毁你的意志。”
</p>
<p>
 谢燕益说，作为一个没有信仰的人，很容易被摧毁驯服的，对于有信仰的人，信仰决定自由。我们相信在这个世界存在一个更高的主宰力量，摆脱无神论的谎言，而中共则想通过无神论构建一个罗网，在它们的意识形态、编织的谎言里来控制主宰你，一切向钱看，崇拜权利金钱，纵情色欲。
</p>
<p>
 而在被打压的“709”群体中，很多人都有信仰，是信神的。“我们知道了真相，相信神的存在，就能告别恐惧与黑暗。”他说。
</p>
<p>
 谢燕益认为，709事件加速了整个社会的觉醒，它推动了海内外更多有良知的同胞、同仁甚至整个国际社会，对中共治下的人权灾难、极权专制威胁的反思和认识。
</p>
<p>
 四年来，这个群体仍然坚持抗争，大家在觉醒的过程中，寻找到了正的信念，都感觉到了这种善的力量是不可阻挡的，“德不孤，必有邻。”这就是为什么今年又有些同仁和一些普通的公民参与到反迫害当中来，更多的人觉醒了，更加坚定了这种正义战胜邪恶的信念。
</p>
<h4>
 <strong>
  中国正在发生巨变
 </strong>
</h4>
<p>
 谈到中共目前的处境，谢燕益表示，国际上发生的美国对中共的制裁、香港
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 运动、国际大的环境的改变以及法轮功团体讲真相、“709”事件，还有之前所发生的中国民间维权运动等，都将促使
 <strong>
  中国发生巨变。
 </strong>
</p>
<p>
 谢燕益表示，尽管有人是要付出代价的，但这也都是“为中华民族乃至整个世界走向和平民主、法制的社会，为每个个体生命的尊言，人类社会的尊言和自由，为告别这个极权的统治，走向光明的前景，在不断地累积力量。”
</p>
<p>
 中共1999年开始镇压修炼“真、善、忍”的法轮功学员，一直到现在还没有停止。但中共的非法关押、酷刑折磨、判刑甚至活摘法轮功学员的器官，都没能阻止他们反迫害。他们在国内外日复一日地向中国人讲着法轮功是什么、中共是什么、中共为什么要迫害法轮功等真相，劝人们退出中共“党、团、队”组织。截至目前，已有逾3.36亿人退出中共的“党、团、队”组织。
</p>
<p>
 同时，中共也没有打垮国内的维权律师。虽然中共一直打压维权律师，从零星地非法抓捕、判刑，发展到2015年7月9日大批地抓捕维权律师（也称“709”律师），并对他们进行判刑，但中共也仍无法阻止维权律师及其家人维护自己和他人的权利。
</p>
<p>
 而且，中共这个独裁专制政权制造的各种食品、医药、环境等灾难；强拆民宅、强夺土地；搞各种运动，如现在的改地名运动，北京此前拆除违章建筑、驱逐低端人口事件，多个省份强行煤该气，江西等省的抢棺材、挖祖坟运动；以及镇压六四民运、镇压少数民族、维权人士等；中共在不断地制造着各种新的维权人士。
</p>
<p>
 目前，香港如火如荼的反送中运动，也震惊了世界。
</p>
<p>
 谢燕益表示，法轮功学员、709律师、港人等非暴力的运动，都在加速整个社会的觉醒，包括海内外更多的有良知的同胞、同仁甚至整个国际社会的觉醒，推动了大家对中国人权灾难，中共极权专制威胁的反思和认识。
</p>
<p>
 谢律师说，中共这个极权政权就是靠谎言加暴力进行统治，它给你扣帽子、进行妖魔化，并且从肉体上、名誉上、经济上、家庭上去威胁你，给你心里造成恐惧，摧毁你的意志。总之，中共为了维护其政权，不择手段。
</p>
<p>
 但随着越来越多国人的觉醒，越来越多的人参与到反迫害当中来，“尽管说（这些反迫害）也是有代价的，（有）人道灾难，但这些纷争和事件的发生”，会使这个社会有更大的机会完成和平民主的转型。
</p>
<p>
 他认为，“中国社会的转型要从信仰、制度、文化三个方面构建，这三个方面的根本是信仰，信仰决定自由，信仰奠定文明。”
</p>
<p>
 谢律师表示，虽然中国走上和平民主的道路需要一个过程，但他个人对此表示“乐观”。#
</p>
<p>
 责任编辑：李沐恩
</p>
<p>
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370790.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370790.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370790.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/8/n11370790.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

