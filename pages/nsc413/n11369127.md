### 【翻墙必看】美中重启谈判 贸易战大事梳理
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/05/cda435a435e0589c7275e9604e096c0b-600x400.jpg"/>
<div class="red16 caption">
 <p>
  图为示意照。 (KAZUHIRO NOGI / AFP)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月07日讯】大纪元每天为读者梳理翻墙必看的文章：
</p>
<p>
 1.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/3/n11362505.htm" rel="noopener noreferrer" target="_blank">
   美中谈判六大基调已定 贸易战大事记一览
  </span>
 </b>
 <br/>
 美中两国元首6月29日举行双边会晤，同意两国谈判重回正轨，贸易战暂时停火。川普（特朗普）总统7月1日证实，双方谈判基本上又开始了。他说，美中代表正在通过电话会谈，但也会会面。
</p>
<p>
 2.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368860.htm" rel="noopener noreferrer" target="_blank">
   美媒揭叶剑英与习近平家族间的亲密关系
  </span>
 </b>
 <br/>
 在中共多名去世太子党中，叶选宁的葬礼最为“高规格”，显示出中共红色家庭间的亲疏远近。美媒刊文称，这凸显叶剑英家族与习仲勋一家关系密切。
</p>
<p>
 3.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368835.htm" rel="noopener noreferrer" target="_blank">
   川普施压中共结构性改革 获国内外广泛支持
  </span>
 </b>
 <br/>
 在美中恢复贸易谈判之际，中共商务部扬言，如果美国不停止现有的惩罚性关税，中共不会跟美国达成贸易战停火协议。但是美国总统川普的关税行动是为了迫使中共进行结构性改革，得到了美国各界的广泛支持。
</p>
<p>
 4.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368608.htm" rel="noopener noreferrer" target="_blank">
   新疆7‧5事件 目击者亲历中共警察枪杀维人
  </span>
 </b>
 <br/>
 新疆“七五”事件十周年之际，中共新疆各地军警全面提升戒备。曾亲历七五事件的两名中国人，回忆了那起十年前的往事：事件爆发当天并没有看到死人，但中共军警当天已开枪镇压；其中一名目击者7日亲眼看到中共警察在医院开枪杀维人。
</p>
<p>
 5.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368951.htm" rel="noopener noreferrer" target="_blank">
   彭定康：英方有权跟中方讨论香港事务
  </span>
 </b>
 <br/>
 香港人反对《引渡条例》（又称《逃犯条例》、《送中条例》）事件，引起国际关注。英国最后一任港督彭定康（Chris Patten）表示，英方有权跟中方讨论香港事务。英国政府应提出诉求，撤回《引渡条例》。
</p>
<p>
 6.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368385.htm" rel="noopener noreferrer" target="_blank">
   中共多次声称美国应取消关税 专家分析原因
  </span>
 </b>
 <br/>
 中共当局日前再次声称，若美中达成贸易协议，美国应该全部取消对中国商品加征的关税，这已经不是中共第一次想让美国取消关税，缘何中共急着让美国取消关税，美国专家分析了其中的缘由。
</p>
<p>
 7.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368256.htm" rel="noopener noreferrer" target="_blank">
   粤上市电子公司突倒闭 大批供应商员工维权
  </span>
 </b>
 <br/>
 位于广东深圳市宝安区福永街道的新三板上市公司深圳聚电智能科技股份有限公司突然倒闭，大批供应商与员工围堵公司讨债、讨薪。
</p>
<p>
 8.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/4/n11365012.htm" rel="noopener noreferrer" target="_blank">
   一个被监控15年的中国家庭 （1）
  </span>
 </b>
 <br/>
 依法去起诉一个违法之人，从此被这个国家监控。妻子给他看了一张照片，没想到所引发的事件，成为“709”大抓捕的导火索。他因此被监禁酷刑，而他的妻子和家庭，长期处于严密监控之下。
</p>
<p>
 9.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368802.htm" rel="noopener noreferrer" target="_blank">
   中共威逼利诱禁阻无效 武汉民众坚持抗议
  </span>
 </b>
 <br/>
 湖北武汉市新洲区阳逻民众抗议修建陈家冲垃圾焚烧发电项目已有10天，大批民众仍然坚持每天晚上出去“散步”抗议，当地政府则通过威逼利诱、宵禁等手段持续打压，完全无视民众的诉求。
</p>
<p>
 10.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11369094.htm" rel="noopener noreferrer" target="_blank">
   李尚平被枪杀案17年未破 背后有阻力？
  </span>
 </b>
 <br/>
 2002年4月26日，湖南益阳赫山区教师李尚平被枪杀，但17年过去，凶手仍未被抓获。该案先是被警方认为交通命案，而后定为抢劫命案，家属和坊间认为李尚平的死与举报腐败问题相关。
</p>
<p>
 11.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/7/n11369249.htm" rel="noopener noreferrer" target="_blank">
   陈日君急见教宗 警告新文告恐灭中国天主教
  </span>
 </b>
 <br/>
 罗马教廷最新公布的中国神职人员牧灵指导文件，遭到香港退休枢机主教陈日君的质疑。他表示担忧实施后恐消灭中国真正的天主教会。日前，他已火速飞往梵蒂冈面见教宗提出警告。
</p>
<p>
 12.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368998.htm" rel="noopener noreferrer" target="_blank">
   杨宁：俄核动力潜艇失事背后的海底战
  </span>
 </b>
 <br/>
 俄罗斯国防部7月2日证实，一艘隶属于俄罗斯海军北方舰队的“科研潜水器”在北莫尔斯克周边水域水下失火，造成至少14名军人因“吸入致命毒气”死亡，其中有7名上校，2人为“俄罗斯英雄”。“这对我国的海军，是极大的损失”，俄总统普京随即如此表示。
</p>
<p>
 13.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368689.htm" rel="noopener noreferrer" target="_blank">
   新证据：华为员工简历披露和中共关系密切
  </span>
 </b>
 <br/>
 在西方国家对华为的质疑声中，一个中心议题就是华为与中共军方和情报机构的关系。华为一直极力否认。但英国外交政策智库HJS的研究显示，在对华为员工的简历进行分析后发现，华为与这些中共机构的关系要比之前认为的要密切得多，甚至有华为员工曾当过中共国安部特工。
</p>
<p>
 14.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368939.htm" rel="noopener noreferrer" target="_blank">
   川普总统连任前景乐观 经济是主要因素
  </span>
 </b>
 <br/>
 美国《国会山报》（The Hill）周六（7月6日）报导说，总统川普（特朗普）有理由对自己以及2020年的连任前景保持乐观。
</p>
<p>
 15.
 <b>
  <span href="http://www.epochtimes.com/gb/19/7/6/n11368925.htm" rel="noopener noreferrer" target="_blank">
   中共刚说解除疫区“封锁” 广西又现猪疫
  </span>
 </b>
 <br/>
 中共农业农村部副部长于康震刚称，中国25个省区的非洲猪瘟疫区已“全部解除封锁”。但话音才落，隔天广西壮族自治区又现非洲猪瘟疫情。
</p>
<p>
 责任编辑：方明
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11369127.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11369127.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11369127.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/7/n11369127.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

