### 赴联合国演讲反送中 何韵诗行前寄语催人泪下
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="何韵诗受邀到联合国演讲反送中" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/1906300234222384-600x400.jpg"/>
<div class="red16 caption">
 <p>
  港星何韵诗资料照。（陈柏州／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月08日讯】（大纪元记者佟亦加综合报导）多次参加“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”游行的香港歌手
 <span href="http://www.epochtimes.com/gb/tag/%E4%BD%95%E9%9F%B5%E8%AF%97.html">
  何韵诗
 </span>
 ，受邀于7月8日在瑞士日内瓦联合国人权理事会（UNHRC）发表演说，让国际社会了解香港目前的局势。
</p>
<p>
 5日，她在脸书分享出发的照片时表示，虽然演说只有短短的两分钟，但对港人是“何其重要”。出发前每天与市民并肩作战的她说，“不放心离开香港。”“大家答应我，一定要照顾好自己，照顾好身边人，我好快返。”一番话催人泪下。
</p>
<p>
 联合国观察（UN Watch）4日发布的新闻稿透露，身兼歌手和社运人士的
 <span href="http://www.epochtimes.com/gb/tag/%E4%BD%95%E9%9F%B5%E8%AF%97.html">
  何韵诗
 </span>
 ，将前往位于瑞士日内瓦的联合国人权理事会（United Nations Human Rights Council，简称UNHRC），就港府强推将中共视为的“逃犯”引渡到大陆进行审判的《逃犯条例》（《送中条例》）以及由此引发的百万港人大游行发表演说。
</p>
<p>
 6月9日，香港103万港人走上街头，抗议港府强推《送中条例》二读。6月12日，港警用催泪弹、布袋弹、橡胶子弹、胡椒喷雾、警棍等镇压逾万人的集会，港人提出了五大诉求：一、撤回送中条例；二、取消暴动定性；三、追究开枪责任；四、不检控示威者；五、林郑月娥下台。
</p>
<p>
 但截至目前，港府仍然仅仅说暂缓修例，并没有回应港人的诉求，致使6月16日、7月1日、7月7日又分别爆发了在中环的200万、55万以及在九龙的23万人大游行，还发生了港人冲入立法会遭警方暴力驱赶的事件，至少有近百市民在冲突中受伤和被捕，更有四位年轻人对港府拒绝撤回恶法感到失望而轻生。
</p>
<p>
 多次参与“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”集会和游行的何韵诗表示，港人争取民主的行动已经提升到一个新的层次，而现在本港的形势严峻，如警方使用过度武力、港府对民众的诉求采取拖延战术。她希望借此演说，让全世界关注，中共对香港“一国两制”的破坏到了何种程度，“反送中”不仅有关香港民众的利益，也与全球的情势密切相连。
</p>
<p>
 何韵诗5日（周五）在脸书贴出人在机场的照片，并写道：“今晚就飞往瑞士日内瓦，星期一将于联合国人权理事会前讲话。我其实最怕演讲，但非常时期，每个人都要跳出自己的舒适圈（comfort zone）”，表示“有点紧张”的她说：“虽然只有短短两分钟的时间，但对我们香港人何其重要！”她在回应网友关于准备演讲稿的话题时说，自己会尽量做好。
</p>
<p>
 出发前每天都与市民并肩作战的何韵诗在留言中表示：“好不放心，不想离开香港。”她叮嘱创造香港历史的示威民众：“大家答应我，一定要照顾好自己，照顾好身边人，我好快返。”
</p>
<p>
 看到何韵诗在脸书的帖文，香港网友纷纷留言：“刚在（8,000）爸妈集会上见到你发言，原来你又赶着上飞机，辛苦你了！” “把你心里话说出，已经足够感动世界！”“香港人与你同在！”“加油，香港需要你！”“感谢你的付出和贡献！”
</p>
<p>
 还有台湾网友表示：“谢谢你守护香港的民主，谢谢你帮助台湾的未来！”
</p>
<p>
 42岁的何韵诗因2014年参与香港雨伞运动，遭中共全面封杀。虽然接的工作少了，但她透露，有眼光的商家敢请她，又多了去美国登台的机会，叫大家不用为她担心，因为“价值观比金钱更重要”。
</p>
<p>
 自今年香港爆发“反送中”示威游行以来，在许多大大小小的和平抗议场合，都可见到何韵诗的身影。她甚至有次站在最前线，用肉身挡住与民众对峙的警察。网友称赞她：“香港女儿 智、仁、勇！”
</p>
<p>
 责任编辑：杨明
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370428.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370428.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370428.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/7/n11370428.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

