### 再参加九龙反送中大游行 叶德娴：香港撑住
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="叶德娴一次不落地参加反送中游行" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/PO_X7794_meitu_1-600x400.jpg"/>
<div class="red16 caption">
 <p>
  今年71岁的香港资深艺人叶德娴在台湾出席电影记者会资料照。（陈柏州／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月08日讯】（大纪元记者佟亦加综合报导）香港民间发起的“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”运动接连不断。今年71岁的香港影后
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%B6%E5%BE%B7%E5%A8%B4.html">
  叶德娴
 </span>
 （Deanie姐）除了6月9日、6月16日、7月1日与市民一起走上街头，7月7日再度身穿黑衣、手执“香港撑住”和“齐上齐落不撤不散”的标语，在九龙参加23万人反恶法的示威游行。香港商台节目主持人
 <span href="http://www.epochtimes.com/gb/tag/%E6%BD%98%E5%B0%8F%E6%B6%9B.html">
  潘小涛
 </span>
 当晚在脸书贴出叶德娴的照片，并直呼：“感动到想喊！向Deanie姐致敬！”
</p>
<p>
 港府强推《逃犯条例》（又称《送中条例》或《引渡条例》）引发民众忧心和愤怒，近期香港接连举行声势浩大的百万人大游行，还因万人包围港府、冲击立法会反对恶法而遭警方武力镇压。
</p>
<p>
 由于特首林郑拒不回应民众的五大诉求，中共官媒一夜之间突然开足马力歪曲报导港人反恶法，香港民间又发起在九龙区举行游行，向大陆游客传递港人和平理性反恶法的讯息。
</p>
<p>
 7月7日的
 <span href="http://www.epochtimes.com/gb/tag/%E4%B9%9D%E9%BE%99%E5%8C%BA%E5%A4%A7%E6%B8%B8%E8%A1%8C.html">
  九龙区大游行
 </span>
 ，由尖沙咀梳士巴利花园，行至高铁西九龙站。大会当晚宣布有23万人参与。
</p>
<p>
 在茫茫人海中，有市民见到71岁的资深艺人
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%B6%E5%BE%B7%E5%A8%B4.html">
  叶德娴
 </span>
 身着黑衣，与友人一起，手持“香港撑住”、“齐上齐落 不撤不散”的标语，再次现身游行队伍。
</p>
<p>
 <center>
 </center>
</p>
<p>
 当晚，香港商台《在晴朗的一天出发》以及《串》的节目主持人
 <span href="http://www.epochtimes.com/gb/tag/%E6%BD%98%E5%B0%8F%E6%B6%9B.html">
  潘小涛
 </span>
 ，在其个人脸书贴出叶德娴手执“香港撑住”的海报，并激动直呼：“感动到想喊！向Deanie姐致敬！”他还表示，“只是担心Deanie姐的体力能不能应付多场活动。加油，感谢！如何可以不爱她！”
</p>
<p>
 潘小涛在脸书贴文一小时，已有逾1.5万个按赞和近500条留言。
</p>
<p>
 不少网友留言为叶德娴打气：“
 <span dir="ltr">
  <span class="_3l3x">
   Deanie姐，香港人爱你！
  </span>
 </span>
 ”“以你为荣，真爱香港人！”“多谢你用自己的知名度帮香港人！”“感谢 Deanie 姐和香港人走在一起！”
</p>
<p>
 也有不少网友担心叶德娴的身体状况。因为年过71岁的Deanie姐，接连参加了6月9日、6月16日、7月1日的“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”示威游行，也在6月12日与万名港人一起到金钟立法会和政府总部请愿，亲眼目睹了警方释放催泪弹和胡椒喷雾、使用布袋弹和橡胶子弹等镇压手无寸铁的民众。
</p>
<p>
 入行40年的叶德娴，一直无惧权势，每遇她觉得不公正的事情，她都勇于表态。
</p>
<p>
 她在参加200万民众大游行接受访问时曾表示，自己走出来是“理所当然，天经地义”，批评港府只“暂缓”不“撤回”恶法是“当我们人民是蠢的”。还表示，政府选择不听人民的意见，但市民可以继续表达诉求。她还叮嘱，参加示威的任何人都不要受伤和被捕。
</p>
<p>
 一向关心社会和年轻人的叶德娴，多次参加反恶法大游行，赢得民众的普遍支持和尊敬。然而，7月2日传出消息，她在7月1日参加55万人的大游行之后，其歌曲在大陆音乐网站“网易云音乐”、“QQ音乐”、“酷狗音乐”均被下架，甚至连“叶德娴”的名字也无法搜寻。
</p>
<p>
 7月3日，潘小涛在脸书怒斥中共封杀艺人的行为：“整天用这一招，真讨厌！”表示马上要在主持的节目中播“Deanie姐”的歌，用行动力挺叶德娴。
</p>
<p>
 面对中共的封杀，叶德娴本人也并没有退缩。7月6日晚，她在中环爱丁堡广场参加集会，悼念6月30日为反恶法殉命的29岁女子邬幸恩。7月7日，她再次参加九龙区23万人的大游行，坚持不懈地表达“反恶法”的心声。
</p>
<p>
 责任编辑：杨明
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370167.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370167.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11370167.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/7/n11370167.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

