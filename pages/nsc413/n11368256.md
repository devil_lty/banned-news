### 粤上市电子公司突倒闭 大批供应商员工维权
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/3-12-600x400.jpg"/>
<div class="red16 caption">
 <p>
  新三板上市公司深圳聚电智能科技股份有限公司突然倒闭，大批供应商与员工维权。（受访者提供）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月06日讯】（大纪元记者顾晓华采访报导）位于广东深圳市宝安区福永街道的新三板上市
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%AC%E5%8F%B8.html">
  公司
 </span>
 深圳聚电智能科技股份有限公司突然
 <span href="http://www.epochtimes.com/gb/tag/%E5%80%92%E9%97%AD.html">
  倒闭
 </span>
 ，大批
 <span href="http://www.epochtimes.com/gb/tag/%E4%BE%9B%E5%BA%94%E5%95%86.html">
  供应商
 </span>
 与
 <span href="http://www.epochtimes.com/gb/tag/%E5%91%98%E5%B7%A5.html">
  员工
 </span>
 围堵公司讨债、讨薪。
</p>
<p>
 现场一位
 <span href="http://www.epochtimes.com/gb/tag/%E4%BE%9B%E5%BA%94%E5%95%86.html">
  供应商
 </span>
 向大纪元记者透露，7月4日、5日连续两天百余名供应商聚集在
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%AC%E5%8F%B8.html">
  公司
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E7%BB%B4%E6%9D%83.html">
  维权
 </span>
 ，目前还有供应商还不知道
 <span href="http://www.epochtimes.com/gb/tag/%E5%80%92%E9%97%AD.html">
  倒闭
 </span>
 的事情，陆陆续续会有更多的供应商过来讨债。
</p>
<figure class="wp-caption aligncenter" id="attachment_11368261" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/2-12.jpg">
  <img alt="" class="wp-image-11368261 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/2-12-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  新三板上市公司深圳聚电智能科技股份有限公司突然倒闭，大批供应商与
  <span href="http://www.epochtimes.com/gb/tag/%E5%91%98%E5%B7%A5.html">
   员工
  </span>
  <span href="http://www.epochtimes.com/gb/tag/%E7%BB%B4%E6%9D%83.html">
   维权
  </span>
  。（受访者提供）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11368263" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/4-6.jpg">
  <img alt="" class="wp-image-11368263 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/4-6-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  新三板上市公司深圳聚电智能科技股份有限公司突然倒闭，大批供应商与员工维权。（受访者提供）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11368264" style="width: 450px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/5-3.jpg">
  <img alt="" class="size-medium wp-image-11368264" src="http://i.epochtimes.com/assets/uploads/2019/07/5-3-450x801.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  新三板上市公司深圳聚电智能科技股份有限公司突然倒闭，大批供应商与员工维权。（受访者提供）
 </figcaption><br/>
</figure><br/>
<p>
 这位供应商还表示，他们在维权的同时，也有500余名员工在维权，现场来了大批警察，员工们要求去政府上访，结果遭到警察阻拦，有一名员工被十几名警察围着要将他带走，千方百计阻止他去上访。
</p>
<p>
 据了解，供应商们也想出各种方法维权，“我们上访没用，我们去派出所报案，让我们找经侦（经济犯罪侦察警察），经侦又把我们踢到法院，让我们起诉，反正是像踢皮球一样把我们踢来踢去的。我们有些供应商已经起诉了，但是也没有用，人家也不出面。”
</p>
<p>
 目前供应商正在起诉该公司涉嫌诈骗，公司在倒闭前数月大批量地向供应商订购原材料、生产配件等，在6月份曾在大酒店召集供应商进行了一场酒会，承诺从7月份开始陆续打款，但是却在6月末公司人去楼空，部门主管也随之失联，并且公司还款开出的支票是空头支票。
</p>
<p>
 目前公司值钱的设备已经被搬离，仅剩下不值钱的设备等，具体搬到何处不得而知。
</p>
<p>
 有供应商透露，有消息称该公司法人代表肖彤已入籍加拿大，公司财务总监是肖彤的妻子，供应商们希望警方限制他们出国。
</p>
<p>
 据悉，该公司共欠140余家供应商的货款，金额或达上亿元，目前初步统计总金额已达6000余万元。上述供应商被欠款25万元，“以前也欠款，但是欠的钱会慢慢还，但是这几个月拚命进货，不给钱。”
</p>
<p>
 目前，供应商维权还没有任何结果，仅仅是街道办进行了登记。员工维权已有劳动部门介入。
</p>
<p>
 该公司成立于2004年12月，拥有自己的大型工业园，厂房面积20000多平方米，注册资本为7344万元人民币，员工规模1000人以上。从事网通产品、车联网产品、智慧家庭产品和数字电视产品研发、生产、销售、服务一体的高新技术企业。
</p>
<p>
 2016年12月20日公司在全国中小企业股份转让系统挂牌，两个月前，聚电科技发布公告称，已提交终止挂牌股票挂牌的申请，于5月10日起正式终止挂牌。
</p>
<p>
 目前主要客户有：乐视、烽火、中兴、长城宽带、中国移动、康佳集团、长虹集团、浪潮集团、海信集团、北京四达、上海云视、韩国XCORE、HULEN等。
</p>
<p>
 去年至今公司还陷入了多起金融借款合同纠纷和买卖合同纠纷，公司及董事长本人均在被告之列，可见公司的业绩情况早已不容乐观。
</p>
<p>
 责任编辑：林琮文
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11368256.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11368256.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc413/n11368256.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/6/n11368256.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

