### 政界吁港人再上街：向中共强权打压说不
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010139011366@1200x1200-600x400.jpg"/>
<div class="red16 caption">
 <p>
  新民主同盟立法会议员范国威。（林怡／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月01日讯】（大纪元记者林怡香港报导）
 <span href="http://www.epochtimes.com/gb/tag/%E6%B0%91%E9%97%B4%E4%BA%BA%E6%9D%83%E9%98%B5%E7%BA%BF.html">
  民间人权阵线
 </span>
 （民阵）今天（7月1日）发起七一大游行，呼吁市民继续上街向中共施压，要求撤回恶法，
 <span href="http://www.epochtimes.com/gb/tag/%E7%89%B9%E9%A6%96%E6%9E%97%E9%83%91%E6%9C%88%E5%A8%A5.html">
  特首林郑月娥
 </span>
 下台。多个政党团体在铜锣湾街头设街站，呼吁更多人走上街头，向中共和港府压迫人民说“不”。
</p>
<p>
 在“民主动力”街站的前香港城市大学政治学教授郑宇硕指，市民仍然很愤怒，相信会有很多人在“七一”走出来，“市民对于林郑不下台、不撤回《逃犯条例》的修订，以及不设立独立调查委员会调查警方暴力，感到很不满意，市民依然会利用游行和其它活动表达诉求。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11356634" style="width: 600px">
 <img alt="" class="wp-image-11356634 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010110091366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  前香港城市大学政治学教授郑宇硕。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 他还呼吁，接下来，11月的区议会选举，市民可以用手中选票冲击建制派绝大多数的议席。“高投票率对我们一定有帮助。我相信很多市民对于建制派是彻底失望，因为最基本的林郑也好，建制派政党议员也好，大家看得很清楚，他们都是唯北京命是从，绝对没有好好维护香港人的利益，为香港人的权利打拼，大家看得很清楚。”
</p>
<p>
 他强调，港府强推
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%81%E4%B8%AD%E6%81%B6%E6%B3%95.html">
  送中恶法
 </span>
 的背后，是中共在背后大力“支持”，“送中条例不管是否是林郑自己提出的，肯定之前是得到（中共）中央领导层大力支持，大家都见到韩正、很多中共官员在几个月前就不断接见香港不同的访问团，都叫他们支持条例。”
</p>
<p>
 过去一段时间，有几位反送中的年轻人不幸坠楼离世。前立法会议员刘小丽则在街站准备了大量白色鲜花，让市民为因反送中而牺牲生命的年轻人表达悼念和追思，在游行终点金钟也有设置祭坛，以悼念死者。
</p>
<p>
 刘小丽说，两个周日的逾百万人上街，当中很多是年轻人，“想表达香港是我家，以及他们的诉求，背后的原因及行动，都是源自于对香港的爱。在送中条例之下，香港的言论自由、民主等等完全崩溃，他们都是看到自己香港要自己救，他们不得不走出来维护自己的家园。”
</p>
<p>
 “偏偏这个政权这样的狠心，一意孤行，年轻人受到文攻武斗，用短片去抹黑他们，还谎称他们充满仇恨，我觉得这个政府真的是无药可救。”刘小丽说。
</p>
<figure class="wp-caption aligncenter" id="attachment_11356635" style="width: 600px">
 <img alt="" class="wp-image-11356635 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010110121366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  前立法会议员刘小丽。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 对于港府至今仍拒绝撤回
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%81%E4%B8%AD%E6%81%B6%E6%B3%95.html">
  送中恶法
 </span>
 ，刘小丽表示，她也不明白背后的因素，“收到消息说，很多建制派要求林郑撤回，因为背后太暧昧，但为何又迟迟不肯撤回呢？市民有很多疑虑。但看到林郑的狠心，完全不回应市民的疑虑。我们不知道是否背后有北京的因素，或者林郑自己还有什么图谋？”
</p>
<p>
 对于更多香港人因今次反送中运动而觉醒，刘小丽表示，虽然大家士气高涨，但背后有更多的伤感，“如果不是林郑所逼，香港已经无路可退，怎么会有越来越多的人走出来。”
</p>
<p>
 新民主同盟立法会议员范国威向市民派发反送中和林郑下台的海报。他说，港人今次向中共独裁明确表态说“不”，已不是林郑月娥政府暂缓恶法可以纾解，“因为强推送中，引起极大民愤及国际回响，全球的华人社区都高度关注此恶法，现在已经不是林郑为首的特区政府以暂缓送中来纾解政治危机，现在是北京政府向全世界输出‘锐实力’──直接亮剑，又要打压香港的人权自由民主法治。这种管治，令全球世界各地的华人都出声关注香港，也都表示对中共这种专制独裁说‘不’。”
</p>
<p>
 范国威相信，今天会有数以十万计的港人上街，“港人是再一次站在最前线”。他形容，香港是处于最好的时代，也是最恶劣的时代，“最好就是奋起反抗，为自己的公民权利、香港的法治人权而发声。恶劣是12日警察向赤手空拳、戴着口罩眼罩手持雨伞的港人开枪，我们的政府已经是丧心病狂。特区政府一定要采取适当的方法回应港人的强烈诉求及国际社会的质疑。”
</p>
<p>
 他担心当局透过警权及过度的武力去维持政权，会引发更大的抗争和打压，“当官逼民反时，警方会采取非常恶劣的方法企图将香港人的声音压下去。”#
</p>
<p>
 责任编辑：郑桦
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11356624.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11356624.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11356624.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/1/n11356624.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

