### 香港清场过后仍有零星抗议
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/1906130304121366-600x400.jpg"/>
<div class="red16 caption">
 <p>
  一批市民手持写有“停止射击香港人民”的纸张，在中信桥默站。（蔡雯文／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月14日讯】（大纪元记者林心仪香港报导）经过一夜的清场，昨日和今日（13日及14日）政府总部暂停开放。
 <span href="http://www.epochtimes.com/gb/tag/%E7%AB%8B%E6%B3%95%E4%BC%9A.html">
  立法会
 </span>
 秘书处昨日发通知，昨日和今日不会举行会议。昨日陆续有零星的抗议活动。
</p>
<p>
 上午十时半，添美道连接中信大厦天桥上，有近百名民众聚集，部分人高举起写有“撤回、香港加油”等的纸牌。民主党
 <span href="http://www.epochtimes.com/gb/tag/%E7%AB%8B%E6%B3%95%E4%BC%9A.html">
  立法会
 </span>
 议员
 <span href="http://www.epochtimes.com/gb/tag/%E9%82%9D%E4%BF%8A%E5%AE%87.html">
  邝俊宇
 </span>
 及胡志伟到中信大厦天桥，告诉示威者“立法会今日不会开会”，邝强调与民众“在同一条船上”，集会原则是“不流血，不被捕”，呼吁在场民众“分散”，或可到附近闲游，不要给警方有借口拉开防线，呼吁大家保护自己的安全“一个手足都不能少”，并一起高呼“香港人加油”。
</p>
<figure class="wp-caption aligncenter" id="attachment_11318980" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/1906130020181366.jpg">
  <img alt="" class="wp-image-11318980 size-large" src="http://i.epochtimes.com/assets/uploads/2019/06/1906130020181366-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  近百名民众聚集在中信大厦天桥上，部分人举起写有“撤回、香港加油”等的纸牌。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 下午一时许起，过百名市民手持“停止暴力对待市民、停止射击香港学生”等纸张，在
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E4%BF%A1%E6%A1%A5.html">
  中信桥
 </span>
 默站，资深大律师、前立法会议员余若薇亦有加入。活动一直持续到晚上，不少市民在下班后到场。
</p>
<p>
 另一边，金钟
 <span href="http://www.epochtimes.com/gb/tag/%E6%B7%BB%E9%A9%AC%E5%85%AC%E5%9B%AD.html">
  添马公园
 </span>
 上午也有几百名示威者聚集，大部分人戴着口罩，在公园不同角落观察，有人分发及收集物资。有大批警员向人群搜身，公民党立法会议员谭文豪及民主党立法会议员尹兆坚到场质疑警方没有出示证件，任意搜身，与警方理论。
</p>
<p>
 有在场中学生表示，站出来是不想让自己将来后悔，不想香港变成第二个中国，她强调香港有言论等自由，不希望将来都失去了。也有示威者表示不知道结果如何，但起码自己有站出来出过力。
</p>
<p>
 晚上近九时，一批基督徒在
 <span href="http://www.epochtimes.com/gb/tag/%E6%B7%BB%E9%A9%AC%E5%85%AC%E5%9B%AD.html">
  添马公园
 </span>
 举行祈祷会，逾一百人参加。◇
</p>
<p>
 责任编辑：陈玟绮
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11321215.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11321215.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11321215.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/14/n11321215.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

