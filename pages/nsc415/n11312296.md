### “八十后浪”摆街站 延续伞运精神拒绝恐惧
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/1906100234401366-600x400.jpg"/>
<div class="red16 caption">
 <p>
  “八十后浪”主席高天晖。（王文君／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月10日讯】（大纪元记者王文君香港报导）6月9日香港全城游行，团体“八十后浪”在湾仔摆街站，宣传理念及筹款，他们是由当年雨伞运动参与者骨干组成的一个团体，表达反修例、拒绝恐惧的声音。
</p>
<p>
 “八十后浪”主席高天晖（Kevin）接受本报记者采访时表示，2015年他们成立了“八十后浪”，旨在“以直接和间接的行动，令社会和政府寻求改变的声音。”他并表示，伞运后一直在幕后支援前线组织，例如参选、做一些专业知识及技术上的支援，而非站在第一线。
</p>
<p>
 再次现身街头，他表示，“今次的修订是一件大事，因为它不止影响香港人的人身安全，我们所有香港人的人身安全也会受到损害。”他坦言“我们认为整件事在宣传上可以做得更加好，弥补中间的游离份子了解发生什么事，因为现在有很多朋友，他们其实知道条例修订做什么，但他们会认为跟他们没关系，只要他们安份守己。我们就希望在宣传上告诉他们，其实并不是你安份守己便没事，因为中国政府是不会跟你讲道理的，你跟不讲道理的一群人说你没有做错，你安份守己？而希望别人可以怜悯，你便可以很安稳生活下去？其实你是将你自己的安危依托于对手的‘慈悲’。”他认为，制止修订通过才会令大家安全许多。
</p>
<p>
 今次游行是反对23条立法后最多人上街的一次，他说：“相信这么多人走出来，是因为很多社会团体不断地宣传。但是我经常认为每一件事情我们也可以做得更加好，例如好像今次的运动，我们可不可以检讨雨伞运动我们的得失，令今次有更加好的成果？”
</p>
<p>
 他认为：“事情并不在于是否真的有人生活受到影响，而是大家都生存在恐惧之中，本身就是一个很大的问题。”他坦言，担心后代的工作空间或生存空间会越来越窄，“我担心他们除了商业服务之外不知道还可以做什么工作。生活上就算我们排除政治因素，我们也太少选择。”
</p>
<p>
 “政府施政所导致的结果，我不认为政府让创业更容易，永远向地产商偏重，大家工作也是为了供楼，试问这种经济体系可以维持多久呢？我们看到外国很多新东西走出来，很简单一件uber来到香港，居然生存不了，说明香港政府政策上已有问题。”他并透露，每隔两个礼拜就会跟女友讨论是否移民的问题。@
</p>
<p>
 责任编辑：叶琴
</p>
<p>
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11312296.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11312296.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11312296.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/10/n11312296.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

