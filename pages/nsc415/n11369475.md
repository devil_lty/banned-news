### 【不断更新】7.7港民自发九龙区游行反修例
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070325381366-600x400.jpg"/>
<div class="red16 caption">
 <p>
  有市民发起“7.7 九龙区游行”，下午2时许起，尖沙咀太空馆至梳士巴利花园一带已有大批港人聚集，准备参与游行。（林怡／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月07日讯】（大纪元香港记者站报导）香港再有大型“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”（《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》修订）
 <span href="http://www.epochtimes.com/gb/tag/%E6%B8%B8%E8%A1%8C.html">
  游行
 </span>
 ，有市民发起7月7日下午3时30分，由九龙梳士巴行花园游行至西九高铁站，重申“五大诉求”。这次是首次于
 <span href="http://www.epochtimes.com/gb/tag/%E4%B9%9D%E9%BE%99%E5%8C%BA.html">
  九龙区
 </span>
 举行的“反送中”
 <span href="http://www.epochtimes.com/gb/tag/%E6%B8%B8%E8%A1%8C.html">
  游行
 </span>
 ，希望向大陆游客表达港人和平的“反送中”诉求。
</p>
<p>
 游行经警方同意，已获警方发出“不反对通知书”。发起者之一、沙田社区网络主席刘颕匡强调，这次集会游行为“和平理性优雅”的活动，希望向大陆旅客表达港人“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”的诉求，并展示和平游行的一面，抗衡大陆的新闻封锁，游行到终点西九高铁站后，会呼吁市民和平散去。
</p>
<p>
 “7.7
 <span href="http://www.epochtimes.com/gb/tag/%E4%B9%9D%E9%BE%99%E5%8C%BA.html">
  九龙区
 </span>
 游行”下午3时30分型4时会先在梳士巴利花园举行集会，4时开始由梳士巴利道、九龙公园径、广东道、柯士甸道，游行至西九高铁站对出的汇民道。
</p>
<p>
 <strong>
  17:20
 </strong>
</p>
<p>
 5时左右，游行起点梳士巴利花园的最后一批市民起步出发，不过，附近道路包括梳士巴利道六条行车线和弥敦道的四条行车线，仍然挤满了等候出发的市民。天不时下起阵雨，市民撑起雨伞继续等候。
</p>
<p>
 游行终点西九高铁站外则继续有市民和平聚集。
</p>
<p>
 2047香港监察召集人、资深对冲基金经理钱志健也有上街游行。他赞叹今次网民自发的呼吁，都有这么多人上街，认为参加人数起码有数以十万，“这将是一个持久战，现在不需要一个大台呼吁⋯⋯尖沙咀是很有地标性的地方，之后游行到高铁那边，基本都不需要怎样呼吁，都已经有六位数字的港人上街了。”
</p>
<p>
 他说，林郑至今没有回应市民五大诉求，“这是一个开始，（之后相信）十八区都有用不同的小型游行。”他又强调反送中运动已不只是反对《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》这么简单，“已经是触及到香港的核心价值被摧毁，这是一场港人的捍卫战。这个捍卫战，（港人）未来28年还生活在香港的话，要与香港共存亡。”
</p>
<p>
 对于又多位反送中的市民轻生离世，钱志健表示感到很难过，“生命是无价的，几条人命之间政府都无回应。”不过今次港人的觉醒，以及持续的和平理性非暴力抗争，都已经让国际社会看到，“不同的方法做不同的事情，让国际社会知道港人在挣扎什么，虽然不知道最后会怎样，港人都一直会做下去。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11369678" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070531351366.jpg">
  <img alt="" class="size-large wp-image-11369678" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070531351366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2047香港监察召集人、资深对冲基金经理钱志健。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369679" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070531381366.jpg">
  <img alt="" class="size-large wp-image-11369679" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070531381366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  由高空所见，九龙公园清真寺外的弥敦道，挤满了参加游行的市民。（吴雪儿／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  17:00
 </strong>
</p>
<p>
 接近下午5点钟，游行已起步一个多小时，仍有大量市民在梳士巴利花园、梳士巴利道、弥敦道等地等候出发，塞得水泄不通。由于前进速度缓慢，接近5点时中间道也开放，让在弥敦道的市民经过中间道加入九龙公园径的大队。
</p>
<figure class="wp-caption aligncenter" id="attachment_11369629" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070455221366.jpg">
  <img alt="" class="size-large wp-image-11369629" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070455221366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  大量市民仍然挤在弥敦道、梳士巴利道等一带等候出发。（骆亚／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369636" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070455251366.jpg">
  <img alt="" class="size-large wp-image-11369636" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070455251366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  大量市民仍然挤在弥敦道、梳士巴利道等一带等候出发。（骆亚／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369642" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070456391366.jpg">
  <img alt="" class="size-large wp-image-11369642" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070456391366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  由于前进速度缓慢，接近5点时中间道也开放，让在弥敦道的市民经过中间道加入九龙公园径的大队。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369643" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070456421366.jpg">
  <img alt="" class="size-large wp-image-11369643" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070456421366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  由于前进速度缓慢，接近5点时中间道也开放，让在弥敦道的市民经过中间道加入九龙公园径的大队。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 大学二年级学生童小姐和郑小姐，特别带着简体字标语参加游行：“同胞们，齐来反对”。她们说，经过几次游行政府仍不回应诉求，“一定要上街，不可能不出来游行。”至于怎样看待林郑月娥的回应？“林郑有回应吗？完全不觉得她有回应⋯⋯（态度）劲差，垃圾！”她们认为，特首应该尽快还争取撤回恶法的示威者清白，撤回暴徒的说法和进行独立调查，“其实她如果想还来一个清白，这也是一个好机会。如果她拖下去，任时间蹉跎，不单影响她个人诚信，而是影响了整个社会，造成社会撕裂，市民对警察、市民对政府、还包括市民之间的撕裂。只要她出来回应，很多事情就可以解决。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11369647" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070506161366.jpg">
  <img alt="" class="size-large wp-image-11369647" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070506161366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  童小姐和郑小姐。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369649" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070507201366.jpg">
  <img alt="" class="size-large wp-image-11369649" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070507201366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  游行西九高铁站外聚集了很多游行人士，气氛大致平静，不少市民以普通话叫口号，向大陆游客宣传反送中。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369650" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070507231366.jpg">
  <img alt="" class="size-large wp-image-11369650" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070507231366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  游行西九高铁站外聚集了很多游行人士，气氛大致平静，不少市民以普通话叫口号，向大陆游客宣传反送中。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  16:40
 </strong>
</p>
<p>
 游行西九高铁站外聚集了很多游行人士，气氛大致平静，不少市民以普通话叫口号，向大陆游客宣传反送中。至于游行起点一带，至星光大道仍然大排长龙。梳士巴利花园台上宣布，“无论去文化中心还是海傍都看不到尽头，大家叫多些人出来塞爆九龙。”
</p>
<p>
 70多岁的梁先生手持写有“落实一国武制 实践基本恶法”的标语，讽刺和谴责中共和港府的暴政。他说，看看这个政府过往所做的一切，“政治上、经济上，在打压我们香港人⋯⋯如何能不激到香港人出来？”他表示自己在香港长大，从未见过有如此一个政府这样对待人民，“你看民怨多深，二百万人出来，都可以当没事，你不要再龟缩了，对不对？香港人是站在正义那边的，说我们不对的人很多也是收利益。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11369616" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070449391366.jpg">
  <img alt="" class="size-large wp-image-11369616" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070449391366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  70多岁的梁先生手持写有“落实一国武制 实践基本恶法”的标语。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  16:20
 </strong>
</p>
<p>
 游行起步后约45分钟后，队头已抵达终点，市民游行后经柯士甸站离开。不过大量市民仍然挤在弥敦道、梳士巴利道等一带等候出发。
</p>
<p>
 参与游行的退休人士王小姐表示，上街是因为自己是香港人，“们港人今次非常的团结心齐，希望林郑回应我们的诉求。所以我们会不辞劳苦的，只要是一天他不撤回，我们都会走出来。”她大学毕业的子女都有出来游行，“今次游行的大部分香港人都是要求撤回送中条例，很多都是我们的下一代。他们无法做出进一步的行动⋯⋯我们看到了都很心痛，希望作为父母出一分力，支持他们，谅解他们，而非谴责。”
</p>
<p>
 她也呼吁北京政府：“要遵守一国两制的原则，不要再进一步的侵蚀我们的自由，破坏一国两制的协定，因为香港人其实看的很清楚，资讯这么发达，我们不会这么容易被他们洗脑的。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11369617" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070449171366.jpg">
  <img alt="" class="size-large wp-image-11369617" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070449171366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  退休人士王小姐。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369618" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070442021366.jpg">
  <img alt="" class="size-large wp-image-11369618" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070442021366-600x263.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  另一边厢的海傍，等待游行的市民一直排到星光大道，耐心静候出发。（梁珍／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369619" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070449201366.jpg">
  <img alt="" class="size-large wp-image-11369619" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070449201366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  大量市民仍然挤在弥敦道、梳士巴利道等游行前往终点。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369620" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070449231366.jpg">
  <img alt="" class="size-large wp-image-11369620" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070449231366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  游行起步后约45分钟后，队头已抵达终点。大量市民仍然挤在弥敦道、梳士巴利道等游行前往终点。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369622" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070457061366.jpg">
  <img alt="" class="size-large wp-image-11369622" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070457061366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  有市民拉起“没有暴动 只有暴政”的横额。（王文君／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  16:15
 </strong>
</p>
<p>
 由于游行人数太多，尖沙咀的弥敦道全部行车线已经封闭，站满了准备参加游行的黑衣市民。另一边厢的海傍，等待游行的市民一直排到星光大道，耐心静候出发。
</p>
<figure class="wp-caption aligncenter" id="attachment_11369604" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070436051366.jpg">
  <img alt="" class="size-large wp-image-11369604" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070436051366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  由于先后几有5名“反送中”的市民不幸身亡，有市民挂起横额和直幅勉励市民要一起撑下去。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369605" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070436111366.jpg">
  <img alt="" class="size-large wp-image-11369605" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070436111366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  由于游行人数太多，尖沙咀的弥敦道全部行车线已经封闭。（梁珍／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369607" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070436141366.jpg">
  <img alt="" class="size-large wp-image-11369607" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070436141366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  由于游行人数太多，尖沙咀的弥敦道全部行车线已经封闭。（章鸿／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 来自教会的Raymond，在游行起点自发设置一个街站，让市民用彩色便利贴写上互相加油鼓励的字句，又派发糖果等小礼物给市民，“今天市民再次走出来，我们教会一群弟兄姊妹就想设立一个街站，希望透过这个街站去帮大家加加油、打打气。我们也有些小礼物，是一些糖跟贴纸，去跟大家说我们之间其实是互相鼓励和支持。”他说，就着反送中条例
</p>
<p>
 近来香港政府跟人民都争执了很久，每个周末都上街十分辛苦，“香港市民基本上每个星期六日或重要事也出来示威，表达自己的诉求给政府。”因此他们希望借着写一些支持香港、“香港加油”的字句，为大家加油。
</p>
<figure class="wp-caption aligncenter" id="attachment_11369609" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070436011366.jpg">
  <img alt="" class="size-large wp-image-11369609" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070436011366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  来自教会的Raymond，在游行起点自发设置一个街站，让市民用彩色便利贴写上互相加油鼓励的字句。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  15:55
 </strong>
</p>
<p>
 游行龙头开始沿梳士巴利道游行往西九高铁站，游行人士高喊“释放被捕者、追究警队滥权、立即双普选”“香港人加油！”等口号。也有市民举着“香港撑住”的漫画海报，以及自制的呼吁字句，向大陆旅客喊话：“二百万勇敢的香港人上街反恶法⋯⋯撤回送中条例，维护一国两制，守护香港。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11369563" style="width: 600px">
 <img alt="" class="wp-image-11369563 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070411241366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  游行队伍等候出发。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369564" style="width: 600px">
 <img alt="" class="wp-image-11369564 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070411271366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  游行龙头开始沿梳士巴利道游行往西九高铁站。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369565" style="width: 600px">
 <img alt="" class="wp-image-11369565 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070411301366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  游行龙头开始沿梳士巴利道游行往西九高铁站。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369566" style="width: 600px">
 <img alt="" class="wp-image-11369566 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070411321366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  游行起点梳士巴利花园一带站满了等候出发的市民。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369567" style="width: 600px">
 <img alt="" class="wp-image-11369567 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070411351366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  游行起点梳士巴利花园一带站满了等候出发的市民。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369568" style="width: 600px">
 <img alt="" class="wp-image-11369568 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070411381366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  游行起点梳士巴利花园一带站满了等候出发的市民。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369569" style="width: 600px">
 <img alt="" class="wp-image-11369569 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070411411366@1200x1200-600x450.jpg"/>
 <br/><figcaption class="wp-caption-text">
  游行起点梳士巴利花园一带站满了等候出发的市民。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  15:45
 </strong>
</p>
<p>
 香港大纪元记者现场所见，除了游行起点梳士巴利花园一带站满人，连半岛酒店门外、弥敦道马路上都站满了黑衣人潮，占据了往佐敦方向的两条行车线。
</p>
<figure class="wp-caption aligncenter" id="attachment_11369528" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070349281366.jpg">
  <img alt="" class="size-large wp-image-11369528" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070349281366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  除了游行起点梳士巴利花园一带站满人，连半岛酒店门外，弥敦道马路上都站满了黑衣人潮，占据了往佐敦方向的两条行车线。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11369530" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070349311366.jpg">
  <img alt="" class="wp-image-11369530 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070349311366-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  除了游行起点梳士巴利花园一带站满人，连半岛酒店门外，弥敦道马路上都站满了黑衣人潮，占据了往佐敦方向的两条行车线。（章鸿／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 刚毕业的Peter表示，觉得今日上街是香港人应该做的事，“香港始终是我的家。”对于港府至今未回应民间的五大诉求，他说：“我觉得这个香港政府跟香港人有仇，我只得到这个答案⋯⋯最重要的诉求当然是撤回恶法，还有撤销暴动的定性。始终，为何香港人那天会冲？政府不去思考反而一下子定性为暴动，如果那天是暴动的话，六七那次也是暴动，对比两个暴动，哪个更暴动些？”
</p>
<p>
 过去两次百万人游行（6月9日、6月16日）和七一大游行，Peter也有上街。对于特首林郑月娥，他认为她犹如“活在自己的世界里，都不理会外面世界如何。”至于中共近年对香港越来越强加干预，他形容是“司马昭之心，路人皆知”，他已经登记做选民，在未来的选举“一定不会投建制派”。
</p>
<figure class="wp-caption aligncenter" id="attachment_11369532" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070356511366.jpg">
  <img alt="" class="size-large wp-image-11369532" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070356511366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  刚毕业的Peter继之前两次百万人游行和七一大游行后，在此走上街头，强调“不撤不散”。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  15:35
 </strong>
</p>
<p>
 大会宣布为不幸身亡的几位“反送中”市民默哀一分钟，之后游行准备提早起步。发起人手持五大诉求的横幅，包括：撤回修订条例、追究警队滥权、实行双真普选、收回暴动定性、撤销义士定罪。由于参与人数太多，大会早前呼吁参加者往海傍集合。前来参加的市民大多穿上了黑色衣服。
</p>
<figure class="wp-caption aligncenter" id="attachment_11369526" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070347031366.jpg">
  <img alt="" class="size-large wp-image-11369526" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070347031366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  发起人手持五大诉求的横幅，包括：撤回修订条例、追究警队滥权、实行双真普选、收回暴动定性、撤销义士定罪。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  15:20
 </strong>
</p>
<p>
 大学四年级的陈小姐，今次跟父母一起上街，一家三口手持“我地系香港人”、“责任捉拿 独立调查”、“齐上齐落 不撤不散”的自制标语牌，“他们（父母）一直也支持我上街，我们每一次也会一起出来。”
</p>
<p>
 陈小姐说，在过去的六月，香港人已经走出来很多次表达诉求。“不论是和平示威也好，还是所谓勇武一些的行为我们也做过，但政府也一直没有想聆听我们的声音。”前几天特首林郑月娥假装想聆听学生的声音，要求进行闭门会议，“但她实际上根本只想给中共交待，她还忽略了一个事实，其实这是一个全香港人要求跟她对话，而不是只找学生出来谈就算，她五个诉求也没有回应过。”
</p>
<p>
 陈小姐强调，希望可以继续站出来表达诉求，“我们不可以让这团火这么快熄灭。我们现在还没争取到任何东西，所以我觉得每次例如网上召集的时候，我们也要继续站出来发声，使国际上更加关注这件事情，不要让它被淡忘，或者拖一阵子就完了，就不再理会香港人的想法。”
</p>
<p>
 对于中共对香港核心价值的蚕食，陈小姐直斥“中共愈来愈放肆”，“一国两制现在已经明存实亡，因为他们愈来愈渗透香港，不论是操控立法会选举，或者是想通过一些条例，他们完全漠视了香港的独立司法存在，但他们就不断在冲击我们的一国两制、港人治港的原则。所以我觉得他们真的很卑鄙。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11369492" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070336361366.jpg">
  <img alt="" class="size-large wp-image-11369492" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070336361366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  大学四年级生陈小姐和父母一起上街表达诉求，她表示父母对此非常支持，“每次都会一起出来。”（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 15:00
</p>
<p>
 下午2时许起，尖沙咀太空馆至梳士巴利花园一带已有大批港人聚集，准备参与今天的反送中九龙大游行。不少人带同自制的标语展板，准备向大陆游客讲述香港数百万人反对中共和港府强推《逃犯条例》的事实真相。
</p>
<figure class="wp-caption aligncenter" id="attachment_11369496" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907070325341366.jpg">
  <img alt="" class="size-large wp-image-11369496" src="http://i.epochtimes.com/assets/uploads/2019/07/1907070325341366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  下午2时许起，尖沙咀太空馆至梳士巴利花园一带已有大批港人聚集，准备参与游行。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 责任编辑：李薇
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11369475.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11369475.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11369475.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/7/n11369475.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

