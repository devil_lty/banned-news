### 传港府或暂缓修法 民阵吁周日续游行
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/20190615-biyun-hk001.jpg"/>
<div class="red16 caption">
 <p>
  6月12日“反送中”集会，警方以警棍和催泪弹等暴力驱赶市民。（AFP）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月15日讯】（大纪元记者梁珍香港报导）6‧12港警血腥镇压事件之后，香港特首林郑月娥仍发表讲话，冷血定性当时抗议活动为“暴动”，舆论哗然。各界纷纷站出来谴责林郑一意孤行，要将香港前程尽毁。加上美国出手，要在本周推《香港人权与民主法案》制裁涉事中港官员，亲共阵营纷纷和林郑划清界线，形势逆转。
</p>
<h4>
 传
 <span href="http://www.epochtimes.com/gb/tag/%E6%B8%AF%E5%BA%9C.html">
  港府
 </span>
 受压要推迟条例
</h4>
<p>
 据接近政府高层的前中策组全职顾问、资深传媒人士刘细良向大纪元透露，香港特首林郑月娥有可能在今日（15日）开记者会，宣布暂缓推迟
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 。
</p>
<p>
 至于推迟的原因，刘细良表示，主要是因为民怨太大，特别是民间人权阵线将在周日再度举行大游行，“如果她不想周日会有更多人上街，爆发更大的冲突的话，只有在游行前夕、周六举行记者会，缓和局势。”
</p>
<p>
 事实上，昨日（14日）政圈已经流传林郑月娥晚间八点开记者会，但政府新闻署迟迟没有按常规贴出新闻通知，其后有泛民主派议员向政府查询，表示当晚没有开记者会。
</p>
<p>
 刘细良分析，这说明政府已准备让步，但最后宣布的时间还没有确定。
</p>
<h4>
 中共否认令香港修例 林郑被抛出？
</h4>
<p>
 林郑月娥在6‧12警方暴力镇压示威以来，一直“龟缩”，没有和传媒见面，只是6‧12当晚透过电视访问，向外发布冷血的“暴动论”，更以所谓的母亲身份谴责学生，令社会民愤达到高潮。
</p>
<p>
 6月12日，中共驻英国大使刘晓明接受英国广播公司专访时声称，为堵塞所谓的现时法例之漏洞，修例完全由香港政府自己发起，“（中共）中央从未指示或下令香港修例”。
</p>
<p>
 《苹果日报》消息称，韩正、港澳办和中联办官员正聚集深圳，听取
 <span href="http://www.epochtimes.com/gb/tag/%E5%BB%BA%E5%88%B6%E6%B4%BE.html">
  建制派
 </span>
 商界大佬意见，评估《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》修订的事态发展。据称北京已充分意识到香港正面临一场空前危机，不仅影响到特区政府的管治，亦影响到国家颜面。为平息事件，中央把这次事件定性为外国势力介入，修例乃特区政府自行提出，属香港事务。
</p>
<p>
 据悉，北京已要求特区政府自行收拾烂摊子，即无限期暂缓条例修订，但不撤回。
</p>
<h4>
 亲共阵营纷纷转态
</h4>
<p>
 立法会已连续3日不召开大会，包括下周一、周二也宣布停会。
</p>
<p>
 早前力挺政府的亲共阵营人士，纷纷转态。昨日（14日）包括汤家骅、林正财、罗范椒芬等多名行会成员和政圈重量级人物，不约而同先后表态，支持政府暂缓修订条例，行会召集人陈智思更指现时不能再加剧社会对立，政府应盘算重新解说及咨询。
</p>
<p>
 前亲共第一政党民建联主席、立法会前主席曾钰成接受“Now新闻台”专访时，同意暂缓修法可以纾缓怨气。
</p>
<h4>
 民间人权阵线呼吁市民周日上街
</h4>
<p>
 对于“暂缓”之说，泛民主派议员批评这只是政府缓兵之计。民阵强调“一日不撤回，一日不妥协”。
</p>
<p>
 继6月9日百万大游行，民阵宣布，已向警方申请周日（16日）下午两点半，举办“谴责镇压 撤回恶法”大游行，由维园游行到政府总部。
</p>
<p>
 民阵呼吁市民穿着黑色衫，谴责警方镇压，要求撤回恶法，香港特首林郑月娥下台。@#
</p>
<p>
 责任编辑：陈玟绮
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11323569.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11323569.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11323569.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/15/n11323569.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

