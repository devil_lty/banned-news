### 组图：港府总部“连侬墙”见真实民意
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085716100615-600x400.jpg"/>
<div class="red16 caption">
 2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
</div>
<hr/><p>
 【大纪元2019年06月20日讯】香港雨伞运动期间在香港政府总部外出现的“连侬墙”（Lennon Wall）近日再度贴满抗议字语及图案，民众借此表达对港警的暴力镇压的愤怒与不满。
</p>
<p>
 连侬墙是指香港政府总部大楼的一处外墙，2014年雨伞运动时，民众在这里贴满超过一万张的手写便利贴，上面以语录、歌词、字句和图形等，表达对民主普选的心声，连侬墙成为一种心灵寄托和表达延续雨伞运动的标志。
</p>
<figure class="wp-caption aligncenter" id="attachment_11336036" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085648100615.jpg">
  <img alt="" class="size-large wp-image-11336036" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085648100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部外，市民以文字和图画抗议港警的暴力行为及表达
  <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
   反送中
  </span>
  的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336038" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085636100615.jpg">
  <img alt="" class="size-large wp-image-11336038" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085636100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部外，市民以文字和图画抗议港警的暴力行为及表达
  <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
   反送中
  </span>
  的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336039" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085629100615.jpg">
  <img alt="" class="size-large wp-image-11336039" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085629100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部外，市民以文字和图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336041" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085623100615.jpg">
  <img alt="" class="size-large wp-image-11336041" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085623100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部外，市民以文字和图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336035" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085655100615.jpg">
  <img alt="" class="size-large wp-image-11336035" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085655100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部外，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336034" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085701100615.jpg">
  <img alt="" class="size-large wp-image-11336034" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085701100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部外，市民以文字和图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336042" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085617100615.jpg">
  <img alt="" class="size-large wp-image-11336042" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085617100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部外，市民以文字和图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336018" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620090449100615.jpg">
  <img alt="" class="size-large wp-image-11336018" src="http://i.epochtimes.com/assets/uploads/2019/06/190620090449100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336022" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620090438100615.jpg">
  <img alt="" class="size-large wp-image-11336022" src="http://i.epochtimes.com/assets/uploads/2019/06/190620090438100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以鲜花和头盔悼念坠楼身亡的反送中人士。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336023" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620090432100615.jpg">
  <img alt="" class="size-large wp-image-11336023" src="http://i.epochtimes.com/assets/uploads/2019/06/190620090432100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以鲜花悼念坠楼身亡的反送中人士。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336024" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620090425100615.jpg">
  <img alt="" class="size-large wp-image-11336024" src="http://i.epochtimes.com/assets/uploads/2019/06/190620090425100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336025" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620090416100615.jpg">
  <img alt="" class="size-large wp-image-11336025" src="http://i.epochtimes.com/assets/uploads/2019/06/190620090416100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336030" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620090358100615.jpg">
  <img alt="" class="size-large wp-image-11336030" src="http://i.epochtimes.com/assets/uploads/2019/06/190620090358100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336027" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620090410100615.jpg">
  <img alt="" class="size-large wp-image-11336027" src="http://i.epochtimes.com/assets/uploads/2019/06/190620090410100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336029" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620090404100615.jpg">
  <img alt="" class="size-large wp-image-11336029" src="http://i.epochtimes.com/assets/uploads/2019/06/190620090404100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336031" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085728100615.jpg">
  <img alt="" class="size-large wp-image-11336031" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085728100615-600x400.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11336032" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085722100615.jpg">
  <img alt="" class="size-large wp-image-11336032" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085722100615-600x412.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月20日，香港政府总部的“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达反送中的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 #
 <br/>
 责任编辑：卓惠如
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11336013.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11336013.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11336013.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/20/n11336013.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

