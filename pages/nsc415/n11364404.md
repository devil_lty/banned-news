### 一个香港学生眼中的“立法会遭冲击”事件
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/15a88f5491190036_ttl7daypsa_IMG_1100-600x400.jpg"/>
<div class="red16 caption">
 <p>
  一名参加6‧12抗议活动的香港学生，因不满港警血腥镇压，在推特上呼吁民众挤提中资银行换美金，结果遭到中共网军辱骂甚至勒令其删帖。（梁珍/大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月04日讯】（大纪元记者骆亚采访报导）7月1日晚
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E7%AB%8B%E6%B3%95%E4%BC%9A.html">
  立法会
 </span>
 遭到冲击，外界质疑声大，
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 警方被疑设下“空城计”。有香港学生也认为，整个事件疑点重重，极有可能是香港版的“国会纵火案”，有黑社会人士混入担任“特别任务”。
</p>
<p>
 7.1香港50万人大游行反“送中条例”的这天晚上9点，香港
 <span href="http://www.epochtimes.com/gb/tag/%E7%AB%8B%E6%B3%95%E4%BC%9A.html">
  立法会
 </span>
 遭到冲击，铁闸被示威者撬开，他们进入后发现原本很多警察留守的立法会已成一座“空城”。示威者砸了一些画像，涂鸦了立法会的一些墙面等。
</p>
<p>
 凌晨四点
 <span href="http://www.epochtimes.com/gb/tag/%E6%9E%97%E9%83%91%E6%9C%88%E5%A8%A5.html">
  林郑月娥
 </span>
 召开新闻会，称示威者“以极暴力的方式”冲击立法会大楼，政府必定会追究到底。
</p>
<p>
 保安局长李家超称，冲击立法会的示威者涉嫌一系列严重罪行，违反了公安条例、刑事罪行条例和立法会权力及特权法。
</p>
<p>
 积极投入港人
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 行动的香港学生David接受大纪元采访时表示，“冲击立法会的事件，极有可能是中共的计略，是香港版的‘国会纵火案’，整个事件疑点重重。”
</p>
<p>
 他披露，在7.1当日，他有朋友在立法会战线，“根据复数的信息来源显示，当日在立法会一带发生很多怪异事件。”
</p>
<p>
 他进一步阐述，“首先，面对所谓‘示威者’暴力冲击立法会，全副武装的警察，居然没有什么反应，几乎任由“示威者”冲击玻璃门，如果警察想阻止示威者，是完全可以的，人数足够，重装备，但那天却任由示威者冲击大门。
</p>
<p>
 其次，有游行人士拍到照片，有冲击者在冲击前换衣服，在换衣服的时候，身上露出龙虎纹身，让人怀疑黑社会参与整个事件。”
</p>
<p>
 而且，在冲击者冲入立法会大楼时，警察理论上应该阻止进一步占领，但警察却突然撤退，完全不合逻辑。”
</p>
<p>
 他说，“必须注意到，绝大多数参与冲击立法会的人，是热爱香港的年轻人，很多人都随身携带了遗书，其实做好了牺牲准备。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11364241" style="width: 551px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/2019-07-04-19.28.50.jpg">
  <img alt="" class="wp-image-11364241" src="http://i.epochtimes.com/assets/uploads/2019/07/2019-07-04-19.28.50.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  立法会大厅的墙上抗议学生涂鸦写上“狗官”（骆亚/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 “年轻人占领立法会后，也没有恶意破坏任何大楼设施，甚至在餐厅，甚至主动留下金钱买饮品，完全没有任何偷盗。他们之所以做出激烈行为，完全是因为港共政权和中共，对于示威者的诉求，几乎没有任何回应，包括撤回修法，撤销暴动定性，彻查开枪责任，释放所有无辜民众。”David说。
</p>
<p>
 “他们是因为政权的冷血和对香港的爱，才不惜牺牲自己一生前途，冲击立法会。即使不认同他们的抗争手法，他们的出发点和牺牲都值得最高的尊重。”
</p>
<p>
 这次的港人的
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 系列抗议行动，外界发现香港的学生已经成为港人维权行动的主力军，而且这波学生抗争似乎并没有领头羊和特定的组织。
</p>
<p>
 David以6月自己全程参加的反送中活动为例，“我可以真诚地说，这次的运动是香港市民自发的，没有任何‘领导组织’，甚至所谓‘外部势力介入’。”
</p>
<p>
 “我们都是用社交软体互相协调行动，一切行动都是自我组织，没有‘大台’，即使是民阵，也只是负责协调游行安排，没有任何具体‘指导’。这次的运动，体现出明显的‘去中心化’特征，显示香港人强大的行动力，组织力和协调能力，显示香港的公民社会已经高度成熟。”
</p>
<p>
 对于学生在运动中表现出令人惊叹的行动力和有条不紊的成熟，David认为显示香港年轻人强大的学习能力。“香港从雨伞运动以来，年轻人政治觉醒，积极参与公民运动，从以往的失败不断总结、吸取经验，抗争模式不断进化，体现良好行动力和创意。”
</p>
<p>
 他举例，比如全球登报呼吁关注香港等都是年轻人创举，“显示香港年轻人绝对不是中共想像的只会打游戏和娱乐，不但有公民责任感，抗争模式上，也比老一辈有突破和进步，未来中共想完全控制香港非常困难。”
</p>
<p>
 他认为新媒体平台起到了相当重要的作用，“在香港，由于传统媒体，特别是电视台和传统报纸，在20年的红色资本渗透下，90%以上都被中共控制，报导带有明显倾向性。”正因如此，以年轻人为生力军，香港人组建了大量新型平台，比如各种网路电台，以及以年轻人为主的论坛，如高登讨论区和这次发挥很大作用的连登讨论区，成为了抗争者互通信息和宣传的平台和基地，对运动有极大的助力。#
</p>
<p>
 责任编辑：高静
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11364404.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11364404.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11364404.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/4/n11364404.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

