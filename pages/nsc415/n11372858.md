### 警方多次推撞前线记者 两记者协会发联合声明谴责
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<p>
 【大纪元2019年07月09日讯】7日，网民发起九龙区
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 大游行后，部分市民在旺角聚集。警方当日夜晚至8日凌晨在旺角弥敦道清场，期间多次用盾牌推撞前线记者的镜头及身体，记者协会及摄影记者协会发出联合声明，严厉谴责警方，并指警方严重阻碍采访，妨碍
 <span href="http://www.epochtimes.com/gb/tag/%E6%96%B0%E9%97%BB%E8%87%AA%E7%94%B1.html">
  新闻自由
 </span>
 。
</p>
<p>
 声明指，警方在清场行动时，多次用盾牌推撞前线记者的镜头和身体，喝骂、甚至袭击记者。两个记者协会列举3宗个案，涉及《香港01》、《苹果日报》以及新城电台。其中一名摄记在拍摄现场情况时，被一名便衣女警用手肘攻击腹部时，当摄记上前质问时，女警立即否认并退入警察防线，以致无法得知该女警的警员编号。警方传媒联络队成员曾向该记者道歉，并着记者从正常渠道投诉。
</p>
<p>
 另外一名女摄影记者在摄录期间，被一名男警推撞，然后大声喝骂并指该女记者推撞警员，女记者立即澄清没有，该名男警立即被带走。该名男警身上并无委任证，名字和警员编号都被盖上。另外，一名记者拍摄期间被警员阻挡，有关警员向该记者说：“记者无特权，叫你退就退。”
</p>
<p>
 声明指，个案中的记者都身穿印有“记者”或“PRESS”字眼的反光背心，容易让他人察觉身份，但仍遭到警员的刻意推撞甚至袭击，事件极度严重。
</p>
<p>
 声明又指，警方当日在推进防线时，即使面前只有记者，仍然以盾牌推撞，一名示威者被记者查询有否被警察攻击期间，立即被警员带走，其他警员则要求在场记者离开。而近期多次的游行示威中，都有警员推撞甚至辱骂记者的行为，两大记者协会表示，事件极度严重，并妨碍
 <span href="http://www.epochtimes.com/gb/tag/%E6%96%B0%E9%97%BB%E8%87%AA%E7%94%B1.html">
  新闻自由
 </span>
 ，应予严厉谴责。声明最后指，希望警方正视警权问题，尊重传媒行业采访权，维护新闻自由，保障大众市民的知情权。◇
</p>
<p>
 责任编辑：陈玟绮
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11372858.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11372858.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11372858.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/9/n11372858.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

