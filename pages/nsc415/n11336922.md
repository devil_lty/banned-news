### 学生包围港府总部 港团体吁林郑勿伤年轻人
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/190620093728100484-600x400.jpg"/>
<div class="red16 caption">
 <p>
  团体特首办要求林郑回应年轻人诉求
 </p>
</div>
<hr/><p>
 【大纪元2019年06月21日讯】（大纪元记者林怡香港报导）由于特首
 <span href="http://www.epochtimes.com/gb/tag/%E6%9E%97%E9%83%91%E6%9C%88%E5%A8%A5.html">
  林郑月娥
 </span>
 未于20日下午5时限期前回应撤回修订《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》等四项诉求，大专学界及网民决定今早7时开始
 <span href="http://www.epochtimes.com/gb/tag/%E5%8C%85%E5%9B%B4.html">
  包围
 </span>
 政府总部。另外，大批留守现场的市民呼吁林郑政府不要再伤害香港的年轻人。
</p>
<p>
 对于
 <span href="http://www.epochtimes.com/gb/tag/%E6%9E%97%E9%83%91%E6%9C%88%E5%A8%A5.html">
  林郑月娥
 </span>
 没有回应大专学界提出的诉求，港大、中大、科大、浸大、城大、恒大、教大、演艺学院等学生会，以及香港专上学生联会、明爱专上学院及明爱白英奇专业学校联合学生会等在限期前联合发表《大专学界就政权托辞推搪市民诉求之声明》。
</p>
<p>
 声明重申港人并不需要杀人政权假惺惺的道歉，亦绝不接受“暂缓”、“只有五人在暴动”等说辞。若港共政权在限期内没有回应，撤回修订条例草案、收回6.12之“暴动”定性、撤销对所有抗争者的控罪、以及成立独立调查委员会，追究及彻查警察滥权暴行等诉求，会将行动升级，包括在21日发起和平
 <span href="http://www.epochtimes.com/gb/tag/%E5%8C%85%E5%9B%B4.html">
  包围
 </span>
 政总行动，直至得到回应为止。
</p>
<p>
 20日过了限期，确认林郑政府没回应，大专学界正式宣布行动升级。21日早开始发起不合作运动，早上7时包围政总，和平表达诉求，并呼吁各界同声响应。
</p>
<h4>
 “三罢”齐聚立会等地野餐
</h4>
<p>
 多个群组包括18区联合群组、青年新政等发表联合声明，也呼吁市民21日早上7时，参与全面不合作运动，包括罢工、罢课、罢市，在立法会、特首办公室、警察总部和礼宾府等地外面，以野餐等方式表达不满，直至林郑月娥撤回修订《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》、收回6.12暴动定性等四项诉求。
</p>
<p>
 另外，一群家长、教师、宗教及文化团体成员昨日身穿黑衣到特首办外，要求林郑月娥在下午5时前回应年轻人的诉求。
</p>
<h4>
 各界担忧年轻人人身安全
</h4>
<p>
 刘志雄牧师宣读团体的联合声明，指林郑月娥身为特首无权扼杀香港的未来：“我们不准许你再残害香港的年轻人！”
</p>
<p>
 团体要求林郑月娥立即走出特首办，与团体及年轻人对话，纾解危机；答应学生及市民的合理诉求，从根本上解决问题。
</p>
<p>
 家长联盟发言人表明不接受林郑月娥日前的道歉：“因为小朋友很清晰，年轻人也很清晰，道歉要包括行动，不是只出来说‘对不起啰’。如道歉后无行动那不叫道歉，那是一个假道歉，是做骚。所以我们觉得年轻人现在的要求，那几个诉求是很合理，因为你错了就要改，这是道歉的套餐。”
</p>
<p>
 “我们留守这儿等林郑，希望她出来，不是要她施舍，更不是要她解决问题，而是要她正正式式改过，我们才会考虑给她机会。”家长联盟发言人说。
</p>
<p>
 公民党郭家麒议员批评林郑月娥是投下第一块石头引起民间的冲突：“政府在制造暴力，由始至终将所有反对‘送中’声音置之不理，最粗暴掷第一块石是这政府。但今日我跟张超雄及其他人行出来，是因为我们很不愿21日（周五）再有人受催泪弹，受橡胶子弹，因此跟一班家长今日行出来。”
</p>
<p>
 他又说，若今天学生出来沉不住气再引发类似6.12事件，所有民主派议员会出来保护学生：“所有议员都会站在各自位置监察，不会让警方乱打人，不会让他们连自己编号也不展示，委任证不拿出来，然后乱打学生，这不能再发生。我和张超雄会写信给（监警会主席）梁定邦要求监警会代表明日全时间在这儿监察警方。”
</p>
<p>
 工党员张超雄表示，不少成年人都忧虑再有冲突事件发生，不希望看见年轻人流血、受伤或被捕。他认为成年人应该负起责任，继续留守，并与年轻人沟通。一批家长、牧师、学者继续留守特首办外，直到林郑月娥出来回应。
</p>
<p>
 教育界议员叶建源希望市民使用和平的方法争取，避免激烈的方法，做到不流血、不牺牲和不被捕。他并呼吁警方克制，绝对不能再滥用武力，令社会人士再次失望。他同时呼吁所有参加者注意自身的安全，特别是未成年的中学生，绝不前往可能发生冲突的地区，并恳请老师们向同学转达这个讯息。
</p>
<p>
 20日晚在立法会示威区，有过百名市民分散在不同角落集会，包括连日唱圣诗的人，现场气氛和平。
</p>
<p>
 政府20日晚宣布，基于保安考虑，政总今日暂停开放。公务员无需返回政总工作，并应按照所属决策局或部门的应变计划工作。所有访客活动亦会延期或取消。◇#
</p>
<p>
 责任编辑：李薇
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11336922.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11336922.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11336922.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/21/n11336922.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

