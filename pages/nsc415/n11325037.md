### 港男反送中意外坠楼 民间悲愤：代他走下去
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/6b5930334d2bea312c79fac4074ae406-600x400.jpg"/>
<div class="red16 caption">
 <p>
  反送中抗议现场出现自杀明志的者，港人悲愤，难过流泪。（大纪元合成图）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月16日讯】（大纪元记者骆亚报导）
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 市民大规模反对《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》（也叫《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%81%E4%B8%AD%E6%9D%A1%E4%BE%8B.html">
  送中条例
 </span>
 》）修订抗议示威遭警方暴力镇压，引起国际强烈反弹。有一港男周六（15日）在在一处高空棚架上悬挂“反送中”横幅，不幸意外坠亡，激起市民的悲愤、落泪。
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 市民表示“616上街要代他走下去”。
</p>
<p class="p4">
 <span class="s1">
  推友“轻烟姐姐”表示，该梁姓男子
 </span>
 <span class="s4">
  35
 </span>
 <span class="s1">
  岁，身穿黄色雨衣，在晚上近
 </span>
 <span class="s4">
  9
 </span>
 <span class="s1">
  时，“突然情绪激动爬出棚架，大约
 </span>
 <span class="s4">
  3
 </span>
 <span class="s1">
  名消防员趁机上救他，意外跌落行人路上，然后将他送院抢救，可惜最终不治。”
 </span>
</p>
<p class="p4">
 <span class="s1">
  她还说：
 </span>
 <span class="s1">
  “勇士以死相谏，反送中抗恶法，真普选保家园，共产党滚出香港，林邓月娥必须下台，东方明珠才会再次辉煌。”
 </span>
</p>
<p class="p4">
</p>
<blockquote class="twitter-tweet" data-lang="en">
 <p dir="ltr" lang="zh">
  昨天下午4时许，一名35岁、身穿黄色雨衣的姓梁男子突然爬出太古广场外逾20米高的临时工作平台位置，挂上白色横额，内容提及“全面撤回送中，我们不是暴动，释放学生伤者，林郑下台，
  <br/>
  至近9时，男子突然情绪激动爬出棚架，大约3名消防员趁机上救他，意外跌落行人路上，然后将他送院抢救，可惜最终不治
  <span href="https://t.co/7cGmPJhZ5o">
   pic.twitter.com/7cGmPJhZ5o
  </span>
 </p>
 <p>
  — 轻烟姐姐 (@Lydia354217306)
  <span href="https://twitter.com/Lydia354217306/status/1140041525864407042?ref_src=twsrc%5Etfw">
   June 15, 2019
  </span>
 </p>
</blockquote>
<p>
</p>
<p class="p4">
</p>
<p class="p1">
 <span class="s1">
  这名男子坠亡前在社交群内的留言被广泛流传：“全面撤回送中，我们不是暴动，释放学生伤者，林郑下台。
 </span>
 <span class="s2">
  Help Hong Kong,No extraditiong to China.
 </span>
 <span class="s1">
  ”
 </span>
</p>
<p class="p1">
 <span class="s1">
  很多港人都表示悲痛，也有说这样的表达太惨烈了。
 </span>
</p>
<p class="p4">
 <span class="s1">
  还有港人说：“血债血还！坚定我们的决心和信心！相信英雄也会在天国为我们祈福！相信英雄的血不会白流！相信正义必胜！”
 </span>
</p>
<p class="p4">
 <span class="s1">
  也有推友发悼词：“你站在高处，这个政权用谎言、诬陷、暴力把你推下！愿你安息，求上帝接纳你，在天国只有爱，没有恨！”
 </span>
</p>
<p class="p4">
 <span class="s1">
  民间还制作宣传画：“代他走下去，
 </span>
 <span class="s4">
  616
 </span>
 <span class="s1">
  一起守护香港。”
 </span>
</p>
<p class="p4">
 <span class="s1">
  有港商向大纪元表示，
 </span>
 <span class="s1">
  为了香港的自由，估计还会有殉道者走来。
 </span>
</p>
<p class="p4">
 经过上周日（6月9日）103万港人上街游行“反送中”，及周三（12日）警方武力镇压激起更大的民愤后，香港特首
 <span href="http://www.epochtimes.com/gb/tag/%E6%9E%97%E9%83%91%E6%9C%88%E5%A8%A5.html">
  林郑月娥
 </span>
 今天（15日）下午三点召开记者会，正式宣布暂缓修订。但是泛民主派跟民阵均不接受，称暂缓不是撤回，周日的游行照常举行。
</p>
<p class="p4">
 <span class="s1">
  香港有不同团体举行的上街集会
  <span href="http://www.epochtimes.com/gb/tag/%E6%B8%B8%E8%A1%8C%E7%A4%BA%E5%A8%81.html">
   游行示威
  </span>
  ，至少有五大诉求，撤回送中恶法、不要检控示威者、反对定性暴动、追究开枪责任、
  <span href="http://www.epochtimes.com/gb/tag/%E6%9E%97%E9%83%91%E6%9C%88%E5%A8%A5.html">
   林郑月娥
  </span>
  下台等。
 </span>
</p>
<p class="p4">
 Peter Kong还呼吁众人在集会现场献白花纪念去世的港男。他说：“今日去到太古广场各位，大家做番啲野纪念呢位义士！香港人必会继续奋战，直至胜利！ 而家呼吁每位香港人都带一支花，放去呢个位！为兄弟送上最后的敬意！”
</p>
<h4 class="p4">
 港人抗争精神鼓舞大陆民众抗争
</h4>
<p class="p4">
 大陆不方便署名的北京维权律师向大纪元表示，香港市民、教师、学生及各阶层人士因对中共的目的、对香港法治被破坏的后果等看得很清楚，因此激烈抗争。这促使中共及港府不得不暂缓“
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%81%E4%B8%AD%E6%9D%A1%E4%BE%8B.html">
  送中条例
 </span>
 ”二读，但这只是中共及港府的缓兵之计，港人不要掉以轻心。
</p>
<p class="p4">
 他还认为，中共及港府在“送中条例”一事上“毕竟使港人彻底看清了中共和港府的面目，中共和港府也在此事上确实受挫了。港人的抗争精神不仅鼓舞了港人，也大大鼓舞了内地民众的抗争。”
</p>
<p class="p7">
 <span class="s1">
  台湾的朱婉琪律师也认为，香港的主流的民意是必须撤回条例，如果做到了，香港才是港人做主。这次香港市民的义举也让台湾及国际社会认清了中共及特首林郑无下限的无耻。
 </span>
</p>
<p>
 责任编辑：刘毅
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11325037.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11325037.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11325037.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/16/n11325037.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

