### 八千港爸妈再集会撑年轻人：愿与你们同行
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-2@1200x1200-600x400.jpg"/>
<div class="red16 caption">
 <p>
  八千港人集会为年轻人打气，呼吁年轻人珍惜生命，与成年人一起携手反送中。（李逸/大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月06日讯】（大纪元记者林怡香港报导）近8000名“香港爸妈”周五（7月5日）再度在中环遮打花园举行集会，为香港年轻人“加油”，强调香港的成年人与年轻人同行，鼓励年轻人珍惜生命，一起携手同行
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 。
</p>
<p>
 这是继6.12警方开枪镇压学生后，一群反对修订《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》（送中条例）的“香港爸妈”第二次集会。6月14日晚6000“
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF%E5%A6%88%E5%A6%88.html">
  香港妈妈
 </span>
 ”在中环遮打花园集会，抗议港府暴力镇压抗议民众，怒斥特首林郑月娥是“不合格妈妈”，并为香港年轻人“打气”，强调不会再让他们孤军作战。
</p>
<p>
 周五集会以“生命与玻璃：香港人的选择”为主题，于晚上七时在遮打花园举行，现场人山人海，大部分人身穿黑衣。集会开始时，大家默站一分钟，悼念在“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”事件中轻生的人。
</p>
<p>
 中大人类学系教授郑诗灵读出大会宣言，批评特首林郑月娥，对于有年轻人不惜代价抗争，甚至轻生不闻不问，“但对于立法会的玻璃及大楼公物被毁，却立即在深夜4时召开记者会严词谴责。”
</p>
<p>
 郑诗灵又指，立法会大楼是受示威者破坏，但问题的根源是立法会制度被破坏：“而当立法会成为傲慢政府的应声虫、成为粗暴制定法律限制民权的政府帮凶时，这个议会，尊严已经荡然无存。践踏立法会尊严的，不是抗议的民众，而是滥用议会程序为暴政保驾护航的政府官员和建制派议员。”并提出质疑，是谁令今天的年轻人甘愿冒着身体受伤、被捕入狱、前途尽毁之后果，也要抗争反修例？
</p>
<p>
 香港爸妈们向现今的掌权者呼吁，放下傲慢、尊重生命，尽快接纳香港人、年轻人的五点诉求：撤回修例、撤回暴动定性、设立独立调查委员会、停止白色恐怖、重启政改，还香港人真普选。
</p>
<p>
 主办方准备了数百条布条，让现场市民写上给年轻人打气的字句，一起高举布条，高喊“年轻人加油”、“香港人加油”等口号，象征香港的成年人与年轻人一起同行。
</p>
<p>
 多位嘉宾及市民轮流上台发言，感谢年轻人为这次反送中运动的付出。
</p>
<figure class="wp-caption aligncenter" id="attachment_11368277" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-15@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11368277" src="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-15@1200x1200-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  香港歌手何韵诗感谢年轻人的付出。（李逸/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 香港歌手何韵诗感谢年轻人的付出，强调自己从年轻人身上学到很多东西：“无论在2014年雨伞运动到今日，我看到我们的年轻人一直在进步，一直在他们抗争路线上强化自己，我不觉得我们是在输，我们的每一步也许很细小，但其实我们一直在向前走。我希望我们的年轻人将视线拉远少少，当你觉得做什么都无用，好像做任何事政府也无动于衷，但其实你们做的每件事在影响着其他人。”
</p>
<p>
 也有身为爸爸妈妈的家长上台发言，斥责特首林郑月娥应下台，也批评一些建制派人士连日来攻击年轻人的言论。他们为年轻人的付出感动，呼吁要珍惜生命。
</p>
<p>
 年长的林先生在台上发言时，多次哽咽泪流，他说一生只有两次流过如此多的眼泪，一次是1989年“六四”，第二次是这几天。他说，30年前他觉得没有希望，但这次眼泪背后他看到了希望。这希望是香港年轻一代给的。他同时寄语年轻人“路很长，别再喊，我们要笑着抗争！”
</p>
<p>
 也有爸爸在台上说：“世上有一种爸妈，愿意为你们挡棍挡子弹！”同样换来全场掌声。
</p>
<p>
 30年前，由北京移民来港的陈太也忍不住落泪，对如今香港年轻人不顾性命争取自由，她说会“永远站在年轻人一方”。又说，现在在网上说几句都有被捕风险，所有年轻人的父母都要有子女被捕的准备”。
</p>
<p>
 Boyz Reborn主音年轻的Ben在台上直言自己患抑郁症，分享自己的经历，他忍不住哭泣，坦言最近萌轻生念头，但最后忍住了，“我们每个人都见证着战友离去，我不希望再有人因此牺牲”。
</p>
<p>
 大会宣布有近8千人参加集会，反映香港的成年人，没有离开年轻人。＠
</p>
<figure class="wp-caption aligncenter" id="attachment_11368281" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-3@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11368281" src="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-3@1200x1200-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  八千港人集会为年轻人打气，呼吁年轻人珍惜生命，与成年人一起携手反送中。（李逸/大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11368280" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-5@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11368280" src="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-5@1200x1200-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  八千港人集会为年轻人打气，呼吁年轻人珍惜生命，与成年人一起携手反送中。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11368285" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-1@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11368285" src="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-1@1200x1200-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  八千港人集会为年轻人打气，呼吁年轻人珍惜生命，与成年人一起携手反送中。（李逸/大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11368287" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-8@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11368287" src="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-8@1200x1200-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  八千港人集会为年轻人打气，呼吁年轻人珍惜生命，与成年人一起携手反送中。（李逸/大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11368289" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-12@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11368289" src="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-12@1200x1200-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  八千港人集会为年轻人打气，呼吁年轻人珍惜生命，与成年人一起携手反送中。（李逸/大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11368290" style="width: 569px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-14@1200x1200.jpg">
  <img alt="" class="size-full wp-image-11368290" src="http://i.epochtimes.com/assets/uploads/2019/07/20190705-HK-Mother-Poon-14@1200x1200.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  八千港人集会为年轻人打气，呼吁年轻人珍惜生命，与成年人一起携手反送中。（李逸/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 责任编辑：叶紫微
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11368156.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11368156.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11368156.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/6/n11368156.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

