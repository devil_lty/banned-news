### 新移民上街反修例 黎明：望汇入民主主流当中
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/190609090555100311-600x400.jpg"/>
<div class="red16 caption">
 教大科学系讲师黎明女士（中）。（宋碧龙／大纪元）
</div>
<hr/><p>
 【大纪元2019年06月10日讯】（大纪元记者王文君香港报导）在民阵组织的全城“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”大游行中，由新移民组成的“同根社”成员亦在湾仔修顿球场起步，加入到游行队伍当中。2008年来港的教大科学系讲师黎明女士表示，“见证着香港自由空间的缩小”“希望以新移民身份重新进入（香港）民主运动的主流当中，成为其中一个重要的力量。”
</p>
<p>
 在香港成立了20几年的“同根社”，早期是由大陆嫁来香港的婚姻移民妇女组成的自助互助组织，昨日共有10几个人出来游行。“同根社”干事黄佳鑫表示，很多妇女都反对修例，但对基层妇女而言仍有很多考虑。没有出来的妇女希望出来的人能把她们的声音也带出来。
</p>
<figure class="wp-caption aligncenter" id="attachment_11312280" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190609090622100311.jpg">
  <img alt="" class="wp-image-11312280 size-large" src="http://i.epochtimes.com/assets/uploads/2019/06/190609090622100311-600x399.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  同根社也上街游行，抗议香港政府修订
  <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
   逃犯条例
  </span>
  。（宋碧龙／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 同根社成员来自大陆不同的省份，最远的有黑龙江、吉林，近的有广东深圳。“她们非常清楚大陆的法律很差…贪污、收买公安、官员等，有些亲身经历，自己或朋友因传了某些讯息出去，很快被删除及上门问话。还有妇女的亲人经商，长期都会遇到一些不公平待遇，令其小本生意会受影响，甚至有妇女的丈夫现在被困禁等案例发生。”他说，她们害怕，一旦修例通过，香港会由法治变成人治。
</p>
<p>
 以“抗争不拒新移民，民主路上并肩行”口号，同根社在湾仔加入游行队伍，教大科学系讲师黎明女士表示，来到香港后，深刻领会和受益于香港社会的自由环境，“你可以自由地表达自己的意见和观点，可以有自己独立的思考，这是一个很珍贵的社会环境。”可惜，“08年到现在，我一路见证着香港的自由环境越来越小，今次一旦修例通过，香港的自由环境会同大陆差不多。”她表示，亦眼见香港的学术研究受到限制。
</p>
<p>
 她说，虽然雨伞运动期间，国内的家人有遭到国安上门警告。但她会做自己认为对的事，而不会在做之前考虑周全自己的利益得失。@
</p>
<p>
 责任编辑：杜文卿
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11312256.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11312256.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc415/n11312256.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/10/n11312256.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

