### 佚名：一个退伍老兵留给新兵蛋子的信
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/4cbd949d9f1073603281ae9bdfc0b14c-600x400.jpg"/>
<div class="red16 caption">
 <p>
  2018年10月，山东平度老兵维权遭警方用喷辣椒水、警棍暴力清场。（大纪元合成图）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月27日讯】兄弟：
 <br/>
 明天我就要背着行囊回家了，这些天看你走进来，对这个地方充满了期待。接下来的新兵训练日子里如果你没有被老兵们打死，那以后你我若在你们维稳的现场再次相逢，请你看在这些天里我对你还算客气的份上，对我手下留情吧！
</p>
<p>
 当时我也是像现在的你一样对未来充满希望和激情走进这个地方的。几年下来，我终于明白了，所谓的
 <span href="http://www.epochtimes.com/gb/tag/%E4%BF%9D%E5%AE%B6%E5%8D%AB%E5%9B%BD.html">
  保家卫国
 </span>
 ，只不过是一个传说而已！我的不少兄弟在军营的睡梦中老家房子被挖机夷为平地！家中老父慈母爱妻幼子在寒风中哭泣！谁也保不了自己的家！卫国就更扯了，我们军队对越作战后，悄悄地把
 <span href="http://www.epochtimes.com/gb/tag/%E6%9E%AA%E5%8F%A3.html">
  枪口
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E8%B0%83%E8%BD%AC.html">
  调转
 </span>
 对准了手无寸铁的老百姓！
</p>
<p>
 1989年那个夏夜，三十万我们的野战军对天安门广场上迷茫无助的学生和市民发起了勇猛的进攻，“一战成名，战果辉煌”！
</p>
<p>
 接下来三十年里我们的军队从未敢对外放过一枪一弹，而从未放弃过对老百姓的武力威胁！
</p>
<p>
 每次强征血拆都有我们军队矫健的身影，钢枪霍霍，对准那些挣扎在死亡边缘的人们！
</p>
<p>
 说来好笑，我们的老百姓，包括你我的父母都这样，把他们的血汗钱交给政府。政府招来你我，用他们的血汗钱来供我们吃喝，吃饱了再来打压有时不怎么听话的他们！用他们的血汗钱买来子弹用来屠杀他们！所以说
 <span href="http://www.epochtimes.com/gb/tag/%E4%BF%9D%E5%AE%B6%E5%8D%AB%E5%9B%BD.html">
  保家卫国
 </span>
 ，对我们现在的中国军人就是放屁！
</p>
<p>
 这几年里，我们在政府的指挥下
 <span href="http://www.epochtimes.com/gb/tag/%E9%95%87%E5%8E%8B.html">
  镇压
 </span>
 过无数次集体维权的群众，每次都不放过老幼妇孺，一视同仁的打死方休，有时我看到那些无助的眼神，心里也有些难忍，也有想放过他们的时候，可是我们都知道我们的后脑勺上有无数我们自己人的
 <span href="http://www.epochtimes.com/gb/tag/%E6%9E%AA%E5%8F%A3.html">
  枪口
 </span>
 默默的瞄准着！所以今后遇到这样的情况，你在保全自己的情况下，尽量地放过那些可怜的人吧，他们中有很多也曾经穿着军装意气风发过。岁月让他们从施暴者沦落为受虐者，所以放过今天的他们，就是希望有人放过明天的我们自己！这算我对你的第二个请求吧。
</p>
<p>
 我们村里的张大爷上过朝鲜战场，临死前家里穷的叮当响，他自己不说起，都没人知道这个慈祥的老人，有过那样壮阔的人生，然而又能怎么样？做完炮灰就被遗忘，你我将来都一样！
</p>
<p>
 我家隔壁的何大叔，上午在田间插秧，晚上被送进县城，第二天直接去了广西前线，没有一天训练，做了炮兵，干什么，背炮弹！现在他偶尔说起，愤怒不已，还好他在炮弹的缝隙里捡回了一条命。前年的包围八一大楼的老兵中就有他。被我们的兄弟打成骨折，现在还躺在床上！
</p>
<p>
 还有很多我认识的老兵，曾经穿起军装打人，今天脱下军装被打！所以兄弟，以后你
 <span href="http://www.epochtimes.com/gb/tag/%E9%95%87%E5%8E%8B.html">
  镇压
 </span>
 维权群众的时候尽量下手轻点，因为你有天脱下军装后也会沦落为被打的对象！
</p>
<p>
 前些年，我们在某市维稳，大热的天，商店不卖凉水给穿军装的我们。饭馆不卖饭菜给穿军装的我们。他们终于知道吃饱喝足的我们在打他们的时候会更用力，所以他们想渴死饿死我们。曾经宣传的鱼水关系成了最大的谎言！
</p>
<p>
 这些年我们除了征地，拆迁，还有到医院抢尸体和挖老百姓的祖坟，现在回想起来。貌似当几年兵就做了几年的伤天害理的事情。以后慢慢赎罪吧！
</p>
<p>
 兄弟，希望你看到这封信后好自为之吧，希望你到了离开这里的时候，同样留下这样一封信给你后来的兄弟。这是我对你的第三个请求！
</p>
<p>
 你的兄弟
</p>
<p>
 2019年
</p>
<p>
 责任编辑：高义
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc993/n11348123.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc993/n11348123.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc993/n11348123.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/26/n11348123.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

