### 关乐：香港反送中展望
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<p>
 【大纪元2019年06月24日讯】
</p>
<p>
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 启程，为共党送终。
 <br/>
 恶党没想到，美梦成噩梦。
 <br/>
 六四欲重演，招数暗照搬。
 <br/>
 只是临末日，时过境亦迁。
 <br/>
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 非北京，卅年东西岸。
 <br/>
 依旧超限战，恶法破底线。
 <br/>
 党虽更无耻，力却不从愿。
 <br/>
 九评①扒光衣，巨著②剖心肝。
 <br/>
 真相广传播，邪灵原形现。
 <br/>
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%89%E9%80%80.html">
  三退
 </span>
 ③连根拔，已过三亿三。
 <br/>
 二十三条推，直冲法轮功。
 <br/>
 法徒讲真相，
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%89%E9%80%80.html">
  三退
 </span>
 劝不停。
 <br/>
 刺党还放血，必欲拔干净。
 <br/>
 失手心不甘，气急败坏疯。
 <br/>
 走投无路际，送中抛匆匆。
 <br/>
 也是内斗剧，江曽挖陷阱。
 <br/>
 无法无天贼，未料碰大丁。
 <br/>
 世人渐觉醒，港民气难平。
 <br/>
 百万上街吼，橡皮弹回应。
 <br/>
 猛增二百万，刀断潮更涌。
 <br/>
 倒海香江翻，天下大奇观。
 <br/>
 染红特首府，漂摇欲沉船。
 <br/>
 全港不合作，直逼邪政瘫。
 <br/>
 月娥已被抛，收回仍不敢。
 <br/>
 易上不易下，贼船特凶险。
 <br/>
 韩正曾庆红，火上把油添。
 <br/>
 重演六四败，倒习计不变。
 <br/>
 捧习抬火上，乘势夺回权。
 <br/>
 若习再不醒，难逃此劫难。
 <br/>
 无论习醒否，鬼谋均枉然。
 <br/>
 天在灭中共，有谁能阻拦？
 <br/>
 习若醒过来，顺天除佞奸。
 <br/>
 习若不醒悟，合污同凄惨。
 <br/>
 神自有胜算，明珠复灿烂。
 <br/>
 港民釜已破，愈勇志愈坚。
 <br/>
 得道自多助，全球援手牵。
 <br/>
 明珠如火炬，引爆众火山。
 <br/>
 红朝陷火海，共党化灰烟。
 <br/>
 好人迎未来，中华换新天。
</p>
<p>
 注：
 <br/>
 1. 九评：这里指的是大纪元系列社论《九评
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%B1%E4%BA%A7%E5%85%9A.html">
  共产党
 </span>
 》。
 <br/>
 2. 巨著：这里指《九评》编辑部：《共产主义的终极目的——中国篇》；《魔鬼在统治着我们的世界——
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%B1%E4%BA%A7%E5%85%9A.html">
  共产党
 </span>
 的幽灵并没有随着东欧共产党的解体而消失》。
 <br/>
 3. 三退：用真名或化名退出中国共产党的党、（共青）团、（少先）队邪教组织。有关资料显示，截止到2018年3月23日，在大纪元退党网站（http://tuidang.epochtimes.com）声明“三退”总人数突破三亿。截至2019年6月23日，三退人数已超过三亿三千五百四十多万。并且，这些年来一直以每天十万的数字递增着。
</p>
<p>
 “‘青山遮不住，毕竟东流去。’邪不胜正，看似猖獗的所有邪恶表象都是暂时的，一切都掌握在神的手中。2004年，《大纪元时报》发表的《九评共产党》开启了中国的‘三退’大潮，数亿人退出了中共的党、团、队组织，这是中国人驱除共产邪灵附体的自救之举。人只要主动‘三退’，神就会将邪灵附体瞬间清除，这个生命就将属于未来！”
</p>
<p>
 “解体共产邪党，清理人间的共产主义邪恶因素，全面反思近二百年来人类社会的堕落和魔变，成为今天人类的当务之急。归正人心，净化社会，回归传统，重建信仰，重新体认与神的联系，找回与神的纽带，这是每个人的责任，也是每个人得救的希望所在！神的慈悲与威严同在！神在看着每个人的内心。一个人在此时此刻的抉择和所为，就会决定他（她）的未来。”（《共产主义的终极目的——中国篇》结语）
</p>
<p>
 “中国虽然集中了共产邪灵最主要的力量，但千千万万中国人在坚持信仰和普世价值，和平抵抗共产暴政；在《九评共产党》引发的‘三退’（退出共产党、团、队组织）运动中，三亿多人勇敢选择从精神上脱离共产枷锁。这种个人发自心底的选择，正在解体共产党于无形。神安排了中共最后的解体。中国的执政者和其他掌握权柄的人，如果有意解体中共，神为其安排好了所有的一切，包括未来天赋神授的真正权柄；相反，如果死抱中共不放，必定会在最后的过程中遭遇中共解体所带来的一切灾祸、魔难。”（《魔鬼在统治着我们的世界——共产党的幽灵并没有随着东欧共产党的解体而消失(28)》结束语，大纪元2018年12月27日）
</p>
<p>
 责任编辑：高义
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc993/n11342228.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc993/n11342228.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc993/n11342228.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/24/n11342228.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

