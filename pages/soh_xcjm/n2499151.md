### 【往事回首】原北大教授夏业良：走出自己的人生路（九）——第八篇  奋笔疾书
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="zhidingtu">
 <div class="ar-wrap-3x2">
  <img class="ar-wrap-inside-fill" src="http://img.soundofhope.org/2018/12/xiayeliang9-600x400.jpg"/>
 </div>
</div>
<hr/>
<div class="content">
 <p class="content-info-datetype">
  <span class="content-info-date">
   【希望之声2018年12月20日】
  </span>
  <span class="content-info-type">
   （人物专访：经济学家、原北大教授夏业良）
  </span>
 </p>
 <div class="soh-audio-player status-published">
  <div class="jp-jplayer" id="audio-player-0">
  </div>
  <div aria-label="media player" class="jp-audio" data-url-h="http://media.soundofhope.org/audio03/2018/12/xiayeliandi9ji.mp3" data-url-l="http://media.soundofhope.org/16K-mp3/audio03/2018/12/xiayeliandi9ji_16K.mp3" id="audio-player-container-0" role="application">
   <div class="panel">
    <div class="control">
     <div class="button pnp">
      <i aria-hidden="true" class="fa fa-play">
      </i>
     </div>
     <div class="slider-progress">
     </div>
     <div class="button mute">
      <i aria-hidden="true" class="fa fa-volume-up">
      </i>
     </div>
     <div class="slider-vol">
     </div>
     <div class="button download">
      <span href="http://media.soundofhope.org/16K-mp3/audio03/2018/12/xiayeliandi9ji_16K.mp3">
       <i aria-hidden="true" class="fa fa-cloud-download">
       </i>
      </span>
     </div>
     <div aria-label="..." class="btn-group bps" role="group">
      <button class="btn btn-default bps-l active" type="button">
       16K
      </button>
      <button class="btn btn-default bps-h" type="button">
       128K
      </button>
     </div>
    </div>
   </div>
  </div>
 </div>
 <p>
  上一篇我们谈到夏业良拒绝和校方签署保密协议，让校方无可奈何。而在2009年5月，夏业良又做了一件大事，那就是给时任中宣部部长刘云山写了一封公开信，在他的新浪博客上发表，公开信主要有两方面的内容，一个是直接点名刘云山对央视大楼着火应该负主要责任。另一个就是指责刘云山领导下的中宣部控制国民思想、阻碍言论自由、学术自由。
 </p>
 <p>
 </p>
 <p style="text-align: center;">
  <img alt="" class="aligncenter size-full wp-image-2499172" src="http://img.soundofhope.org/2018/12/xyl20181220.jpg" srcset="http://img.soundofhope.org/2018/12/xyl20181220.jpg 600w, http://img.soundofhope.org/2018/12/xyl20181220-180x101.jpg 180w, http://img.soundofhope.org/2018/12/xyl20181220-366x205.jpg 366w">
   经济学家夏业良。（图片来源： TheLibertyWeb）
  </img>
 </p>
 <p>
  “2009年春节期间不是中央电视台着火吗，新楼给烧了的那一次大火，后来中央处置结果很晚才出来，五月份才公布处置的结果，就是把办公室主任撤销，哪些人哪些人有责任，这样处理的，据我了解当时放烟火引起的，当时是因为中宣部部长刘云山要看烟火，所以点燃了烟火造成的火灾，所以这样处理我是非常不服气的，我觉得怎么会这样处理，我一看就很气愤，当场我就花了二十多分钟写了一个致刘云山的公开信，而且那个信不是一个严肃的文件，是带有一点情绪上的发泄、愤慨，20多分钟一回写了就发在我的博客上，如果要发表在正式的报刊或者其他方面，我可能还会更加的斟酌文字，结果没想到，反响非常的热烈，海内外那么多的人，媒体都关注到了，所以点击量非常高，然后中央也很震惊，我记得大概信的内容，你这样一个事件，怎么这样就处理了，把人家当替罪羊，我说你这个刘云山生为主要负责人，为什么不公开说道歉，你应该向全国人民说道歉，这是第一个要求，第二个，我对他的不满是，基于长期以来，他是对意识形态上的打压和控制，而且不光是他个人，是中宣部这个机构就是专门干这个事，要控制人们的思想自由和言论自由，所以我讲我说你何德何能，刘云山你的学历是内蒙古中专，是一个中等师范学校，他的专业叫政治教育专业，我说你后来中央党**拿了一个什么文凭，假的东西，你的真实的程度就是这样的程度，你何德何能你要控制中国人的思想，说这个不对那个不对，你的正确思想从哪来的?你怎么能够保证你说的是对的?所以我愤怒的斥责了他，用一些语言非常，当然用得非常不恭敬，甚至是*贬损的语言，这个发表以后，学校领导找我谈话，说你太过分了，你怎么对中央领导人用这样的词句攻击，你要好好的反省，要写检讨，我说我不会写检讨，我认为我做的事情没错，他说你为什么还认为你做得没错呢?我说当年陈独秀李大朝胡适他们在北大的时候，他们也经常写文章，公开的批评政府，公开的批评一些东西，他说你怎么这么大胆，把你自己跟他们相提并论，我说我有什么不能跟他们相提并论呢?他们是历史人物，但他们对有些东西的认知，他未必有我今天这个认知高度，他们当时是在北大，思想某一个流派的，某一种学说或者学问的，这样一种领导者、引领者。我现在在北大，我们也在做这样的工作，我们也是希望把一些普世价值，自由主义的一些好的理念，分享给大家。他说你的思想太反动了，而且我现在告诉你要悬崖勒马，你要是不听，将来你的后果一切责任自负，警告我，其实警告我不止一次了。”
 </p>
 <div class="widget ad-free">
  <!-- 3.0 TEST RESPONSIVE -->
  <ins class="adsbygoogle" data-ad-client="ca-pub-1519518652909441" data-ad-format="auto" data-ad-slot="4345005170" style="display:block">
  </ins>
 </div>
 <div class="content-info-btm">
  <p class="content-info-zerenbianji">
   <span class="content-info-title">
    责任编辑：
   </span>
   <span class="content-info-content">
    香梅
   </span>
  </p>
  <p class="content-info-refernote">
   （希望之声版权所有，未经希望之声书面允许，不得转载，违者必究。）
  </p>
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_xcjm/n2499151.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_xcjm/n2499151.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/soh_xcjm/n2499151.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.soundofhope.org/gb/2018/12/20/n2499151.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

