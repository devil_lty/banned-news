### 色心渎神招奇报 拒色守德荫福子孙
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/2019-07-05_080806.png" target="_blank">
  <figure>
   <img alt="色心渎神招奇报 拒色守德荫福子孙" src="https://i.ntdtv.com/assets/uploads/2019/07/2019-07-05_080806-800x450.png"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  高句丽墓葬壁画，女娲像。中国吉林省集安市洞沟古墓群五盔坟4号墓。（公有领域）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  《封神演义》中，商纣王前往女娲宫进香，因陡然刮起一阵狂风，卷起了宫殿幔帐，显现出女娲圣像。纣王见圣像容貌端丽，栩栩如生，顿时神魂飘荡，陡起淫心。提笔作诗亵渎正神，从此导致四海荒荒，大好江山就此断送在纣王手上。
 </p>
 <p>
  在不少古籍记载中，因不守本分，大动色心遭报的例子不胜枚举；而因拒绝美色，获得
  <span href="https://www.ntdtv.com/gb/福报.htm">
   福报
  </span>
  的人也大有人在。历史演绎文化，留下了正反两方面的教训。
 </p>
 <p>
  <strong>
   题淫诗亵渎神的恶果
  </strong>
 </p>
 <p>
  晚清文人李庆辰著作《醉茶志怪》，其中记载了一则作诗亵渎神灵，遭石龟所压的例子。庆都（今保定）有座古庙，因年久失修，色彩剥落，加之无人主持与打理，古庙显得很是破败。唯独庙宇后殿有一尊侍女站像，衣裳色彩鲜丽光洁，姿态动人。
 </p>
 <p>
  庆都有个姚生行事放浪，为人轻薄。一天，他在古庙看到这尊侍女站像，顿时大动色心，提笔在侍女像的胸襟前提了一首淫诗，亵渎神女像。
 </p>
 <p>
  姚生回到家后，一直想着那尊侍女像，因其色心炽烈，辗转反侧，始终难以成眠。这时，忽然他听到院子里响起一阵玉环玉佩碰撞的声音。于是，他隔窗相望，只见一个貌美的女子，踏着月光珊珊走来，敲著屋门请求进来。
 </p>
 <p>
  姚生一见，正是古庙中的那位侍女呀。他欣喜若狂，赶紧打开屋门，让她进来，急不可耐得即要求欢。那女子说：“你再这么癫狂粗暴，我马上就走。”姚某这才放手。女子坐在他的腿上，千娇百媚地看着他。姚生淫心荡漾，不知所措。
 </p>
 <p>
  不一会儿，他感觉到双腿被什么东西压得越来越重，痛到难以忍受的地步。姚生再仔细一看，哪里还是什么庙中侍女，原来是庙中驮石碑的赑屃（石刻龟）啊。
 </p>
 <p>
  石龟死死地压在他的腿上，他腿痛剧烈，犹如骨折一般。姚生恐慌，赶紧喊人求救。来了几个力大的乡民一起合力，才把石龟移开。可是，姚生的腿已经废了。
 </p>
 <p>
  常言道：“色心起时，即为大过。”姚生大动淫心，作淫诗亵渎神明，轻薄之行为自己招致现世奇报。
 </p>
 <p>
  此民间故事中姚生心念不正，看到塑像都敢动妄念。然而古代的正直之士，面对主动投怀送抱的女色，却能以正念抵挡，亦在民间留下美谈。
 </p>
 <p>
  <strong>
   拒绝美色 荫福子孙
  </strong>
 </p>
 <p>
  宋神宗熙宁初年，仪州华亭有一位医生，医术颇为精湛，名叫聂从志。他的同乡邑丞的妻子李氏，曾经罹患重疾，一度生命垂危，幸得聂医生的治疗后，得以康复。
 </p>
 <p>
  李氏貌美，然而好淫，贪慕聂医生的容貌。一天，她的丈夫前往邻郡办理公务，李氏谎称自己旧疾复发，派人去叫聂从志。
 </p>
 <p>
  当聂医生到来后，李氏对他说：“上次得病险些死亡，幸得先生治疗，得以复生。考虑到世间之物不足以报答先生救命之恩，今天我愿以身相报。”
 </p>
 <p>
  聂从志一听，惊惧不已，婉言力拒。李氏哭着哀求着，坚持委身相报。聂医生是位正直之士，岂会做这等有辱医风医德之事。他急忙转身离开，火速回到家。李氏再派人去请，聂医生终是不去。
 </p>
 <p>
  等到夜里，李氏盛装打扮，直接推门进入聂的屋室。不知廉耻地纠缠着，她抓着聂的手，请他一定要随其所愿。聂从志奋力挣脱，断袖而去。这件事就此停止了，聂从来没有对人说起过。
 </p>
 <p>
  过了一年多，仪州推官黄靖国患病，昏冥之中被阴差带入冥界作证。黄靖国还阳之前，有一个冥吏叫他稍留片刻，带他来到一条河边。只见狱卒揪著一个妇人，持刀剖她的腹部，为其洗肠。
 </p>
 <p>
  旁边有一位僧人说：“这个妇人是你的同官邑丞的妻子。因其想与聂从志医生私通，聂没有答应。他见美色而不动心，可谓是一位善士。本来聂医生寿数六十，因
  <span href="https://www.ntdtv.com/gb/拒色.htm">
   拒色
  </span>
  积德，神明为他延寿一纪（十二年），并在每一世赐予他子孙一人做官。而这名妇人则减寿十二年。为了去掉她的淫心，所以在此为她清洗肠胃。”
 </p>
 <p>
  黄靖国素来与聂医生多有交往，二人交情颇深。当他醒来后，专程赶至聂家，向对方询问此事。聂从志听罢，吃惊地说：“李氏和我私语时，没有一个人听到。而且她夜晚来我家时，也正好只有我一人独处于室中。此事只有我和那妇人知道。您是从何处听来的？”
 </p>
 <p>
  黄靖国就将在地府的所见所闻，全都告诉了聂从志。此事渐渐地就传开了。聂医生去世后，他的一个儿子科举登科，他的孙子聂图南，则于绍兴年间，做了汉州雒县县丞。果然应了僧人所说。
 </p>
 <p>
  （据《夷坚志》丙卷02，《醉茶志怪》）#
 </p>
 <p>
  ──转自《大纪元》
  <br/>
  （责任编辑：张信燕）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102615898.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102615898.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102615898.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/05/a102615898.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

