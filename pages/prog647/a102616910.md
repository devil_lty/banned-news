### 小暑节热风来 冬病夏治三伏贴
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/1ab597a79eaf18f76efcb6ec7db1d0af.jpg" target="_blank">
  <figure>
   <img alt="小暑节热风来 冬病夏治三伏贴" src="https://i.ntdtv.com/assets/uploads/2019/07/1ab597a79eaf18f76efcb6ec7db1d0af-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  夏日炎炎，幸有荷花可赏。（Pixabay)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月06日讯】
  <span href="https://www.ntdtv.com/gb/小暑.htm">
   小暑
  </span>
  是黄历24
  <span href="https://www.ntdtv.com/gb/节气.htm">
   节气
  </span>
  的第11节气，夏天的第5个节气，每年7月7日前后，太阳到达黄经105°时为小暑。2019年的小暑交于7月7日17时09分（北京时间）。
 </p>
 <p>
  《说文》曰：暑，热也。古书记载：“斗指辛为
  <span href="https://www.ntdtv.com/gb/小暑.htm">
   小暑
  </span>
  ，斯时天气已热，尚未达淤极点，故名也”。《月令七十二候集解》中说:“六月节……暑，热也，就热之中分为大小，月初为小，月中为大，今则热气犹小也。”民间谚语中也有“小暑不算热，大暑三伏天”的说法。
 </p>
 <p>
  古代将小暑分为三候：“一候温风至；二候蟋蟀居宇；三候鹰始鸷。”
 </p>
 <p>
  一候温风至，是指小暑
  <span href="https://www.ntdtv.com/gb/节气.htm">
   节气
  </span>
  之后，空气温热，风吹在身上没有凉爽的感觉。
 </p>
 <p>
  二候蟋蟀居宇，这个时期蟋蟀的羽翼还没有长成，只能躲在洞穴中“面壁”。
 </p>
 <p>
  三候鹰始鸷，这是说小暑节气的后五天，老鹰开始在高空中飞翔。
 </p>
 <p>
  <strong>
   小暑食新
  </strong>
 </p>
 <p>
  过去民间有“食新”习俗，即在小暑过后即把新米做成米饭，酿成新酒。新割的稻谷碾成米后，做好饭供祀五谷大神和祖先，以此感谢神明赐予，恳请祖先保佑。然后尝新酒吃酒，夏天喝白酒，就像热水泡脚一样，在适量的情况下，对人体是有益，也是养生的一个方法。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102616940" style="width: 600px">
  <img alt="" class="size-medium wp-image-102616940" src="https://i.ntdtv.com/assets/uploads/2019/07/lotus-root-1039492_1280-600x400.jpg">
   <br/><figcaption class="wp-caption-text">
    藕具有清热养血除烦等功效，适合夏天食用，鲜藕以小火煨烂，切片后加适量蜂蜜，有安神助睡、治血虚失眠等功效。（Pixabay)
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  <strong>
   小暑食藕
  </strong>
 </p>
 <p>
  民间素有小暑吃藕的习俗，早在清咸丰年间，莲藕就被钦定为御膳贡品了。因与“偶”同音，故民俗用食藕祝愿婚姻美满，又因其出污泥而自洁白，也成为清廉高洁的象征。
 </p>
 <p>
  南方地区有小暑吃蜜汁藕的习惯。将鲜藕用小火煨烂后，切片后加适量蜂蜜当凉菜吃。也可以把藕切成薄片，用开水焯一下，加醋等凉拌。还可以把藕和排骨一起清炖。藕中含有大量的碳水化合物及丰富的钙磷铁等，具有清热养血除烦等功效，适合夏天吃。
 </p>
 <p>
  人们平时食用时，往往把藕节丢弃。其实藕节本身就是很好的药材，而且营养丰富。将藕节加红糖煎服，对于吐血、咳血、尿血、便血、子宫出血等都有不错的效果。
 </p>
 <p>
  <strong>
   小暑黄鳝赛人参
  </strong>
 </p>
 <p>
  除了吃藕之外，在南方的一些地区，有着“小暑黄鳝赛人参”的说法，小暑前后是吃黄鳝的好时节。黄鳝作为食品，按中医的说法有着祛湿、滋补的功效，而小暑前后正是黄鳝最为肥美的时候，确实也很适合食用。
 </p>
 <p>
  从中医角度来讲，黄鳝性温味甘，具有补中益气、补肝脾、除风湿等作用。而从营养学角度来说，黄鳝在鱼类中属于热量较低的那种，和红色肉类比热量就更低了，但所含蛋白质、维生素和矿物质却一点也不低，非常适合天气炎热时食用。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102616930" style="width: 600px">
  <img alt="西瓜清甜多汁，是夏季的解暑佳品。（Pixabay)" class="size-medium wp-image-102616930" src="https://i.ntdtv.com/assets/uploads/2019/07/watermelon-1969949_1280-600x400.jpg">
   <br/><figcaption class="wp-caption-text">
    西瓜清甜多汁，是夏季的解暑佳品。（Pixabay)
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  <strong>
   解暑水果数西瓜
  </strong>
 </p>
 <p>
  炎炎夏日出汗多，需要及时补充水分。所以一些水分充足的水果，特别适合小暑前后食用。比如西瓜和西红柿，都是特别适合的选择。可以将这两种水果去皮、去籽之后混在一起，压榨成果汁饮用。西瓜是特别好的消暑补水的水果，有着“天生白虎汤”的美誉。“白虎汤”是东汉末年张仲景名著《伤寒论》内的方子，用来退烧降温。西瓜性寒，具退热功效，故人称西瓜为“天生白虎汤”。西红柿能生津止渴，对于夏季口渴烦躁、消化不良等热症有很好的效果。
 </p>
 <p>
  <strong>
   小暑
   <span href="https://www.ntdtv.com/gb/保健.htm">
    保健
   </span>
   -敷贴
   <span href="https://www.ntdtv.com/gb/三伏贴.htm">
    三伏贴
   </span>
  </strong>
 </p>
 <p>
  小暑后，7月12日就正式进入今年的初伏，可以敷贴
  <span href="https://www.ntdtv.com/gb/三伏贴.htm">
   三伏贴
  </span>
  。
 </p>
 <p>
  三伏贴可疏通经络，调理气血，宽胸降气，健脾胃，鼓舞阳气，调节人体的肺脾功能，使机体的免疫功能不断增强，从而达到振奋阳气、促进血液循环、祛除寒邪、提高卫外功能的效果。
 </p>
 <p>
  三伏天是肺经气血运行最旺盛的时刻，这时候在俞穴上敷以辛温发散之中药，可达到祛除寒邪、宣通经络、补益人体正气的功效，而且又让季节温度与药物相辅相成，达到冬病夏治的预防目的。
 </p>
 <p>
  <strong>
   静心养心
  </strong>
 </p>
 <p>
  小暑之季，气候炎热，人易心烦不安，疲倦乏力。所以民谚说“大暑小暑，有米懒煮”。小暑是人体阳气最旺盛的时候，保护人体阳气，以符合“春夏养阳”之原则。《灵枢•百病始生》曰：“喜怒不节则伤脏”。所以
  <span href="https://www.ntdtv.com/gb/养生.htm">
   养生
  </span>
  重点突出“心静”二字，平心静气，确保心脏机能的旺盛。
 </p>
 <p>
  <strong>
   夏不坐木
  </strong>
 </p>
 <p>
  民间有“冬不坐石，夏不坐木”的说法。暑过后，气温高、湿度大。久置露天里的木料，如椅凳等，经过露打雨淋，含水分较多，表面看上去是干的，可是经太阳一晒，温度升高，便会向外散发潮气，在上面坐久了，能诱发痔疮、风湿和关节炎等疾病。所以，尤其是中老年人，一定要注意不能长时间坐在露天放置的木料上。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102616935" style="width: 600px">
  <img alt="" class="size-medium wp-image-102616935" src="https://i.ntdtv.com/assets/uploads/2019/07/carnation-393259_1280-600x422.jpg"/>
  <br/><figcaption class="wp-caption-text">
   石竹花颜色殷红，花朵玲珑。（Pixabay)
  </figcaption><br/>
 </figure><br/>
 <p>
  <strong>
   小暑诗歌赏析
  </strong>
 </p>
 <p>
  唐诗《小暑六月节》形象地描述了小暑三候：“倏忽温风至，因循小暑来。竹喧先觉雨，山暗已闻雷。户牖深青霭，阶庭长绿苔。鹰鹯新习学，蟋蟀莫相催。”
 </p>
 <p>
  “殷疑曙霞染，巧类匣刀裁。不怕南风热，能迎小暑开。游蜂怜色好，思妇感年催。览赠添离恨，愁肠日几回。”
 </p>
 <p>
  这首《答李滁州题庭前石竹花见寄》，由艳丽娇媚的石竹花写起：石竹花颜色殷红，花形精致，像是朝霞染红，又好像经巧手裁过。石竹花不怕小暑的热风，依然摇曳生资；它引得蜜蜂飞来流连忘返；独守空闺的妇人看见了，不免触景伤情，自伤自怜。诗人阅读朋友的赠诗，更增添了离恨别愁，忧思郁结在心里，一天难过好几回。
 </p>
 <p>
  唐代白居易的《消暑》诗写道：“何以消烦暑，端坐一院中。眼前无长物，窗下有清风。散热由心静，凉生为室空。此时身自保，难更与人同”。心静自然凉，一直坐到“眼前无长物”，自然能感受到窗下吹来的凉风。这首诗平和恬淡，兼备哲理禅理，犹如一汩清流，能抚慰浮躁的心灵。
 </p>
 <p>
  （记者李蒨蒨报导/责任编辑：祝馨睿）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102616910.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102616910.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102616910.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/06/a102616910.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

