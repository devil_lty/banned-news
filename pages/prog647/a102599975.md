### 中华传统美德故事（孝篇）之四：曾参养志
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/2018-05-13_130946.png" target="_blank">
  <figure>
   <img alt="中华传统美德故事（孝篇）之四：曾参养志" src="https://i.ntdtv.com/assets/uploads/2019/06/2018-05-13_130946-800x450.png"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  在曾子的心中，时刻想到的都是父母的需要。（图pixabay）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  <span href="https://www.ntdtv.com/gb/曾子.htm">
   曾子
  </span>
  名参，字子舆，是春秋时期鲁国人。他与父亲曾点都是孔子的优秀学生。曾子非常孝敬他的父母。
 </p>
 <p>
  在日常生活中，每到吃饭的时候，
  <span href="https://www.ntdtv.com/gb/曾子.htm">
   曾子
  </span>
  一定都会细心观察和体会父母的饮食口味与习惯，并将父母最喜欢吃的食物牢牢记在心里。因此，一日三餐，曾子总能准备出父母最爱吃而又很丰盛的菜肴。
 </p>
 <p>
  在曾子的心中，时刻想到的都是父母的需要，父母所喜爱的一切事物，他也都会放在自己心里，以便随时满足他们的心愿。
 </p>
 <p>
  父亲曾点深受圣贤教诲的熏陶，平常乐善好施，经常接济贫困的邻里乡亲。对于父亲的这个习惯，曾子也同样铭记在心。所以，每次父母用过饭后，他都会毕恭毕敬的向父亲请示，这一次余下的饭菜该送给谁。
 </p>
 <p>
  曾子不但对于奉养父母的身体非常重视，即使在日常生活、言语行为当中，也非常谨慎，惟恐有辱父母的养育之恩，担心自己表现不好而使父母蒙羞。
 </p>
 <p>
  孔子知道曾子是个
  <span href="https://www.ntdtv.com/gb/孝子.htm">
   孝子
  </span>
  ，所以将“孝道”的学问传述给他。在《孝经》中，孔子与曾子以一问一答的形式，把孝道表露开解无遗。他嘱托曾子一定要把孝道发扬光大。可见，曾子的为人和孝心孝行非同一般常人。
 </p>
 <p>
  曾子一生秉承孔子的教诲，依教奉行，专心致力于孝道，他用自己一生的行为操守来告诉我们，如何将孝道落实在日常生活当中。他不但做到了“入则孝，出则弟”，还做到了“谨而信”，并且把夫子所教的这些德行流传于后世，培育他的学生。由他所传述的《孝经》，也传至今日。
 </p>
 <p>
  纵观天下父母之心，都是希望自己的孩子能够成龙成凤，希望他们能有所成就。然而，成就“功名利禄”并不算真有成就，而像曾子这样成就“道德学问”才算真有成就。
 </p>
 <p>
  ──转自《明慧网》
 </p>
 <p>
  （责任编辑：张信燕）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102599975.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102599975.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102599975.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/13/a102599975.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

