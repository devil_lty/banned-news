### 佛家故事：得道高僧的临别赠言
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/2019-06-28_173624.jpg" target="_blank">
  <figure>
   <img alt="佛家故事：得道高僧的临别赠言" src="https://i.ntdtv.com/assets/uploads/2019/06/2019-06-28_173624-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  佛家故事（图pixabay）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  耆域从印度来到中国，住在洛阳好多年了。他是一位
  <span href="https://www.ntdtv.com/gb/得道高僧.htm">
   得道高僧
  </span>
  。有一年，洛阳发生了战乱，耆域要离开洛阳，回印度去。
 </p>
 <p>
  有一个名叫竺法行的和尚，也是当时有名的高僧，人们把他比作乐令。竺法行和尚对耆域说：“大师既然是一位
  <span href="https://www.ntdtv.com/gb/得道高僧.htm">
   得道高僧
  </span>
  ，请留下一句话，作为我们永久性的警诫之词吧。”
 </p>
 <p>
  耆域同意后，说：“可把大家都召集来。”
 </p>
 <p>
  众人都来了，耆域登上讲坛，说了四句偈语：
 </p>
 <p>
  “守口摄身意，
  <br/>
  慎莫犯众怒；
  <br/>
  修行一切善，
  <br/>
  如是得度世。”
 </p>
 <p>
  （这四句偈语的大意是讲：修身要谨慎，不要犯众怒；要永远做好事，这样才能得到超度。摄身：约束自身。度世：超度此生此世，不堕轮回之苦。）
 </p>
 <p>
  耆域说罢，便静默不语了。
 </p>
 <p>
  竺法行又请求说：“希望大师告诉一些前所未闻的话。像您刚才说的这个偈语，八岁小孩子，也能背下来，这可不是我们对得道之人所希望的啊！”
 </p>
 <p>
  耆域笑着说：“八岁孩子虽然能背诵，可是活到一百岁都不去履行，仅仅是背诵，那有什么益处呢！人们都知道尊敬得道的人。却不知道：履行──才是能使自己得道的法则。可悲啊！我说的虽少，但是做起来，好处可就大了。”
 </p>
 <p>
  说罢，就告辞了。耆域慢慢地走着，后面的人却怎么跑，也追不上。耆域用手杖在地上画了一下，说：“在此分别了！”
 </p>
 <p>
  正是：
 </p>
 <p>
  纸上得来非彻明，
  <br/>
  绝知此后应躬亲；
  <br/>
  背诵的确很重要，
  <br/>
  做到才是真修行，
 </p>
 <p>
  做到、做到、做到才是真修行！
 </p>
 <p>
  （事据《高僧传奇》）
 </p>
 <p>
  ──转自《明慧网》
  <br/>
  （责任编辑：张信燕）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102611125.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102611125.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102611125.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/28/a102611125.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

