### 修身拒色　志道方成
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/2019-06-19_133753.jpg" target="_blank">
  <figure>
   <img alt="修身拒色　志道方成" src="https://i.ntdtv.com/assets/uploads/2019/06/2019-06-19_133753-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  成恶淫为首（图pixabay）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  世间有志能成者，多为重德行善之人，而德行中拒色戒淫堪为首要。
 </p>
 <p>
  古代山东有个叫林长康的秀才，到了四十岁，还没考中举人。一天，他心里正思虑著不再举仕途了，忽然听到旁边有声音高呼：“不要灰心！”林秀才惊问：“何人？”只听那声音回应：“我乃阴间魂魄，这几年来一直跟在先生您身边，守护着您。”
 </p>
 <p>
  林秀才要他显形，他不肯。林秀才一连要求四次，魂魄说：“先生当真想见我也行，只是你见到我不要心生恐惧。”秀才表示同意。魂魄随即跪立于秀才面前，只是流血却看不见面孔，说：“我是蓝城县的平民，掖县的张某人将我谋害了，尸首就压在东城门外的石磨底下。先生您日后要官升掖县县令，所以我常侍奉在您左右，请求日后为我破案申冤。”
 </p>
 <p>
  冤魂还告诉林秀才，哪一年要中乡试举人，哪一年要进士及第，说完就不见了。到了那一年，林秀才果然举孝廉，但是预言中进士那一年却爽期了，林秀才感叹：“世间功名事项，鬼魂也有不知啊！”话未落音，空中忽有声音大声的说：“先生您自己行为有不端啊，不是我误报！先生在某月某日与某孀妇有私通不轨之事，幸好没有胎孕，世上无人知晓，但阴司已经记下您的恶行，只是宽恕了罪，将您本应通过的两科科考推后了。”
 </p>
 <p>
  林秀才大惊，方知世上的一切，神灵都有记载。于是自那时起，他诚心悔过，谨言谦行，
  <span href="https://www.ntdtv.com/gb/戒色.htm">
   戒色
  </span>
  拒淫，时时修身。后如期通过了先前未能通过的两科考试，擢取进士，授官掖县。上任后，他立刻巡城，在东城门外看见了一个石磨，命手下启开，果然有具尸体。林进士立刻传讯拘捕了张某，经审问，张某将自己杀人始末具实说出，后被依律惩办。
 </p>
 <p>
  世人贪色消福禄，悔过守正事业成。修道的人贪色，则直接影响功力。
 </p>
 <p>
  晋太康元年的旌阳令许逊是道教净明道、闾山派祖师，曾是大洞真君吴猛的弟子。当时江东一带有很多蛇祸。吴猛打算清除它们，吴猛选了一百多个徒弟前往。到了高安这个地方时，吴猛命人准备了一百斤木炭，然后用尺子量好尺寸将木炭截断成均码的木炭条，再把木炭条放在道坛上。
 </p>
 <p>
  到了晚上，这些木炭条全部化成了白衣女子，纷纷奔赴众徒弟面前，搔首弄姿。次日清晨，吴猛查看他的弟子们，没有一个人的衣服上没有木炭黑灰的，唯有许逊的衣服没被染黑。吴猛就带着许逊到了辽江，等待巨蛇出没。吴猛年迈，巨蛇出来时，许逊踏着七星步伐，手持利剑蹬上蛇头，挥剑将蛇杀掉了。
 </p>
 <p>
  显然，其他的众徒弟们因贪色而无力除魔。
 </p>
 <p>
  事据：袁枚《子不语》、段成式《酉阳杂俎》
 </p>
 <p>
  ──转自《明慧网》
  <br/>
  （责任编辑：张信燕）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102608785.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102608785.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog647/a102608785.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/25/a102608785.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

