### 【直播】独立日演讲 川普：我们都是神的子民
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153784177-600x400.jpg"/>
<div class="red16 caption">
 川普和夫人抵达会场。(MANDEL NGAN/AFP/Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月04日讯】7月4日是美国独立纪念日（也叫国庆日），对美国人来说是最为重要的纪念日，同时也是表达爱国的日子。今年的
 <span href="http://www.epochtimes.com/gb/tag/%E7%8B%AC%E7%AB%8B%E6%97%A5.html">
  独立日
 </span>
 庆祝，美国还将展现美国强大的军力，美国总统川普还将在林肯纪念堂前发表演讲。
</p>
<p>
</p>
<p>
 在每年的7月4日，美国民众都要纪念美国于1776年脱离英国成为独立的国家。
</p>
<p>
 《华盛顿自由灯塔》分析发现，2020年总统大选的民主党竞选者们的“
 <span href="http://www.epochtimes.com/gb/tag/%E7%8B%AC%E7%AB%8B%E6%97%A5.html">
  独立日
 </span>
 ”计划和川普的计划相比，爱国主义严重缺乏。
</p>
<p>
 前总统巴拉克．奥巴马（Barack Obama）曾在一次国庆会上展示酷刑折磨设备；川普今年以向美军致敬来庆祝美国243岁生日，国内股市更在国庆前一天创下历史新高。
</p>
<p>
 周四上午，川普在推特上写道，“今天和今晚，人们从各地来到这里，和我们一起参加我们国家历史上最大的庆祝活动之一。美国军力展示全新的武器，军事人员和前沿装备。在总统演讲时，会有21响礼炮，空军一号也会飞行。”
</p>
<p>
 川普承诺今晚的烟火秀是“华盛顿特区有史以来最大的烟花表演”，将为“我们国家历届7月4日庆祝活动中，最伟大的庆祝活动”做结尾。
</p>
<p>
 他周三发推文说，“这将是一生中最精彩的表演”。此前，他在谈到今年独立日庆典时也曾表示，“这将是独一无二的”，并说届时将有战机从头顶飞过，有坦克展现在人们面前。 “这将是一生中最精彩的表演”。
</p>
<p>
 <span style="font-weight: 400;">
  川普在向美国致敬演讲中，提到美国历史上的数位总统和其他名人：从托马斯﹒杰斐逊（Thomas Jefferson）到乔治﹒华盛顿（George Washington），再到亚伯拉罕﹒林肯（Abraham Lincoln）；从托马斯﹒爱迪生（Thomas Edison）、亚历山大﹒格雷厄姆﹒贝尔（Alexander Graham Bell），到莱特兄弟（Wright Brothers）和克拉拉﹒巴顿（Clara Barton）。
 </span>
</p>
<p>
 <span style="font-weight: 400;">
  他在结语中说：“我们永远不会忘记我们是美国人，未来属于我们。未来属于勇敢、强者、自豪和自由。我们是一群追逐一个梦想和一个伟大命运的人。”
 </span>
</p>
<h4>
</h4>
<h4>
 下午7:40
</h4>
<figure class="wp-caption aligncenter" id="attachment_11365434" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/15ae59788c9b6841_ttl7day7Jg_000_1IC5YN-e1562284639916.jpg">
  <img alt="" class="size-large wp-image-11365434" src="http://i.epochtimes.com/assets/uploads/2019/07/15ae59788c9b6841_ttl7day7Jg_000_1IC5YN-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  川普结束讲话，与第一夫人一起向民众挥手致意。(Brendan Smialowski/AFP)
 </figcaption><br/>
</figure><br/>
<h4>
</h4>
<h4>
 下午7:30
</h4>
<figure class="wp-caption aligncenter" id="attachment_11365412" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790433-e1562283462287.jpg">
  <img alt="" class="size-large wp-image-11365412" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790433-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  川普、梅拉妮亚和副总统彭斯在台前聆听演奏。(Brendan Smialowski/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<p>
 <span style="font-weight: 400;">
  川普、
 </span>
 <span style="font-weight: 400;">
  梅拉妮亚和副总统彭斯一起聆听演奏《共和国战歌》。
 </span>
</p>
<h4>
</h4>
<h4>
 下午7:28
</h4>
<p>
 川普说，“我们都是
 <span href="http://www.epochtimes.com/gb/tag/%E7%A5%9E%E7%9A%84%E5%AD%90%E6%B0%91.html">
  神的子民
 </span>
 。”
</p>
<p>
 “我们永远不要忘记我们是美国人，未来属于美国。未来属于勇士，强者，荣耀和自由。”
</p>
<p>
 川普说，美国有着共同的梦想和伟大目标，同时欣赏共同的英雄，家庭和精神，“我们都是神创造的”。他表示，美国的独立精神永远不会消失，但她会随着时间发生变化。
</p>
<p>
 川普再次向美国致敬，并介绍下面的庆祝活动，包括蓝天使的飞行表演。他说：“神保佑美国，神保佑美国民众。”
</p>
<h4>
</h4>
<h4>
 下午7:25
</h4>
<p>
 川普说，250年前，由农民、黑人、民兵组成的血肉之躯为美国赢来了自由以及自治，让后来人见证了这些勇士的正义之举。
</p>
<p>
 “他们捍卫了我们与生俱来的权利，以及对代表我们美国的国旗和伟大国家的忠诚。”他说。
</p>
<p>
 川普表示，美国必须作为一个整体以及为同一个目标前进。“只有按照先驱之路，以及铭记我们伟大的历史，为我们更美好的未来而努力，美国必须这样做。”
</p>
<h4>
</h4>
<h4>
 下午7:22
</h4>
<p>
 川普谈到美国陆军，并提到在二战期间，美国陆军被部署到欧洲，参加打击纳粹的战斗。
</p>
<p>
 他接着说，陆军战士在朝鲜、越南和阿富汗参加战斗。
</p>
<p>
 <span style="font-weight: 400;">
  川普说，陆军是地球上最伟大的军队。
 </span>
</p>
<h4>
</h4>
<h4>
 下午7:20
</h4>
<p>
 <span style="font-weight: 400;">
  在音乐声中，两架海军陆战队的
 </span>
 <span style="font-weight: 400;">
  新型运输机鱼鹰式倾斜旋翼机（MV-22 Ospreys）以及将接替总统专机、尚未服役的“陆战队一号”（Marine One）飞过主席台。
 </span>
</p>
<h4>
</h4>
<h4>
 下午7:16
</h4>
<figure class="wp-caption aligncenter" id="attachment_11365454" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790462-e1562285567213.jpg">
  <img alt="" class="size-large wp-image-11365454" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790462-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  海军的蓝色天使战队届时将在华府上空组成一个天桥。(BRENDAN SMIALOWSKI/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<p>
 <span style="font-weight: 400;">
  川普谈到海军陆战队，“在世界各地，海军陆战队都让美国的敌人感到害怕，他们总是站在前沿。”
 </span>
</p>
<p>
 <span style="font-weight: 400;">
  川普提到贝鲁特军营爆炸案，美国在那次事件中失去220名海军陆战队员。
 </span>
</p>
<h4>
</h4>
<h4>
 下午7:12
</h4>
<p>
 <span style="font-weight: 400;">
  川普说：“天空属于美利坚合众国。”
 </span>
</p>
<p>
 接下来川普谈到美国海军，并提到F/A-18大黄蜂式战斗攻击机。
</p>
<h4>
</h4>
<h4>
 下午7:08
</h4>
<figure class="wp-caption aligncenter" id="attachment_11365430" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790471-e1562284178637.jpg">
  <img alt="" class="size-large wp-image-11365430" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153790471-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  两架F-22和一架B-2出现在天空中。(MANDEL NGAN/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<p>
 <span style="font-weight: 400;">
  川普开始谈到美国空军的实力，并表示F-22战机和一架B-2幽灵轰炸机将升空表演。随后，两架F-22和一架B-2出现在天空中。
 </span>
</p>
<p>
 F-22“猛禽”（F-22 Raptor）是世界上第一架单座双引擎第五代隐形战斗机，主要任务是取得并确保战区的制空权，额外的任务包括对地攻击，电子战和信号情报。F-22于2005年进入美国空军服役。洛克希德·马丁为主承包商，负责设计大部分机身、武器系统和F-22的最终组装。合作伙伴波音航太则提供机翼、后机身、航空电子综合系统和培训系统。
</p>
<p>
 <span style="font-weight: 400;">
  B-2“幽灵”（Spirit）是目前世界上唯一的匿踪战略轰炸机。
 </span>
</p>
<h4>
</h4>
<h4>
 下午7:02
</h4>
<p>
 <span style="font-weight: 400;">
  总统称赞美国海岸警卫队及其成就。
 </span>
</p>
<h4>
</h4>
<h4>
 下午6:58
</h4>
<p>
 <span style="font-weight: 400;">
  川普总统称赞（美国）军队、执法人员以及妇女和民权运动。
 </span>
</p>
<p>
 <span style="font-weight: 400;">
  川普呼吁美国年轻人加入美军。他说：“你应该这样做。”
 </span>
</p>
<h4>
</h4>
<h4>
 下午6:56
</h4>
<p>
 <span style="font-weight: 400;">
  川普向50年前在月球插上美国国旗的美国宇航局宇航员表示敬意。
 </span>
</p>
<h4>
</h4>
<h4>
 下午6:50
</h4>
<figure class="wp-caption aligncenter" id="attachment_11365385" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153784304-e1562281803178.jpg">
  <img alt="" class="size-large wp-image-11365385" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153784304-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  川普在台上致词。(BRENDAN SMIALOWSKI/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<p>
 “当我们今晚聚在一起，伴随着自由的喜悦，我们需要铭记那些所有分享非凡的传统美德的人。我们是美国故事，有史以来最伟大的故事中的一部分。”川普说。
</p>
<p>
 在全场的掌声和欢呼声中，川普说：“美国人热爱我们的自由，没有人能把它从我们身边带走。”
</p>
<p>
 他补充说：“我们的国家今天比以往任何时候都更强大，是她最强大的时候”，现场人群齐声高喊“USA”。
</p>
<p>
 当总统抵达时，一架标志性的、被称为空军一号的波音747飞越林肯纪念堂，沿着国家广场一直飞到美国国会大厦，然后转向。
</p>
<h4>
</h4>
<h4>
 下午6:44
</h4>
<figure class="wp-caption aligncenter" id="attachment_11365377" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153784207-e1562281418288.jpg">
  <img alt="" class="size-large wp-image-11365377" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153784207-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  川普和夫人抵达会场。(MANDEL NGAN/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<p>
 川普和第一夫人梅拉妮亚抵达演讲现场 。
</p>
<p>
</p>
<h4>
 下午6:35
</h4>
<p>
 <span style="font-weight: 400;">
  军事飞机将在下午6:30-7:30之间飞行在天空。 预计将看到固定翼和旋翼飞机，这不是军事行动。
 </span>
</p>
<p>
 <span style="font-weight: 400;">
  一名国防官员告诉美国有线电视新闻网，7月4日将有大约750至800名军人参加在华盛顿举行的“向美国致敬”庆祝活动。
 </span>
</p>
<h4>
</h4>
<h4>
 下午5:57
</h4>
<p>
 川普发推文说，天气看起来不错，气温快速下降。45分钟后见，下午6:30至7:00见。在林肯纪念堂（见）！
</p>
<blockquote class="twitter-tweet" data-width="550">
 <p dir="ltr" lang="en">
  Weather looking good, clearing rapidly and temperatures going down fast. See you in 45 minutes, 6:30 to 7:00 P.M. at Lincoln Memorial!
 </p>
 <p>
  — Donald J. Trump (@realDonaldTrump)
  <span href="https://twitter.com/realDonaldTrump/status/1146900759906279424?ref_src=twsrc%5Etfw">
   July 4, 2019
  </span>
 </p>
</blockquote>
<p>
</p>
<h4>
 川普讲话摘要
</h4>
<p>
 白宫发布的川普讲话摘要如下：
</p>
<p>
 今天，在向美国致敬活动上，我们作为一个国家齐聚在一起，庆祝我们的历史，我们的人民，以及这些怀着自豪捍卫我们国旗的英雄——勇敢的美国男性女性军人！
</p>
<p>
 当我们今晚沉浸在自由的喜悦中时，我们所有人依然记得要真实地分享一个非凡的故事。我们是有史以来最伟大的故事——美国故事的一部分。
</p>
<p>
 我们拥有和开国元勋同样的美国精神，鼓舞着我们，使我们在整个历史中保持强大。直到今天，这种精神贯穿于每个爱国的美国人的血脉中，存在于每一个人身上。
</p>
<p>
 今天，就像243年前一样，美国自由的未来掌握在愿意捍卫她的男人和女人的肩上。
</p>
<p>
 只要我们忠于起源，只要我们记得伟大的历史，只要我们永不停止为更美好的未来奋斗，就没有美国成不了的事。
</p>
<h4>
 国庆游行和烟火表演
</h4>
<p>
 每年的这一天，首都华盛顿都会吸引数十万人到国家大草坪欣赏音乐会和烟花表演。
</p>
<p>
 华盛顿DC的独立日国庆游行和烟火表演，每年都是全美电视转播的重头戏；今年的系列庆祝活动版本更是众所瞩目的焦点。
</p>
<p>
 首先在上午11:45，于华盛顿特区宪法大道7街展开的游行，将一路走到第17街结束，全程约2小时15分钟。官方估计今年有数十万名观众在场观看。基于安全原因，警方在上午10:00关闭大部分国家广场区域，不过仍允许公众进入观赏游行乐队、花车和要经过的舞蹈团。
</p>
<p>
</p>
<p>
 这次参加游行的队伍相当丰富，除了美国陆军乐队、老卫队鼓乐队外，还有民间团体多种乐队、横笛和鼓乐队，五颜六色的花车、巨型气球、挥舞红白蓝旗帜的舞旗队等，每个州都至少有一个游行队伍。
</p>
<p>
 这次参与“向美国致敬”阅兵典礼的主战坦克及其它战车，由于其重量可能会压坏街道路面，所以不参加游行。
</p>
<figure class="wp-caption aligncenter" id="attachment_11364919" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1IB6R3.jpg">
  <img alt="" class="size-large wp-image-11364919" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1IB6R3-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  独立日大游行。 (Photo by Armend NIMANI/AFP)
 </figcaption><br/>
</figure><br/>
<h4>
 下午4:30
 <strong>
  数十年来 美国首次面对公众演讲
 </strong>
</h4>
<p>
 川普在林肯纪念堂的讲话的主题是“向美国致敬”，两辆布拉德利战车将停放在总统的两侧，两辆60吨级艾布拉姆斯战斗坦克位于华盛顿国家广场或附近。
</p>
<p>
 政府官员表示，今年的展示将大约是去年的两倍，并包括一些新的元素，如一面巨大的美国国旗和天空中拼写出的“美国”字样等。
</p>
<h4>
 下午1:02
 <strong>
  大游行
 </strong>
</h4>
<p>
 国家独立日游行现在正在华盛顿特区举行。
</p>
<p>
 根据大游行网站信息，有一些关于这个历史性游行的有趣的事情：
</p>
<p>
 由国家公园管理局共同主办。
 <br/>
 游行路线长约一英里，沿宪法大道延伸。
 <br/>
 根据每个州州长办公室的建议邀请参加的乐队。
 <br/>
 对于乐队和其它考虑参加游行的团体，必须填写完整的申请表格，包括录像、传记和奖励。
 <br/>
 目标是在游行的50个州中至少有一个乐队。
</p>
<h4>
 下午12:10
 <strong>
  首都机场暂时停飞
 </strong>
</h4>
<p>
 美国联邦航空管理局周二宣布，罗纳德里根华盛顿国家机场的运营将于周四下午6:15到7:45分暂停，以便于军用飞机表演。
</p>
<p>
 跑道也将于晚上9点关闭，直到9:45，以方便年度烟花汇演。
</p>
<figure class="wp-caption aligncenter" id="attachment_11364920" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1IB6Y8.jpg">
  <img alt="" class="size-large wp-image-11364920" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1IB6Y8-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  独立日大游行。(Photo by Armend NIMANI/AFP)
 </figcaption><br/>
</figure><br/>
<h4>
 AM11:00
</h4>
<p>
 今年的庆祝活动将在美国国家广场上举行首次阅兵式。届时将展示美军最新武器，比如主战战车M1A2艾布拉姆斯坦克、布雷德利步兵战车（Bradley Fighting Vehicles）和M88装甲救济车（Armored Recovery Vehicles）。
</p>
<p>
 此外，还有空军一号（VC-25）、美国海军的蓝色天使（Blue Angels）特技飞行队、B-2幽灵隐形轰炸机（B-2 Spirit bomber）、F-22猛禽隐形战机（F-22 Raptors）、F-35闪电II隐形战机（F-35 Lightning II Joint Strike Fighters）和海军陆战队直升机中队也将飞越阅兵场。
</p>
<p>
 据悉，海军陆战队的主要突击运输机新型鱼鹰式倾斜旋翼机（MV-22 Ospreys）以及尚未服役的陆战队版未来总统专机“陆战队一号”（Marine One）也将亮相。
</p>
<figure class="wp-caption aligncenter" id="attachment_11364922" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1IC0Y3.jpg">
  <img alt="" class="size-large wp-image-11364922" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1IC0Y3-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  独立日大游行。(Photo by SAUL LOEB / AFP)
 </figcaption><br/>
</figure><br/>
<h4>
 AM10:30 美国宇航员庆祝独立日
</h4>
<p>
 美国宇航局宇航员Christina Koch和Nick Hague目前正在国际空间站上，担任轨道太空实验室探险队60号的飞行工程师。他们表示：“当我们在上空环绕我们的星球时，我们想花一点时间，祝愿所有在家里的美国人，以及全世界7月4日非常开心。”
</p>
<p>
 “我们的旗帜（美国国旗）已经与人类和机器人一起，旅行到太阳系最远的地方，美国人的聪明才智通过勇气、决心和想像力抵达可能的（太阳系）边界。”
</p>
<p>
</p>
<blockquote class="twitter-tweet" data-lang="en">
 <p dir="ltr" lang="en">
  It’s a star spangled space station! Wherever you may be celebrating,
  <span href="https://twitter.com/Astro_Christina?ref_src=twsrc%5Etfw">
   @Astro_Christina
  </span>
  and
  <span href="https://twitter.com/AstroHague?ref_src=twsrc%5Etfw">
   @AstroHague
  </span>
  send Independence Day greetings on behalf of the crew aboard the orbiting laboratory, 250 miles above the planet we call home. Happy
  <span href="https://twitter.com/hashtag/4thOfJuly?src=hash&amp;ref_src=twsrc%5Etfw">
   #4thOfJuly
  </span>
  !
  <span href="https://t.co/d12rLUyatO">
   pic.twitter.com/d12rLUyatO
  </span>
 </p>
 <p>
  — Intl. Space Station (@Space_Station)
  <span href="https://twitter.com/Space_Station/status/1146433419443707904?ref_src=twsrc%5Etfw">
   July 3, 2019
  </span>
 </p>
</blockquote>
<p>
 <p>
 </p>
 <h4>
  10:00AM
 </h4>
 <h3>
  2019年国庆日活动一览
 </h3>
 <p>
  <strong>
   1. 国家独立日游行
  </strong>
 </p>
 <p>
  时间：上午11:45—下午2点。
 </p>
 <p>
  地点：宪法大道（Constitution Avenue）7街靠西北方向出发，到第17街结束。
 </p>
 <p>
  游行包括：军乐队、花车、巨型气球、骑兵、爱国演习队、旗队等。
 </p>
 <p>
  <strong>
   2. 向美国致敬活动
  </strong>
 </p>
 <p>
  时间：下午6:30—晚上7:30。下午3点开放入场。
 </p>
 <p>
  地点：林肯纪念堂。
 </p>
 <p>
  总统川普及第一夫人梅拉妮亚将出席活动，川普将致词。有现场音乐表演。
 </p>
 <p>
  <strong>
   3. 国会大厦独立日音乐会
  </strong>
 </p>
 <p>
  时间：晚上8:00—晚上9:30。下午3点开放入场。
 </p>
 <p>
  地点：美国国会大厦西边草坪。
 </p>
 <p>
  由国家公园管理局和国家交响乐团共同举办，主持人为约翰·斯塔莫斯（John Stamos），邀请来宾包括：格莱美奖音乐传奇人物卡洛尔·金（Carole King）、白金唱片演唱家凡妮莎·威廉斯（Vanessa Williams）、获奖创作歌手蔻比·凯蕾（Colbie Caillat）、国家交响乐团等。
 </p>
 <p>
  <strong>
   4. 烟火秀
  </strong>
 </p>
 <p>
  时间：晚上9:07 —晚上9:42。
 </p>
 <p>
  独立日庆祝活动将以壮观的烟火表演划下句号。烟火将从西波托马克公园（West Potomac Park）和林肯纪念堂（Lincoln Memorial）后面发射。民众可以在华盛顿DC和北弗吉尼亚州的各个地方观赏。
 </p>
 <p>
  活动信息，比如安全规则、道路封闭消息和违禁物品等，可至官方网站
  <span href="https://is.gd/A21wmS">
   https://is.gd/A21wmS
  </span>
  查阅。#
 </p>
 <p>
  责任编辑：林妍
 </p>
</p>
<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11364638.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11364638.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11364638.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/4/n11364638.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

