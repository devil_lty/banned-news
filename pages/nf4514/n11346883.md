### 逾千港人到G20国领事馆请愿 促施压中共
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/20190626-hsichiao-hk-01-600x400.jpg"/>
<div class="red16 caption">
 <p>
  约1,500名市民，趁日本大阪召开G20峰会前夕，前往19个驻港总领事馆递信请愿，促请各国于峰会上就香港现况向中国施压。（李逸／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月26日讯】（大纪元记者林怡香港报导）约1,500名香港市民响应网民号召，趁日本大阪召开
 <span href="http://www.epochtimes.com/gb/tag/g20%E5%B3%B0%E4%BC%9A.html">
  G20峰会
 </span>
 前夕，前往19个
 <span href="http://www.epochtimes.com/gb/tag/%E9%A9%BB%E6%B8%AF%E6%80%BB%E9%A2%86%E4%BA%8B%E9%A6%86.html">
  驻港总领事馆
 </span>
 递信请愿，促请各国于峰会上就香港现况向中国（中共）施压。
</p>
<p>
 参加活动的香港市民早上在中环遮打花园集合后出发，部分人戴上口罩，手持自制标语，没有喊口号。他们先游行到美国驻港澳总领事馆，一名代表用英文宣读请愿信，有部分市民在门口高喊“Free Hong Kong”、“Help Hong Kong”、“We Need Help”等口号。美国驻港澳总领事馆派代表接收了请愿信，领事馆外有逾30名警员驻守。
</p>
<figure class="wp-caption aligncenter" id="attachment_11346900" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/20190626-hsichiao-hk-03.jpg">
  <img alt="" class="size-large wp-image-11346900" src="http://i.epochtimes.com/assets/uploads/2019/06/20190626-hsichiao-hk-03-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  美国驻港澳总领事馆派代表接收香港市民的请愿信。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 刚从德国公干回港的陈先生，带同自制德文纸牌标语参与游行。他表示，香港两次过百万游行时他都在德国公干，很惊讶当地的头条新闻就是香港百万人游行。他说之前错失几次上街的机会，今次看到网上呼吁便前来参加活动，希望能尽一份绵力，“过去几个月德国政府似乎对香港事务高调很多，我觉得德国政府应会帮香港人，因为此事都会影响到香港的德国侨民的权益。我自己都有做德国生意，德国厂商在国内的投资都比较多⋯⋯我很开心除了英美国家关心香港外，德国都开始关心香港。”
</p>
<p>
 现场有不少年轻学子参与游行。刚从大学毕业的吕小姐表示，走出来是希望各国领事能
 <span href="http://www.epochtimes.com/gb/tag/%E5%A3%B0%E6%8F%B4%E6%B8%AF%E4%BA%BA.html">
  声援港人
 </span>
 “
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”的诉求。
</p>
<p>
 游行队伍接着到欧盟驻港澳办事处，同样读出请愿信，并高呼口号“Save Hong Kong”。其后转到英国驻香港总领事馆时，由英国驻港总领馆副总领事彭雅慧（Esther Blythe）接收请愿信。
</p>
<figure class="wp-caption aligncenter" id="attachment_11346904" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/20190626-hsichiao-hk-04.jpg">
  <img alt="" class="size-large wp-image-11346904" src="http://i.epochtimes.com/assets/uploads/2019/06/20190626-hsichiao-hk-04-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  英国驻港总领馆副总领事彭雅慧（Esther Blythe）接收请愿信。（李逸／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 之后，游行队伍“兵分三路”到湾仔、中环一带的十多个领事馆请愿、递信。在湾仔中环广场，有南非共和国、沙特阿拉伯、意大利三国的总领事馆设于此处，他们都有代表会见集会人士，并接收信件，获全场鼓掌，并有人大喊“Thank you”。
</p>
<p>
 当中南非共和国领事馆总领事倪清阁（Madoda Ntshinga）表示，会将信件转交给南非总统。另外，加拿大及日本驻港领事馆都有派代表接信，加拿大领事耐心地聆听代表读出的陈词，态度友善，但未有表态。
</p>
<p>
 游行发起人之一代表刘先生表示，今次活动有逾1,500人参加，他表示非常感动，同时呼吁参与游行的市民今晚出席由民阵在爱丁堡广场举办的晚会。
</p>
<p>
 刘先生指，《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》修订争议现时已获得国际广泛关注，今次游行是希望出席峰会的国家能向中方提出“香港问题”并施压，借此保障香港的营商、司法自由。#
</p>
<p>
 责任编辑：昌英
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11346883.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11346883.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11346883.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/26/n11346883.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

