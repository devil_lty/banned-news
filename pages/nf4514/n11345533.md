### 猪肉价格续飞涨 大陆5月进口肉类创纪录
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2011/10/1110140616511459-600x400.jpg"/>
<div class="red16 caption">
 <p>
  非洲猪瘟疫情肆虐整个大陆，导致生猪减少、猪肉价格大涨，5月进口包括猪肉在内的肉类数量创下新高。(TEH ENG KOON/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月25日讯】（大纪元记者周心鉴综合报导）大陆
 <span href="http://www.epochtimes.com/gb/tag/%E9%9D%9E%E6%B4%B2%E7%8C%AA%E7%98%9F.html">
  非洲猪瘟
 </span>
 疫情持续肆虐，导致生猪减少、
 <span href="http://www.epochtimes.com/gb/tag/%E7%8C%AA%E8%82%89%E4%BB%B7%E6%A0%BC.html">
  猪肉价格
 </span>
 大涨，5月中国进口的包括猪肉在内的肉类数量创下新高。截至6月24日，A股猪肉概念板块今年已经累积上涨73.71%。
</p>
<h4>
 6月猪肉出厂价同比涨43.7%
</h4>
<p>
 据中共农业农村部最新数据，2019年6月，16省瘦肉型白条猪肉出厂价为每公斤20.26元（人民币，下同），较去年同比上涨43.7%；5月
 <span href="http://www.epochtimes.com/gb/tag/%E7%8C%AA%E8%82%89%E4%BB%B7%E6%A0%BC.html">
  猪肉价格
 </span>
 涨幅较大，全国集贸市场猪肉月均价为每公斤24.71元。
</p>
<p>
 另据中共国家统计局发布的数据，截至6月24日14时，全国农产品批发市场猪肉平均价格为22.63元/公斤，比上周五上涨2.6%。
</p>
<p>
 猪肉价格上涨的背后是生猪供应的下滑。因
 <span href="http://www.epochtimes.com/gb/tag/%E9%9D%9E%E6%B4%B2%E7%8C%AA%E7%98%9F.html">
  非洲猪瘟
 </span>
 疫情影响，导致生猪减少、猪肉价格快速上涨。
</p>
<h4>
 非洲猪瘟持续肆虐
</h4>
<p>
 目前非洲猪瘟疫情在大陆持续扩展。尽管中共农业农村部6月24日声称，非洲猪瘟疫情得到有效控制，但贵州省仅仅在6月份，官方就发布4起非洲猪瘟疫情：平塘县落良村和乐阳村发生的两起疫情；三都县连心村和姑引村发生两起瘟疫情。共有157头生猪发病，118头生猪死亡。
</p>
<p>
 去年8月爆发非洲猪瘟后，短短数月内，疫情已传遍大陆31个省市自治区。
</p>
<h4>
 非洲猪瘟重创养猪业
</h4>
<p>
 受非洲猪瘟疫情的影响，整个生猪养殖行业加速去产能明显。据中共农业农村部6月12日公布的“2019年5月份400个监测县生猪存栏信息”显示，5月份生猪存栏环比下降4.2%，比去年同期下降22.9%；能繁母猪（指产过一胎仔猪、能够继续正常繁殖的母猪）环比下降4.1%，比去年同期下降23.9%。
</p>
<p>
 而大陆行业资深专家、“老徐聊猪市”创始人徐青松对“界面新闻”表示，“根据目前我们监测的数据显示，本轮非洲猪瘟，导致养殖户数同比下降47%，母猪存栏数同比下降34%。”
</p>
<h4>
 前5个月进口肉量创纪录
</h4>
<p>
 彭博社24日报导，中国为应对非洲猪瘟造成的价格上涨，在今年前5个月包括牛、鸡、猪等的肉类进口量创纪录。中国23日的官方海关数据显示，其5月份的肉和内脏购买量比去年增加约45%，至55万6276吨；前5个月的进口总量比去年增长23%，至220万吨。
</p>
<p>
 数据显示，猪肉进口量在所有肉类中增涨最快，5月猪肉进口量为18.74万吨，按年飙63%；羊肉4.2万吨，按年大升53%；牛肉亦增41%，达12.37万吨；冰鲜鸡升26%，达6.34万吨。
</p>
<p>
 各种肉类进口量大增，主因是猪肉价格大升，令消费者转食其它肉类，继而刺激进口一同增长。
</p>
<p>
 值得注意的是，由于是夏季关系，人们的饮食相对清淡，为猪肉需求淡季，但猪肉价格依然上扬。换言之，到了夏季结束，今年底猪肉价格将升得更高。
</p>
<h4>
 今年过年猪肉恐现“天价”
</h4>
<p>
 受非洲猪瘟疫情的影响，目前整体养殖户补栏比较谨慎。中共农业部门此前曾对1,500户养猪场进行问卷调查，结果显示，因疫情风险大而明确选择不补栏的养殖户占55%，犹豫观望的占22%，打算补栏的仅占18%。
</p>
<p>
 中国生猪预警网首席分析师冯永辉对《北京商报》表示，生猪价格有可能今年三季度就会突破历史高点。
</p>
<p>
 在冯永辉看来，影响猪肉价格最关键的因素就是要恢复产能，而恢复产能则需要非洲猪瘟疫苗的“支撑”。乐观地说，如果今年年底非洲猪瘟疫苗能够研制出来，那么也需要一年的时间来恢复产能。因此，今年过年猪肉可能会是一个“天价”。
</p>
<p>
 在涨价预期下，猪肉概念股也站在了风口上。截至6月24日，A股猪肉概念板块今年已经累积上涨73.71%，大盘猪肉股温氏股份今年以来股价累计上涨43.93%，牧原股份累积上涨108.70％；中小盘猪肉股正邦科技今年累积上涨248.31%，天邦股份累积上涨91.18%。
</p>
<p>
 另据中共农业农村部预测，大陆全年猪肉产量会下降，后期生猪供应会趋紧，四季度活猪价格将突破2016年历史高点。#
</p>
<p>
 责任编辑：高静
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11345533.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11345533.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11345533.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/25/n11345533.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

