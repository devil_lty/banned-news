### 曾要求官员公示财产 张宝成再遭构陷被捕
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/2-21-589x400.jpg"/>
<div class="red16 caption">
 北京新公民运动的积极推行者、人权活动人士张宝成，被以涉嫌宣扬恐怖主义等罪名逮捕，他的妻子刘玨帆谴责中共故意陷害。（推特图片）
</div>
<hr/><p>
 【大纪元2019年07月11日讯】（大纪元记者洪宁采访报导）中共建党七十周年前夕，对
 <span href="http://www.epochtimes.com/gb/tag/%E5%BC%82%E8%A7%81%E4%BA%BA%E5%A3%AB.html">
  异见人士
 </span>
 及访民的打压明显升级。北京新公民运动的积极推行者、要求官员公示财产的人权活动人士
 <span href="http://www.epochtimes.com/gb/tag/%E5%BC%A0%E5%AE%9D%E6%88%90.html">
  张宝成
 </span>
 ，被以涉嫌“宣扬恐怖主义”等罪名逮捕，他的妻子刘玨帆谴责中共故意陷害。
</p>
<h4>
 刘玨帆：当局构陷罪名欲重判
 <span href="http://www.epochtimes.com/gb/tag/%E5%BC%A0%E5%AE%9D%E6%88%90.html">
  张宝成
 </span>
</h4>
<p>
 张宝成妻子、现在美国的刘玨帆女士接受大纪元采访时说，在大陆的女儿于前几日收到当局对张宝成的逮捕证，上面写着莫须有的罪名：涉嫌寻衅滋事罪，宣扬恐怖主义、极端主义、煽动实施恐怖活动罪，于2019年7月4日批准逮捕。
</p>
<p>
 张宝成与六四前5月28日，被当局诬陷涉枪抓走，刘玨帆一直都觉得敏感日过后就会放人，如今他又被罗织多项罪名，刘玨帆气愤地说：“这些罪名非常夸张，共匪故意制造假象想重判刑张宝成。”
</p>
<p>
 律师曾两次去看守所会见张，第二次会见时，张宝成说他被严管，原因是帮助生病的狱友
 <span href="http://www.epochtimes.com/gb/tag/%E7%BB%B4%E6%9D%83.html">
  维权
 </span>
 。
</p>
<p>
 张宝成经常声援共产党眼中的敏感人物，如黄琦母亲、余文生律师妻子许艳、王全璋妻子李文足等。
</p>
<p>
 刘玨帆说，丈夫经常讲一句话：“反共是做人的底线，不反共就不是人。” “所有的自由与空间都是大家争取来的，我们不能因为坐过牢，受过迫害，我就闭嘴，不行！必须我要来做，我要是不做，以后就要由我的孩子来流血牺牲，不可以。”
</p>
<p>
 2013年，张宝成参与“新公民运动”要求官员公示财产，被判2年冤狱，2015年出狱后，一直没停止抗争活动。2018年3月至今年3月，张宝成被北京警方约谈过二十多次。
</p>
<p>
 当局搞株连，给他女儿的学校施加压力，导致女儿精神负担过重辍学一年，精神面临崩溃。
</p>
<p>
 2017年10月，张宝成与妻子女儿准备前往美国，为女儿考察学校，但被当局禁止出境，无奈，妻子带着女儿留在了美国。
</p>
<p>
 后来，张宝成被允许出境去美，但他最终还是选择返回大陆，他对妻子说：“我们一起坐牢的那么多生死兄弟都没有出来，我也不能留在这里。”“共产党在大陆，14亿同胞还在共产党的魔爪下，我不能自己出来啊。”
</p>
<p>
 刘玨帆还透露，以前，张宝成被关押期间，遭受过多种酷刑。如：上背铐；抖铐，“能让人疼到骨头里”；竹板打脚心，打到竹板都劈了，整条腿肿到大腿根上；全口牙齿被警察用拳头打得都松动；警察用凉水泼人后，用电棍电生殖器。
</p>
<p>
 许多声援张宝成的网民谴责中共：“中共是恐怖主义政权，它不但在国内恐吓、折磨、迫害异议人士，还株连九族，恐吓在国外的亲友，中共的恐怖主义活动早已从国内延伸到世界的各个角落。”
</p>
<blockquote class="twitter-tweet" data-lang="zh-tw">
 <p dir="ltr" lang="zh">
  北京公安是最大的黑恶势力！
  <br/>
  栽脏构陷人权活动家张宝成。中共是要把所有的良心人士关进监狱！
  <span href="https://t.co/3DF17wMRek">
   pic.twitter.com/3DF17wMRek
  </span>
 </p>
 <p>
  — 无欲则刚 (@liu_juefan)
  <span href="https://twitter.com/liu_juefan/status/1148602113775284227?ref_src=twsrc%5Etfw">
   2019年7月9日
  </span>
 </p>
</blockquote>
<p>
</p>
<p>
 另外，眼下正处于中共建党七十周年前夕，当局明显开始加紧打压异见、
 <span href="http://www.epochtimes.com/gb/tag/%E7%BB%B4%E6%9D%83.html">
  维权
 </span>
 人士。
</p>
<h4>
 当局对黄琦母亲的看管升级
</h4>
<p>
 知情人向记者说，被软禁在家的黄琦母亲蒲文清女士，近日得知中央巡视组到四川绵阳中院的消息，想去为儿子反映问题，但仍遭到当局阻止。绵阳警方又在老人家旁边租了一套房子住下，对她加强监控。蒲妈妈家里有人三班倒看管她。
</p>
<p>
 知情人说，张宝成被抓后，蒲妈妈着急，压力很大，为张宝成担心。此前她去北京为儿子伸冤，张宝成曾帮助过她。
</p>
<h4>
 沈良庆拒绝官派律师
</h4>
<p>
 此外记者获悉，安徽著名异议人士、前检察官沈良庆于6月遭安徽当局以涉嫌“寻衅滋事罪”逮捕。家属透露，7月8日，律师会见沈良庆得知，国保曾要求沈良庆解除家属聘请的律师，用官派律师辩护，但遭到沈良庆拒绝。#
</p>
<p>
 责任编辑：高静
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11377759.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11377759.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11377759.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/11/n11377759.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

