### 虫害继续蔓延 草地贪夜蛾侵大陆19省
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-954036218-600x400.jpg"/>
<div class="red16 caption">
 <p>
  “粮食杀手”草地贪夜蛾目前已经扩散至大陆19个省1000多个县。图为示意图。  (SIMON MAINA/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月26日讯】（大纪元记者李心茹报导）
 <span href="http://www.epochtimes.com/gb/tag/%E8%8D%89%E5%9C%B0%E8%B4%AA%E5%A4%9C%E8%9B%BE.html">
  草地贪夜蛾
 </span>
 是暴食玉米的重大迁飞性
 <span href="http://www.epochtimes.com/gb/tag/%E5%AE%B3%E8%99%AB.html">
  害虫
 </span>
 ，自今年1月入侵云南后，已快速蔓延至大陆19个省，农作物受害面积达500余万亩。当局拨5亿元人民币防治，声称“虫口夺粮”。
</p>
<p>
 中共农业农村部26日举行新闻发布会，介绍今年保障粮食安全和农产品供应的情况。种植业管理司司长潘文博称旱涝灾害和虫害是今年影响秋粮收成的两大灾害因素。
</p>
<p>
 旱涝灾害方面，大陆已经进入主汛期，气象部门预测，在今年汛期期间降雨呈“南多北少”分布，极端天气偏多，尤其是东北地区出现夏伏旱，前期降雨较多，后期干旱的概率增加。南方局部地区则可能发生洪涝灾害。
</p>
<p>
 至于虫害方面，根据目前监测，稻飞虱、粘虫、稻瘟病病虫偏重发生概率大于常年，特别是
 <span href="http://www.epochtimes.com/gb/tag/%E8%8D%89%E5%9C%B0%E8%B4%AA%E5%A4%9C%E8%9B%BE.html">
  草地贪夜蛾
 </span>
 ，对粮食生产构成新的威胁。
</p>
<p>
 潘文博声称，中共农业农村部针对草地贪夜蛾防控，已启动了应急防治用药机制，公布了25种防治用药名录，会同财政部下拨5亿元人民币，欲“虫口夺粮”。
</p>
<p>
 不过，草地贪夜蛾目前几乎没有天敌，几十年来从美洲出发跨越各大洲，经历过数十个国家的几十种农药的打击，对很多农药已经有了抗性，非常不容易防治。2018年草地贪夜蛾在非洲造成的经济损失高达30亿美元。
</p>
<h4>
 “粮食杀手”威胁玉米产区
</h4>
<p>
 草地贪夜蛾是暴食玉米的重大
 <span href="http://www.epochtimes.com/gb/tag/%E5%AE%B3%E8%99%AB.html">
  害虫
 </span>
 ，具有超强的飞行能力，自今年1月入侵云南后，已快速蔓延至国内19个省的1,000多个县、市、区，农田受害面积达500多万亩。
</p>
<p>
 草地贪夜蛾起源于美洲，又称
 <span href="http://www.epochtimes.com/gb/tag/%E7%A7%8B%E8%A1%8C%E5%86%9B%E8%99%AB.html">
  秋行军虫
 </span>
 ，具有迁飞扩散快、繁殖能力强、暴食危害和几乎无法根治的特点。草地贪夜蛾被认为是玉米杀手，它孵化的幼虫喜食玉米，受害后能使玉米减产高达50%，严重时可造成绝收。
</p>
<p>
 联合国粮食及农业组织（UNFAO） 2018年8月发出全球预警，称草地贪夜蛾是重要的农业害虫，表示其“分布太广，数量太多，无法（从其广泛入侵的地区）消灭”。
</p>
<p>
 美国农业部曾发布一份报告指出，草地贪夜蛾对各种作物而言都是场重大灾难，给玉米等重要农作物带来严重影响，且目前尚未发现大规模扑杀这种害虫的方法，一旦遭入侵几乎无法根治。
</p>
<p>
 中国是全球第二大玉米生产国和消费国，玉米生产量在谷物生产中占比是最大的，达到42%，玉米主要产区集中在东北地区。
</p>
<p>
 大陆专家分析，7至8月是西南季候风最强的季节，具有超强的飞行能力的草地贪夜蛾，将随季候风向北迁徙，经过黄淮、华北乃至东北和西北地区。期间黄淮海地区玉米将进入生长旺季，将对玉米产量构成极大的威胁。
</p>
<h4>
 今年玉米供需缺口达2570万吨
</h4>
<p>
 除了虫害和洪涝灾害，因种地的化肥、农药、收割机等费用持续不断上涨，大陆农民种地的意愿不断降低。今年的玉米种植面积继续减少，总体连续第三年下降。
</p>
<p>
 据中共官方发布的《2019年5月中国农产品供需形势分析》显示，今年玉米供需缺口达2,570万吨。而据美国农业部消息，这一缺口在未来两年内料将扩大。
</p>
<p>
 目前，中美贸易战的紧张局势已限制了中国从美国进口玉米，北京已对美国的玉米加征了高达90%的关税。
</p>
<p>
 在中共限制进口玉米、国内产量下降的情况下，中国的玉米价格飘升至4年来高点。
</p>
<p>
 如今，草地贪夜蛾的侵入，让大陆玉米的紧缺雪上加霜。#
</p>
<p>
 责任编辑：林诗远
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11347684.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11347684.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11347684.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/26/n11347684.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

