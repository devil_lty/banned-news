### 川普：不会取消2500亿中国商品25%关税
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/000_1I189L-600x400.jpg"/>
<div class="red16 caption">
 <p>
  周日（6月30日）上午，川普总统向韩国商业领袖讲话。他表示，在达成美中贸易协议之前，不会取消已经施加的对华关税。(Brendan Smialowski / AFP)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月30日讯】（大纪元记者张婷综合报导）周日（6月30日）上午，川普在一个活动上向韩国商业领袖发表讲话时证实，在新的贸易协议达成之前，已经实施的对华2500亿美元中国商品关税不会去除。中方一直在寻求美方取消这些关税。川普的这次讲话，对外明确了美方的态度。
</p>
<p>
 川普周六在商业领袖会上感慨表示，自己做总统已经两年半了，“时光飞逝”。
</p>
<p>
 “今天，我非常荣幸能够加入这个才华横溢的（商业）领袖群体……伟大的领袖们，伟大的商业人士。”川普说。
</p>
<figure class="wp-caption aligncenter" id="attachment_11355633" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/000_1I189P-e1561909034874.jpg">
  <img alt="" class="size-large wp-image-11355633" src="http://i.epochtimes.com/assets/uploads/2019/06/000_1I189P-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  6月30日，川普总统向韩国商业领袖发表讲话，并合影留念。(Brendan Smialowski / AFP)
 </figcaption><br/>
</figure><br/>
<p>
 他还说，你们来自美国和韩国的公司，来这里讨论美韩之间重要的经济关系。许多公司，包括韩国公司，正在从世界各地来美。特别是汽车公司，他们要去密歇根州、俄亥俄州、北卡罗来纳州和佛罗里达州和宾夕法尼亚州投资。
</p>
<p>
 川普说，美国的经济增长现已超过3%。第一季度为3.2%。他称赞韩国企业在美投资。“乐天集团最近在路易斯安那州投资31亿美元。”川普说。
</p>
<h4>
 不达成协议不会撤除对华关税
</h4>
<p>
 川普在这次商业领袖会上还谈到了周六与中国的贸易谈判以及备受关注的
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E4%B9%A0%E4%BC%9A.html">
  川习会
 </span>
 。“我们（川习）举行了一次非常好的会面。真的非常好的会面。因此，我们将会继续就贸易协议进行讨论。我们将会看看什么会发生。”川普说，“一个好机会。”
</p>
<p>
 “我们正从中国获得巨额商品关税：2500亿美元（中国商品的）25%关税。”川普说，“在我们的协议下，这（对华关税）将会保留直到我们达成新协议。之后，我们将看看会发生什么。”
</p>
<p>
 当地时间周日早上（美东周六傍晚），川普也发推文表示：“再次与中方合作，我们与他们的关系仍然非常好。对我而言。交易的质量比速度更重要。我并不着急，但事情看起来非常好！ 目前向中国收取的关税不会减少。”
</p>
<p>
 “我们原本非常接近与中方达成一个协议。但不幸的是，一些事情发生了，协议没有达成。但我们现在重新回到正轨上。我们将看看会发生什么。现在，我们将会重启谈判。我们（川习）举行了一个非常好的会议。昨天我们真的举行了一个非常棒的会议。我非常喜欢习主席，我把他当成一个朋友……我非常了解他。”川普周日在韩国表示。
</p>
<p>
 周六（6月29日），川普与习近平举行了大约80分钟的会谈。美中双方同意从谈判破局的地方开始继续谈判，看看是否能够达成协议。从破局之处开始谈也是美方一直提出的要求。美方不会答应中共企图重新谈判的要求。
</p>
<p>
 此外，中方在这次
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E4%B9%A0%E4%BC%9A.html">
  川习会
 </span>
 上承诺购买美方大量的食品和农产品。“他们将会很快开始行动，几乎是立即的。” 川普说，美国农民将会是最大受益者。
</p>
<p>
 作为交换，美方目前不会对对剩下的3250亿美元的中国商品加征关税。
</p>
<p>
 自去年7月美中贸易关税战开打后到目前为止，美国已经对总计2,500亿美元中国商品加征25%惩罚性关税，中方则对1,100亿美元美国商品课征5%到25%不等的报复性关税。北京一直要求美方应取消所有惩罚性关税。而川普总统周日在商业领袖会上的讲话很明确，在新的贸易协议达成之前，美方不会取消这些关税。即使是达成协议，美方也要看看如何处理。
</p>
<h4>
 川普最喜欢的一个词：“对等”
</h4>
<p>
 除了中美贸易外，川普还向商业领袖们表示，在20国集团（G20）峰会期间，他向与会者分享了那些有助于美国取得非凡经济成功的政策，并“呼吁所有G20会员在公平和对等原则的基础上进行合作”。
</p>
<p>
 “这么重要的一个词。也许所有词汇中我最喜欢的一个就是‘对等’。”川普说。
</p>
<p>
 川普表示，美国和韩国在减少贸易不平衡，以及为两国带来新的繁荣方面取得了很大进展。
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11355616.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11355616.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11355616.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/30/n11355616.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

