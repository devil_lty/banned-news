### 【不断更新】港人七一再上街促撤恶法
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010313381366-600x400.jpg"/>
<div class="red16 caption">
 <p>
  2019年7月1日，香港七一大游行龙头从维多利亚公园起步。领头的举行向天横幅写有“撤回恶法 林郑下台”的游行诉求。（余钢／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月01日讯】（大纪元香港记者站报导）2019年7月1日是香港主权移交22周年。每年七一这天，香港都会举行七一大游行，成为港人抗衡
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1%E4%BE%B5%E8%9A%80.html">
  中共侵蚀
 </span>
 、追求民主自由的象征。今年正值全球瞩目的“
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
  反送中
 </span>
 ”（反对《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》修订）抗议浪潮，主办方民间人权阵线（民阵）定出今年七一游行几大主题：“撤回恶法 林郑下台”；“重启政改、释放所有政治犯”；“撤销暴动定性 彻查6‧12镇压”。游行于下午2时半在铜锣湾维园集合，3时起步，游行至金钟政府总部。
</p>
<p>
 过去一段日子，香港人经历了“最漫长的6月”，反对中共和港府强推引渡恶法、欲将人强行遣送大陆受审的运动浪潮，震惊国际。6月4日六四30周年18万人烛光集会、6月9日103万白衣人大游行、6月12日金钟立法会外的大冲突、6月16日200万人黑衣大游行、6月26日民阵集会向G20喊话，以至民间自发马拉松式向领事馆递信，在全球各国报章登全版广告，运动风起云涌。
</p>
<p>
 这场运动中，香港年轻人也首次尝到了橡胶子弹、布袋弹的开枪武力镇压。而恶法至今未撤回，特首林郑月娥仍未下台。今早，主权移交22年的七一升旗礼严密布防，警方再次出动了警棍、胡椒喷雾武力驱赶示威市民。而七一的下午，香港人再一次走上街头，向中共强权说不，向恶法说不，受到全球媒体的密切聚焦。
</p>
<p>
</p>
<p>
 <strong>
  16:20
 </strong>
</p>
<p>
 下午约4时，七一大游行队头已到金钟太古广场。民阵早前将游行终点由原来的政府总部改为中环遮打道，有部分市民按民阵建议，继续游行往中环；亦有市民选择按原本往金钟的路线，向政府总部前进。而在起点维园，目前仍有市民等待出发。有人在酷热天气下不适，需救护员协助。
</p>
<figure class="wp-caption aligncenter" id="attachment_11357051" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010432161366.jpg">
  <img alt="" class="size-large wp-image-11357051" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010432161366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民举起不同的横额，除了强调学生在6.12包围立法会的行动中没有最户外，又批评警察使用暴力清场，及多项不当行为，包括瞄准市民的头部开枪，以催泪弹围困市民，没有配戴委任证等。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11357056" style="width: 400px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010432191366.jpg">
  <img alt="" class="size-large wp-image-11357056" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010432191366.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  因应市民在“
  <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
   反送中
  </span>
  ”的市民堕楼死亡，有市民拉起“一个都不能少”的大型标语。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  15:50
 </strong>
</p>
<p>
 除了游行路线的轩尼诗道全线走满游行人士，铜锣湾一带的大街小巷，包括希慎广场、利园山道，波斯富街都挤满了人。不少游行人士手持印刷和自制的示威标语、展板、横幅，除了表达中心诉求：“全面撤回送中恶法”、“林郑下台”、“追究警察暴行 反对定性暴动”；也有勉励香港人的说话：“香港加油”、“选择站在良知一边”、“撑下去”、“战斗到底”等。
</p>
<p>
 也有人用各国的语言，写上“反对将绑架到中国合法化”、“停止射击香港人”等口号，向关注香港游行的国际社会发声。另外，当游行队头经过中共喉舌媒体《大公报》门外，示威人士报以嘘声。
</p>
<figure class="wp-caption aligncenter" id="attachment_11356951" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010402491366.jpg">
  <img alt="" class="size-large wp-image-11356951" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010402491366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  铜锣湾轩尼诗道希慎外的天桥。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356905" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010351031366.jpg">
  <img alt="" class="size-large wp-image-11356905" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010351031366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  除了游行路线的轩尼诗道全线走满游行人士，铜锣湾一带的大街小巷，包括希慎广场、利园山道，波斯富街都挤满了人。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356918" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010351011366.jpg">
  <img alt="" class="size-large wp-image-11356918" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010351011366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  除了游行路线的轩尼诗道全线走满游行人士，铜锣湾一带的大街小巷，包括希慎广场、利园山道，波斯富街都挤满了人。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356956" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010334391366.jpg">
  <img alt="" class="size-large wp-image-11356956" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010334391366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  游行队伍经过铜锣湾伊荣街。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356961" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010334331366.jpg">
  <img alt="" class="size-large wp-image-11356961" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010334331366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民举起以各式海报表达诉求。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356957" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010334311366.jpg">
  <img alt="" class="size-large wp-image-11356957" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010334311366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民举起以各式海报表达诉求。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356969" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010349201528.jpg">
  <img alt="" class="size-large wp-image-11356969" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010349201528-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  有市民举起选择站在良知一边的标语（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356970" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010334361366.jpg">
  <img alt="" class="size-large wp-image-11356970" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010334361366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民举起以英文写的标语。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 <strong>
  15:30
 </strong>
</p>
<p>
 游行龙头出发逾半个小时，已经过铜锣湾鹅颈桥。维园现在仍有大批市民未能出发。由于警方只开放一个出口让市民出发，有人高呼“开路、开路”。另外，很多身穿黑衣的市民，从天后沿高士威道中央图书馆加入游行队伍，警方在3时左右开放高士威道往湾仔方向的行车线予游行人士使用。在铜锣湾，由于人太多，整条轩尼诗道要全封。大量巿民在路旁等候加入游行，怡和街东西行线都逼满人。
</p>
<figure class="wp-caption aligncenter" id="attachment_11356913" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010334281366@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11356913" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010334281366@1200x1200-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  在怡和街东西行线都逼满人。（林怡/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 15:20
</p>
<p>
 下午3时许，游行队伍先后经过铜锣湾怡和街、伊荣街和驹克道。
</p>
<p>
 《大纪元时报》和新唐人电视台的游行队伍从维园出发。他们高举“全民打共时代来临 香港人加油！”、“执法不守法 立法不合法 港人齐自发 自救想方法”等横额。
</p>
<figure class="wp-caption aligncenter" id="attachment_11356907" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010350581366.jpg">
  <img alt="" class="size-large wp-image-11356907" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010350581366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  游行队伍经过驹克道。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356917" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010334211366@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11356917" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010334211366@1200x1200-600x401.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  大纪元时报》和新唐人电视台的游行队伍。（余钢/大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356919" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010334241366@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11356919" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010334241366@1200x1200-600x401.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  《大纪元时报》和新唐人电视台的游行队伍。（余钢/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 14:50
</p>
<p>
 民阵的七一大游行龙头从维多利亚公园起步。领头的举起向天横幅写有“撤回恶法 林郑下台”的游行诉求，民主派元老李柱铭、民阵召集人岑子杰等手持横幅。游行又安排坐轮椅的市民先出发，获掌声欢迎。
</p>
<figure class="wp-caption aligncenter" id="attachment_11356856" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010302081366@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11356856" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010302081366@1200x1200-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  民阵的七一大游行龙头从维多利亚公园起步。领头的举起向天横幅写有“撤回恶法 林郑下台”的游行诉求。（林怡/大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356859" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010301241366.jpg">
  <img alt="" class="size-large wp-image-11356859" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010301241366-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  游行安排坐轮椅的市民先出发，获掌声欢迎。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356857" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010304161366.jpg">
  <img alt="" class="size-large wp-image-11356857" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010304161366-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  民阵的七一大游行龙头从维多利亚公园起步。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356855" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010313361366.jpg">
  <img alt="" class="size-large wp-image-11356855" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010313361366-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  民阵的七一大游行从维多利亚公园起步。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 14:40
</p>
<p>
 鉴于金钟立法会出现冲突，警方要求民阵将游行延期或将终点改为湾仔。
</p>
<p>
 不过，民阵与警方无法达成共识，因此决定继续游行，以遮打道为终点。由于修顿球场至遮打道一段道路或不受不反对通知书保障，民阵呼吁游行人士保障自身安全，按照自己需要离开。
</p>
<figure class="wp-caption aligncenter" id="attachment_11356853" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010313381366.jpg">
  <img alt="" class="size-large wp-image-11356853" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010313381366-600x401.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  领头的向天横幅写有“撤回恶法 林郑下台”。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356877" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010249161366@1200x1200.jpg">
  <img alt="" class="size-large wp-image-11356877" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010249161366@1200x1200-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  鉴于金钟立法会出现冲突，警方要求民阵将游行延期或将终点改为湾仔。民阵与警方无法达成共识，因此决定继续游行，以中环遮打道为终点。（孙青天/大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 13:50
</p>
<p>
 警方以金钟一带有示威者冲突为由，向民阵建议延期举行七一游行，或将活动改为只在维多利亚公园草坪集会，或将游行路线终点由金钟政府总部改为湾仔，民阵全部拒绝。
</p>
<p>
 13:30
</p>
<p>
 下午1时许，已陆续有身穿黑衣的市民，来到维多利亚公园草坪集合。由于烈日当空，市民都撑起雨伞耐心等待游行开始。就读中一的陈同学一个人来参加游行，她表示之前的反送中游行都有上街，“我的诉求就是要全面撤回送中条例，而非暂缓。释放所有学生。全面撤回暴动的这种说法。”
</p>
<p>
 对于特首林郑月娥不肯撤回恶法，只说暂缓，她认为背后是北京的指示，“我认为就是北京要求林郑这样做，因为林郑搞了很久，所以最后就让林郑来背黑锅。一国两制已经没有了界限，所谓50年不变，香港已经在被蚕食。”
</p>
<p>
 因此，她表示今次上街也是向中共说“不”，同时表达对前线抗争学生的支持。6‧12当天，她也到了金钟现场，“催泪弹的情况我没看到，我不是站在最前面，催泪弹后来看直播看到。警方清场都非常暴力，其实大部分的市民都很平和，但警察同样开枪发放催泪弹。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11356715" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010151511366.jpg">
  <img alt="" class="size-large wp-image-11356715" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010151511366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  就读中一的陈同学。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356716" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010152041366.jpg">
  <img alt="" class="size-large wp-image-11356716" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010152041366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民由记利佐治街步行到维园参加七一游行。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356717" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010152011366.jpg">
  <img alt="" class="size-large wp-image-11356717" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010152011366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民由记利佐治街步行到维园参加七一游行。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356718" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010151571366.jpg">
  <img alt="" class="size-large wp-image-11356718" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010151571366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民在维多利亚公园草坪撑起雨伞耐心等待游行开始。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 12:30
</p>
<p>
 距离七一大游行开始还有两个多小时，多个政党团体已经在铜锣湾港铁站出口的记利佐治街摆设街站，呼吁更多市民参与七一大游行。
</p>
<p>
 在“民主动力”街站的前香港城市大学政治学教授郑宇硕指，市民仍然很愤怒，相信会有很多人七一走出来，“市民对于林郑不下台，对于不撤回《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》的修订，不设立独立调查委员会调查警方暴力，感到很不满意，市民依然会利用游行和其它活动表达诉求。”他又呼吁接下来，11月的区议会选举，市民可以用手中选票冲击建制派绝大多数的议席。
</p>
<figure class="wp-caption aligncenter" id="attachment_11356719" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010110091366.jpg">
  <img alt="" class="size-large wp-image-11356719" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010110091366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  前香港城市大学政治学教授郑宇硕。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 前立法会议员刘小丽则在街站准备了大量白色鲜花，让市民为因反送中牺牲生命的年轻人表达悼念和追思。她说，两个周日过百万人上街，当中很多是年轻人，“想表达香港是我家，以及他们的诉求，背后的原因及行动，都是源自于对香港的爱。在送中之下，香港的言论自由民主等等完全崩溃，他们都是看到自己香港自己救，他们不得不走出来维护自己的家园。”
</p>
<p>
 “偏偏这个政权这样地狠心，一意孤行，年轻人受到文攻武斗，用短片去抹黑他们，还谎称他们充满仇恨，我觉得这个政府真的是无药可救。”刘小丽说。#
</p>
<figure class="wp-caption aligncenter" id="attachment_11356720" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010110121366.jpg">
  <img alt="" class="size-large wp-image-11356720" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010110121366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  前立法会议员刘小丽。（林怡／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356721" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010127091366.jpg">
  <img alt="" class="size-large wp-image-11356721" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010127091366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  多个政党团体已经在铜锣湾港铁站出口的记利佐治街摆设街站。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356722" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010127061366.jpg">
  <img alt="" class="size-large wp-image-11356722" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010127061366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  不同团体在记利佐治街利派发标语和海报予游行的市民。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356723" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010127031366.jpg">
  <img alt="" class="size-large wp-image-11356723" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010127031366-600x401.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  不同团体在记利佐治街派发标语和海报予游行的市民。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356724" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010116341366.jpg">
  <img alt="" class="size-large wp-image-11356724" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010116341366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  在港铁金钟站月台站满了前往维园参加游行的市民。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356725" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010132281366.jpg">
  <img alt="" class="size-large wp-image-11356725" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010132281366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民陆续抵达港铁铜锣湾站，前往维园参加游行。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356727" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010132261366.jpg">
  <img alt="" class="size-large wp-image-11356727" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010132261366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民陆续抵达港铁铜锣湾站，前往维园参加游行。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356728" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010132231366.jpg">
  <img alt="" class="size-large wp-image-11356728" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010132231366-600x450.jpg" title=""/>
 </span>
 <br/><figcaption class="wp-caption-text">
  市民陆续抵达港铁铜锣湾站，前往维园参加游行。（孙青天／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
</p>
<p>
 责任编辑：叶琴
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11356690.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11356690.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11356690.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/1/n11356690.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

