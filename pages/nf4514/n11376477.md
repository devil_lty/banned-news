### 一宗间谍案 揭露中共黑手伸到中亚
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-161950063-600x400.jpg"/>
<div class="red16 caption">
 今年2月，哈萨克斯坦逮补一名涉嫌泄露国家机密给中共特务的政府高级顾问，并且对外曝光中共势力入侵中亚。图为哈萨克斯坦总统府。(John Macdougall / AFP / Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月11日讯】（大纪元记者洪雅文编译报导）2019年2月19日，
 <span href="http://www.epochtimes.com/gb/tag/%E5%93%88%E8%90%A8%E5%85%8B%E6%96%AF%E5%9D%A6.html">
  哈萨克斯坦
 </span>
 （Kazakhstan）反情报部门突袭了阿拉木图（Almaty）一栋苏联时代的老公寓楼，当场逮捕一名政府高级顾问，指控他涉嫌为
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1.html">
  中共
 </span>
 从事间谍任务。
</p>
<p>
 《华尔街日报》7月10日报导，事发几个月后，
 <span href="http://www.epochtimes.com/gb/tag/%E5%93%88%E8%90%A8%E5%85%8B%E6%96%AF%E5%9D%A6.html">
  哈萨克斯坦
 </span>
 当局做出了罕见的决定，他们允许当地媒体曝光这起秘密案件的信息，公开反击北京在中亚国家日益增长的影响力。哈萨克斯坦领导人试图平衡对中资的渴望与忧患，他担心
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1.html">
  中共
 </span>
 进一步侵犯该国。
</p>
<p>
 知情人士透露，被逮捕者康斯坦丁‧司罗耶钦（Konstantin Syroyezhkin）是前苏联特工，被指控从中共间谍处受贿，泄露国家机密文件。
</p>
<p>
 他在担任哈萨克斯坦中国关系高级顾问期间，曾劝告当时的总统就哈萨克斯坦边境划界问题与中方进行谈判​​。司罗耶钦曾撰写了大量关于中国的文章，被认为是前苏联最重要的北京问题专家之一。
</p>
<p>
 莫斯科高等经济学院的中国问题专家瓦西里·卡申（Vasily Kashin）认为，通过这个公开的故事，哈萨克斯坦向中国（中共）发出的信息之一就是，不要在哈萨克斯坦过于大胆。
</p>
<p>
 在苏联解体后的几十年里，哈萨克斯坦的领导人一直试图与西方以及莫斯科维持关系。但近年来，中共已经成为中亚新的第三方平衡势力。俄罗斯官员私下表示，他们认为他们在哈萨克斯坦的影响正在减弱。
</p>
<p>
 哈萨克斯坦新总统卡西姆-乔马特·托卡耶夫（Kassym-Jomart Tokayev）在最近一次的采访中称，中国是重要的战略伙伴，双方在经济领域关系亲密。
</p>
<p>
 但事过不久，前苏联最重要的中国专家之一司罗耶钦就因间谍罪被捕，暴露哈萨克斯坦对中共影响力日益增长的不安情绪，以及该国在亚洲十字路口的脆弱感。哈萨克斯坦位处中国的西部边界到欧洲的最东端，中共2013年正是在这里启动“
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%80%E5%B8%A6%E4%B8%80%E8%B7%AF.html">
  一带一路
 </span>
 ”倡议。
</p>
<p>
 与哈萨克斯坦政府关系密切的人士指出，中国国有银行和贷款机构向哈萨克斯坦提供的贷款已飙升至数百亿美元。哈萨克斯坦官员表示，北京正在试图利用这些经济关系来增强其政治影响力，他们（哈萨克斯坦官员）正在对其意图强加一些界限。
</p>
<p>
 据知情人士透露，北京最近游说哈萨克斯坦政府，允许中国安全承包商在其境内开展业务；此外，中国商人还要求接管哈萨克斯坦的公司，来换取中国提供该国的贷款量。
</p>
<p>
 报导指出，与中国的商业关系占哈萨克斯坦贸易总额的12%，虽然还有增长趋势，但是一些较大的中资项目已开始放缓，例如：首都建轻轨系统的计划。中国和哈萨克斯坦公司之间单独签订的270亿美元投资计划也几乎崩溃。
</p>
<p>
 目前尚不清楚两国谈判的进展情况。中共外交部没有立即回应置评请求。
</p>
<p>
 近年来，中共在亚洲、欧洲和美国开展了大量的间谍活动和情报搜集，根据先前报导，中共特工常利用现金、免费旅游和物质奖励来招募美国人为他们进行间谍活动。美国法院判定这些人均属于“中国（中共）代理人”。#
</p>
<p>
 责任编辑：李寰宇
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11376477.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11376477.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11376477.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/10/n11376477.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

