### 每日进账数百澳元 中国假乞丐墨尔本被捕
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-155604296-600x400.jpg"/>
<div class="red16 caption">
 <p>
  澳洲维州警方破获一个利用旅游签证从中国飞到澳洲、专门在墨尔本市中心街头行乞的职业乞丐组织。图为示意图。（ROSLAN RAHMAN/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月05日讯】（大纪元记者天睿澳洲悉尼编译报导）维州警方破获一个利用
 <span href="http://www.epochtimes.com/gb/tag/%E6%97%85%E6%B8%B8%E7%AD%BE%E8%AF%81.html">
  旅游签证
 </span>
 从中国飞到澳洲、专门在墨尔本市中心街头行乞的
 <span href="http://www.epochtimes.com/gb/tag/%E8%81%8C%E4%B8%9A%E4%B9%9E%E4%B8%90.html">
  职业乞丐
 </span>
 组织。据称，这些年迈的假乞丐一天能有数百澳元的进账，他们会把这些钱电汇回中国大陆。维州法律禁止人们在大街上行乞，违反者最高将面临一年徒刑。
</p>
<p>
 据太阳先驱报报导，墨尔本市中心的伊丽莎白街（Elizabeth St）、斯旺斯顿街（Swanston St）、弗林德斯街（Flinders St）和伯克街（ Bourke St）的主要路口都有这些
 <span href="http://www.epochtimes.com/gb/tag/%E8%81%8C%E4%B8%9A%E4%B9%9E%E4%B8%90.html">
  职业乞丐
 </span>
 的身影。
</p>
<p>
 这些头发花白的中国老人将职业乞丐装套在日常衣服的外面，拿着讨钱的碗或袋子在人流密集的大街上乞讨。
</p>
<p>
 这些老年人被组织者安排住在墨尔本内城区的旅店，他们会将乞讨来的现金交给中间人。维州警方观察到这些人曾和两名中间人有互动，这两人均持
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD%E6%8A%A4%E7%85%A7.html">
  中国护照
 </span>
 。维州警方已经把他们的信息交给
 <span href="http://www.epochtimes.com/gb/tag/%E8%81%94%E9%82%A6%E8%AD%A6%E5%AF%9F.html">
  联邦警察
 </span>
 做进一步调查。
</p>
<p>
 警方从这些老年人身上一共发现了近1,000元的现金。一些钱和其它物品被藏匿在衣服里。
</p>
<p>
 据称，这些中国老人的护照和
 <span href="http://www.epochtimes.com/gb/tag/%E6%97%85%E6%B8%B8%E7%AD%BE%E8%AF%81.html">
  旅游签证
 </span>
 是由一个犯罪组织负责办理的，然后他们再安排这些人乘飞机来到澳洲做短暂停留，期间组织他们上街乞讨。
</p>
<p>
 维州警方和
 <span href="http://www.epochtimes.com/gb/tag/%E8%81%94%E9%82%A6%E8%AD%A6%E5%AF%9F.html">
  联邦警察
 </span>
 、边境执法局展开合作，共同收集了这个犯罪组织的信息，包括他们在机场的活动。
</p>
<p>
 本周，维州警察和墨尔本市政府发起联合行动
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%AE%E6%8D%95.html">
  逮捕
 </span>
 了7名来自中国大陆的职业乞丐，他们都是用旅游签证来到澳洲的。警方指控他们违法乞讨和处理犯罪所得。
</p>
<p>
 警方经过审问之后，释放了几名假乞丐。但据称其中几人又回到街上乞讨。
</p>
<p>
 东墨尔本警察局警官奥布莱恩（Chris O’Brien）表示，警方通过翻译对这些人进行了审问，他们口径一致，都说自己无家可归，很穷，住在街上。“我们认为这是一个有组织犯罪集团，他们来到墨尔本利用当地居民的同情心讨钱。”
</p>
<p>
 当救世军（Salvation Army）为他们提供支持服务时，只有一人表示接受。奥布莱恩说：“他们为了从中国来到这里做这些事下了很大本钱。”当警察拿走他们的钱当作证物封存时，这些老人哭着说，他们需要这些钱飞回中国。
</p>
<p>
 墨尔本副市长伍德（Arron Wood）表示自己也曾亲身遇到过这种“极端的行乞方式”。“以前，我从来没在墨尔本的街头见到过这样的状况，特别是在十字路口正中央低头行乞。”
</p>
<p>
 他表示，墨尔本人对那些遭遇不幸的人很有同情心，这些职业乞丐就是利用了人们这一点，因此政府需要迅速采取措施制止这种犯罪行为。#
</p>
<p>
</p>
<p>
 责任编辑：尧宁
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11365919.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11365919.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11365919.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/5/n11365919.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

