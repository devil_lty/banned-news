### 蒙面也能认出 中共“步态识别”监控被批
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1052877606-600x400.jpg"/>
<div class="red16 caption">
 <p>
  对民众的监控不断升级，中共2日发布号称全球首个“步态识别”监控系统。图为资料图。（NICOLAS ASFOURI/AFP/Getty Images）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月03日讯】（大纪元记者李心茹报导）中共对民众的
 <span href="http://www.epochtimes.com/gb/tag/%E7%9B%91%E6%8E%A7.html">
  监控
 </span>
 不断升级。继人脸辨识系统后，中共当局2日发布号称全球首个“
 <span href="http://www.epochtimes.com/gb/tag/%E6%AD%A5%E6%80%81%E8%AF%86%E5%88%AB.html">
  步态识别
 </span>
 ”监控系统。目标人物即使将脸遮住，系统也可以通过走路姿态辨认出来。
</p>
<h4>
 号称建全球首个
 <span href="http://www.epochtimes.com/gb/tag/%E6%AD%A5%E6%80%81%E8%AF%86%E5%88%AB.html">
  步态识别
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E7%9B%91%E6%8E%A7.html">
  监控
 </span>
</h4>
<p>
 据中共官媒报导，由中科院自动化研究所建立的人工智慧（AI）企业“银河水滴”，2日发布号称全球首个步态识别互联系统“水滴慧眼”，具有所谓的步态建库、步态识别、步态检索、大范围追踪等功能，可进行海量监控器下步态识别的即时智慧互联。
</p>
<p>
 银河水滴CEO黄永祯声称，此系统可在50公尺内辨识人们的身份，就算当事人背对摄像机镜头或把脸遮住，也能够辨识出来。
</p>
<p>
 “银河水滴”也声称，此系统跨视角步态识别精度可达94%，可广泛应用于车站、机场、博物馆、学校、景区、商场等城市场景等的监控。
</p>
<p>
 步态是一种既含有生理性又含有行为性的生物特征。每个人走路的姿态都不同，中共用此来识别民众身份，搜集个人信息。
</p>
<p>
 早在去年10月，银河水滴就曾发布了一款所谓的“步态检索智慧一体机”，可在许多监视视频中快速进行目标人物检索和身份识别。
</p>
<p>
 事实上，早在2015年，银河水滴就已经采集了1万多人的步态数据。这些数据涵盖了这1万多人在不同场景中走路的姿势；不同季节、不同角度下的体型，甚至细致到头型等一系列信息……
 <br/>
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/ad7563bfe8c8f766.gif">
  <img alt="" class="aligncenter wp-image-11362233 size-full" src="http://i.epochtimes.com/assets/uploads/2019/07/ad7563bfe8c8f766.gif"/>
 </span>
</p>
<h4>
 中共建全球最大步态数据库
</h4>
<p>
 截至今年，中共已经在大陆构建了全球最大的步态数据库。虽然官方并没有公布这个数据采集的方法，但外界猜测，有可能是借助大街小巷无处不在的摄像头，被认为细思恐极。
</p>
<p>
 据了解，步态识别监控系统已陆续在湖北、广东、上海等地应用。
</p>
<p>
 近几年来，警方已使用人脸识别系统在人群中辨识民众的身份，并发展一套监视器资料的全国整合系统。有行业调研公司估计，中共在公共和私人领域共装有1.76亿个监控摄像头，预计到2020年，中共将装约4.5亿个摄像头。
</p>
<h4>
 “主要用于监控老百姓”
</h4>
<p>
 不断提升的中共监控系统受到外界批评。旅美时政评论员郑浩昌7月3日对大纪元记者表示：步态识别的最主要应用就是监控民众。
</p>
<p>
 郑浩昌说：“人脸识别监控已经够老百姓心惊肉跳的了，现在又出了个步态识别，这其实是以‘科技兴国’为名，行‘监控老百姓’之实。虽然步态识别有不同的应用，但是不用想都会知道其首要应用就是‘维稳’，盯住所有中共感觉对其政权有威胁的人。
</p>
<p>
 美国不是没有这样的技术能力，但在民主国家，这种对民众
 <span href="http://www.epochtimes.com/gb/tag/%E9%9A%90%E7%A7%81.html">
  隐私
 </span>
 构成重大威胁的技术是有很多限制的，在中共治下就完全放开了，只要政府有需求，想怎么来就怎么来。”
</p>
<p>
 《华尔街日报》曾刊文批评中共利用人脸识别技术监控民众。报导说，美国使用该技术是为了确认犯罪嫌疑人的身份，而中共的目的有所不同，它正快速运用新技术对人民进行监视。
</p>
<p>
 另外，欧盟也推出新的数据
 <span href="http://www.epochtimes.com/gb/tag/%E9%9A%90%E7%A7%81.html">
  隐私
 </span>
 法规，限制公司搜集个人数据、生物特征数据和社交媒体数据。
</p>
<p>
 对于中共的监控，网民纷纷批评道：“中共用各种邪恶的手段监控14亿民众，把中国人全看作敌人，所以它就是中国人的最大敌人。”
</p>
<p>
 “真正抓坏人时这些监控肯定是坏了，抓异议人士、访民时就灵敏得很。”#
</p>
<p>
 责任编辑：林诗远
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11362202.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11362202.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11362202.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/3/n11362202.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

