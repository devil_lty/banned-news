### 花旗：5成台湾科技厂商将撤离中国
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/20190514-HUAMING-HONGKONG-03-600x400.jpg"/>
<div class="red16 caption">
 日前，花旗银行发表的研究报告显示，5成台湾科技厂商或将撤离中国。图为准备从大陆撤资的富士康，在大陆的生产线。(Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月09日讯】（大纪元记者周心鉴综合报导）在
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD%E7%BB%8F%E6%B5%8E%E4%B8%8B%E8%A1%8C.html">
  中国经济下行
 </span>
 、美中
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%B8%E6%98%93%E6%88%98.html">
  贸易战
 </span>
 的冲击下，包括
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%B0%E5%95%86.html">
  台商
 </span>
 在内的外资加速撤离大陆。日前，
 <span href="http://www.epochtimes.com/gb/tag/%E8%8A%B1%E6%97%97.html">
  花旗
 </span>
 银行发表的研究报告显示，5成台湾科技厂商或将撤离中国大陆，并导致多达近300万份工作消失。
</p>
<p>
 据《自由时报》引述
 <span href="http://www.epochtimes.com/gb/tag/%E8%8A%B1%E6%97%97.html">
  花旗
 </span>
 银行的报告说：“台厂出口占中国总出口的至少10%，在中国营运的37家台厂名列对美国出口的100大出口商名单，由于中国劳动和其它成本快速上升，劳力密集产业已陆续撤离中国。”
</p>
<p>
 今年5月5日，因中共对双方之前谈判所做的承诺反悔，美国宣布对2,500亿美元中国商品征收25%的关税，该措施于6月1日起正式对到达美国港口的中国商品生效。
</p>
<h4>
 大陆近300万份工作消失
</h4>
<p>
 花旗的报告预估，台厂在中国雇用1千万名工人，60%是资讯和通讯科技产业，累计的投资金额超过660亿美元，“未来几年30%至50%的台厂将撤离中国，并将裁撤177万至295万份工作”。
</p>
<p>
 另据中国国际金融公司的研究报告显示，美国对2,500亿美元中国商品加征25%的关税以后，将会影响中国制造电脑和电子产品的上市公司去年总获利的18.7%。
</p>
<h4>
 回台投资率创7年新高
</h4>
<p>
 台湾金融研训院7月2日举办的“回流资金，再造产业成长契机”研讨会上，国发会主委陈美伶表示，今年台湾的投资率可回升至21.47%，连2年成长，今年的投资率同时创下7年来的新高。
</p>
<p>
 鼓励
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%B0%E5%95%86.html">
  台商
 </span>
 资金回流，台湾立法院临时会7月3日三读通过“境外资金汇回管理运用及课税条例”，明定汇回资金租税优惠首年税率8%，第2年10%，若汇回资金用于实质投资，税率可再减半，即税率仅4%与5%。
</p>
<p>
 此外，台湾经济部投审会统计，今年1到5月，台商对中国大陆投资意愿续减。投资件数为249件，件数较上年同期减少2.73%；核准投资金额19亿513万1,000美元，较上年同期减少45.99%。
</p>
<h4>
 进大陆投资 台商：死路一条
</h4>
<p>
 由于台商对大陆投资意愿续减，为此，中共国务院台湾事务办公室（国台办）再度通过发言系统，大吹“台商一起来，融入大湾区”力邀台商到大陆投资。主题活动于6月3日至5日在广东举行。并声称惠台31项政策（台湾称对台政策）与各项投资优惠。
</p>
<p>
 不过，一名已前往东南亚设厂的纺织业台商小方（化名）曾向大纪元表示，自十多年前起，台商就开始撤资，“现在进去（大陆）就是死路一条”。
</p>
<p>
 小方说，很多没资产的台商，索性直接“拍拍屁股走人”，现在还没走（离开中国大陆）的都在想办法抛售资产。据他知悉的纺织同业、这次
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%B8%E6%98%93%E6%88%98.html">
  贸易战
 </span>
 受创最严重的科技业，很多（要离开却）卡在土地处理问题，“卖地与所得税金将近40%”，加上结束清算与资遣代价太高，“他们（政府）可能会追讨五险一金差额保费（很多台商当初进入大陆投资时，当局允以最低投保金额支应雇主的劳工险负担）”。
</p>
<p>
 此外，关厂遣散大陆员工，后续可能引发劳工抗议、诉讼等处理不完的纠纷。这些代价，对于许多员工规模500人以下的中小型台商，很难应付。
</p>
<p>
 另一位台湾本土企业公会代表曾向大纪元表示，今年4月，他前往大陆视察美中贸易战对台商沿海工厂、大湾区厂家的影响，“其实他们对于那里（对岸）的情况都很知道”，贸易战对台湾长期有利，加上台商大量回台这都是事实。
</p>
<p>
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD%E7%BB%8F%E6%B5%8E%E4%B8%8B%E8%A1%8C.html">
  中国经济下行
 </span>
 、美中贸易战的压力下，以及生产成本节节升高，在大陆投资的海外厂商（包括台商）开始将生产线迁出中国，甚至一些中国本土的工厂也把生产线迁至东南亚一些国家，或是到墨西哥建厂，以避免大环境下带来的冲击，其中包括苹果公司等这些国际大品牌和富士康这样的大工厂。#
</p>
<p>
 责任编辑：孙芸
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11372442.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11372442.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11372442.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/8/n11372442.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

