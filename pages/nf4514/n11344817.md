### 三中资银行被点名替朝鲜洗钱 或遭美惩罚
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2015/03/150319090138100311-600x400.jpg"/>
<div class="red16 caption">
 <p>
  招商银行等三家大型中资银行在对朝鲜反制裁的调查中，因拒绝配合传票要求，已遭美国法官裁定蔑视法庭。（宋祥龙/大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月25日讯】外媒报导，三家大型中资银行对
 <span href="http://www.epochtimes.com/gb/tag/%E6%9C%9D%E9%B2%9C.html">
  朝鲜
 </span>
 反制裁的调查中，因拒绝配合传票要求，已遭美国法官裁定蔑视法庭。
</p>
<p>
 《华盛顿邮报》报导，三家中资银行与一家注册于香港的离岸公司有业务往来，替
 <span href="http://www.epochtimes.com/gb/tag/%E6%9C%9D%E9%B2%9C.html">
  朝鲜
 </span>
 受制裁的国有外贸银行洗钱超过1亿美元。这三家银行不遵守美国司法系统的指令，拒绝提供与前述公司有关的文件。此事件或导致三家银行被拒于美国金融体系门外，亦可能失去参与美元交易的资格。
</p>
<p>
 报导说，法庭文件虽未披露上述银行名称，但是法院判决的细节与2017年的一项民事案件内容一致。当时美国司法部称，中国的
 <span href="http://www.epochtimes.com/gb/tag/%E4%BA%A4%E9%80%9A%E9%93%B6%E8%A1%8C.html">
  交通银行
 </span>
 、
 <span href="http://www.epochtimes.com/gb/tag/%E6%8B%9B%E5%95%86%E9%93%B6%E8%A1%8C.html">
  招商银行
 </span>
 与上海
 <span href="http://www.epochtimes.com/gb/tag/%E6%B5%A6%E4%B8%9C%E5%8F%91%E5%B1%95%E9%93%B6%E8%A1%8C.html">
  浦东发展银行
 </span>
 ，与明正国际贸易公司有交易往来，该公司跟朝鲜有关。
</p>
<p>
 据报导，面临被美国制裁风险的银行可能是上海浦发银行，根据该行的股权结构、美国有限业务，和涉及与其它银行的行为，均与裁决中拟制裁的银行表述相同。
</p>
<p>
 消息出来后，这三家银行相继回应称，“没有受到因涉嫌违反任何制裁法律的相关调查。”
</p>
<p>
 受上述消息冲击，在香港股市，
 <span href="http://www.epochtimes.com/gb/tag/%E6%8B%9B%E5%95%86%E9%93%B6%E8%A1%8C.html">
  招商银行
 </span>
 、
 <span href="http://www.epochtimes.com/gb/tag/%E4%BA%A4%E9%80%9A%E9%93%B6%E8%A1%8C.html">
  交通银行
 </span>
 及浦发银行股价重挫，其中招商银行下跌8.99%。而在大陆A股方面，银行板块也表现低迷，招商银行大跌6.8%，平安银行下跌3.87%，浦发银行、交通银行均跌逾3%。
</p>
<p>
 多位市场人士告诉财新网，6月25日招商银行 、交通银行、浦发银行三家领跌银行板块，则跟《华盛顿邮报》所披露的一则三家中资银行被制裁消息有关。
</p>
<p>
 美国法庭早前已因三家银行在事件中的不合作态度，对它们开征每日5万美元的罚款。当时法官更表示，如果三家银行继续不配合，将提高处罚力度。
</p>
<p>
 此案将于今年7月12日开庭。#
</p>
<p>
 责任编辑：许梦儿
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11344817.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11344817.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11344817.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/25/n11344817.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

