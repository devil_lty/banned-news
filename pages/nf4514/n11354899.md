### 【不断更新】川普将在板门店会见金正恩
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-1152906887-600x400.jpg"/>
<div class="red16 caption">
 <p>
  6月30日下午，川普总统与金正恩在韩朝非军事区（DMZ）见面。(Brendan Smialowski/AFP)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月30日讯】6月30日，在韩国首尔青瓦台举行的美韩总统联合记者会上，美国总统
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E6%99%AE.html">
  川普
 </span>
 （特朗普）和韩国总统文在寅证实，川普总统将与
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%91%E6%AD%A3%E6%81%A9.html">
  金正恩
 </span>
 在韩朝非军事区（DMZ）见面。
</p>
<p>
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E6%99%AE.html">
  川普
 </span>
 在G20峰会期间，在日本大阪6月29日上午发推文称，他或将在韩朝边境与
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%91%E6%AD%A3%E6%81%A9.html">
  金正恩
 </span>
 见面。随后，朝鲜回应说，这一建议“非常有趣”，并表示，若朝美首脑在韩朝边境会面将成重要契机。
</p>
<p>
 当地时间下午召开新闻发布会时，川普在谈到朝鲜问题时说，他愿意去朝鲜访问。当记者问到，如果金正恩爽约，没有在边境线露面怎么办？川普说，“我当然想到了这一点”，不过他认为，这不是一个坏迹象。
</p>
<p>
 川普还说，他不会跟金正恩进行深入谈判，“我们不是进行深入会谈，只是一个快速的问候。”川普2017年11月访韩期间，曾计划前往非军事区，但因天气问题未能成行。
</p>
<p>
 川普曾与金正恩有过两次会晤，一次是2018年6月在新加坡，另一次是2019年2月在越南，但是迄今两国没有达成任何协议。
</p>
<p>
 下面是及时跟踪报导。（韩国当地时间）
</p>
<p class="p1">
 <span class="s1">
  14:54
  <span class="Apple-converted-space">
  </span>
 </span>
 <span class="s2">
  川普：自首次与金正恩会面以来，已发生
 </span>
 <span class="s1">
  “
 </span>
 <span class="s2">
  巨大变化
 </span>
 <span class="s1">
  ”
 </span>
</p>
<p class="p1">
 <span class="s2">
  美国总统川普正在韩国警卫塔上讲话，在这里可以俯瞰韩国和朝鲜之间的非军事区（
 </span>
 <span class="s1">
  DMZ
 </span>
 <span class="s2">
  ）。
 </span>
</p>
<p class="p1">
 <span class="s2">
  川普表示，该地区曾经
 </span>
 <span class="s1">
  “
 </span>
 <span class="s2">
  非常非常危险
 </span>
 <span class="s1">
  ”
 </span>
 <span class="s2">
  ，但自从他与朝鲜领导人金正恩谈话后，情况发生了重大变化。
 </span>
 <span class="s1">
  “
 </span>
 <span class="s2">
  在我们的第一次首脑会议之后，所有的危险都消失了。
 </span>
 <span class="s1">
  ”
 </span>
 <span class="s2">
  他补充说，在平壤、华盛顿和首尔之间的会谈以来，军事威胁已大大减少。”
 </span>
</p>
<p class="p1">
 14:35 川普和文在寅确认了
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E9%87%91%E4%BC%9A.html">
  川金会
 </span>
 ，但朝鲜沉默
</p>
<p class="p2">
 <span class="s1">
  美国总统川普和韩国总统文在寅都证实，今天他们将在非军事区（
 </span>
 <span class="s2">
  DMZ
 </span>
 <span class="s1">
  ）会见朝鲜领导人金正恩。
 </span>
</p>
<p class="p2">
 <span class="s2">
  “
 </span>
 <span class="s1">
  我非常期待，
 </span>
 <span class="s2">
  ”
 </span>
 <span class="s1">
  川普说，
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  我们理解彼此。
 </span>
 <span class="s2">
  ”
 </span>
 <span class="s1">
  文在寅则表示，
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  美国和朝鲜的领导人在
  <span href="http://www.epochtimes.com/gb/tag/%E6%9D%BF%E9%97%A8%E5%BA%97.html">
   板门店
  </span>
  会面，是历史首次。
 </span>
 <span class="s2">
  ”
 </span>
</p>
<p class="p2">
 <span class="s1">
  然而。会议的一方平壤却没有任何迹象证实这次会面。
 </span>
</p>
<p>
 14:50
</p>
<p>
 川普和文在寅驾车进入非军事区（DMZ）， 预计将在几分钟后与朝鲜的金正恩会面。
</p>
<p>
 他们路过了一个提供朝鲜观点的韩国岗哨。
</p>
<p>
 14:24 川普和金正恩会在
 <span href="http://www.epochtimes.com/gb/tag/%E6%9D%BF%E9%97%A8%E5%BA%97.html">
  板门店
 </span>
 “创造历史”
</p>
<p>
 川普对跟金正恩会面一直很积极，然而到目前为止，平壤一直很“腼腆”，官方媒体只是说这个建议“很有趣”。
</p>
<p>
 但美国国家利益中心韩国研究高级主任哈里·卡齐尼斯认为：“川普总统和金主席肯定会在DMZ会面，在镜头前握手，这是历史性的转折：川普总统将跨上朝鲜领土，成为第一位正式进入朝鲜的在位美国总统，巩固了两国领导者对改善关系的承诺。”
</p>
<p>
 今天可以期待什么？卡齐亚尼斯说：“我们应该期待，这个会议本身简短而有意义，因为它将标志着两国关系的重新定位，可能会从河内谈判中止的地方重新开始谈判。”
</p>
<p>
 14:09川普正前往DMZ
</p>
<p>
 美国总统川普（特朗普）已经离开首尔，途经韩国非军事区（DMZ），预计他将在那里会见朝鲜领导人金正恩。
</p>
<p>
 他应该在大约半小时后降落在两个韩国之间的边界。
 <br/>
</p>
<p>
 责任编辑：林琮文
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11354899.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11354899.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11354899.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/30/n11354899.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

