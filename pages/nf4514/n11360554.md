### 纳瓦罗：贸易谈判很复杂 美要超越零和游戏
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1125849630-600x400.jpg"/>
<div class="red16 caption">
 周二（7月2日），白宫贸易与制造业政策办公室主任彼得‧纳瓦罗（PETER NAVARRO）在接受CNBC电视采访时表示，川习会后，美中贸易谈判回到正轨，但谈判十分复杂。图为莱特希泽（左）和纳瓦罗（右）。(Jim WATSON / AFP)         (JIM WATSON/AFP/Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月03日讯】（大纪元记者许祯祺综合报导）周二（7月2日），白宫贸易与制造业政策办公室主任彼得‧
 <span href="http://www.epochtimes.com/gb/tag/%E7%BA%B3%E7%93%A6%E7%BD%97.html">
  纳瓦罗
 </span>
 （PETER NAVARRO）在接受CNBC电视采访时表示，
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E4%B9%A0%E4%BC%9A.html">
  川习会
 </span>
 后，美中
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%B8%E6%98%93%E8%B0%88%E5%88%A4.html">
  贸易谈判
 </span>
 回到正轨，但鉴于谈判十分复杂，所以需要时间，且这次谈判已经超越
 <span href="http://www.epochtimes.com/gb/tag/%E9%9B%B6%E5%92%8C%E6%B8%B8%E6%88%8F.html">
  零和游戏
 </span>
 。
</p>
<p>
 谈到
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E4%B9%A0%E4%BC%9A.html">
  川习会
 </span>
 的成果，
 <span href="http://www.epochtimes.com/gb/tag/%E7%BA%B3%E7%93%A6%E7%BD%97.html">
  纳瓦罗
 </span>
 表示，美中重新开始接触，“我们已经在通电话了，可能会有访问。都很好。我认为当时是必要的停顿。但是，这对市场非常好，整体而言不确定性较低。我们正朝着一个非常好的方向前进”。
</p>
<h4>
 美中贸易协议有150页 分七大章节
</h4>
<p>
 当主持人萨拉‧艾森（SARA EISEN）问6月川习会和去年12月川习会有何不同时，纳瓦罗回答说，这是一个非常复杂的过程，有七个不同的部分。
</p>
<p>
 “我们有一份超过150页的协议，有七个不同的章节。这是进一步向前发展的基础。情况很复杂。正如总统所说，这需要时间，我们希望做正确的事情。”他说。
</p>
<p>
 他还提到市场，纳瓦罗表示，现状对市场非常好，要在短期内实现道琼斯指数达到30,000点以上，可以通过美墨加贸易协定和美联储降低利率完成。
</p>
<h4>
 要让中方按国际规则行事
</h4>
<p>
 主持人提到，作为对华鹰派人物，美方同意暂停加征3000亿美元中国商品关税，这不符合纳瓦罗的做派。
</p>
<p>
 纳瓦罗解释说，目前美国对价值2500亿美元中国产品征收25%的关税，这是保持谈判正常进行的保险政策。这也是美国对中共掠夺的防御方法。
</p>
<p>
 “我们知道它们（中共，下同）做了什么。它们窃取我们的知识产权，迫使技术转让，它们将产品转移到它们的市场。这些是我们正在处理的结构性问题，不仅适用于美国，也适用于世界其它地区。”他说。
</p>
<p>
 纳瓦罗表示，美方想达到和中方的贸易关系是，让中方按照我们所知道的国际贸易规则（行事），而不是它们那套做法。“这（中共遵循的规则）更像是
 <span href="http://www.epochtimes.com/gb/tag/%E9%9B%B6%E5%92%8C%E6%B8%B8%E6%88%8F.html">
  零和游戏
 </span>
 ，我们希望超越这一点。”
</p>
<p>
 他接着说，从投资者的角度来看必须要知道的是：谈判已经回到迄今为止所做的工作轨道上，150页的文件，从那里前进。
</p>
<p>
 “（美方）已经做了很多很好的工作。有美国最好的贸易代表罗伯特‧莱特希泽（Robert Lighthizer），他正在与川普总统密切合作，正积极参与所有这些事务。”纳瓦罗说。
</p>
<p>
 他表示：“我们有一个计划。我认为人们需要耐心等待。正如总统所说，他想要做到这一点。这些事情需要时间。顺便说一句，这笔交易远比我们以前的任何一个谈判都复杂得多，这是有史以来最复杂、最睿智的协议。所以，我们有很多工作要做，我们正在做。我认为市场应该对它感到高兴，投资者应该对此感到高兴。”
</p>
<h4>
 十几年来 川普首次让中共回到谈判桌
</h4>
<p>
 主持人还提到财政部长史蒂芬‧姆钦（Steven Mnuchin）谈到美中协议已经完成90%，剩下10%包括让中方立法承诺在知识产权、任何数量的技术转让方面做出改变，因为中方似乎很不愿意这样做。
</p>
<p>
 纳瓦罗解释说，首先要让莱特希泽闭门进行谈判。当时机成熟时，将会有重大的信息公布。“但在那之前，让我们讨论谈判。我认为这就是重要的事情。让我们清醒地、静静地、闭门进行谈判，然后在时机成熟时一切都会透明”。
</p>
<p>
 他还引用了川普此前所说的话。即“我们需要达成协议，但协议要么包含很多内容（历史性伟大协议），要么没有协议”。
</p>
<p>
 纳瓦罗强调，川普是
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%B8%E6%98%93%E8%B0%88%E5%88%A4.html">
  贸易谈判
 </span>
 的领导者，在川普总统上任之前，美国花了十多年时间，甚至没有人愿意挑战中共的贸易行为，而川普政府让中共回到谈判桌上，这非常好。
</p>
<p>
 他表示，人们都同意中共需要从根本上纠正它的行为，并且中共已经在奥巴马和克林顿的执政期间度过了15年。而川普是那个为美国人民挺身而出的人，大家都同意这一点。#
</p>
<p>
 责任编辑：李寰宇
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11360554.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11360554.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11360554.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/2/n11360554.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

