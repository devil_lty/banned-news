### 7.1大游行迫近 港警称出动军队好 挨批
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/S__21790752-600x400.jpg"/>
<div class="red16 caption">
 <p>
  2019年6月12日，香港警察发射催泪弹、布袋弹及橡胶子弹对抗议民众进行驱离。(DALE DE LA REY/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月30日讯】（大纪元新闻中心记者梁珍报导）7.1前夕，
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 局势再度升级。香港警方出动至少5000警力，将会展一带全面封路，香港警队高层29日在记者会上更公开表示，出动解放军（
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1%E5%86%9B%E9%98%9F.html">
  中共军队
 </span>
 ）或驻港部队“是好主意”。
</p>
<p>
 同时，有消息指，中共内部有文件，要求7月起所有政府部门和事业单位不准赴港澳地区，以及官媒统一口径报导
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF.html">
  香港
 </span>
 事务，慎防香港事态辐射大陆。
</p>
<p>
 距离7.1还有两天，警方29日中午举行记者会释7.1大游行当日安排。被BBC记者问及警方有否借用外援（包括大陆资源），中区副指挥官辛子健爽快回答“good idea”，被追问是否包括
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1%E5%86%9B%E9%98%9F.html">
  中共军队
 </span>
 或驻港部队，他再次表示是“好的意见”，他又称所知警方至今未有动员境外资源。
</p>
<p>
 对于针对7.1游行，港警首次表态“出动解放军是好主意”，熟悉中国问题的香港时事评论员
 <span href="http://www.epochtimes.com/gb/tag/%E7%A8%8B%E7%BF%94.html">
  程翔
 </span>
 表示担忧。
</p>
<h4>
 <span href="http://www.epochtimes.com/gb/tag/%E7%A8%8B%E7%BF%94.html">
  程翔
 </span>
 ：
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%80%E5%9B%BD%E4%B8%A4%E5%88%B6.html">
  一国两制
 </span>
 玩完
</h4>
<p>
 事实上，早在6.12港警用橡皮子弹开枪镇压香港示威民众时，外界已有很多质疑中共军队参与其中。对于警方今次是否会出动中共军队镇压7.1游行，程翔表示不排除这个可能性，他以两点为例证。
</p>
<p>
 首先，他最近接到香港左派朋友内部传达中共有关香港问题四点指示，其一，香港是中国的香港，不是香港的香港，“这个等于说及警告，香港人不要妄想你能怎么样”；第二点，香港政权属于中央人民政府，“意思是中央政府可以直接治理你”；第三点，邓小平
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%80%E5%9B%BD%E4%B8%A4%E5%88%B6.html">
  一国两制
 </span>
 是体现对港人的信任和体恤，“换句话说，这种信任是可以收回的，一国两制就没有了”；第四点，驻港部队是定海神针。
</p>
<p>
 其次，6月12日警方开枪后，香港Now TV访问行政会议召集人陈智思，他用接近饮泣的语调说：“不愿看到有纪律部队以外的人执法”。
</p>
<h4>
 暴露中共极度危机感
</h4>
<p>
 程翔称，作为行政会议召集人，陈智思说出这番话，“说明他肯定听到类似的部署，用境外力量来维持治安。加上我收到的传达，强调驻港部队是定海神针，很明显暗示不排除这个可能。”
</p>
<p>
 如果真的这样的话，程翔强调：“香港就玩蛋了，一国两制彻底没有了。”
</p>
<p>
 不过，程翔指，中共要恐吓港人军队过境执法，也说明中共虚怯和危机感，包括最近中共国家主席习近平6月24日中共政治局会议上表示，“动摇党的危机无所不在”，正说明此点。他补充：“中共最后的武器就是军队了”。
</p>
<h4>
 中共文件：不得擅自报香港事务
</h4>
<p>
 另外，据时事评论员杰斯FB贴文表示：“要求慎防香港事态及西方价值观对中国内部人员造成影响。中国内地所有媒体7月后对香港的论述和报导，不得擅自报导一切香港事务。”
</p>
<p>
 本报记者向杰斯求证。他表示，经多个管道证实，此消息来自于中共内部红头文件，包括香港多个传媒今天都有收到。
</p>
<p>
 他指，最近大陆来港的旅游团明显减少，香港上水一带水货客也减少，说明中共很紧张香港局势。杰斯又表示，一些大陆人不在WhatsApp、微信、电话中提及香港问题，担心惹祸上身。有消息称，中共也内部要求7月1日，限制政府部门（公务员）、事业单位（媒体、大学、公安、消防等）赴港澳地区的人数。
</p>
<p>
 对于港警今日的表态，他也表示担忧，“这等于送中条例过了，香港玩完了。”又指，五年前雨伞运动都吹风中共军队要出动，“如果真的要做到这个地步，北京政府需要承担后果，后果相当严重，一国两制名存实亡。”他呼吁港人要站出来抗争，‘香港政府不代表香港市民，只是中共代言人。’
</p>
<h4>
 鲍彤：已收禁令拒评论香港问题
</h4>
<p>
 对于港警暗示要出动中共军队，前赵紫阳政治秘书鲍彤表示，很关注这个消息。但他表示，已经在6月22日收到当局禁令，不准谈论香港问题，所以不方便评论。
</p>
<p>
 责任编辑：连书华
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11354696.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11354696.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11354696.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/30/n11354696.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

