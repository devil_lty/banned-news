### 拆解华为新机P30 Pro 零件5成来自美日韩
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-1145807814-600x400.jpg"/>
<div class="red16 caption">
 <p>
  经拆解华为最新智慧手机P30 Pro发现，其机内零件按金计算约有5成来自美国、日本和韩国。（Hector Retamal/AFP/Getty Images）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月27日讯】（大纪元记者洪雅文编译报导）美国对
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%8E%E4%B8%BA.html">
  华为
 </span>
 实施出口管制，禁止国内芯片制造商向华为提供货源及技术。《日经》26日报导，
 <span href="http://www.epochtimes.com/gb/tag/%E6%8B%86%E8%A7%A3.html">
  拆解
 </span>
 华为最新智能手机P30 Pro发现，机内零件按金计算约有5成来自美国、日本和韩国，所有外国零件中又以美国制造单价最高。
</p>
<p>
 东京研究机构Fomalhaut Techno Solutions表示，
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%8E%E4%B8%BA.html">
  华为
 </span>
 P30 Pro有62％的手机零件成本来自中国以外的公司。虽然在美生产的15个零件仅占这部手机组件的1％，但其成本约占手机成本的16％。这些部件包括来自北卡罗来纳州芯片制造商科尔沃（Qorvo）的通信半导体，以及纽约技术材料集团康宁（Corning）的大猩猩玻璃等。
</p>
<p>
 由于华盛顿下达商务禁令，美国公司禁止供应华为组件和技术更新，华为现在不得不为其旗舰手机的15个高价值零件寻找新的供应商。目前美方允许华为在8月19日之前，购买美国制造的商品、为现有的华为手机提供安卓系统及软件更新，减轻对使用客户的影响。但仍然禁止其进口
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD%E9%9B%B6%E4%BB%B6.html">
  美国零件
 </span>
 生产新产品。
</p>
<p>
 尽管华为已经备存零件，但当库存枯竭时，该公司仍须寻找其它具有同等质量的独立供应商。
</p>
<p>
 经
 <span href="http://www.epochtimes.com/gb/tag/%E6%8B%86%E8%A7%A3.html">
  拆解
 </span>
 华为P30 Pro手机得出，该部智能手机的总零件成本约为363.83美元，
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD%E9%9B%B6%E4%BB%B6.html">
  美国零件
 </span>
 59.36美元（16.3%），中国零件138. 61美元（38.1%），日本零件83.71美元（23.0%），韩国零件28美元（7.7%），台湾零件28.85美元（7.9%）。
</p>
<p>
 P30 Pro总使用零件约1,631个，美国产15个（0.9%），中国产80个（4.9%），日本产869个（53.2%），韩国产562个（34.4%），台湾产83个（5.0%）。
</p>
<p>
 撇除中国当地制造零件，P30 Pro使用的最昂贵零件是美国美光科技公司（Micron Technology, Inc.）生产的DRAM芯片，单价40.96美元。其次是韩国三星的NAND Flash快闪记忆体，单价28.16美元。三是台湾制造的机体面板，单价20美元。四和五都是日本索尼制造的照相镜头，单价为15.15美元和12.16美元。
</p>
<p>
 华为P30 Pro内建使用的美产零件还包括：思佳讯通讯技术发展公司（Skyworks Solutions,Inc.）的通信半导体、科尔沃的通信半导体、康宁的保护玻璃、德州仪器（Texas Instruments）的MIPI开关，以及美、日合作生产的Large IC。
</p>
<p>
 本月初，华为将其未来两年的销售预期削减了300亿美元，并预计在2020年缩减大约1,000亿美元。华为创始人兼首席执行官任正非也承认，川普（特朗普）政府对美国供应商施加的限制对该集团造成了沉重的打击。
</p>
<p>
 与此同时，华盛顿正在研拟对中国商品征收新一轮关税。#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11349927.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11349927.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11349927.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/27/n11349927.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

