### 雨季总“看海” 大陆每年超百座城市内涝
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/3bd1b13549b3033f289e02c21a299f8a-600x400.jpg"/>
<div class="red16 caption">
 <p>
  2012年7月21日，北京遭遇强降雨，城市内涝严重，官方称160多万人受灾，其中79人死亡，经济损失上百亿元。（大纪元合成图片）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月02日讯】（大纪元记者李心茹报导）进入汛期以来，从南到北的强降雨天气，导致大陆多个城市出现
 <span href="http://www.epochtimes.com/gb/tag/%E5%86%85%E6%B6%9D.html">
  内涝
 </span>
 。该情景近年来已成常态，据中共水利部的数据，2010年～2016年，大陆平均每年有超过180座城市进水受淹或发生内涝。
</p>
<p>
 今年6月入汛以来，从南到北的强降雨天气，导致多个城市出现
 <span href="http://www.epochtimes.com/gb/tag/%E5%86%85%E6%B6%9D.html">
  内涝
 </span>
 。根据官方发布的消息，截至6月16日10时，仅南方最近此轮
 <span href="http://www.epochtimes.com/gb/tag/%E6%9A%B4%E9%9B%A8.html">
  暴雨
 </span>
 导致的包括城市内涝在内的灾害已造成8省614万人受灾，88人死亡、17人失踪。
</p>
<p>
 <center>
 </center>
 <br/>
 以上视频是在6月23日，网民上传的“江西萍乡
 <span href="http://www.epochtimes.com/gb/tag/%E6%9A%B4%E9%9B%A8.html">
  暴雨
 </span>
 山洪暴发入侵城镇”的视频。
</p>
<p>
 据中国天气网消息，未来的7月3日至4日，西藏、四川、云南、江南、华南等地有中到大雨，其中，江西东北部、浙江中部、华南南部沿海、海南岛西部等地局地有暴雨或大暴雨（100～140毫米）。
</p>
<h4>
 官方城市内涝统计数据
</h4>
<p>
 暴雨年年有，城市年年内涝。据中共住建部资料显示，2007年至2015年，全国超过360个城市遭遇内涝，其中六分之一单次内涝淹水时间超过12小时，淹水深度超过半米。
</p>
<p>
 据中共水利部数据显示，2010年至2016年，全国平均每年有超过180座城市进水受淹或发生内涝。
</p>
<p>
 有关城市内涝，有一个事件无法绕过去，那就是2012年发生在北京的“7·21”特大暴雨。当年7月21日，北京市遭遇强暴雨，城市内涝严重，官方称160多万人受灾，其中79人死亡，经济损失上百亿元。
</p>
<p>
 近十几年来，一下雨就面临“汪洋大海”的状态，已成为大陆城市的普遍顽疾。
</p>
<h4>
 武汉年年上演
 <span href="http://www.epochtimes.com/gb/tag/%E2%80%9C%E7%9C%8B%E6%B5%B7%E2%80%9D.html">
  “看海”
 </span>
</h4>
<p>
 例如，湖北武汉当年誓要投130亿告别看海，然而武汉
 <span href="http://www.epochtimes.com/gb/tag/%E2%80%9C%E7%9C%8B%E6%B5%B7%E2%80%9D.html">
  “看海”
 </span>
 情景依旧。
</p>
<p>
 武汉近年逢雨必涝。2011年，武汉“6·18”暴雨，东湖排水不畅，毗邻的武汉大学积水严重，武大学子戏谑邀请民众到武大去看海。“看海”一词从此风靡全国。
</p>
<p>
 2013年，武汉市水务局声称通过3年努力，投资130亿元，系统完善排水体系。当时有媒体报导称“武汉投资130亿告别‘看海’，一天下15个东湖也不怕”。但3年的时间早就过去了，武汉至今仍是内涝不断。
</p>
<p>
 今年6月21日晨，暴雨袭击湖北武汉。“武汉暴雨”、“武汉看海”、“武汉大学暴雨中毕业典礼”都上了热搜。以下是网民上传的“欢迎到武汉来看海”视频：
</p>
<p>
 <center>
 </center>
</p>
<h4>
 学者：根子在政府
</h4>
<p>
 整治内涝都那么多年了，为什么没有改善？旅美时政评论员郑浩昌7月2日对大纪元记者表示：这显然不是个技术问题，而是个政府不作为的问题。
</p>
<p>
 郑浩昌说：“政府不作为，根子就在政绩考量上。你说建设了个一流的下水道，领导下来视察，你听说过有带领导参观又黑又脏的下水道的吗？领导是要看高楼大厦、看GDP的，你下级官员当然要投其所好，才能升官发财。这就是中共治下的官场现状。问题就出在体制上，非民选的政权就老出这种问题。”
</p>
<h4>
 “地下水道是城市的良心”
</h4>
<p>
 大文豪雨果曾说， “地下水道是城市的良心”。而大陆大城市地上建设光鲜亮丽，而“良心”却不佳。
</p>
<p>
 与此相对比的是，中国古代人的城市规划和排水系统。北京很多地方的排水系统还是明清时期修建的排水明沟和暗沟及早期修建的污水管线。历经将近600年，许多地下管网仍在使用中。
</p>
<p>
 建于600年前的紫禁城，虽然经历暴雨，但紫禁城却安然无恙。根据历史记载来看，不管下多大的雨，故宫内从没有出现雨水阻塞的现象。
</p>
<p>
 <center>
 </center>
 而坐落在北海公园南门西侧的团城，距今已近600年的一套明朝建成的古代集雨排水工程仍在“服役”。#
</p>
<p>
 责任编辑：高静
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11360124.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11360124.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11360124.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/2/n11360124.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

