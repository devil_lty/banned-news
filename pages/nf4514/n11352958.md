### 湖北万民反对垃圾焚烧项目 千警暴力清场
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/IMG_6756-600x400.jpg"/>
<div class="red16 caption">
 6月28日，在湖北武汉市新洲区阳逻街，上万人进行游行示威，抗议当地政府建垃圾焚烧发电厂。（受访者提供）
</div>
<hr/><p>
 【大纪元2019年06月29日讯】（大纪元记者顾晓华采访报导）6月28日，在湖北武汉市新洲区
 <span href="http://www.epochtimes.com/gb/tag/%E9%98%B3%E9%80%BB.html">
  阳逻
 </span>
 街，上万人进行游行示威，
 <span href="http://www.epochtimes.com/gb/tag/%E6%8A%97%E8%AE%AE.html">
  抗议
 </span>
 当地政府建
 <span href="http://www.epochtimes.com/gb/tag/%E5%9E%83%E5%9C%BE%E7%84%9A%E7%83%A7.html">
  垃圾焚烧
 </span>
 发电厂。当局调动上千警力镇压，有民众被抓、被打，最后是警方暴力清场。
</p>
<p>
 据了解，当地有一座已运行十余年的陈家冲垃圾填埋场，臭气熏天，造成的污染已无法估量，政府却要在填埋场原址偷建
 <span href="http://www.epochtimes.com/gb/tag/%E5%9E%83%E5%9C%BE%E7%84%9A%E7%83%A7.html">
  垃圾焚烧
 </span>
 发电厂，周边民众闻讯后群起
 <span href="http://www.epochtimes.com/gb/tag/%E6%8A%97%E8%AE%AE.html">
  抗议
 </span>
 。
</p>
<p>
 一位女居民向大纪元记者表示，6月17日民众得知建垃圾焚烧发电厂的消息，6月23日百余名民众在保利园梦城处维权，他们身穿白色T恤，发传单。由于警方事先获得消息，在那里“守株待免”，结果有二十余人被抓，第二天才获释。
</p>
<link href="//vs.youmaker.com/css/api2.css" media="all" rel="stylesheet" target="_blank" type="text/css"/>
<div class="video_fit_container">
</div>
<p>
 但民众的维权自6月23日开始一直没有间断，至6月28日，事件迅速升级，上万名民众走上
 <span href="http://www.epochtimes.com/gb/tag/%E9%98%B3%E9%80%BB.html">
  阳逻
 </span>
 街的街头游行示威，高呼“还我绿水青山”的口号。
</p>
<p>
 一位现场民众对记者说：“我们早上八点钟开始，从阳逻政务中心，经过大转盘，再到中商、武商、量贩，到保利圆梦城，一路三四公里长，全部都是人，最后游行到经济开发区地铁站处。”
</p>
<p>
 许多现场民众向记者证实参加抗议的民众人数达到上万人，整个阳逻街都陷入瘫痪状态。
</p>
<div class="video_fit_container">
</div>
<p>
 以下视频显示，在游行队伍中，有一名小学生手持喇叭，高喊口号，走在队伍的前面，许多大人被这位孩子感动。
</p>
<div class="video_fit_container">
</div>
<p>
 当地政府则不断增加警力，最后，从武汉调集防暴警察进行镇压。
</p>
<p>
 在上午游行过程中多次发生警民
 <span href="http://www.epochtimes.com/gb/tag/%E5%86%B2%E7%AA%81.html">
  冲突
 </span>
 ，另一位现场民众说：“特警直接冲过来上百人，把人群分开两半，有的人直接被推倒，然后被踩踏。特警几次冲阵，连小孩都冲，我亲眼看到一个三十多岁的妇女被冲倒后，被后面特警踩踏的场景。”
</p>
<p>
 大纪元采访的第一位现场民众也说：“陆续有人被警察打，有个女的衣服被扯破，有十几个人被打。还有人被抓，我亲眼看到3人被抓走。”
</p>
<p>
 期间，民众向警察扔矿泉水瓶还击，场面几度失控。
</p>
<div class="video_fit_container">
</div>
<p>
 傍晚时，游行队伍集结在开发区地铁站处，新洲区区长到现场试图安抚民众，但是也未给出明确回复。许多下班后的民众加入，人越聚越多，防暴警察也在不断增加。
</p>
<p>
 至晚上9时许，警方开始暴力清场，将民众驱散。警察手持警棍到处乱打，不分男女老少，专门殴打头部。一位陶姓先生被打得头破血流，被送入第二人民医院后，由于伤势严重需转院。
</p>
<p>
 陶先生的妻子向记者透露，她与丈夫在游行时走散，后来接到丈夫被打入院的电话，她急忙赶到第二人民医院，结果数十名警察把守，不让她见自己的丈夫。之后，陶先生被救护车转院，但是不让家属跟随。她从医院处得知转入161医院，开车赶到该医院却找不到自己的丈夫。
</p>
<p>
 她心急如焚，最后通过朋友圈才得知丈夫被送到新洲人民医院。
</p>
<div class="video_fit_container">
</div>
<p>
 上万民众的抗议活动最终以警方暴力清场的方式结束，多少人被打伤、被抓目前不得而知。新洲区政府最后仅承诺，如果民众不同意，项目不开工。
</p>
<figure class="wp-caption aligncenter" id="attachment_11352977" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/IMG_6755.jpg">
  <img alt="" class="wp-image-11352977 size-large" src="http://i.epochtimes.com/assets/uploads/2019/06/IMG_6755-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  6月28日，在湖北武汉市新洲区阳逻街，上万人进行游行示威，抗议当地政府建垃圾焚烧发电厂。（受访者提供）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11352976" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/IMG_6754.jpg">
  <img alt="" class="wp-image-11352976 size-large" src="http://i.epochtimes.com/assets/uploads/2019/06/IMG_6754-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  6月28日，在湖北武汉市新洲区阳逻街，上万人进行游行示威，抗议当地政府建垃圾焚烧发电厂。（受访者提供）
 </figcaption><br/>
</figure><br/>
<p>
 而民众则表示，事实上垃圾发电厂已经在偷偷开工了，大家要的是彻底取消该项目，将现有的垃圾填埋场变成公园。
</p>
<p>
 民众还表示，将持续抗议，直至令垃圾场滚出阳逻。
</p>
<p>
 据公开资料显示，陈家冲垃圾填埋场隶属于市管企业，2007年投产，占地1,005亩，总容量1,400万立方米，设计日处理量为200吨。一期投入使用仅仅5年，填埋量已超过320万吨，处于超填状态。过去10年，所产生的恶臭经常覆盖8公里范围内的阳逻全镇以及阳逻港口，严重影响阳逻发展和居民生活。
</p>
<p>
 据悉，以陈家冲垃圾填埋场为中心，方圆3公里内聚集着两所高校、二十多个小区，数十家工厂企业。陈家冲的污染对阳逻乃至整个新洲的发展负面影响巨大，三十多万人的生活受到影响。#
</p>
<p>
 责任编辑：周仪谦
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11352958.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11352958.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11352958.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/28/n11352958.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

