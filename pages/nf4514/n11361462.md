### 组图：2019年唯一一次日全食惊艳南美
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/bf8d8a9a50714b4db56045f41a10d060_18-600x400.jpg"/>
<div class="red16 caption">
 2019年日全食现象发生在南美。(AFP)
</div>
<hr/><p>
 【大纪元2019年07月03日讯】（大纪元记者李言综合报导）周二（7月2日），成千上万的天文迷聚集南美，目睹罕见的
 <span href="http://www.epochtimes.com/gb/tag/%E6%97%A5%E5%85%A8%E9%A3%9F.html">
  日全食
 </span>
 奇观。这是两年以来全球首见日全食，而本次日全食更为罕见之处，是它恰好发生在地球上对其观测和研究准备最充分的地方。
</p>
<p>
 综合BBC和中央社等媒体报导，2019年的“
 <span href="http://www.epochtimes.com/gb/tag/%E6%97%A5%E5%85%A8%E9%A3%9F.html">
  日全食
 </span>
 带”（Path of totality）从太平洋开始，横跨智利和阿根廷各个地区，长达6,000英里。宽约100英里的地区都在观测范围内。
</p>
<p>
 像往常一样，人们观看日食时要小心，需要适当的保护，例如使用达标的太阳镜。因为直接盯着太阳看会伤害眼睛。
</p>
<p>
 本次日全食期间，月亮巨大的影子，也叫本影，首先触及新西兰东部海面。太平洋上首先也是唯一位于日全食覆盖面之内的地表是小小的上野岛（Oeno Island）——皮特凯恩英国海外领土（Pitcairns British Overseas Territory）的一部分。
</p>
<p>
 当地时间10点24分（格林威治标准时间18点24分）开始，这个无人居住的环礁岛陷入黑暗中——持续近3分钟。
</p>
<figure class="wp-caption aligncenter" id="attachment_11361639" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1I923A.jpg">
  <img alt="" class="wp-image-11361639 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1I923A-600x488.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  日全食奇观。(Stan HONDA/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 随后，当地时间16点38分（格林威治标准时间20点38分）日全食到达达拉‧塞雷纳（La Serena）附近的智利海岸。当月亮完全遮盖住太阳的那一刻，在场的数千人爆发出如雷的欢呼声和鼓掌声。
</p>
<figure class="wp-caption aligncenter" id="attachment_11361625" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1I9288.jpg">
  <img alt="" class="wp-image-11361625 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1I9288-600x450.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  月亮马上就要完全遮盖住太阳的那一刻。(Stan HONDA/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 日全食的路径还延伸到智利北部科金博（Coquimbo）的几个地方，这个地区有众多的天文台——世界上最强大的几架望远镜位于这里。
</p>
<p>
 价值2,000美元（1,588英镑）的观日全食门票在三分钟内售罄。有了这个门票，可以在阿塔卡马沙漠（Atacama Desert）高处的欧洲南方天文台（European Southern Observatory）与天文学家一起观看日食。
</p>
<figure class="wp-caption aligncenter" id="attachment_11361643" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/107722274_hi055049742.jpg">
  <img alt="" class="wp-image-11361643 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/107722274_hi055049742-600x338.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  人们在聚精会神观看日全食。(AFP)
 </figcaption><br/>
</figure><br/>
<p>
 月亮本影在安第斯（Andes）山脉和整个大陆上迅速通过。当地时间17:44（格林威治标准时间20:44），即日落前不久，日全食到达其终点——布宜诺斯艾利斯地区的查斯科木斯（Chascomús）。
</p>
<p>
 日全食有一些经典特征，包括“
 <span href="http://www.epochtimes.com/gb/tag/%E5%80%8D%E9%87%8C%E7%8F%A0.html">
  倍里珠
 </span>
 ”（Baily’s beads）——当最后的阳光穿过月球上的山谷时出现的现象。
</p>
<figure class="wp-caption aligncenter" id="attachment_11361626" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1I923U.jpg">
  <img alt="" class="wp-image-11361626 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1I923U-600x488.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  日全食奇观——
  <span href="http://www.epochtimes.com/gb/tag/%E5%80%8D%E9%87%8C%E7%8F%A0.html">
   倍里珠
  </span>
  （Baily’s beads）。(Stan HONDA/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 天文学家帕特里克‧麦卡锡（Patrick McCarthy）说，“你完全被日全食迷住了。”“不管它持续多久，感觉只有8秒钟。你完全沉浸其中。你对自己说，‘回去，回去；我没看够！时间太短了。’”
</p>
<p>
 澳洲游客贝特西‧克拉克（Betsy Clark）则说：“我不认为世上有比拉西亚（La Silla Observatory）更棒的日全食观测地点，因为这里非常干燥，几乎确定看得到太阳。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11361647" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1I929O.jpg">
  <img alt="" class="wp-image-11361647 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1I929O-600x452.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  日全食后在山中看的日落景象。(Stan HONDA/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 智利总统塞巴斯蒂安‧皮涅拉（Sebastian Pinera）也前往拉西亚附近的拉伊格拉（La Higuera），和数千名民众一同见证日全食奇观。
</p>
<p>
 周二，南美数以万计的游客还见证日全食的另一经典特征：“
 <span href="http://www.epochtimes.com/gb/tag/%E9%92%BB%E7%9F%B3%E6%88%92%E6%8C%87.html">
  钻石戒指
 </span>
 ”（Diamond Ring）。
</p>
<figure class="wp-caption aligncenter" id="attachment_11361636" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1I923Q.jpg">
  <img alt="" class="wp-image-11361636 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1I923Q-600x513.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  日全食奇观——
  <span href="http://www.epochtimes.com/gb/tag/%E9%92%BB%E7%9F%B3%E6%88%92%E6%8C%87.html">
   钻石戒指
  </span>
  （Diamond Ring）。(Stan HONDA/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 麦卡锡在智利观看了此次日全食。他是巨型麦哲伦望远镜（Giant Magellan Telescope，缩写为GMT）项目副总裁，该设施正在阿塔卡马沙漠建造。
</p>
<p>
 GMT是下一代望远镜，拥有24.5米宽的主镜系统。首次观测天空将于2026年末开始，预计将于2028年全面投入运营。
</p>
<figure class="wp-caption aligncenter" id="attachment_11361648" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1I92ES.jpg">
  <img alt="" class="wp-image-11361648 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1I92ES-600x391.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  日全食后，太阳重新露面。(Stan HONDA/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 此次，在智利和阿根廷的所有其它地区，以及秘鲁、厄瓜多尔、巴拉圭、玻利维亚、乌拉圭及哥伦比亚、巴西、委内瑞拉和巴拿马的部分地区均在日全食现象不同程度的影响范围内。只有南美洲北部海岸错过了。
</p>
<figure class="wp-caption aligncenter" id="attachment_11361656" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/107658037_pathofeclipse2july_eng-nc.png">
  <img alt="" class="wp-image-11361656 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/107658037_pathofeclipse2july_eng-nc-600x656.png"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年日全食路径。(NASA)
 </figcaption><br/>
</figure><br/>
<p>
 预计南美洲还能看到下一次——2020年12月14日的日全食。
</p>
<p>
 据“国家地理杂志”（National Georaphic Magazine）网站显示，这是2017年以来全球首见的日全食，而且是发生在全球对观测和研究太阳准备最充分的地方。
</p>
<figure class="wp-caption aligncenter" id="attachment_11361650" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/000_1I92UW.jpg">
  <img alt="" class="wp-image-11361650 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/000_1I92UW-600x284.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  从飞机上看日全食。(Ariel MARINKOVIC/AFP)
 </figcaption><br/>
</figure><br/>
<p>
 拉西亚天文台（La Silla Observatory）天文学家琼斯（Matias Jones）说：“日全食全貌可透过天文台观测的情况非常罕见，上次是在1991年发生过。”拉西亚天文台隶属欧洲南方天文台（European Southern Observatory）。#
</p>
<p>
 参考视频：
 <div class="video_fit_container">
 </div>
</p>
<p>
 责任编辑：李寰宇
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11361462.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11361462.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11361462.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/3/n11361462.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

