### 入疆游客被中共偷装软件 7.3万项内容遭监控
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1148108450-600x400.jpg"/>
<div class="red16 caption">
 <p>
  海外媒体联合调查发现，中共边检人员在游客手机上秘密安装监控应用软件。图为2019年6月4日，新疆儿童在监控镜头下走过。 (GREG BAKER / AFP)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月03日讯】（大纪元记者徐简综合报导）当你进入到新疆旅游，在手机上跟家人或朋友进行亲密私人谈话的时候，再也想不到，有第三方正在偷听。
</p>
<p>
 外媒报导，中共对新疆自由的箝制力度再加大。除了把数十到上百万新疆本地人拘禁到
 <span href="http://www.epochtimes.com/gb/tag/%E5%86%8D%E6%95%99%E8%82%B2%E8%90%A5.html">
  再教育营
 </span>
 、在街道和清真寺装设脸部辨识监视器之外，又把监控黑手伸向了外来游客、商人和其他到访者。
</p>
<p>
 英国《卫报》、美国《纽约时报》、德国《南德意志报》及德国网络安全公司Cure53进行了联合调查，发现当游客从中亚国家入境中国新疆时，必须交出手机供中共边防人员检查。这时边检人员就会在手机上秘密安装监控应用软件。
</p>
<h4>
 中共偷装手机软件
</h4>
<p>
 中共当局自称，每年约有1亿中国和外国游客访问新疆地区，包括中国籍与外国籍游客。
</p>
<p>
 媒体调查发现，当游客从中亚国家入境中国新疆时，必须把手机解锁，然后交给中共边防人员检查，游客的其它装置如相机也要交出来。这些装置被送到不同房间，稍后才送回。这个过程中，边检人员就会在手机上秘密安装一个监控应用软件。
</p>
<p>
 《卫报》说，iPhone手机被插入一个读取机扫描，Andriod手机则被安装监视app，大部分手机在送回来时该app已被移除，但部分手机上还有。《VICE》杂志旗下科技频道报导，中共当局并不是透过Google商店替Android手机安装软件，而是直接通过外部装置将监控应用软件插入手机之中。
</p>
<h4>
 可怕的“蜜蜂采蜜”
</h4>
<p>
 《卫报》、《纽约时报》、《南德意志报》等媒体通过手机上的应用软件副本，进行逆向工程操作，从而发现了这个监控程序的运作细节。该应用程序的代码自称为CellHunter，但显示的图标名称为蜂采（Fēngcǎi ，指蜜蜂采集花粉）。
</p>
<p>
 那么这个蜜蜂要采集什么呢？调查发现，该应用程序有两个主要功能：从旅行者的设备中提取个人和私人信息，并搜索“可疑文件”。
</p>
<p>
 研究表明，该应用程序搜集手机主人的电子邮件、联系电话、短信消息、社交媒体账户标识符和有关手机的其它详细信息，之后将其发送到边境办公室本地内联网（Intranet）上的服务器上。
</p>
<p>
 虽然没有证据证实，该程序会追踪旅客的行踪，但是它收集了手机所连接的信号塔详细信息，再加上其它手机资料、护照详细信息，通过移动电话塔的连接等，就能用来跟踪游客。
</p>
<h4>
 7万3千项“违禁内容”
</h4>
<p>
 那么“蜂采”搜索的“可疑文件”是什么呢？ 外媒对“蜂采”进行了分析，发现它在手机的照片、视频、文件或录音档中搜索约73,000个文件，也就是说，被中共当局认定“有问题”的内容，高达7万3千项。
</p>
<p>
 “蜂采”把每个文件都根据其字节大小和“#”（一种数字指纹）来标识。它在手机中搜索大小相同的文件后，就为该文件生成一个“数字指纹标示”。如果文件大小和“数字指纹标识”都与“违禁”列表中的内容匹配，“蜂采”就会记录下来。
</p>
<p>
 在外媒分析的样本中，“违禁清单”上除了恐怖主义宣传材料外，还有和恐怖主义无关的“违禁”内容，这包括达赖喇嘛的著作、关于新疆历史和文化的著作，以及批评中共干预台湾的内容。还有美国作家格林（Robert Greene）的手册《战争的33种战略》。
</p>
<p>
 令人困惑的是，一首日本重金属歌曲的音频文件也在违禁之列——日本乐队不洁坟冢（Unholy Grave）的《因果》，这首歌被包括在内的原因尚不明确，
</p>
<p>
 人权观察组织（Human Rights Watch）中国研究员王松莲说，中共经常打着打击恐怖主义的幌子，来打压正常的宗教活动，“在新疆，隐私是一种‘基本权利’——一旦你失去了你的隐私权，你就会害怕信奉你的宗教、不敢说出你的想法。”#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11362221.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11362221.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11362221.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/3/n11362221.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

