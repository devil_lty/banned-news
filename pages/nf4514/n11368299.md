### 《归途》亚洲首映 姜光宇魏德圣畅谈人性
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061040052384-600x400.jpg"/>
<div class="red16 caption">
 <p>
  获得美国阿克莱德电影节3项主要大奖的剧情片《归途》，7月5日晚间在台北真善美剧院首映，男主角姜光宇（右）与台湾名导演魏德圣（左）出席映后座谈。（陈柏州／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月06日讯】（大纪元记者钟元台北报导）获得美国阿克莱德电影节3项主要大奖的剧情片《
 <span href="http://www.epochtimes.com/gb/tag/%E5%BD%92%E9%80%94.html">
  归途
 </span>
 》，7月5日晚间在台北真善美剧院首映，亚洲首映会特邀男主角
 <span href="http://www.epochtimes.com/gb/tag/%E5%A7%9C%E5%85%89%E5%AE%87.html">
  姜光宇
 </span>
 与台湾名导演
 <span href="http://www.epochtimes.com/gb/tag/%E9%AD%8F%E5%BE%B7%E5%9C%A3.html">
  魏德圣
 </span>
 座谈，两人相见欢与观众互动愉快。前驻法国代表吕庆龙大使说，很高兴有机会参加好片在台湾的首映。多位观众表示，“影片非常感人”。
</p>
<h4>
 <span href="http://www.epochtimes.com/gb/tag/%E5%A7%9C%E5%85%89%E5%AE%87.html">
  姜光宇
 </span>
 ：非常喜欢台湾
</h4>
<figure class="wp-caption aligncenter" id="attachment_11368671" style="width: 600px">
 <img alt="" class="wp-image-11368671 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061036512384-600x400.jpg"/>
 <br/><figcaption class="wp-caption-text">
  获得美国阿克莱德电影节3项主要大奖的剧情片《
  <span href="http://www.epochtimes.com/gb/tag/%E5%BD%92%E9%80%94.html">
   归途
  </span>
  》，7月5日晚间在台北真善美剧院首映，男主角姜光宇（中）与台湾名导演
  <span href="http://www.epochtimes.com/gb/tag/%E9%AD%8F%E5%BE%B7%E5%9C%A3.html">
   魏德圣
  </span>
  （左）出席映后座谈，两人相见欢与观众互动愉快。（陈柏州／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 美国
 <span href="http://www.epochtimes.com/gb/tag/%E6%96%B0%E4%B8%96%E7%BA%AA%E5%BD%B1%E8%A7%86%E5%9F%BA%E5%9C%B0.html">
  新世纪影视基地
 </span>
 出品的剧情片《归途》，5日晚间首映的座谈会，姜光宇曾因在以连续剧《雍正王朝》中饰演三阿哥而出名，他说非常喜欢台湾，并分享来台第一天就在脸书发文：“还似旧时游上苑，车如流水马如龙，花月正春风。”因信仰不得已离开中国的他感受台湾的自由民主，与对岸就是两个世界，“这几天我去了台北四大夜市很开心，希望台湾人民能珍惜，你们现在自由民主的生活”。
</p>
<p>
 魏德圣以《海角七号》享誉国际，姜光宇形容《海角七号》“是那种能触动心弦，会让你的泪腺发酸的电影”。主持人周怡怡问魏德圣怎么拍出能让大家都感动的电影，他分享，“就是人性”“电影讲人文、讲关怀；就是你面对整个社会，心胸是开的，去接纳不同时代、接纳不同环境，站在角色的心情在看世界，这样子就很容易打动人。”
</p>
<p>
 姜光宇笑问魏德圣下一部电影，有无大陆背景40岁左右的男性角色？当场观众哄堂大笑。魏德圣笑说：“好好努力，因为现在还没进行到那个阶段”。主持人问魏德圣，一个好的演员应该要具备的特质，他表示，“时间是淬炼一个演员最重要的媒介，还有经历会让你的感受力增加，就会转换到戏剧表演上。基本上发现愈好的演员，都是上了35岁的年纪，才会成为真正成熟的演员。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11368677" style="width: 600px">
 <img alt="" class="wp-image-11368677 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061037072384-600x400.jpg"/>
 <br/><figcaption class="wp-caption-text">
  获得美国阿克莱德电影节3项主要大奖的剧情片《归途》，7月5日晚间在台北真善美剧院首映，男主角姜光宇与台湾名导演魏德圣出席映后座谈，两人相见欢与观众互动愉快。（陈柏州／大纪元）
 </figcaption><br/>
</figure><br/>
<h4>
 前驻法代表吕庆龙：《归途》是上乘之作
</h4>
<p>
 《归途》讲述了一个迷失的生命重新找到生命的意义，走上了回归永恒生命道路的故事。吕庆龙受访表示，这部片剧情的编排、镜头的运转、拍摄的技巧来讲，都是属于上乘，以观众看电影来讲，会觉得视觉效果非常好。他说，“生老病死是人生的必然，你得癌症万般无奈但没有选择，但男主角不愿意去投降、屈服，那种精神是值得赞扬，非常令人感动的地方；也希望看过片子的人口耳相传。”
</p>
<p>
 有观众提问姜光宇，“一个演员要保持艺术的长久，他必须要怎么去做？”他表示，做为一个演员要保持对艺术的热心、热诚、热情，和对生活的好奇心。你得去找到生活中触动你心里面，最柔软的那些东西、那些感触，建议大家多看书和多出去旅行。
</p>
<h4>
 魏德圣：多看电影吸收不同人的生命经历
</h4>
<figure class="wp-caption aligncenter" id="attachment_11368663" style="width: 600px">
 <img alt="" class="wp-image-11368663 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061038412384-600x400.jpg"/>
 <br/><figcaption class="wp-caption-text">
  获得美国阿克莱德电影节3项主要大奖的剧情片《归途》，7月5日晚间在台北真善美剧院首映，男主角姜光宇（右）与台湾名导演魏德圣（左）出席映后座谈，两人相见欢与观众互动愉快。（陈柏州／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 魏德圣补充，除了看书、旅行，还有多看电影，在短短的1、2小时之内，很多导演或作者，就要把他的生命和经历，浓缩在电影里面，而你只要花新台币200多元，就可看到一个人生命累积的东西，“你就可以储存在你的心里面，那是一个最好的吸收，你在阅读、旅行、看一部电影，其实都是在吸收不同人的生命经历，我觉得很重要，所以也期待更多不一样多元的电影可以被出现。”
</p>
<p>
 姜光宇也分享拍片的神奇经历，“《归途》最后那一场戏很有意思，我们走到海滩上要拍日出有意境的场景”，当时在加拿大是早上4点多，看到有无数的群鸥慵懒的趴在沙滩上，当我们摄影的机器架好之后，想说这些鸟都趴在这儿不好看，“我们希望拍出一个秋水共常天一色，类似烟霞与孤鹜齐飞，这样子非常有意境的场景，才刚一想完，这些鸟听话极了就开始飞。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11368666" style="width: 600px">
 <img alt="" class="wp-image-11368666 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061039322384-600x400.jpg"/>
 <br/><figcaption class="wp-caption-text">
  获得美国阿克莱德电影节3项主要大奖的剧情片《归途》，7月5日晚间在台北真善美剧院首映，男主角姜光宇（中）与台湾名导演魏德圣（左）出席映后座谈，两人相见欢与观众互动愉快。（陈柏州／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 “最后那个镜头不是特效做的”姜光宇强调。他说，那天真的是群鸥开始在天上飞，“而且特别有意思，就在我们的镜头范围里面飞，而且不飞远，当第一个机位拍完后，换个机位，群鸥又飞了一遍，听话极了”。姜光宇感谢老天爷非常的给力，最后一场戏营造了美好的意境。
</p>
<h4>
 观众：《归途》非常有净化人心的作用
</h4>
<p>
 观众冯国徐和太太韩惠真一起来看《归途》。他说，电影最感动人的地方，就是女主角很善良，让人感觉到她无私，“我们这个社会有时就怕惹事，她很勇敢站出来愿意去掏钱，希望不要责备一时有贪念偷钱的孩子。”还有剧中男主角得癌症了，他没有被病魔牵挂着，还很勇敢的走出来。“看这部电影后的感受，人与人之间还是要多与人为善，《归途》非常有净化人心的作用。”
</p>
<p>
 韩惠真表示，真的很感动！女主角的善良，引导一个大明星走到最后的人生阶段，能够经由法轮功翻转他的人生，因为他身体健康了可以继续走下去，然后癌细胞也没了。看了电影《归途》之后，“我们夫妻有时间行有余力，也会开始多接触法轮功。”她表示，“我们这个年纪不需要很剧烈的运动，法轮功的静坐能让人静下心来，做一些沉思也不错；我们也愿意去推广，让朋友看《转法轮》。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11368667" style="width: 450px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907061040252384.jpg">
  <img alt="" class="wp-image-11368667 size-medium" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061040252384-450x300.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年7月5日晚间，中华世界客家妇女协会前理事长古秋香观看获得美国阿克莱德电影节3项主要大奖的剧情片《归途》在台北真善美剧院首映。（陈柏州／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 中华世界客家妇女协会前理事长古秋香说，看完电影感受最重要是有一个均衡的人生，也对法轮功有不同的认识，可以让一个人的生命重新找到希望。她表示，剧中女主角小雪对完全不认识的人的关怀，是全心的付出，对人性的传达是丰富及温度。现在的社会比较冷漠，“这部电影让我们重新思考人与人之间关怀，我一看完电影就发讯息到许多群组，推荐朋友来看，可以让这个社会多一点温暖。”
</p>
<p>
 龙岩公司业务经理刘恒硕表示，这部电影会带来正向的力量，没有利益的得到，还是愿意帮助人，“女主角去帮助失智老人，这让我很感动。”他说，人生都会有起伏，不可能是直线，当起伏时需要被鼓舞，看这部片不管遇到什么挫折还是生病，都勇敢的去面对。“让我们知道不论遇到什么事情，只要坚持勇敢面对，一定会看到彩虹。”这部电影不但鼓励自己，也可以鼓励大家，非常推荐大家来看。
</p>
<figure class="wp-caption aligncenter" id="attachment_11368668" style="width: 450px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907061040342384.jpg">
  <img alt="" class="wp-image-11368668 size-medium" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061040342384-450x300.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年7月5日晚间，国际同济会台北会前会长刘邦民观看获得美国阿克莱德电影节3项主要大奖的剧情片《归途》在台北真善美剧院首映。（陈柏州／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 国际同济会台北会前会长刘邦民说，这部电影的主题真的很棒，起死回生，振奋人心。信仰很重要代表的是一个力量，自由也很重要，“因为有自由才能维护我们的信仰，电影传递的讯息，就是全世界都能讲法轮功，只有大陆不行（迫害法轮功），我们应该要珍惜生命。”
</p>
<p>
 周女士说，看《归途》真是蛮感动的，“可能因为我们年纪大，而且历程经历了很多，看起来生命并不是有什么名利、就是过眼云烟确实是如此。”她说，《归途》很用心把这样的意境显示出来，法轮功这些人努力的过程，其实还有很多更辛苦的事情，应该都还没有真实呈现出来。她表示，法轮功的朋友们，非常辛苦、非常坚定，然后也很善良努力的在争取，“真的是佩服，真的是感动”。
</p>
<p>
 刘先生表示，这个故事起承转合的点，都是那么的令人惊艳，衔接的那么surprise，“不像是我们传统的看一部电影，而是以电影的艺术形式讲一个真实的故事。”他说，看《归途》有一些很震撼心灵的感受，一个面对生命即将结束的人，最后能够脱胎换骨，有了一个新的生命，这可以给大家一个很大的启示，癌细胞真的会好，“因为他有机缘接触到《转法轮》，最后他的癌细胞就消失了”。
</p>
<figure class="wp-caption aligncenter" id="attachment_11368680" style="width: 600px">
 <img alt="" class="wp-image-11368680 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061039072384-600x400.jpg"/>
 <br/><figcaption class="wp-caption-text">
  获得美国阿克莱德电影节3项主要大奖的剧情片《归途》，7月5日晚间在台北真善美剧院首映，男主角姜光宇（中）与台湾名导演魏德圣（左）出席映后座谈，两人相见欢与观众互动愉快。（陈柏州／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 姜光宇分享以前在演艺圈追求名利，但日益开始富足时，他最担心的就是死亡，也看到朋友、亲人的病逝，他不知道生命最终的归宿在哪儿？“最终机缘巧合之下，看到了《转法轮》这本书，指引我走上修炼之路。”
</p>
<p>
 “你知道看完这本书，我的感受是什么吗？”姜光宇说，在他之前的人生，“我仿佛在一个黑房子里面，当然每个人都会有这样的想法，你不知道明天会发生什么，人从出生之后，只有一件事情非常清晰，就是走向死亡。”他表示，但是他开始修炼之后，“我找到了一条光明的路，而且知道生命到底会怎么走，所以我特别建议大家去看看这本书，也许你能找到困惑已久问题的答案。”
</p>
<figure class="wp-caption aligncenter" id="attachment_11368673" style="width: 400px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907061038142384.jpg">
  <img alt="" class="wp-image-11368673 size-full" src="http://i.epochtimes.com/assets/uploads/2019/07/1907061038142384.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  获得美国阿克莱德电影节3项主要大奖的剧情片《归途》，7月5日晚间在台北真善美剧院首映，男主角姜光宇出席映后座谈。（陈柏州／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11368299.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11368299.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nf4514/n11368299.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/6/n11368299.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

