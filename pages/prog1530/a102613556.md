### 【禁闻】55万港人七一大游行 历年最高
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月02日讯】
  <strong>
   55万港人七一大游行 历年最高
  </strong>
 </p>
 <p>
  香港主权转移22周年，民阵发起的七一游行，在7月1号下午3点左右正式从维多利亚公园出发。今年七一游行主题是“撤回恶法 林郑下台”，诉求包括重启政改，释放所有政治犯，撤销暴动定性，彻查612镇压。
 </p>
 <p>
  中午时分，示威人群中有少数身份不明的人士包围立法会，并以铁枝、铁笼车等物品冲击立法会玻璃门。受此影响，民阵将游行终点，由原来的政府总部，改为遮打道。
 </p>
 <p>
  这次游行队伍浩浩荡荡，整个轩尼诗道爆满，现场秩序理性和平。晚上9点前，游行龙尾才到达金钟太古广场。9:30民阵公布，这次集会人数逾55万人，比03年的50万人、2014年占领行动前夕的51万人还要高。
 </p>
 <p>
  <strong>
   路透：
   <span href="https://www.ntdtv.com/gb/非洲猪瘟.htm">
    非洲猪瘟
   </span>
   死猪数是中共公布的两倍
  </strong>
 </p>
 <p>
  “路透社”6月29号报导说，大陆死于
  <span href="https://www.ntdtv.com/gb/非洲猪瘟.htm">
   非洲猪瘟
  </span>
  和为了防疫而遭扑杀的猪只数量，是中共官方公布数据的两倍。
 </p>
 <p>
  报导引述3名企业高管的估计说，大陆母猪数量过去一年锐减了40%至50%。但中共官方却声称，今年5月全国母猪的存栏数量，较去年同期只减少了23.9%。
 </p>
 <p>
  报导还引述四名匿名农民和一名官员的话说，最近在广东、广西和湖南等地爆发的疫情都没有被统计。广西博白县一名猪农表示，她饲养的猪只几乎全部病死，但当局不准通报，当地还有猪农因披露疫情被指“散播谣言”遭拘留。
 </p>
 <p>
  <strong>
   河北省深泽县一天绑架8名
   <span href="https://www.ntdtv.com/gb/法轮功学员.htm">
    法轮功学员
   </span>
  </strong>
 </p>
 <p>
  中共对法轮功的迫害仍然在持续，据“法轮大法明慧网”报导，5月31号，河北省深泽县, 一天之内就有8名
  <span href="https://www.ntdtv.com/gb/法轮功学员.htm">
   法轮功学员
  </span>
  被非法绑架。
 </p>
 <p>
  报导说，这次绑架行动是由河北省深泽县政法委、公安局策划，各乡镇派出所具体执行的。他们统一在当天早上七点左右，非法闯入这些法轮功学员家中抄家，抢走私人财物，然后将学员非法带走拘留，同时还向这些学员每人勒索了数百元现金。
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102613556.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102613556.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102613556.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/01/a102613556.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

