### 遭受中共蹂躏的中国文艺界精英们（1）
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/1-19-1.jpg" target="_blank">
  <figure>
   <img alt="遭受中共蹂躏的中国文艺界精英们（1）" src="https://i.ntdtv.com/assets/uploads/2019/06/1-19-1.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  42岁北京乐手于宙，45岁广东珠海知名画家郑艾欣，40岁吉他演奏家、歌手李京生因修炼法轮功被迫害致死。（大纪元合成图）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月23日讯】据明慧网统计，大陆文艺界法轮功学员自1999年7月遭中共迫害以来，至少有9人被迫害致死，至少75人遭绑架，其中遭非法判刑或非法劳教迫害的至少50人次。
 </p>
 <p>
  1992年法轮功在中国开传，继而迅速洪传于神州大地，使整个社会道德回升，祛病效果神奇，给人们带来福祉；各阶层人士相继而来，修者日众，上亿人深受裨益。他们中不乏各界精英，包括享誉中外的画家、出类拔萃的音乐家、演艺高超的演员、多才多艺的诗人。
 </p>
 <p>
  他们本该享受人生中最美好的时光，尽情展现才华，造福人类，然而1999年中共
  <span href="https://www.ntdtv.com/gb/江泽民.htm">
   江泽民
  </span>
  集团对信仰“
  <span href="https://www.ntdtv.com/gb/真、善、忍.htm">
   真、善、忍
  </span>
  ”的民众发动了一场持续至今的浩劫，这些文艺界的精英们成为中共蹂躏的重点对象，他们被非法抓捕、关押、劳教、判刑、被失踪、受折磨，甚至被虐杀⋯⋯
 </p>
 <p>
  以下部分案例讲述他们不尽的悲哀以及中共的邪恶本性。
 </p>
 <h3>
  <strong>
   知名画家
   <span href="https://www.ntdtv.com/gb/郑艾欣.htm">
    郑艾欣
   </span>
   遭劳教洗脑迫害离世
  </strong>
 </h3>
 <p>
  广东省珠海市法轮功学员、知名画家
  <span href="https://www.ntdtv.com/gb/郑艾欣.htm">
   郑艾欣
  </span>
  女士由于坚持修炼法轮功，被洗脑、关押近四年；回家后，遭24小时电话监控，身心遭受巨大伤害、精神极度压抑，于2012年9月1日，不幸英年早逝。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102607392" style="width: 600px">
  <img alt="" class="size-medium wp-image-102607392" src="https://i.ntdtv.com/assets/uploads/2019/06/2-14-600x446-600x446.jpg">
   <br/><figcaption class="wp-caption-text">
    郑艾欣（明慧网）
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  2001年3月5日至19日，郑艾欣被非法关押在斗门区“
  <span href="https://www.ntdtv.com/gb/610.htm">
   610
  </span>
  ”（专门迫害法轮功的非法机构）洗脑班迫害2个星期，之后被非法劳教1年、送入广东省三水女子
  <span href="https://www.ntdtv.com/gb/劳教所.htm">
   劳教所
  </span>
  （妇教所）。她始终坚守自己的信仰。
 </p>
 <p>
  2002年4月，非法劳教期满，因没有“转化”（放弃修炼），郑艾欣被关进斗门区“
  <span href="https://www.ntdtv.com/gb/610.htm">
   610
  </span>
  ”3个月，被继续洗脑迫害。期间，她绝食反迫害，并坚持炼功。
 </p>
 <p>
  同年7月24日，她再次被非法刑事拘留、非法劳教2年，于7月29日，戴着脚镣手铐，再次被送进广东省三水女子
  <span href="https://www.ntdtv.com/gb/劳教所.htm">
   劳教所
  </span>
  。
 </p>
 <p>
  他丈夫李正天虽然不修炼法轮功，但理解和尊重妻子的信仰。2003年，他参加在日本举办的世界博览会中国馆的创意设计，击败群雄、一举夺标。2004年，他接受记者采访时，提到他的身体不好，需要妻子回来照顾，并表示，妻子是个好人，只因修炼法轮功、做好人，被中共关进劳教所。
 </p>
 <p>
  约在2004年4月，郑艾欣从劳教所提前获释。回家后，她精神压抑、很少说话。
 </p>
 <p>
  她家的电话全天被监听，她不能自由外出。其丈夫也被下“三不”禁令：不准上电视、不准上报纸、不准出国。由于被非法监控，夫妻俩非常压抑。
 </p>
 <p>
  她善良美丽、才艺出众，丈夫是画家、音乐家、艺术家、哲学家，他们的朋友来自世界各地、全国各地的美术界、影视界、传媒界等。她利用各种机会向人传播法轮功真相。
 </p>
 <p>
  2005年3月至2008年10月底，她修炼法轮功的母亲连续两次被绑架，被非法关进劳教所。对母亲的牵挂、担心，令她倍感精神压力之大。2012年，她终因身体出现多种疾病而离世，年仅45岁。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102607395" style="width: 500px">
  <img alt="" class="size-full wp-image-102607395" src="https://i.ntdtv.com/assets/uploads/2019/06/2015-9-2-mh-zhuhai-zhengaixin-4.jpg">
   <br/><figcaption class="wp-caption-text">
    郑艾欣的作品：金莲呈祥。（明慧网）
   </figcaption><br/>
  </img>
 </figure><br/>
 <figure class="wp-caption aligncenter" id="attachment_102607396" style="width: 497px">
  <img alt="" class="size-full wp-image-102607396" src="https://i.ntdtv.com/assets/uploads/2019/06/2015-9-2-mh-zhuhai-zhengaixin-5.jpg"/>
  <br/><figcaption class="wp-caption-text">
   郑艾欣的作品：箫声吉祥。（明慧网）
  </figcaption><br/>
 </figure><br/>
 <figure class="wp-caption aligncenter" id="attachment_102607397" style="width: 485px">
  <img alt="" class="size-full wp-image-102607397" src="https://i.ntdtv.com/assets/uploads/2019/06/2015-9-2-mh-zhuhai-zhengaixin-6.jpg"/>
  <br/><figcaption class="wp-caption-text">
   郑艾欣的作品：山魂汉韵。（明慧网）
  </figcaption><br/>
 </figure><br/>
 <h3>
  <strong>
   漫画家张之泉遭迫害含冤离世
  </strong>
 </h3>
 <p>
  河北省衡水师范学校讲师、漫画家张之泉，毕业于中央美术学院，退休后学炼法轮功，身心受益。
 </p>
 <p>
  2001年3月19日晚，张之泉被衡水深州市警察贾双万、尚运航从老家深州市西阳台村绑架后，被非法判重刑7年，在河北省第四监狱遭受迫害。
 </p>
 <p>
  在他被非法关押的前4年，身体还十分健康，狱医惊奇地赞叹他70出头的人，心脏还呈现40岁年轻人的健康图像。
 </p>
 <p>
  之后，狱警先后唆使两个刑事犯对他进行人身摧残：任意打骂、往脸上吐唾沫、不让睡觉、逼供、审讯、体罚、惊吓等等。狱警还叫嚣：“我叫谁死在监狱里，他就别想出去。”并断言张之泉一定会死在监狱里。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102607398" style="width: 300px">
  <img alt="" class="size-full wp-image-102607398" src="https://i.ntdtv.com/assets/uploads/2019/06/2010-8-4-zhangzhiquan-600x890.jpg"/>
  <br/><figcaption class="wp-caption-text">
   张之泉（明慧网）
  </figcaption><br/>
 </figure><br/>
 <p>
  残酷的摧残彻底摧垮了张之泉的身体，原本一百四五十斤重的他，只剩了六七十斤，完全脱了相。在家人强烈要求下，他于2007年3月被“保外就医”。到家人去接他时，他已经处于半昏迷状态，被从监狱抬出来时，他的儿子都快认不出他了。
 </p>
 <p>
  回到家后，张之泉不断遭到当地“610”、公安国保的骚扰，退休工资还不发给他。在各方面压力之下，他的身体精神一直不能恢复，于2010年5月1日含冤离世，终年76岁。
 </p>
 <h3>
  <strong>
   世界书画名人李光伟失踪
  </strong>
 </h3>
 <p>
  李光伟，1935年8月出生，江西南昌松柏巷小学退休教师，被国际美术家联合会、世界书画协会、比利时世界文化交流中心、中国书画艺术名人研究院等16家机构联合审定授予“世界书画名人”荣誉称号。其作品及传略载入《中国当代艺术界名人录》第5卷下册、《世界当代书画篆刻家大辞典》、《世界当代著名画家真迹博览大典》等多部大辞典。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102607399" style="width: 132px">
  <img alt="" class="size-full wp-image-102607399" src="https://i.ntdtv.com/assets/uploads/2019/06/2019-6-7-194254-1.jpg"/>
  <br/><figcaption class="wp-caption-text">
   李光伟（明慧网）
  </figcaption><br/>
 </figure><br/>
 <p>
  1998年底，李光伟身患重病，到处求医无效。老伴听说法轮功有起死回生的功效，就劝其炼法轮功。病情严重得不能走路的他，靠老伴用单车送到炼功点。修炼不长时间，他的身体好转，自己可以走去炼功点炼功了。
 </p>
 <p>
  李光伟在家中炼功，公安知道了抄了他的家，抢走了他的法轮功书籍和炼功磁带，并把他抓走。在文革时受过重伤的他，受惊吓过度、旧病复发。回到家心里难受至极，买来纸墨开始写自己的修炼心得体会。他写道：“修炼人都按李老师讲的‘
  <span href="https://www.ntdtv.com/gb/真、善、忍.htm">
   真、善、忍
  </span>
  ’的标准严格要求自己。这是宇宙的特性，在不同层次做好人和更高尚的人。我们修炼法轮功没有错！”
 </p>
 <p>
  他把自己写的体会和揭露媒体欺世谎言的文字用寄信的方式寄给不知真相的人们和自己的画友，因此遭到监视。他曾对家人说：“我出门有人跟踪我。”于是那段时间他都待在家里不出门。
 </p>
 <p>
  2005年7月25日那天，他对家里人说出去走一走就回来，谁知一走就再没回来了。
 </p>
 <p>
  李光伟至今下落不明，哭干了眼泪的老伴和儿女到处找他，也曾向当地派出所报案，也找了各公安部门甚至市长，可他们几乎都以李光伟炼法轮功而推脱，不受理、不帮助找人，甚至还威胁李光伟的女儿“不要自找麻烦”。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102607400" style="width: 600px">
  <img alt="" class="size-medium wp-image-102607400" src="https://i.ntdtv.com/assets/uploads/2019/06/Gucn_68396_20091031662360CheckCurioPic5-600x450-600x450.jpg"/>
  <br/><figcaption class="wp-caption-text">
   李光伟的作品。（明慧网）
  </figcaption><br/>
 </figure><br/>
 <h3>
  <strong>
   歌手被虐杀
  </strong>
 </h3>
 <p>
  2008年，42岁的法轮功修炼者于宙被中共警察假借“迎奥运”之名非法抓捕，当时他正在北京从演唱会返家的途中。被捕后，仅仅11天，他就被虐杀。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102607402" style="width: 472px">
  <img alt="" class="size-full wp-image-102607402" src="https://i.ntdtv.com/assets/uploads/2019/06/2008-4-3-yuzhou1.jpg"/>
  <br/><figcaption class="wp-caption-text">
   于宙（明慧网）
  </figcaption><br/>
 </figure><br/>
 <p>
  于宙的老家是吉林省，当年他以文科状元考入北京大学法语系，通晓多种语言，多才多艺，是公认的才子。1995年，于宙通过朋友了解到法轮功，找到人生真谛，夫妇俩走上了修炼之路。
 </p>
 <p>
  1999年7月，中共发动了对法轮功的迫害，于宙夫妇一直遭到迫害。1999年8月，两人被非法扣留15天，被严刑逼供下，他们没出卖一个其他的法轮功学员。
 </p>
 <p>
  2008年，中共以奥运的名义对法轮功学员开始大规模的非法抓捕。2008年1月26日，于宙在演出结束后与妻子驾车返家，途中夫妻俩被非法拘留，后被送入通州看守所。
 </p>
 <p>
  2月6日，家属接到通知，当赶到北京清河急救中心时，原本因修炼法轮功而身体健康的于宙已离开人世，还戴着呼吸罩，腿已冰凉。
 </p>
 <p>
  之后，北京警察严密封锁消息，不许他的亲属向外界透露消息，并把双方父母的家暗中包围起来，不许别人接近。
 </p>
 <p>
  正值风华正茂的于宙就这样离开了他深爱的人们，生前他创作和演唱了一首首充满真诚与关爱的歌曲。
 </p>
 <figure class="wp-caption aligncenter" id="attachment_102607403" style="width: 471px">
  <img alt="" class="size-full wp-image-102607403" src="https://i.ntdtv.com/assets/uploads/2019/06/4775-3.jpg"/>
  <br/><figcaption class="wp-caption-text">
   于宙在音乐会上。（明慧网）
  </figcaption><br/>
 </figure><br/>
 <p>
  他曾与一对朋友夫妇组成了三人民谣乐队，在北京各地用外文演唱英、法、日等国的乡村歌曲，深受观众欢迎。《我的家》是他的最后一个MV。当2008年这个MV推出时，他已永远离开了人世。
 </p>
 <div class="video_fit_container">
 </div>
 <h3>
  <strong>
   北京吉他演奏家、歌手李京生遭迫害离世
  </strong>
 </h3>
 <figure class="wp-caption aligncenter" id="attachment_102607404" style="width: 151px">
  <img alt="" class="size-full wp-image-102607404" src="https://i.ntdtv.com/assets/uploads/2019/06/5-8-1.jpg"/>
  <br/><figcaption class="wp-caption-text">
   李京生和妻子。（明慧网）
  </figcaption><br/>
 </figure><br/>
 <p>
  李京生，吉他演奏家、歌手，自幼患先天性心脏病，曾被医院判“死刑”。1998年5月，他开始修炼法轮功，从此重获新生。他和妻子万喻的演唱艺术事业渐入佳境。
 </p>
 <p>
  2001年6月25日，李京生在天安门广场打坐炼功，向人们证实法轮功的美好，并高喊“法轮大法是正法，还我们一个合法的炼功环境！”他被警察狠踢胸部致伤，被绑架、非法劳教；9月21日，被劫持到团河劳教所。
 </p>
 <p>
  在那里，他绝食反迫害，被多次劫持至“集训队”加重折磨，遭殴打、罚坐，不让睡觉、不给吃饱，致身体伤痕累累，并被非法延期10个月关押。
 </p>
 <p>
  从劳教所回来后，李京生极度虚弱、反应迟钝，于2004年12月13日不幸离世，年仅40岁。
 </p>
 <p>
  李京生的妻子万喻，当时住在北京海淀区。2005年9月28日晚，她暂借住在一位法轮功学员家，遭绑架，被非法关押在海淀区清河看守所，后被非法劳教2年，之后又从北京调遣处被秘密遣送到河北省高阳劳教所关押迫害。#（待续）
 </p>
 <p>
  ──转自《大纪元》
 </p>
 <p>
  （责任编辑：王馨宇）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102607364.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102607364.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102607364.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/23/a102607364.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

