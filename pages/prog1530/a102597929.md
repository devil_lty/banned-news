### “中共无法治”最大例证：610办公室
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月11日讯】香港人反送中条例，是因为恐惧中共治下无法治。而大陆无法治的一个典型例证，就是
  <span href="https://www.ntdtv.com/gb/迫害法轮功.htm">
   迫害法轮功
  </span>
  的
  <span href="https://www.ntdtv.com/gb/610办公室.htm">
   610办公室
  </span>
  。一位律师用亲身经历，揭开610操纵司法的黑幕。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/610办公室.htm">
   610办公室
  </span>
  ，成立于1999年6月10日，全称是中共“中央处理法轮功问题领导小组办公室”。是时任中共党魁江泽民，为
  <span href="https://www.ntdtv.com/gb/迫害法轮功.htm">
   迫害法轮功
  </span>
  ，成立的非法组织。
 </p>
 <p>
  横河：“说它是非法组织，是因为它成立没有经过任何立法和行政当局的授权。虽然它归属于中共系统，但是成立的时候，甚至都没有经过中共这个系统的授权程序。没有注册、没有被授权的机构，即使按照中共自己的宪法和法律，也是非法的。”
 </p>
 <p>
  610成立之初，唯一作用就是指挥和推动全国迫害法轮功。
 </p>
 <p>
  据专家统计，中国各省、市、街道各级，共有约一千个610办公室。610通过中共党务系统，可任意指挥中国所有单位，无视宪法保障的公民信仰自由权，非法展开对法轮功学员的监控、抹黑宣传、法外关押、强迫洗脑、构陷判刑、甚至酷刑和虐杀。
 </p>
 <p>
  原上海维权律师李明，就揭秘了610操纵司法，未审先判的内幕。
 </p>
 <p>
  维权律师 李明：“在每一个法轮功学员被构陷绑架之后，
  <span href="https://www.ntdtv.com/gb/政法委.htm">
   政法委
  </span>
  和610办公室，他们会针对每一个个案，召开一个所谓的协调会议和联席会议。你比如说我曾经参与的案子，就是（610办）把公、检、法、司法局，律师管理科的科长、审判长、公诉科长，召集到一块。针对每一个个案，他们确定一个审判的方略，确定一个大体的刑期。针对这个案子，这就是不能更改的一道命令，任何人不能逾越。这就是典型的未审先判。明明都是些冤假错案，但是他们就是强行进行有罪判决。”
 </p>
 <p>
  李明透露，610还会派人旁听、监控整个庭审过程。法官和检察官出于恐惧，只能按照中共的意志办案。
 </p>
 <p>
  李明：“他们布置公检法的人员，去旁听、去监督法院和检察机关审判和公诉的程序，对这些工作人员形成巨大的压力。导致即便这些审判人员和公诉人员认为，法轮功学员坚持自己的信仰是无罪的，但是他们在那种高压的环境下，也不敢自由表达。”
 </p>
 <p>
  但仍有一些律师不向中共屈服，依法为法轮功学员做无罪辩护，610就展开非法打压，断绝敢言律师的生路。
 </p>
 <p>
  李明：“我是因为为法轮功学员无罪辩护，就受到610系统、国保国安系统的迫害和打压。当时他们（找我）谈的内容就是说，你不能在上海执业，你也不能在上海找另外的律师事务所。不允许你过（律师）年检。另外，你上监控黑名单，24小时监控监听，对你的手机24小时GPS定位系统和行动路线进行监控。被逼无奈，我们离开上海，到其他省份找工作期间，这种恶魔般的威胁，如影随形。”
 </p>
 <p>
  在江泽民及其亲信掌权期间，610和
  <span href="https://www.ntdtv.com/gb/政法委.htm">
   政法委
  </span>
  系统权力急速扩大，形成所谓第二中央，对继任者习近平构成实质威胁。在随后习王的反腐运动中，610被不断弱化。中共中央三级的610头目接连落马，基层610也有多人遭到贪腐调查；2018年中共机构改革，又将610撤并划入中共政法委和公安部。但迫害政策，并未停止。
 </p>
 <p>
  横河：“现在知道的是2018年底，省一级的机构调整已经完成了，省的610办公室，整体划到省委政法委里面。这说明，一方面，作为过去20年，中共迫害法轮功的核心机构和象征，610组织和名称从公众面前消失，本身就说明这场迫害难以为继，并且彻底失败了。另一方面，尽管对外撤销了，但是610的功能甚至编制，都可能还单独存在于政法委里面。也就是说，这个机构迫害法轮功的职能还继续存在，即使他的名称已经不再使用。这也和中共迫害法轮功的严重程度并没有减弱相符。”
 </p>
 <p>
  中共残酷迫害持续20年，但法轮功学员始终和平理性坚守信仰，国际社会也用越趋严厉的谴责、以及对迫害者个人启动追责，制止这场21世纪最大的人权灾难。
 </p>
 <p>
  新唐人记者林澜纽约报导
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102597929.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102597929.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1530/a102597929.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/10/a102597929.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

