### 美中贸易战的4个获益国家 台湾上榜
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1151905547-600x400.jpg"/>
<div class="red16 caption">
 在美中贸易战的影响下，越南、台湾、孟加拉和韩国成为输美商品增加的赢家。图为2019年5月21日，世界最大的自行车制造商——台湾巨大集团在台中的工厂。在投资中国多年之后，该公司最近也在台湾扩厂，成为回流的台商之一。(SAM YEH/AFP/Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月05日讯】（大纪元记者陈俊村编译报导）
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E4%B8%AD%E8%B4%B8%E6%98%93%E6%88%98.html">
  美中贸易战
 </span>
 开打至今已有一年，目前尚在持续进行之中，但已经有4个从中获益的国家，包括
 <span href="http://www.epochtimes.com/gb/tag/%E8%B6%8A%E5%8D%97.html">
  越南
 </span>
 、
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%B0%E6%B9%BE.html">
  台湾
 </span>
 、
 <span href="http://www.epochtimes.com/gb/tag/%E5%AD%9F%E5%8A%A0%E6%8B%89.html">
  孟加拉
 </span>
 和
 <span href="http://www.epochtimes.com/gb/tag/%E9%9F%A9%E5%9B%BD.html">
  韩国
 </span>
 ，它们出口美国的商品已呈现显著增长。
</p>
<p>
 据CNN报导，在
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E4%B8%AD%E8%B4%B8%E6%98%93%E6%88%98.html">
  美中贸易战
 </span>
 的冲击下，美国人减少购买中国商品，转而购买其它亚洲国家生产的商品，以期避免关税的提高。
</p>
<p>
 美国人口普查局在7月3日公布的资料显示，在美中两国断断续续进行贸易协商超过一年的时间里，这个趋势已经形成，而且持续到（已公布资料的）5月。
</p>
<p>
 就今年前5个月而言，美国从中国进口的商品较去年同期减少了12%。相较之下，美国从
 <span href="http://www.epochtimes.com/gb/tag/%E8%B6%8A%E5%8D%97.html">
  越南
 </span>
 、
 <span href="http://www.epochtimes.com/gb/tag/%E5%8F%B0%E6%B9%BE.html">
  台湾
 </span>
 、
 <span href="http://www.epochtimes.com/gb/tag/%E5%AD%9F%E5%8A%A0%E6%8B%89.html">
  孟加拉
 </span>
 和
 <span href="http://www.epochtimes.com/gb/tag/%E9%9F%A9%E5%9B%BD.html">
  韩国
 </span>
 进口的商品，较去年同期分别增加了36%、23%、14%和12%。
</p>
<p>
 对于美国进口商而言，美国政府对中国商品加征关税已经使中国制造的棒球帽、行李箱、自行车和手提袋等生活消费品变得比较贵，同时影响多种机械和工业用品，其中包括洗碗机、洗衣机、烘干机和净水器的零件。
</p>
<figure class="wp-caption aligncenter" id="attachment_11366593" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1151905538-1.jpg">
  <img alt="" class="wp-image-11366593 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1151905538-1-600x405.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年5月21日，台湾巨大集团在台中的工厂。(SAM YEH/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<p>
 密苏里州公司“美国帽子”（Cap America）从中国进口大多数的帽子，然后在当地绣上图案和字样。该公司正在尝试从孟加拉寻找新的供应商，但这些订单只占了今年进口总量的20%左右，因为他们还必须测试新供应商的生产品质。
</p>
<p>
 美国商会（United States Chamber of Commerce）在5月所进行的调查指出，大约40%的美国公司正在考虑或已经将部分生产线移出中国。而在已经将生产线移出中国的公司中，有大约四分之一是移转到东南亚。
</p>
<p>
 尽管外国公司将生产线移出中国者在今年大幅增加，但早在美国政府开始加征关税之前，有些生产已经移转到工资较低的国家。
</p>
<p>
 台湾和韩国比较专注于半导体之类的高科技产品的生产，但越南与孟加拉仍以相对较低的工资来吸引外资投资，以从事消费性产品的制造，例如衣服和鞋子。
</p>
<figure class="wp-caption aligncenter" id="attachment_11366596" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-51344750.jpg">
  <img alt="" class="wp-image-11366596 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-51344750-600x421.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2000年2月25日，台湾宏碁公司在中国福建省投资的一家组装工厂。(STEPHEN SHAVER/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<h4>
 台商回流 投资金额破4,346亿元
</h4>
<p>
 随着美中贸易战效应持续扩大，有越来越多台湾厂商将产线从中国移回台湾，不但为台湾创造就业机会，也带动了金融业的成长。
</p>
<p>
 中华民国经济部在周四（7月4日）核准瀚宇彩晶等三家台商回台投资。自行政院于今年1月1日实施“欢迎台商回台投资行动方案”迄今，已有84家台商通过审查，其投资金额超过4,346亿元，预估可以创造近39,110个就业机会。
</p>
<p>
 而台商回台投资也促使今年第1季的台湾固定资产投资大幅增加。经济部公布的资料显示，第1季制造业不包含土地的固定资产投资的增幅达29.8%，为2011年第1季以来最高，也是继2008年金融海啸复苏后的最大增幅。#
</p>
<p>
 责任编辑：林琮文
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11366572.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11366572.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11366572.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/5/n11366572.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

