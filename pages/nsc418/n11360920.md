### 美军：中共在南海试射导弹违背承诺
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-671379744-600x400.jpg"/>
<div class="red16 caption">
 <p>
  中共日前在南海测试反舰导弹。美军批评此举乃恫吓邻国的威胁手段，而且违背了中共先前不将南海岛礁军事化的承诺。图为2017年4月21日，南沙群岛美济礁的空拍照。(TED ALJIBE/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月03日讯】（大纪元记者陈俊村编译报导）正当美中两国达成贸易战暂时停火协议之际，中共于上个周末在
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%97%E6%B5%B7.html">
  南海
 </span>
 多次
 <span href="http://www.epochtimes.com/gb/tag/%E6%B5%8B%E8%AF%95.html">
  测试
 </span>
 反舰
 <span href="http://www.epochtimes.com/gb/tag/%E5%AF%BC%E5%BC%B9.html">
  导弹
 </span>
 。对此，美军批评此举“令人担忧”，形同恫吓邻国的威胁手段，而且违背了中共先前不将南海
 <span href="http://www.epochtimes.com/gb/tag/%E5%B2%9B%E7%A4%81.html">
  岛礁
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E5%86%9B%E4%BA%8B%E5%8C%96.html">
  军事化
 </span>
 的承诺。
</p>
<p>
 美国CNBC电视台引述两名政府官员的话说，中共正在
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%97%E6%B5%B7.html">
  南海
 </span>
 进行一系列的
 <span href="http://www.epochtimes.com/gb/tag/%E5%AF%BC%E5%BC%B9.html">
  导弹
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E6%B5%8B%E8%AF%95.html">
  测试
 </span>
 ，而且至少发射了一枚导弹到海中。这段测试期间持续到周三（7月3日），预计中共还会在结束之前进行测试。
</p>
<p>
 官员说，尽管美军目前有船舰在南海航行，但它们并未接近导弹测试地区，所以没有危险。然而，这项测试却“令人担忧”。
</p>
<p>
 除了贸易战、美国制裁和台湾问题之外，南海也是美中关系的爆发点之一。为应对中共在南海日益扩张的军事态势，美军多次在南海行使自由航行权。
</p>
<p>
 在上个月中旬，美军的里根号航空母舰（USS Ronald Reagan）和日本的直升机护卫舰出云号（JS Izumo）等战舰，在南海完成了为期三天的联合操演，以促进该区域的安全和稳定。
</p>
<figure class="wp-caption aligncenter" id="attachment_11360972" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/190611-N-AB123-0002.jpg">
  <img alt="" class="wp-image-11360972 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/190611-N-AB123-0002-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月11日，美军航空母舰里根号（左）与日本海上自卫队直升机护卫舰出云号共同航行于南海。(U.S. Navy photo courtesy of JMSDF)
 </figcaption><br/>
</figure><br/>
<p>
 据路透社报导，针对中共本次在南海测试导弹，美国国防部发言人伊斯特本（Dave Eastburn）指出：“当然，国防部知道中共导弹从南海南沙群岛附近的人造设施上发射。”
</p>
<p>
 伊斯特本说：“我不是代表该区域的所有主权国讲话，但我确定它们同意说，中共的行为与其希望把和平带到该区域的说词相反，而这种行为显然是意在恫吓其它（南海）声索国的威胁手段。”
</p>
<p>
 在过去，美国多次指控中共藉由在人工
 <span href="http://www.epochtimes.com/gb/tag/%E5%B2%9B%E7%A4%81.html">
  岛礁
 </span>
 兴建军事设施的方式将南海
 <span href="http://www.epochtimes.com/gb/tag/%E5%86%9B%E4%BA%8B%E5%8C%96.html">
  军事化
 </span>
 ，破坏了区域和平。
</p>
<p>
 美军参谋长联席会议主席邓福德（Joseph Dunford）于5月29日在华府智库布鲁金斯学会（Brookings Institution）的一场研讨会上表示，中共违背了不将南海岛礁军事化的承诺，呼吁世界各国对中共采取“集体行动”。这并非呼吁采取军事行动，而是强调履行国际法的必要性。
</p>
<p>
 邓福德在这场会议中指出，中共曾于2016年秋天向时任美国总统的奥巴马承诺，他们不会将南海岛礁军事化，但现在南海岛礁上却有跑道和军火库等军事设施，他们显然违背了承诺。
</p>
<p>
 他说：“在南海和其它有主权主张的地方，法治、国际法、规范和标准正处于危急关头。”#
</p>
<p>
 责任编辑：叶紫微
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360920.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360920.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360920.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/3/n11360920.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

