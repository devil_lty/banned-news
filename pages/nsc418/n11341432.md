### 急性脑炎疫情蔓延 印度逾129名儿童死亡
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-1151088360-600x400.jpg"/>
<div class="red16 caption">
 <p>
  图为印度穆扎夫法尔普尔镇果园里掉落的荔枝。（STR/AFP/Getty Images）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月23日讯】（大纪元记者洪雅文编译报导）
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%B0%E5%BA%A6.html">
  印度
 </span>
 官方周日（6月23日）表示，当地东部比哈省（Bihar）穆扎夫法尔普尔镇（Muzaffarpur）自六月初爆发
 <span href="http://www.epochtimes.com/gb/tag/%E6%80%A5%E6%80%A7%E8%84%91%E7%82%8E.html">
  急性脑炎
 </span>
 综合症（AES）以来，已经至少有129名
 <span href="http://www.epochtimes.com/gb/tag/%E5%84%BF%E7%AB%A5.html">
  儿童
 </span>
 死于此病。
</p>
<p>
 穆扎夫法尔普尔镇罹患
 <span href="http://www.epochtimes.com/gb/tag/%E6%80%A5%E6%80%A7%E8%84%91%E7%82%8E.html">
  急性脑炎
 </span>
 综合症的人数，正在从上周一（6月17日）的97人逐步攀升，这些患者通常有发烧、呕吐等症状，严重者则会出现癫痫发作、瘫痪和昏迷，影响中枢神经系统。其中以婴儿、孩童和老年人特别容易被感染。
</p>
<p>
 穆扎夫法尔普尔镇的总人口约35万，当地最高的州医疗官员沙雷什·普拉萨德·辛格（Shailesh Prasad Singh）告诉路透社，根据23日下午的最后一次更新记录，到目前为止已有129名
 <span href="http://www.epochtimes.com/gb/tag/%E5%84%BF%E7%AB%A5.html">
  儿童
 </span>
 死亡。
</p>
<p>
 辛格说，现在包括医生、护理人员和政府官员在内的专家小组已全天候工作，来遏制AES蔓延。
</p>
<p>
 当局也陆续派遣其它地区的医生到穆扎夫法尔普尔，协助Sri Krishna医学院医院的治疗。由于AES患者激增，该医院已先出院了一批病患、空出病房应对。大多数的离世病童，生前都有在医院接受治疗。
</p>
<p>
 报导指出，穆扎夫法尔普尔将近一半的儿童体重不足，发育不良，这使得他们很容易罹患AES。大部分医疗专业人士认为，爆发急性脑炎综合症的原因可能与当地的“热浪”天气有关。该地区几乎每年夏季都会受到AES的影响。
</p>
<p>
 另有一些研究指出，这些病亡的儿童或都死于“
 <span href="http://www.epochtimes.com/gb/tag/%E8%8D%94%E6%9E%9D.html">
  荔枝
 </span>
 中毒”。穆扎夫法尔普尔盛产荔枝，当地多数家庭经济状况不佳，每当荔枝成熟季节5、6月到来时，孩子们经常在果园里吃掉在地上已熟透的荔枝果腹，导致吃不下晚饭，血糖降低。
</p>
<p>
 研究人员在检查这些
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%B0%E5%BA%A6.html">
  印度
 </span>
 病童时，注意到他们发病的症状与加勒比海地区出现在孩子们身上的一种脑肿和抽搐的病症类似，而后者的病就是由西非
 <span href="http://www.epochtimes.com/gb/tag/%E8%8D%94%E6%9E%9D.html">
  荔枝
 </span>
 果所引起。
</p>
<p>
 正常状况下，血糖降低后，身体会开始代谢脂肪酸制造所需葡萄糖，但这些病童的尿液样本显示，约2/3体内含有存在于荔枝果肉里的毒素：次甘氨酸，使其体内无法合成葡萄糖，加上病童通常没吃晚饭，所以到晚上病发时，这些血糖过低的病童就会从睡梦中惊醒尖叫，很快便出现急性脑肿而抽搐、失去知觉等症状。
</p>
<p>
 不过，印度政府并没有确切说明此病的爆发原因。当地的许多家庭也表示，他们的孩子最近几周都没有吃过荔枝。
</p>
<p>
 责任编辑：李缘
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11341432.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11341432.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11341432.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/23/n11341432.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

