### 美拒中国科技公司 日本安控设备企业受益
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="中美科技战，日本脸部识别和其它安全设备供应商意外获益。" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-163909515-600x400.jpg"/>
<div class="red16 caption">
 <p>
  中美科技战，日本脸部识别和其它安全设备供应商意外获益。 (YOSHIKAZU TSUNO/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月23日讯】（大纪元记者吴英编译报导）由于担忧中国科技行业可能受中共控制在海外从事间谍活动，美国禁用此等中企的产品及技术。在这场
 <span href="http://www.epochtimes.com/gb/tag/%E7%A7%91%E6%8A%80%E6%88%98.html">
  科技战
 </span>
 中，
 <span href="http://www.epochtimes.com/gb/tag/%E6%97%A5%E6%9C%AC.html">
  日本
 </span>
 脸部识别和其它安全设备供应商意外获益。
</p>
<p>
 《华尔街日报》报导，负责安全系统的
 <span href="http://www.epochtimes.com/gb/tag/%E6%97%A5%E6%9C%AC.html">
  日本
 </span>
 NEC公司高管Toshifumi Yoshizaki周五（6月21日）在东京的一场活动中表示，这是一个“前所未有的巨大机会，我们现在必须进入（美国市场）。”
</p>
<p>
 在美国市场，NEC的
 <span href="http://www.epochtimes.com/gb/tag/%E4%BA%BA%E8%84%B8%E8%AF%86%E5%88%AB.html">
  人脸识别
 </span>
 竞争对手包括美国的微软公司和Ayonix Face Technologies公司，以及中国的商汤科技（Sensetime Group Ltd.）和依图科技（Yitu Technology）公司等。
</p>
<p>
 NEC表示，该公司
 <span href="http://www.epochtimes.com/gb/tag/%E4%BA%BA%E8%84%B8%E8%AF%86%E5%88%AB.html">
  人脸识别
 </span>
 业务的营收，可能会在几年内增长到数十亿美元，是目前的好几倍。
</p>
<p>
 去年亚特兰大国际机场的航站大厅开始使用NEC的软件，达美航空公司在该国际机场启用摄像设备，给已经提前登记护照信息的乘客，提供更简便的登机手续及安检程序。
</p>
<p>
 美国总统川普（特朗普）上个月中旬签署一项行政命令，禁止美国公司使用来自其他国家，可能构成国家安全风险的电信设备，并指示商务部和其它联邦部门在150天内完成执行禁令的计划。
</p>
<p>
 美国商务部随后宣布将
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%8E%E4%B8%BA.html">
  华为
 </span>
 （Huawei Technologies Co.）列入出口管制实体名单，要求华为必须先取得许可才能进口美国公司的零部件及技术。日本电信运营商停用华为设备后，NEC股价上涨超过两成。
</p>
<p>
 除了NEC外，跨足安全业务的日本电讯电话公司（Nippon Telegraph and Telephone Corp.，NTT）也受益于中美贸易战，开始收到美国公司的订单，包括向拉斯维加斯市提供高清晰摄像机和声音传感器在内的安全设备。
</p>
<p>
 NTT安全设备的特色是可以提供公共场合如体育馆和街道等的实时数据，以利警察及安全官员及时获得突发状况的信息。NTT希望可以在五年内将这项产品及技术扩展到美国100个城市。
</p>
<p>
 日本今年5月出口额比去年同期下降7.8%，连续第六个月下降，其中对中国大陆的出口减少了9.7%。相比之下，日本上个月对美国的出口增加。
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11340264.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11340264.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11340264.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/22/n11340264.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

