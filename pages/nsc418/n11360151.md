### 美驻港总领事卸任前 谈香港7.1事件
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/DSC_6698-600x400.jpg"/>
<div class="red16 caption">
 针对七一大游行和民众占领立法会事件，本周卸任的美国驻港总领事唐伟康（Kurt Tong）2日表示，和平地行使表达自由的权利，“是最有效和恰当的。”（宋碧龙/大纪元）
</div>
<hr/><p>
 【大纪元2019年07月02日讯】（大纪元记者梁珍香港报导）针对香港7.1大游行和民众占领
 <span href="http://www.epochtimes.com/gb/tag/%E7%AB%8B%E6%B3%95%E4%BC%9A.html">
  立法会
 </span>
 事件，本周卸任的美国驻港总领事
 <span href="http://www.epochtimes.com/gb/tag/%E5%94%90%E4%BC%9F%E5%BA%B7.html">
  唐伟康
 </span>
 （Kurt Tong）昨表示，和平地行使表达自由的权利是“最有效和恰当的。”他又称，美国目前无须修订《美国香港政策法》。
</p>
<p>
 即将卸任的
 <span href="http://www.epochtimes.com/gb/tag/%E5%94%90%E4%BC%9F%E5%BA%B7.html">
  唐伟康
 </span>
 将于本周五离港，加入私人机构工作，其职位由主管中国事务处的副助理国务卿史墨客（Hanscom Smith）接任。唐伟康昨晚出席美国独立日活动时，主动谈到前一天民众占领
 <span href="http://www.epochtimes.com/gb/tag/%E7%AB%8B%E6%B3%95%E4%BC%9A.html">
  立法会
 </span>
 事件。
</p>
<p>
 他称，美国呼吁各方不要使用暴力，对立法会内的暴力和破坏感到失望和惋惜。他强调，香港的成功是建基于尊重法治和基本自由，包括言论自由和集会自由。“美国的看法是，和平地行使表达自由的权利，是最有效和恰当的。”
</p>
<h4>
 “抗争令我感到乐观”
</h4>
<p>
 居港三年的唐伟康坦言，大家爱香港，“是因为香港的司法体系、法治、表达自由等，这些令香港之所以特别的原则⋯⋯我相信所有美国人都对香港的未来感到乐观和有信心。”
</p>
<p>
 但他说，过去几个月对这个城市来讲，是一段艰难时期，也铸成了一些错误，出现了冲突。“但正是这些抗争令我感到乐观。”
</p>
<p>
 唐伟康称，美国宪法，也出现过犯错，也有冲突发生，“但背后意愿是好的。”他以美国宪法中“我们合众国人民为建立更完善的联盟”为例。
</p>
<p>
 唐伟康称，“我相信理念是很重要的，价值观亦然。我相信香港拥有正确的理念，正确价值，甚至有正确的基本架构——一国两制，以促成社会的成功和繁荣。”成功的另一个关键元素当然是投入付出，唐伟康表示，从香港这座城市的人民身上也充分看到了投入付出。
</p>
<h4>
 唐伟康吁特首要真诚与所有人沟通
</h4>
<p>
 被问到特首林郑月娥是否做了足够的事情避免暴力发生，唐伟康表示，不方便评论。但他说，往后最好的作法是真诚地与所有人沟通。
</p>
<p>
 作为外国人，他想继续跟香港维持关系，希望香港和平、快乐和繁荣，也希望人民融洽相处和享受生活。他又称赞香港真是个很美妙的城市，有着高度自治和表达自由，“我希望人们明白这是一件好事，也要欣赏为此而抗争的人。”
</p>
<p>
 对于外界对《逃犯条例》（修订）表达忧虑，唐伟康表示，他听到一些对近期动荡情况的忧虑，但他称，美国会长远地在香港营运，虽然物价（或成本）确有点贵。“我认为他们将香港视为做生意的好地方。人们需要对香港有信心，因为信心能创造更大信心，自己越有信心，就能得到更大的信心，是一个正向的循环。”
</p>
<h4>
 无须改变《香港政策法》
</h4>
<p>
 对于《香港政策法》未来是否被取消，唐伟康表示，《香港政策法》是美国维持与香港关系的法律框架，给予香港跟中国不同待遇，是个很珍贵的法律框架，也很有弹性，创造了很多机遇。“因此我看不到有任何需要改变它，很多人都知道它是基于一国两制下的现实情况下、安排与香港关系的好方法。”
</p>
<p>
 中共将香港百万游行归咎于外国势力插手，唐伟康昨午出席亚洲协会举行的美国-香港关系讨论的闭门演讲时，遭遇亲共团体工联会在场外抗议，声称美国干预香港事务。会议原本邀请传媒采访，但昨午突然接令要取消，改为闭门会议。
</p>
<p>
 唐伟康昨也回应了所谓干预之说，同时否认美国主张暴力。他称，美国在香港的投资巨大，有接近一万名美国公民在香港居住，在此投资了数以百亿，有1400家美国公司在香港。有一万香港人受雇于美国公司。因此，他们有正当理由表达对政治、管治与经济等议题的关注，“我们当然不觉得这是干预。我们可以表达自己的观点。”
</p>
<h4>
 <span href="http://www.epochtimes.com/gb/tag/%E9%99%88%E6%96%B9%E5%AE%89%E7%94%9F.html">
  陈方安生
 </span>
 ：最大责任在政府
</h4>
<p>
 前政务司司长
 <span href="http://www.epochtimes.com/gb/tag/%E9%99%88%E6%96%B9%E5%AE%89%E7%94%9F.html">
  陈方安生
 </span>
 昨午出席唐伟康演讲后表示，香港一向习惯以和平、理性手法表达意见，她十分希望示威者“不要用暴力”，认为暴力手法会弄巧成拙和令民情反弹。
</p>
<p>
 不过陈方安生呼吁大家想想为什么年轻人觉得暴力是唯一的选择，她认为最主要原因是过去两星期，即使有两次逾百万人游行，林郑不仅没有露面，更无回应示威者五项诉求。
</p>
<p>
 她说特区政府不能除道歉之外，全部拒绝所有示威者提出的诉求，这无法令市民相信，行政长官有聆听市民心声。
</p>
<p>
 另外，对于林郑或其相关团队是否要下台，陈方安生说，2002年引进高官问责制，目的是出现管治危机时有官员需要问责下台，但至今却没有人有问责。
</p>
<p>
</p>
<p>
 责任编辑：连书华
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360151.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360151.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360151.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/2/n11360151.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

