### 夏日好去处——韩国巨济岛
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/3-2.png"/>
<div class="red16 caption">
 巨济海洋世界海豚表演。(海洋世界)
</div>
<hr/><p>
 【大纪元2019年06月25日讯】（大纪元记者赵润德韩国报导）为期4天的海洋体育“Blue might 2019走向巨济大海、走向世界”庆典，将从8月1日始在韩国
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%A8%E6%B5%8E%E5%B2%9B.html">
  巨济岛
 </span>
 举行，届时将为您带来凉爽的夏季。
</p>
<p>
 韩国
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%A8%E6%B5%8E%E5%B2%9B.html">
  巨济岛
 </span>
 本身就是资源丰富的自然公园。距韩国第二大城市
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%9C%E5%B1%B1.html">
  釜山
 </span>
 仅一小时车程。巨济岛虽然是岛屿，但因有巨加大桥与釜山市相连，不用坐船，只靠汽车就可以到达。
</p>
<p>
 巨济岛是韩国仅次于济州岛的第二大岛屿，那里是以利亚斯式海岸(因河流侵蚀的陆地沉降或海平面上升而形成的海岸)、奇岩怪石、清净的大海等迷人的景色而闻名。
</p>
<p>
</p>
<p>
 庆典由白天和晚上两个主题组成。白天可以挑战令人兴奋的体验型海洋运动，晚上则可享受美食和演唱会等多彩的娱乐活动。
</p>
<p>
 奔跑在野外自然形成的道路上的户外运动——越野跑，这次庆典上将举行“40公里向大海、向世界”的越野跑。将在海滩上举行沙滩足球大赛和海上举行的蹼泳比赛，还有巨济市长杯龙舟赛等夏季体育比赛。游客还可以体验抓范孙鱼、寻宝、龙舟体验等多种体验活动。在这里为游客准备了很多令人留下美好回忆的丰富多彩的活动。
</p>
<p>
 夜晚有“玩海夜”为主题举办的仲夏夜庆典，将提供丰富的美食、娱乐和可看的、可玩的的活动。在C-Food Night(海鲜之夜)可以品尝到不同国家的饮食，每晚还有著名歌手(DJDOC-高耀太-Norazo)的演唱会和华丽的烟花表演，将把庆典的气氛推向高潮。
</p>
<p>
 除了庆典之外，还有巨济旅行不可或缺的旅游主题，包括能看到海底的透明皮艇、享受波涛的滑板、刺激的喷气式滑雪、能治愈心灵的帆船旅行、在海上飞翔的高空飞索等运动项目，巨济可谓是海洋休闲的乐园。
</p>
<p>
 另外，这里还有为不喜欢运动的游客准备的景点。以竹子为主题的巨济毛竹主题公园、有可以观赏海豚表演的“巨济海世界”、以电视剧拍摄地著称的“风之坡”，地中海海滩般的“外岛”等，可以拍到如梦如幻的美景。
</p>
<figure class="wp-caption aligncenter" id="attachment_11343941" style="width: 310px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/6-2-e1561424485790.png">
  <img alt="" class="size-full wp-image-11343941" src="http://i.epochtimes.com/assets/uploads/2019/06/6-2-e1561424485790.png"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  赛龙舟。（巨济市厅)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11343942" style="width: 312px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/1-6.png">
  <img alt="" class="size-full wp-image-11343942" src="http://i.epochtimes.com/assets/uploads/2019/06/1-6.png"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  滑板。（巨济海洋体育）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11343940" style="width: 308px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/5-e1561424580327.png">
  <img alt="" class="wp-image-11343940" src="http://i.epochtimes.com/assets/uploads/2019/06/5-e1561424580327.png"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  烟花。（巨济市政府）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11343939" style="width: 304px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/4-2.png">
  <img alt="" class="wp-image-11343939" src="http://i.epochtimes.com/assets/uploads/2019/06/4-2.png"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  风之坡夜景。（巨济市政府）
 </figcaption><br/>
</figure><br/>
<p>
 责任编辑：叶紫微
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11343909.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11343909.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11343909.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/25/n11343909.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

