### 【直击】香港大批示威者冲进立法会全过程
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153126990-1-600x400.jpg"/>
<div class="red16 caption">
 防暴警察向立法会推进，投掷催泪弹驱散示威者。(ANTHONY WALLACE/AFP/Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月02日讯】7月2日凌晨0时，数百名手持防暴器械的香港警察采取行动驱离
 <span href="http://www.epochtimes.com/gb/tag/%E7%AB%8B%E6%B3%95%E4%BC%9A.html">
  立法会
 </span>
 内外的示威者。有示威者向警察扔掷砖块，警方则向示威者发射催泪瓦斯，现场陷入混乱。
</p>
<p>
 7月1日，香港超过50万民众走上街头参加
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%83%E4%B8%80%E5%A4%A7%E6%B8%B8%E8%A1%8C.html">
  七一大游行
 </span>
 反暴政。在不同地点还有其他反送中示威活动也在持续进行。
</p>
<p>
 下午起，
 <span href="http://www.epochtimes.com/gb/tag/%E7%AB%8B%E6%B3%95%E4%BC%9A.html">
  立法会
 </span>
 外的示威者冲击立法会大门的强化玻璃，下午5时左右，再有一批示威者涌入俗称“煲底”的立法会示威区，搬走入口前面的大型铁防撞栏，又用铁支、铁板等破坏示威区公众入口的玻璃门。下午近6时以铁条等硬物击破立法会大楼玻璃门。
</p>
<figure class="wp-caption aligncenter" id="attachment_11356736" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153080177.jpg">
  <img alt="抗议者聚集立法会。（VIVEK PRAKASH/AFP/Getty Images)" class="wp-image-11356736 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153080177-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年7月1日，香港
  <span href="http://www.epochtimes.com/gb/tag/%E6%8A%97%E8%AE%AE.html">
   抗议
  </span>
  者聚集在立法会。(VIVEK PRAKASH/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption alignnone" id="attachment_11357005">
 <p>
  <figure class="wp-caption aligncenter" id="attachment_11357005" style="width: 600px">
   <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010407211366.jpg">
    <img alt="" class="wp-image-11357005 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010407211366-600x450.jpg"/>
   </span>
   <br/><figcaption class="wp-caption-text">
    2019年7月1日，香港立法会大楼玻璃被撞破，警方依然手持盾牌和警棍戒备。（李逸／大纪元）
   </figcaption><br/>
  </figure><br/>
  <br/><figcaption class="wp-caption-text">
  </figcaption><br/>
 </p>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11357006">
 <p>
  <figure class="wp-caption aligncenter" id="attachment_11357006" style="width: 600px">
   <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010407411366.jpg">
    <img alt="" class="wp-image-11357006 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010407411366-600x450.jpg"/>
   </span>
   <br/><figcaption class="wp-caption-text">
    2019年7月1日，香港立法会大楼内，警方依然手持盾牌和警棍戒备，且举旗警告停止冲击否则动武。（宋碧龙／大纪元）
   </figcaption><br/>
  </figure><br/>
  <br/><figcaption class="wp-caption-text">
  </figcaption><br/>
 </p>
</figure><br/>
<figure class="wp-caption alignnone" id="attachment_11356740">
 <p>
  <figure class="wp-caption aligncenter" id="attachment_11356740" style="width: 600px">
   <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153080284.jpg">
    <img alt="抗议者冲撞立法会大门。（ANTHONY WALLACE/AFP/Getty Images)" class="wp-image-11356740 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153080284-600x400.jpg"/>
   </span>
   <br/><figcaption class="wp-caption-text">
    2019年7月1日，香港
    <span href="http://www.epochtimes.com/gb/tag/%E6%8A%97%E8%AE%AE.html">
     抗议
    </span>
    者冲撞立法会大门。(ANTHONY WALLACE/AFP/Getty Images)
   </figcaption><br/>
  </figure><br/>
  <br/><figcaption class="wp-caption-text">
  </figcaption><br/>
 </p>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356732">
 <p>
  <figure class="wp-caption aligncenter" id="attachment_11356732" style="width: 600px">
   <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010229511366.jpg">
    <img alt="" class="wp-image-11356732 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010229511366-600x450.jpg"/>
   </span>
   <br/><figcaption class="wp-caption-text">
    2019年7月1日，香港防暴警察在立法会内戒备。（孙青天／大纪元）
   </figcaption><br/>
  </figure><br/>
  <br/><figcaption class="wp-caption-text">
  </figcaption><br/>
  <br/><figcaption class="wp-caption-text">
   <figure class="wp-caption alignnone" id="attachment_11356745">
    <p>
     <figure class="wp-caption aligncenter" id="attachment_11356745" style="width: 600px">
      <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153080072-1.jpg">
       <img alt="抗议者冲击玻璃门。（VIVEK PRAKASH/AFP/Getty Images)" class="wp-image-11356745 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153080072-1-600x400.jpg"/>
      </span>
      <br/><figcaption class="wp-caption-text">
       2019年7月1日，香港激进抗议者用废纸回收铁笼车冲撞立法会玻璃门。(VIVEK PRAKASH/AFP/Getty Images)
      </figcaption><br/>
     </figure><br/>
     <br/><figcaption class="wp-caption-text">
     </figcaption><br/>
    </p>
   </figure><br/>
   <figure class="wp-caption aligncenter" id="attachment_11356743">
   </figure><br/>
  </figcaption><br/>
 </p>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356741">
 <br/><figcaption class="wp-caption-text">
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356739">
 <p>
  <figure class="wp-caption aligncenter" id="attachment_11356739" style="width: 600px">
   <span href="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153080191.jpg">
    <img alt="抗议者仍要冲击玻璃门。（ANTHONY WALLACE/AFP/Getty Images)" class="wp-image-11356739 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153080191-600x400.jpg"/>
   </span>
   <br/><figcaption class="wp-caption-text">
    2019年7月1日，香港，激进抗议者持绩冲撞立法会玻璃门。(ANTHONY WALLACE/AFP/Getty Images)
   </figcaption><br/>
  </figure><br/>
  <br/><figcaption class="wp-caption-text">
  </figcaption><br/>
 </p>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356958">
 <p>
  <figure class="wp-caption aligncenter" id="attachment_11356958" style="width: 600px">
   <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010407481366.jpg">
    <img alt="" class="wp-image-11356958 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010407481366-600x450.jpg"/>
   </span>
   <br/><figcaption class="wp-caption-text">
    立法会大楼有玻璃被撞破。（宋碧龙／大纪元）
   </figcaption><br/>
  </figure><br/>
  <br/><figcaption class="wp-caption-text">
  </figcaption><br/>
 </p>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11357237">
 <figure class="wp-caption aligncenter" id="attachment_11357237" style="width: 600px">
  <span href="http://i.epochtimes.com/assets/uploads/2019/07/1907010541171366.jpg">
   <img alt="" class="wp-image-11357237 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/1907010541171366-600x450.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   大纪元记者拍到有警方配备用以射橡胶子弹或者布袋弹的“低致命性”长枪。（李逸／大纪元）
  </figcaption><br/>
 </figure><br/>
 <p>
  晚上近九点，示威者打开示威区铁闸，进入立法会大堂。示威者进入立法会会议厅，涂污原本挂在会议厅的香港区徽，并在墙上写上“太阳花”、“释放义士”、“取消功能组别”、“反送中”、“真普选”等字句。
 </p>
 <p>
  占领香港立法会议场的示威群众于7月1日深夜发表声明，要求港府完成“解散立法会，立即实行双真普选”、彻底收回《逃犯条例》修订等5项诉求。
 </p>
 <p>
  声明中提到，“万不得已，我们并不想走上以身对抗暴政的路，以占领香港特区政府立法会作为我们谈判的筹码”。但“满口谎言、满口歪理的政府”，却无意回应香港人不断走上街的诉求，示威者“只好以公义、良知、以及对香港、对香港人无穷无尽的爱，去抗衡横蛮的政府”。
  <br/><figcaption class="wp-caption-text">
  </figcaption><br/>
 </p>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11356988">
 <p>
  <figure class="wp-caption aligncenter" id="attachment_11356988" style="width: 600px">
   <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190701-HUAMING-HONGKONG-01.jpg">
    <img alt="" class="wp-image-11356988 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/20190701-HUAMING-HONGKONG-01-600x450.jpg"/>
   </span>
   <br/><figcaption class="wp-caption-text">
    立法会大楼外的示威者继续与大楼内的警方对峙。（宋碧龙／大纪元）
   </figcaption><br/>
  </figure><br/>
 </p>
</figure><br/>
<div class="red16 caption">
 <figure class="wp-caption aligncenter" id="attachment_11357858">
  <figure class="wp-caption aligncenter" id="attachment_11357868" style="width: 600px">
   <span href="http://i.epochtimes.com/assets/uploads/2019/07/20190701-LC-Poon-2.jpg">
    <img alt="" class="wp-image-11357868 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/20190701-LC-Poon-2-600x400.jpg"/>
   </span>
   <br/><figcaption class="wp-caption-text">
    香港七一移交主权22周年之际，聚集在立法会外的香港民众在一整天与警察激烈冲突之后，冲进了立法会。（李逸／大纪元）
   </figcaption><br/>
  </figure><br/>
  <figure class="wp-caption aligncenter" id="attachment_11358117" style="width: 600px">
   <img alt="" class="wp-image-11358117 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153125910-600x400.jpg"/>
   <br/><figcaption class="wp-caption-text">
    立法会议场被挂上“没有暴徒，只有暴政”等布条及标语。(VIVEK PRAKASH/AFP/Getty Images)
   </figcaption><br/>
  </figure><br/>
  <figure class="wp-caption aligncenter" id="attachment_11358118" style="width: 600px">
   <img alt="" class="wp-image-11358118 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153125446-600x400.jpg"/>
   <br/><figcaption class="wp-caption-text">
    抗议者粘贴“护我香港自由人权”标语。 (VIVEK PRAKASH/AFP/Getty Images)
   </figcaption><br/>
  </figure><br/>
  <p>
   7月2日凌晨0时，香港警方开始采取行动。大批防暴警察由警察总部出发，从多个方向朝着立法会推进，包括龙和道及龙汇道，期间施放多枚催泪弹，驱离在场的示威者。#
  </p>
  <figure class="wp-caption aligncenter" id="attachment_11358181" style="width: 600px">
   <img alt="" class="wp-image-11358181 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153127462-1-600x400.jpg"/>
   <br/><figcaption class="wp-caption-text">
    7月2日凌晨0时，数百名手持防暴器械的香港警察采取行动武力清场。(Photo credit should read ANTHONY WALLACE/AFP/Getty Images)
   </figcaption><br/>
  </figure><br/>
  <figure class="wp-caption aligncenter" id="attachment_11358182" style="width: 600px">
   <img alt="" class="wp-image-11358182 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153128580-600x400.jpg"/>
   <br/><figcaption class="wp-caption-text">
    7月2日凌晨0时，数百名手持防暴器械的香港警察采取行动武力清场。 (PHILIP FONG/AFP/Getty Images)
   </figcaption><br/>
  </figure><br/>
  <figure class="wp-caption aligncenter" id="attachment_11358167" style="width: 600px">
   <img alt="" class="wp-image-11358167 size-large" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153127239-1-600x400.jpg"/>
   <br/><figcaption class="wp-caption-text">
    香港警察7月2日凌晨0时采取行动，发射催泪瓦斯驱离在场的示威者。 (ANTHONY WALLACE/AFP/Getty Images)
   </figcaption><br/>
  </figure><br/>
  <p>
   责任编辑：孙芸
  </p>
 </figure><br/>
</div>
<div class="red16 caption">
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11358101.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11358101.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11358101.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/1/n11358101.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

