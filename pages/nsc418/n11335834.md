### 悉尼歌剧院景点 大陆游客读禁书了解真相
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/2016-1-1-minghui-sydney-opera-house-600x400.jpg"/>
<div class="red16 caption">
 中国大陆游客在澳洲悉尼歌剧院景点看法轮功真相展板。（明慧网）
</div>
<hr/><p>
 【大纪元2019年06月20日讯】五月是旅游的高峰，在澳大利亚，部分悉尼法轮功学员在歌剧院景点给中国人讲法轮功真相，帮助人们认清中共反天地神佛、反人类的邪恶本质，做“
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%89%E9%80%80.html">
  三退
 </span>
 ”（退出中共党、团、队）保平安。
</p>
<p>
 澳洲著名的
 <span href="http://www.epochtimes.com/gb/tag/%E6%82%89%E5%B0%BC%E6%AD%8C%E5%89%A7%E9%99%A2.html">
  悉尼歌剧院
 </span>
 （Opera House）是全世界的旅游热点之一，每天有很多中国大陆游客到此观光。游客下车后，首先映入眼帘的是“法轮大法好 真善忍好”和“‘
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%89%E9%80%80.html">
  三退
 </span>
 ’保平安”等醒目的大横幅，还有面带微笑的法轮功学员，他们向人们发送法轮功真相资料、以及被中共严厉禁刊的《九评共产党》、《共产主义的终极目的》等海外著作。
</p>
<figure class="wp-caption aligncenter" id="attachment_11335843" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/2016-5-4-minghui-falun-gong-sydney-01-e1561038549207.jpg">
  <img alt="" class="wp-image-11335843 size-large" src="http://i.epochtimes.com/assets/uploads/2019/06/2016-5-4-minghui-falun-gong-sydney-01-600x382.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  中国大陆游客在澳洲
  <span href="http://www.epochtimes.com/gb/tag/%E6%82%89%E5%B0%BC%E6%AD%8C%E5%89%A7%E9%99%A2.html">
   悉尼歌剧院
  </span>
  景点会看到法轮功真相展板和横幅。（明慧网）
 </figcaption><br/>
</figure><br/>
<figure class="wp-caption aligncenter" id="attachment_11335849" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/2019-6-19-sydney-truth-point_01.jpg">
  <img alt="" class="size-large wp-image-11335849" src="http://i.epochtimes.com/assets/uploads/2019/06/2019-6-19-sydney-truth-point_01-600x226.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  中国大陆游客在澳洲悉尼歌剧院景点会看到法轮功真相展板和横幅。（明慧网）
 </figcaption><br/>
</figure><br/>
<h4>
 三人听
 <span href="http://www.epochtimes.com/gb/tag/%E6%98%8E%E7%9C%9F%E7%9B%B8.html">
  明真相
 </span>
 “三退”
</h4>
<p>
 明慧网报导，一天下午，一位法轮功学员手拿着真相展板，正往歌剧院景点的游客上下巴士处赶，一大陆男士遇到她后，说：“我知道你们法轮功的人都是好人，法轮功的人不会干坏事，也不会说谎，也不会欺骗别人。”法轮功学员简要地给他讲了“三退”的意义，那位男士很快同意用化名“福星”退出了共青团和少先队。
</p>
<p>
 2004年，大纪元报社发表系列社论《九评共产党》从历史角度，全面深刻的揭露了中共的邪恶本质，并在海内外引发三退大潮。不管什么原因参加过中共少先队、共青团、党的中国人，纷纷宣布脱离中国共产党的任何组织。人们利用网络、电话、张贴或者通过退党义工法轮功学员，把一份份退党声明以及自己的名字和心愿传送到全球退党网站，至今已有超过3亿多人退出中共组织。
</p>
<p>
 学员对这位男士说：“记住‘法轮大法好、真善忍好 ’，你从此有神佛保护了，你脱离作恶多端的共产邪党的组织了，你就不是它的一分子了，你是一个得到老天承认的好人了，你从此会平安幸福。”
</p>
<p>
 男士听了后高兴的说，他家人还在另一边等他。他把学员带到其家人跟前，途中一直听学员讲法轮功受迫害真相。
</p>
<p>
 他的妹妹和太太站在那里，于是学员上前跟她们打招呼，还对她们说：“这位先生已经退出了共青团和少先队了，他已经了解法轮功真相了，你们也做‘三退’吧？”
</p>
<p>
 法轮功学员紧接着简单地给她们讲了法轮大法的美好和为什么要做“三退”的原因。
</p>
<p>
 法轮功教人们修炼“真、善、忍”，洪传世界一百多个国家和地区，受到各族裔民众的欢迎，获得国际褒奖3,600多项。中共迫害做好人、修炼佛法的人，天理不容，要受到惩罚。中国人都相信善恶有报是天理，退出中共的党、团、队，才会有平安和未来。
</p>
<p>
 那位男士的妹妹爽快地同意用化名“美满”加上她的姓退出了共青团和少先队。她嫂子在一旁也听明白了真相，同意用化名“美好”加上自己的姓退出了共青团和少先队。
</p>
<p>
 法轮功学员最后祝他们全家平安幸福，他们都十分高兴。
</p>
<h4>
 大陆游客的转变
</h4>
<p>
 5月中旬，游客特别多，大多是年轻人，其中大部分来自中国大陆。据说，他们是某大公司的员工，他们乘坐的大巴士的编号已超过一百。
</p>
<p>
 法轮功学员举着写有动态网网址的小展板站在一辆大巴士旁，告诉正在上车的一大群年轻人，上动态网看真实世界、下载翻墙软件、为自己声明“三退”等等。这时一位男士用手里举着的大巴士编号牌挡住法轮功学员的小展板，不让大家看。
</p>
<p>
 法轮功学员问他，这位先生，你为什么要做这种事？你是不是拿了共产党的钱在民主国家为共产党摇旗呐喊的那种人？如果是的话，那就太危险了。澳洲已经出台了反间谍和反外国渗透法，就是要寻找这样的人，把他们驱逐出去。“我要报个警，你就麻烦了，下次你就来不了澳洲了；如果你有永居签证，可能它就会被澳洲政府吊销。”
</p>
<p>
 那个男士听后就不再遮挡法轮功学员的展板了。
</p>
<p>
 另一辆旅游车的大陆游客从真相展板前经过，走在队伍稍后一点的一位男游客从法轮功学员手中接过一本《共产主义的终极目的》的书，旁边一位女游客见状立即阻止，边说边伸手抢夺那本书，男游客阻挡着不想让她抢走。
</p>
<p>
 法轮功学员立即上前对那位女士说：“你不可以阻止别人看资料，这里是民主社会，人人都有权利看我们的（法轮功真相）资料。你这样做，在人的层面上是违法的；在神佛的眼里，你是在干坏事。我们是在帮人（了解真相），就好比一个不会游泳的人掉河里了，你在阻止着不让他得救，你干了一件多大的坏事哪！大家明白吗？”
</p>
<p>
 和他们走在一起的好几位男游客异口同声地说：“明白！”那位女游客不再阻止了，看上去她似乎也明白了，没有任何抵触，反而变得开心了。
</p>
<p>
 此时，大伙都开心地朝海边走去。那位男游客大大方方地边走边翻阅《共产主义的终极目的》的书。
</p>
<p>
 《共产主义的终极目的——中国篇》是继《九评共产党》之后的又一宏篇巨制，自2017年11月在大纪元新闻网首发，赢得广大读者的赞誉。
</p>
<p>
 该书序言中提到：共产主义不是一种学说、一种社会制度，它是一个邪灵，其目的是通过毁灭文化、败坏道德来毁灭全人类。今天的人类，在表面上享受着科技发达、物质繁荣，而实际上却受困于道德下滑所导致的种种难题败象。若想寻求转机，重获生机，必须反躬自省，重视精神层面的净化和提升，而在此过程中，彻底清除共产主义思想的毒害，至关重要。#
</p>
<p>
 文字整理：李洁思，责任编辑：高静
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11335834.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11335834.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11335834.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/20/n11335834.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

