### 美拟放宽高科技绿卡配额国籍限制 中印受益
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2017/08/e55806faa7017586900240db666a64d3-600x400.jpg"/>
<div class="red16 caption">
 <p>
  取消绿卡限制法案若成美国新法律，中、印籍高科技移民将成为主要受惠者。（John Moore/Getty Images）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月11日讯】（大纪元记者洪雅文综合报导）2017年2月，印籍妇人苏娜亚娜．杜玛拉（Sunayana Dumala）的丈夫在堪萨斯州奥拉西的一家餐馆遭人枪杀。为了生活，她多次访问华府，推动取消绿卡的国籍批准额上限。时隔2年，联邦众议院周三（7月10日）晚间通过相关法案，取消绿卡限制，中、印籍高科技移民将成为主要受惠者。
</p>
<p>
 众议院10日依365票比65票通过提案，但该法案还需要参议院决议通过，经川普总统签字才能成为法律。
</p>
<p>
 在现行移民制度下，每个国家的绿卡
 <span href="http://www.epochtimes.com/gb/tag/%E9%85%8D%E9%A2%9D.html">
  配额
 </span>
 为7％。新法案（HR 1044）将把每个国家的配额限制提高到15％，取消7％的职业移民签证上限，并撤销减少
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD.html">
  中国
 </span>
 个人签证数量的限制。
</p>
<p>
 目前，持有在美临时工作签证者多数为
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD.html">
  中国
 </span>
 和印度人，杜玛拉就属于此类人。与他们一起来美的眷属也依赖申请人的签证及其工作状态。
</p>
<p>
 由于中国和印度申请签证转公民的人数众多，通常须等待数十年才能获得绿卡。
</p>
<p>
 法案还为非中、印籍公民保留一定比例的EB-2签证（具有高级学位或特殊能力的工人）、EB-3和EB-5（投资者签证），为2020-2财政年度的职业签证建立过渡规则。
</p>
<p>
 美国国会研究服务处（Congressional Research Service）表示，未预留的签证最多会有85％分配给任何一个国家的移民。
</p>
<p>
 该法案最初由联邦众议员、共和党人凯文．耶德（Kevin Yoder）在上届国会期间提出；今年2月，民主党众议员莎莉丝．戴维斯（Sharice Davids）再提议案。
</p>
<p>
 戴维斯在声明中说，该法案将使企业留住高技能工人提高竞争力，为当地经济做出贡献，同时减少移民等待绿卡批发的时间。她希望此法案能促进移民法走向完善。
</p>
<p>
 该法案目前受到来自全国各地的中、印籍专业人士的欢迎，特别是在加州硅谷、华盛顿州西雅图地区、华盛顿特区以及纽约、新泽西和康涅狄格州等地。
</p>
<p>
 微软总裁布拉德．史密斯（Brad Smith）表示，美国众议院通过的新法案将促进商业和经济，有利于高技术移民制度。
</p>
<p>
 代表顶级硅谷公司（包括脸书、谷歌、微软和DropBox）的倡导组织FWD.uS总裁托德．舒尔特（Todd Schulte）说，新法案消除就业绿卡上限和提高家庭绿卡的上限，将加强美国招募和留住顶尖全球人才，一改中国和印度等国人在转美国公民前，须等待50年或更长时间的情况。#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11378695.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11378695.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11378695.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/11/n11378695.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

