### 盘点计划或已将生产线转移出中国的美企
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-73193069-600x400.jpg"/>
<div class="red16 caption">
 <p>
  图为中国广东省佛山市一家工厂的部分生产线。（Cancan Chu/Getty Images）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月21日讯】（大纪元记者洪雅文编译报导）川习会下周在日本登场，美中贸易谈判或迈入最后阶段，但姑且不论谈判结果如何，美国部分企业早已在贸易战过程中，陆续将供应链迁出中国，躲避关税风险，降低对“中国制造”的依赖。
</p>
<p>
 这些企业在最新一轮谈判的结果出炉后，相继减少在中国生产的份额，转而投资美国、
 <span href="http://www.epochtimes.com/gb/tag/%E5%A2%A8%E8%A5%BF%E5%93%A5.html">
  墨西哥
 </span>
 或
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%9C%E5%8D%97%E4%BA%9A.html">
  东南亚
 </span>
 国家，如越南、柬埔寨等。
</p>
<p>
 与此同时，白宫拟推进另一项新关税，向价值约3,000亿美元的中国产品。目前，美国政府已经对2,500亿美元的中国货物征收了税款。
</p>
<p>
 福克斯商业新闻周五（6月21日）汇整
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%9B%BD%E4%BC%81%E4%B8%9A.html">
  美国企业
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E5%87%BA%E8%B5%B0%E4%B8%AD%E5%9B%BD.html">
  出走中国
 </span>
 的完整名单。以下是已经宣布或正在考虑迁厂的企业：
</p>
<h4>
 1. 苹果公司（Apple Inc.）
</h4>
<p>
 据报导，苹果正在考虑将15%～30%的产能从中国到
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%9C%E5%8D%97%E4%BA%9A.html">
  东南亚
 </span>
 ，并重组其制造业务，即便两国有了贸易解决方案，该公司也不会走回头路。
</p>
<h4>
 2. GoPro
</h4>
<p>
 运动相机公司GoPro将在2019年中旬，将大部分的产品生产链从中国移往
 <span href="http://www.epochtimes.com/gb/tag/%E5%A2%A8%E8%A5%BF%E5%93%A5.html">
  墨西哥
 </span>
 。但该公司仍会保留部分产能在中国。
</p>
<p>
 GoPro首席财务官布莱恩·麦基（Brian McGee）在去年12月表示，“在今天，地缘政治的商业环境需要适度地灵活调整”，“我们正在积极解决关税问题。”
</p>
<h4>
 3. 孩之宝（Hasbro Inc）
</h4>
<p>
 玩具制造商孩之宝正将其大部分产品生产从中国转移到墨西哥、越南和印度。
</p>
<h4>
 4. 史蒂夫·马登（Steve Madden）
</h4>
<p>
 史蒂夫·马登是许多好莱坞女星爱不释手的鞋、手袋品牌，这家公司正在把大部分的生产转移到柬埔寨。
</p>
<p>
 “我们喜欢在美国制鞋，”首席执行官埃德·罗森菲尔德（Ed Rosenfeld）告诉全国公共广播电台（NPR），“（但是）我们很难想像在美国制造产品的场景和价格。”
</p>
<h4>
 5. 史丹利百得（Stanley Black &amp; Decker, Inc.）
</h4>
<p>
 这家五金公司正在将它的主要品牌Craftsman生产线移到美国，并在德州沃思堡开设新工厂。新工厂预估价值约9,000万美元，史丹利百得将在那里雇佣500名员工。
</p>
<p>
 据报，他们将采用机器和其它先进技术来维持生产成本，预计将和中国成本相符。
</p>
<h4>
 6. 布鲁克斯跑鞋（Brooks Running）
</h4>
<p>
 巴菲特所有的运动鞋公司布鲁克斯跑鞋，将把大部分制造业务从中国迁往越南。
</p>
<p>
 该公司的首席执行官吉姆·韦伯（Jim Weber）告诉路透社，“我们不得不就作出长期决定。尽管它会带来很大的影响，但这就是现实。因此，到今年年底，我们的主要（产能）将在越南。”
</p>
<h4>
 7. 惠而浦公司（Whirlpool Corporation）
</h4>
<p>
 惠而浦公司正在把部分KitchenAid设备的生产线，从中国转移到美国。
</p>
<h4>
 8. 英特尔（Intel Corp.）
</h4>
<p>
 英特尔首席执行官鲍勃·斯旺（Bob Swan）在今年6月份告诉彭博社，该公司正在审查其供应链以及生产是否能转移到中国之外。
</p>
<p>
 斯旺表示，英特尔正在研究如何调整他们在全球供应链中的工作，以便有更少的产品直接从中国进入美国。#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11338377.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11338377.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11338377.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/21/n11338377.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

