### 美军首次拆除F-35战机机翼 以运输机空运
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/190621-F-F3962-0634-600x400.jpg"/>
<div class="red16 caption">
 <p>
  2019年5月8日，美军首次将一架F-35战机的机翼拆除，准备以运输机空运。(Monica Lubis/U.S. Air Force)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月30日讯】（大纪元记者陈俊村编译报导）美国空军上个月首次将一架F-35
 <span href="http://www.epochtimes.com/gb/tag/%E6%88%98%E6%9C%BA.html">
  战机
 </span>
 的
 <span href="http://www.epochtimes.com/gb/tag/%E6%9C%BA%E7%BF%BC.html">
  机翼
 </span>
 拆除，再以
 <span href="http://www.epochtimes.com/gb/tag/%E8%BF%90%E8%BE%93%E6%9C%BA.html">
  运输机
 </span>
 空运到目的地，这有别于先前以飞行员驾驶该型战机前往目的地的方式。
</p>
<p>
 5月8日，这架
 <span href="http://www.epochtimes.com/gb/tag/%E6%9C%BA%E7%BF%BC.html">
  机翼
 </span>
 被拆除的F-35
 <span href="http://www.epochtimes.com/gb/tag/%E6%88%98%E6%9C%BA.html">
  战机
 </span>
 被推进佛罗里达州埃格林空军基地（Eglin Air Force Base）的一架C-17
 <span href="http://www.epochtimes.com/gb/tag/%E8%BF%90%E8%BE%93%E6%9C%BA.html">
  运输机
 </span>
 的货舱内，准备运往犹他州希尔空军基地（Hill Air Force Base）。
</p>
<p>
 美国空军空战司令部（Air Combat Command）在6月24日公布的一份声明中表示，这是
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%86%9B.html">
  美军
 </span>
 首次将F-35战机的机翼拆除，再以运输机空运。而这架F-35战机将成为空军第一架F-35战机的战斗损害训练机。
</p>
<figure class="wp-caption aligncenter" id="attachment_11354936" style="width: 450px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190621-F-F3962-0635.jpg">
  <img alt="" class="wp-image-11354936 size-medium" src="http://i.epochtimes.com/assets/uploads/2019/06/190621-F-F3962-0635-450x562.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E5%86%9B.html">
   美军
  </span>
  士兵将这架战机推入C-17运输机的货舱内。(Monica Lubis/U.S. Air Force)
 </figcaption><br/>
</figure><br/>
<p>
 F-35是美国洛克希德‧马丁公司（Lockheed Martin）生产的第五代多用途战机，具有隐形功能，可在全天候操作，以执行对地面作战或取得制空权的任务，也可以用来侦搜情报。
</p>
<p>
 相较于前一世代，第五代的技术让战机飞行员有比较好的存活能力、状态意识（situational awareness，指环境因素与事件对应于时间或空间的知觉与理解）、效能和备战状态，而其支援成本也较低。
</p>
<p>
 美军现在使用三种主要版本的F-35战机。其中，空军使用F-35A来搭配F-22战机，它将取代具有空对地作战能力的F-16战机和A-10攻击机；海军陆战队使用F-35B，它将取代F/A-18和AV-8B攻击机；而海军将以F-35C取代F/A-18E/F攻击机。
</p>
<p>
 就差异而言，F-35A使用传统跑道起降，部署在空军基地；F-35B可以短距离起飞和垂直起降，可搭载在形同轻型航空母舰的两栖攻击舰上；而F-35C则作为航空母舰的舰载机。
</p>
<p>
 F-35战机已经出售给英国、日本和以色列等多个国家。而在6月25日，美国的F-35A、英国的F-35B和以色列的F-35I这三种版本的战机，在地中海东部举行联合演习，这是首度有不同国家的F-35系列战机举行国际性的演习。
</p>
<figure class="wp-caption aligncenter" id="attachment_11354937" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190625-F-ZD147-0196-1.jpg">
  <img alt="" class="wp-image-11354937 size-large" src="http://i.epochtimes.com/assets/uploads/2019/06/190625-F-ZD147-0196-1-600x384.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月25日，英国、美国和以色列空军的F-35战机在地中海举行联合演习。在队形中居中领航的是美军的F-35战机。(Keifer Bowes/U.S. Air Force)
 </figcaption><br/>
</figure><br/>
<p>
 到目前为止，美国、英国和以色列的F-35战机均有实战经验。其中，以色列最早让F-35战机投入作战。
</p>
<p>
 以色列空军在2018年5月宣布其F-35战机已参加实战，并自称是世界上第一个让F-35战机投入作战的单位。
</p>
<blockquote class="twitter-tweet" data-conversation="none" data-lang="zh-tw">
 <p dir="ltr" lang="en">
  “The Adir planes are already operational and flying in operational missions. We are the first in the world to use the F-35 in operational activity”
 </p>
 <p>
  — Israel Defense Forces (@IDF)
  <span href="https://twitter.com/IDF/status/998816986917408768?ref_src=twsrc%5Etfw">
   2018年5月22日
  </span>
 </p>
</blockquote>
<p>
 <p>
  责任编辑：林琮文
 </p>
</p>
<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11354931.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11354931.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11354931.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/30/n11354931.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

