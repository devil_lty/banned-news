### 英海军吓退伊朗船 国防大臣赞捍卫国际法
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-466681513-600x400.jpg"/>
<div class="red16 caption">
 <p>
  英国海军巡防舰“蒙特罗斯”号（HMS Montrose）。(/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月11日讯】
 <span class="s1">
  （大纪元记者徐简综合报导）周四（
 </span>
 <span class="s3">
  7
 </span>
 <span class="s1">
  月
 </span>
 <span class="s3">
  11
 </span>
 <span class="s1">
  日）
  <span href="http://www.epochtimes.com/gb/tag/%E8%8B%B1%E5%9B%BD.html">
   英国
  </span>
  政府证实，三艘
  <span href="http://www.epochtimes.com/gb/tag/%E4%BC%8A%E6%9C%97.html">
   伊朗
  </span>
  船只周三（
 </span>
 <span class="s3">
  10
 </span>
 <span class="s1">
  日）企图拦截一艘通过
  <span href="http://www.epochtimes.com/gb/tag/%E9%9C%8D%E5%B0%94%E6%9C%A8%E5%85%B9%E6%B5%B7%E5%B3%A1.html">
   霍尔木兹海峡
  </span>
  （
 </span>
 <span class="s3">
  Strait of Hormuz
 </span>
 <span class="s1">
  ）的
  <span href="http://www.epochtimes.com/gb/tag/%E8%8B%B1%E5%9B%BD.html">
   英国
  </span>
  <span href="http://www.epochtimes.com/gb/tag/%E6%B2%B9%E8%BD%AE.html">
   油轮
  </span>
  ，英国
  <span href="http://www.epochtimes.com/gb/tag/%E6%B5%B7%E5%86%9B.html">
   海军
  </span>
  随即出动巡防舰斥退
  <span href="http://www.epochtimes.com/gb/tag/%E4%BC%8A%E6%9C%97.html">
   伊朗
  </span>
  船。英国国防大臣对
  <span href="http://www.epochtimes.com/gb/tag/%E6%B5%B7%E5%86%9B.html">
   海军
  </span>
  表示赞扬。
 </span>
</p>
<p>
 <span class="s1">
  路透社报导，周三英国
 </span>
 <span class="s4">
  “
 </span>
 <span class="s1">
  遗产号”
 </span>
 <span class="s4">
  <span class="s1">
   （
  </span>
  <span class="s3">
   British Heritage
  </span>
  <span class="s1">
   ）
  </span>
 </span>
 <span class="s1">
  <span href="http://www.epochtimes.com/gb/tag/%E6%B2%B9%E8%BD%AE.html">
   油轮
  </span>
  当时正准备从波斯湾进入
  <span href="http://www.epochtimes.com/gb/tag/%E9%9C%8D%E5%B0%94%E6%9C%A8%E5%85%B9%E6%B5%B7%E5%B3%A1.html">
   霍尔木兹海峡
  </span>
  航道时，三艘伊朗船只要求英国油轮转向进入伊朗领海，但遭英国军舰驱逐，英军一度将枪口对准伊朗船只，并给予口头警告。而美方飞机记录下来整个事件。
 </span>
</p>
<p class="p7">
 <span class="s1">
  但伊朗周四对此予以否认，自称
  <span class="s4">
   “
  </span>
  <span class="s1">
   在过去
  </span>
  <span class="s4">
   24
  </span>
  <span class="s1">
   小时，没有遇到包括英国船只在内的任何外国船只
  </span>
  <span class="s4">
   ”。
  </span>
 </span>
</p>
<h4 class="p5">
 <span class="s1">
  英国国防大臣赞英军捍卫国际法
 </span>
</h4>
<p class="p3">
 <span class="s1">
  周四，英国国防部长
  <span class="s1">
   彭妮·莫当特（
  </span>
  <span class="s3">
   Penny Mordaunt）
  </span>
 </span>
 <span class="s1">
  表示，英国王室海军帮助油轮穿过霍尔木兹海峡，捍卫了国际法。
 </span>
</p>
<p class="p3">
 <span class="s3">
  “
 </span>
 <span class="s1">
  我要感谢英国海军的专业精神，他们捍卫国际法，并支持全球贸易重要航道的航行自由。
 </span>
 <span class="s3">
  ”莫当特
 </span>
 <span class="s1">
  在
 </span>
 <span class="s3">
  推特
 </span>
 <span class="s1">
  上发布的一份声明表示。
 </span>
</p>
<p class="p3">
 <span class="s1">
  当天早些时候，英国政府确认，当英国商船“英国遗产”号油轮（
 </span>
 <span class="s3">
  British Heritage
 </span>
 <span class="s1">
  ）通过霍尔木兹海峡时，三艘伊朗船不顾国际法，企图进行拦截。
 </span>
</p>
<p class="p9">
 <span class="s1">
  政府声明说，在这种情况下，英国海军巡防舰“蒙特罗斯”号（
 </span>
 <span class="s3">
  HMS Montrose
 </span>
 <span class="s1">
  ）不得不介入，驶到油轮与伊朗船舰之间，并作出口头警告。伊朗船随后驶离。
 </span>
</p>
<p class="p9">
 <span class="s1">
  英国政府表示对这次行动感到忧虑，将继续敦促伊朗当局缓和地区紧张局势。
 </span>
</p>
<h4 class="p3">
 <span class="s1">
  事件回放
 </span>
 <span class="s1">
  英军一度枪炮对准伊朗船
 </span>
</h4>
<p class="p3">
 <span class="s1">
  周三，两名美国官员分别证实，伊朗船只曾企图劫持英国油轮。
 </span>
</p>
<p class="p3">
 <span class="s1">
  其中一名官员说，英国油轮遭到伊朗骚扰时，英国海军的“蒙特罗斯号”
 </span>
 <span class="s1">
  以无线广播警告伊朗船只，并用甲板上的枪炮对准伊朗船只，才驱离企图骚扰英国油轮的伊朗海军船只。
 </span>
</p>
<p class="p11">
 <span class="s1">
  据报导，蒙特罗斯号甲板装备有30釐米枪炮，特别设计用来驱离小型船只。
 </span>
</p>
<p class="p13">
 <span class="s1">
  一架美国飞机拍下整起事件发生过程的视频，从中可看到英国巡防舰将枪炮对准伊朗船只，并成功地警告他们撤退，此骚扰事件才得以结束。
 </span>
</p>
<p class="p15">
 <span class="s3">
  7
 </span>
 <span class="s1">
  月
 </span>
 <span class="s3">
  4
 </span>
 <span class="s1">
  日，英军在直布罗陀扣押一艘伊朗油轮，直布罗陀官员说，这艘船载运的货物据信是要运往受到欧盟制裁的叙利亚，英国的行为受到美国的赞扬。
 </span>
 <span class="s1">
  但之后伊朗扬言，若英国不马上把放走这艘油轮，伊朗就要扣押英国油轮作为报复。
 </span>
</p>
<p class="p5">
 <span class="s1">
  海湾地区紧张局势最近一直升级，伊朗当局宣称伊朗浓缩铀纯度已超出
 </span>
 <span class="s4">
  2015
 </span>
 <span class="s1">
  年签订核协议的
 </span>
 <span class="s4">
  3.
 </span>
 <span class="s1">
  上限，美国总统川普（特朗普）已经对伊朗加大了制裁，
 </span>
 <span class="s4">
  7
 </span>
 <span class="s1">
  月
 </span>
 <span class="s4">
  10
 </span>
 <span class="s1">
  日川普再次发推特警告，对伊朗的制裁很快就会
 </span>
 <span class="s4">
  “
 </span>
 <span class="s1">
  大幅增加
 </span>
 <span class="s4">
  ”
 </span>
 <span class="s1">
  。#
 </span>
</p>
<p class="p5">
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11378652.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11378652.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11378652.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/11/n11378652.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

