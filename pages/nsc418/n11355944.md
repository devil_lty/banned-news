### 过去两月 港人咨询移民台湾呈“爆炸性增长”
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1150190390-600x399-600x399.jpg"/>
<div class="red16 caption">
 6月22日深夜，香港百万抗议人群在游行结束后不仅未散去，而且依然大声疾呼撤回恶法、要求香港特首林郑月娥下台。(HECTOR RETAMAL/AFP/Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月01日讯】随着反“逃犯条例”修订的抗议行动不但升级，香港人移民海外的话题再受关注。有移民公司负责人表示，在港府最近修订“逃犯条例”引发上百万人上街示威后，咨询移民台湾的人数呈“爆炸性成长”。
</p>
<p>
 <span class="s1">
  香港中文大学亚太研究所
 </span>
 <span class="s2">
  2019
 </span>
 <span class="s1">
  年发布的一份调查报告显示，大多数香港人因
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  香港政治斗争太严重
 </span>
 <span class="s2">
  ”
 </span>
 <span class="s1">
  、
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  居住环境压迫
 </span>
 <span class="s2">
  ”
 </span>
 <span class="s1">
  及
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  不满意政治制度
 </span>
 <span class="s2">
  ”
 </span>
 <span class="s1">
  而选择移民。
 </span>
 <span class="s2">
  <span class="Apple-converted-space">
  </span>
 </span>
 <span class="s1">
  而语言跟文化都相近的台湾成为很多香港人的选项，此外还有加拿大和澳洲。
 </span>
</p>
<p class="p2">
 <span class="s1">
  办理港人移民台湾的朗峰国际移民公司香港负责人林耀宗此前对美国之音表示，2014年雨伞运动期间产生了一波港人移民台湾热，这波热潮在林郑月娥当选特首后开始减退，相信是很多港人观望林郑月娥的施政，在港人发现林郑月娥只不过是实行以前那套制度后，港人移民台湾的意愿更高。
 </span>
</p>
<p class="p2">
 <span class="s1">
  2018年后港人移民台湾热潮再度兴起，林耀宗说，尤其是过去两个月，咨询移民台湾的热度急剧攀升，呈“爆炸性增长”，他认为这是跟台港两地对“逃犯条例”修订的抗议情绪和示威活动有关。
 </span>
</p>
<p class="p2">
 <span class="s1">
  一位来台定居并经营青年旅馆的港人王女士对美联社说，最近她接到很多询问，都是问投资移民获得台湾公民权的可能性。
 </span>
</p>
<p class="p2">
 香港自6月9日起，爆发反对港府修订《引渡条例》（又称《
 <span href="http://www.epochtimes.com/gb/tag/%E9%80%83%E7%8A%AF%E6%9D%A1%E4%BE%8B.html">
  逃犯条例
 </span>
 》或《送中条例》）的抗议活动。这次“修例”受到中共的支持，引发了自香港主权移交以来最大规模的抗议。反对者担心，中共可能滥用该法律，把异己人士从香港引渡到中国内地受审。在那里他们可能被虐待或非法监禁。商业团体警告说，该议案将削弱香港的法律自治，动摇国际社会对香港这个金融中心的信心。
</p>
<p class="p2">
 6月9日的反“逃犯条例”大游行，人数达103万；6月16日，又有200万港人参加反送中大游行，创下香港游行人数历史最高纪录。
</p>
<p class="p2">
 7月1日是香港主权移交大陆22周年，香港民主派团体在这天举行大游行。港警发言人近日对媒体公开表示，游行当日安排中共军队或驻港部队介入是“好意见” ，引发外界谴责。熟悉中国问题的香港时事评论员
 <span href="http://www.epochtimes.com/gb/tag/%E7%A8%8B%E7%BF%94.html">
  程翔
 </span>
 表示，如果真这样，“一国两制就彻底没了。”
</p>
<p>
 <span class="s1">
  台湾移民署统计资料显示，2014年获得台湾定居许可的港人大约700人。2016年开始突破到1086人。2018年获得台湾居留权的香港与澳门居民总共有1267人。2019年的前四个月就有400港澳居民移居台湾。
 </span>
</p>
<h4 class="p2">
 港府讨好中共还是为港人福祉
</h4>
<p class="p4">
 <span class="s1">
  年幼时就从大陆移居香港的陈先生
 </span>
 <span class="s2">
  2018
 </span>
 <span class="s1">
  年带着妻子与两个孩子从香港搬到台北，他告诉德国之声说，离开香港是因为整体生活的质量已不尽理想。
 </span>
</p>
<p class="p7">
 <span class="s1">
  陈先生观察到香港社会中自我审查的情形变得越来越严重，而香港政府也变得越来越不顾民意。
 </span>
</p>
<p class="p7">
 <span class="s1">
  他表示
 </span>
 <span class="s2">
  ，
 </span>
 <span class="s1">
  近几年来，香港的媒体或公司渐渐开始进行自我审查。
 </span>
 <span class="s2">
 </span>
 <span class="s1">
  对一些比较明显反中共的东西，香港人都不太敢支持。
 </span>
</p>
<p class="p7">
 <span class="s1">
  “香港政府也变得不太管民意。
 </span>
 <span class="s1">
  他们想推什么就推了。
 </span>
 <span class="s1">
  不少香港人民对于政府的决策都会质疑它们是为了讨好中央，还是为了香港所有人的福祉。
 </span>
 <span class="s2">
  ”
 </span>
</p>
<p class="p4">
 <span class="s1">
  此外，陈先生说：
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  在香港生活，没有生活感，只有生存感。
 </span>
 <span class="s1">
  香港房价很贵，租金也很贵。
 </span>
 <span class="s1">
  商场、超市里的东西也都变得很贵。
 </span>
 <span class="s1">
  虽然我在香港赚的钱比在台湾赚得多，但我自己能支配的现金是很少。”
 </span>
</p>
<p class="p7">
 <span class="s1">
  对于王小姐而言，
 </span>
 <span class="s2">
  2014
 </span>
 <span class="s1">
  年的雨伞运动是中国逐渐加强对香港公民社会控制的起始点。
 </span>
 <span class="s1">
  她在雨伞运动中，全程目睹香港政府与警察驱离示威者的过程。
 </span>
</p>
<p class="p7">
 <span class="s1">
  她告诉德国之声，
 </span>
 <span class="s1">
  在雨伞运动期间，她仍然对香港抵抗中共渗透的能力抱有希望，但在运动结束的那一刻，
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  我也意识到这些希望都只是错觉。
 </span>
 <span class="s1">
  此后，我便开始思考移居海外的可能性。
 </span>
 <span class="s2">
  ”
 </span>
</p>
<p class="p7">
 <span class="s1">
  王小姐表示，
 </span>
 <span class="s2">
  2014
 </span>
 <span class="s1">
  年开始，香港政府不但通过不同的理由或法律来阻止特定香港人参与政治活动，更试图透过《国歌法》或《逃犯条例》修正草案等法案来限缩香港公民社会的空间。
 </span>
</p>
<p class="p7">
 <span class="s1">
  在她眼中，这一切都与中共加强对香港的掌控有密切关系。
 </span>
</p>
<p class="p7">
 <span class="s1">
  王小姐说：
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  随着中国移居香港的人数越来越多，香港人必须开始与这些移民争夺原本属于他们的医疗与教育资源。
 </span>
 <span class="s1">
  而越涨越高的房价，也让年轻一代的香港人对拥有自己的家感到无望。
 </span>
 <span class="s2">
  ”
 </span>
</p>
<p class="p7">
 <span class="s2">
  在台生活多年的香港人、专栏作家温朗东对BBC表示，
 </span>
 上世纪80年代，香港是华人世界里最自由开放的地方，但如今只有台湾拥有相对彻底的民主自由环境。 温朗东说：“香港人在台湾找到过去的香港，可能是种怀旧情怀。”
 <span class="s2">
  #
 </span>
</p>
<p class="p7">
 责任编辑：高静
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11355944.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11355944.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11355944.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/30/n11355944.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

