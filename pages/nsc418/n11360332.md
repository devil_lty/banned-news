### 非法向中共出口导弹芯片 美华裔18项罪成
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/Shih-e1560998840246.jpg"/>
<div class="red16 caption">
 <p>
  加州大学洛杉矶分校教授石怡驰（音译，Yi-Chi Shih，左）和他在加拿大的兄弟、蒙特利尔麦吉尔大学的教授石怡祥（Ishiang Shih，音译）。 （UCLA/McGill 网站）
 </p>
</div>
<hr/><p>
 【大纪元2019年07月03日讯】（大纪元记者洪雅文报导）周二（7月2日），美国司法部发布新闻稿说，加州大学洛杉矶分校（UCLA）工程学系兼任教授石怡驰（Yi-Chi Shih，音译）被控犯多项联邦刑事罪，共谋非法获取美国军事应用的集成电路芯片走私中国，且罪名成立。
</p>
<p>
 现年64岁的石怡驰，和共同被告65岁的
 <span href="http://www.epochtimes.com/gb/tag/%E6%A2%85%E6%9D%B0%E5%AE%89.html">
  梅杰安
 </span>
 （Kiet Ahn Mai，音译）曾是同事关系。石怡驰长年在外旅行，在美国住不到一半时间，在南加有很多房地产，在中国与台湾也有房地产。梅杰安是工程师出身，家住加州帕萨迪纳（Pasadena）。
</p>
<p>
 根据审判时提供的证据，他们安排
 <span href="http://www.epochtimes.com/gb/tag/%E6%A2%85%E6%9D%B0%E5%AE%89.html">
  梅杰安
 </span>
 冒充客户，从美国特殊芯片制造公司购买单片微波集成电路（MMIC），谎称要在美国国内使用，但得手后就交由石怡驰运往中国。
</p>
<p>
 这款遭盗的单片微波集成电路是一种宽带、高功率的
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%8A%E5%AF%BC%E4%BD%93%E8%8A%AF%E7%89%87.html">
  半导体芯片
 </span>
 ，仅可在美国使用，可用于导弹、导弹控制系统、战斗机、电子战和雷达应用。受害公司的半导体芯片涉及诸多商业和军事系统，其客户包括美国空军、海军和国防高级研究计划局。
</p>
<p>
 调查指出，被石怡驰运往中国的
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%8A%E5%AF%BC%E4%BD%93%E8%8A%AF%E7%89%87.html">
  半导体芯片
 </span>
 ，后来被送到成都
 <span href="http://www.epochtimes.com/gb/tag/%E5%98%89%E7%9F%B3%E7%A7%91%E6%8A%80.html">
  嘉石科技
 </span>
 公司（CGTC）。嘉石科技是中国一家专门制造MMIC的公司，于2014年被美国商务部列入实体管制名单。而石怡驰正好曾是该公司总裁。
</p>
<p>
 经过六个星期的审判，石怡驰上周三（6月26日）除了被判串谋违反联邦法律《国际紧急经济权力法》（ International Emergency Economic Powers Act，IEEPA），大陪审团还认定石某犯有邮件欺诈、电汇欺诈，定期缴交虚假纳税申报表向政府机构作出虚假陈述，以及在未经授权情况下访问受保护的电脑获取信息等，总计被判18项罪名，并批准政府要求没收其75万美元财产的申请。
</p>
<p>
 根据法院文件，石怡驰参与违反美国国家安全和外交政策利益的活动，涉及非法采购，最终向中共提供未经授权的军事应用设备。
</p>
<p>
 加州洛杉矶地方法院法官约翰·克朗施塔特（John A. Kronstadt）周一（7月1日）表示，他将重选新陪审团，稍后决定是否没收石怡驰的财产，并安排量刑听证会，石男或面临最高刑期219年。
</p>
<p>
 石怡驰和梅杰安于去年1月被起诉，后者在去年12月认罪，罪名为一项重罪，将于今年9月19日被判刑，届时他将面临法定最高刑期10年徒刑。
</p>
<p>
 此案还有第三名嫌犯、加拿大人石怡祥（Ishiang Shih，音译），是石怡驰的弟弟，蒙特利尔麦吉尔大学（McGill University）的教授，他也被控与该案有关，但他至今没有被引渡，在加拿大能自由活动。
</p>
<p>
 该案件由联邦调查局、美国商务部、工业和安全局、出口执法办公室、国税局和加拿大骑警合作调查。#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360332.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360332.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11360332.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/2/n11360332.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

