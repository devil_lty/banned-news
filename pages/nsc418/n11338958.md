### 香港大规模反送中 凸显中共体制脆弱性
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/1906162243372378-600x400-2.jpg"/>
<div class="red16 caption">
 <p>
  美媒指出，香港反“送中”大游行让人关注到中共政治体制的脆弱。（蔡雯文／大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月22日讯】（大纪元记者张婷综合报导）香港近日爆发空前的反“送中”大游行，不仅引发全球对这个国际金融中心的关注，也让中共政府在这一事件背后扮演的角色登上国际舆论焦点。美媒6月20日发文说，香港事件让人关注中共政治体制的脆弱性。
</p>
<p>
 香港近两周来连续爆发大规模抗议活动，抗议人潮在香港市中心涌动，反对香港政府修订中共支持的《引渡条例》（也称《逃犯条例》、《送中条例》）。抗议活动在上周日（6月16日）约200万人的规模达到高潮。
</p>
<p>
 根据香港政府统计处的数据，香港在2018年底有大约748万人口。200万的游行规模意味着每不到4个人中就有1人走入了抗议队伍。周五（6月21日），抗议者包围了政府总部，要求当局回应包括撤回恶法在内的四项诉求。
</p>
<p>
 《华盛顿邮报》专栏作家法瑞德·扎卡亚（Fareed Zakaria）发文表示，虽然很难预测这个事件最终将会走向何方，但它凸显中共政治体制的脆弱性。
</p>
<h4>
 中共政治体制变得更具镇压性 并加强审查制度
</h4>
<p>
 扎卡亚表示，几十年的政治科学研究表明，经济增长与民主之间存在着相当强的联系。随着各国追求经济现代化，通常也会被迫改变他们的社会，最终改变其政治制度，使其更加开放、负责任和民主。但中共统治下的中国并非如此。中国虽然成为了世界第二大经济体，但仍然保持“绝对的非民主”。近年来，中共的政治体制变得更加具有镇压性，并加强了审查制度。
</p>
<p>
 彭博社报导，就在近日来巨大的抗议人潮在香港市中心涌动，反对中共支持的引渡条例之际，另一场战争却在大陆展开。中共的防火墙正在封锁中国互联网，网民对
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF%E6%8A%97%E8%AE%AE.html">
  香港抗议
 </span>
 者表示支持的帖子被删除，让中国人没法看到这场自97年以来最大规模香港抗议游行的照片和信息。
</p>
<figure class="wp-caption aligncenter" id="attachment_11335238" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/1906161511461528-1-600x400-1.jpg">
  <img alt="" class="size-large wp-image-11335238" src="http://i.epochtimes.com/assets/uploads/2019/06/1906161511461528-1-600x400-1-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月16日，200万香港市民以各种自制标语要求撤回送中恶法。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 大陆一位搞IT的工程师李先生向大纪元记者表示，“中共对香港游行的封杀十分严密，绝大多数的大陆人一无所知。国内互联网公司的屏蔽手段非常高级，应该是动用了人工智能。所以这次活动大陆人没有感觉。”
</p>
<p>
 李先生说，自己翻墙出来看到香港这么壮观的游行队伍时很震撼，“非常受到鼓舞，支持香港人民反抗暴政”。
</p>
<h4>
 中共的政治体制正面临压力
</h4>
<p>
 《华盛顿邮报》专栏作家扎卡亚表示，中共的政治体制面临着真正的压力。中国共产党通过承诺国家增长和动用武力来维持其权力。它利用精心设计的审查制度，并对其本国人民进行日益复杂的间谍活动。而目前，中共面临着数百万明确要求民主民众的挑战。对中共领导人来说，这个问题可能会比中美贸易战更大。
</p>
<p>
 分析指出，中国民众对民主体制的诉求凸显中共的镇压式政治体制的脆弱。
</p>
<figure class="wp-caption aligncenter" id="attachment_11336031" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085728100615.jpg">
  <img alt="" class="size-large wp-image-11336031" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085728100615-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  香港“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达
  <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
   反送中
  </span>
  的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<p>
 “我们无法知道大陆公民对自由和民主体制的态度是什么，因为在（中共）严格控制公民的制度下，外界不可能对这些敏感问题得到诚实的答案。”《华盛顿邮报》专栏作家梅根‧麦卡德尔（Megan McArdle）6月19日撰文说，“但如果你质疑目前是否有相当数量的中国人想要民主，你只需看看上周日（16日）香港的照片。”
</p>
<p>
 “我们所知道的是，在香港，人们愿意公然反抗（中共）政权来维护自由。也许自由主义制度是一种后天的品味，也许一旦你有了品味，你永远不会满足于其它任何东西。”麦卡德尔写道。
</p>
<p>
 她说，这可能就是为什么中共想把香港变得不那么特别、加进更多行政管理的举措，会遭遇如此强烈的抵抗的原因。
</p>
<figure class="wp-caption aligncenter" id="attachment_11336039" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/190620085629100615.jpg">
  <img alt="" class="size-large wp-image-11336039" src="http://i.epochtimes.com/assets/uploads/2019/06/190620085629100615-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  香港“连侬墙”，市民以文字和简单图画抗议港警的暴力行为及表达
  <span href="http://www.epochtimes.com/gb/tag/%E5%8F%8D%E9%80%81%E4%B8%AD.html">
   反送中
  </span>
  的心声。（余钢／大纪元）
 </figcaption><br/>
</figure><br/>
<h4>
 港人对中共政治体制下的大陆法治极度不信任
</h4>
<p>
 BBC、美国之音等媒体都发表了相关报导表示，反“送中”大游行揭示了中国人对中共政府的不信任，对中共政治体制下的大陆法治的极度不信任。
</p>
<p>
 在中国大陆，任何被视为是对中共政府或中共“威胁”的人都可以被禁声、消失或被投入大牢。此类案件的讨论基本被禁止。但在香港，此类案件可以被公开讨论、分析和批评。
</p>
<p>
 报导指出，“一国两制”模式本是应该保证香港能够保持独立的司法体制，以及1997年香港这个前英国殖民地交还给中国时享有的自由。但北京方面近年来采取激进措施，制止香港民主的发展。现在反对人士认为《引渡条例》修订可以让任何人成为中共政府的压制对象，这就让人担心，“一国两制”的保证正在快速消退。
</p>
<h4>
 中共体制受到大陆新移民的抵制
</h4>
<p>
 特别引人注意的是，这次
 <span href="http://www.epochtimes.com/gb/tag/%E9%A6%99%E6%B8%AF%E6%8A%97%E8%AE%AE.html">
  香港抗议
 </span>
 行动中，有不少从大陆到香港的新移民积极参与。其中一名叫妮珂的新移民接受自由亚洲电台采访时表示，以前不关心政治的新移民，这次都积极参加抗议，要争个鱼死网破。她还说，她身边的很多人都在把港币换成美元，以防不测。
</p>
<p>
 在被问及新移民为什么要参加这次大游行时，妮珂表示，原因很简单，她只要和大家说，“大陆法律搬来香港”，这一句话就搞定让大家都出来（抗议）了。“他们（新移民）说，哦，那就不行喽，我千辛万苦才来到香港，为什么要让香港和大陆一样呢？”
</p>
<p>
 “这一次我们是全民抗暴、全香港抗共，第一次面对面地、真正地抵抗这个共产党。不是哪个人、哪个团体，个个都有份。因为共产党，我们都知道它不可信。这个条例通过以后，它可以抓人，想怎样就怎样。我们觉得还是抵抗它，留我们的三权分立。这次香港人非常齐心，很难得。”妮珂说。
</p>
<figure class="wp-caption aligncenter" id="attachment_11335484" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-1150190302.jpg">
  <img alt="" class="size-large wp-image-11335484" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-1150190302-600x399.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  2019年6月16日，游行期间，人群很守秩序，数次有救护车、担架床、救护电单车、轮椅队伍，大家都自动让开，还会为感谢救护人员的努力，以及支持伤健人士身体力行，欢呼拍掌。(HECTOR RETAMAL/AFP/Getty Images)
 </figcaption><br/>
</figure><br/>
<h4>
 中共低估了香港人对一党专政的不信任
</h4>
<p>
 《日经亚洲评论》表示，中共领导人似乎有信心，大陆已经加固了对香港的控制，但中共似乎低估了香港人对大陆一党专政的不信任。
</p>
<p>
 香港特首林郑月娥6月15日宣布了中共政府支持的无限期暂缓修订《引渡条例》。在这之前，她先是前往深圳与中共政府官员会面，并带回来了这个在不失脸面的同时，力求恢复秩序的决定。
</p>
<p>
 暂缓而不是撤回修例。《日经》分析说，对于中共领导人来说，这是一个前所未有的折中，中共很可能希望让这个事件暂时平息下来。
</p>
<p>
 然而，中共领导人错估了香港人。第二天，大约200万港人再次走上街头，要求完全废除修改《引渡条例》。
</p>
<p>
 报导说，香港的政治动荡在林郑月娥宣布暂缓修例后没有任何松缓的迹象。港人对北京强硬战术的多年不满正达到沸点。
</p>
<figure class="wp-caption aligncenter" id="attachment_11219684" style="width: 600px">
 <span href="http://i.epochtimes.com/assets/uploads/2019/04/4e9af63f4523db2cc487d2b85a979076.jpg">
  <img alt="" class="size-large wp-image-11219684" src="http://i.epochtimes.com/assets/uploads/2019/04/4e9af63f4523db2cc487d2b85a979076-600x400.jpg"/>
 </span>
 <br/><figcaption class="wp-caption-text">
  6月16日，有200万的港人参加游行，要求香港政府撤回修订“逃犯条例”等5项诉求。 (Anthony WALLACE / AFP)
 </figcaption><br/>
</figure><br/>
<h4>
 反“送中”抗议令中共领导人为之震荡
</h4>
<p>
 《经济学人》说，香港的强烈反抗令香港政府和北京领导人为之震荡。文章总结了香港人民上周抗议的三大特点：
</p>
<p>
 其一，人数空前；其二，参加抗议的大多数人是年轻人，他们对北京对香港的严厉管制不满，并非因为怀念英国殖民时期统治的缘故；其三，本次抗争显示了香港人民非凡的勇气。自从2014年“雨伞运动”以来，中共曾明确表示将不再容忍“犯上”行为。但香港抗议者顶住催泪瓦斯、橡皮子弹以及法律报复的威胁再次走上街头抗争，因为他们知道他们的抗争关系到香港的未来。
</p>
<p>
 《华日》引述香港民主派立法会议员杨岳桥（Alvin Yeung）的话说，香港人并不愚蠢，他们知道什么时候斧头会砍下来。
</p>
<p>
 报导还引述25岁的活动人士罗冠聪（Nathan Law）的话说，香港人对逆境的容忍度很高，但一旦你越过了他们的底线，他们便会强势爆发。罗冠聪表示，过去几年紧张局势一直在发酵，直至达到沸点。#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11338958.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11338958.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11338958.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/21/n11338958.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

