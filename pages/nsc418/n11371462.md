### 菲律宾天空出现神秘光柱
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/Fotolia_74280675_Subscription_L-600x400.jpg"/>
<div class="red16 caption">
 菲律宾苏禄省在6月30日晚上出现神秘光柱。图为金光闪耀的示意图，与本文无关。(Fotolia)
</div>
<hr/><p>
 【大纪元2019年07月08日讯】（大纪元记者陈俊村报导）苏禄群岛（Sulu Archipelago）位于
 <span href="http://www.epochtimes.com/gb/tag/%E8%8F%B2%E5%BE%8B%E5%AE%BE.html">
  菲律宾
 </span>
 西南部，是该国的一个省份。该省的
 <span href="http://www.epochtimes.com/gb/tag/%E5%A4%A9%E7%A9%BA.html">
  天空
 </span>
 中最近出现壮观的
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%89%E6%9F%B1.html">
  光柱
 </span>
 ，其照片已经在网上走红。
</p>
<p>
 据
 <span href="http://www.epochtimes.com/gb/tag/%E8%8F%B2%E5%BE%8B%E5%AE%BE.html">
  菲律宾
 </span>
 媒体报导，包括吉达拉（Amarkhan Jidara）在内的脸书用户分享了几张
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%89%E6%9F%B1.html">
  光柱
 </span>
 的照片，其拍摄角度各有不同。而吉达拉是在6月30日晚上7点左右拍到这些照片的，这据信是今年第二次出现的光柱。
</p>
<p>
 吉达拉表示，这个景象在
 <span href="http://www.epochtimes.com/gb/tag/%E5%A4%A9%E7%A9%BA.html">
  天空
 </span>
 中持续了20至30分钟，让当地居民感到惊奇。
</p>
<p>
 不过，这些光柱并非十分罕见，苏禄群岛的原住民陶苏格人（Tausug people）声称，这种景象一年会出现一两次。在陶苏格语中，它被称为“蜡烛”（Lansuk-Lansuk）。
</p>
<p>
 有些陶苏格人认为这种景象会带来厄运或悲剧，但也有人认为它是好运的征兆。
</p>
<p>
</p>
<p>
 类似这样的神秘光柱，在世界上的其它地方也出现过。
</p>
<p>
 在2015年5月22日凌晨，日本鸟取市北方天空出现超过20条光柱，有目击者表示，这么大规模的光柱还是头一次见到。
</p>
<p>
 鸟取市天文爱好者多贺利宽当天进行了约2个小时的观测。他先前曾多次观测到5条左右的光柱，但第一次见到这么多光柱。
</p>
<p>
 除了鸟取市之外，在本州岛最西端的山口县下关市，也有民众目睹这些梦幻般的光柱，同时拍下照片。
</p>
<blockquote class="twitter-tweet" data-lang="zh-tw">
 <p dir="ltr" lang="ja">
  【New!】幻想的な光の柱“渔火光柱” 下関で捉えた（画像）
  <span href="http://t.co/Lg2oNViQ5Z">
   http://t.co/Lg2oNViQ5Z
  </span>
  <span href="http://t.co/bxTqdAZRM5">
   pic.twitter.com/bxTqdAZRM5
  </span>
 </p>
 <p>
  — ハフポスト日本版 (@HuffPostJapan)
  <span href="https://twitter.com/HuffPostJapan/status/602429107255803904?ref_src=twsrc%5Etfw">
   2015年5月24日
  </span>
 </p>
</blockquote>
<p>
 <p>
  另外在2017年10月24日，阿根廷费利西亚诺（Feliciano）一名女子拍到天空中出现不明光柱，犹如通天的“电梯”，从地面直达天际。
 </p>
 <p>
  这个神秘现象出现在当地的圣荷西公园（San Jose Park）。该名女子在公园内散步时，一道垂直的光柱突然出现在她面前，矗立于天地之间。她将这个神秘现象拍了下来。#
 </p>
</p>
<blockquote class="twitter-tweet" data-lang="zh-tw">
 <p dir="ltr" lang="en">
  Mysterious light column in the sky over Feliciano, Argentina. 10/26/17:
  <span href="https://t.co/wJ2ZAt6rCa">
   https://t.co/wJ2ZAt6rCa
  </span>
  <span href="https://t.co/H4GjECKy2v">
   pic.twitter.com/H4GjECKy2v
  </span>
 </p>
 <p>
  — Skywatch Media News (@skymednews)
  <span href="https://twitter.com/skymednews/status/924035497151754240?ref_src=twsrc%5Etfw">
   2017年10月27日
  </span>
 </p>
</blockquote>
<p>
 <p>
  责任编辑：林琮文
 </p>
</p>
<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11371462.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11371462.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11371462.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/8/n11371462.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

