### 中共收购美合唱学院告吹 运作黑幕也曝光
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/Williamson_Hall.jpg"/>
<div class="red16 caption">
 西敏寺合唱学院是美国杰出学术机构之一，主要教授基督教福音派传统高级音乐研究。（维基共享资源图片）
</div>
<hr/><p>
 【大纪元2019年07月03日讯】（大纪元记者洪雅文编译报导）美国新泽西州莱德大学（Rider University）去年六月宣布，将把旗下近100年历史的
 <span href="http://www.epochtimes.com/gb/tag/%E8%A5%BF%E6%95%8F%E5%AF%BA%E5%90%88%E5%94%B1%E5%AD%A6%E9%99%A2.html">
  西敏寺合唱学院
 </span>
 （Westminster Choir College）卖给中共国营企业
 <span href="http://www.epochtimes.com/gb/tag/%E5%8C%97%E4%BA%AC.html">
  北京
 </span>
 凯文德信教育科技股份有限公司（以下简称
 <span href="http://www.epochtimes.com/gb/tag/%E5%87%AF%E6%96%87%E6%95%99%E8%82%B2.html">
  凯文教育
 </span>
 ）。此举立即引发一群教职员和校友联手起诉校方违反校内协议，并警告此次出售案将扩大
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1%E6%B8%97%E9%80%8F.html">
  中共渗透
 </span>
 美国学术界的威胁。
</p>
<p>
 目前这起收购案已经告吹，而中共机构如何隐瞒身份、创造实体实施收购案的幕后手段也浮上水面。
</p>
<p>
 福克斯新闻报导，经历时一年多的调查搜证发现，
 <span href="http://www.epochtimes.com/gb/tag/%E5%87%AF%E6%96%87%E6%95%99%E8%82%B2.html">
  凯文教育
 </span>
 前身竟为中共海军国防承包商，在宣布有意收购学校的前三周才改名，摇身一变成国营教育机构。近日
 <span href="http://www.epochtimes.com/gb/tag/%E8%A5%BF%E6%95%8F%E5%AF%BA%E5%90%88%E5%94%B1%E5%AD%A6%E9%99%A2.html">
  西敏寺合唱学院
 </span>
 教职人员和校友终于成功阻止收购案继续进行。
</p>
<p>
 位于新泽西普林斯顿市中心的西敏寺合唱学院，是由俄亥俄州代顿市一间长老会教堂，于1929年成立的音乐学校，是美国杰出学术机构之一，主要教授基督教福音派传统高级音乐研究，培育出很多纽约大都会歌剧院的歌唱家和指挥家。
</p>
<p>
 去年六月，莱德大学希望以4000万美元的价格将西敏寺合唱学院转售凯文教育，引发哗然。校友指出，凯文教育打算经营这所学校，收购后，该校即其归子公司
 <span href="http://www.epochtimes.com/gb/tag/%E5%8C%97%E4%BA%AC.html">
  北京
 </span>
 文化学新教育（Bejing Wenhuaxuexin Education）所有，成为中共政府拥有和管制的营利性国营实体。
</p>
<p>
 校友起诉小组表示，凯文教育曾在宣布其有意购买学院的前三周更改公司名称，原名和教育学术完全无关，叫做江苏中泰桥梁钢构股份有限公司，是中共海军舰艇部件的制造商和国防承包商。小组披露，他们（凯文教育）显然“创造了一个教育实体的外观来证明其收购合唱学院的合理性”。
</p>
<p>
 “中国（中共）现在已经放弃（收购该校），它们试图控制美国大学，这种趋势威胁到美国学术和知识自由。”西敏寺合唱学院校友的辩护律师布鲁斯‧阿夫兰（Bruce Afran）说，“今天美国的学术自由能变得更加强大，因为中国（中共）控制美国大学的情况正在减少，这种趋势现在有望停止。”
</p>
<p>
 阿夫兰表示，取消这笔交易是保护美国高等教育机构免受
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%85%B1%E6%B8%97%E9%80%8F.html">
  中共渗透
 </span>
 的第一步。“目前有数十所美国大学已经面临压力，无法满足中国（中共）政府对课程和公众演讲者的要求，而面临失去中方对交换学生项目和孔子学院的财政支持，这些学院目前已在一百多所美国大学中出现。”
</p>
<p>
 “由中共政府控制的公司计划收购西敏寺合唱学院一案，是中共首次收购一所美国大学，而且在这个案例中，西敏寺合唱学院是世界领先的文化机构之一。”他说。
</p>
<p>
 “这是一场巨大的胜利。”校友康斯坦斯‧费（Constance Fee）说，“莱德大学不得不放弃这项收购案，因为此举将摧毁一流的音乐学校和国际文化界的指标。多年来，我作为欧洲歌剧界的一员，罗伯特‧卫斯理学院（Roberts Wesleyan College）的音乐教授，我可以说我们在保护文化财富和知识自由方面迈出了重要的一步。”
</p>
<p>
 中共收购案证实告吹，莱德校方表示，他们将继续经营西敏寺合唱学院，并同时与凯文教育“探索另一种关系”，双方将就未来三年的学术和艺术活动达成协议。
</p>
<p>
 “在整个过程中，西敏寺合唱学院作为世界一流的机构，我们不断寻求保护和提升该学院。”莱德校长格雷戈里‧戴尔奥莫（Gregory G. Dell’Omo）在声明中说，“鉴于这次交易案的复杂性，越来越清楚的是，与外部实体合作，像凯文这样的实体，即使是在可行的时间表上也不可行。”
</p>
<p>
 本次案件除了校友团体、活动家和特伦顿的州立法机构表示反对，州检察长办公室也介入调查。总检察长古尔皮沃‧雷瓦尔（Gurbir S. Grewal）指责校方没有回应其询问，并且隐瞒有关出售的相关信息。
</p>
<p>
 州议会决议还表示，“将合唱学院出售给中共政府所有公司的提议可能会危及美国公民的安全。”#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11361906.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11361906.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11361906.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/3/n11361906.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

