### 市场预期美联储降息 道指纳指收于历史高位
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1151299084-600x400.jpg"/>
<div class="red16 caption">
 美国股市周三（7月3日）收于创纪录高点。图为纽约证券交易所
。(Drew Angerer/Getty Images)
</div>
<hr/><p>
 【大纪元2019年07月04日讯】（大纪元记者洪雅文编译报导）周三（7月3日）美国股市收盘创历史
 <span href="http://www.epochtimes.com/gb/tag/%E6%96%B0%E9%AB%98.html">
  新高
 </span>
 ，
 <span href="http://www.epochtimes.com/gb/tag/%E6%8A%95%E8%B5%84.html">
  投资
 </span>
 者押注
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E8%81%94%E5%82%A8.html">
  美联储
 </span>
 本月可能在商务部发布经济数据低于预期的消息后降息。
</p>
<p>
 道琼斯工业平均指数上涨179.32点，收于26,966点，创历史
 <span href="http://www.epochtimes.com/gb/tag/%E6%96%B0%E9%AB%98.html">
  新高
 </span>
 。
</p>
<p>
 纳斯达克综合指数上涨0.7%至8,170.23点，亦是收市新高。
</p>
<p>
 标准普尔500指数也上涨0.7%至2,995.82，因房地产和消费品板块推动广泛指数达到创纪录水平。科技股也推动该指数上涨0.7%至历史新高。
</p>
<p>
 标准普尔500指数收盘仅下跌0.1%，低于3,000点。
</p>
<p>
 脸书、亚马逊、Netflix（官方中文名：网飞）和谷歌母公司Alphabet的股票均上涨。由于周四（7月4日）是美国独立日，周三交易提前在美东时间下午1点收盘。
</p>
<p>
 美国股市资讯网站T3Live合伙人斯科特‧雷德勒（Scott Redler）告诉CNBC，“这是我们第一次到达这一水平，你可能会看到一些获利回吐。如果科技股继续走强，而且半导体类股出现反弹，可能会推动标普500指数突破3000点大关。”
</p>
<p>
 ADP研究机构及穆迪分析公司表示，美国6月份私人就业人数增加了10.2万人。美国道琼斯调查的经济学家则预计增长13.5万人。
</p>
<p>
 这些数据增强了
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E8%81%94%E5%82%A8.html">
  美联储
 </span>
 在7月底货币政策会议上降息的理由。央行曾在6月表示将采取适当行动维持当前的经济扩张，从而为宽松的货币政策打开了大门。
</p>
<p>
 “美联储几乎有100%的机会在7月会议上宣布降低利率。我认为美联储将会看到美国的经济指标开始放缓。”顾问资产管理公司（Advisors Asset Management）首席
 <span href="http://www.epochtimes.com/gb/tag/%E6%8A%95%E8%B5%84.html">
  投资
 </span>
 官史考特‧柯力尔（Scott Colyer）说。
</p>
<p>
 美国总统川普（特朗普）3日的一条推文也呼吁采取更宽松的货币政策，增加了投资人对美联储放松政策的预期。川普在文中写道，美国应该“奉陪”中共、欧洲等地的货币政策，并指出这些国家“正在进行大规模的货币操纵游戏，将资金注入它们的体系，以便和美国竞争”。#
</p>
<blockquote class="twitter-tweet" data-width="550">
 <p dir="ltr" lang="en">
  China and Europe playing big currency manipulation game and pumping money into their system in order to compete with USA. We should MATCH, or continue being the dummies who sit back and politely watch as other countries continue to play their games – as they have for many years!
 </p>
 <p>
  — Donald J. Trump (@realDonaldTrump)
  <span href="https://twitter.com/realDonaldTrump/status/1146423819906748416?ref_src=twsrc%5Etfw">
   July 3, 2019
  </span>
 </p>
</blockquote>
<p>
</p>
<p>
 责任编辑：李缘
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11362556.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11362556.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11362556.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/3/n11362556.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

