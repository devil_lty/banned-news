### 消息：印度拟以奖励措施 吸引企业撤离中国
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/GettyImages-963451580-600x400.jpg"/>
<div class="red16 caption">
 <p>
  彭博社援引消息说，印度政府正在考虑提供奖励措施，以吸引该国公司撤离中国。图为2018年5月28日，印度阿默达巴德（Ahmedabad）郊区的一家电动车工厂。(SAM PANTHAKY/AFP/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年06月25日讯】（大纪元记者陈俊村编译报导）在
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E4%B8%AD%E8%B4%B8%E6%98%93%E6%88%98.html">
  美中贸易战
 </span>
 的冲击下，在
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD.html">
  中国
 </span>
 的外资企业纷纷转移阵地，以规避可能被加征的
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%B3%E7%A8%8E.html">
  关税
 </span>
 。有美国媒体援引消息说，
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%B0%E5%BA%A6.html">
  印度
 </span>
 政府正在考虑提供税收优惠等奖励措施，以吸引该国公司撤离
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E5%9B%BD.html">
  中国
 </span>
 ，此举除了规避
 <span href="http://www.epochtimes.com/gb/tag/%E5%85%B3%E7%A8%8E.html">
  关税
 </span>
 之外，也将有助于减少印中
 <span href="http://www.epochtimes.com/gb/tag/%E8%B4%B8%E6%98%93%E9%80%86%E5%B7%AE.html">
  贸易逆差
 </span>
 。
</p>
<p>
 彭博社引述一名知情人士的话说，
 <span href="http://www.epochtimes.com/gb/tag/%E5%8D%B0%E5%BA%A6.html">
  印度
 </span>
 政府目前正在考虑的奖励措施包括税收优惠，以及越南政府用来吸引外资的免税期等。
</p>
<p>
 该社取得的一份印度商工部文件显示，这些奖励措施适用的产业包括：电子产品、消费性家电、电动车、鞋类和玩具等。
</p>
<p>
 这份文件指出，其它措施包括在印度沿岸设立廉价工业区、政府采购时优先选择本土制造商等，进而诱使公司寻找中国之外的生产基地。
</p>
<p>
 这项奖励方案将有助于印度制造业成长、减少印中贸易赤字，并协助该国总理莫迪（Narendra Modi）达成“印度制造”的目标，其目标是在2020年之前提升制造业的成长至该经济体的25%。
</p>
<p>
 由监管外国直接投资政策的产业部门所做的分析显示，来自中国公司的投资可以流向手机和元件生产、消费性家电、电动车与零件、日常用品等产业，目前印度这些产品的95%是从中国进口的。
</p>
<p>
 另据印度《经济时报》报导，该国商工部的一项研究报告指出，
 <span href="http://www.epochtimes.com/gb/tag/%E7%BE%8E%E4%B8%AD%E8%B4%B8%E6%98%93%E6%88%98.html">
  美中贸易战
 </span>
 可能造成全球贸易型态的改变，而这场贸易战也为印度提供了一个机会，可对美中两国增加多达350项产品的出口。
</p>
<p>
 其中，可以抢攻中国市场的印度产品包括：铜矿、橡胶、纸与纸板、网络传输设备、输送管等。而可以增加对美出口的产品则包括：工业用阀门、硫化橡胶、碳或石墨电极、天然蜂蜜等。#
</p>
<p>
 责任编辑：林琮文
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11344992.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11344992.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11344992.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/25/n11344992.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

