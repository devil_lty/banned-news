### 生意伙伴谈川普：外表强硬 内心慈善
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-963609516-600x400.jpg"/>
<div class="red16 caption">
 <p>
  图为美国总统川普（特朗普）。(Win McNamee/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月05日讯】（大纪元记者徐简综合报导）美国总统
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E6%99%AE.html">
  川普
 </span>
 （
 <span href="http://www.epochtimes.com/gb/tag/%E7%89%B9%E6%9C%97%E6%99%AE.html">
  特朗普
 </span>
 ）是全球瞩目的明星，他身上有各种褒义和贬义的标签，那么他身边的人是怎么看他的呢？让我们听一听川普年轻时的商业伙伴、纽约律师事务所Meister Seelig＆Fein的创始合伙人斯蒂芬‧梅斯特（Stephen Meister）是怎么说的。
</p>
<p>
 梅斯特曾代表
 <span href="http://www.epochtimes.com/gb/tag/%E5%B7%9D%E6%99%AE.html">
  川普
 </span>
 处理过多起商业案件，他表示，川普是一个外表强硬但是富有慈善之心的人，他深爱家庭，还是一位杰出的谈判专家。
</p>
<h4>
 不希望“与任何国家发生敌对”
</h4>
<p>
 梅斯特表示，川普不是好战之人，不希望“与任何国家发生敌对”。他举例说，川普处理美国与
 <span href="http://www.epochtimes.com/gb/tag/%E4%BC%8A%E6%9C%97.html">
  伊朗
 </span>
 的冲突时，他的同情心和平衡理念主导了美国的外交政策。
</p>
<p>
 “他非常注重保持平衡。实际上他富有慈善之心，非常关心双方的人员损失，并希望只进行对等的回应。”梅斯特在接受英文大纪元资深记者者扬‧耶凯利克（Jan Jekielek）的采访时说道。扬‧耶凯利克是英文大纪元系列访谈节目“美国思想领袖”（
 <wbr/>
 American Thought Leader）的主持人。
</p>
<p>
 五角大楼确认，6月20日，
 <span href="http://www.epochtimes.com/gb/tag/%E4%BC%8A%E6%9C%97.html">
  伊朗
 </span>
 在国际空域击落了一架美国无人机。川普原本当天同意攻击伊朗雷达和导弹发射台等目标，时间设定在周五（6月21日）的拂晓时分，以期将伊朗士兵或平民的可能伤亡减至最低。
</p>
<p>
 川普后来表示，在被告知美国的攻击行动将会导致150个伊朗人死亡后，他在攻击前10分钟取消了这一次军事攻击行动，并在推特上说，“这（150人死亡的代价）与击落一架无人机不成比例。”
</p>
<p>
 梅斯特说，国家安全顾问约翰博尔顿（John Bolton）是鹰派人物，他对推翻伊朗政权直言不讳。但川普仍决定取消军事打击，“所以，我认为这表明了（川普）权衡利弊、保持平衡和富有同情心的特点。”
</p>
<h4>
 川普的实际行为 打破媒体不实报导
</h4>
<p>
 6月24日川普对伊朗实施了新的强硬制裁，目标是伊朗的最高领导人哈梅内伊和伊斯兰革命卫队（IRGC）的8名“高级指挥官”，对他们进行经济堵截。川普在白宫签署这一行政命令时告诉媒体，伊朗发动这一系列“侵略行为”非常“不合适”。
</p>
<p>
 梅斯特表示，西方主流媒体对川普进行了很多不实报导，“我认为这（伊朗事件）显示了，以前人们错误理解了川普的个人能力和思想方式。”
</p>
<p>
 梅斯特说，川普明白，冤家宜解不宜结，即国家之间的关系紧张升级容易，而恢复关系更难。他说，川普知道美国对伊朗的制裁非常有效，并且已经“把他们逼到了绝望的地步”。
</p>
<p>
 梅斯特还表示，媒体不负责任地对川普进行不准确报导，但他认为川普的实际行动是最好的证明，会争取到一些中间选民，“他正在达成好的协议，失业率创下历史新低，股市创下历史新高，其中一些中间选民会发现，他并非如媒体说的那样‘精神错乱’，他是审慎的、具有人道主义精神的和理性节制的。”
</p>
<h4>
 “一位非凡的谈判者”
</h4>
<p>
 梅斯特说，川普是一位“非凡的谈判者”，他知道如何“发挥谈判的杠杆和压力”。在川普还是一位商业大亨的时候，他们两人之前曾有很多次合作。
</p>
<p>
 “我与他谈判过法律的案件，他是一位非常精明的成功的谈判代表”，梅斯特解释道，“他在纽约市兴建高楼，与各行各业的人进行谈判——这就是他的年轻时代，他就是在这种商业生涯中磨练出来的。”
</p>
<p>
 梅斯特说，美国与墨西哥达成的关税协议就是典型的“川普式的谈判”风格，“他坚持要求墨西哥提供帮助，通过运用权力来实现谈判目标，就我而言，这是一个巨大的成功。”
</p>
<p>
 6月21日，墨西哥外交部长马塞洛‧埃布拉德（Marcelo Ebrard）表示，墨西哥将在未来几周与其它19个国家举行会晤，协商共同阻止非法移民进入美国。墨西哥的做法是为了避免川普用关税惩罚该国。
</p>
<h4>
 <span href="http://www.epochtimes.com/gb/tag/%E7%A1%AC%E6%B1%89.html">
  硬汉
 </span>
 外表 温柔内心
</h4>
<p>
 梅斯特说，虽然川普有着强硬的外表，但你一旦了解他，会发现他是一位传统的、守护家庭价值观的男人。
</p>
<p>
 “我认为他是一个典型的纽约商人——他有时会连讽带刺，他是一个
 <span href="http://www.epochtimes.com/gb/tag/%E7%A1%AC%E6%B1%89.html">
  硬汉
 </span>
 、一个强硬的谈判者，外表强悍。但如果你理解这个男人，就会知道他内心深处是一个‘家庭男’，他爱他的孩子和家人。”
</p>
<p>
 “你看，他取消对伊朗的军事打击，不愿看到生命死亡，即使那是敌人的生命”，梅斯特继续说， “他是退伍军人的坚定支持者。他对那些为我们国家冒险的人深表敬意。”
</p>
<p>
 梅斯特还说，川普的哥哥因酗酒去世，这对川普的生活产生了重大影响。“川普从不喝酒、不吸烟，他有一种令人难以置信的职业道德，他工作时间疯狂工作，你可以看到在这届总统任期内，他永远不会停止工作。”“而且他不会先发制人，但当他受到攻击时他会反攻，我们已经看到了。”#
</p>
<p>
 责任编辑：林琮文
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11366629.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11366629.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11366629.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/5/n11366629.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

