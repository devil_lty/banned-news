### 英独立法庭证实中共活摘器官 韩媒争相报导
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/06/ea959f75c0523986129baea958cab5cb-600x400.jpg"/>
<div class="red16 caption">
 <p>
  由大律师尼斯爵士担任主席的“人民法庭”6月17日宣判。（冠奇/大纪元）
 </p>
</div>
<hr/><p>
 【大纪元2019年06月26日讯】（大纪元记者李明儒韩国报导）英国“独立人民法庭”于6月17日（当地时间）在伦敦宣布，中共政府对大规模强制摘取以
 <span href="http://www.epochtimes.com/gb/tag/%E6%B3%95%E8%BD%AE%E5%8A%9F.html">
  法轮功
 </span>
 学员为主的良心犯器官，犯下了危害人类罪及酷刑罪。消息一出，立即引发世界各国媒体关注，韩国多家媒体也纷纷以“中国（中共）长期摘取被关押者器官，每年进行9万场移植手术”为主题予以报导。
</p>
<h4>
 韩国9家大型媒体同时报导
</h4>
<p>
 6月18日~19日，韩国的韩联社、东亚日报、亚洲经济、MBC、News1、SBS、世界日报等9家大型新闻媒体不约而同地报导了此次震惊世界的判决结果。
</p>
<p>
 多家韩国媒体表示 ，独立法庭由来自多个国家的人权专家、移植手术医护人员和国际关系领域的专家组成，经过多方取证、听证、审查了大量的视频和文本证据并调查后得出结论：在中国，
 <span href="http://www.epochtimes.com/gb/tag/%E6%B3%95%E8%BD%AE%E5%8A%9F.html">
  法轮功
 </span>
 修炼者等被关押的良心犯长期被非法摘取器官。
</p>
<p>
 报导重点引述法庭上的证词指，中国每年最多进行9万例器官移植手术，远高于中共政府公布的官方数据。
</p>
<p>
 韩国《亚洲经济》则以“中共强摘器官的来源 ——法轮功是什么”为题，详细介绍了法轮功受到中共镇压的过程：法轮功是1992年在吉林长春传出的身心修炼功法，还曾因对国民健康有益受到表彰，但修炼人数超过1亿人后，中共以其可能威胁体制为由进行镇压。2006年，中共强摘法轮功学员器官的行为首次被公诸于世。
</p>
<h4>
 从“不敢相信”到“不得不信”
</h4>
<p>
 虽然加拿大前内阁部长大卫‧乔高和人权律师大卫‧麦塔斯曾出版调查报告《血腥的
 <span href="http://www.epochtimes.com/gb/tag/%E6%B4%BB%E6%91%98%E5%99%A8%E5%AE%98.html">
  活摘器官
 </span>
 》披露过这一事实，但为了攫取器官而杀人这样的事情，太惨无人道，许多人不敢相信。
</p>
<p>
 英国大法官杰弗里‧尼斯爵士担任“人民法庭” 的主席，他郑重公布了宣判结果：“法庭成员一致确信，毋庸置疑，中国（中共）强制从良心囚犯身上摘取器官，涉案时间很长，涉及受害者众多。”
</p>
<p>
 来自阿姆斯特丹的欧洲问题专家Tromp博士在法庭上表示：“这次法庭经过独立调查后，拥有非常详实的证据。这些证据明确证实，受到
 <span href="http://www.epochtimes.com/gb/tag/%E6%B4%BB%E6%91%98%E5%99%A8%E5%AE%98.html">
  活摘器官
 </span>
 罪行迫害的对象属于一个团体，这个团体叫法轮功。”
</p>
<p>
 裁判过程中，曾在女性劳教所里被监禁一年的法轮功学员、作家曾铮出庭作证，自己在被关押期间，曾多次被要求接受体检和验血。在2001年逃出中国后，得知中共活摘器官的罪行时，曾铮才察觉，当初的身体检查很可能是活摘器官的医疗筛选程序。
</p>
<p>
 2014年中共称将停止从死刑犯身上摘取器官，间接承认此前一直摘取死刑犯器官，但否认活摘器官的罪行，还推出了“器官自愿捐赠计划”的政策。而人民法庭的判决书驳斥了该说辞，判决书称：“累积的数字证据（不包括虚假的中国数据）表明：与实际执行的移植手术数量相比，自愿捐赠计划’不可能提供足够的‘合格捐赠者’。”#
</p>
<p>
 责任编辑：叶紫微
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11346364.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11346364.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11346364.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/6/26/n11346364.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

