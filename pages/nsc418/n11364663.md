### 对峙升级 英外长：不排除驱逐中共外交官
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/GettyImages-1153265805-600x400.jpg"/>
<div class="red16 caption">
 <p>
  7月2日，英国外交大臣亨特严厉警告中共，如果违反保护香港自由的《中英联合声明》，将会产生“严重后果”。(Peter Morrison – WPA Pool/Getty Images)
 </p>
</div>
<hr/><p>
 【大纪元2019年07月04日讯】（大纪元记者徐简综合报导）英国和北京之间关于香港问题的“口水战”再次升级。周四（7月4日），
 <span href="http://www.epochtimes.com/gb/tag/%E8%8B%B1%E5%9B%BD%E5%A4%96%E4%BA%A4%E5%A4%A7%E8%87%A3.html">
  英国外交大臣
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E4%BA%A8%E7%89%B9.html">
  亨特
 </span>
 （Jeremy Hunt，侯俊伟）在接受采访时表示，英国不排除对中共实施制裁、驱逐其外交官的可能性。
</p>
<p>
 自从香港人民发起反对“引渡条例”（又称送中条例、逃犯条例）修订的运动后，英国不断就香港问题对中共发出指责，而且态度愈来愈强硬。
</p>
<p>
 英国首相特蕾莎‧梅在7月3日的讲话中表示，她在最近的G20会议上，直接向习近平提出了她对香港问题的担忧。“至关重要的是，香港的高度自治以及
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E8%8B%B1%E8%81%94%E5%90%88%E5%A3%B0%E6%98%8E.html">
  中英联合声明
 </span>
 中规定的权利和自由，必须得到尊重。”她告诉国会议员。
</p>
<h4>
 “不排除制裁中共、驱逐外交官”
</h4>
<p>
 <span href="http://www.epochtimes.com/gb/tag/%E8%8B%B1%E5%9B%BD%E5%A4%96%E4%BA%A4%E5%A4%A7%E8%87%A3.html">
  英国外交大臣
 </span>
 <span href="http://www.epochtimes.com/gb/tag/%E4%BA%A8%E7%89%B9.html">
  亨特
 </span>
 多次公开批评中共，并敦促中共不要将抗议活动作为“镇压借口”。周四，亨特重复了他对香港问题的担忧，并坚持认为，英国对付中共方式是多样性的，当被记者问及他是否计划对中共实施制裁，或驱逐中共外交官时，亨特表示，不会排除这种可能性。
</p>
<p>
 “镇压不是平息暴力的方法，而是要了解示威者为何这样做，因为他们一生所拥有的自由可能会被新的引渡法破坏。”他告诉BBC广播四台（Radio 4）的Today节目。
</p>
<p>
 周三（7月3日），中共驻英国大使刘晓明在新闻发布会上称英国干涉“中国内政”，还说英国似乎已经忘记了香港不再是殖民地，应该放弃香港，并指责亨特的言行“危害两国关系”。刘的言论激怒了英国政府，英国外交部当天传召刘晓明，要求他解释对香港问题的言论。
</p>
<p>
 7月2日，亨特严厉警告中共，如果违反保护香港自由的《
 <span href="http://www.epochtimes.com/gb/tag/%E4%B8%AD%E8%8B%B1%E8%81%94%E5%90%88%E5%A3%B0%E6%98%8E.html">
  中英联合声明
 </span>
 》，将会产生“严重后果”。他还说英国“坚定支持”香港人民。自从香港1997年从英国手中主权移交给中国后，英国极少使用如此强硬的语气。
</p>
<h4>
 “民主价值观应置于经济利益之上”
</h4>
<p>
 本周，中共外交部发言人称，英方支持香港的言论是“信口雌黄”，口吻近乎谩骂，而中共外交部驻香港公署负责人更把英国的支持行为解释成“公认美化暴力犯罪行为，包庇纵容暴力犯罪分子”。驻英大使刘晓明也多次公开表示《中英联合声明》已经过期无效，成为历史文件。
</p>
<p>
 亨特说，他对北京的反应并不感到惊讶，他说：“中国（中共）就擅长这种腔调。”他补充说，“中共从国际规则体系中受益匪浅。因此，如果中共不尊重英中之间这一非常重要的协议，当然会对两国（关系）产生影响。”
</p>
<p>
 亨特认为，香港的情况“非常非常严重”。他补充说：“我们是一个支持民主、法治、世界公民权利的国家。我们认为（香港）情况非常令人担忧。而且我们只是要求，我们与中国1984年达成的协议得到尊重。”
</p>
<p>
 当被问及英国是否跟中共对立，是否会导致太多的经济利益受损，亨特表示一个国家的价值观更为重要：“如果你问我贸易关系和我们民主原则之间的权衡，归根究柢，我们一直捍卫我们所信奉的价值观，我们认为这是一项非常重要的原则，即国际协议得到尊重。”#
</p>
<p>
 责任编辑：林妍
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11364663.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11364663.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11364663.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/4/n11364663.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

