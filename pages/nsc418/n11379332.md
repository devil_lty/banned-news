### 金正恩在悼父大会上犯困 金与正疑升至核心层
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<img alt="金正恩在出席前领导人金日成的追悼会上，打起了瞌睡。（朝鲜中央电视台截图）" class="aligncenter wp-post-image" src="http://i.epochtimes.com/assets/uploads/2019/07/4228033-600x400.jpg"/>
<div class="red16 caption">
 金正恩在出席前领导人金日成去世25周年的悼念会上，打起了瞌睡。（朝鲜中央电视台截图）
</div>
<hr/><p>
 【大纪元2019年07月12日讯】朝鲜领导人
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%91%E6%AD%A3%E6%81%A9.html">
  金正恩
 </span>
 的
 <span href="http://www.epochtimes.com/gb/tag/%E8%BA%AB%E4%BD%93%E7%8A%B6%E5%86%B5.html">
  身体状况
 </span>
 一直备受外界关注。继日前在同美国总统川普（特朗普）会面时呼吸困难上了屏幕后，近日金正恩在出席前朝鲜领导人金日成去世25周年的悼念会上，又打起了瞌睡。
</p>
<p>
 7月8日是朝鲜前领导人金日成去世25周年，平壤当局举行悼念大会。
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%91%E6%AD%A3%E6%81%A9.html">
  金正恩
 </span>
 和政治局委员阶级以上的朝鲜高层出席，金正恩坐在主席台第一排的中央位置，听着歌颂金日成的追悼词。
</p>
<p>
 Ettoday网站报导，“朝鲜中央电视台”的画面却显示，面对大批群众的金正恩眼皮沉重，一度缓缓地闭上双眼，接着还做了几次深呼吸。而当讲者激昂地发表悼词时，金正恩的头几度转向两侧，甚至几乎垂到肩膀上。
</p>
<p>
 另外，他还打破过去朝鲜领导人的惯例，成为全场最后一个开始鼓掌、又第一个停下双手的人。
</p>
<p>
 据早前媒体报导，此前有脱北者指出，若是有人在不该睡的时候打瞌睡，或是在重大活动中鼓掌不够用力，就会被视为不服从金正恩，甚至被处以死刑。
 <span class="Apple-converted-space">
 </span>
</p>
<p>
 金正恩的
 <span href="http://www.epochtimes.com/gb/tag/%E8%BA%AB%E4%BD%93%E7%8A%B6%E5%86%B5.html">
  身体状况
 </span>
 一直是外界的关注点。6月30日，美国总统川普访问韩国，在板门店同金正恩会面。并在众目睽睽下迈入朝鲜领土，吸引了国际关注。当时金正恩的身体状况也引发关注。
</p>
<p>
 当时的录像显示，金正恩喘息有些困难，上气不接下气，不清楚是因为紧张还是身体状况不佳。现场的美国
 <span class="s1">
  福克斯资深主持人塔克·卡尔森则形容，
 </span>
 <span class="s1">
  金正恩看起来身体不
  <span href="http://www.epochtimes.com/gb/tag/%E5%81%A5%E5%BA%B7.html">
   健康
  </span>
  ，呼吸沉重，好像一个迅速恶化的
 </span>
 <span class="s2">
  “
 </span>
 <span class="s1">
  肺气肿
 </span>
 <span class="s2">
  ”
 </span>
 <span class="s1">
  病人。
 </span>
</p>
<p>
 此前有韩国媒体报导称，因为美朝河内峰会（川金二会）搁浅后，金正恩情绪低落，
 <span href="http://www.epochtimes.com/gb/tag/%E5%81%A5%E5%BA%B7.html">
  健康
 </span>
 甚至出现转差迹象。韩国国家情报院曾于2016年7月发布一份报告称，金正恩的体重已达130公斤，并因严重的压力导致失眠、暴饮暴食，因此很有可能患有糖尿病、高血压、痛风、心脏病等疾病。
</p>
<h4>
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%91%E4%B8%8E%E6%AD%A3.html">
  金与正
 </span>
 疑升核心领导层
</h4>
<p>
 这次追悼会的另一个关注点是金正恩的胞妹
 <span href="http://www.epochtimes.com/gb/tag/%E9%87%91%E4%B8%8E%E6%AD%A3.html">
  金与正
 </span>
 ，她被安排坐在距金正恩四个位子的地方，这让外界猜测，金与正已经升至核心领导阶层。早前在川金二会破局后，一度传出金与正失势的传闻。
</p>
<p>
 据韩国“韩联社”分析认为，主席台的座位，是依照权力排名的，而分坐在金正恩左右，也就意味着金与正是排名第9。
</p>
<p>
 南韩世宗研究机构研究员分析说：“当金与正和劳动党中央委员会副主席，及这些政治家并肩而坐，很有可能她的位阶已经从一位常委候选人，变成政治局里的常委一员了。”
 <span class="Apple-converted-space">
 </span>
</p>
<p>
 韩国《朝鲜日报》7月10日报导，韩国共同民主党正在推进访朝计划，拟邀请朝鲜最高领导人金正恩胞妹、朝鲜劳动党宣传鼓动部第一副部长金与正参加即将于8月举行的韩国前总统金大中逝世十周年纪念活动。疑似与金与正在朝鲜的政治地位上升有关。
 <span class="Apple-converted-space">
  #
 </span>
</p>
<p>
 责任编辑：林诗远
</p>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11379332.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11379332.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/nsc418/n11379332.md.png'/></a> <br/>
原文地址（需翻墙访问）：http://www.epochtimes.com/gb/19/7/11/n11379332.htm


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

