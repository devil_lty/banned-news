### 美国庆庆典展示创新与军力 民众称赞川普爱国
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/8ddd74570db0745fde8057aac9680f04.jpg" target="_blank">
  <figure>
   <img alt="美国庆庆典展示创新与军力 民众称赞川普爱国" src="https://i.ntdtv.com/assets/uploads/2019/07/8ddd74570db0745fde8057aac9680f04-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  7月4日，川普总统及第一夫人梅拉尼娅（Melania Trump）出席美国建国243周年国庆庆典。（Tasos Katopodis/Getty Images）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月05日讯】周四（7月4日），川普总统参加美国建国243周年国庆
  <span href="https://www.ntdtv.com/gb/庆典.htm">
   庆典
  </span>
  ，发表令人激动的演讲，并向全世界展示了美国的创新能力和军事实力，受到美国
  <span href="https://www.ntdtv.com/gb/爱国.htm">
   爱国
  </span>
  民众的普遍好评。
 </p>
 <p>
  周四，川普如愿以偿，出席了其上任以来首次大型的独立日
  <span href="https://www.ntdtv.com/gb/庆典.htm">
   庆典
  </span>
  及
  <span href="https://www.ntdtv.com/gb/阅兵.htm">
   阅兵
  </span>
  ，在国家广场（National Mall）发表了长达45分钟的演说，激发了全民的
  <span href="https://www.ntdtv.com/gb/爱国.htm">
   爱国
  </span>
  热情。
 </p>
 <p>
  川普在题为“向美国致敬”（Salute to America）的演说中，赞扬了美国在科学、医学和太空探索等方面的创新。
 </p>
 <p>
  川普讲述了一些来自美国革命和内战、以及妇女参政和民权运动的故事，并赞扬了“鼓舞我们的创始人的美国精神”和“贯穿每个美国爱国者的血脉”。
 </p>
 <p>
  川普指出，今天的美国比以往任何时候都强大，是最强的时候。
 </p>
 <p>
  川普还提到，本月晚些时候将迎来阿波罗11号（Apollo 11）登月50周年。他还直接向出席庆典的退役美国航空航天局（NASA）飞行总监克兰兹（Gene Kranz）致意。
 </p>
 <p>
  川普告诉克兰兹，美国将重返月球，而且不久后的某一天会将美国国旗插到火星上。
 </p>
 <p>
  川普还介绍了传奇的肿瘤科医生埃米尔•弗雷瑞奇（Emil Freireich）博士，他为儿童白血病患者开发了一种有效的治疗方法。
 </p>
 <p>
  川普向全国的金星家庭致敬，他们的儿女为美国做出了最大牺牲。
 </p>
 <p>
  在川普的安排下，今年的庆祝活动包含简单的
  <span href="https://www.ntdtv.com/gb/阅兵.htm">
   阅兵
  </span>
  式，并展示了美军的最新
  <span href="https://www.ntdtv.com/gb/武器.htm">
   武器
  </span>
  ，包括M1A2艾布拉姆斯坦克（M1A2 Abrams tanks）、布雷德利步兵战车（Bradley Fighting Vehicles）、M88装甲救济车（Armored Recovery Vehicles），以及先进的F-22猛禽隐形战斗机（F-22 Raptors）、B-2幽灵隐形轰炸机（B-2 Spirit bomber）等。
 </p>
 <p>
  另外，海军陆战队的2架新型运输机——鱼鹰式倾斜旋翼机（MV-22 Ospreys），以及尚未服役的陆战队版未来总统专机“陆战队一号”（Marine One）也飞过主席台亮相。
 </p>
 <p>
  今年美国独立日，川普终于实现了其上任以来的首次国内阅兵。盛大的国庆庆典吸引了密集的民众，尽管当天有雨，但挡不住他们的爱国热情。
 </p>
 <p>
  虽然川普的反对者连续几天来着力批评庆典的花销，但川普周三在推特上表示，这与庆典本身的价值相比不算什么。
 </p>
 <p>
  曾投票支持川普的前民主党人安德森夫妇——阿里克和佩吉（Aric and Page Anderson）都表示，他们从马里兰州的安纳波利斯（Annapolis）赶来，参加这一“历史性”活动。
 </p>
 <p>
  65岁的阿里克表示，他认为，他目前在爱国方面对川普的支持更多。
 </p>
 <p>
  （记者李佳欣综合报导）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102616176.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102616176.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102616176.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/05/a102616176.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

