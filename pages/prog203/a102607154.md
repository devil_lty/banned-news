### 【华府冲击播】专访Steven Mosher：美中贸易战与香港反送中
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月23日讯】华府“当前危险委员会”成员、中国问题专家毛思迪，近日接受《英文大纪元》专访。谈到香港“
  <span href="https://www.ntdtv.com/gb/反送中.htm">
   反送中
  </span>
  ”以及美中贸易谈判，毛思迪认为，美国将采取多种措施，促成中共解体。他表示，三分之一港人上街“反送中”，体现出港人反抗中共对香港自由的侵蚀。
 </p>
 <p>
  中国问题专家 毛思迪：“香港民众比任何人都清楚，边界线之外是一个腐败的共产主义政权，以及司法腐败。这个《引渡条例》将会让每个香港人置于危险，（可能）被送往中国进行表演型审判，不公平的法庭审判（袋鼠审判）。”
 </p>
 <p>
  而港府和警方对示威的暴力镇压，使港人对未来警醒。
 </p>
 <p>
  中国问题专家 毛思迪：“我认为这是我们第一次看见，香港警方以这种暴力行径回应。过去他们总是更能控制自己。我认为香港人从警方这种回应中，也预测到未来如果他们直接受北京控制，他们（港人）可能要面对什么。”
 </p>
 <p>
  毛思迪说，如果《逃犯条例》通过，香港不仅自由失守，经济繁荣也不复存在。
 </p>
 <p>
  中国问题专家 毛思迪：“如果香港陷入中共法律的统治，或者陷入缺失的中国法治统治下，（香港）房价会直线下降。香港将不再是世界领先的金融中心。因为在中国共产党暴政下，货币和财产将不再安全。”
 </p>
 <p>
  毛思迪提到，美中贸易谈判将震撼中国的经济体制。
 </p>
 <p>
  中国问题专家毛思迪：“如果没有经济的出口产业，而中国民众还需要承受来自国有企业，以及中共的负担。我认为，这将震撼中国（经济）体系。它（中共）可能无法再维持下去。”
 </p>
 <p>
  毛思迪还说，美方要求中共进行的结构性改革，使中共面临抉择。
 </p>
 <p>
  中国问题专家毛思迪：“任何一种方式，它们（中共）都会以自毁收场。或者是经济上，在法律或贸易问题上拒绝国际社会的要求；或者另一种方式，同意美国的要求，使自己（中共）走上失去权力的道路。”
 </p>
 <p>
  毛思迪认为，美中贸易协议必然包含执行机制，如果中共不签署协议，经济将进入萎缩。
 </p>
 <p>
  中国问题专家毛思迪：“我不认为川普总统，或（贸易代表）莱特希泽，或（白宫贸易顾问）纳瓦罗或任何人，会同意签署不包含执行机制、以及惩罚措施的协议。如果他们（中共）不签署贸易协议，经济出口行业，整个经济将开始萎缩，出现赤字。”
 </p>
 <p>
  为了应对中共，今年3月，华盛顿重启了“当前危险委员会”。
 </p>
 <p>
  毛思迪是委员会成员之一。他介绍说，“当前危险委员会”创立于上个世纪50年代，促成了后来苏联解体。他希望这一次能促成中共解体。
 </p>
 <p>
  中国问题专家毛思迪：“我们想看到同样事情在中国发生，所以我们创立了“当前危险委员会：中国”。我们将呈交给政府和国会，鼓励他们采取更强硬措施，促成中共解体。”
 </p>
 <p>
  毛思迪介绍说，美国政府可以采取当年打击苏联的模式，包括阻断资本，阻断科技，利用强大的军事力量等措施，打击中共，帮助中国实现民主。
 </p>
 <p>
  新唐人电视台综合报导
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102607154.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102607154.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102607154.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/22/a102607154.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

