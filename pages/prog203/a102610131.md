### 川普指控社交媒体企图操纵大选 警告或予起诉
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/Untitled-53.jpg" target="_blank">
  <figure>
   <img alt="川普指控社交媒体企图操纵大选 警告或予起诉" src="https://i.ntdtv.com/assets/uploads/2019/06/Untitled-53-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  川普总统6月26日表示，推特等社交媒体巨头企图操纵2020年大选，他可能会提起诉讼。（LOIC VENANCE/AFP/Getty Images）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月27日讯】周三（6月26日），
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  总统在接受福克斯商业新闻（Fox Business）采访时，指控
  <span href="https://www.ntdtv.com/gb/脸书.htm">
   脸书
  </span>
  （Facebook）等
  <span href="https://www.ntdtv.com/gb/社交媒体.htm">
   社交媒体
  </span>
  巨头持有反保守派偏见，并企图利用各自平台操纵2020年大选。他表示或为纠正这一问题而提起诉讼。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  告诉福克斯主持人玛丽亚‧巴蒂罗姆（Maria Bartiromo），他可能会针对
  <span href="https://www.ntdtv.com/gb/脸书.htm">
   脸书
  </span>
  、
  <span href="https://www.ntdtv.com/gb/谷歌.htm">
   谷歌
  </span>
  （Google）、推特（Twitter）等科技公司审查保守派言论的行为提起诉讼。
 </p>
 <p>
  川普特别谴责了推特，称该
  <span href="https://www.ntdtv.com/gb/社交媒体.htm">
   社交媒体
  </span>
  非常糟糕，他们“让人们很难加入我的推特，他们让我更难以传达信息”。
 </p>
 <p>
  川普指出，这都是民主党人干的，他们的工作完全偏向民主党一方。“如果我明天宣布我将成为一名很好的自由派民主党人，我会接受5倍以上的跟贴者”。
 </p>
 <p>
  巴蒂罗姆提到，
  <span href="https://www.ntdtv.com/gb/谷歌.htm">
   谷歌
  </span>
  公司的某高管说他们不想让2016年发生的一切再发生于2020年，川普对此回应说，“让我告诉你，他们正试图
  <span href="https://www.ntdtv.com/gb/操纵选举.htm">
   操纵选举
  </span>
  ”。
 </p>
 <p>
  川普指出，这才是人们应该关注的，而不是虚假的“猎巫”（Witch Hunt）。川普指的是特别检察官穆勒（Robert Mueller）所领导的通俄门调查。
 </p>
 <p>
  川普总统的次子艾瑞克•川普（Eric Trump）周二在推特上建议人们看看英媒《每日邮报》（Daily Mail）的一篇报导，并看看其中的视频。这段卧底视频显示，谷歌的一名高管誓言阻止川普再次当选总统。
 </p>
</div>
<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102610131.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102610131.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102610131.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/27/a102610131.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

