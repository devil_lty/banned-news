### “川习会”前一刻 中国向美购买54万吨大豆
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/2018-08-08-5b6a838828c83.jpg" target="_blank">
  <figure>
   <img alt="“川习会”前一刻 中国向美购买54万吨大豆" src="https://i.ntdtv.com/assets/uploads/2019/06/2018-08-08-5b6a838828c83-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  “川习会”的前一刻，美国农业部透露，中方向美国购买大量大豆。（视频截图）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月29日讯】G20“
  <span href="https://www.ntdtv.com/gb/川习会.htm">
   川习会
  </span>
  ”前一刻。美国农业部透露，中方将向美国购买超过54万吨大豆，这是自去年3月以来中方购入的最大数量。舆论对此感到惊讶，相信是中方在“川习会”前向美国示好让步。
 </p>
 <p>
  日本时间29日上午，
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  （特朗普）总统与
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  会面。此前一刻，美国农业部透露，中方向美国购买大量大豆。根据农业部的声明，被购买的大豆超过54万吨，是自去年3月以来中国购入的最大数量。声明强调，这是一宗新交易。
 </p>
 <p>
  有市场人士对此感到惊讶，相信是中方希望在“
  <span href="https://www.ntdtv.com/gb/川习会.htm">
   川习会
  </span>
  ”前向美国示好之举。
 </p>
 <p>
  美国财经频道CNBC引述资深华府官员说，美方也希望透过“川习会”缓和紧张的中美关系，但如果
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  拒绝将执行机制写入贸易协议或中共法律，贸易战将难免持续。
 </p>
 <p>
  美国总统
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  6月26日启程前往
  <span href="https://www.ntdtv.com/gb/g20峰会.htm">
   G20峰会
  </span>
  的几小时，接受福克斯电视台访问时，他再次重申，假如他同习近平达不成贸易协议，将对余下3000亿美元中国商品加征额外关税。
 </p>
 <p>
  5月初，
  <span href="https://www.ntdtv.com/gb/美中贸易战.htm">
   美中贸易战
  </span>
  再次升级后， 美国指责中方在谈判中出尔反尔，川普一怒之下对价值逾2,000亿美元的中国商品加征进口关税，并将对余下的3000亿美元中国商品加征额外关税，以迫使北京当局保护知识产权和对贸易制度进行其他改革。
 </p>
 <p>
  川普说：如今美国的“企业纷纷迁离中国，其中有些回到美国，因为他们不想支付关税。”“他们希望达成协议。他们比我们更希望达成协议。”
 </p>
 <p>
  29日中午11点半，川普和习近平就两国间的贸易争端进行磋商。不过，外界普遍不看好这次川习会，从目前的迹象来看，中方并没有想要达成协议的诚意。
 </p>
 <p>
  《华尔街日报》引述美国官员的话称，美方参加这次会晤，只是想看看中方是否还愿意恢复谈判。
 </p>
 <p>
  美前国安官员预计，川普将继续把中共作为美国的战略竞争对手对待。
 </p>
 <p>
  美国智库哈德逊研究所研究员、美国白宫前副国安顾问薛德丽（Nadia Schadlow）28日在福克斯电视台网站发表评论文章说，美国对华战略从扶持向竞争的转变，是对中国共产党多年努力改造国际体系作出的回应。”
 </p>
 <p>
  作为川普政府首份“国家安全战略”报告的主笔人，薛德丽表示，中共一直伺机推进专制制度，同时使他国在贸易上处于不利地位。她说，川普在对华政策上设定了双重目标：重塑经济关系、不让对手占美国的便宜，同时平衡寻求自由开放的国家力量。
 </p>
 <p>
  为达成这两个目标，川普政府已经着手4个方面的具体战略：包括保持美国的技术优势、要求贸易对等（reciprocity）、加强威慑以及恢复区域力量平衡。
 </p>
 <p>
  薛德丽认为，川普策略并不是一成不变的，随着环境的变化，也会随之改变。比如：川普总统曾表示，如果中方做出有意义的改变，他愿意与习主席保持沟通渠道，并与他合作。
 </p>
 <p>
  而这次
  <span href="https://www.ntdtv.com/gb/g20峰会.htm">
   G20峰会
  </span>
  不仅是重启贸易谈判的机会，也是一个机会让中方注意到现在是时候停止破坏国际体系的行为了。
 </p>
 <p>
  （记者李韵报导/责任编辑：戴明）
 </p>
 <p>
  <strong>
   相关链接：
   <span href="https://cn.ntdtv.com/gb/2019/06/28/a102611114.html" rel="noopener" target="_blank">
    G20习近平单挑全场？ 一张照片引热议
   </span>
   <br/>
   <strong>
    相关链接：
    <span href="https://cn.ntdtv.com/gb/2019/06/22/a102606722.html" rel="noopener" target="_blank">
     习近平承诺大力援朝 川普立即对朝采取行动
    </span>
    <br/>
    <strong>
     相关链接：
     <span href="https://cn.ntdtv.com/gb/2019/06/27/a102610155.html" rel="noopener" target="_blank">
      日媒：北京提强烈要求 维护习近平面子
     </span>
     <br/>
     相关链接：
     <span href="https://cn.ntdtv.com/gb/2019/06/28/a102611037.html" rel="noopener" target="_blank">
      G20川习会 党媒扔“手榴弹” 炸翻网络
     </span>
     <br/>
    </strong>
    <br/>
   </strong>
  </strong>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102611547.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102611547.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102611547.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/29/a102611547.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

