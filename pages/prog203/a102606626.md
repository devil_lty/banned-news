### 川习会热点话题：习近平和川普的手机谁的贵？
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-871895.jpg" target="_blank">
  <figure>
   <img alt="川习会热点话题：习近平和川普的手机谁的贵？" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-871895-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  21日，有法媒刊文说，川普和习近平的手机都非常贵，两人会见时肯定会谈及两国手机巨头华为和苹果的话题。（FRED DUFOUR/AFP/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月22日讯】美中两国领导人将于6月底G20峰会期间举行会晤。然而两国领导人谁的手机更贵成为热点话题。日前，有法媒刊文说，
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  和
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  的手机都非常贵，两人会见时肯定会谈及两国手机巨头华为和苹果的话题。
 </p>
 <p>
  《法国世界报》6月21日在其经济版面上刊发一篇标题为“
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  和
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  的手机非常贵”文章。文章说，川普和习近平会和其他人一样，当他们见面时，会像其他人一样喜欢比较各自智能手机的优点。
 </p>
 <p>
  6月28日，习近平和川普将在日本大阪举行的G20峰会上举行会谈。文章认为，两人会见时谈论的话题，肯定会涉及中美移动电话巨头华为和苹果。
 </p>
 <p>
  文章说，两人的手机非常昂贵。习近平的“华为手机”价值1050亿美元，这是全球拥有近20万名员工的华为公司的营业额。如今，华为的主宰地位正受到美国禁令的威胁。
 </p>
 <p>
  至于川普的“iPhone手机”也很值钱，这一点儿可能川普已经忘记了，为了唤醒川普的记忆，苹果老板库克给川普发了一封信，提醒川普，苹果员工有200万人，在接下来的5年里，苹果致力于为美国经济贡献3500亿美元。
 </p>
 <p>
  iPhone手机是欧洲、日本、韩国、美国设备的集成体，在中国组装，然后直接发送到销售点。当然，苹果也可以在全球范围内重新布局它的生产。
 </p>
 <p>
  由于中美移动电话巨头华为和苹果的发展前景，牵扯到两国的经济，文章认为，川普和习近平之间的有关手机的谈话，肯定会是特别的激烈。
 </p>
 <p>
  <strong>
   华为遭双杀禁令后祸事连连
  </strong>
 </p>
 <p>
  川普于5月15日签署行政令宣布敌手国对美国信息和通信技术和服务供应链的威胁，属于“国家紧急状态”。次日，美国商务部宣布，将华为与其68家子公司纳入出口管制“实体名单”。
 </p>
 <p>
  双杀禁令出台后，先是各大芯片商对华为断供，SD协会及Wi-Fi联盟先后同华为断绝联系，蓝牙技术联盟也对华为采取行动，网路商店下架华为产品，全球电信业者纷纷暂售华为手机，富士康已经将华为手机生产线停工等。
 </p>
 <p>
  专家分析，华为退出国际市场是早晚的事，而美方对华为的制裁，其实是对中共的制裁，以及对中共权贵的围堵。
 </p>
 <p>
  而SK海力士也接力延迟中国工厂量产时间，韩国最大网路监控摄影机制造商也加入抵制华为的行列。
 </p>
 <p>
  进入6月，华为要求美企支付巨额专利费用，美议员批评“华为已成专利流氓”，同时提案禁止华为索赔，华为勒索10亿美金亦泡汤。英国议员们则将华为与中共政府的合作，比作“纳粹合作者”。
 </p>
 <p>
  面对全球围堵，华为得到中共官方的力挺。中共领导人满世界为华为找“婆家”，尽管如此，6月上旬，华为总裁
  <span href="https://www.ntdtv.com/gb/任正非.htm">
   任正非
  </span>
  仍谈及华为公司存亡问题，坦承美国制裁给华为判了死刑，如今的华为已经被打得千疮百孔了。
 </p>
 <p>
  他还对华为提前留下“遗言”，称华为的覆灭与川普实施的制裁有关，他还对美媒记者说：“是川普把我3年以后看会怎么样，如果华为死了，请你带一束玫瑰花放在墓前。是川普把我们打死的，不是其他什么人。”
 </p>
 <p>
  最讽刺的是在中共政府要求抵制苹果，力撑华为之时，
  <span href="https://www.ntdtv.com/gb/任正非.htm">
   任正非
  </span>
  5月份公开承认，“我小孩就是不爱华为，因为他爱苹果。我们的家人现在还在用苹果手机，苹果的生态很好，家人出国我还送他们苹果电脑”。
 </p>
 <p>
  在听到任正非的这番话之后，“爱华为用苹果”的五毛和小粉红们纷纷傻眼了，这还不够，任正非甚至还公开训斥，“买华为不等于爱国”、“不要瞎喊口号煽动民族情绪”。不过，这番话被外界解读，任正非是想撇清协助中共搞间谍的指控。
 </p>
 <p>
  专家分析，四面楚歌的华为退出国际市场是早晚的事，而美国对华为的制裁，其实是对中共的制裁，以及对中共权贵的围堵。
 </p>
 <p>
  （记者李韵报导/责任编辑：戴明 ）
 </p>
 <p>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/27/a102587200.html" rel="noopener" target="_blank">
    王岐山一罕见举动 爆华为与中共的关系
   </span>
  </strong>
  <br/>
  <strong>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/05/28/a102587854.html" rel="noopener" target="_blank">
    华为库存已告急？向韩国求援内幕曝光
   </span>
   <br/>
   相关链接
   <span href="https://www.ntdtv.com/gb/2019/06/07/a102595587.html" rel="noopener" target="_blank">
    任正非留“遗言”：是川普把我们打死的
   </span>
  </strong>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102606626.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102606626.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102606626.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/22/a102606626.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

