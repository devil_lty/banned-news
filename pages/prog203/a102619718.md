### 对川普政府负面评价外泄 英驻美大使辞职
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月11日讯】因外交电报和备忘录
  <span href="https://www.ntdtv.com/gb/泄密.htm">
   泄密
  </span>
  而引发一场风波的故事主角，英国驻美大使金·达罗赫爵士，被英国外交部证实已经辞职。英国政坛普遍对泄密持批评态度，外交大臣亨特表示要追查到底。
 </p>
 <p>
  周三，英国驻美大使达罗赫爵士（Kim Darroch）决定辞职，他因为对
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  政府非常负面的评价被泄露，而成为连日来的焦点。
 </p>
 <p>
  7月7日，英国《每日邮报》刊文，曝光了达罗赫回传给英国政府的外交电报等文件，包含他对
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  政府的观点和分析。他认为川普政府“无能”且“一团糟”。
 </p>
 <p>
  川普当天即做出回应。
 </p>
 <p>
  美国总统川普：“我得说，英国（政府）和大使并没有很好地为英国服务。我可以告诉你，我们并不喜欢那个人，他没有为英国尽职。”
 </p>
 <p>
  在随后两天的推文中川普对达罗赫都表达了不满，表示将不再与他打交道。川普也对英国首相特雷莎·梅及其内阁对脱欧的处理方式表达了不满。
 </p>
 <p>
  对于达罗赫的辞职，特雷莎·梅表示遗憾。
 </p>
 <p>
  英国首相特雷莎·梅：“今天早上我和金·达罗赫爵士谈过。他觉得有必要离开他的大使职位，我告诉他这是一件非常遗憾的事。”
 </p>
 <p>
  正在竞争保守党党魁一职的外交大臣亨特，严重关切这次的
  <span href="https://www.ntdtv.com/gb/泄密.htm">
   泄密
  </span>
  事件，表示要追究到底。
 </p>
 <p>
  英国外交大臣亨特：“我希望我们要追究到底。在我们发现责任人后，显然要承担非常严重的后果。”
 </p>
 <p>
  事件发生后，美国左派媒体不断对美英战略伙伴关系提出质疑。不过英、美都做出了正面回应。
 </p>
 <p>
  英国外交大臣亨特：“我说过我认为美国政府非常有效率。我们的关系非常好，并且是基于共同的价值观。”
 </p>
 <p>
  美国务院发言人 Morgan Ortagus：“美国与英国长期以来拥有难以置信的特殊的战略伙伴关系，超出任何个人和政府。”
 </p>
 <p>
  美国国务院发言人奥特加斯（Morgan Ortagus）表示，因为英国正在进行新一任首相选举，对事件不做更多评价。
 </p>
 <p>
  英国另一位保守党党魁竞争者、前外交大臣约翰逊对达罗赫的辞职表示遗憾，认为需要追责泄密者。在周二晚的党魁竞选电视辩论上，约翰逊表示，美国是英国最重要的盟友。
 </p>
 <p>
  英国前外交大臣 约翰逊：“我和白宫有非常好的关系。我们的紧密友谊非常重要、与美国的紧密关系。他们是我们最重要的盟友。”
 </p>
 <p>
  达罗赫爵士2016初赴美就职。在《每日邮报》发表相关报道的记者奥克肖特（Isabel Oak)是脱欧忠实支持者，也是脱欧党党魁法拉奇的盟友。
 </p>
 <p>
  法拉奇周三发推文表示，达罗赫的辞职，“这是一个正确的决定”。
 </p>
 <p>
  新唐人记者李兰综合报导
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102619718.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102619718.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102619718.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/10/a102619718.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

