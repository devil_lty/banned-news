### 启动2020竞选连任 川普：为人民而战
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月20日讯】观众朋友大家好，欢迎收看新唐人全球新闻。周二，
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  总统在
  <span href="https://www.ntdtv.com/gb/佛州奥兰多.htm">
   佛州奥兰多
  </span>
  正式宣布2020年
  <span href="https://www.ntdtv.com/gb/竞选连任.htm">
   竞选连任
  </span>
  ，并确定新的选战口号是：让美国继续伟大。
 </p>
 <p>
  周二晚，
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  在
  <span href="https://www.ntdtv.com/gb/佛州奥兰多.htm">
   佛州奥兰多
  </span>
  举办了竞选造势大会，正式宣布在2020年大选中再次竞逐总统宝座。
 </p>
 <p>
  美国总统 川普：“我面对你们正式宣布：启动我美国总统第二任期的竞选活动。”
 </p>
 <p>
  在集会上，川普让大家通过欢呼声，来选择选战口号：是“让美国再次伟大”，还是“让美国继续伟大”。
 </p>
 <p>
  虽然川普表示自己仍然偏向“让美国再次伟大”，不过，支持者们以更大的欢呼声选择了后者。
 </p>
 <p>
  美国总统 川普：“你们准备好了吗？让美国继续伟大！”
 </p>
 <p>
  川普在演讲中提到延续两年半的通俄门调查，他指出那场恶作剧实质是在针对美国和美国人。
 </p>
 <p>
  美国总统 川普：“民主党人不在乎俄罗斯，只在意他们的政治权力。他们调查我的家人、我的生意、我的资产、我的员工和几乎每一个和我工作过的人，但实质上这些人针对的是你们。那才是通俄门调查的实质，不在于我和我的家人朋友，而是你们。他们试图拿走你们的投票权、取消竞选这一伟大的传统、还有否定或许是我们国家历史上最伟大的一次选举结果（指2016总统大选）。”
 </p>
 <p>
  川普说，他遇到的难度是因为他的“美国优先”原则，触动了那些伤害美国的势力及他们的利益。
 </p>
 <p>
  美国总统 川普：“我考虑的唯一一件事就是如何让美国人赢。我为你们而战，那不容易，我想你们也看到了。我们去除了那些不好的联盟、说客、捐赠者，他们靠抽干我们的国家来谋生。我们让这些人都走人了。我们拆掉了华盛顿暗室的墙，里面的人干着不可告人的勾当：关闭我们的公司、拿走你们的工作、关闭工厂、放弃主权、牺牲你们每个人的生活方式。我们终结了那一切。我说过很多次我们要抽干沼泽，那正是我们现在做的事，抽干沼泽。”
 </p>
 <p>
  美国副总统 彭斯：“再给（川普政府）4年，意味着更多的就业、更多（传统理念）法官、更多支持给到军队；至少还需要4年抽干那个沼泽。”
 </p>
 <p>
  把权力归还给人民，川普在就职演说中的声音，延续到佛州
  <span href="https://www.ntdtv.com/gb/竞选连任.htm">
   竞选连任
  </span>
  的现场。
 </p>
 <p>
  美国总统 川普：“想像一下，2020年我们有一个民主党总统和民主党控制的国会。他们会剥夺你们的自由言论。利用法律来惩罚他们的对手，这正是他们现在努力而为的事。他们总是试图保护自己。他们将剥夺美国人的宪法权利，同时让非法移民涌入美国，以扩大其政治基础、基本盘。并且未来在某些地方让这些非法移民成为民主党的票仓。这就是所有这一切的实质。”
 </p>
 <p>
  美国副总统 彭斯：“你在2020大选中的选择，不仅是在2个候选人中做选择，而是选择两种不同的未来，是要更多的民主自由，还是要更多政府管控。”
 </p>
 <p>
  当晚，第一夫人梅拉妮娅陪同川普参加集会。
 </p>
 <p>
  美国第一夫人 梅拉妮娅：“他真的爱这个国家。我们全家会继续全力以赴为美国人工作，只要他有机会，我们家庭每个人都会这样做。”
 </p>
 <p>
  川普上任至今，已为美国创造600万份工作，就业总人数首次超过1.5亿，达到1.6亿。
 </p>
 <p>
  新唐人记者李兰综合报导
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102604777.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102604777.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102604777.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/19/a102604777.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

