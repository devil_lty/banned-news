### 减缓移民涌入美国 墨西哥部署1万5000名军人
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1151323665.jpg" target="_blank">
  <figure>
   <img alt="减缓移民涌入美国 墨西哥部署1万5000名军人" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1151323665-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  墨西哥部署近1万5000名军人和国家卫队在美墨边。图为墨西哥北部城市华雷斯城，士兵捉拿移民。(HERIKA MARTINEZ/AFP/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月25日讯】美墨边境
  <span href="https://www.ntdtv.com/gb/移民.htm">
   移民
  </span>
  跨境数量近几个月暴增，为减缓移民涌入美国，墨国军事首长24日表示，墨国已在墨美边境部署近1万5000名军人和国家卫队（National Guard）人员，但坦承政策引发反弹后，已拘留多名试图跨越边界的移民。
 </p>
 <p>
  法新社报导，美国总统川普因中美洲
  <span href="https://www.ntdtv.com/gb/移民.htm">
   移民
  </span>
  排山倒海涌入美国，而向墨国施压，墨国本月初承诺部署6000名国家卫队人员来加强管理墨南边界，但当初未对外披露他们在墨北边境扫荡移民的程度。
 </p>
 <p>
  在
  <span href="https://www.ntdtv.com/gb/墨西哥.htm">
   墨西哥
  </span>
  总统罗培兹欧布拉多（Andres Manuel Lopez Obrador）陪伴下，墨国国防部长桑多华（Luis Cresencio Sandoval）在记者会上表示：“我们从国家卫队和军事单位进行调度，在北部边界共部署1万4000，将近1万5000的人力。”
 </p>
 <p>
  当被问及部队是否曾为了阻止移民跨境而拘留这些人时，桑多华回答说：“有。”
 </p>
 <figure class="wp-caption alignnone" id="attachment_102608830" style="width: 600px">
  <img alt="" class="size-medium wp-image-102608830" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1151323658-600x384.jpg">
   <br/><figcaption class="wp-caption-text">
    <span href="https://www.ntdtv.com/gb/墨西哥.htm">
     墨西哥
    </span>
    部署近1万5000名军人和国家卫队在美墨边境。图为墨西哥北部城市华雷斯城，士兵捉拿移民。(HERIKA MARTINEZ/AFP/Getty Images)
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  他表示，“鉴于（无证）迁移并非犯罪行为，仅属行政违规，基本上我们拘留他们之后，会把他们交给”墨西哥国家移民局（National Migration Institute）处置。
 </p>
 <p>
  中美洲移民为了摆脱贫穷、逃离暴力帮派而远走他乡，借道墨西哥前往美国时，大多没有移民文件。
 </p>
 <p>
  然而，国际法保障无照移民跨越国际边界寻求庇护的权利，美国法院也维持移民在美国边界沿线任何一处这方面的权利，无论是正式跨境与否。
 </p>
 <p>
  美国官员5月在美墨边境拘留约14万4000名移民，比4月多32%，比去年同期更是暴增278%。总人数含破纪录的8万9000个家庭。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102608833" style="width: 600px">
  <img alt="" class="size-medium wp-image-102608833" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1150397034-600x400.jpg">
   <br/><figcaption class="wp-caption-text">
    墨西哥部署近1万5000名军人和国家卫队在美墨边。(HERIKA MARTINEZ/AFP/Getty Images)
   </figcaption><br/>
  </img>
 </figure><br/>
 <p>
  (责任编辑：卢勇信)
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102608812.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102608812.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog203/a102608812.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/25/a102608812.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

