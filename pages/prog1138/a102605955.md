### 习近平访朝现异常 王沪宁罕见未陪同
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/p8960891a522626073.jpg" target="_blank">
  <figure>
   <img alt="习近平访朝现异常 王沪宁罕见未陪同" src="https://i.ntdtv.com/assets/uploads/2019/06/p8960891a522626073-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  习近平正在对朝鲜进行为期两天的国事访问。王沪宁罕见没有随行。（ Feng Li/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月21日讯】6月21日，
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  结束了对朝鲜为期两天的国事访问。外界发现，在陪同人员中，中共政治局常委
  <span href="https://www.ntdtv.com/gb/王沪宁.htm">
   王沪宁
  </span>
  罕见没有随行。王作为主导中共对朝党际外交，且在金正恩4次访华时都会登场亮相的要角，却没有随习访朝，引发舆论猜测。
 </p>
 <p>
  6月20日至21日，
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  对朝鲜进行国事访问。根据中共新华社报导，陪同习出访的中共大员包括：中共中央办公厅主任丁薛祥，外事工作委员会办公室主任杨洁篪，外交部部长王毅，发改委主任何立峰等。
 </p>
 <p>
  值得关注的是，陪同人员中少了中共政治局常委、中央政策研究室主任
  <span href="https://www.ntdtv.com/gb/王沪宁.htm">
   王沪宁
  </span>
  。王曾在金正恩4次访华期间，都成为登场要角，全程接送陪同金正恩。
 </p>
 <p>
  作为给习近平在外交领域出谋划策，并被指分管朝鲜外交的中共政策研究室主任的王沪宁，此次没有陪同习近平出访，让外界颇为诧异。
 </p>
 <p>
  大纪元评论员周晓辉说，如果习近平在海内外的点醒下，已对王沪宁产生怀疑，甚至意识到了王的贰心，那么将王沪宁排除在出访朝鲜名单之外，也不令人意外。
 </p>
 <p>
  而王沪宁未随行，与习月初在俄罗斯释放“川普是朋友”，回国后又与川普通话确认“川习会”，释放贸易谈判重启等信号，同时央视也从连播抗美片改播美中爱情片等，可以看出习与当前“抗美派”背道而行。
 </p>
 <p>
  同时也彰显中共高层之间暗流汹涌，你死我活的斗争形势。至于王沪宁的结局，以及习近平最终选择哪条路，还有待进一步观察。
 </p>
 <p>
  王沪宁十九大之后晋升中央政治局常委，接管了意识形态和党务工作。2017年底，在朝鲜战云笼罩的敏感时刻，中共海外党媒多维网曾称，王沪宁开始分管对朝外交。
 </p>
 <p>
  但在去年北戴河会议前后，王沪宁在公众视线中消失1个多月，外界猜测王沪宁因误导习打贸易战，而且对习搞低级红、高级黑的舆论包装，因此遭到习的问责。
 </p>
 <p>
  当时，评论员张怀玉分析说，十九大从幕后走到前台的王沪宁，一步一步把习近平往泥坑里带。与意识形态有关的馊主意都是他出的，不停地把江泽民热衷的歪理邪说往习近平脑子里灌。中美贸易战的紧要关头，王沪宁真面目逐渐暴露出来了。
 </p>
 <p>
  王沪宁一直被指是“误导”习近平、激化贸易战的罪魁祸首。在应对贸易问题上的强力施压方面，让习误判了中共的实力，误判了川普政府的决心，从开始的强硬、以牙还牙，到后来的服软，再到新一波抗美，王沪宁均指负有责任。
 </p>
 <p>
  近日，有港媒称，习近平在美中“新冷战”中进退失据，丧失部分高干的信任，许多人正在消极抵制，伺机反扑。而7个常委中也立场分裂，王沪宁和
  <span href="https://www.ntdtv.com/gb/韩正.htm">
   韩正
  </span>
  ，作为江泽民和曾庆红布下的“暗器”角色，正在利用贸易战和香港事件搅局。
 </p>
 <p>
  <strong>
   王沪宁深得江泽民的赏识
  </strong>
 </p>
 <p>
  王沪宁有中共三朝“党师”之称，被视为江泽民“三个代表”、胡锦涛“科学发展观”及习近平“中国梦”、“习核心”及“习思想”意识形态的幕后军师。
 </p>
 <p>
  王沪宁深得江泽民的赏识，外界视之为上海帮人马。1995年，江召王沪宁上京，出任中共政策研究室政治组组长，后升至研究室副主任，为江包装推出“三个代表”，成为江思想的核心。
 </p>
 <p>
  江泽民退位后胡锦涛主政，王沪宁续受赏识，升任研究室主任，成为中南海首席智囊，是胡锦涛“科学发展观”的重要推手。
 </p>
 <p>
  2012年习近平上台后，王沪宁继续受重任，习近平“中国梦”、“习思想”理论均是王策划。习国内考察、出国外访，王都陪同左右，显示无论内政、外交，王的地位都举足轻重。
 </p>
 <p>
  王沪宁成为中共新一届常委后，去年
  <span href="https://www.ntdtv.com/gb/美中贸易战.htm">
   美中贸易战
  </span>
  开打，当时习近平出访中东、非洲等地，王沪宁从随行人员中罕见消失。此后，王沪宁“失踪”超过一个多月。此后，有关其被整肃、打入冷宫的传闻不断。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/美中贸易战.htm">
   美中贸易战
  </span>
  开打当天，亲习港媒发表了对几名中共“匿名官员”的专访。他们没有指责美国，而是将贸易战升温的责任推给中共党内部分官员和党媒，矛头直指王沪宁。
 </p>
 <p>
  海外媒体也盛传，王沪宁因宣传不当和过度夸大中共实力，误导习近平，在党内陷入“麻烦”。
 </p>
 <p>
  （记者文馨报导/责任编辑：李泉）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102605955.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102605955.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102605955.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/21/a102605955.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

