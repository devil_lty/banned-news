### 高层内斗多激烈？党媒：有人“手榴弹向后扔”
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1129984778.jpg" target="_blank">
  <figure>
   <img alt="高层内斗多激烈？党媒：有人“手榴弹向后扔”" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1129984778-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  26日，党媒再次刊发评论文章，狠批中共党内那些“手榴弹向后扔”的人。示意图（Kevin Frayer/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月28日讯】
  <span href="https://www.ntdtv.com/gb/川习会.htm">
   川习会
  </span>
  前夕，中南海局势诡异，中共高层分裂再度公开化。江派常委王沪宁掌控的党媒，除了连发6篇评论批评美国，同时把矛头对准党内“投降派”。近日，党媒再次刊发评论文章，狠批中共党内那些“手榴弹向后扔”的人。
 </p>
 <p>
  6月26日，中共党媒新华社刊发《警惕那些“手榴弹向后扔”的人》一文，批评有人鼓吹投降论，在中美经贸摩擦“两军对垒”之时，要警惕这些“手榴弹向后扔”的人。
 </p>
 <p>
  文章继续以中共惯用的文革式腔调，煽动中国人民“要做好斗争到底的准备。谈，大门敞开；打，奉陪到底。”同时劝告将“手榴弹向后扔”的人，以及试图将“手榴弹向后扔”的人，尽快收手。
 </p>
 <p>
  6月17日至22日，《人民日报》更是连发6篇评论文章，除了批评美国之外，矛头还对准党内“投降派”，声称美方升级贸易摩擦，一些人却要求北京“韬光养晦”、“忍气吞声”。
 </p>
 <p>
  同时批评“崇美恐美者”制造恐慌，如今应该说一声“厉害了我的国”等等。“厉害了我的国”是2018年向中共两会献礼的政治片，因坐实中共盗用、偷窃、强制转移欧美技术，而引发连锁反应，去年4月紧急停播。
 </p>
 <p>
  <strong>
   王沪宁煽情遭遇反弹
  </strong>
 </p>
 <p>
  5月初，中方突然撤回10个月以来的中美贸易谈判做出的承诺后，美国
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  （特朗普）总统一怒之下，宣布对中国商品全面征税，
  <span href="https://www.ntdtv.com/gb/美中贸易战.htm">
   美中贸易战
  </span>
  再度开打。
 </p>
 <p>
  中共沉默多日后，由中共江派常委王沪宁控制的宣传口，发起声势浩大的反美宣传，然而此举遇到民间反弹，引来网民一片嘲讽。进入6月，党媒开始气急败坏的调转枪口，猛批本国民众“人心涣散”，同时矛头指向党内投降派。
 </p>
 <p>
  《人民日报》则从5月23日至31日连续刊发所谓的“九论”，倒打一耙指责美国“言而无信”，“一意孤行”。央视则以“不怕打”，“奉陪到底”等强硬言论煽动民族情绪。与此同时，北京又下令播出70部“爱国主义影片”。
 </p>
 <p>
  除了反美宣传外，中共于6月2日发表“中美贸易谈判白皮书”，倒打一耙指责美国在贸易谈判中出尔反尔、不讲诚信，致使中美经贸磋商严重受挫等。
 </p>
 <p>
  然而，
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  6月7日访问俄罗斯期间，公开表示，
  <span href="https://www.ntdtv.com/gb/川普.htm">
   川普
  </span>
  总统是“我的朋友”，双方都不愿看到中美完全割裂。但习的这番话遭党媒封杀。新华社同时还刊发批评投降派的文章，被指暗批习近平。
 </p>
 <p>
  <strong>
   专家：中南海内斗残酷 习左右摇摆
  </strong>
 </p>
 <p>
  一名哈佛学者引述中共高官透露，
  <span href="https://www.ntdtv.com/gb/美中贸易战.htm">
   美中贸易战
  </span>
  下，中南海内斗的残酷气氛，不是你死，就是我亡。有台湾知名政论家说，中共党内牵制力量太大，
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  在应对贸易战上左右摇摆。
 </p>
 <p>
  台湾专家对《自由亚洲》分析说，习近平现在是内外交困。同时，中共党内存在4大反习力量，即残存江派势力，中央到地方数以百万的贪官，贪腐经商的太子党，军中反习势力。
 </p>
 <p>
  分析还认为，习近平现在是党内摆不平，外部也会相互激荡。如果选择不好，习近平肉体和政治生命的都会受影响，而关键是习近平是否要抛弃共产党，目前，他在这方面也是两边摇摆。
 </p>
 <p>
  时评人唐浩在大纪元刊文说，如今北京仍在狂妄应对贸易战 ，或将招来川普发起更大反击。而中共对美方逞凶斗狠，是以牺牲无辜的14亿中国百姓为代价的，同时也为中共自身点燃了垮台崩解的导火索。
 </p>
 <p>
  一旦中共垮台，那些靠着血腥迫害民众，来升官加爵的大批中共贪官，未来不但无法留在中国境内或躲入美国养老，甚至还可能被全球追查通缉。
 </p>
 <p>
  至于北京领导人，更应谨慎深思：是要主动解体这个祸乱神州大地70年的西来乱党，还是要等著中共被解体垮台时，被一并清算究责？
 </p>
 <p>
  善恶一念间，决定了自己的历史定位，也决定了自己的生命未来。
 </p>
 <p>
  （记者李韵报导/责任编辑：李泉）
 </p>
 <p>
  <strong>
   相关链接：
   <span href="https://cn.ntdtv.com/gb/2019/06/15/a102601274.html" rel="noopener" target="_blank">
    习近平陷入空前危机 港人反“送中”再添新变数
   </span>
   <br/>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/17/a102602589.html" rel="noopener" target="_blank">
    习近平遭“埋刀设伏”？专家揭香港事件内幕
   </span>
   <br/>
   相关链接：
   <span href="https://www.ntdtv.com/gb/2019/06/16/a102602007.html" rel="noopener" target="_blank">
    怕习近平喊川普朋友？习前脚出访王沪宁后院点火
   </span>
  </strong>
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102611009.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102611009.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102611009.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/28/a102611009.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

