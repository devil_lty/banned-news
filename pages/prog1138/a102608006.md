### 中南海设3年大限 出手整治基层两大势力惹议
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-52002604.jpg" target="_blank">
  <figure>
   <img alt="中南海设3年大限 出手整治基层两大势力惹议" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-52002604-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  23日，中南海下发文件要整治乡村势力，包括铲除基层黑恶势力，重整遭侵蚀的基层政权。不过，评论认为，真想彻底改变基层，首先应该打掉中共这个黑恶势力。（ Guang Niu/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月24日讯】
  <span href="https://www.ntdtv.com/gb/湖南怀化.htm">
   湖南怀化
  </span>
  新晃一中操场下
  <span href="https://www.ntdtv.com/gb/埋尸案.htm">
   埋尸案
  </span>
  邓世平的身份被确认，基层涉黑案件惊动全社会。23日，中南海下发改进乡村治理的文件，包括铲除基层
  <span href="https://www.ntdtv.com/gb/黑恶势力.htm">
   黑恶势力
  </span>
  ，重整遭侵蚀的基层政权。不过，评论认为，真想彻底改变基层，首先应该打掉中共这个黑恶势力。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/湖南怀化.htm">
   湖南怀化
  </span>
  新晃一中操场
  <span href="https://www.ntdtv.com/gb/埋尸案.htm">
   埋尸案
  </span>
  曝光后，中共喉舌《人民日报》旗下侠客岛，也刊发评论文章强调，中共许多地方政权已被基层
  <span href="https://www.ntdtv.com/gb/黑恶势力.htm">
   黑恶势力
  </span>
  嵌入、结合，形成有组织保驾护航利益集团。该案再次证明，不扫黑除恶，像操场埋尸、孙小果等陈年旧案还会不断的出现。
 </p>
 <p>
  6月23日，中共党政两大办公厅下发有关加强和改进乡村治理的指导性文件，提及乡村两委选举有关涉黑的问题。
 </p>
 <p>
  文件强调要完善村级组织体系，严厉打击破坏村两委选举的黑恶势力、宗族势力，把受过刑事处罚、存在村霸和涉黑涉恶等问题的人，从组织中清理出去。
 </p>
 <p>
  中共自2018年1月就提出开展为期三年的“扫黑除恶专项斗争”运动，包括铲除基层黑恶势力，重整遭侵蚀的基层政权。
 </p>
 <p>
  如今中共的扫黑除恶已经进行了一年多，“扫黑除恶”的标语出现在各个大小城乡街头，但是最大的黑恶势力从未被触及。
 </p>
 <p>
  对此，中共海外党媒24日报导称，目前已着手布局乡村治理，首要任务是完善中共组织领导乡村治理的体制机制，贯彻中共的决定，推动改革发展，借此强化基层政权建设等。
 </p>
 <p>
  不过，观察人士分析认为，无论中共如何设大限，如何整治基层，不打掉中共这个统治集团，都是纸上谈兵，因为中共政权才是中国社会最大的黑恶势力。
 </p>
 <p>
  前山东维权律师李向阳对《新唐人》表示，黑恶势力都有保护伞，而这保护伞就是中共的主要官员。他们是不会自己扫自己的。他们只不过是借着“扫黑除恶”来打压百姓。比如，很多访民、维权人士、法轮功学员，居然被“扫黑除恶”定性为“黑恶分子”。
 </p>
 <p>
  他认为，真正黑恶势力的都有中共官僚背景，甚至有的不法官员已经成为黑社会势力的头子，把他们所在地的单位都变成黑社会的组织了。在这个情况下，到底谁是黑社会？谁是黑社会保护伞？一目了然。
 </p>
 <p>
  自中共开展扫黑除恶以来，各地公安系统官员纷纷落马，不少地方的公、检、法更是被一锅端。据中共党媒报导，湖北武汉市公、检、法一把手，因充当黑恶势力的保护伞，悉数落马；南昌江西公安系统官员，多因涉贪、充当“保护伞”而倒台。
 </p>
 <p>
  多数观点均认为，真要扫黑除恶就必须先打掉中共这个黑恶势力、黑恶集团，因为中共政权才是基层大大小小黑恶势力的总代表，是他们的保护伞。如果做不到这一点，扫黑不会有任何效果。
 </p>
 <p>
  （记者李韵报导/责任编辑：祝馨睿）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102608006.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102608006.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102608006.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/24/a102608006.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

