### 为何多数港人仇恨共产党？中共少将说漏嘴（视频）
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/06/201811130830194358.jpg" target="_blank">
  <figure>
   <img alt="为何多数港人仇恨共产党？中共少将说漏嘴（视频）" src="https://i.ntdtv.com/assets/uploads/2019/06/201811130830194358-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  中共少将徐焰演讲中，恶毒攻击港人时说漏嘴了，道出了多数香港人仇恨共产党的原因。（视频截图）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月14日讯】为阻止香港立法会恢复审议《逃犯条例》，百万香港民众上街示威无效后，又包围立法会抗议，警方对示威者开枪造成流血冲突，事件震惊国际社会。同日曝光的
  <span href="https://www.ntdtv.com/gb/中共少将.htm">
   中共少将
  </span>
  徐焰的一段演讲中，恶毒攻击港人时说漏嘴，道出了多数香港人仇恨
  <span href="https://www.ntdtv.com/gb/共产党.htm">
   共产党
  </span>
  的原因。
 </p>
 <p>
  6月12日网络热传中共国防大学教授徐焰少将的一段演讲，徐焰用阶级分析法批评香港的社会结构，他说，1997年中共军队进驻香港时，他同驻港部队领导一起参加香港的社情研究，发现香港的社会基础是最坏的，比台湾都坏。
 </p>
 <p>
  他还称，1997年香港主权移交北京后，香港居民成分有三种：
  <br/>
  一种是：接受港英教育的原居民，对大陆没太大仇恨。
  <br/>
  第二种是：三分之一是1949年到1950年遭
  <span href="https://www.ntdtv.com/gb/共产党.htm">
   共产党
  </span>
  清算和打倒后逃到香港的，他指这部分是“最坏的”，对共产党有“
  <span href="https://www.ntdtv.com/gb/刻骨仇恨.htm">
   刻骨仇恨
  </span>
  ”。
  <br/>
  第三种是，1958～1961三年大饥荒期间挨饿，逃到香港的难民，他们对中共印象好得了吗？
 </p>
 <p>
 </p>
 <p>
  徐焰演讲中恶毒攻击港人的言论，道出了多数香港人仇恨共产党的原因。同时也道出了中共当局仇恨港人的原因。
 </p>
 <p>
  有人将徐焰的言论与港府最新修订的“送中条例”联系在一起，提醒说“三分之一避难的和三分之一逃荒的都是坏人。 所以你们知道这个引渡法是打算抓谁了吗？”
 </p>
 <p>
  大纪元查证发现，徐焰这段讲述出自2018年11月5日至7日，在成都举行的第十二届全国名师工作室发展论坛，徐焰身穿军装在台上做了《南海博弈》的报告。
 </p>
 <p>
  徐焰上述讲话曝光之时，香港迎来史上最黑暗的一天。
 </p>
 <figure class="wp-caption alignnone" id="attachment_102600528" style="width: 600px">
  <span href="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1149436299-600x400-1.jpg">
   <img alt="" class="size-medium wp-image-102600528" src="https://i.ntdtv.com/assets/uploads/2019/06/GettyImages-1149436299-600x400-1-600x337.jpg"/>
  </span>
  <br/><figcaption class="wp-caption-text">
   香港特区政府12日出动催泪弹、橡胶子弹和布袋弹驱散反对送中条例的示威者，至少79人受伤。（GettyImages）
  </figcaption><br/>
 </figure><br/>
 <p>
  6月12日，香港政府无视百万民众上街抗议的心声，宣布如期恢复二读辩论修订《逃犯条例》。数万香港市民再次发起罢工罢课罢市活动，对香港立法会、特区政府总部等发起“和平包围”，希望以和平的手段争取民主自由。
 </p>
 <p>
  参与抗议活动的27岁的香港建筑测量师Suki Ma对CNN表示，中共政府希望夺走我们的自由。我们出生时就有自由，1997年之后，自由在我们这里渐渐地消失。
 </p>
 <p>
  参加绝食活动的菲利克斯说，我们的绝食目标是103小时，象征103万人大游行。我是学生，发起绝食的还有教师、讲师、以及文化艺术界人士等。
 </p>
 <p>
  然而，在当天下午，香港政府出动催泪弹、橡胶子弹和布袋弹等，血腥镇压示威者，至少造成79人受伤，二人情况严重，甚至有人头部中枪。
 </p>
 <p>
  香港政府的暴行震惊国际社会，引发国际舆论强烈谴责，包含美国、英国、欧盟、德国等元首以及国际人权组织，纷纷站出来声援港人。
 </p>
 <p>
  香港立法委员会原定在当日上午11点对《引渡条例》进行二读，最终被推迟了。那么《引渡条例》为何激怒全球谴责抗议？
 </p>
 <p>
  《引渡条例》修订内容拟议的法案，允许香港将逃犯引渡到没有正式引渡协议的地区，包括中国大陆、台湾和澳门。
 </p>
 <p>
  大纪元报导说，若依港府建议的修订版本，如果某国家或地区和香港没有长期引渡协议，可由其政府（包括中共政府）对香港提出要求，启动“特别移交安排”。
 </p>
 <p>
  该法案的反对者表示，这可能意味着民主活动家、记者和外国企业主可能会被引渡到中国大陆。而今次修例的另一敏感点是剔除立法会的个案审批，而由特首决定 。也就是说，修例修订后，特首有最大的裁量权。
 </p>
 <p>
  为何这个问题如此敏感？相比于中国大陆，香港享有独立的法律制度和政治制度，允许公民享受在大陆没有受到保护的自由。香港法律沿袭英国法律，与中国大陆不同。
 </p>
 <p>
  批评人士表示，该法案将使香港境内的任何人都很容易因政治原因，或无意中的商业罪行，而落入中共当局手中；该法案破坏香港半自治法律制度。
 </p>
 <p>
  早些时候，香港记者协会在一份声明中表示，该修正案“不仅威胁到记者的安全，而且对香港的言论自由产生了寒蝉效应”。
 </p>
 <p>
  （记者李韵报导/责任编辑：戴明）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102600523.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102600523.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102600523.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/14/a102600523.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

