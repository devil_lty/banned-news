### 中共前公安副部长孟宏伟受审
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月20日讯】中共前公安部副部长、国际刑警组织主席
  <span href="https://www.ntdtv.com/gb/孟宏伟.htm">
   孟宏伟
  </span>
  ，涉嫌
  <span href="https://www.ntdtv.com/gb/受贿案.htm">
   受贿案
  </span>
  ，周四在天津中院
  <span href="https://www.ntdtv.com/gb/受审.htm">
   受审
  </span>
  。孟宏伟被控受贿人民币1446多万元。
 </p>
 <p>
  指控称，2005年到2017年期间，
  <span href="https://www.ntdtv.com/gb/孟宏伟.htm">
   孟宏伟
  </span>
  担任公安部副部长、中国海警局局长等职务期间，利用职务便利，为有关单位和个人在企业经营、职务晋升等事项上提供帮助，收受财物，共计折合人民币1446多万元。
 </p>
 <p>
  2004年4月起，孟宏伟任公安部副部长、党委委员；同年8月兼任国际刑警组织中共国家中心局局长，2011年8月，兼任中共国家反恐怖工作协调小组办公室主任；2012年3月，兼任国家海洋局副局长、党组副书记兼中共海警局局长。
 </p>
 <p>
  2016年11月，孟宏伟当选国际刑警组织主席，曾引发外界广泛批评和谴责。
 </p>
 <p>
  去年10月5日，孟宏伟的妻子高歌在法国报案，称孟宏伟9月回国后失踪，引起国际关注。
 </p>
 <p>
  孟宏伟在公安部任职多年，被指是前中共政法委书记周永康的老下属和心腹。
 </p>
 <p>
  另据《明慧网》报导，孟宏伟曾参与中共江泽民集团利用政法系统对法轮功的迫害，早就被列为迫害法轮功学员的涉案责任人之一。
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102605301.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102605301.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102605301.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/20/a102605301.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

