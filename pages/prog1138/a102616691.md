### 逾40美议员致函川普 制裁新疆书记陈全国
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/497cfa51caf948ce41ea201ab80e5fd2.jpg" target="_blank">
  <figure>
   <img alt="逾40美议员致函川普 制裁新疆书记陈全国" src="https://i.ntdtv.com/assets/uploads/2019/07/497cfa51caf948ce41ea201ab80e5fd2-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  这些议员在信中指责中共政府和官员，合谋或指使了新疆不断发生的侵犯人权行为，这些行为可能构成反人类罪行。图为新疆维族人（ Kevin Frayer/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月06日讯】
  <span href="https://www.ntdtv.com/gb/美国.htm">
   美国
  </span>
  有超过40名
  <span href="https://www.ntdtv.com/gb/国会议员.htm">
   国会议员
  </span>
  5日再次致函川普（特朗普）政府，敦促
  <span href="https://www.ntdtv.com/gb/制裁.htm">
   制裁
  </span>
  中国新疆涉侵犯当地维吾尔族人及其他少数民族权益的官员，当中包括中共政治局委员、新疆党委书记陈全国。
 </p>
 <p>
  据了解，敦促
  <span href="https://www.ntdtv.com/gb/制裁.htm">
   制裁
  </span>
  中共政治局委员陈全国等人的多名
  <span href="https://www.ntdtv.com/gb/美国.htm">
   美国
  </span>
  议员，包括美国参议员鲁比奥（Marco Rubio）、麦戈文（Jim McGovern）等。
 </p>
 <p>
  鲁比奥和麦戈文表示，7月5日是2009年新疆乌鲁木齐“七．五骚乱”发生10周年，在这个悲伤的周年纪念日，我们再次呼吁让中共官员承担责任。
 </p>
 <p>
  今年4月3日，包括鲁比奥和麦戈文在内的43名美国参、众两院
  <span href="https://www.ntdtv.com/gb/国会议员.htm">
   国会议员
  </span>
  ，已经联名致信国务卿蓬佩奥、财政部长姆努钦和商务部长罗斯，呼吁制裁严重侵犯穆斯林人权的中共官员，包括主政新疆的陈全国。
 </p>
 <p>
  这些议员在信中指责中共政府和官员，合谋或指使了新疆不断发生的侵犯人权行为，这些行为可能构成反人类罪行。
 </p>
 <p>
  联合国的消除种族歧视委员会去年8月引述可靠报告，指控中共将百万的新疆维吾尔人，秘密拘留在“再教育营”强制洗脑。
 </p>
 <p>
  一项新调查发现，中共还将维族儿童与他们的家庭分开，仅一个乡镇就有逾400名孩童的父母“下落不明”。BBC揭露，中共一方面将成年人关押在“集中营”，另一方面又将孩童送往“儿童再教育营地”。
 </p>
 <p>
  尽管中共辩解这是为了打击所谓的“宗教极端主义”而设立的教育培训中心。但中共将孩童送往“儿童再教育营地”做法让人不解，与此同时，也不断有幸存者揭穿，“再教育营”里残酷的精神洗脑和肉体折磨。
 </p>
 <p>
  被关押者被迫歌颂和感激中共，受酷刑折磨，被迫服不明药物，不少人被折磨致死。
 </p>
 <p>
  上述43位美国国会议员在联名信中，要求美国政府迅速实施《全球马格尼茨基法》，制裁陈全国和其他侵犯人权的
  <span href="https://www.ntdtv.com/gb/新疆官员.htm">
   新疆官员
  </span>
  。
 </p>
 <p>
  去年11月，也曾有15个西方国家的驻华大使联署致信陈全国，要求他讲清楚侵犯新疆维吾尔人权利的情况。
 </p>
 <p>
  当时美国大使没有签名，但美驻华使馆在一份声明中说，美国自2017年4月，得知中共在多个再教育营里拘压80万以上、甚至近200万维吾尔人的信息，且一直对此警觉。
 </p>
 <p>
  声明还说，美国将继续呼吁中共结束类似的适得其反政策，并立即释放所有被任意拘押的人，美国政府将致力于对那些犯有
  <span href="https://www.ntdtv.com/gb/侵犯人权罪.htm">
   侵犯人权罪
  </span>
  行的人进行问责，包括针对那些
  <span href="https://www.ntdtv.com/gb/新疆官员.htm">
   新疆官员
  </span>
  进行制裁。
 </p>
 <p>
  （记者李韵报导/责任编辑：李泉）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102616691.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102616691.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102616691.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/06/a102616691.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

