### 港媒揭高层内斗：习一票定生死 刘鹤险成罪人
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1130590290.jpg" target="_blank">
  <figure>
   <img alt="港媒揭高层内斗：习一票定生死 刘鹤险成罪人" src="https://i.ntdtv.com/assets/uploads/2019/07/GettyImages-1130590290-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  有港媒说，由于中共6常委对待贸易战观点对立，习近平投了决定的一票，中方毁约，刘鹤险些成为“丧权辱国”罪人。（ Andrea Verdelli/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月04日讯】
  <span href="https://www.ntdtv.com/gb/34765.htm">
   美中贸易谈判
  </span>
  关键时刻，北京翻脸毁约的内幕，传出新说法。7月4日，有港媒披露，当时中共6常委观点对立，投票结果是3比3。最后
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  投了决定性的一票，形成4比3的结局。导致中方毁约，中方谈判代表刘鹤因此险成“丧权辱国”的罪人。
 </p>
 <p>
  日前，G20川习会上，美中达成共识，双方贸易战暂时停火，并从一个多月前谈判破裂的地方开始恢复谈判。但整个谈判过程并不顺利。
 </p>
 <p>
  中美贸易战于去年4月爆发，两国随后成立高层谈判小组磋商解决办法，至今年4月底，双方共举行10轮谈判，拟定出协议草案，准备在5月初的第11轮谈判达成协议。
 </p>
 <p>
  但在第10轮谈判中，中方突然翻脸毁约，使美中双方过去一年来的谈判前功尽弃。导致川普大怒，指责中共出尔反尔、试图重新谈判，因此决定对中国商品加征关税。
 </p>
 <p>
  <strong>
   北京翻脸内幕
  </strong>
 </p>
 <p>
  7月4日，香港苹果日报引述北京消息披露，中美共10轮谈判得出的协议草案，在中共内部产生激烈争斗。4月底的一次中央政治局常委会上，7名中共常委以投票方式对协议草案表决，结果是3比3。
 </p>
 <p>
  同意者包括总理李克强、政协主席汪洋和中纪委书记赵乐际；反对者包括中共人大委员长栗战书、中央书记处书记
  <span href="https://www.ntdtv.com/gb/王沪宁.htm">
   王沪宁
  </span>
  ，以及常务副总理
  <span href="https://www.ntdtv.com/gb/韩正.htm">
   韩正
  </span>
  。最后是
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  投下决定性的一票：反对。
 </p>
 <p>
  消息还透露，协议草案又拿到中央政治局讨论，25名政治局成员以大比数通过了上述投票结果，这份协议草案因此被“判处死刑”。
 </p>
 <p>
  上述消息说，事后，有人质疑刘鹤何以搞出这个堪称“丧权辱国”、一旦公开肯定会遭全国人民指骂的“卖国条约”，还批评刘政治敏感度不够。而习近平则挺身力保刘鹤，称一切责任由习自己承担。
 </p>
 <p>
  上述消息与此前透露出的消息似乎有些出入，不过也有类似之处。
 </p>
 <p>
  <strong>
   江系拟反攻倒算拿下习
  </strong>
 </p>
 <p>
  国际政治金融专家汪浩在台媒引述北京消息说，
  <span href="https://www.ntdtv.com/gb/34765.htm">
   美中贸易谈判
  </span>
  破裂，是因在5月1日的政治局常委会上，
  <span href="https://www.ntdtv.com/gb/韩正.htm">
   韩正
  </span>
  跳出来带头反对刘鹤努力达成的贸易协议，导致习近平最后拍板要和美国长期对抗。
 </p>
 <p>
  韩正出身上海帮，而被指激化贸易战的罪魁祸首
  <span href="https://www.ntdtv.com/gb/王沪宁.htm">
   王沪宁
  </span>
  也来自上海，两人都被曾庆红提拔进京。因此，一些海外媒体分析，习近平对美国强硬，其中很大一部分因素是江派误导所致。
 </p>
 <p>
  5月美中谈判破裂后，王沪宁主管的宣传系统掀起声势浩大的反美宣传，接连播放“抗美电影”，连发社论攻击美国，开足马力进行舆论备战。
 </p>
 <p>
  就连6月底G20“川习会”前夕，党媒还连续发布多篇批评美国的文章、同时把矛头对准党内“投降派”，狠批那些“手榴弹向后扔”的人。
 </p>
 <p>
  阿波罗网时事评论员王笃然分析说，江系反对习近平与美国签署贸易协议，实际上是给习下套，让习相信不保党就权力地位不保的假相，并抛出集体负责的说法迷惑习。
 </p>
 <p>
  他说，这样一来，由贸易战升级带来的经济萧条和民怨，都会集中到习身上，最终中共做下的这些恶事、坏事还是要由习一人来承担。江系的如意算盘就是趁机反攻倒算，拿下习近平。
 </p>
 <p>
  正在发酵的香港事件，也被指与江派搅局有关。港澳历来是曾庆红的地盘，现在主管港澳事务的是韩正，港首林郑当年也是获江派力挺上位。
 </p>
 <p>
  （记者李韵报导/责任编辑：李泉）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102615316.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102615316.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102615316.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/04/a102615316.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

