### 官方通报5虎非法聚会 一场惊心政变内幕曝光
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2019/07/f408165e375487a9d3e76f6426b21572-800x450-1.jpg" target="_blank">
  <figure>
   <img alt="官方通报5虎非法聚会 一场惊心政变内幕曝光" src="https://i.ntdtv.com/assets/uploads/2019/07/f408165e375487a9d3e76f6426b21572-800x450-1-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  早2017年年初，中共官方通报：周永康、薄熙来等5虎是党内野心家、阴谋家，搞“非法聚会”等阴谋活动。（合成图片）
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年07月07日讯】
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  第一任期反腐
  <span href="https://www.ntdtv.com/gb/打虎.htm">
   打虎
  </span>
  ，不时传出遭政治暗杀和
  <span href="https://www.ntdtv.com/gb/政变未遂.htm">
   政变未遂
  </span>
  等爆炸性传闻。有港媒称，早在2017年年初，中共官方通报：周永康、薄熙来等5虎是党内野心家、阴谋家，搞“非法聚会”等阴谋活动，间接证实了5虎暗中勾连、涉政变未遂等传闻。
 </p>
 <p>
  目前，中共原证监会主席刘士余正在配合调查。刘落马前曾在中共十九大会议期间的分组会议上披露，孙政才、薄熙来、周永康、令计划、徐才厚、郭伯雄6人，“在党内位高权重，既巨贪又巨腐，又阴谋
  <span href="https://www.ntdtv.com/gb/篡党夺权.htm">
   篡党夺权
  </span>
  的这些案件，令人不寒而栗”。
 </p>
 <p>
  这是近40年来，中共高官首度公开承认内部存在“
  <span href="https://www.ntdtv.com/gb/篡党夺权.htm">
   篡党夺权
  </span>
  ”的高官，但刘的话当时遭陆媒过滤。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/习近平.htm">
   习近平
  </span>
  的贴身大秘丁薛祥2018年2月的讲话中，也曾批评中共十八大以来反腐中被查的高官，“几乎无一例外地都有政治问题，拉票贿选、拉帮结派，有的甚至想要篡党夺权”。
 </p>
 <p>
  <strong>
   薄周等5人非法聚会
  </strong>
 </p>
 <p>
  据香港杂志披露，2017年年初的中共十八届中纪委第七次大会上，时任中纪委书记王岐山在报告中，指名道姓的说，周永康、薄熙来、郭伯雄、徐才厚、令计划是中共党内的“野心家、阴谋家”，并指他们为实现野心，有妄图篡权、搞分裂的图谋，严重威胁国家政治安全。
 </p>
 <p>
  何为野心家、阴谋家？随着军方全面彻底肃清郭伯雄、徐才厚的余毒，以及军方上层大换班，同年5月上旬，由中央军委、中纪委、军纪委、军政法委联合下达有关郭伯雄、徐才厚野心家、阴谋家案中案内情。
 </p>
 <p>
  同时在中共军方军一级党委、省部一级党委传达了部分情况，列作机密。其中有原始档案材料；有郭徐案发后的交代；也有郭徐审讯期间的交代；还有郭徐帮派内部人士举报等经核实资料。
 </p>
 <p>
  报导说，有关郭徐野心家、阴谋家案披露，自2008年十七大四中全会后，周永康、薄熙来、郭伯雄、徐才厚、令计划5人借执行任务、公干，或借在北京召开会议期间，搞非法聚会、讨论、通报中央政治局、中央政治局常委会的内情，以及中央政治局主要领导人活动、有关文件提示等。
 </p>
 <p>
  5人还部署、组织、指使亲信、身边工作人员非法、违法收集党政军领导人的材料，加以编造罗列罪名，向中央政治局的常委会、中央军委、中纪委诬告，蓄意在党政军上层，散步相关信息。
 </p>
 <p>
  2012年2月，重庆事件爆发后，薄熙来、郭伯雄、徐才厚、周永康、令计划组成的政治联盟，密谋发动政变被曝光。同年两会期间，薄熙来出事，郭徐从周令处，得到有关消息后，在政治局会议上，为薄“鸣冤叫屈”，声称有政治力量要把薄整垮。
 </p>
 <p>
  《凤凰周刊》曾在封面报导中披露，周永康与薄熙来曾在重庆私下缔结“政治同盟”，要“大干一场”。而中共最高法院2015年3月发布的年度报告中，首次提及周永康“搞非组织政治活动”，侧面印证了“薄周政变”。
 </p>
 <p>
  <strong>
   “五人帮”下场
  </strong>
 </p>
 <p>
  习近平掌权后的前5年，在王岐山的协助致力于反腐“
  <span href="https://www.ntdtv.com/gb/打虎.htm">
   打虎
  </span>
  ”大力清除政敌，最终将薄周政变集团瓦解。
 </p>
 <p>
  五人帮中最早倒台的是薄熙来。2013年9月，他被当局以受贿、贪污和滥用职权3项罪名判处无期徒刑。薄妻谷开来则在2012年被安徽省合肥市中级法院裁定谋杀英国人海伍德罪名成立，判处死刑，缓期2年执行；2015年底被减为无期徒刑。
 </p>
 <p>
  周永康则于2014年7月29日遭立案审查；2015年6月11日被以受贿罪、滥用职权罪、故意泄露国家秘密罪，一审被判处无期徒刑。周落马被认为打破中共党内邓小平时代以来“刑不上常委”的政治潜规则。
 </p>
 <p>
  在周永康被查期间，军中“大老虎”徐才厚2014年被移送审查起诉。但他在2015年3月15日艾滋病发死于医院，军事检察院最后作出不起诉决定。
 </p>
 <p>
  在徐才厚死亡当月，3月初，郭伯雄的儿子郭正钢因贪腐被查，同年4月9日，郭伯雄被查，2016年7月25日，郭伯雄被判处无期徒刑。
 </p>
 <p>
  至于令计划，则于2014年被双开，2016年7月4日被以受贿罪、非法获取国家秘密罪、滥用职权罪，判处无期徒刑。
 </p>
 <p>
  外界普遍认为“五人帮”政变集团是以江泽民、曾庆红、周永康、薄熙来为主导，同时还有军方的郭伯雄、徐才厚及前中办主任令计划等及其属下，该政变集团被指是江泽民架空胡锦涛及向习近平夺权的棋子。
 </p>
 <p>
  有海外媒体披露，由于担心迫害法轮功遭清算，江派在中共十七大后制定了薄熙来、周永康联手政变、废掉习近平的计划，在十八大上让薄熙来接替周永康掌管政法委“第二权力中央”。
 </p>
 <p>
  待时机成熟后，联合江泽民在军中势力，意图在2年内赶习近平下台，推薄熙来上位。而令计划则居中联络，暗中策应。不过王立军出逃成都美驻馆的事件使该政变计划曝光，从而引爆中共高层的公开分裂和权斗。
 </p>
 <p>
  （记者李芸报导/责任编辑：赵云）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102617187.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102617187.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102617187.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/07/07/a102617187.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

