### 任正非自称“小蛤蟆” 混乱情史及前妻往事曝光
------------------------

#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;&nbsp;|&nbsp;&nbsp; [手把手翻墙教程](https://github.com/gfw-breaker/guides/wiki) &nbsp;&nbsp;|&nbsp;&nbsp; [禁闻聚合安卓版](https://github.com/gfw-breaker/bn-android) &nbsp;&nbsp;|&nbsp;&nbsp; [网门安卓版](https://github.com/oGate2/oGate) &nbsp;&nbsp;|&nbsp;&nbsp; [神州正道安卓版](https://github.com/SzzdOgate/update) 



<div class="featured_image">
 <span href="https://i.ntdtv.com/assets/uploads/2018/12/gettyimages-461.jpg" target="_blank">
  <figure>
   <img alt="任正非自称“小蛤蟆” 混乱情史及前妻往事曝光" src="https://i.ntdtv.com/assets/uploads/2018/12/gettyimages-461-800x450.jpg"/>
  </figure><br/><br/>
 </span>
 <span class="caption">
  华为总裁任正非遭起底。网络文章曝光其三任妻子，以及混乱的情史。（FABRICE COFFRINI/AFP/Getty Images)
 </span>
</div>
<hr/><div class="post_content" itemprop="articleBody">
 <p>
  【新唐人北京时间2019年06月25日讯】华为遭美国双杀禁令后，华为总裁
  <span href="https://www.ntdtv.com/gb/任正非.htm">
   任正非
  </span>
  混乱的情史也遭起底。网络曝光其有三任妻子。任正非也曾公开称，其前妻在文革时是红卫兵政委，天上飞的“白天鹅”，而自己是“小蛤蟆”。
 </p>
 <p>
  <span href="https://www.ntdtv.com/gb/任正非.htm">
   任正非
  </span>
  此前曾公开称，他的人生有两次婚姻，三个小孩。前妻性格很刚烈，在文化大革命中曾是重庆30万红卫兵的政委，是一个叱咤风云的人物。
 </p>
 <p>
  任正非说，自己是连红卫兵都参加不了的逍遥派，大学毕业没有女朋友，别人介绍其前妻，而其前妻能看上他，他表示真的不理解，“她是天上飞的‘白天鹅’，我是地上的‘小蛤蟆’，那时除了学习好，家庭环境也不好，我父亲还在‘牛棚’里，她怎么就看上我了？”
 </p>
 <p>
  任正非还说，后来他和前妻分开了，但他和姚凌办结婚证这些都是其前妻帮忙，小孩上户口也是前妻帮助的。
 </p>
 <p>
  去年12月，任正非的女儿、华为副董事兼首席财务官
  <span href="https://www.ntdtv.com/gb/孟晚舟.htm">
   孟晚舟
  </span>
  ，在加拿大被捕事件发酵之际，陆媒当时起底任正非有三任妻子，最小的是80后，比孟晚舟还年轻。
 </p>
 <p>
  报导说，现年74岁的任正非，其三任妻子分别是孟军，姚凌，苏薇。任正非第一任妻子孟军育有一女一子，
  <span href="https://www.ntdtv.com/gb/孟晚舟.htm">
   孟晚舟
  </span>
  和孟平。孟晚舟随母姓。
 </p>
 <p>
  网传孟军的父亲孟东波曾经担任过副省长，孟东波的领导杨超则担任过中共国家领导人的秘书，任正非正是靠第一个老婆孟军的父亲飞黄腾达。
 </p>
 <p>
  任正非和孟军结婚之后，经常去看望岳父孟东波和杨超，并从他们身上学到了不少东西。但相关消息并未得到官方的直接证实。
 </p>
 <p>
  据称，任正非和前妻孟军是经人介绍认识的，在一个单位，1982年中共大裁军，孟军来到深圳做了南油集团的高管，任正非也来到深圳，做了下属一家电子厂的领导。坊间传闻，任正非擅用职权，玩弄厂里的漂亮年轻女性。
 </p>
 <p>
  在公开场合，任正非称，两人是因理念不合而离婚。而内部传闻称，出身高干家庭的孟军无法忍受丈夫的风流成性，断然离婚。
 </p>
 <p>
  后来任正非由于工作上的失误给原单位造成了200万元人民币的经济损失，被单位辞退，中年创业之时孟军及其家人给予了很大支持。
 </p>
 <p>
  任正非的第二任妻子叫姚凌，是任正非的秘书，比任正非小十几岁。在昆明生下了女儿姚安娜 (Annable Yao)，直到 Annable Yao8个月大时，才来到深圳，与任正非一同生活。
 </p>
 <p>
  而这段婚姻最终还是破裂结局，至于原因，外界已不得而知。
 </p>
 <p>
  任正非的第三任妻子也是任的秘书，是80后美女苏薇，比任正非小几十岁，比孟晚舟还要年轻。据悉，苏薇是四川人，成都电子科大的硕士。目前任正非在深圳的饮食起居，都由苏薇照顾。两人未育有子女。
 </p>
 <p>
  任正非有三个孩子，一个儿子二个女儿。女儿孟晚舟和儿子孟平是和第一任妻子孟军所生，两个孩子都随母姓孟。
 </p>
 <p>
  任正非第三个孩子姚安娜现年21岁，去年参加巴黎名媛舞会首次亮相。姚安娜也是随母姓。任正非54岁高龄生下小女儿，对这个小女儿宠爱有加。
 </p>
 <p>
  2018年11月24日，全球顶级的名媛舞会在巴黎香格里拉大酒店举行，姚安娜受邀参加。并接受媒体专访而一度成为媒体新宠。
 </p>
 <p>
  《巴黎竞赛画报》在舞会开幕前，曾用长达6版的篇幅报导姚安娜的家庭背景、成长经历，并刊出了一张任正非与家人一起拍摄了全家福。任的小女儿也因此首次曝光。
 </p>
 <p>
  （记者李芸报导/责任编辑：李泉 ）
 </p>
 <div class="single_ad">
 </div>
</div>

<hr/>
手机上长按并复制下列链接或二维码分享本文章：<br/>
https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102608654.md <br/>
<a href='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102608654.md'><img src='https://github.com/gfw-breaker/banned-news/blob/master/pages/prog1138/a102608654.md.png'/></a> <br/>
原文地址（需翻墙访问）：https://www.ntdtv.com/gb/2019/06/25/a102608654.html


------------------------
#### [首页](https://github.com/gfw-breaker/banned-news/blob/master/README.md) &nbsp;|&nbsp; [Web代理](https://github.com/labour-camp/helloworld) &nbsp;|&nbsp; [一键翻墙软件](https://github.com/gfw-breaker/nogfw/blob/master/README.md) &nbsp;| [《九评共产党》](https://github.com/gfw-breaker/9ping.md/blob/master/README.md#九评之一评共产党是什么) | [《解体党文化》](https://github.com/gfw-breaker/jtdwh.md/blob/master/README.md) | [《共产主义的终极目的》](https://github.com/gfw-breaker/gczydzjmd.md/blob/master/README.md)

